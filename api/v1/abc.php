<?php
$oauthToken = $_REQUEST['oauth_token'];
$oauth_verifier = $_REQUEST['oauth_verifier'];
$org = $_REQUEST['org'];

$live = false;
$staging = false;

if($live){
    $api_url = "http://pikohana.com/api/v1/client/xero/token/".$oauthToken."/".$oauth_verifier . "/" . $org;
}
else if($staging){
    $api_url = "http://pikohana.com/beta/api/v1/client/xero/token/".$oauthToken."/".$oauth_verifier . "/" . $org;
}
else{
    $api_url = "http://localhost/pikohana-beta-dev/api/v1/client/xero/token/".$oauthToken."/".$oauth_verifier . "/" . $org;
}

header('Location:'.$api_url); die();
?>