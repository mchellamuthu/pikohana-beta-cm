<?php

ini_set('max_execution_time', 600);
set_time_limit(0);

// Turn off all error reporting
ini_set("display_errors", 0);
error_reporting(0);

date_default_timezone_set('Asia/Singapore');
//date_default_timezone_set('Asia/Kolkata');
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

require_once '../include/Config.php';
require_once '../include/utility.php';
require_once '../include/client.php';
require_once '../include/group.php';
require_once '../include/PassHash.php';
require_once '../include/adminUser.php';
require_once '../include/application.php';
require_once '../include/report.php';
require_once '../include/clientApplicationMap.php';
require_once '../include/reportClientApplicationMap.php';
require_once '../include/dashboardReport.php';
require_once '../include/xero/lib/XeroOAuth.php';
require '.././libs/Slim/Slim.php';
require_once  dirname(__FILE__).'/vendor/autoload.php';
require '../include/library/JSONMerger.php';
require '../include/library/ConSolPNL.php';
require '../include/library/TrendPNL.php';
require '../include/library/BalanceSheet.php';
require '../include/library/CashInFlow.php';
require '../include/library/flashMerger.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = null;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route)
{
    // Getting request headers
    $headers = apache_request_headers();

    $response = array();
    $app = \Slim\Slim::getInstance();
    global $is_client;
    /*if (!isset($headers['Is-Client'])) {
    $response["error"] = true;
    $response["message"] = CHOOSE_CLIENT_ADMIN;
    echoRespnse(400, $response);
    $app->stop();
    } */

    // Verifying Authorization Header
    if (isset($headers['X-Authorization'])) {
        $db = new utility();

        // get the api key
        $api_key = $headers['X-Authorization'];
        $is_client = $headers['Is-Client'];
        // validating api key
        if (!$db->isValidApiKey($api_key, $is_client)) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = API_ACCESS_DENIED;
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;

            // get user primary key id
            $result = $db->getUserID($api_key, $is_client);
            if ($result['error'] == false) {
                $user_id = $result['user_id'];
            } else {
                $response["error"] = true;
                $response["message"] = INVALID_USER;
                echoRespnse(401, $response);
                $app->stop();
            }
        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = API_KEY_MISSING;
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * ----------- ADMIN METHODS ---------------------------------
 */

/**
 * User Registration
 * url - /user
 * method - POST
 * params - name, email, password
 */
$app->post('/admin/client', 'authenticate', function () use ($app) {
    $response = array();
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('report_start_date', 'default_currency', 'companyname', 'weekly_monthly_report', 'audit_activity_report', 'mrr_graph'));
        $report_start_date = $app->request->post('report_start_date');
        $report_end_date = $app->request->post('report_end_date');
        $default_currency = $app->request->post('default_currency');
        $companyname = $app->request->post('companyname');
        $weekly_monthly_report = $app->request->post('weekly_monthly_report');
        $weekly_monthly_report = ($weekly_monthly_report == true) ? 1 : 0;
        $audit_activity_report = $app->request->post('audit_activity_report');
        $audit_activity_report = ($audit_activity_report == true) ? 1 : 0;
        $mrr_graph = $app->request->post('mrr_graph');
        $mrr_graph = ($mrr_graph == true) ? 1 : 0;
        $error = false;

        if ($error == false) {
            $db = new client();
            $response = $db->createClient($report_start_date, $report_end_date, $default_currency, $companyname, $weekly_monthly_report, $audit_activity_report, $mrr_graph);
            $db = null;
        }
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/admin/clientuser', 'authenticate', function () use ($app) {
    $response = array();
    global $is_client;

    verifyRequiredParams(array('name', 'mobile', 'email', 'role', 'password', 'password_confirm'));
    $user_name = $app->request->post('name');
    $mobile = $app->request->post('mobile');
    $email = $app->request->post('email');
    $role = $app->request->post('role');
    $password = $app->request->post('password');
    $selectedClients = $app->request->post('selectedClients');
    $client_master_id = $app->request->post('client_master_id');
    $selectedGroups = $app->request->post('selectedGroups');
    $is_admin = 0;
    $error = false;

    if (strlen($password) < 8) {
        $response["error"] = true;
        $response["message"] = PASSWORD_MINIMUM_LENGTH;
        $error = true;
    }

    if ($error == false) {
        $client = new client();
        $response = $client->createClientUser($user_name, $mobile, $email, $role, $password, $selectedClients, $selectedGroups, $client_master_id);
        if($response['error'] == false) {
            $client->clientGroupMapping($response['client_id'], $selectedGroups, $client_master_id);
        }
       $client = null;
    }
    echoRespnse(200, $response);
});

$app->get('/admin/clientmaster/:id', 'authenticate', function ($client_master_id) use ($app) {
    $response = array();
    $result = array();

    global $is_client;
    global $user_id;
    //verifyRequiredParams(array('client_master_id'));
    $db = new client();
    $db1 = new utility();
    $resp = array();
    $resp = $db1->getSuperAdminID($client_master_id);
    if ($resp["error"] == false) {
        $result = $db->getClientByClientID($resp['client_id']);
        if ($result["client_id"] == 0) {
            $response["error"] = true;
            $response["message"] = NO_RECORD_FOUND;
        } else {
            $response["clientDetails"] = $result;
            $response["error"] = false;
            $response["message"] = RECORD_FOUND;
        }
    } else {
        $response = $resp;
    }
    $db = null;
    // echo json response
    echoRespnse(200, $response);
});

$app->get('/admin/getAllClients/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $client = new client();
        $is_active = $app->request->get('is_active');
        $pdf_report_status = $app->request->get('pdf_report_status');
        $is_group = $app->request->get('is_group');
        $searchItems = array();
        if (strlen(trim($is_active)) > 0) {
            $searchItems["is_active"] = $is_active;
        }
        if (strlen(trim($pdf_report_status)) > 0) {
            $searchItems["pdf_report_status"] = $pdf_report_status;
        }
        if (strlen(trim($is_group)) > 0) {
            $searchItems["is_group"] = $is_group;
        }
        $result = $client->getAllClients($searchItems);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $client = null;
    echoRespnse(200, $result);
});

$app->put('/admin/client/:id', 'authenticate', function ($client_id) use ($app) {
    global $is_client;
    if ($is_client == 0) {
        // check for required params
        verifyRequiredParams(array('report_start_date', 'default_currency', 'company_name', 'weekly_monthly_report', 'audit_activity_report', 'mrr_graph'));
        $client_master_id = $app->request->put('client_master_id');
        $report_start_date = $app->request->put('report_start_date');
        $report_end_date = $app->request->put('report_end_date');
        $default_currency = $app->request->put('default_currency');
        $company_name = $app->request->put('company_name');
        $weekly_monthly_report = $app->request->put('weekly_monthly_report');
        $weekly_monthly_report = ($weekly_monthly_report == true) ? 1 : 0;
        $audit_activity_report = $app->request->put('audit_activity_report');
        $audit_activity_report = ($audit_activity_report == true) ? 1 : 0;
        $mrr_graph = $app->request->put('mrr_graph');
        $mrr_graph = ($mrr_graph == true) ? 1 : 0;
        $db = new client();
        $response = array();

        $response = $db->updateClientByAdmin($client_master_id, $report_start_date, $report_end_date, $default_currency, $company_name, $weekly_monthly_report, $audit_activity_report, $mrr_graph);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->put('/admin/client/access/:id', 'authenticate', function ($client_master_id) use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('is_active'));
        $is_active = $app->request->put('is_active');
        $db = new client();
        $response = array();
        $response = $db->accessClient($client_master_id, $is_active);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/admin/clientusers/', 'authenticate', function () use ($app) {
    global $is_client;
    $response = array();
    $db = new client();
    verifyRequiredParams(array('client_master_id'));
    $client_master_id = $app->request->get('client_master_id');
    $is_active = $app->request->get('is_active');
    $searchItems = array();
    $searchItems["client_master_id"] = $client_master_id;
    if (strlen(trim($is_active)) > 0) {
        $searchItems["is_active"] = $is_active;
    }
    $searchItems['is_admin'] = 0;
    $result = $db->getAllClients($searchItems);
    $db = null;
    echoRespnse(200, $result);
});

$app->put('/admin/clientuser/:id', 'authenticate', function ($client_id) use ($app) {
    global $is_client;

    // check for required params
    verifyRequiredParams(array('name', 'mobile', 'email'));
    $name = $app->request->put('name');
    $mobile = $app->request->put('mobile');
    $email = $app->request->put('email');
    $is_admin = $app->request->put('is_admin');
    $client_master_id = $app->request->put('client_master_id');
    $selectedClients = $app->request->put('selectedClients');
    $deletedClients = $app->request->put('clientsToRemove');
    $selectedGroups = $app->request->put('selectedGroups');
    $groupsToRemove = $app->request->put('groupsToRemove');

    $client = new client();
    $response = array();
    $response = $client->updateClientUserByAdmin($client_id, $name, $mobile, $email, $is_admin, $client_master_id, $selectedClients, $deletedClients, $selectedGroups, $groupsToRemove, true);
    $client = null;
    echoRespnse(200, $response);
});

$app->post('/admin/ClientUserApplication', 'authenticate', function () use ($app) {
    global $is_client;

    verifyRequiredParams(array('client_id', 'client_master_id'));
    $client_id = $app->request->post('client_id');
    $client_master_id[0]['id'] = $app->request->post('client_master_id');
    $application_ids = $app->request->post('application_ids');
    $category_sub_ids = $app->request->post('category_sub_ids');
    $report_ids = $app->request->post('report_ids');

    if (sizeof($report_ids) > 0) {
        $report_ids = array_unique($report_ids);
    }

    if (sizeof($category_sub_ids) > 0) {
        $category_sub_ids = array_unique($category_sub_ids);
    }

    $db = new client();
    $response = array();

    $response = $db->updateClientUserApplicationByAdmin($client_id, $client_master_id, $application_ids, $category_sub_ids, $report_ids, true);

    $db = null;
    echoRespnse(200, $response);
});

$app->put('/admin/clientuser/access/:id', 'authenticate', function ($client_id) use ($app) {
    global $is_client;
    verifyRequiredParams(array('is_active'));
    $is_active = $app->request->put('is_active');
    $db = new client();
    $response = array();
    $response = $db->accessClientUser($client_id, $is_active);
    $db = null;
    echoRespnse(200, $response);
});

$app->delete('/admin/budget/:id', 'authenticate', function ($client_master_id) use ($app) {
    global $is_client;

    $db = new dashboardReport();
    $response = array();
    $response = $db->hardDeleteBudget($client_master_id);

    $db = null;
    echoRespnse(200, $response);
});

$app->post('/admin/application', 'authenticate', function () use ($app) {
    global $is_client;
    // check for required
    $auth_key = "";
    if ($is_client == 0) {
        verifyRequiredParams(array('application_name', 'application_type', 'application_api_url', 'is_system_token_required', 'is_user_auth_key_required', 'app_site_url'));
        $response = array();
        $application_name = $app->request->post('application_name');
        $application_type = $app->request->post('application_type');
        $application_api_url = $app->request->post('application_api_url');
        $is_system_token_required = $app->request->post('is_system_token_required');
        $is_user_auth_key_required = $app->request->post('is_user_auth_key_required');
        $app_site_url = $app->request->post('app_site_url');
        if ($is_system_token_required == 1) {
            verifyRequiredParams(array('customer_key', 'secret_key'));
            $customer_key = $app->request->post('customer_key');
            $secret_key = $app->request->post('secret_key');
            $auth_key = $customer_key . ":" . $secret_key;
        }
        if (isset($_FILES)) {
            if (isset($_FILES['application_logo'])) {
                $application_logo = array();
                $application_logo = $_FILES['application_logo'];
            }
        }
        $db = new application();
        // creating new invite
        $response = $db->addNewApplication($application_name, $application_logo, $application_type, $application_api_url, $is_system_token_required, $auth_key, $is_user_auth_key_required, $app_site_url);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/admin/application/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $db = new application();
        $is_active = $app->request->get('is_active');
        if (strlen($is_active) > 0) {
            $searchItems['is_active'] = $is_active;
        }
        $result = $db->getAllApplications($searchItems);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->put('/admin/application/access/:id', 'authenticate', function ($application_id) use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $is_active = $app->request->put('is_active');
        $db = new application();
        $response = array();
        $response = $db->accessApplication($application_id, $is_active);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->post('/admin/application/update', 'authenticate', function () use ($app) {
    // check for required params
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('application_id', 'application_name', 'application_type', 'application_api_url', 'app_site_url'));
        $application_id = $app->request->post('application_id');
        $application_name = $app->request->post('application_name');
        $application_type = $app->request->post('application_type');
        $application_api_url = $app->request->post('application_api_url');
        $app_site_url = $app->request->post('app_site_url');
        /*$is_auth_required = $app->request->post('is_auth_required');
        if ($is_auth_required == 1) {
        verifyRequiredParams(array('customer_key', 'secret_key'));
        $customer_key = $app->request->post('customer_key');
        $secret_key = $app->request->post('secret_key');
        $auth_key = $customer_key . ":" . $secret_key;
        } */
        /////To Update customer_key and secret_key to DB - changes done by systimaNX 01-08-2017
        if ($app->request->post('customer_key') != '' && $app->request->post('secret_key') != '') {
            verifyRequiredParams(array('customer_key', 'secret_key'));
            $customer_key = $app->request->post('customer_key');
            $secret_key = $app->request->post('secret_key');
            $auth_key = $customer_key . ":" . $secret_key;} else {
            $auth_key = "";
        }
        if (isset($_FILES)) {
            if (isset($_FILES['application_logo'])) {
                $application_logo = array();
                $application_logo = $_FILES['application_logo'];
            }
        }
        $db = new application();
        $response = array();
        $response = $db->updateApplication($application_id, $application_name, $application_logo, $application_type, $application_api_url, $app_site_url, $auth_key);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->post('/admin/report', 'authenticate', function () use ($app) {
    global $is_client;
    // check for required params
    if ($is_client == 0) {
        verifyRequiredParams(array('report_name', 'report_auth_key', 'is_source_single', 'report_type'));
        $response = array();
        $report_name = $app->request->post('report_name');
        $report_auth_key = $app->request->post('report_auth_key');
        $is_source_single = $app->request->post('is_source_single');
        $report_type = $app->request->post('report_type');
        if (isset($_FILES)) {
            if (isset($_FILES['report_logo'])) {
                $report_logo = array();
                $report_logo = $_FILES['report_logo'];
            }
        }
        $db = new report();
        // creating new invite
        $response = $db->addNewReport($report_name, $report_auth_key, $report_logo, $is_source_single, $report_type);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/admin/report/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $db = new report();
        $is_active = $app->request->get('is_active');
        if (strlen($is_active) > 0) {
            $searchItems['is_active'] = $is_active;
        }
        $result = $db->getAllReports($searchItems);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->put('/admin/report/access/:id', 'authenticate', function ($report_id) use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('is_active'));
        $is_active = $app->request->put('is_active');
        $db = new report();
        $response = array();
        $response = $db->accessReport($report_id, $is_active);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->post('/admin/report/update', 'authenticate', function () use ($app) {
    // check for required params
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('report_id', 'report_name'));
        $report_id = $app->request->post('report_id');
        $report_name = $app->request->post('report_name');
        $report_auth_key = $app->request->post('report_auth_key');

        if (isset($_FILES)) {
            if (isset($_FILES['report_logo'])) {
                $report_logo = array();
                $report_logo = $_FILES['report_logo'];
            }
        }
        $db = new report();
        $response = array();

        $response = $db->updateReport($report_id, $report_name, $report_auth_key, $report_logo);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->post('/admin/superuser', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        // check for required params
        verifyRequiredParams(array('admin_name', 'admin_phone', 'admin_email', 'admin_role_id', 'admin_password'));
        $response = array();
        $admin_name = $app->request->post('admin_name');
        $admin_phone = $app->request->post('admin_phone');
        $admin_email = $app->request->post('admin_email');
        $admin_role_id = $app->request->post('admin_role_id');
        $admin_password = $app->request->post('admin_password');

        $db = new adminUser();
        // creating new invite
        $response = $db->addNewAdmin($admin_name, $admin_phone, $admin_email, $admin_role_id, $admin_password);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->post('/admin/user', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        // check for required params
        verifyRequiredParams(array('admin_name', 'admin_phone', 'admin_email', 'admin_role_id', 'admin_password'));
        $response = array();
        $admin_name = $app->request->post('admin_name');
        $admin_phone = $app->request->post('admin_phone');
        $admin_email = $app->request->post('admin_email');
        $admin_role_id = $app->request->post('admin_role_id');
        $admin_password = $app->request->post('admin_password');

        $db = new adminUser();
        // creating new invite
        $response = $db->addNewAdmin($admin_name, $admin_phone, $admin_email, $admin_role_id, $admin_password);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/admin/singleuser/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $db = new adminUser();
        $admin_email = $app->request->get('admin_email');
        $result = $db->getAdminUserDetail($admin_email);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->get('/admin/single', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $db = new adminUser();
        global $user_id;
        $result = $db->getAdminUserByID($user_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->get('/admin/user/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $db = new adminUser();
        $is_active = $app->request->get('is_active');
        $result = $db->getAllAdminUsers($is_active);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->put('/admin/user/access/:id', 'authenticate', function ($admin_user_id) use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('is_active'));
        $is_active = $app->request->put('is_active');
        $db = new adminUser();
        $response = array();
        $response = $db->accessAdminUser($admin_user_id, $is_active);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->put('/admin/user/:id', 'authenticate', function ($admin_user_id) use ($app) {
    global $is_client;
    if ($is_client == 0) {
        // check for required params
        verifyRequiredParams(array('admin_name', 'admin_phone', 'admin_email', 'admin_role_id'));
        $admin_name = $app->request->put('admin_name');
        $admin_phone = $app->request->put('admin_phone');
        $admin_email = $app->request->put('admin_email');
        $admin_role_id = $app->request->put('admin_role_id');
        $db = new adminUser();
        $response = array();

        $response = $db->updateAdmin($admin_user_id, $admin_name, $admin_phone, $admin_email, $admin_role_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->post('/admin/login', function () use ($app) {
    // check for required params

    verifyRequiredParams(array('admin_email', 'admin_password'));

    // reading post params
    $admin_email = $app->request->post('admin_email');
    $admin_password = $app->request->post('admin_password');
    $response = array();

    $db = new adminUser();
    // check for correct user_id and password
    $response = $db->checkLogin($admin_email, $admin_password);

    $db = null;
    $user = null;

    echoRespnse(201, $response);
});

$app->put('/admin/changepwd', 'authenticate', function () use ($app) {
    verifyRequiredParams(array('oldpassword', 'newpassword'));
    // check for required params
    global $is_client;
    if ($is_client == 0) {

        $response = array();
        $oldpassword = $app->request->put('oldpassword');
        $newpassword = $app->request->put('newpassword');
        $error = false;
        if (strlen($oldpassword) < 8) {
            $response["error"] = true;
            $response["message"] = PASSWORD_MINIMUM_LENGTH;
            $error = true;
        }
        if (strlen($newpassword) < 8) {
            $response["error"] = true;
            $response["message"] = PASSWORD_MINIMUM_LENGTH;
            $error = true;
        }

        if ($error == false) {
            global $user_id;
            $db = new adminUser();
            $response = $db->changePassword($user_id, $oldpassword, $newpassword);
            $db = null;
        }
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    echoRespnse(200, $response);

});

$app->put('/admin/mailkey', function () use ($app) {
    $response = array();
    verifyRequiredParams(array('admin_email'));
    $admin_email = $app->request->put('admin_email');
    $db = new adminUser();
    $response = $db->sendResetKey($admin_email);
    $db = null;
    echoRespnse(200, $response);
});

$app->put('/admin/resetpwd', function () use ($app) {
    $response = array();
    verifyRequiredParams(array('reset_key', 'new_password'));
    $reset_key = $app->request->put('reset_key');
    $new_password = $app->request->put('new_password');
    $db = new adminUser();
    $response = $db->resetPassword($reset_key, $new_password);
    $db = null;
    echoRespnse(200, $response);
});

$app->put('/admin/resetadminpwd', 'authenticate', function () use ($app) {
    global $is_client;
    $response = array();
    if ($is_client == 0) {
        verifyRequiredParams(array('admin_user_id'));
        $db = new adminUser();
        $admin_user_id = $app->request->put('admin_user_id');
        $response = $db->resetAdminPassword($admin_user_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->post('/admin/budget', 'authenticate', function () use ($app) {
    global $is_client;
    // check for required params
    if ($is_client == 0) {
        verifyRequiredParams(array('client_master_id', 'client_category_id1', 'sub_category_id1'));
        $response = array();
        $client_master_id = $app->request->post('client_master_id');
        $client_category_id1 = $app->request->post('client_category_id1');
        $sub_category_id1 = $app->request->post('sub_category_id1');
        $client_category_id2 = $app->request->post('client_category_id2');
        $sub_category_id2 = $app->request->post('sub_category_id2');
        //    $org_name = trim($app->request->post('org_name'));

        if (isset($_FILES)) {
            if (isset($_FILES['budget_report'])) {
                $csvData = array();
                $csvData = $_FILES['budget_report'];
            }
        }
        $db = new dashboardReport();
        $response = $db->insertBudgetForClient($client_master_id, $client_category_id1, $sub_category_id1, $client_category_id2, $sub_category_id2, $csvData);

    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/admin/category/', 'authenticate', function () use ($app) {
    global $is_client;
    // check for required params
    if ($is_client == 0) {
        verifyRequiredParams(array('client_id'));
        $response = array();
        $client_id = $app->request->get('client_id');

        $db = new dashboardReport();
        $searchItems = array();
        // creating new invite
        $searchItems['client_id'] = $client_id;
        $searchItems["groupby"] = 1;
        $response = $db->getAllTrackingCategories($searchItems);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->put('/admin/resetclientpwd', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('client_id'));
        $response = array();
        $db = new client();

        $client_id = $app->request->put('client_id');
        $response = $db->resetPasswordByAdmin($client_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/admin/category/master', 'authenticate', function () use ($app) {
    global $is_client;
    // check for required params
    if ($is_client == 0) {
        verifyRequiredParams(array('client_master_id'));
        $response = array();
        $client_master_id = $app->request->get('client_master_id');
        $db = new dashboardReport();
        $searchItems = array();
        $utility = new utility();
        $resp = array();
        $resp = $utility->getSuperAdminID($client_master_id);
        if ($resp["error"] == false) {
            $searchItems['client_id'] = $resp['client_id'];
            $searchItems['client_master_id'] = $client_master_id;
            $searchItems["groupby"] = 1;
            $response = $db->getAllTrackingCategories($searchItems);
        } else {
            $response = $resp;
        }
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/admin/metric/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('client_master_id'));
        $response = array();
        $db = new dashboardReport();
        $client_master_id = $app->request->get('client_master_id');
        $result = $db->getMetrics($client_master_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->post('/admin/metric/employee', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('client_master_id'));
        $response = array();
        $db = new dashboardReport();
        global $user_id;
        $client_master_id = $app->request->post('client_master_id');
        $employees = $app->request->post('employees');
        $result = $db->saveEmployeesForMetrics($client_master_id, $employees);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $result);
});

/* This method for testing purpose */
$app->get('/admin/generatemetric/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('client_master_id'));
        $response = array();
        $db = new dashboardReport();
        $utility = new utility();
        $client_id = $app->request->get('client_id');
        $client_master_id = $app->request->get('client_master_id');
        $sub_category_id1 = 1000;
        // $sub_category_id2 = 0;

        $res_client_master_detail = $utility->getClientMasterDetailByID('client_master_detail', 'client_master_id', $client_master_id);
        if ($res_client_master_detail["error"] == false) {
            $report_start_date = $res_client_master_detail["report_start_date"];
        }

        // $res_client_master = $utility->getFieldByID('client_master', 'client_master_id', $client_master_id, 'client_id');
        // $client_id = $res_client_master['field_key'];

        $res_category_count = $utility->getCategoryCountByClientID('category_subcategory', $client_id, $client_master_id);
        if ($res_category_count["error"] == false) {
            $count = $res_category_count["count"];
            if ($count == 2) {
                $sub_category_id2 = 1000;
            }
        }

        $result = $db->GenerateMetrics($client_master_id, $client_id, $report_start_date, $sub_category_id1, $sub_category_id2);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    $utility = null;
    echoRespnse(200, $result);
});

$app->get('/admin/roles/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $db = new adminUser();
        $is_active = $app->request->get('is_active');
        $result = $db->getRoles();
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->get('/admin/userroles/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $db = new adminUser();
        $is_active = $app->request->get('is_active');
        $result = $db->getUserRoles();
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $result);
});

/*
 * Upload bizfile file for companies
 * url - /admin
 * method - POST
 * params - client_master_id, file_path
 */
$app->post('/admin/bizfile/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('client_master_id'));
        if (isset($_FILES['bizfile_pdf'])) {
            $bizfile_upload = array();
            $photo_scan = $_FILES['bizfile_pdf'];
            $response = array();
            $client = new client();
            $client_master_id = $app->request->post('client_master_id');
            $response = $client->bizfileUpload($client_master_id, $_FILES);
        } else {
            $response['error'] = true;
            $response['message'] = BIZ_FILE_VALIDATION;
        }
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $client = null;
    echoRespnse(200, $response);
});

/*
 * save weekly report settings for companies
 * url - /admin
 * method - POST
 * params - client_master_id, report_run_date, report_run_time
 */
$app->post('/admin/saveweeklyreport/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('client_master_id', 'report_run_date', 'report_run_time'));
        $response = array();
        $client = new client();
        $client_master_id = $app->request->post('client_master_id');
        $report_run_date = $app->request->post('report_run_date');
        $report_run_time = $app->request->post('report_run_time');
        $response = $client->saveWeeklyreport($client_master_id, $report_run_date, $report_run_time);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $client = null;
    echoRespnse(200, $response);
});

/*
 * Edit weekly report settings for companies
 * url - /admin
 * method - PUT
 * params - client_master_id, report_run_date, report_run_time
 */
$app->put('/admin/saveweeklyreport/:id', 'authenticate', function ($client_master_id) use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('client_master_id', 'report_run_date', 'report_run_time'));
        $response = array();
        $client = new client();
        $client_master_id = $app->request->put('client_master_id');
        $report_run_date = $app->request->put('report_run_date');
        $report_run_time = $app->request->put('report_run_time');
        $response = $client->updateWeeklyreport($client_master_id, $report_run_date, $report_run_time);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $client = null;
    echoRespnse(200, $response);
});

/*
 * List view weekly report settings for companies
 * url - /admin
 * method - POST
 * params - Nill
 */
$app->get('/admin/listweeklyreport/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $client = new client();
        $response = $client->listWeeklyreport();
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $client = null;
    echoRespnse(200, $response);
});

/*
 * Delete weekly report settings for companies
 * url - /admin
 * method - DELETE
 * params - client_master_id
 */
$app->delete('/admin/deleteweeklyreport/:id', 'authenticate', function ($client_master_id) use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $client = new client();
        $response = $client->deleteWeeklyreport($client_master_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $client = null;
    echoRespnse(200, $response);
});

$app->get('/admin/getAllUsers', 'authenticate', function () use ($app) {
    $response = array();
    $client = new client();
    $response = $client->getAllUsers();
    $client = null;
    echoRespnse(200, $response);
});

$app->get('/admin/getUserdetails', 'authenticate', function () use ($app) {
    $response = array();
    verifyRequiredParams(array('client_id'));
    $client = new client();
    $client_id = $app->request->get('client_id');
    $response = $client->getUsersDetails($client_id);
    $client = null;
    echoRespnse(200, $response);
});

$app->get('/admin/getUserApplicationDetails', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        verifyRequiredParams(array('client_id', 'client_master_id'));
        $client = new client();
        // $utility = new utility();
        // $is_admin = 0;
        $client_id = $app->request->get('client_id');
        $client_master_id = $app->request->get('client_master_id');

        // $res_is_admin = $utility->getFieldByID('client_master', 'client_id', $user_id, 'is_admin');
        // if ($res_is_admin['error'] == false) {
        //     $is_admin = $res_is_admin['field_key'];
        // }

        // if ($is_admin != 1) {
        //     $res_super_admin = $utility->getSuperAdminID($client_master_id);
        //     $client_id = $res_super_admin['client_id'];
        // }

        $result = $client->getApplicationDetails($client_id, $client_master_id);
        $response['applicationDetails'] = $result;
        $response['error'] = false;
        $response['message'] = RECORD_FOUND;
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }

    $client = null;
    // $utility = null;
    echoRespnse(200, $response);
});

/**
 * ----------- CLIENT APPLICATION METHODS  ---------------------------------
 */

/**
 * User Login
 * url - /login
 * method - POST
 * params - email, password
 */

$app->post('/client/login', function () use ($app) {
    // check for required params
    verifyRequiredParams(array('email', 'password'));
    // reading post params
    $email = $app->request->post('email');
    $password = $app->request->post('password');
    $response = array();
    $db = new client();

    // check for correct user_id and password
    $response = $db->checkLogin($email, $password);
    $user = array();
    if (sizeof($response['userdetails']) > 0) {
        $response["error"] = false;
        $response["message"] = USER_LOGGED_IN;
    }
    $db = null;
    $user = null;

    echoRespnse(200, $response);
});

$app->put('/client/mailkey', function () use ($app) {
    $response = array();
    verifyRequiredParams(array('email'));
    $email = $app->request->put('email');
    $db = new client();
    $response = $db->sendResetKey($email);

    $db = null;
    echoRespnse(200, $response);
});

$app->put('/client/resetpwd', function () use ($app) {
    $response = array();
    verifyRequiredParams(array('reset_key', 'new_password'));
    $reset_key = $app->request->put('reset_key');
    $new_password = $app->request->put('new_password');
    $client = new client();
    $response = $client->resetPassword($reset_key, $new_password);
    $client = null;
    echoRespnse(200, $response);
});

$app->put('/client/resetclientpwd', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        verifyRequiredParams(array('client_id'));
        $response = array();
        $client = new client();

        $client_id = $app->request->put('client_id');
        $response = $client->resetPasswordByAdmin($client_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $client = null;
    echoRespnse(200, $response);
});

$app->get('/client/single', 'authenticate', function () use ($app) {
    $response = array();
    global $is_client;
    if ($is_client == 1) {
        global $user_id;
        $client = new client();
        $client_master_id = $app->request->get('client_master_id');
        $response = $client->getClientByClientID($user_id, $client_master_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }

    $client = null;
    // echo json response
    echoRespnse(200, $response);
});

$app->put('/client/secquery', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        // check for required params
        verifyRequiredParams(array('security_question', 'security_answer'));
        global $user_id;
        $security_question = $app->request->put('security_question');
        $security_answer = $app->request->put('security_answer');
        $response = array();
        $db = new client();
        $response = $db->updateSecurityQuestion($user_id, $security_question, $security_answer);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->post('/client/profile', 'authenticate', function () use ($app) {
    $response = array();
    global $user_id;

    verifyRequiredParams(array('name', 'email', 'mobile'));
    if (isset($_FILES)) {
        if (isset($_FILES['client_photo'])) {
            $client_photo = array();
            $client_photo = $_FILES['client_photo'];
        } else {
            $client_photo = $app->request->put('client_photo');
            if ($client_photo == "null") {
                $client_photo = null;
            }
        }
        if (isset($_FILES['company_logo'])) {
            $company_logo = array();
            $company_logo = $_FILES['company_logo'];
        } else {
            $company_logo = $app->request->put('company_logo');
            if ($company_logo == "null") {
                $company_logo = null;
            }
        }
    }
    $name = $app->request->put('name');
    $email = $app->request->put('email');
    $dob = $app->request->put('dob');
    $mobile = $app->request->put('mobile');
    $company_name = $app->request->put('company_name');
    $company_owner = $app->request->put('company_owner');
    $company_address = $app->request->put('company_address');
    $time_zone = $app->request->put('time_zone');
    $notification_status = $app->request->put('notification_status');
    $client_master_id = $app->request->post('client_master_id');

    $db = new client();
    $response = $db->updateProfile($user_id, $name, $email, $mobile, $client_photo, $company_name, $company_owner, $company_address, $company_logo, $time_zone, $notification_status, $client_master_id);
    $db = null;
    echoRespnse(200, $response);
});

/**
 * Change password in db
 * method POST
 * params - name
 * url - /chpasswd/
 */
$app->put('/client/changepwd', 'authenticate', function () use ($app) {
    // check for required params
    global $is_client;
    if ($is_client == 1) {
        verifyRequiredParams(array('oldpassword', 'newpassword'));

        $response = array();
        $oldpassword = $app->request->put('oldpassword');
        $newpassword = $app->request->put('newpassword');
        $error = false;
        if (strlen($oldpassword) < 8) {
            $response["error"] = true;
            $response["message"] = PASSWORD_MINIMUM_LENGTH;
            $error = true;
        }
        if (strlen($newpassword) < 8) {
            $response["error"] = true;
            $response["message"] = PASSWORD_MINIMUM_LENGTH;
            $error = true;
        }

        if ($error == false) {
            global $user_id;
            $db = new client();
            $response = $db->changePassword($user_id, $oldpassword, $newpassword);
            $db = null;
        }
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    echoRespnse(200, $response);
});

$app->get('/client/notification', 'authenticate', function () use ($app) {
    $response = array();
    global $is_client;
    if ($is_client == 1) {
        global $user_id;
        $db = new client();
        $response = $db->getUnReadNotifications($user_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }

    $db = null;
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/client/notification', 'authenticate', function () use ($app) {
    $response = array();
    global $is_client;
    if ($is_client == 1) {
        verifyRequiredParams(array('notification_message', 'is_read'));
        $notification_message = $app->request->post('notification_message');
        $is_read = $app->request->post('is_read');
        global $user_id;
        $db = new client();
        $response = $db->saveNotification($user_id, $notification_message, $is_read);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }

    $db = null;
    // echo json response
    echoRespnse(200, $response);
});

$app->put('/client/setnotify', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        // check for required params
        verifyRequiredParams(array('status'));
        global $user_id;
        $status = $app->request->put('status');
        $response = array();
        $db = new client();
        $response = $db->updateNotification($user_id, $status);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->put('/client/setread/:id', 'authenticate', function ($notification_id) use ($app) {
    global $is_client;
    if ($is_client == 1) {
        // check for required params
        verifyRequiredParams(array('is_read'));
        global $user_id;
        $is_read = $app->request->put('is_read');
        $response = array();
        $db = new client();
        $response = $db->markAsRead($notification_id, $user_id, $is_read);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->post('/client/application', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        // check for required params
        verifyRequiredParams(array('application_id', 'is_active', 'client_master_id'));
        $response = array();
        $application_id = $app->request->post('application_id');
        $client_master_id = $app->request->post('client_master_id');
        $auth_key = $app->request->post('auth_key');
        $is_active = $app->request->post('is_active');
        $db = new clientApplicationMap();
        global $user_id;
        $response = $db->addNewClientApplicationMap($user_id, $client_master_id, $application_id, $auth_key, $is_active);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/client/application/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        global $user_id;
        $response = array();
        $db = new clientApplicationMap();
        $is_active = $app->request->get('is_active');
        $client_master_id = $app->request->get('client_master_id');

        $result = $db->getAllClientApplicationWrapper($user_id, $client_master_id, $is_active);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->delete('/client/application/:id', 'authenticate', function ($client_application_map_id) use ($app) {
    global $is_client;
    if ($is_client == 1) {
        $db = new clientApplicationMap();
        global $user_id;
        $response = array();
        $response = $db->deleteClientApplicationMap($client_application_map_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->put('/client/application/:id', 'authenticate', function ($client_application_map_id) use ($app) {
    global $is_client;
    if ($is_client == 1) {
        // check for required params
        verifyRequiredParams(array('application_id', 'is_active', 'client_master_id'));
        global $user_id;
        $application_id = $app->request->put('application_id');
        $auth_key = $app->request->put('auth_key');
        $is_active = $app->request->put('is_active');
        $client_master_id = $app->request->put('client_master_id');
        $db = new clientApplicationMap();
        $response = array();
        $aResponse = array();
        $response = $db->updateClientApplicationMap($client_application_map_id, $user_id, $application_id, $client_master_id, $auth_key, $is_active);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

/************* Report User application map **********/

$app->post('/client/map/reportapplication', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        // check for required params
        verifyRequiredParams(array('report_id', 'application_id'));
        $response = array();
        $report_id = $app->request->post('report_id');
        $application_id = $app->request->post('application_id');
        $client_application_map_id = $app->request->post('client_application_map_id');

        $db = new reportClientApplicationMap();
        global $user_id;

        $response = $db->addNewReportClientApplicationMap1($report_id, $user_id, $application_id, $client_application_map_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/client/map/reportapplication/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        global $user_id;
        $response = array();
        $searchItems = array();
        $db = new reportClientApplicationMap();
        $is_active = $app->request->get('is_active');
        $client_master_id = $app->request->get('client_master_id');
        $searchItems['client_id'] = $user_id;
        $searchItems['client_master_id'] = $client_master_id;
        if (strlen($is_active) > 0) {
            $searchItems['is_active'] = $is_active;
            $searchItems['is_active1'] = $is_active;
        }

        $result = $db->getAllReportClientApplicationMapWrapper($searchItems);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->delete('/client/map/reportapplication/:id', 'authenticate', function ($report_client_app_map_id) use ($app) {
    global $is_client;
    if ($is_client == 1) {
        global $user_id;
        $response = array();
        $db = new reportClientApplicationMap();
        $response = $db->deleteReportClientApplicationMap($report_client_app_map_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->put('/client/map/reportapplication/:id', 'authenticate', function ($report_client_app_map_id) use ($app) {
    global $is_client;
    if ($is_client == 1) {
        // check for required params
        verifyRequiredParams(array('application_id', 'client_master_id'));
        global $user_id;

        $application_id = $app->request->put('application_id');
        $client_master_id = $app->request->put('client_master_id');

        $db = new reportClientApplicationMap();
        $response = array();

        $response = $db->updateReportClientApplicationMap($report_client_app_map_id, $user_id, $application_id, $client_master_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/client/report/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        $response = array();
        $db = new report();
        $is_active = $app->request->get('is_active');
        if (strlen($is_active) > 0) {
            $searchItems['is_active'] = $is_active;
        }
        $result = $db->getAllReports($searchItems);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->post('/client/report', 'authenticate', function () use ($app) {
    $response = array();
    global $is_client;
    if ($is_client == 1) {
        global $user_id;
        verifyRequiredParams(array('start_date', 'end_date'));
        $start_date = $app->request->post('start_date');
        $end_date = $app->request->post('end_date');
        $end_date .= " 23:59:59";
        $db = new client();
        $response = $db->doShowReport($user_id, $start_date, $end_date);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }

    $db = null;
    // echo json response
    echoRespnse(200, $response);
});

$app->put('/client/archive/:id', 'authenticate', function ($report_history_id) use ($app) {
    global $is_client;
    if ($is_client == 1) {
        global $user_id;
        $response = array();
        $db = new client();
        $response = $db->archiveReport($report_history_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/client/kpi/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        verifyRequiredParams(array('report_id'));
        $response = array();
        $db = new dashboardReport();
        $utility = new utility();
        global $user_id;
        $report_id = $app->request->get('report_id');
        $client_master_id = $app->request->get('client_master_id');
        $result = $db->getKPIData($user_id, $report_id, $client_master_id);
        /*
        Update Registration Number if it is not exist
        Prabhu - 24-11-2017
         */
        $db->UpdateRegistrationNumber($user_id, $result["kpiReport"]['registration_number']);

        $res_client_master_detail = $utility->getClientMasterDetailByID('client_master_detail', 'client_master_id', $client_master_id);
        if ($res_client_master_detail["error"] == false) {
            $financial_year_start_date = $res_client_master_detail["report_start_date"];
            $financial_year_end_date = substr($utility->dateAfterDays(1, $financial_year_start_date, 'years'), 0, -3);
        }

        /*
        Audit Activities Starts Here
        Prabhu - 24-11-2017
         */
        $audit_activity_array = array("schedule_next_audit", "begin_audit", "complete_audit", "hold_agm", "file_annual_return", "file_eci", "file_prior_year's_tax_return");
        foreach ($audit_activity_array as $record) {
            $audit_type = $record;
            $audit_date = $result["kpiReport"]["$record"];
            switch($record){
                case 'schedule_next_audit':
                    $audit_date = $utility->dateAfterDays(schedule_next_audit, $financial_year_end_date);
                    break;
                case 'begin_audit':
                    $audit_date = $utility->dateAfterDays(begin_audit, $financial_year_end_date);
                    break;
                case 'complete_audit':
                    $audit_date = $utility->dateAfterDays(complete_audit, $financial_year_end_date);
                    break;
                case 'hold_agm':
                    $audit_date = $utility->dateAfterDays(hold_agm, $financial_year_end_date);
                    break;
                case 'file_annual_return':
                    $audit_date = $utility->dateAfterDays(file_annual_return, $financial_year_end_date);
                    break;
                case 'file_eci':
                    $audit_date = $utility->dateAfterDays(file_eci, $financial_year_end_date);
                    break;
                case "file_prior_year's_tax_return":
                    $audit_date = date("Y", strtotime("+1 year", strtotime($financial_year_end_date))) . "-11-01";
                    break;
            }
            if (!empty($audit_type)) {
                $db->SaveAuditActivities($user_id, $audit_type, $audit_date, $client_master_id);
            }
        }
        /*Audit Activities Ends Here*/
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->get('/client/salesreport/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        verifyRequiredParams(array('report_id', 'client_master_id'));
        $response = array();
        $db = new dashboardReport();
        global $user_id;
        $report_id = $app->request->get('report_id');
        $client_master_id = $app->request->get('client_master_id');
        $response = $db->getSalesReport($user_id, $report_id, $client_master_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/client/xero/authenticate', 'authenticate', function () use ($app) {
    global $is_client;
    $dash_db = new dashboardReport();
    $utility = new utility();
    $is_admin = 0;
    $response = array();

    $sub_category_id2 = $app->request->get('sub_category_id2');
    $sub_category_id1 = $app->request->get('sub_category_id1');
    $mtd = $app->request->get('mtd');
    $refresh = $app->request->get('refresh');
    $pagename = $app->request->get('page_name');
    $client_master_id = $app->request->get('client_master_id');
    $user_id = $app->request->get('client_id');
    $actual_user_id = $user_id;

    $res_is_admin = $utility->getFieldByID('client_master', 'client_id', $user_id, 'is_admin');
    if ($res_is_admin["error"] == false) {
        $is_admin = $res_is_admin["field_key"];
    }

    if ($is_admin != 1) {
        $res_super_admin = $utility->getSuperAdminID($client_master_id);
        $user_id = $res_super_admin['client_id'];
        $refresh = 0;
    }

    $pageResult = $utility->isValidToken('client_oauth_token', 'client_id', $user_id);
    $is_authenticated = ($pageResult["error"] == false) ? $pageResult['isValid'] : 0;
    /* if ($is_authenticated == 0) {
        $dash_db->XeroRefreshToken($user_id);
    } */
    // if is_authenticated true, there is no need to connect xero.
    $result = $dash_db->getoAuthKeys($user_id, $client_master_id);
    $dash_db->updateLastLogin($user_id);
    $loginDate = date('Y-m-d');
    if ($refresh == 0) {
        if (strlen(trim($sub_category_id1)) <= 0 && strlen(trim($sub_category_id2)) <= 0) {
            $sub_category_id1 = 1000;
            $res_category_count = $utility->getCategoryCountByClientID('category_subcategory', $user_id, $client_master_id);
            if ($res_category_count["error"] == false) {
                $count = $res_category_count["count"];
                if ($count == 2) {
                    $sub_category_id2 = 1000;
                }
            }
        }
        $data = $utility->checkCacheFileExist($client_master_id, $pagename, $loginDate, $sub_category_id1, $sub_category_id2);
        
        if (strlen(trim($data)) > 0) {
            $response = json_decode($data, true);
            $last_updated_datetime = $response["last_updated_time"];
            if ($mtd == 0) {
                $field1 = "consolidatedPNL_yty";
                $field2 = "consolidatedPNL_mtd";
                $field3 = "consolidatedPNL_ytd";
            } else if ($mtd == 1) {
                $field1 = "consolidatedPNL_mtd";
                $field2 = "consolidatedPNL_ytd";
                $field3 = "consolidatedPNL_yty";
            } else if ($mtd == 2) {
                $field1 = "consolidatedPNL_ytd";
                $field2 = "consolidatedPNL_yty";
                $field3 = "consolidatedPNL_mtd";
            }
            
            foreach ($response as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k1 => $v1) {
                        if ($k1 == $field1) {
                            $response[$k]["consolidatedPNL"] = $response[$k][$field1][$field1];
                            unset($response[$k][$field1]);
                        } else if ($k1 == $field2) {
                            unset($response[$k][$field2]);
                        } else if ($k1 == $field3) {
                            unset($response[$k][$field3]);
                        }
                    }
                }
            }
            
            $response = array_values($response);
            $response["error"] = false;
            $response["message"] = RECORD_FOUND;
            $response["last_updated_time"] = $last_updated_datetime;
            $pageResult = array();
            $pageResult = $utility->getFieldByID('client_oauth_token', 'client_id', $user_id, 'generated_date');
            $response["generated_date"] = $pageResult['field_key'];
            $pageResult = null;
            $exist = true;
        } else {
            $loadDataFromLive = true;
            $refresh = 1;
            $exist == false;
        }

        if ($exist == false) {
            $searchItems = array();
            // creating new invite
            if ($is_admin != 1) {
                $searchItems['client_id'] = $actual_user_id;
            } else {
                $searchItems['client_id'] = $user_id;
            }
            $searchItems['client_master_id'] = $client_master_id;
            $searchItems["groupby"] = 1;
            $response = $dash_db->getAllTrackingCategories($searchItems);
        }
    } else {
        $loadDataFromLive = true;
        $refresh = 1;
    }
    
    if ($loadDataFromLive == true) {
        $params = array();
        
        verifyRequiredParams(array('client_id', 'authenticate', 'page_name'));
        
        $authenticate = $app->request->get('authenticate');
        $authorize = $app->request->get('authorize');
        $report_id = $app->request->get('report_id');
        $ajax = $app->request->get('ajax');
        $currencycode = $app->request->get('currencycode');
        
        if (strlen(trim($currencycode)) > 0) {
            $params["CurrencyCode"] = $currencycode;
        }
        if (strlen(trim($sub_category_id1)) > 0) {
            $params["sub_category_id1"] = $sub_category_id1;
        }
        if (strlen(trim($sub_category_id2)) > 0) {
            $params["sub_category_id2"] = $sub_category_id2;
        }
        if (strlen(trim($mtd)) > 0) {
            $params["mtd"] = $mtd;
        }
        
        // here you get all keys
        $ajax = true;
        if (sizeof($result) > 0) {
            if ($result['error'] == true || $result['is_authorized'] == 0) {
                if (isset($ajax) && ($ajax == true)) {
                    $response["error"] = true;
                    $response["message"] = USER_NOT_AUTHORIZED;
                    $response["expiry"] = true;
                } else {
                    if ($authenticate == 1 || $authorize == 1) {
                        /* $response = $db->requestToken($user_id, $result, $pagename);
                        if (isset($response['authurl'])) {
                            $authurl = $response['authurl'];
                            header('Location:' . $authurl);
                            die();
                        } */
                    }
                }
            } else {
                $response = array();
                $expiry = false;
                
                $res_client_master_detail = $utility->getClientMasterDetailByID('client_master_detail', 'client_master_id', $client_master_id);
                if ($res_client_master_detail["error"] == false) {
                    $report_start_date = $res_client_master_detail["report_start_date"];
                    $default_currency = $res_client_master_detail["default_currency"];
                }
                
                $last_three_months = $utility->lastThreeMonths();
                $dates = $utility->getDayTillMonthNew($client_master_id, $report_start_date, $is_admin);
                if (isset($result['XeroOAuth'])) {
                    
                    $XeroOAuth = $result['XeroOAuth'];
                    $XeroOAuth->config['access_token'] = utf8_encode($result['oauth_token']);
                    $XeroOAuth->config['access_token_secret'] = utf8_encode($result['oauth_token_secret']);
                    
                    $currencies = $dash_db->getCurrencies($user_id, $XeroOAuth, $ajax,$client_master_id);
                    if (isset($currencies['expiry'])) {
                        return $currencies;
                    }
                    
                    $organisations = $dash_db->getOrganisation($user_id, $XeroOAuth, $ajax, $client_master_id);
                    if (isset($organisations['expiry'])) {
                        return $organisations;
                    }
                    // echo 'hellow'; exit;
                }
              
                $trackingCategories = $dash_db->getTrackingCategoriesFromXero($user_id, $XeroOAuth, $ajax, $client_master_id);
                $response_flash_report = $dash_db->getFlashReport($user_id, $result, $ajax, $params, $loginDate, $report_start_date, $default_currency, $last_three_months, $dates, $currencies, $organisations, $client_master_id, $trackingCategories);
                if ($response_flash_report["error"] == false) {
                    $response_pl_report = $dash_db->getPNLReport($user_id, $result, $ajax, $params, $loginDate, $is_admin, $report_start_date, $last_three_months, $dates, $currencies, $organisations, $client_master_id, $default_currency);
                    if ($response_pl_report["error"] == false) {
                        if ($pagename == "flashreport") {
                            $response = $response_flash_report;
                        } else {
                            $response = $response_pl_report;
                        }
                        
                        $sub_category_id1 = 1000;
                        // $sub_category_id2 = 0;
                        $res_category_count = $utility->getCategoryCountByClientID('category_subcategory', $user_id, $client_master_id);
                        if ($res_category_count["error"] == false) {
                            $count = $res_category_count["count"];
                            if ($count == 2) {
                                $sub_category_id2 = 1000;
                            }
                        }

                        $metric_result = $dash_db->GenerateMetrics($client_master_id, $user_id, $report_start_date, $sub_category_id1, $sub_category_id2);
                        $delete_flash_report_file = $utility->delete_old_files('flashreport', $client_master_id);
                        $delete_pl_report_file = $utility->delete_old_files('plreport', $client_master_id);
                        $norecord = false;
                    } else {
                        if (isset($response_pl_report['norecord'])) {
                            $norecord = true;
                            $errormessage = $response_pl_report['message'];
                        } else if (isset($response_pl_report['expiry'])) {
                            $expiry = $response_pl_report['expiry'];
                            $is_authenticated = 0;
                            $norecord = false;
                        }
                    }
                } else {
                    if (isset($response_flash_report['norecord'])) {
                        $norecord = true;
                        $errormessage = $response_flash_report['message'];
                    } else if (isset($response_flash_report['expiry'])) {
                        $expiry = $response_flash_report['expiry'];
                        $is_authenticated = 0;
                        $norecord = false;
                    }
                }

                if ($norecord == true) {
                    $response['error'] = true;
                    $response['message'] = $errormessage;
                } else {
                    // $dash_db->XeroRefreshToken($user_id);
                    // $response["is_authenticated"] = $is_authenticated;
                    if ($is_authenticated == 0) {
                        $response['error'] = true;
                        $response['expiry'] = $expiry;
                        $response['message'] = TOKEN_EXPIRED;
                    }

                    // if ($refresh == 1 && $response["error"] == false) {
                    //     header('Location:' . CLIENT_URL . $pagename); die();
                    // }
                }
            }
        } else {
            $response['error'] = true;
            $response['message'] = NO_AUTH_KEYS;
        }
    }
    $dash_db = null;
    $utility = null;
    echoRespnse(200, $response);
});

$app->get('/client/xero/connect', function () use ($app) {
    global $is_client;
    $dash_db = new dashboardReport();
    $utility = new utility();
    $is_admin = 0;
    $response = array();

    $user_id = $app->request->get('client_id');
    $actual_user_id = $user_id;
    $refresh = $app->request->get('refresh');
    $authenticate = $app->request->get('authenticate');
    $pagename = $app->request->get('page_name');
    $client_master_id = $app->request->get('client_master_id');

    $res_is_admin = $utility->getFieldByID('client_master', 'client_id', $user_id, 'is_admin');
    if ($res_is_admin['error'] == false) {
        $is_admin = $res_is_admin['field_key'];
    }

    if ($is_admin != 1) {
        $res_super_admin = $utility->getSuperAdminID($client_master_id);
        $user_id = $res_super_admin['client_id'];
        $refresh = 0;
    }
    $xresponse = $dash_db->XeroRefreshToken($user_id, $client_master_id);
    if (isset($xresponse['authurl'])) {
        $authurl = $xresponse['authurl'];
        header('Location:' . $authurl);
        die();
    }

  /*  $pageResult = $utility->isValidToken('client_oauth_token', 'client_id', $user_id);
    $is_authenticated = ($pageResult["error"] == false) ? $pageResult['isValid'] : 0;
    if ($is_authenticated == 0) {
        $dash_db->XeroRefreshToken($user_id);
    }*/
    // if is_authenticated true, there is no need to connect xero.
    $result = $dash_db->getoAuthKeys($user_id, $client_master_id);
    $dash_db->updateLastLogin($user_id);
    $loginDate = date('Y-m-d');

    // here you get all keys
    if (sizeof($result) > 0) {
        if ($result['error'] == true) {
            // if ($authenticate == 1) {
                $response = $dash_db->XeroRefreshToken($user_id, $client_master_id);
                if (isset($response['authurl'])) {
                    $authurl = $response['authurl'];
                    header('Location:' . $authurl);
                    die();
                }
            // }
        } else {
            $response["is_authenticated"] = $is_authenticated;
            header('Location:' . CLIENT_URL . $pagename);
            die();
        }
    } else {
        $response['error'] = true;
        $response['message'] = NO_AUTH_KEYS;
    }

    $dash_db = null;
    $utility = null;
    echoRespnse(200, $response);
});

$app->put('/client/cushion', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        // check for required params
        verifyRequiredParams(array('cushion_value'));
        global $user_id;
        $cushion_value = $app->request->put('cushion_value');
        $response = array();
        $db = new client();
        $response = $db->updateCushionValue($user_id, $cushion_value);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->delete('/client/cleartoken', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        global $user_id;
        $response = array();
        $db = new client();
        // $response = $db->clearToken($user_id);
        $response["error"] = false;
        $response["message"] = CLIENT_TOKEN_REMOVED;
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/client/users/', 'authenticate', function () use ($app) {
    global $is_client;
    global $user_id;
    if ($is_client == 1) {
        $response = array();
        $searchItems = array();
        $result = array();
        $db = new client();
        $utility = new utility();

        verifyRequiredParams(array('client_master_id'));
        $client_master_id = $app->request->get('client_master_id');
        $is_active = $app->request->get('is_active');

        $searchItems["client_master_id"] = $client_master_id;
        if (strlen(trim($is_active)) > 0) {
            $searchItems["is_active"] = $is_active;
        }

        $result = $db->getAllClientUsers($searchItems);

        $db = null;
        $utility = null;
    }
    echoRespnse(200, $result);
});

// checks if user already authenticaed or not.
$app->get('/client/authenticated', function () use ($app) {
    global $is_client;
    $utility = new utility();
    $dash_db = new dashboardReport();
    $response = array();

    $user_id = $app->request->get('client_id');
    $client_master_id = $app->request->get('client_master_id');
    $res1 = $utility->getFieldByID('client_master', 'client_id', $user_id, 'is_admin');
    $is_admin = $res1["field_key"];

    $actual_user_id = $user_id;

    if ($is_admin != 1) {
        $result = $utility->getFieldByID('client_master', 'client_id', $user_id, 'client_master_id');
        $result = $utility->getSuperAdminID($res1['field_key']);
        $user_id = $result['client_id'];
    }
   
    $utility = null;
    $token = $dash_db->getAuthFromDB($user_id, $client_master_id);
    $authenticated = ($token['error'] == true) ? 0 : 1;
    $response = array('client_id' => $actual_user_id, 'is_authenticated' => $authenticated);
    echoRespnse(200, $response);
});

$app->get('/client/reloadTrackingCategories', function () use ($app) {
    $response = array();
    $dashboardReportDB = new dashboardReport();
    $db1 = new utility();
    $connect = true;

    verifyRequiredParams(array('client_id', 'authenticate', 'page_name'));

    $authenticate = $app->request->get('authenticate');
    $pagename = $app->request->get('page_name');
    $client_id = $app->request->get('client_id');
    $client_master_id = $app->request->get('client_master_id');
    $refresh = $app->request->get('refresh');

    /**While refresh the tracking category we need to do the following
    1. Remove all the budget loaded for that client
    2. Remove all the user access permission who belong to that client. Retain the user in the system
    3. Remove the current tracking category for that client
    4. Pull the tracking category from xero
    5. Pull all the data from xero again.

    Step - 2
    --------
    Remove data from client_category_master.
    Remove data from category_subcategory.
    Remove track_client_sub_category.
     **/
    // step 1
    $dashboardReportDB->hardDeleteBudget($client_master_id);

    // 2
    $dashboardReportDB->removeClientTrackCategories($client_id);
    $dashboardReportDB->removeClientCategories($client_id);

    $pageResult = $db1->isValidToken('client_oauth_token', 'client_id', $client_id);
    $is_authenticated = ($pageResult["error"] == false) ? $pageResult['isValid'] : 0;
    if ($is_authenticated == 0) {
        $dashboardReportDB->XeroRefreshToken($user_id, $client_master_id);
    }

    if ($connect) {
        $params = array();

        $authorize = $app->request->get('authorize');
        $report_id = $app->request->get('report_id');
        $ajax = $app->request->get('ajax');
        $currencycode = $app->request->get('currencycode');

        $ajax = true;

        if (strlen(trim($currencycode)) > 0) {
            $params["CurrencyCode"] = $currencycode;
        }
        if (strlen(trim($sub_category_id1)) > 0) {
            $params["sub_category_id1"] = $sub_category_id1;
        }
        if (strlen(trim($sub_category_id2)) > 0) {
            $params["sub_category_id2"] = $sub_category_id2;
        }
        if (strlen(trim($mtd)) > 0) {
            $params["mtd"] = $mtd;
        }

        // here you get all keys
        $result = $dashboardReportDB->getoAuthKeys($client_id, $client_master_id);
        $loginDate = date('Y-m-d');

        if (sizeof($result) > 0) {
            $res_client_master = $db1->getClientMasterByID('client_master', 'client_id', $client_id);
            if ($res_client_master["error"] == false) {
                $is_admin = $res_client_master["is_admin"];
                $client_master_id = $res_client_master["client_master_id"];
            }

            $res_client_master_detail = $db1->getClientMasterDetailByID('client_master_detail', 'client_master_id', $client_master_id);
            if ($res_client_master_detail["error"] == false) {
                $report_start_date = $res_client_master_detail["report_start_date"];
                $default_currency = $res_client_master_detail["default_currency"];
            }

            $last_three_months = $db1->lastThreeMonths();
            $dates = $db1->getDayTillMonthNew($client_master_id, $report_start_date, $is_admin);

            if (isset($result['XeroOAuth'])) {
                $XeroOAuth = $result['XeroOAuth'];
                $XeroOAuth->config['access_token'] = utf8_encode($result['oauth_token']);
                $XeroOAuth->config['access_token_secret'] = utf8_encode($result['oauth_token_secret']);

                $currencies = $dashboardReportDB->getCurrencies($user_id, $XeroOAuth, $ajax);
                if (isset($currencies['expiry'])) {
                    return $currencies;
                }

                $organisations = $dashboardReportDB->getOrganisation($user_id, $XeroOAuth, $ajax);
                if (isset($organisations['expiry'])) {
                    return $organisations;
                }
            }
            $trackingCategories = $dash_db->getTrackingCategoriesFromXero($user_id, $XeroOAuth, $ajax, $client_master_id);
            $response_flash_report = $dashboardReportDB->getFlashReport($client_id, $result, $ajax, $params, $loginDate, $report_start_date, $default_currency, $last_three_months, $dates, $currencies, $organisations, $client_master_id, $trackingCategories);
            if ($response_flash_report["error"] == false) {
                $response_pl_report = $dashboardReportDB->getPNLReport($client_id, $result, $ajax, $params, $loginDate, $is_admin, $report_start_date, $last_three_months, $dates, $currencies, $organisations, $client_master_id, $default_currency);
                if ($response_pl_report["error"] == false) {
                    if ($pagename == "flashreport") {
                        $response = $response_flash_report;
                    } else {
                        $response = $response_pl_report;
                    }

                    $sub_category_id1 = 1000;
                    // $sub_category_id2 = 0;
                    $res_category_count = $db1->getCategoryCountByClientID('category_subcategory', $client_id, $client_master_id);
                    if ($res_category_count["error"] == false) {
                        $count = $res_category_count["count"];
                        if ($count == 2) {
                            $sub_category_id2 = 1000;
                        }
                    }

                    $metric_result = $dashboardReportDB->GenerateMetrics($client_master_id, $client_id, $report_start_date, $sub_category_id1, $sub_category_id2);
                    $delete_flash_report_file = $db1->delete_old_files('flashreport', $client_master_id);
                    $delete_pl_report_file = $db1->delete_old_files('plreport', $client_master_id);
                    $norecord = false;
                } else {
                    if (isset($response_pl_report['norecord'])) {
                        $norecord = true;
                        $errormessage = $response_pl_report['message'];
                    } else if (isset($response_pl_report['expiry'])) {
                        $expiry = $response_pl_report['expiry'];
                        $is_authenticated = 0;
                        $norecord = false;
                    }
                }
            } else {
                if (isset($response_flash_report['norecord'])) {
                    $norecord = true;
                    $errormessage = $response_flash_report['message'];
                } else if (isset($response_flash_report['expiry'])) {
                    $expiry = $response_flash_report['expiry'];
                    $is_authenticated = 0;
                    $norecord = false;
                }
            }

            if ($norecord == true) {
                $response['error'] = true;
                $response['message'] = $errormessage;
            } else {
                $dashboardReportDB->XeroRefreshToken($client_id, $client_master_id);
                $response["is_authenticated"] = $is_authenticated;
                if ($is_authenticated == 0) {
                    $response['error'] = true;
                    $response['expiry'] = $expiry;
                    $response['message'] = TOKEN_EXPIRED;
                }

                // if ($refresh == 1 && $response["error"] == false) {
                //     header('Location:' . CLIENT_URL . $pagename); die();
                // }
            }
        } else {
            $response['error'] = true;
            $response['message'] = NO_AUTH_KEYS;
        }
    }

    $dashboardReportDB = null;
    $db1 = null;
    echoRespnse(200, $response);
});

//FIXME:
// Refactor this API to connect,
// then get data from Xero on Refresh.
// Check if user is already authenticate.

$app->get('/client/xero/refresh', function () use ($app) {
    global $is_client;
    $dash_db = new dashboardReport();
    $utility = new utility();
    $group = new group();
    $is_admin = 0;
    $response = array();

    $sub_category_id2 = $app->request->get('sub_category_id2');
    $sub_category_id1 = $app->request->get('sub_category_id1');
    $mtd = $app->request->get('mtd');
    $refresh = $app->request->get('refresh');
    $pagename = $app->request->get('page_name');
    $user_id = $app->request->get('client_id');
    $client_master_id = $app->request->get('client_master_id');

    $res_is_admin = $utility->getFieldByID('client_master', 'client_id', $user_id, 'is_admin');
    if ($res_is_admin['error'] == false) {
        $is_admin = $res_is_admin['field_key'];
    }

    if ($is_admin != 1) {
        $res_super_admin = $utility->getSuperAdminID($client_master_id);
        $user_id = $res_super_admin['client_id'];
        $refresh = 0;
    }

    $pageResult = $utility->isValidToken('client_oauth_token', 'client_id', $user_id);
    $is_authenticated = ($pageResult["error"] == false) ? $pageResult['isValid'] : 0;
    if ($is_authenticated === 0) {
        $dash_db->XeroRefreshToken($user_id, $client_master_id);
    }
    // if is_authenticated true, there is no need to connect xero.
    $result = $dash_db->getoAuthKeys($user_id, $client_master_id);
    // $dash_db->updateLastLogin($user_id);
    $loginDate = date('Y-m-d');

    if ($refresh == 1) {
        $params = array();
        $ajax = true;
        verifyRequiredParams(array('client_id', 'authenticate', 'page_name'));
        $authenticate = $app->request->get('authenticate');
        $authorize = $app->request->get('authorize');
        $report_id = $app->request->get('report_id');
        $currencycode = $app->request->get('currencycode');

        if (strlen(trim($currencycode)) > 0) {
            $params["CurrencyCode"] = $currencycode;
        }
        if (strlen(trim($sub_category_id1)) > 0) {
            $params["sub_category_id1"] = $sub_category_id1;
        }
        if (strlen(trim($sub_category_id2)) > 0) {
            $params["sub_category_id2"] = $sub_category_id2;
        }
        if (strlen(trim($mtd)) > 0) {
            $params["mtd"] = $mtd;
        }

        if (sizeof($result) > 0) {
            if ($result['error'] == true) {
                $response = $dash_db->XeroRefreshToken($user_id,$client_master_id);
                if (isset($response['authurl'])) {
                    $authurl = $response['authurl'];
                    header('Location:' . $authurl);
                    die();
                }

                /* if (isset($ajax) && ($ajax == true)) {
                    $response["error"] = true;
                    $response["message"] = USER_NOT_AUTHORIZED;
                    $response["expiry"] = true;
                } else {
                    if ((1 == $authenticate) || (1 == $authorize)) {
                        $response = $dash_db->XeroRefreshToken($user_id);
                        if (isset($response['authurl'])) {
                            $authurl = $response['authurl'];
                            header('Location:' . $authurl);
                            die();
                        }
                    }
                } */
            } else {
                $expiry = false;
                
                $res_client_master_detail = $utility->getClientMasterDetailByID('client_master_detail', 'client_master_id', $client_master_id);
                if ($res_client_master_detail["error"] == false) {
                    $report_start_date = $res_client_master_detail["report_start_date"];
                    $default_currency = $res_client_master_detail["default_currency"];
                }

                $last_three_months = $utility->lastThreeMonths();
                $dates = $utility->getDayTillMonthNew($client_master_id, $report_start_date, $is_admin);

                if (isset($result['XeroOAuth'])) {
                    $XeroOAuth = $result['XeroOAuth'];
                    $XeroOAuth->config['access_token'] = utf8_encode($result['oauth_token']);
                    $XeroOAuth->config['access_token_secret'] = utf8_encode($result['oauth_token_secret']);

                    $currencies = $dash_db->getCurrencies($user_id, $XeroOAuth, $ajax, $client_master_id);
                    if (isset($currencies['expiry'])) {
                        return $currencies;
                    }

                    $organisations = $dash_db->getOrganisation($user_id, $XeroOAuth, $ajax, $client_master_id);
                    if (isset($organisations['expiry'])) {
                        return $organisations;
                    }
                }
                $dash_db->XeroRefreshToken($user_id, $client_master_id);
                $trackingCategories = $dash_db->getTrackingCategoriesFromXero($user_id, $XeroOAuth, $ajax, $client_master_id);
                // print_r();
                // exit;
                $response_flash_report = $dash_db->getFlashReport($user_id, $result, $ajax, $params, $loginDate, $report_start_date, $default_currency, $last_three_months, $dates, $currencies, $organisations, $client_master_id, $trackingCategories);
                // $response_flash_report["error"] = false;
                if ($response_flash_report["error"] == false) {
                    $response_pl_report = $dash_db->getPNLReport($user_id, $result, $ajax, $params, $loginDate, $is_admin, $report_start_date, $last_three_months, $dates, $currencies, $organisations, $client_master_id, $default_currency);

                    if ($response_pl_report["error"] == false) {
                        if ($pagename == "flashreport") {
                            $response = $response_flash_report;
                        } else {
                            $response = $response_pl_report;
                        }

                        $sub_category_id1 = 1000;
                        $res_category_count = $utility->getCategoryCountByClientID('category_subcategory', $user_id, $client_master_id);
                        if ($res_category_count["error"] == false) {
                            $count = $res_category_count["count"];
                            if ($count == 2) {
                                $sub_category_id2 = 1000;
                            }
                        }

                        $metric_result = $dash_db->GenerateMetrics($client_master_id, $user_id, $report_start_date, $sub_category_id1, $sub_category_id2);
                        $delete_flash_report_file = $utility->delete_old_files('flashreport', $client_master_id);
                        $delete_pl_report_file = $utility->delete_old_files('plreport', $client_master_id);
                    } else {
                        $is_authenticated = 0;
                        if (isset($response_pl_report['expiry'])) {
                            $expiry = $response_pl_report['expiry'];
                        }
                    }
                    /* read plreport json file for group category eliminate process */
                    $dir = BASE_PATH . "/plreport/" . $client_master_id . "/";
                    if (file_exists($dir)) {
                        mkdir($dir, 0777, true);
                        $filePath = $dir . $client_master_id . "_plreport_" . $loginDate . "_" . 1000 . ".json";
                        $jsondata = json_decode(file_get_contents($filePath), true);
                        $data = $group->storeJsonData($jsondata, $client_master_id);
                    }
                } else {
                    $is_authenticated = 0;
                    if (isset($response_flash_report['expiry'])) {
                        $expiry = $response_flash_report['expiry'];
                    }
                }
                if ($is_authenticated == 0) {
                    $dash_db->XeroRefreshToken($user_id, $client_master_id);
                    $response["is_authenticated"] = $is_authenticated;
                    $response['error'] = false;
                    // $response['expiry'] = $expiry;
                    // $response['message'] = TOKEN_EXPIRED;
                }

                // if ($refresh == 1 && $response["error"] == false) {
                //     header('Location:' . CLIENT_URL . $pagename);
                //     die();
                // }
            }
        } else {
            $response['error'] = true;
            $response['message'] = NO_AUTH_KEYS;
        }
    }
    $dash_db = null;
    $utility = null;
    
    echoRespnse(200, $response);
});

$app->post('/client/xero/disconnect', 'authenticate', function () use ($app) {
    $client_id = $app->request->post('client_id');
    $client_master_id = $app->request->post('client_master_id');
    if ($client_id > 0) {
        $response = array();
        $db = new client();
        $response = $db->clearToken($client_id, $client_master_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->post('/client/user', 'authenticate', function () use ($app) {
    $response = array();
    global $is_client;

    verifyRequiredParams(array('client_master_id', 'email', 'name', 'mobile', 'password'));
    $email = $app->request->post('email');
    $user_name = $app->request->post('name');
    $mobile = $app->request->post('mobile');
    $password = $app->request->post('password');
    $client_master_id = $app->request->post('client_master_id');
    $application_ids = $app->request->post('application_ids');
    $category_sub_ids = $app->request->post('category_sub_ids');
    $report_ids = $app->request->post('report_ids');
    $is_admin = $app->request->post('is_admin');
    $selectedClients = $app->request->post('selectedClients');
    $error = false;

    if (strlen($password) < 8) {
        $response["error"] = true;
        $response["message"] = PASSWORD_MINIMUM_LENGTH;
        $error = true;
    }

    if (sizeof($report_ids) > 0) {
        $report_ids = array_unique($report_ids);
    }

    if (sizeof($category_sub_ids) > 0) {
        $category_sub_ids = array_unique($category_sub_ids);
    }

    if ($error == false) {
        $db = new client();
        $response = $db->createClientUser($user_name, $mobile, $email, $is_admin, $password, $selectedClients, null, $client_master_id);
        $response1 = $db->updateClientUserApplicationByAdmin($response['client_id'], $client_master_id, $application_ids, $category_sub_ids, $report_ids, true);
        $db = null;
    }

    // echo json response
    echoRespnse(200, $response);
});

$app->put('/client/user/:id', 'authenticate', function ($client_id) use ($app) {
    global $is_client;

    // check for required params
    verifyRequiredParams(array('name', 'mobile', 'email'));
    $name = $app->request->put('name');
    $mobile = $app->request->put('mobile');
    $email = $app->request->put('email');
    $application_ids = $app->request->put('application_ids');
    $category_sub_ids = $app->request->put('category_sub_ids');
    $report_ids = $app->request->put('report_ids');
    $is_admin = $app->request->put('is_admin');
    $client_master_id = $app->request->put('client_master_id');
    $selectedClients = $app->request->put('selectedClients');
    $deletedClients = $app->request->put('clientsToRemove');

    if (sizeof($report_ids) > 0) {
        $report_ids = array_unique($report_ids);
    }

    if (sizeof($category_sub_ids) > 0) {
        $category_sub_ids = array_unique($category_sub_ids);
    }

    $db = new client();
    $response = array();
    $response1 = array();

    $response = $db->updateClientUserByAdmin($client_id, $name, $mobile, $email, $is_admin, $client_master_id, $selectedClients, $deletedClients, $selectedGroups, $groupsToRemove);
    $response1 = $db->updateClientUserApplicationByAdmin($client_id, $client_master_id, $application_ids, $category_sub_ids, $report_ids, true);

    $db = null;
    echoRespnse(200, $response);
});

$app->put('/client/user/access/:id', 'authenticate', function ($client_id) use ($app) {
    global $is_client;

    verifyRequiredParams(array('is_active'));
    $is_active = $app->request->put('is_active');
    $db = new client();
    $response = array();
    $response = $db->accessClientUser($client_id, $is_active);

    $db = null;
    echoRespnse(200, $response);
});

$app->get('/client/dashboard/', 'authenticate', function () use ($app) {
    $response = array();
    $db = new dashboardReport();
    $utility = new utility();
    verifyRequiredParams(array('client_master_id', 'client_id'));
    $client_master_id = $app->request->get('client_master_id');
    $client_id = $app->request->get('client_id');
    $is_group = $app->request->get('is_group');
    $sub_category_id1 = 1000;

    $res_client_master_detail = $utility->getClientMasterDetailByID('client_master_detail', 'client_master_id', $client_master_id);
    if ($res_client_master_detail["error"] == false) {
        $report_start_date = $res_client_master_detail["report_start_date"];
    }
    $res_category_count = $utility->getCategoryCountByClientID('category_subcategory', $client_id, $client_master_id);
    if ($res_category_count["error"] == false) {
        $count = $res_category_count["count"];
        if ($count == 2) {
            $sub_category_id2 = 1000;
        } else {
            $sub_category_id2 = 0;
        }
    }

    $metrics_result = $utility->isMetricsUpdated($client_id);
    if ($metrics_result["error"] == false) {
        if ($metrics_result["isMetricsUpdated"] == false) {
            $result = $db->GenerateMetrics($client_master_id, $client_id, $report_start_date, $sub_category_id1, $sub_category_id2);
        }
    }

    $resultdb = $db->getDashboardBudgetDetails($client_master_id, $sub_category_id1, $sub_category_id2, $client_id, $report_start_date);

    $db = null;
    $utility = null;
    echoRespnse(200, $resultdb);
});

$app->get('/client/xero/authenticatedLive', function () use ($app) {
    global $is_client;
    $dash_db = new dashboardReport();
    $utility = new utility();
    $is_admin = 0;
    $response = array();

    $user_id = $app->request->get('client_id');
    $client_master_id = $app->request->get('client_master_id');
    $actual_user_id = $user_id;

    $res_is_admin = $utility->getFieldByID('client_master', 'client_id', $user_id, 'is_admin');
    if ($res_is_admin["error"] == false) {
        $is_admin = $res_is_admin["field_key"];
    }

    if ($is_admin != 1) {
        $res_super_admin = $utility->getSuperAdminID($client_master_id);
        $user_id = $res_super_admin['client_id'];
        $refresh = 0;
    }

    $token = $dash_db->getAuthFromDB($user_id, $client_master_id);
    $authenticated = ($token['error']===true) ? 0 : 1;
    $response = array('client_id' => $actual_user_id, 'is_authenticated' => $authenticated);
    $utility = null;
    $dash_db = null;
    echoRespnse(200, $response);
});

/*
 * get BizFile path
 * url - /client
 * method - POST
 * params - client_master_id
 */
$app->get('/client/getBizFile/', 'authenticate', function () use ($app) {
    $client = new client();
    $utility = new utility();
    global $is_client;

    if ($is_client == 1) {
        global $user_id;
        verifyRequiredParams(array('client_master_id'));
        $client_master_id = $app->request->get('client_master_id');
        $response = $client->bizfileClient($client_master_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $client = null;
    echoRespnse(200, $response);
});

/* Flash Report Detailed to PDF to download from webpage based on report_pdf_to_client */
$app->get('/client/flash_report_detailed_pdf/', 'authenticate', function () use ($app) {
    $db = new dashboardReport();
    $utility = new utility();
    verifyRequiredParams(array('client_id', 'category1', 'category2'));
    $client_id = $app->request->get('client_id');
    $client_master_id = $app->request->get('client_master_id');
    $category1 = $app->request->get('category1');
    $category2 = $app->request->get('category2');
    $category_name = $db->getCategoryNameById($category1, $category2, $client_master_id);

    $resultOutput = array();
    $resultOutput['outputfile'] = '';

    $checkPdfExist = $db->getPdfFileExists($client_master_id, 'flash_report', $category1, $category2);

    if ($checkPdfExist['record'] == 'yes') {
        $file_name = $checkPdfExist['file_name'];
        $resultOutput['error'] = false;
        $resultOutput['outputfile'] = $file_name;
    } else {

        $result = $db->getClientPdfDetails($client_id, "flash_report", $category1, $category2, $client_master_id, "");
        if (count($result) > 0) {
            require_once '../include/tcpdf/tcpdf.php';
            $company_name = $result['company_name'];
            $name = $result['name'];
            $email = $result['email'];
            $metrics_path = $result['metrics_path'];
            $report_path = $result['report_path'];
            $cushion_value = ($result['cushion_value'] == null) ? 10000 : $result['cushion_value'];
            $metrics_obj = json_decode(file_get_contents($metrics_path), true);
            $total_cash_on_hand = $metrics_obj['accountsSummarytotal'];
            $agedReceivablestotal = $metrics_obj['agedReceivablestotal'];
            $agedPayablestotal = $metrics_obj['agedPayablestotal'];
            $estimatedPayrolltotal = $metrics_obj['estimatedPayrolltotal'];
            $sub_total = round($total_cash_on_hand) + round($agedReceivablestotal);
            $deduct_total = round($agedPayablestotal) + round($estimatedPayrolltotal);
            $partial_total = $sub_total - $deduct_total;
            $total = $partial_total - $cushion_value;
            $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetMargins(10, 10, 10);
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->SetFont('helvetica', '', 8);
            $pdf->startTransaction();
            $pdf->AddPage('L', 'A4');

            $report_obj = json_decode(file_get_contents($report_path), true);
            $last_updated_on = date("d M Y", strtotime($report_obj['last_updated_time']));
            $message = '<table style="width:100%">
                                <tr>
                                    <td style="line-height:30px;border-bottom:2px solid #009fc5;text-left;font-size: 16px;font-weight: bold;font-family: Arial, sans-serif; float: left;">
                                        Flash Report
                                    </td>
                                    <td style="text-align: right;line-height:30px;border-bottom:2px solid #009fc5;">
                                        <img src="images/logo.jpg" alt="Smiley face" style="width:90px;"/>
                                    </td>
                                </tr>
                                <tr><td colspan="2"></td></tr>
                                <tr><td colspan="2" style="font-size: 14px;font-weight: bold;padding-left:35%;text-align:center;text-color: #009fc5">' . $company_name . '</td></tr>
                                <tr>
                                <td colspan="2" style="font-size: 12px;  font-weight: 700;text-align:center;">
                                    Reports Data last updated on '.$last_updated_on.'
                                </td>
                            </tr>
                        </table>';
            if(sizeof($report_obj[1]['accountsSummary']) > 0){
                $message .= $db->getReportValues($report_path, 1, "Cash-On-Hand", $company_name,  $pdf, $message);
            }
            if(sizeof($report_obj[3]['agedReceivables']) > 0){
                $message .= $db->getReportValues($report_path, 3, "Account-Receivable", $company_name, $pdf);
            }
            if(sizeof($report_obj[2]['agedPayables']) > 0){
                $message .= $db->getReportValues($report_path, 2, "Account-Payable", $company_name, $pdf);
            }
            if(sizeof($report_obj['4']['estimatedPayroll']) > 0){
                $message .= $db->getReportValues($report_path, 4, "Estimated-PayRoll", $company_name, $pdf);
            }

            $message1 .= '<tr><td></td></tr><tr><tr><td></td></tr><tr><tr><td></td></tr>
                            </table>
                                <div>
                                    <table style="width:100%;text-align: left; background-color: #CCC; margin-top: 20px;
                                    height: 20px; line-height: 20px; font-size: 10px; border-radius: 3px; ">
                                        <tr>
                                            <td>
                                                Cushion ($ Amount Input by Client)
                                                <table style="text-align: left; background-color: #CCC; margin-top: 20px;
                                                    height: 20px; line-height: 20px; font-size: 10px; border-radius: 3px;"><tr><td style="padding-left:100%; text-align: right;font-size: 10px;">$' . number_format($cushion_value) . '
                                                </td></tr></table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>

                                <table style="width:100%;text-align: left; color: #fff; background-color: #63b145; margin-top: 20px;
                                height: 20px; line-height: 20px; font-size: 10px; border-radius: 3px; ">
                                <tr>
                                    <td>
                                    Total Estimated Short-Term Working Capital
                                        <table style="text-align: left; color: #fff; background-color: #63b145; margin-top: 20px; height: 20px; line-height: 20px; font-size: 10px; border-radius: 3px;">
                                            <tr>';
            if ($total < 0) {
                $message1 .=  '<td style="padding-left:100%; text-align: right;font-size: 10px; font-weigth: bold;">'. $db->format_number($total, 0) .'</td>';
            }
            else {
                $message1 .=  '<td style="padding-left:100%; text-align: right;font-size: 10px;" >'. $db->format_number($total, 1) .'</td>';
            }

            $message1 .= '</tr></table></td></tr></table></div>';

            $pdf->writeHTML($message1, true, false, false, false, '');

            $curDate = date('Y-m-d h:i:s');
            if (!file_exists(BASE_PATH . '/report')) {
                mkdir(BASE_PATH . '/report', 0777, true);
            }
            if (!file_exists(BASE_PATH . '/report/' . $client_master_id)) {
                mkdir(BASE_PATH . '/report/' . $client_master_id, 0777, true);
            }
            $company_name = str_replace(" ", "_", $company_name);
            //$pdf->writeHTML($message, true, false, false, false, '');
            if ($category_name['sub_category_name1'] != "" || $category_name['sub_category_name1'] != null) {
                $pdf_name = $company_name . $category_name['sub_category_name1'] . '_' . $category_name['sub_category_name1'] . '_flashreport.pdf';
            } else {
                $pdf_name = $company_name . $category_name['sub_category_name1'] . '_flashreport.pdf';

            }

            $path = BASE_PATH . '/report/' . $client_master_id . '/' . $pdf_name;
            $fileatt = $pdf->Output($path, 'F');
            $insertResult = $db->insertUpdatePdfGenerateDate($client_master_id, $category1, $category2, 'flash_report');
            if ($insertResult['error'] == false) {
                $resultOutput['error'] = false;
                $resultOutput['outputfile'] = "api/include/report/" . $client_master_id . '/' . $pdf_name;
            } else {
                $resultOutput['error'] = true;
            }
        } else {
            echo "No Record Found.";
        }
    }

    echoRespnse(200, $resultOutput);

    $db = null;
});

$app->get('/client/pl_report_detailed_pdf/', 'authenticate', function () use ($app) {
    $db = new dashboardReport();
    $utility = new utility();
    $curDate = date('Y-m-d H:i:s');
    verifyRequiredParams(array('client_id', 'category1', 'category2'));
    $client_id = $app->request->get('client_id');
    $client_master_id = $app->request->get('client_master_id');
    $category1 = $app->request->get('category1');
    $category2 = $app->request->get('category2');
    $section = $app->request->get('section');
    $sorting = $app->request->get('sorting');
    $is_group = $app->request->get('is_group');

    $category_name = $db->getCategoryNameById($category1, $category2, $client_master_id);

    $checkPdfExist = $db->getPdfFileExists($client_master_id, 'pl_report', $category1, $category2);
    if ($checkPdfExist['record'] == 'yes') {
        $file_name = $checkPdfExist['file_name'];
        $resultOutput['error'] = false;
        $resultOutput['outputfile'] = $file_name;
    } else {
        $result = $db->getClientPdfDetails($client_id, "pl_report", $category1, $category2, $client_master_id, $is_group);
        $datestrendpnl = $utility->getDayTillMonthPdfReport("TrendPNL");
        $datesforbalancesheet = $utility->getDayTillMonthPdfReport("BalanceSheet");
        $datesforCashFlow = $utility->getDayTillMonthPdfReport("CashFlow");

        if (count($result) > 0) {
            require_once '../include/tcpdf/tcpdf.php';
            $company_name = $result['company_name'];
            $email = $result['email'];
            $report_path = $result['report_path'];
            $cushion_value = $result['cushion_value'];
            $report_start_date = $result['report_start_date'];
            $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetMargins(10, 10, 10);
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->SetFont('helvetica', '', 8);
            $pdf->startTransaction();
            $current_date = date("dS F Y");
            $footer_message = "Financial Report" . ' | ' . $company_name . ' | ' . $current_date;
            $current_date = date("dS F Y");

            //Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=0, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M')

            $message = '<table style="width:100%">
                            <tr>
                                <td style="line-height:30px;border-bottom:2px solid #009fc5;text-left;font-size: 14px;font-weight: bold;font-family: Arial, sans-serif; float: left;">
                                    Financial Report
                                </td>
                                <td style="text-align: right;line-height:30px;border-bottom:2px solid #009fc5;">
                                    <img src="images/logo.jpg" alt="Smiley face" style="width:90px;"/>
                                </td>
                            </tr>
                            <tr><td colspan="2"></td></tr>
                            <tr><td colspan="2" style="font-size: 10px;font-weight: bold;padding-left:35%;text-align:center;text-color: #009fc5">' . $company_name . '</td></tr>
                        </table>';

            switch ($section) {
                case 'YTD':
                    $pdf->AddPage('P', 'A4');
                    $message .= '<table style="width:100%">
                                    <tr><td style="font-weight:bold;font-size:9px;padding-left:40%;text-align:center;">Consolidated P&L YTD</td></tr>
                                </table>';

                    $message .= $db->getPLReportValues($report_path, 2, $report_start_date, "consolidatedPNL_ytd", $datestrendpnl, $pdf, $message);
                    break;
                case 'YTY':
                    $pdf->AddPage('P', 'A4');
                    $message .= '<table style="width:100%">
                                    <tr><td style="font-weight:bold;font-size:9px;padding-left:40%;text-align:center;">Consolidated P&L YTY</td></tr>
                                </table>';
                    $message .= $db->getPLReportValues($report_path, 2, $report_start_date, "consolidatedPNL_yty", $datestrendpnl, $pdf, $message);
                    break;
                case 'MTD':
                    $pdf->AddPage('P', 'A4');
                    $message .= '<table style="width:100%">
                                    <tr><td style="font-weight:bold;font-size:9px;padding-left:40%;text-align:center;">Consolidated P&L MTD</td></tr>
                                </table>';
                    $message .= $db->getPLReportValues($report_path, 2, $report_start_date, "consolidatedPNL_mtd", $datestrendpnl, $pdf, $message);
                    break;
                case 'Monthly':
                    $pdf->SetMargins(5, 10, 5);
                    $pdf->AddPage('L', 'A4');
                    $message .= '<table style="width:100%">
                                    <tr><td style="font-weight:bold;font-size:10pxpadding-left:40%;text-align:center;">Trend P&L Report - Monthly</td></tr>
                                </table>';
                    $message .= $db->getPLReportValues($report_path, 3, $report_start_date, "trendPNL", $datestrendpnl, $pdf, $message);
                    break;
                case 'Quartely':
                    $pdf->AddPage('P', 'A4');
                    $message .= '<table style="width:100%">
                                    <tr><td style="font-weight:bold;font-size:10pxpadding-left:40%;text-align:center;">Trend P&L Report - Quartely</td></tr>
                                </table>';
                    $message .= $db->getPLReportValues($report_path, 4, $report_start_date, "trendPNL", $datestrendpnl, $pdf, $message);
                    break;
                case 'BalanceSheet':
                    $pdf->SetMargins(5, 10, 5);
                    $pdf->AddPage('L', 'A4');
                    $message .= '<table style="width:100%">
                            <tr><td style="font-weight:bold;font-size:8pxpadding-left:40%;text-align:center;">Balance Sheet Report</td></tr>
                                </table>';
                    $message .= $db->getPLReportValues($report_path, 1, $report_start_date, "Balance Sheet", $datesforbalancesheet, $pdf, $message);
                    break;
                case 'CashFlow':
                    $pdf->AddPage('L', 'A4');
                    $message .= '<table style="width:100%">
                                <tr><td style="font-weight:bold;font-size:9px;text-align:center;">Cash Flow Statement</td></tr>
                            </table>';
                    $message .= $db->getPLReportValues($report_path, 5, $report_start_date, "cashFlowStatement", $datesforCashFlow, $pdf, $message);
                    break;
                case 'Income':
                    $pdf->AddPage('L', 'A4');
                    $message .= '<table style="width:100%">
                                    <tr><td style="font-weight:bold;font-size:9px;text-align:center;">Amount Invoiced by Contact</td></tr>
                                </table>';
                    $message .= $db->getPLReportValues($report_path, 6, $report_start_date, "contactByName", $datestrendpnl, $pdf, $message, $sorting);
                    break;
            }

            if (!file_exists(BASE_PATH . '/report')) {
                mkdir(BASE_PATH . '/report', 0777, true);
            }
            if (!file_exists(BASE_PATH . '/report/' . $client_master_id)) {
                mkdir(BASE_PATH . '/report/' . $client_master_id, 0777, true);
            }

            $company_name = str_replace(" ", "_", $company_name);
            if ($category2 != '' && $category2 != null) {
                $pdf_name = $company_name . '_plreport_' . $category_name['sub_category_name1'] . '_' . $category_name['sub_category_name1'] . '_' . $section . '.pdf';
            } else {
                $pdf_name = $company_name . '_plreport_' . $category_name['sub_category_name1'] . '_' . $section . '.pdf';
                $category2 = 0;
            }
            $path = BASE_PATH . '/report/' . $client_master_id . '/' . $pdf_name;
            $insertResult = $db->insertUpdatePdfGenerateDate($client_master_id, $category1, $category2, 'pl_report');
            if ($insertResult['error'] == false) {
                $resultOutput['error'] = false;
                $downloadpath = 'api/include/report/' . $client_master_id . '/' . $pdf_name;
                $resultOutput['outputfile'] = $downloadpath;
            }
            $fileatt = $pdf->Output($path, 'F');
        } else {
            echo "No Record Found.";
        }
    }
    echoRespnse(200, $resultOutput);
    $db = null;
    $utility = null;
});

$app->get('/client/pl_report_detailed_xls', function () use ($app) {

    $db = new dashboardReport();
    $utility = new utility();
    $curDate = date('Y-m-d h:i:s');
    $client_id = $app->request->get('client_id');
    $client_master_id = $app->request->get('client_master_id');
    $category1 = $app->request->get('category1');
    $category2 = $app->request->get('category2');
    $section = $app->request->get('section');
    $sorting = $app->request->get('sorting');
    $is_group = $app->request->get('is_group');

    $result = $db->getClientPdfDetails($client_id, "pl_report", $category1, $category2, $client_master_id, $is_group);
    $report_path = $result['report_path'];
    $report_start_date = $result['report_start_date'];
    $company_name = $result['company_name'];
    $datestrendpnl = $utility->getDayTillMonthPdfReport("TrendPNL");
    $datesforbalancesheet = $utility->getDayTillMonthPdfReport("BalanceSheet");
    $datesforCashFlow = $utility->getDayTillMonthPdfReport("CashFlow");
    $objPHPExcel = new Spreadsheet();
    $objPHPExcel->getActiveSheet()->getStyle("A1:G2")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setSize(11);
    $objPHPExcel->getActiveSheet()->getStyle("A2:G2")->getFont()->setSize(10);
    $logo = 'images/logo.jpg';

    $drawing = new PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
    $drawing->setPath($logo);
    $drawing->setWidth(80);
    $drawing->setHeight(30);

    $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
    $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
    $objPHPExcel->getActiveSheet()->getRowDimension(4)->setRowHeight(15);

    $objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setSize(13);
    $objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setSize(11);
    $objPHPExcel->getActiveSheet()->getStyle('C4')->getFont()->setSize(11);
    $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

    switch ($section) {
        case "YTY":
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(42);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->setCellValue('A2', "Consolidated P&L YTY");
            $objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $company_name);
            $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            // $drawing->setCoordinates('E1');
            // $drawing->setWorksheet($objPHPExcel->getActiveSheet());
            $message = $db->getPLReportValuesxls($report_path, 2, $report_start_date, "consolidatedPNL_yty", $datestrendpnl, $objPHPExcel);
            break;

        case "YTD":
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(42);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->setCellValue('A2', "Consolidated P&L YTD");
            $objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $company_name);
            $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            // $drawing->setCoordinates('E1');
            // $drawing->setWorksheet($objPHPExcel->getActiveSheet());
            $message = $db->getPLReportValuesxls($report_path, 2, $report_start_date, "consolidatedPNL_ytd", $datestrendpnl, $objPHPExcel);
            break;

        case "MTD":
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(42);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->setCellValue('A2', "Consolidated P&L MTD");
            $objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $company_name);
            $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            // $drawing->setCoordinates('E1');
            // $drawing->setWorksheet($objPHPExcel->getActiveSheet());
            $message = $db->getPLReportValuesxls($report_path, 2, $report_start_date, "consolidatedPNL_mtd", $datestrendpnl, $objPHPExcel);
            break;

        case "Monthly":
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(42);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(25);
            $objPHPExcel->getActiveSheet()->setCellValue('A2', "Trend P&L Report - Monthly");
            $objPHPExcel->getActiveSheet()->mergeCells('A2:M2');
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $company_name);
            $objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            // $drawing->setCoordinates('G1');
            // $drawing->setWorksheet($objPHPExcel->getActiveSheet());
            $message = $db->getPLReportValuesxls($report_path, 3, $report_start_date, "trendPNL", $datestrendpnl, $objPHPExcel);

            break;

        case "Quartely":
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(42);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->setCellValue('A2', "Trend P&L Report - Quartely");
            $objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $company_name);
            $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            // $drawing->setCoordinates('G1');
            // $drawing->setWorksheet($objPHPExcel->getActiveSheet());
            $message = $db->getPLReportValuesxls($report_path, 4, $report_start_date, "trendPNL", $datestrendpnl, $objPHPExcel);

            break;

        case "BalanceSheet":
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
            $objPHPExcel->getActiveSheet()->setCellValue('A2', "Balance Sheet");
            $objPHPExcel->getActiveSheet()->mergeCells('A2:L2');
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $company_name);
            $objPHPExcel->getActiveSheet()->mergeCells('A1:L1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            // $drawing->setCoordinates('G1');
            // $drawing->setWorksheet($objPHPExcel->getActiveSheet());
            $message = $db->getPLReportValuesxls($report_path, 1, $report_start_date, "Balance Sheet", $datesforbalancesheet, $objPHPExcel);

            break;

        case "CashFlow":
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(38);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(25);
            $objPHPExcel->getActiveSheet()->setCellValue('A2', "Cash Flow Statement");
            $objPHPExcel->getActiveSheet()->mergeCells('A2:K2');
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $company_name);
            $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            // $drawing->setCoordinates('G1');
            // $drawing->setWorksheet($objPHPExcel->getActiveSheet());
            $message = $db->getPLReportValuesxls($report_path, 5, $report_start_date, "CashFlowStatement", $datesforCashFlow, $objPHPExcel);

            break;

        case "Income":
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(42);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
            $objPHPExcel->getActiveSheet()->setCellValue('A2', "Amount By Contact");
            $objPHPExcel->getActiveSheet()->mergeCells('A2:N2');
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $company_name);
            $objPHPExcel->getActiveSheet()->mergeCells('A1:N1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            // $drawing->setCoordinates('G1');
            // $drawing->setWorksheet($objPHPExcel->getActiveSheet());
            $message = $db->getPLReportValuesxls($report_path, 6, $report_start_date, "contactByName", $datestrendpnl, $objPHPExcel, $sorting);
            break;
    }

    if (!file_exists(BASE_PATH . '/report')) {
        mkdir(BASE_PATH . '/report', 0777, true);
    }
    if (!file_exists(BASE_PATH . '/report/' . $client_master_id)) {
        mkdir(BASE_PATH . '/report/' . $client_master_id, 0777, true);
    }

    $file = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
    $excel_name = $company_name . '_' . $section . '.xlsx';
    $path = BASE_PATH . '/report/' . $client_master_id . '/' . $excel_name;
    $file->save($path);
    $downloadpath = 'api/include/report/' . $client_master_id . '/' . $excel_name;
    $resultOutput['error'] = false;
    $resultOutput['outputfile'] = $downloadpath;
    echoRespnse(200, $resultOutput);
});

$app->get('/client/flash_report_detailed_xls', function () use ($app) {
    $db = new dashboardReport();
    $utility = new utility();
    verifyRequiredParams(array('client_id', 'category1', 'category2'));
    $client_id = $app->request->get('client_id');
    $client_master_id = $app->request->get('client_master_id');
    $category1 = $app->request->get('category1');
    $category2 = $app->request->get('category2');
    $category_name = $db->getCategoryNameById($category1, $category2, $client_master_id);
    $resultOutput = array();
    $resultOutput['outputfile'] = '';
    $checkPdfExist = $db->getPdfFileExists($client_master_id, 'flash_report', $category1, $category2);

    if ($checkPdfExist['record'] == 'yes') {
        $file_name = $checkPdfExist['file_name'];
        $resultOutput['error'] = false;
        $resultOutput['outputfile'] = $file_name;
    } else {

        $result = $db->getClientPdfDetails($client_id, "flash_report", $category1, $category2, $client_master_id, "");
        if (count($result) > 0) {
            require_once '../include/tcpdf/tcpdf.php';
            $company_name = $result['company_name'];
            $name = $result['name'];
            $email = $result['email'];
            $metrics_path = $result['metrics_path'];
            $report_path = $result['report_path'];
            $cushion_value = ($result['cushion_value'] == null) ? 10000 : $result['cushion_value'];
            $metrics_obj = json_decode(file_get_contents($metrics_path), true);
            $total_cash_on_hand = $metrics_obj['accountsSummarytotal'];
            $agedReceivablestotal = $metrics_obj['agedReceivablestotal'];
            $agedPayablestotal = $metrics_obj['agedPayablestotal'];
            $estimatedPayrolltotal = $metrics_obj['estimatedPayrolltotal'];
            $sub_total = round($total_cash_on_hand) + round($agedReceivablestotal);
            $deduct_total = round($agedPayablestotal) + round($estimatedPayrolltotal);
            $partial_total = $sub_total - $deduct_total;
            $total = $partial_total - $cushion_value;

            $report_obj = json_decode(file_get_contents($report_path), true);
            $last_updated_on = date("d M Y", strtotime($report_obj['last_updated_time']));

            $objPHPExcel = new Spreadsheet();
            $objPHPExcel->getActiveSheet()->getStyle("A1:G4")->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Flash Report');

            $logo = 'images/logo.jpg';
            $drawing = new PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setPath($logo);
            $drawing->setWidth(80);
            $drawing->setHeight(30);

            $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
            $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);

            $objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setSize(11);
            $objPHPExcel->getActiveSheet()->getStyle('C4')->getFont()->setSize(11);

            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(11);
            $objPHPExcel->getActiveSheet()->setCellValue('A2', $company_name);
            $objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A3:E3')->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->getStyle('A3:E3')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', "Reports Data last updated on ".$last_updated_on);
            $objPHPExcel->getActiveSheet()->mergeCells('A3:G3');
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->setCellValue('A6', 'All values are represented in SGD');
            $objPHPExcel->getActiveSheet()->getRowDimension(6)->setRowHeight(15);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);

            $message = "";
            $message1 = "";
            $message2 = "";
            $row = 8;
            if(sizeof($report_obj[1]['accountsSummary']) > 0){
                $message .= $db->getReportValuesxls($report_path, 1, "Cash-On-Hand", $company_name,  $objPHPExcel, $row);
                $row = $message + 2;
            }
            if(sizeof($report_obj[3]['agedReceivables']) > 0){
                $message1 .= $db->getReportValuesxls($report_path, 3, "Account-Receivable", $company_name, $objPHPExcel, $row);
                $row = $message1 + 2;
            }
            if(sizeof($report_obj[2]['agedPayables']) > 0){
                $message2 .= $db->getReportValuesxls($report_path, 2, "Account-Payable", $company_name, $objPHPExcel, $row);
                $row = $message2 + 2;
            }
            if(sizeof($report_obj['4']['estimatedPayroll']) > 0){
                $message3 .= $db->getReportValuesxls($report_path, 4, "Estimated-PayRoll", $company_name, $objPHPExcel, $row);
                $row = $message3;
            }

            $row4 = $row;
            $row4++;
            $row5 = $row4 + 2;
            for($col =1; $col <= 7; ++$col){
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row4)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row4)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row4)->getStyle()->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row4)->getStyle()->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row4, "Cushion ($ Amount Input by Client)");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row4, number_format($cushion_value));
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row4)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row4)->getStyle()->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('e6e6e6');
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row4)->getStyle()->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('e6e6e6');

                $objPHPExcel->getActiveSheet()->getRowDimension($row4)->setRowHeight(25);

            }$row4++;
            for($col =1; $col <= 7; ++$col){
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row5)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row5)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row5)->getStyle()->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row5)->getStyle()->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row5, "Total Estimated Short-Term Working Capital");
                if($total < 0){
                    $style = array('font' => array('bold' => true, 'color' => array('rgb' => 'A52A2A')));
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row5)->getStyle()->applyFromArray($style);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row5, $db->format_number_exportXls($total, 0));
                }
                else {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row5, $db->format_number_exportXls($total, 1));
                }
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row5)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row5)->getStyle()->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('b2dca3');
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row5)->getStyle()->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('b2dca3');
                $objPHPExcel->getActiveSheet()->getRowDimension($row5)->setRowHeight(25);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row5)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
            }
            if (!file_exists(BASE_PATH . '/report')) {
                mkdir(BASE_PATH . '/report', 0777, true);
            }
            if (!file_exists(BASE_PATH . '/report/' . $client_master_id)) {
                mkdir(BASE_PATH . '/report/' . $client_master_id, 0777, true);
            }

            $file = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $excel_name = $company_name .'_'.'flashreport'.'.xlsx';
            $path = BASE_PATH . '/report/' . $client_master_id . '/' . $excel_name;
            $file->save($path);
            $downloadpath = 'api/include/report/' . $client_master_id . '/' . $excel_name;
            $resultOutput['error'] = false;
            $resultOutput['outputfile'] = $downloadpath;
            echoRespnse(200, $resultOutput);

        }
    }
});

$app->get('/client/userroles/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        $response = array();
        $db = new adminUser();
        $is_active = $app->request->get('is_active');
        $result = $db->getUserRoles();
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->get('/client/clientreportapplication/', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        global $user_id;
        $response = array();
        $searchItems = array();
        $db = new reportClientApplicationMap();
        $client_master_id = $app->request->get('client_master_id');
        $client_id = $app->request->get('client_id');
        $searchItems['client_id'] = $client_id;
        $searchItems['client_master_id'] = $client_master_id;
        $searchItems['is_active'] = 1;
        $searchItems['is_active1'] = 1;
        $result = $db->getClientReportApplication($searchItems);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    echoRespnse(200, $result);
});

$app->get('/client/xero/token/:authtoken/:outhverifier/:org', function ($oauth_token, $oauth_verifier, $org) use ($app) {
    $response = array();
    $db = new dashboardReport();
    $objUtility = new utility();

    //$tokenResult = $objUtility->getFieldByID('client_oauth_token', 'org', $org, 'client_id');
    $tokenResult = $objUtility->getFieldByID('client_oauth_token', 'oauth_token', $oauth_token, 'client_id');
    if ($tokenResult["error"] == false) {
        $client_master_data = $objUtility->getFieldByID('client_oauth_token', 'oauth_token', $oauth_token, 'client_master_id');
        $user_id = $tokenResult['field_key'];
        $client_master_id = $client_master_data['field_key'];

        //global $user_id;
        $result = $db->getoAuthKeys($user_id, $client_master_id); // here you get all keys
        if ($result['error'] == false) {
            $response = $db->XeroRefreshToken($user_id,$client_master_id, $oauth_verifier, $oauth_token, $org);
            if ($response['error'] == false) {
                $pageResult = $objUtility->getFieldByID('client_oauth_token', 'client_id', $user_id, 'page_name');
                $page_name = $pageResult['field_key'];
                // $db->updateXeroAuthenticated($user_id, 1);
                header('Location:' . CLIENT_URL . $page_name);
                die();
            }
        } else {
            $response['error'] = true;
            $response['message'] = NO_AUTH_KEYS;
        }
    } else {
        $response['error'] = true;
        $response['message'] = AUTHORIZATION_FAILURE;
    }
    /*} else {
    $response['error'] = true;
    $response['message'] = NOT_CLIENT;
    } */
    $db = null;
    $objUtility = null;
    echoRespnse(200, $response);
});

/**
 * ----------- CLIENT APPLICATION CRON JOBS METHODS  ---------------------------------
 */

/* Flash Report Summary - PDF to mail by weekly based on report_pdf_to_client */
$app->get('/client/flash_report_summary/', function () use ($app) {
    $response = array();
    $db = new dashboardReport();
    $utility = new utility();
    $result = $db->getPdfReportClients();
    if (count($result['ClientList']) > 0) {
        define(PDF_HEADER_TITLE, '');
        define(PDF_HEADER_STRING, '');
        define(PDF_HEADER_LOGO, 'logo.jpg');
        define(PDF_HEADER_LOGO_WIDTH, '40');
        define(PDF_MARGIN_HEADER, '3');
        require_once '../include/tcpdf/tcpdf.php';
        foreach ($result['ClientList'] as $value) {
            $client_master_id = $value['client_master_id'];
            $metrics_path = $value['metrics_path'];
            $cushion_value = $value['cushion_value'];
            $company_name = $value['company_name'];
            $report_start_date = $value['report_start_date'];
            $metrics_obj = json_decode(file_get_contents($metrics_path), true);
            /*Below code for get file created date*/
            $report_date = date("d F Y", strtotime($metrics_obj['last_updated_time']));
            $total_cash_on_hand = $metrics_obj['accountsSummarytotal'];
            $agedReceivablestotal = $metrics_obj['agedReceivablestotal'];
            $agedPayablestotal = $metrics_obj['agedPayablestotal'];
            $estimatedPayrolltotal = $metrics_obj['estimatedPayrolltotal'];
            $total = round(($total_cash_on_hand + $agedReceivablestotal) - ($estimatedPayrolltotal + $agedPayablestotal + $cushion_value));
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->setHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '', array(0, 0, 0), array(255, 255, 255));
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->AddPage();
            $pdf->SetFont('helvetica', '', 10);
            $message = '<table class="Financial-report-table" style="width: 100%; margin-top: 15px;">
            <tbody><tr><td colspan="2"></td></tr><tr><td colspan="2" style="line-height: 30px;padding-bottom:200px;border-bottom:2px solid #009fc5;text-left;font-size: 20px;font-weight: bold;font-family: Arial, sans-serif;">Flash Report Summary</td>
            </tr>
            <tr><td colspan="2"></td></tr><tr><td colspan="2" style="margin-top:20px;color:#000;text-align:center;font-size: 14px;font-weight: bold;font-family: Arial, sans-serif;">' . $value['company_name'] . ' <br/>As at ' . $report_date . '
            </td></tr><br/>
            <tr style="border-bottom: 2px solid #009fc5;"><td style="height:10px;line-height:200px; border-bottom: 1px solid #cdcdcd; font-size: 12px; line-height: 16px; text-align: left; font-family: Arial, sans-serif;">Total Cash On-Hand</td>
            <td style="height:10px; line-height:200px;border-bottom: 1px solid #cdcdcd; font-size: 12px; line-height: 16px; text-align: right; font-weight: bold; font-family: Arial, sans-serif;">' . $db->format_number($total_cash_on_hand, $total_cash_on_hand >= 0 ? 1 : 0) . '</td>
            </tr><br/>
            <tr><td style="height:10px; line-height:200px; border-bottom: 1px solid #cdcdcd; font-size: 12px; line-height: 16px; text-align: left; font-family: Arial, sans-serif;">Current Accounts Receivable Due</td>
            <td style="height:10px;line-height:200px;  border-bottom: 1px solid #cdcdcd; font-size: 12px; line-height: 16px; text-align: right; font-weight: bold; font-family: Arial, sans-serif;">' . $db->format_number($agedReceivablestotal, $agedReceivablestotal >= 0 ? 1 : 0) . '</td>
            </tr><br/>
            <tr> <td style="height:10px;line-height:200px; border-bottom: 1px solid #cdcdcd; font-size: 12px; line-height: 16px; text-align: left; font-family: Arial, sans-serif;">Current Accounts Payable Due</td>
            <td style="height:10px;line-height:200px; border-bottom: 1px solid #cdcdcd; font-size: 12px; line-height: 16px; text-align: right; font-weight: bold; font-family: Arial, sans-serif;">' . $db->format_number($agedPayablestotal, $agedPayablestotal >= 0 ? 1 : 0) . '</td>
            </tr><br/>
            <tr><td style="height:10px;line-height:200px;border-bottom: 1px solid #cdcdcd; font-size: 12px; line-height: 16px; text-align: left; font-family: Arial, sans-serif;">Estimated Payroll</td>
            <td style="height:10px;line-height:200px; border-bottom: 1px solid #cdcdcd; font-size: 12px; line-height: 16px; text-align: right; font-weight: bold; font-family: Arial, sans-serif;">' . $db->format_number($estimatedPayrolltotal, $estimatedPayrolltotal >= 0 ? 1 : 0) . '</td>
            </tr><br/>
            <tr><td style=" height:10px;line-height:200px; border-bottom: 1px solid #cdcdcd; font-size: 12px; line-height: 16px; text-align: left; font-family: Arial, sans-serif;">Cushion Value</td>
            <td style="height:10px;line-height:200px; border-bottom: 1px solid #cdcdcd; font-size: 12px; line-height: 16px; text-align: right; font-weight: bold; font-family: Arial, sans-serif;">' . $db->format_number($cushion_value, $cushion_value >= 0 ? 1 : 0) . '</td>
            </tr><br/>
            <tr><td style="height:10px;line-height:200px;border-bottom: 1px solid #cdcdcd; font-size: 12px; line-height: 16px; text-align: left; font-family: Arial, sans-serif; font-weight: bold;">Total</td>
            <td style="height:10px;line-height:200px; border-bottom: 1px solid #cdcdcd; font-size: 12px; line-height: 16px; text-align: right; font-weight: bold; font-family: Arial, sans-serif;">' . $db->format_number($total, $total >= 0 ? 1 : 0) . '</td>
            </tr>
            <tr><td colspan="2" style="line-height:200px;font-size: 8px; line-height: 16px; text-align: left; font-family: Arial, sans-serif;">
            <p style="text-align: left;font-size: 8px;line-height: 24px;border-top: 1px solid #009fc5;
            font-family: Arial, sans-serif;margin-bottom: 20px;">Flash Report Summary  |  ' . $value['company_name'] . '  |  ' . $report_date . '</p>
            </td></tr>
            </tbody></table>';

            $company_name = str_replace(" ", "_", $company_name);
            $pdf->writeHTML($message, true, false, false, false, '');
            $pdf_name = $company_name . '_Summary_Report.pdf';
            $fileatt = $pdf->Output($pdf_name, 'S');

            $subject = "Flash Report Summary";
            $content_message = '<p>Good morning!</p>';
            $content_message .= '<p>Your accounts have been reconciled and are ready for review, so please find attached your latest Working Capital Flash Report.</p>';
            $content_message .= '<p>Please also access your comprehensive PikoHANA reporting dashboard <a href="https://pikohana.com/client/#/login">here</a>.</p>';
            $content_message .= '<p>As always, we\'re here to help, so if you have any questions or issues, feel free to contact us at your convenience.</p><p>Have a great day!</p>';
            $content_message .= '<p>Regards, <br />Team PikoHANA</p>';
            if(ENV_LIVE) {
                $to_email = $value['email']; //TESTING_EMAIL;
                $utility->send_mail($to_email, $subject, $content_message, FROM_NAME, FROM_EMAIL, $fileatt, $pdf_name, true);
            }
        }
    } else {
        echo "No Record Found.";
    }
    $db = null;
    $utility = null;
});

$app->get('/client/reset_flash_summary/', function () use ($app) {
    $db = new dashboardreport();
    $reset_flash_summary = $db->restFlashSummary();
});

$app->get('/client/audit_activity_alert/', function () use ($app) {
    $response = array();
    $db = new dashboardReport();
    $utility = new utility();
    $result = $db->getTodayAuditAlerts();
    if (count($result['CompanyList']) > 0) {
        foreach ($result['CompanyList'] as $value) {
            $subject = $value['subject'];
            $message = $value['message'];
            $company_name = $value['company_name'];
            $registration_number = $value['registration_number'];

            $searchArray = array("[COMPANY_NAME]", "[COMPANY_UEN]");
            $replaceArray = array($company_name, $registration_number);
            $final_content = str_replace($searchArray, $replaceArray, $message);
            if(ENV_LIVE){
                $to_email = $value['email']; //TESTING_EMAIL;
                $utility->send_mail($to_email, $subject, $final_content, FROM_NAME, FROM_EMAIL, false, false, true);
            }
        }
    } else {
        echo "No Record Found.";
    }
    $db = null;
    $utility = null;
});

$app->get('/client/getAllMappedUsers', 'authenticate', function () use ($app) {
    global $is_client;
    global $user_id;
    if ($is_client == 1) {
        $response = array();
        $client = new client();
        $client_master_id = $app->request->get('client_master_id');
        $response = $client->getAllMappedUsers($user_id, $client_master_id);
        $client = null;
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    echoRespnse(200, $response);
});

$app->get('/client/getAllMappedClients/', 'authenticate', function () use ($app) {
    global $is_client;
    global $user_id;
    if ($is_client == 1) {
        $response = array();
        $client = new client();
        $result = $client->getAllMappedClients($user_id);
        $client = null;
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    echoRespnse(200, $result);
});

$app->get('/client/getUserdetails', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        $response = array();
        verifyRequiredParams(array('client_id'));
        $client = new client();
        $client_id = $app->request->get('client_id');
        $response = $client->getUsersDetails($client_id);
        $client = null;
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    echoRespnse(200, $response);
});

$app->get('/client/getUserApplicationDetails', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 1) {
        $response = array();
        verifyRequiredParams(array('client_id', 'client_master_id'));
        $client = new client();
        $client_id = $app->request->get('client_id');
        $client_master_id = $app->request->get('client_master_id');
        $result = $client->getApplicationDetails($client_id, $client_master_id);
        $response['applicationDetails'] = $result;
        $response['error'] = false;
        $response['message'] = RECORD_FOUND;
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $client = null;
    echoRespnse(200, $response);
});

$app->post('/client/ClientUserApplication', 'authenticate', function () use ($app) {
    global $is_client;

    verifyRequiredParams(array('client_id', 'client_master_id'));
    $client_id = $app->request->post('client_id');
    $client_master_id[0]['id'] = $app->request->post('client_master_id');
    $application_ids = $app->request->post('application_ids');
    $category_sub_ids = $app->request->post('category_sub_ids');
    $report_ids = $app->request->post('report_ids');

    if (sizeof($report_ids) > 0) {
        $report_ids = array_unique($report_ids);
    }

    if (sizeof($category_sub_ids) > 0) {
        $category_sub_ids = array_unique($category_sub_ids);
    }

    $client = new client();
    $response = array();

    $response = $client->updateClientUserApplicationByAdmin($client_id, $client_master_id, $application_ids, $category_sub_ids, $report_ids, true);

    $client = null;
    echoRespnse(200, $response);
});

// ////This service is to test Cash Flow Report
// $app->get('/client/xero/generateCashFlowReport', function() use($app) {
//     global $is_client;
//     $dash_db = new dashboardReport();
//     $utility = new utility();
//     $is_admin = 0;
//     $response = array();

//     $sub_category_id2 = $app->request->get('sub_category_id2');
//     $sub_category_id1 = $app->request->get('sub_category_id1');
//     $mtd = $app->request->get('mtd');
//     $refresh = $app->request->get('refresh');
//     $pagename = $app->request->get('page_name');
//     $user_id = $app->request->get('client_id');
//     $client_master_id = $app->request->get('client_master_id');
//     $actual_user_id = $user_id;

//     // $res_client_master = $utility->getClientMasterByID('client_master', 'client_id', $user_id);

//     // if ($res_client_master["error"] == false) {
//     //     $is_admin = $res_client_master["is_admin"];
//     //     $client_master_id = $res_client_master["client_master_id"];
//     // }

//     $res_is_admin = $utility->getFieldByID('client_master', 'client_id', $user_id, 'is_admin');
//     if ($res_is_admin['error'] == false) {
//         $is_admin = $res_is_admin['field_key'];
//     }

//     $loginDate = date('Y-m-d');

//     $res_client_master_detail = $utility->getClientMasterDetailByID('client_master_detail', 'client_master_id', $client_master_id);

//     if ($res_client_master_detail["error"] == false) {
//         $report_start_date = $res_client_master_detail["report_start_date"];
//         $default_currency = $res_client_master_detail["default_currency"];
//     }

//     $dates = $utility->getDayTillMonthNew($client_master_id, $report_start_date, $is_admin);

//     if ($refresh == 0 ) {
//         if(strlen(trim($sub_category_id1)) <= 0 && strlen(trim($sub_category_id2)) <= 0)
//         {
//             $sub_category_id1 = 1000;
//             $res_category_count = $utility->getCategoryCountByClientID('category_subcategory', $user_id, $client_master_id);
//             if ($res_category_count["error"] == false) {
//                 $count = $res_category_count["count"];
//                 if ($count == 2) {
//                     $sub_category_id2 = 1000;
//                 }
//             }
//         }

//         $data = $utility->checkCacheFileExist($client_master_id, $pagename, $loginDate, $sub_category_id1, $sub_category_id2);

//         if (strlen(trim($data)) > 0) {
//             $response1 = json_decode($data, true);

//             $aPNLTrend = $response1[3];
//             $aBalanceSheet = $response1[1];
//             unset($aPNLTrend['trendPNL']['trendPNL_Quartely']);

//             $f = array();
//             if ((sizeof($aPNLTrend) > 0) && (sizeof($aBalanceSheet) > 0)) {
//                 if (($aPNLTrend["error"] == false) && ($aBalanceSheet["error"] == false)) {
//                     $f[] = array("cashFlowStatement"=>$dash_db->getCashFlowStatement($aPNLTrend, $aBalanceSheet, $user_id, $dates));
//                 } else {
//                     $f[] = $aPNLTrend;
//                 }
//                 $response["error"] = false;
//                 $response["message"] = RECORD_FOUND;
//                 $response["cashflow"] = $f;
//             } else {
//                 $f[] = array("error"=>true, "message"=> NO_RECORD_FOUND);
//             }
//         }
//         else {
//             $response["error"] = false;
//             $response["message"] = NO_RECORD_FOUND;
//         }
//     }
//     $dash_db = null;
//     $utility = null;
//     echoRespnse(200, $response);
// });

// ////This service is to test Amount Invoiced by Contact
// $app->get('/client/xero/generateAmountInvoicedbyContact','authenticate', function() use($app) {
//     global $is_client;
//     $dash_db = new dashboardReport();
//     $utility = new utility();
//     $is_admin = 0;

//     //if ($is_client == 1) {
//         verifyRequiredParams(array('client_id'));
//         $user_id = $app->request->get('client_id');

//         $res_client_master = $utility->getClientMasterByID('client_master', 'client_id', $user_id);
//         if ($res_client_master["error"] == false) {
//             $is_admin = $res_client_master["is_admin"];
//             $client_master_id = $res_client_master["client_master_id"];
//         }

//         if ($is_admin != 1) {
//             $res_super_admin = $utility->getSuperAdminID($client_master_id);
//             $user_id = $res_super_admin['client_id'];
//             $refresh = 0;
//         }

//         $pageResult = $utility->isValidToken('client_oauth_token', 'client_id', $user_id);
//         $is_authenticated = ($pageResult["error"] == false) ? $pageResult['isValid'] : 0;
//         if($is_authenticated == 0 ){
//             $dash_db->updateXeroAuthenticated($user_id, $is_authenticated);
//         }
//         // if is_authenticated true, there is no need to connect xero.
//         $result = $dash_db->getoAuthKeys($user_id);

//         $res_client_master_detail = $utility->getClientMasterDetailByID('client_master_detail', 'client_master_id', $client_master_id);
//         if ($res_client_master_detail["error"] == false) {
//             $report_start_date = $res_client_master_detail["report_start_date"];
//             $default_currency = $res_client_master_detail["default_currency"];
//         }

//         if (sizeof($result) > 0) {
//             if ($result['error'] == true || $result['is_authorized'] == 0) {
//                 if ($authenticate == 1 ) {
//                     $response = $dash_db->requestToken($client_master_id, $result, $pagename);
//                     if (isset($response['authurl'])) {
//                         $authurl = $response['authurl'];
//                         header('Location:' . $authurl); die();
//                     }
//                 }
//             }
//             else {
//                 $ajax = true;
//                 if (isset($result['XeroOAuth'])) {
//                     $XeroOAuth = $result['XeroOAuth'];
//                     $XeroOAuth->config ['access_token'] = utf8_encode($result['oauth_token']);
//                     $XeroOAuth->config ['access_token_secret'] = utf8_encode($result['oauth_token_secret']);

//                     $organisations = $dash_db->getOrganisation($user_id, $XeroOAuth, $ajax);
//                     if (isset($organisations['expiry'])) {
//                         return $organisations;
//                     }
//                 }

//                 $dates = $utility->getDayTillMonthNew($client_master_id, $report_start_date, $is_admin);

//                 $categories_response = $dash_db->getInvoiceListDatareport($user_id, $XeroOAuth, $ajax, $organisations, $dates);

//                 if($categories_response['error'] ==false) {
//                     $response['error'] = false;
//                     $response['message'] = INVOICE_API_SUCCESS;
//                     $response['Invoice'] = $categories_response;
//                 }
//                 else
//                 {
//                     $response['error'] = true;
//                     $response['message'] = NO_INVOICE_LIST;
//                 }
//             }
//         } else {
//             $response['error'] = true;
//             $response['message'] = NO_AUTH_KEYS;
//         }
//     // }
//     // else {
//     //     $response['error'] = true;
//     //     $response['message'] = NOT_CLIENT;
//     // }
//     $db = null;
//     $utility = null;
//     echoRespnse(200, $response);
// });

/* This service is to test PDF Report Genearted Date
report_type   {1=>'generate_report', 2=>'financial_pdf', 3=>'flash_pdf' } */
// $app->post('/client/pdf_report_generated_date/', function () use ($app) {
//     verifyRequiredParams(array('client_id', 'report_type'));
//     $client_id = $app->request->post('client_id');
//     $report_type = $app->request->post('report_type');
//     $category1 = $app->request->post('category1');
//     $category2 = $app->request->post('category2');
//     $report_generated_date = $app->request->post('report_generated_date');
//     $db = new dashboardReport();
//     $result = $db->ReportGenerateDateforPDF($client_id, $report_type, $category1, $category2, $report_generated_date);
//     $db = null;
//     echoRespnse(200, $result);
// });

/**
 * Verifying required params posted or not.
 */
function verifyRequiredParams($required_fields)
{
    $error = false;
    $error_fields = array();
    $request_params = array();
    $request_params = $_REQUEST;

    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields[] = $field;
        }
    }
    if ($error) {
        // Required field(s) are missing or empty
        return array(
            'error' => true,
            'message' => 'Required field(s) ' . implode(', ', $error_fields) . ' is missing or empty',
        );
    }

    // return appropriate response when successful?
    return array(
        'success' => true,
    );
}

/**
 * Validating email address.
 */
function validateEmail($email)
{
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response)
{
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

/* Create group - method : POST; Params : Group name, admin id, client master id */
$app->post('/admin/creategroup', 'authenticate', function () use ($app) {
    global $is_client;
    global $user_id;
    if ($is_client == 0) {
        //verifyRequiredParams(array('group_name', 'selectedClients', 'report_start_date', 'default_currency'));
        $group_name = $app->request->post('group_name');
        $selectedClients = $app->request->post('selectedClients');
        $report_start_date = $app->request->post('report_start_date');
        $default_currency = $app->request->post('default_currency');
        $response = array();
        $group = new group();
        $response = $group->InsertGroupDetails($group_name, $user_id, $report_start_date, $default_currency, $selectedClients);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $group = null;
    echoRespnse(200, $response);
});

/* List of all group with details  */
$app->get('/admin/listgroup', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $group = new group();
        $response = $group->GetListGroup();
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $group = null;
    echoRespnse(200, $response);
});

/* List all the companies are linked with particular group*/
/* method : GET; Params : group_id */
$app->get('/admin/getGroupDetails', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('group_id'));
        $group_id = $app->request->get('group_id');
        $response = array();
        $group = new group();
        $response = $group->GetGroupCompanyList($group_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $group = null;
    echoRespnse(200, $response);
});

/* Update group - method : PUT; Params : Group name, admin id, client master id */
$app->put('/admin/updateGroup', 'authenticate', function () use ($app) {

    global $is_client;
    global $user_id;
    if ($is_client == 0) {
        verifyRequiredParams(array('group_id', 'default_currency', 'selectedClients'));
        $group_id = $app->request->put('group_id');
        $group_name = $app->request->put('group_name');
        $selectedClients = $app->request->put('selectedClients');
        $default_currency = $app->request->put('default_currency');
        $deletedClients = $app->request->put('clientsToRemove');
	$response = array();
        $group = new group();
        $response = $group->UpdateGroupDetails($group_id, $group_name, $user_id, $selectedClients, $deletedClients, $default_currency);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $group = null;
    // echo json response
    echoRespnse(200, $response);
});

$app->put('/admin/updateGroupDate', function () use ($app) {
    global $is_client;
    global $user_id;
    if ($is_client == 0) {
        verifyRequiredParams(array('group_id', 'report_start_date'));
        $group_id = $app->request->put('group_id');
        $report_start_date = $app->request->put('report_start_date');
        $response = array();
        $group = new group();
        $response = $group->UpdateGroupDateDetails($group_id, $report_start_date, $user_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $group = null;
    echoRespnse(200, $response);
});

$app->put('/admin/updateGroupName', function () use ($app) {
    global $is_client;
    global $user_id;
    if ($is_client == 0) {
        verifyRequiredParams(array('group_id', 'group_name'));
        $group_id = $app->request->put('group_id');
        $group_name = $app->request->put('group_name');
        $response = array();
        $group = new group();
        $response = $group->UpdateGroupName($group_id, $group_name, $user_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $group = null;
    echoRespnse(200, $response);
});

$app->post('/admin/updateGroupLogo', function () use ($app) {
    global $is_client;
    global $user_id;
    if ($is_client == 0) {
        verifyRequiredParams(array('group_id', 'group_logo'));
        $group_id = $app->request->post('group_id');
        if (isset($_FILES)) {
            if (isset($_FILES['group_logo'])) {
                $group_logo = array();
                $group_logo = $_FILES['group_logo'];
            } else {
                $group_logo = $app->request->post('group_logo');
                if ($group_logo == "null") {
                    $group_logo = null;
                }
            }
        }
        $response = array();
        $group = new group();
        $response = $group->UpdateGroupLogo($group_id, $group_logo, $user_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $group = null;
    echoRespnse(200, $response);
});

$app->get('/admin/getCategoryByGroupCompany', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('group_id'));
        $group_id = $app->request->get('group_id');
        $client_master_id = $app->request->get('client_master_id');
        $response = array();
        $group = new group();
        $response = $group->getCategoryByGroupCompany($group_id, $client_master_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $group = null;
    echoRespnse(200, $response);
});

$app->post('/admin/eliminateCategory', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('category_id'));
        $category_id = $app->request->post('category_id');
        $is_eliminated = $app->request->post('is_eliminated');
        $response = array();
        $group = new group();
        $response = $group->updateEliminate($category_id, $is_eliminated);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $group = null;
    echoRespnse(200, $response);
});

$app->get('/client/generateFathomData/', function () use ($app) {
    $loader = require '../vendor/autoload.php';
    $loader->add('Tightenco/Collect', __DIR__.'/../src/');
        $response = array();
        $group = new group();
        $category1 = 1000;
        $category2 = "";
        /**** group comapny mapping the data ****/
        //$response = $group->GenerateGroupMetrics(63, 101, '2018-06-06', 1000, 1000);
        $group_array = $group->getGroupDetailList();

        $finalArray = array();
        foreach($group_array['result'] as $first_inner_array){
            $client_id = $first_inner_array['client_id'];
            $group_ids = $first_inner_array['group_id'];
            $report_start_date = $first_inner_array['report_start_date'];
            $toCur = $first_inner_array['default_currency'];
            $files = $group->getJsonDataByGroup($client_id, "pl_report", $category1, $category2, $first_inner_array);

            if($result['error'] == false){
                $currency_value = $group->getCurrencyValue($files, $toCur);
                $jsonMerger = new JSONMerger($files);
                $tendData = $jsonMerger->trendIndex($group_id, $eliminate_json);
                $balanceData = $jsonMerger->balanceSheetIndex($group_id, $eliminate_json);
                $trendPNL = new TrendPNL($tendData);
                $balanceSheet = new BalanceSheet($balanceData);

                /**Merge Array **/
                $totalArray  = "";
                $totalArray  .= "{";
                /**trackingcategories */
                $getTrackingCategory  = $group->getTrackingCategory();
                $totalArray .= $getTrackingCategory;
                /**balancesheet */
                $balancesheetArray = $group->getTrendBAlancesheetReportValues($files, $toCur, $balanceSheet);
                $totalArray .= $balancesheetArray;
                /**consolidate */
                /**mtd */
                $MTDReportVAlus = $group->getConsolidateMTDReport($files, $toCur, $currency_value);
                $final_array = "";
                $final_array .= $MTDReportVAlus;
                /**YTD */
                $YTDReportVAlus = $group->getConsolidateYTDReport($files, $toCur, $currency_value);
                $final_array .= $YTDReportVAlus;
                /**YTY */
                $YTYReportVAlus = $group->getConsolidateYTYReport($files, $toCur, $currency_value);
                $final_array .= $YTYReportVAlus;
                $totalArray .= $final_array;
                /**trendPNL */
                 $trendpnlarray = $group->getTrendQuartelyReportValues($files, $toCur, $trendPNL, $currency_value);
                 $totalArray .= $trendpnlarray;
                /** */
                /**currencyList */
                $getCurrencyListArray = $group->getCurrencyList();
                $totalArray .= $getCurrencyListArray;
                /** */
                /**cashFlowStatement */
                $cashflow_finalArray = $group->getCashFlowReport($files, $toCur, $currency_value);
                $totalArray .= $cashflow_finalArray;
                /** */
                /**invoiceByName */
                $invoiceByName_finalArray = $group->getinvoiceByNameReport();
                $totalArray .= $invoiceByName_finalArray;
                /**Currency conversion */
                $invoiceByName_finalArray = $group->currencyConversion($files, $toCur, $currency_value);
                $totalArray .= $invoiceByName_finalArray;
                $totalArray .= ',';

                $date = date("Y-m-d H:i:s");
                $totalArray .= '"last_updated_time":'. '"'.$date.'"';
                $totalArray .= '}';

                /** generate plreport and metrics json files*/
                foreach($first_inner_array['group_details'] as $values){
                     $result = $group->getClientGroupDetails($group_ids, $client_id, "pl_report", $category1, $category2, $values);
                    if($result['error'] == false){
                        $id = $result['group_id'];
                        if (!file_exists(BASE_PATH . '/plreport')) {
                            mkdir(BASE_PATH . '/plreport', 0777, true);
                        }
                        if (!file_exists(BASE_PATH . '/plreport/' . $group_ids)) {
                            mkdir(BASE_PATH . '/plreport/' . $group_ids, 0777, true);
                        }

                        $file_name = $group_ids . '_plreport_' .$report_start_date.'_'. 1000  . '.json';
                        $path = BASE_PATH . '/plreport/' . $group_ids . '/'. $file_name;

                        $response['error'] = false;
                        $response['message'] = RECORD_FOUND;
                        $data = file_put_contents($path, $totalArray);
                        $GenerateGroupMetrics = $group->GenerateGroupMetrics($group_ids, $report_start_date, 1000, "");
                    }
                }
            }else{
                $response['error'] = false;
                $response['message'] = NO_RECORD_FOUND;
            }
        }
    $group = null;
    echoRespnse(200, $response);
});

$app->get('/admin/syncGroupData/:id', 'authenticate', function ($group_id) use ($app) {
    $loader = require '../vendor/autoload.php';
    $loader->add('Tightenco/Collect', __DIR__.'/../src/');
    $response = array();
    $group = new group();
    $master_data = $group->getMasterIdByGroupId($group_id);

    $report_start_date = $master_data['result']['report_start_date'];
    $default_currency = $master_data['result']['default_currency'];
    $client_master_id = $master_data['result']['group_details'];

    $finalResponse = $group->groupCategoryDetails($group_id, $client_master_id, $report_start_date, $default_currency);
    if($finalResponse['error'] == false){
        $response['error'] = false;
        $response['message'] = SYNC_GROUP;
    }else{
        $response['error'] = true;
    }

    $group = null;
    echoRespnse(200, $response);
});

$app->put('/admin/deleteGroup/:id', 'authenticate', function ($group_id) use ($app) {
    global $is_client;
    $response = array();
    $group = new group();
    if ($is_client == 0) {
        $response = $group->deleteGroup($group_id);
        $dir = BASE_PATH . '/plreport/' . $group_id;
        if(chmod($dir, 0777) ) {
            $group->deleteFolderByGroupId($dir);
        }
    }else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $group = null;
    echoRespnse(200, $response);
});

$app->get('/client/get_account_categories/', 'authenticate', function () use ($app) {
    global $is_client;
    $dash_db = new dashboardReport();
    $utility = new utility();
    if ($is_client == 1) {
        verifyRequiredParams(array('client_master_id', 'page_name', 'client_id'));
        $pagename = $app->request->get('page_name');
        $client_master_id = $app->request->get('client_master_id');
        $client_id = $app->request->get('client_id');
        $group_id = $app->request->get('group_id');
        $pageResult = $utility->isValidToken('client_oauth_token', 'client_id', $client_id);
        $is_authenticated = ($pageResult["error"] == false) ? $pageResult['isValid'] : 0;
        if ($is_authenticated == 0) {
            $dash_db->XeroRefreshToken($client_id, $client_master_id);
        }
        $result = $dash_db->getoAuthKeys($client_id, $client_master_id);
        if (sizeof($result) > 0) {
            if ($result['error'] == true || $result['is_authorized'] == 0) {
                if ($is_authenticated == 1) {
                    $response = $dash_db->requestToken($client_master_id, $result, $pagename);
                    if (isset($response['authurl'])) {
                        $authurl = $response['authurl'];
                        header('Location:' . $authurl);
                        die();
                    }
                }
            } else {
                //$response["is_authenticated"] = $is_authenticated;
                $categories_response = $dash_db->getAccountCategoriesData($group_id, $client_master_id, $result);
                $response['final_result'] = $categories_response;
                $response['error'] = false;
                $response['message'] = ACCOUNT_CATGORY_API_SUCCESS;
            }
        } else {
            $response['error'] = true;
            $response['message'] = NO_AUTH_KEYS;
        }
    } else {
        $response['error'] = true;
        $response['message'] = NOT_CLIENT;
    }
    $db = null;
    $utility = null;
    // echo json response
    echoRespnse(200, $response);
});

$app->post('/client/fetchCategories', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('group_id', 'client_master_id'));
        $group_id = $app->request->post('group_id');
        $client_master_id = $app->request->post('client_master_id');
        $response = array();
        $client = new client();
        $response = $client->UpdateGroupName($group_id, $client_master_id, $group_name);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});



/* List all the companies for mapping to groups */
/* method : POST; Optional Params : group_id */
/* if group_id present already selected companies wont come in left array */
$app->post('/client/companylist', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $client = new client();
        $group_id = $app->request->post('group_id');
        $response = $client->GetCompanyList($group_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

/* Map clients to group or companies */
/* method : POST; */
/* Group Link Params - group_id, client id */
/* Company Link Params - client id, client_master_id */
$app->post('/client/add_group_company_link', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('client_id'));
        $group_id = $app->request->post('group_id');
        $client_id = $app->request->post('client_id');
        $client_master_id = $app->request->post('client_master_id');
        $response = array();
        $client = new client();
        $response = $client->AddGroupCompanyLink($group_id, $client_id, $client_master_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

/* Update already mapped clients to group or companies */
/* method : POST; */
/* Group Link Params - group_id, client id */
/* Company Link Params - client id, client_master_id */
$app->post('/client/edit_group_company_link', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        verifyRequiredParams(array('client_id'));
        $group_id = $app->request->post('group_id');
        $client_id = $app->request->post('client_id');
        $client_master_id = $app->request->post('client_master_id');
        $response = array();
        $client = new client();
        $response = $client->EditGroupCompanyLink($group_id, $client_id, $client_master_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

/* List all the companies and group mapped with particular client_id*/
/* method : POST; Params : client id*/
$app->post('/client/user_companies_and_group_list', 'authenticate', function () use ($app) {
    global $is_client;
    // check for required params
    if ($is_client == 0) {
        verifyRequiredParams(array('client_id'));
        $client_id = $app->request->post('client_id');
        $response = array();
        $client = new client();
        $response = $client->GetUserCompanyAndGroupList($client_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

/* clients list for add/edit group or companies */
/* method : POST; */
/* param - mode : add/edit */
/* Group Add/edit Params - group_id, client_id */
/* Company Add/edit Params - client_id, client_master_id */
$app->post('/client/user_list', 'authenticate', function () use ($app) {
    global $is_client;
    if ($is_client == 0) {
        $response = array();
        $client = new client();
        $group_id = $app->request->post('group_id');
        $mode = $app->request->post('mode');
        $client_master_id = $app->request->post('client_master_id');
        $response = $client->GetUserList($group_id, $mode, $client_master_id);
    } else {
        $response['error'] = true;
        $response['message'] = NOT_ADMIN;
    }
    $db = null;
    echoRespnse(200, $response);
});

$app->get('/client/admin_xero_connect', function () use ($app) {
    $dash_db = new dashboardReport();
    $result = $dash_db->getoAdminAuthKeys();
    // here you get all keys
    $response = $dash_db->AdminrequestToken($result);
    if (isset($response['authurl'])) {
        $authurl = $response['authurl'];
        header('Location:' . $authurl);
        die();
    }
});

$app->get('/client/xero/admin_token/:authtoken/:outhverifier/:org', function ($oauth_token, $oauth_verifier, $org) use ($app) {
    $response = array();
    $db = new dashboardReport();
    $result = $db->getoAdminAuthKeys();
    $response = $db->getAdminAccessToken($result, $oauth_verifier, $oauth_token, $org);
    $categories_response = $db->getAccountCategoriesData(1, 1, $result);
    $db = null;
    //echoRespnse(200, $response);
});
$app->run();
