<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
require_once dirname(__FILE__) . '/DbConnect.php';

class dashboardReport extends utility
{

    // private $conn;

    public function __construct()
    {
        // opening db connection
        $this->db = new DbConnect();
        $this->conn = $this->db->connect();
    }

    public function doCurl($url, $params)
    {
        try {
            header("Content-type: application/json");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 120);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            $result = curl_exec($ch);
            curl_close($ch);
            $resp = json_decode($result, true);
            return $resp;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    /**
     * Fetching all industry_type
     *
     */
    public function getKPIData($client_id, $report_id, $client_master_id)
    {
        try {

            $response = array();
            $searchItems['client_id'] = $client_id;
            //$searchItems['is_active'] = 1;
            $searchItems['report_id'] = $report_id;
            $searchItems['client_master_id'] = $client_master_id;
            $searchItems['application_id'] = 2;
            $objReportMap = new reportClientApplicationMap();
            $resp = array();
            $kpiResponse = array();
            $response = $objReportMap->getAllReportClientApplicationMaps($searchItems);

            if (sizeof($response) > 0) {
                if ($response['error'] == false) {
                    foreach ($response['reportClientApplicationMapDetails'] as $k => $v) {
                        $company_name = $v['company_name'];
                        if (strlen(trim($company_name)) > 0) {
                            $path_url = KPI_ZOHO_FETCH_URL;
                            $token = $v['report_auth_key'];
                            $api_url = $v['application_api_url'] . $path_url;
                            $param = "authtoken=" . $token . "&scope=crmapi&criteria=(Company Name:" . $company_name . ")";

                            $curlResult = $this->doCurl($api_url, $param);

                            if (sizeof($curlResult) > 0) {
                                //Check error code from curl if api url is not valid
                                $curlResponse = isset($curlResult['response']['result']['CustomModule1']['row'][0]) ? $curlResult['response']['result']['CustomModule1']['row'][0]['FL'] : $curlResult['response']['result']['CustomModule1']['row']['FL'];

                                if (sizeof($curlResponse) > 0) {
                                    foreach ($curlResponse as $k => $v) {
                                        $key = str_replace(' ', '_', strtolower($v['val']));
                                        $key = preg_replace("/\([^)]+\)/", "", $key);
                                        $resp[$key] = $v['content'];
                                    }
                                } else {
                                    $kpiResponse['error'] = true;
                                    $kpiResponse['message'] = NO_CRM_RECORD_FOUND;
                                    return $kpiResponse;
                                }
                            } else {
                                $kpiResponse['error'] = true;
                                $kpiResponse['message'] = SERVER_NO_RESPONSE;
                                return $kpiResponse;
                            }
                        } else {
                            $kpiResponse['error'] = true;
                            $kpiResponse['message'] = CONFIGURE_COMPANY;
                            return $kpiResponse;
                        }
                    }
                }
            }

            $kpiResponse['kpiReport'] = $resp;
            if (sizeof($kpiResponse['kpiReport']) > 0) {
                $kpiResponse['error'] = false;
                $kpiResponse['message'] = RECORD_FOUND;
                $kpiResponse["company_name"] = $company_name;
            } else {
                $kpiResponse['error'] = true;
                $kpiResponse['message'] = NO_CRM_RECORD_FOUND;
            }
            return $kpiResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getWeightedPipeLine($response, $client_id, $application_id, $currencyResult)
    {
        try {
            $resp = array();
            $pipelineResponse = array();
            $temp = array();
            $tmp = array();
            $aFormatted = array();

            foreach ($response as $k => $v) {
                $company_name = $v['company_name'];
                $path_url = WEIGHTED_PIPELINE_ZOHO_FETCH_URL;
                $token = $v['auth_key'];
                $api_url = $v['application_api_url'] . $path_url;
                $param = "newFormat=1&authtoken=" . $token . "&scope=crmapi&fromIndex=1&toIndex=200";
                $curlResult = $this->doCurl($api_url, $param);

                if (sizeof($curlResult) > 0) {
                    $flArray = array();
                    $aPipelineRow = array();
                    $curlResponse = $curlResult['response']['result']['Potentials']['row'];
                    if (((sizeof($curlResponse)) > 0) && (isset($curlResponse[0]['FL']))) {
                        for ($i = 0; $i < sizeof($curlResponse); $i++) {
                            $flArray = $curlResponse[$i]['FL'];
                            foreach ($flArray as $k => $v) {
                                $key = str_replace(' ', '_', strtolower($v['val']));
                                $key = preg_replace("/\([^)]+\)/", "", $key);
                                if ($key == 'teritory') {
                                    $key = 'territory';
                                }

                                $resp[$key] = $v['content'];
                            }
                            $aPipelineRow[$i] = $resp;
                        }
                    }
                } else {
                    $pipelineResponse["error"] = true;
                    $pipelineResponse["message"] = SERVER_NO_RESPONSE;
                    return $pipelineResponse;
                }
            }

            if (sizeof($aPipelineRow) > 0) {
                foreach ($aPipelineRow as $ky => $vl) {
                    if ($vl["stage"] != 'Closed Won') {
                        $tmp[$vl["territory"]][$vl["brand"]][] = $vl;
                    }
                }

                foreach ($tmp as $k => $v) {
                    $temp["territory"] = $k;
                    $i = 0;
                    foreach ($v as $key => $val) {
                        $temp["brand"] = $key;
                        $temp["total_amount"] = 0;
                        foreach ($val as $k2 => $v2) {
                            $temp["total_amount"] += ($v2["currency"] != $currencyResult['field_key']) ? round($v2["expected_revenue"] / $v2["exchange_rate"]) : round($v2["expected_revenue"]);
                            $val[$k2]["amount_in_sgd"] = ($v2["currency"] != $currencyResult['field_key']) ? round($v2["amount"] / $v2["exchange_rate"]) : round($v2["amount"]);
                            $val[$k2]["expected_revenue_in_sgd"] = ($v2["currency"] != $currencyResult['field_key']) ? round($v2["expected_revenue"] / $v2["exchange_rate"]) : round($v2["expected_revenue"]);
                        }
                        $temp["currency"] = $currencyResult['field_key'];
                        $temp["reports"] = $val;
                        $aFormatted[] = $temp;
                    }
                }
                $pipelineResponse['pipelineReport'] = $aFormatted;
                if (sizeof($pipelineResponse['pipelineReport']) > 0) {
                    $pipelineResponse['error'] = false;
                    $pipelineResponse['message'] = RECORD_FOUND;
                } else {
                    $pipelineResponse['error'] = true;
                    $pipelineResponse['message'] = NO_RECORD_FOUND;
                }
            } else {
                $pipelineResponse['error'] = true;
                $pipelineResponse['message'] = NO_RECORD_FOUND;
            }
            return $pipelineResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getSalesOrders($response, $client_id, $application_id, $currencyResult, $client_master_id)
    {
        try {
            $temp = array();
            $tmp = array();
            $aFormatted = array();
            $salesOrdersResponse = array();

            $report_start_date = $this->getReportFromDate($client_master_id);
            foreach ($response as $k => $v) {
                $company_name = $v['company_name'];
                $path_url = SALES_ORDERS_ZOHO_FETCH_URL;
                $token = $v['auth_key'];
                $api_url = $v['application_api_url'] . $path_url;
                $thisYear = date('Y') . '-01-01';
                $param = "authtoken=" . $token . "&scope=crmapi&fromIndex=1&toIndex=200";

                $curlResult = $this->doCurl($api_url, $param);

                if (sizeof($curlResult) > 0) {

                    $flArray = array();
                    $aSalesOrderRow = array();
                    $curlResponse = $curlResult['response']['result']['SalesOrders']['row'];
                    if ((sizeof($curlResponse) > 0) && isset($curlResponse[0]['FL'])) {

                        for ($i = 0; $i < sizeof($curlResponse); $i++) {
                            $flArray = $curlResponse[$i]['FL'];
                            foreach ($flArray as $k => $v) {
                                $key = str_replace(' ', '_', strtolower($v['val']));
                                $key = preg_replace("/\([^)]+\)/", "", $key);
                                $resp[$key] = $v['content'];
                            }
                            $aSalesOrderRow[] = $resp;
                        }

                        if ((sizeof($aSalesOrderRow) > 0) && strlen(trim($report_start_date)) > 0) {
                            $aFinalSalesOrderRow = array();
                            for ($i = 0; $i <= sizeof($aSalesOrderRow); $i++) {
                                $checkDate = $aSalesOrderRow[$i]['year'] . '-' . $aSalesOrderRow[$i]['issue_/_month'] . '-01';
                                if (date("Y-m-01", strtotime($report_start_date)) <= date("Y-m-01", strtotime($checkDate))) {
                                    $aFinalSalesOrderRow[] = $aSalesOrderRow[$i];
                                }
                            }
                        } else {
                            $aFinalSalesOrderRow = [];
                        }
                    }
                } else {
                    $salesOrdersResponse['error'] = true;
                    $salesOrdersResponse['message'] = SERVER_NO_RESPONSE;
                    return $salesOrdersResponse;
                }
            }

            $budgetSummarys = array();
            $searchParams = array();
            $searchParams["item_head"] = "Total Income";
            $userAdmin = $this->getFieldByID('client_master', 'client_id', $client_id, 'is_admin');
            if ($userAdmin["field_key"] == 0) {
                $resp1 = array();
                $resp1 = $this->getFieldByID("client_master", 'client_id', $client_id, 'client_master_id');
                $client_master_id = $resp1["field_key"];
                $resp2 = $this->getSuperAdminID($client_master_id);
                $client_id = $resp2["client_id"];
                $resp2 = null;
                $resp = null;
            }

            $budgetSummarys = $this->getBudgetDetailsFromDB($client_master_id, $searchParams);

            if (sizeof($budgetSummarys["budgetSummary"]) > 0) {
                $amount = 0;
                foreach ($budgetSummarys["budgetSummary"] as $ky => $vy) {
                    if (strlen(trim($vy["territory_name"])) > 0) {
                        $amount = round(trim(str_replace('$', '', $vy["total_values"])));
                        $budget[trim($vy["territory_name"])][trim($vy["brand_name"])] = $amount;
                    }
                }
            }

            if (sizeof($aFinalSalesOrderRow) > 0) {
                foreach ($aFinalSalesOrderRow as $ky => $vl) {
                    if ($vl["status"] == "Delivered") {
                        $total = round($vl["grand_total"] / $vl["exchange_rate"]);
                        $vl["grand_total"] = ($vl["currency"] != $currencyResult['field_key']) ? $total : round($vl["grand_total"]);
                        $tmp[$vl["territory"]][$vl["brand"]][] = $vl;
                    }
                }

                if (sizeof($tmp) > 0) {
                    foreach ($tmp as $k => $v) {
                        $temp["territory"] = $k;

                        foreach ($v as $key => $val) {
                            $temp["brand"] = $key;
                            $temp["reports"] = $val;
                            $temp["actual_amount"] = 0;
                            $temp["budget"] = 0;
                            $temp["variance"] = 0;
                            $temp["varpercent"] = 0;
                            foreach ($val as $k1 => $v1) {
                                $temp["actual_amount"] += $v1["grand_total"];
                            }

                            $temp["budget"] = isset($budget[$k][$key]) ? $budget[$k][$key] : '0';
                            $temp["variance"] = ($temp["budget"] != '0') ? round($temp["actual_amount"] - $temp["budget"]) : round($temp["actual_amount"]);
                            $temp["varpercent"] = ($temp["budget"] != 0 || $temp["budget"] != 0.00) ? round($temp["variance"] / $temp["budget"], 2) . '%' : 0;
                            $aFormatted[] = $temp;
                        }
                    }
                }
            }

            $salesOrdersResponse['salesOrderReport'] = $aFormatted;
            if (sizeof($salesOrdersResponse['salesOrderReport']) > 0) {
                $currencyResult = null;
                $salesOrdersResponse['error'] = false;
                $salesOrdersResponse['message'] = RECORD_FOUND;
            } else {
                $salesOrdersResponse['error'] = true;
                $salesOrdersResponse['message'] = NO_RECORD_FOUND;
            }
            return $salesOrdersResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getTotalOfSales($weighPipeline, $salesOrder)
    {
        try {
            $aFormatted = array();
            $totalSalesOrdersResponse = array();
            $aWeighted = $weighPipeline[0]["pipelineReport"];
            $aSalesOrder = $salesOrder[0]["salesOrderReport"];
            foreach ($aWeighted as $k1 => $v1) {
                $weighted[$v1["territory"]][$v1["brand"]] = $v1;
            }

            foreach ($aSalesOrder as $k => $v) {
                $tmp = array();
                $tmp["territory"] = $v["territory"];
                $tmp["brand"] = $v["brand"];
                $tmp["actual_amount"] = round($v["actual_amount"] + $weighted[$tmp["territory"]][$tmp["brand"]]["total_amount"]);
                $tmp["budget_amount"] = round($v["budget"]);
                $tmp["variance"] = round($tmp["actual_amount"] - $tmp["budget_amount"]);
                $tmp["varpercent"] = ($tmp["budget_amount"] != 0 || $tmp["budget_amount"] != 0.00) ? round($tmp["variance"] / $tmp["budget_amount"], 2) . '%' : 0;
                $aFormatted[] = $tmp;
            }

            $totalSalesOrdersResponse['totalSalesOrder'] = $aFormatted;
            if (sizeof($totalSalesOrdersResponse['totalSalesOrder']) > 0) {
                $totalSalesOrdersResponse['error'] = false;
                $totalSalesOrdersResponse['message'] = RECORD_FOUND;
            } else {
                $totalSalesOrdersResponse['error'] = true;
                $totalSalesOrdersResponse['message'] = NO_RECORD_FOUND;
            }
            return $totalSalesOrdersResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getXeroKeys()
    {
        try {
            $objReportMap = new application();
            $resp = array();
            $searchItems = array();
            $searchItems['application_id'] = 1;
            $response = $objReportMap->getAllApplications($searchItems);

            if (sizeof($response) > 0) {
                if ($response['error'] == false) {
                    foreach ($response['applicationDetails'] as $k => $v) {
                        $resp['customer_key'] = $v['customer_key'];
                        $resp['secret_key'] = $v['secret_key'];
                    }
                    $resp['error'] = false;
                    $resp['message'] = KEYS_FOUND;
                } else {
                    $resp['error'] = true;
                    $resp['message'] = NO_AUTH_KEYS;
                }
            } else {
                $resp = $response;
            }
            return $resp;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function configureSignature($keys)
    {
        try {
            $signatures = array(
                'consumer_key' => $keys['customer_key'],
                'shared_secret' => $keys['secret_key'],
                // API versions
                'core_version' => '2.0',
                'payroll_version' => '1.0',
                'file_version' => '1.0',
            );

            if (XRO_APP_TYPE == "Private" || XRO_APP_TYPE == "Partner") {
                $signatures['rsa_private_key'] = BASE_PATH . '/certs/privatekey.pem';
                $signatures['rsa_public_key'] = BASE_PATH . '/certs/publickey.cer';
            }
            if (XRO_APP_TYPE == "Partner") {
                $signatures['curl_ssl_cert'] = BASE_PATH . '/xero/certs/entrust-cert-RQ3.pem';
                $signatures['curl_ssl_password'] = '1234';
                $signatures['curl_ssl_key'] = BASE_PATH . '/xero/certs/entrust-private-RQ3.pem';
            }
            return $signatures;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAuthFromDB($client_id, $client_master_id)
    {
        try {
            $sql = "SELECT client_id, client_master_id, oauth_token, oauth_token_secret, is_authorized, req_oauth_token, page_name, session_handle, expires FROM client_oauth_token WHERE client_id = ? AND client_master_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ii", $client_id, $client_master_id);
                $stmt->execute();
                $stmt->store_result();
                //$stmt->close();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($client_id, $client_master_id, $oauth_token, $oauth_token_secret, $is_authorized, $req_oauth_token, $page_name, $session_handle, $expires);
                    while ($result = $stmt->fetch()) {
                        $response["error"] = false;
                        $response["message"] = RECORD_FOUND;
                        $response["client_id"] = $client_id;
                        $response["client_master_id"] = $client_master_id;
                        $response["oauth_token"] = $oauth_token;
                        $response["oauth_token_secret"] = $oauth_token_secret;
                        $response["session_handle"] = $session_handle;
                        $response["expires"] = $expires;
                        $response['is_authorized'] = $is_authorized;
                        $response['req_oauth_token'] = $req_oauth_token;
                        $response['page_name'] = $page_name;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getoAuthKeys($client_id, $client_master_id)
    {
        try {
            $publicKeys = array();
            $publicKeys = $this->getXeroKeys();

            if (sizeof($publicKeys) > 0) {
                if ($publicKeys['error'] == false) {
                    $aSignatures = array();
                    $aSignatures = $this->configureSignature($publicKeys);
                    $XeroOAuth = new XeroOAuth(array_merge(array(
                        'application_type' => XRO_APP_TYPE,
                        'oauth_callback' => utf8_encode(OAUTH_CALLBACK),
                        'user_agent' => utf8_encode(XERO_ACCOUNT),
                    ), $aSignatures));

                    $initialCheck = $XeroOAuth->diagnostics();
                    $checkErrors = count($initialCheck);
                    if ($checkErrors > 0) {
                        // you could handle any config errors here, or keep on truckin if you like to live dangerously
                        foreach ($initialCheck as $check) {
                            $response['error'] = true;
                            $response['message'] = 'Error: ' . $check . PHP_EOL;
                        }
                    } else {
                        $here = XeroOAuth::php_self();
                        $oAuthValues = array();
                        $oAuthValues = $this->getAuthFromDB($client_id, $client_master_id);
                        if (sizeof($oAuthValues) > 0) {
                            if ($oAuthValues['error'] == false) {
                                $oAuthValues['XeroOAuth'] = $XeroOAuth;
                                return $oAuthValues;
                            } else {
                                $oAuthValues['XeroOAuth'] = $XeroOAuth;
                                $response = $oAuthValues;
                            }
                        } else {
                            $response['error'] = true;
                            $response['message'] = NO_AUTH_KEYS;
                            $response['XeroOAuth'] = $XeroOAuth;
                        }
                    }
                } else {
                    $response = $publicKeys;
                }
            } else {
                $response['error'] = true;
                $response['message'] = ISSUE_ON_KEYS;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAccessToken($client_id, $data, $oauthVerifier, $oauth_token, $org)
    {
        try {
            $here = XeroOAuth::php_self();
            $XeroOAuth = $data['XeroOAuth'];

            $XeroOAuth->config['access_token'] = utf8_encode($data['req_oauth_token']);
            $XeroOAuth->config['access_token_secret'] = utf8_encode($data['oauth_token_secret']);
            $code = $this->makeRequest($client_id,'getAccessToken',$XeroOAuth,'GET', $XeroOAuth->url('AccessToken', ''), array(
                'oauth_verifier' => utf8_encode($oauthVerifier),
                'oauth_token' => utf8_encode($oauth_token),
            ));

            if ($XeroOAuth->response['code'] == 302) {
                $response["error"] = true;
                $response["message"] = XERO_HAVING_PROBLEM;
                return $response;
            }
            if ($XeroOAuth->response['code'] == 200) {
                $response = $XeroOAuth->extract_params($XeroOAuth->response['response']);
                $oauth_new_token = $response['oauth_token'];
                $oauth_token_secret = $response['oauth_token_secret'];
                $resp = array();
                $response = $this->saveAuthKeys($client_id, $oauth_new_token, $oauth_token_secret, 1, $org, '', "");
            } else {
                $response['error'] = true;
                $response['message'] = $XeroOAuth;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getOauthClientToken($client_id, $client_master_id)
    {
         $stmt = $this->conn->prepare("SELECT * FROM client_oauth_token WHERE client_id=" . $client_id . " AND  client_master_id = ".$client_master_id." LIMIT 1");
         $stmt->execute();
         $row = $stmt->fetch();
         return $row;
    }

     public function XeroRefreshToken($client_id,$client_master_id, $oauth_verifier = null, $req_oauth_token = null, $org = null)
     {
         $publicKeys = $this->getXeroKeys();
         $aSignatures = $this->configureSignature($publicKeys);
         $XeroOAuth = new XeroOAuth(array_merge(array(
             'application_type' => XRO_APP_TYPE,
             'oauth_callback' => utf8_encode(OAUTH_CALLBACK),
             'user_agent' => utf8_encode(XERO_ACCOUNT),
         ), $aSignatures));
         $initialCheck = $XeroOAuth->diagnostics();
         $checkErrors = count($initialCheck);
         if ($checkErrors > 0) {
             // you could handle any config errors here, or keep on truckin if you like to live dangerously
             foreach ($initialCheck as $check) {
                 $response['error'] = true;
                 $response['message'] = 'Error: ' . $check . PHP_EOL;
             }
         } else {
             $here = XeroOAuth::php_self();
             $token = $this->getAuthFromDB($client_id,$client_master_id);
             if (session_status() === PHP_SESSION_NONE) {
                 session_start();
             }
             $responseData = [];
             /* Insert token for new client */
             if (empty($token) && $oauth_verifier == null || $token['error']==true) {
                 $params = array(
                     'oauth_callback' => utf8_encode(OAUTH_CALLBACK),
                 );
                 $response = $this->makeRequest($client_id, 'XeroRefreshToken', $XeroOAuth, 'GET', $XeroOAuth->url('RequestToken', ''), $params);
                 if ($XeroOAuth->response['code'] == 200) {
                     $scope = "";
                     $_SESSION['oauth'] = $XeroOAuth->extract_params($XeroOAuth->response['response']);
                     $authurl = $XeroOAuth->url("Authorize", '') . "?oauth_token={$_SESSION['oauth']['oauth_token']}&scope=" . $scope;
                     $response = $this->saveAuthKeys($client_id, $_SESSION['oauth']['oauth_token'], $_SESSION['oauth']['oauth_token_secret'], 1, '', '', "","","",$client_master_id);
                     $_SESSION['oauth']['client_id'] = $client_id;
                     $responseData['authurl'] = $authurl;
                     $responseData['XeroOAuth'] = $XeroOAuth;
                     $responseData['error'] = false;
                 } else {
                     $responseData['error'] = true;
                     $responseData['message'] = $XeroOAuth->response['response'];
                 }
             } else if (!empty($token['session_handle']) && (!empty($token['expires']) && $token['expires'] <= (time() + 100))) {
                 $refreshResponse = $XeroOAuth->refreshToken($token['oauth_token'], $token['session_handle']);
                 if ($XeroOAuth->response['code'] == 200) {
                     $oauth_new_token = $refreshResponse['oauth_token'];
                     $oauth_token_secret = $refreshResponse['oauth_token_secret'];
                     $session_handle = $refreshResponse['oauth_session_handle'];
                     $expires = time() + intval($refreshResponse['oauth_expires_in']);
                     $response = $this->saveAuthKeys($client_id, $oauth_new_token, $oauth_token_secret, 1, $org, '', "", $session_handle, $expires,$client_master_id);
                     $newtoken = $this->getAuthFromDB($client_id, $client_master_id);
                     $XeroOAuth->config['access_token'] = $newtoken['oauth_token'];
                     $XeroOAuth->config['access_token_secret'] = $newtoken['oauth_token_secret'];
                     $responseData['XeroOAuth'] = $XeroOAuth;
                     $responseData['error'] = false;
                 } else {
                     $responseData['error'] = true;
                     $responseData['message'] = $XeroOAuth->response['response'];
                 }

             } else {
                 if (!empty($oauth_verifier) || $oauth_verifier !== null) {
                     $XeroOAuth->config['access_token'] = $_SESSION['oauth']['oauth_token'];
                     $XeroOAuth->config['access_token_secret'] = $_SESSION['oauth']['oauth_token_secret'];

                     $code = $this->makeRequest($client_id,'XeroRefreshToken',$XeroOAuth,'GET', $XeroOAuth->url('AccessToken', ''), array(
                         'oauth_verifier' => $oauth_verifier,
                         'oauth_token' => $req_oauth_token,
                     ));

                     if ($XeroOAuth->response['code'] == 200) {
                         $response = $XeroOAuth->extract_params($XeroOAuth->response['response']);
                         $oauth_new_token = $response['oauth_token'];
                         $oauth_token_secret = $response['oauth_token_secret'];
                         $session_handle = $response['oauth_session_handle'];
                         $expires = time() + intval($response['oauth_expires_in']);
                         $response = $this->saveAuthKeys($client_id, $oauth_new_token, $oauth_token_secret, 1, $org, '', "", $session_handle, $expires,$client_master_id);
                         $responseData['XeroOAuth'] = $XeroOAuth;
                         $responseData['error'] = false;
                         unset($_SESSION['oauth']);
                     } else {
                         $responseData['error'] = true;
                         $responseData['message'] = $XeroOAuth->response['response'];
                     }
                 }
                 else{
                    $responseData['error'] = true;
                    $responseData['message'] = "You are already connected ".date("Y-m-d h:i:s");
                }
             }
             return $responseData;
         }
     }

     public function saveAuthKeys($client_id, $oauth_token, $oauth_token_secret, $is_authorized, $org, $req_oauth_token, $page_name, $session_handle = '', $expires = '', $client_master_id)
     {
         try {
             $this->conn->autocommit(false);
             $date = date("Y-m-d H:i:s");
             $is_active = 1;
             $response = array();
             
             $tokenResult = $this->getOauthTokenByClientMaster($client_id, $client_master_id);
             if ($tokenResult["error"] == true) {
                 $sql = "INSERT INTO client_oauth_token(client_id, oauth_token, oauth_token_secret, generated_date, is_authorized, req_oauth_token, page_name, session_handle, expires, client_master_id) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                 $e = 1;
             } else {
                 if (strlen(trim($page_name)) <= 0) {
                     $sql = "UPDATE client_oauth_token set oauth_token = ?, oauth_token_secret = ?, is_authorized = ?, generated_date = ?, org = ?, session_handle = ?, expires = ? where client_id = ? AND  client_master_id = ?";
                 } else {
                     $sql = "UPDATE client_oauth_token set oauth_token = ?, oauth_token_secret = ?, is_authorized = ?, generated_date = ?, org = ?, page_name = ?, session_handle = ?, expires = ? where client_id = ? AND  client_master_id = ?";
                 }
                 $e = 2;
             }
             if ($stmt = $this->conn->prepare($sql)) {
                 if ($e == 1) {
                     $stmt->bind_param("isssissssi", $client_id, $oauth_token, $oauth_token_secret, $date, $is_authorized, $req_oauth_token, $page_name, $session_handle, $expires, $client_master_id);
                 } else {
                     if (strlen(trim($page_name)) <= 0) {
                         $stmt->bind_param("ssissssii", $oauth_token, $oauth_token_secret, $is_authorized, $date, $org, $session_handle, $expires, $client_id, $client_master_id);
                     } else {
                         $stmt->bind_param("ssisssssii", $oauth_token, $oauth_token_secret, $is_authorized, $date, $org, $page_name, $session_handle, $expires, $client_id, $client_master_id);
                     }
                 }
                 $result = $stmt->execute();

                 $stmt->close();
                 if ($result) {
                     $this->conn->commit();
                     $response["error"] = false;
                     $response["message"] = KEY_GENERATE_SUCCESS;

                 } else {
                     $response["error"] = true;
                     $response["message"] = KEY_GENERATE_FAILURE;
                 }
             } else {
                 $response["error"] = true;
                 $response["message"] = QUERY_EXCEPTION;
             }

             return $response;
         } catch (Exception $e) {
             $this->conn->rollback();
             echo $e->getMessage();
         }
     }

    public function updateAuthorized($client_id, $is_authorized)
    {
        try {
            $this->conn->autocommit(false);
            $date = date("Y-m-d h:i:s");
            $is_authorized = 1;
            $response = array();
            $sql = "UPDATE client_oauth_token set is_authorized = ?, generated_date = ? where client_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("isi", $is_authorized, $date, $client_id);
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            $result = $stmt->execute();

            $stmt->close();
            if ($result) {
                $this->conn->commit();
                $response["error"] = false;
                $response["message"] = UPDATE_AUTHORIZE_SUCCESS;

            } else {
                $response["error"] = true;
                $response["message"] = UPDATE_AUTHORIZE_FAILURE;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function requestToken($client_id, $data, $page_name)
    {
        try {
            $response = array();

            if (isset($data['XeroOAuth'])) {
                $here = XeroOAuth::php_self();
                $XeroOAuth = $data['XeroOAuth'];
                $params = array(
                    'oauth_callback' => OAUTH_CALLBACK,
                );

                $response = $this->makeRequest($client_id, 'requestToken', $XeroOAuth, 'GET', $XeroOAuth->url('RequestToken', ''), $params);
                if ($XeroOAuth->response['code'] == 302) {
                    $response["error"] = true;
                    $response["message"] = XERO_HAVING_PROBLEM;
                    return $response;
                }
                if ($XeroOAuth->response['code'] == 200) {
                    $scope = "";
                    $xResponse = array();
                    $xResponse = $XeroOAuth->extract_params($XeroOAuth->response['response']);
                    $oauth_token = $xResponse['oauth_token'];
                    $oauth_token_secret = $xResponse['oauth_token_secret'];
                    $resp = array();
                    $response = $this->saveAuthKeys($client_id, $oauth_token, $oauth_token_secret, 0, "", $oauth_token, $page_name);
                    if ($response['error'] == false) {
                        $authurl = $XeroOAuth->url("Authorize", '') . "?oauth_token={$oauth_token}&scope=" . $scope;
                        $response['authurl'] = $authurl;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = $XeroOAuth;
                }
            } else {
                $response = $data;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function array_flatten($array)
    {
        $return = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {$return = array_merge($return, $this->array_flatten($value));} else { $return[$key] = $value;}
        }
        return $return;
    }

    public function getBudgetSummary($client_id, $XeroOAuth, $ajax, $client_master_id)
    {
        try {
            $budgetSummary = array();
            $response = $this->makeRequest($client_id, 'getBudgetSummary', $XeroOAuth, 'GET', BUDGET_SUMMARY_API, array("periods" => 1), "", "json",$client_master_id);
            $budgetResponse = $XeroOAuth->response;
            if ($budgetResponse['code'] == 302) {
                $budgetResponse["error"] = true;
                $budgetResponse["message"] = XERO_HAVING_PROBLEM;
                return $budgetResponse;
            }
            if ($budgetResponse['code'] == 401) {
                if (strpos($budgetResponse['response'], 'token_expired') !== false || strpos($budgetResponse['response'], 'token_rejected') !== false) {
                    if (isset($ajax) && ($ajax == true)) {
                        $budgetSummary["expiry"] = true;
                        $budgetSummary['error'] = true;
                        $budgetSummary['message'] = TOKEN_EXPIRED;
                        return $budgetSummary;
                    } else {
                        $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                        if (isset($resp['authurl'])) {
                            $authurl = $resp['authurl'];
                            header('Location:' . $authurl);die();
                        }
                    }
                }
            }
            if ($budgetResponse['code'] == 200) {
                $aSummaryList = array();
                $budgetDetails = array();

                $aSummaryList = json_decode($XeroOAuth->response['response'], true);
                $company_name = $aSummaryList['Reports'][0]['ReportTitles'][1];
                $budgetDetails = $aSummaryList['Reports'][0]['Rows'];
                if (sizeof($budgetDetails) > 0) {
                    foreach ($budgetDetails as $k => $v) {
                        if ($v["RowType"] == "Section") {
                            if (sizeof($v["Rows"]) > 0) {
                                if (($v["Rows"][0]["RowType"] == "SummaryRow") && (sizeof($v["Rows"][0]["Cells"]) > 0)) {
                                    $key = $v["Rows"][0]["Cells"][0]["Value"];
                                    $value = $v["Rows"][0]["Cells"][1]["Value"];
                                    $key = str_replace(" ", "_", strtolower($key));
                                    $tmp[$key] = $value;
                                }
                            }
                        }
                    }
                }
                $budgetSummary["budgetSummary"] = $tmp;
                $budgetSummary["budgetSummary"]["org_name"] = $company_name;
                $budgetSummary["error"] = false;
                $budgetSummary["message"] = RECORD_FOUND;
            }

            $aSummaryList = null;
            $budgetDetails = null;
            return $budgetSummary;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getContacts($client_id, $XeroOAuth, $client_master_id)
    {
        try {
            $contactList = array();
            $rresponse = $this->makeRequest($client_id,'getContacts',$XeroOAuth,'GET', $XeroOAuth->url('Contacts', 'core'), array(), "", "json");
            $response = $XeroOAuth->response;    
            if ($response['code'] == 302) {
                $response["error"] = true;
                $response["message"] = XERO_HAVING_PROBLEM;
                return $response;
            }
            if ($response['code'] == 401) {
                if (strpos($response['response'], 'token_expired') !== false || strpos($response['response'], 'token_rejected') !== false) {
                    $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                    if (isset($resp['authurl'])) {
                        $authurl = $resp['authurl'];
                        header('Location:' . $authurl);die();
                    }
                }
            }
            if ($response['code'] == 200) {
                $contacts = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
                $contactList = json_decode($XeroOAuth->response['response'], true);
            }
            return $contactList;

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getTrackingCategoriesFromXero($client_id, $XeroOAuth, $ajax, $client_master_id)
    {
        try {
            $territoryList = array();
            $categories = array();

            $rresponse = $this->makeRequest($client_id, 'getTrackingCategoriesFromXero', $XeroOAuth, 'GET', $XeroOAuth->url('TrackingCategories', 'core'), array('IsActive' => 1), "", "json",$client_master_id);
            $response = $XeroOAuth->response;   
            if ($response['code'] == 302) {
                $response["error"] = true;
                $response["message"] = XERO_HAVING_PROBLEM;
                return $response;
            } else if ($response['code'] == 401) {
                if (strpos($response['response'], 'token_expired') !== false || strpos($response['response'], 'token_rejected') !== false) {
                    if (isset($ajax) && ($ajax == true)) {
                        $categories["expiry"] = true;
                        $categories['error'] = true;
                        $categories['message'] = TOKEN_EXPIRED;
                        return $categories;
                    } else {
                        $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                        if (isset($resp['authurl'])) {
                            $authurl = $resp['authurl'];
                            header('Location:' . $authurl);die();
                        }
                    }
                }
            } else if ($response['code'] == 200) {
                $territoryList = json_decode($XeroOAuth->response['response'], true);
                if (sizeof($territoryList) > 0) {
                    $resp = array();
                    // $resp = $this->insertTrackingCategories($client_id, $territoryList['TrackingCategories'], $client_master_id);
                    $resp = $this->InsertTrackingCategoriesToDB($client_id, $territoryList['TrackingCategories'], $client_master_id);
                    
                    // print_r($territoryList); exit;
                    $categories['territorybrand'] = $territoryList['TrackingCategories'];
                    $categories["error"] = false;
                    $categories["message"] = RECORD_FOUND;
                }
            }
            return $categories;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    public function InsertTrackingCategoriesToDB($client_id, $categories, $client_master_id){
        $sizeofCategoryCount = sizeof($categories);
        if ($sizeofCategoryCount == 0) {
        $categories[] = array("Name" => "All", "TrackingCategoryID" => "All", "Options" => array());
        } else {
            $categories[] = array("Name" => "All", "TrackingCategoryID" => "All", "Options" => array(array("Name" => "All", "TrackingOptionID" => BRAND_ID)));
        }
        $categories_ids =[]; 
        foreach($categories  as $category){
           
            $categories_ids[] = $category['TrackingCategoryID'];
            $category_id = $category['TrackingCategoryID'];
            $is_active = 1;
            
            $sql = "SELECT count(*) FROM client_category_master WHERE client_id = ".$client_id." AND client_master_id = ".$client_master_id." AND category_id = '".$category_id."'";
                    // echo $sql; exit;
            $stmt = $this->conn->prepare($sql);
            // $stmt->bind_param("iis", $client_id, $client_master_id,$category['TrackingCategoryID']);
            if ($stmt->execute()) {
                $stmt->bind_result($count);
               while ($stmt->fetch()) {
                    $ss["count"] = $count;
                }
                $stmt->close();

                // echo $count; exit;
                /* check if doesn't exist insert as new record */
                if ($count == 0) {
                 $insertSQLQuery = "INSERT INTO client_category_master (client_id, category_id, category_name, is_active, client_master_id) VALUES (?,?,?,?,?)";       
                 $istmt = $this->conn->prepare($insertSQLQuery);
                 $istmt->bind_param('issii', $client_id, $category_id, $category['Name'], $is_active, $client_master_id);
                 $result = $istmt->execute();
                 $istmt->close();
                 $client_category_id = $this->conn->insert_id;
                }else{
                    // echo 'jj';
                    $select_sql_category = "SELECT client_category_id FROM client_category_master WHERE client_id = ? AND client_master_id = ? AND category_id = ? ";
                    // $select_sql_category = "SELECT client_category_id FROM client_category_master WHERE client_id = ".$client_id." AND client_master_id = ".$client_master_id." AND category_id =  '".$category_id."'";
                    $categstmt = $this->conn->prepare($select_sql_category);
                    $categstmt->bind_param("iis", $client_id, $client_master_id, $category_id);
                    // print_r($categstmt);
                    $categstmt->execute();
                    $categstmt->bind_result($clients_category_id);
                    while ($categstmt->fetch()) {
                        $client_category_id = $clients_category_id;
                    }
                    $categstmt->close();

                }
                /* Insert subcategories if category has any subcategory  */
            $subcategories_ids = [];
            if (count($category['Options']) > 0) {
                foreach ($category['Options'] as $subcategory) {
                    $subcategory_id = $subcategory['TrackingOptionID'];
                    $subcategories_ids[$category_id][] = $subcategory['TrackingOptionID'];
                    // $subcategory_sql = "SELECT count(*) FROM category_subcategory WHERE client_category_id = ? AND sub_category_id = ? ";
                    $subcategorys_sql = "SELECT count(*) FROM category_subcategory WHERE client_category_id =  ". $client_category_id." AND sub_category_id = '".$subcategory_id."' ";
                    // echo $subcategorys_sql."<br>";
                    $sstmt = $this->conn->prepare($subcategorys_sql);
                    // $sstmt->bind_param('is', $client_category_id, $subcategory_id);
                    $sstmt->execute();
                    $sstmt->bind_result($scount);
                    while ($sstmt->fetch()) {
                      $scs["scount"] = $scount;
                    }
                    $sstmt->close();
                    // echo $scount; exit;
                    if ($scount == 0) {
                        $this->conn->autocommit(false);
                        // $sinsertSQLQuery = "INSERT INTO category_subcategory (client_category_id, sub_category_id,sub_category_name, is_active) VALUES (?,?,?,?)";
                        $sinsertSQLQuerys = "INSERT INTO category_subcategory (client_category_id, sub_category_id,sub_category_name, is_active) VALUES ($client_category_id, '".$subcategory_id."', '".$subcategory['Name']."', $is_active)";
                        // echo $sinsertSQLQuerys ."</br>"; 
                        $sistmt = $this->conn->prepare($sinsertSQLQuerys);
                        // $sistmt->bind_param('issi', $client_category_id, $subcategory_id, $subcategory['Name'], $is_active);
                        $sresult = $sistmt->execute();
                        $sistmt->close();
                        $category_sub_id = $this->conn->insert_id;
                         $result = $this->insertTrackClientMap($client_id, $category_sub_id, $client_master_id);
                        $this->conn->commit();
                    }

                }
                if(count($subcategories_ids[$category_id]) > 0){
                    $subcategories_ids[$category_id][] = BRAND_ID;
                   $sub_ids = implode("','", $subcategories_ids[$category_id]);
                //    $select_subcategory_sql = "DELETE FROM category_subcategory WHERE client_category_id = ? AND sub_category_id NOT IN (?)";
                   $selects_subcategory_sql = "DELETE FROM category_subcategory WHERE client_category_id = '".$client_category_id."' AND sub_category_id NOT IN ('".$sub_ids."')";
                //    echo $selects_subcategory_sql ."<br>";
                   $ssstmt = $this->conn->prepare($selects_subcategory_sql);
                //    $ssstmt->bind_param('is', $client_category_id, $sub_ids);
                   $ssstmt->execute();
                   $ssstmt->close();
                }

            }

            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

        }
         if(count($categories_ids) > 0){
             $cat_ids = implode("','", $categories_ids);
            //  $select_category_sql = "DELETE FROM client_category_master WHERE client_id = ? AND client_master_id = ? AND category_id NOT IN (?)";
             $selects_category_sql = "DELETE FROM client_category_master WHERE client_id = ".$client_id." AND client_master_id = ".$client_master_id." AND category_id NOT IN ('".$cat_ids."')";
            // echo $selects_category_sql;
             $cstmt = $this->conn->prepare($selects_category_sql);
             $cstmt->bind_param('iis', $client_id,$client_master_id, $cat_ids);
             $cstmt->execute();
             $cstmt->close();
         }

    }
    public function insertTrackingCategories($client_id, $categories, $client_master_id)
    {
        try {
            $sizeofCategoryCount = sizeof($categories);
            $isExist = $this->checkCategoryExistByClientID($client_id, $client_master_id);

            if ($isExist["error"] == true) {
                $is_active = 1;
                $this->conn->autocommit(false);
                if ($sizeofCategoryCount == 0) {
                    $categories[] = array("Name" => "All", "TrackingCategoryID" => "All", "Options" => array());
                } else {
                    $categories[] = array("Name" => "All", "TrackingCategoryID" => "All", "Options" => array(array("Name" => "All", "TrackingOptionID" => BRAND_ID)));
                }

                foreach ($categories as $k => $v) {
                    $category_id = $v["TrackingCategoryID"];
                    $sql = 'INSERT INTO client_category_master (client_id, category_id, category_name, is_active, client_master_id) VALUES (?,?,?,?,?)';
                    if ($stmt = $this->conn->prepare($sql)) {
                        $stmt->bind_param('issii', $client_id, $category_id, $v['Name'], $is_active, $client_master_id);
                        $result = $stmt->execute();
                        $stmt->close();
                        if ($result) {
                            $client_category_id = $this->conn->insert_id;

                            if ($sizeofCategoryCount == 0) {
                                $childResult = $this->insertSubCategories($client_id, $client_category_id, $v["Options"], $client_master_id);
                            } else if ($v["Name"] != "All") {
                                $childResult = $this->insertSubCategories($client_id, $client_category_id, $v["Options"], $client_master_id);
                            }

                            if ($childResult["error"] == false) {
                                $this->conn->commit();
                                $response["error"] = false;
                                $response["message"] = ADD_CATEGORY_SUCCESS;
                            } else {
                                $response["error"] = true;
                                $response["message"] = ADD_CATEGORY_FAILURE;
                            }
                        } else {
                            $response["error"] = true;
                            $response["message"] = ADD_CATEGORY_FAILURE;
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = QUERY_EXCEPTION;
                    }
                }
            } else {
                $response["error"] = true;
                $response["message"] = RECORD_FOUND;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function insertLocalTrackingCategories($catData, $client_id, $client_master_id)
    {
        try {
            $isExist = $this->checkCategoryExistByClientID($client_id, $client_master_id);
            if ($isExist["error"] == true) {
                $is_active = 1;
                foreach ($catData as $value) {
                    $catID = $value['catId'];
                    $catName = $value['catName'];
                    $client_catID = $value['client_catID'];

                    $sql = "INSERT INTO client_category_master (client_id, category_id, category_name, is_active, client_master_id) VALUES ($client_id, '$catID', '$catName', $is_active, $client_master_id)";

                    if ($stmt = $this->conn->prepare($sql)) {
                        $result = $stmt->execute();
                        $stmt->store_result();
                        if ($result) {
                            $client_category_id1 = $this->conn->insert_id;
                            $sql1 = "SELECT cs.sub_category_id, cs.sub_category_name FROM category_subcategory as cs WHERE client_category_id = $client_catID";
                            if ($stmt1 = $this->conn->prepare($sql1)) {
                                $stmt1->execute();
                                $stmt1->store_result();
                                if ($stmt1->num_rows > 0) {
                                    $stmt1->bind_result($sub_category_id, $sub_category_name);
                                    $subData = array();
                                    while ($result = $stmt1->fetch()) {
                                        $subData[] = array('catId' => $client_category_id1, 'subId' => $sub_category_id, 'subName' => $sub_category_name);
                                    }

                                    $response["error"] = false;
                                } else {
                                    $response["error"] = true;
                                    $response['result'] = NO_COMPANY_FOUND;
                                }
                            } else {
                                $response["error"] = true;
                                $response["message"] = QUERY_EXCEPTION;
                            }

                            if ($childResult["error"] == false) {
                                $response["error"] = false;
                                $response["message"] = ADD_CATEGORY_SUCCESS;
                            } else {
                                $response["error"] = true;
                                $response["message"] = ADD_CATEGORY_FAILURE;
                            }
                        } else {
                            $response["error"] = true;
                            $response["message"] = ADD_CATEGORY_FAILURE;
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = QUERY_EXCEPTION;
                    }
                }

            } else {
                $response["error"] = true;
                $response["message"] = RECORD_FOUND;
            }
            $response["data"] = $subData;
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function insertSubCategories($client_id, $client_category_id, $subCategories, $client_master_id)
    {
        try {
            $response = array();
            $is_active = 1;
            $this->conn->autocommit(false);
            $subCategories[] = array("Name" => "All", "TrackingOptionID" => BRAND_ID);
            $childSQL = "INSERT INTO category_subcategory (client_category_id, sub_category_id, sub_category_name, is_active) VALUES (?,?,?,?)";
            foreach ($subCategories as $k1 => $v1) {
                if ($childStmt = $this->conn->prepare($childSQL)) {
                    $childStmt->bind_param("issi", $client_category_id, $v1["TrackingOptionID"], $v1["Name"], $is_active);
                    $result = $childStmt->execute();
                    $childStmt->close();
                    if ($result) {
                        $category_sub_id = $this->conn->insert_id;
                        $result = $this->insertTrackClientMap($client_id, $category_sub_id, $client_master_id);
                        if ($result["error"] == false) {
                            $this->conn->commit();
                            $response["error"] = false;
                            $response["message"] = ADD_SUB_CATEGORY_SUCCESS;
                        } else {
                            $response["error"] = true;
                            $response["message"] = ADD_SUB_CATEGORY_FAILURE;
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = ADD_SUB_CATEGORY_FAILURE;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function insertLocalSubCategories($client_id, $subData, $client_master_id)
    {
        try {
            $response = array();
            $is_active = 1;
            foreach ($subData['data'] as $value) {

                $catId = $value['catId'];
                $subId = $value['subId'];
                $subName = $value['subName'];

                $childSQL = "INSERT INTO category_subcategory (client_category_id, sub_category_id, sub_category_name, is_active) VALUES ($catId, '$subId', '$subName', $is_active)";

                if ($childStmt = $this->conn->prepare($childSQL)) {
                    $result = $childStmt->execute();
                    $childStmt->store_result();
                    if ($result) {
                        // $category_sub_id = $this->conn->insert_id;
                        // $result = $this->insertTrackClientMap($client_id, $category_sub_id, $client_master_id);
                        if ($result["error"] == false) {
                            $response["error"] = false;
                            $response["message"] = ADD_SUB_CATEGORY_SUCCESS;
                        } else {
                            $response["error"] = true;
                            $response["message"] = ADD_SUB_CATEGORY_FAILURE;
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = ADD_SUB_CATEGORY_FAILURE;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function insertTrackClientMap($client_id, $category_sub_id, $client_master_id)
    {
        try {
            $response = array();
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            $childSQL = 'INSERT INTO track_client_sub_category (client_id, category_sub_id, created_date, client_master_id) VALUES (?,?,?,?)';
            if ($childStmt = $this->conn->prepare($childSQL)) {
                $childStmt->bind_param('iisi', $client_id, $category_sub_id, $date, $client_master_id);
                $result = $childStmt->execute();
                $childStmt->close();
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    $response["message"] = ADD_SUB_CATEGORY_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = ADD_SUB_CATEGORY_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function countCategorySubIDs($client_id)
    {
        try {
            $response = array();
            if ($stmt = $this->conn->prepare("SELECT client_category_id, count(*) as count_values FROM category_subcategory cs JOIN track_client_sub_category tc ON cs.category_sub_id = tc.category_sub_id where tc.client_id = ? and is_active = 1 group by client_category_id")) {
                $stmt->bind_param("i", $client_id);
                $stmt->execute();
                $stmt->bind_result($client_category_id, $count_values);
                $stmt->store_result();
                $num_rows = $stmt->num_rows;

                if ($num_rows > 0) {
                    while ($result = $stmt->fetch()) {
                        $response["category_values"][$client_category_id] = $count_values;
                    }
                    $response["error"] = false;
                    $response["message"] = USER_RECORD_FOUND;

                } else {
                    $response["error"] = true;
                    $response["message"] = USER_DOES_NOT_EXIST;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getCategorySubIDForAll($client_category_id)
    {
        try {
            $response = array();
            if ($stmt = $this->conn->prepare("SELECT category_sub_id FROM category_subcategory  where client_category_id = ? and is_active = 1 AND sub_category_id = 1000")) {
                $stmt->bind_param("i", $client_category_id);
                $stmt->execute();
                $stmt->bind_result($category_sub_id);
                $stmt->store_result();
                $num_rows = $stmt->num_rows;
                if ($num_rows > 0) {
                    while ($result = $stmt->fetch()) {
                        $response["category_sub_id"] = $category_sub_id;
                    }
                    $response["error"] = false;
                    $response["message"] = USER_RECORD_FOUND;

                } else {
                    $response["error"] = true;
                    $response["message"] = USER_DOES_NOT_EXIST;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function findIfAllExist($client_id, $category_sub_ids)
    {
        try {
            $resp = array();
            $findMasterValues = array();
            $findChildValues = array();
            $resp = $this->getFieldByID("client_master", 'client_id', $client_id, 'client_master_id');
            $client_master_id = $resp["field_key"];
            $resp = $this->getSuperAdminID($client_master_id);

            $searchItems = array();
            $searchItems['client_id'] = $client_id;
            $searchItems['client_master_id'] = $client_master_id;
            $searchItems["groupby"] = 1;
            $catResponse = $this->getAllTrackingCategories($searchItems);

            $findMasterValues = $this->countCategorySubIDs($resp["client_id"]);

            if ($findMasterValues["error"] == false) {
                $findChildValues = $this->countCategorySubIDs($client_id);
                if ($findChildValues["error"] == false) {
                    foreach ($findMasterValues["category_values"] as $k => $v) {
                        if (isset($findChildValues["category_values"][$k]) && $findChildValues["category_values"][$k] == $v - 1) {
                            $response = $this->getCategorySubIDForAll($k);
                        }
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = NO_RECORD_FOUND;
            }

            // find client master id
            // find count of category_sub_ids group by client_category_id  for master  where sub_category_id != 1000
            // find count of category_sub_ids group by client_category_id  for this client id  where sub_category_id != 1000
            // if these two match, then insert ALL for client

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function setSubCategoriesForClientUsers($client_id, $category_sub_ids, $client_master_id)
    {
        try {

            $response = array();
            $response = $this->deleteTrackClientMap($client_id, $client_master_id);
            if ($response["error"] == false) {
                if (sizeof($category_sub_ids) > 0) {
                    foreach ($category_sub_ids as $k => $v) {
                        if (is_numeric($v)) {
                            $response = $this->insertTrackClientMap($client_id, $v, $client_master_id);
                        }
                    }
                }
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function deleteTrackClientMap($client_id, $client_master_id)
    {
        try {
            $response = array();
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            $childSQL = 'DELETE FROM track_client_sub_category WHERE client_id = ? and client_master_id = ?';
            if ($childStmt = $this->conn->prepare($childSQL)) {
                $childStmt->bind_param('ii', $client_id, $client_master_id);
                $result = $childStmt->execute();
                $childStmt->close();
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    $response["message"] = DELETE_SUB_CATEGORY_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = DELETE_SUB_CATEGORY_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getTrackMetricReceivables($client_id, $XeroOAuth, $ajax, $params, $currencyResult)
    {
        try {
            $dt = new DateTime();
            $aReceivableListing = array();
            $aReceivables = array();
            $aDates = array();
            $amountDue = array();
            $amountPaid = array();
            $res = $this->getFieldByID('client_master', 'client_id', $client_id, 'client_master_id');
            if ($res["error"] == false) {
                $client_master_id = $res['field_key'];
                $result = $this->getFieldByID('client_master_detail', 'client_master_id', $client_master_id, 'report_start_date');
            }

            $getDates = $this->getDayTillMonth($client_id);
            if ($getDates['isAdmin'] == 1) {
                foreach ($getDates as $k => $v) {
                    if (is_numeric($k) && $k != 0) {
                        $aDates[$k] = $v;
                    }
                }

                foreach ($aDates as $key => $val) {
                    $date = new DateTime($aDates[$key]["lastdate"]);
                    $date->modify('FIRST DAY OF -1 MONTH');
                    $lastDate = $date->format('Y,m,t');

                    if ($lastDate >= date('Y,m,t', strtotime($result["field_key"]))) {
                        $date->modify('FIRST DAY OF -2 MONTH');
                        $firstDate = $date->format('Y,m,01');

                        if ($firstDate <= date('Y,m,01', strtotime($result["field_key"]))) {
                            $firstDate = date('Y,m,01', strtotime($result["field_key"]));
                        }
                        $response = $this->makeRequest($client_id, 'getTrackMetricReceivables', $XeroOAuth, 'GET', INVOICE_API, array('Where' => 'Type=="ACCREC" AND Date >= DateTime(' . $firstDate . ') AND Date <= DateTime(' . $lastDate . ')'), "", "json");
                        $receiveResponse = $XeroOAuth->response;

                        if ($receiveResponse['code'] == 302) {
                            $receiveResponse["error"] = true;
                            $receiveResponse["message"] = XERO_HAVING_PROBLEM;
                            return $receiveResponse;
                        }
                        if ($receiveResponse['code'] == 401) {
                            if (strpos($receiveResponse['response'], 'token_expired') !== false || strpos($receiveResponse['response'], 'token_rejected') !== false) {
                                if (isset($ajax) && ($ajax == true)) {
                                    $aReceivableListing["expiry"] = true;
                                    $aReceivableListing['error'] = true;
                                    $aReceivableListing['message'] = TOKEN_EXPIRED;
                                    return $aReceivableListing;
                                } else {
                                    $resp = $this->XeroRefreshToken($client_id,$client_master_id);
                                    if (isset($resp['authurl'])) {
                                        $authurl = $resp['authurl'];
                                        header('Location:' . $authurl);die();
                                    }
                                }
                            }
                        }
                        if ($receiveResponse['code'] == 200) {
                            $receivableList = array();
                            $reportStartDate = array();
                            $receivableList = json_decode($XeroOAuth->response['response'], true);

                            if (sizeof($receivableList['Invoices']) > 0) {
                                $receivables = $receivableList['Invoices'];
                                $months = array();
                                foreach ($receivables as $k => $v) {
                                    $monthText = date("n", strtotime($v['DateString']));
                                    $amountDue[$monthText] += $v['AmountDue'];
                                    $amountPaid[$monthText] += $v['AmountPaid'];
                                    $totalReceivables[$key] += $v["AmountDue"];
                                }
                            } else {
                                $amountDue[$monthText] += 0;
                                $amountPaid[$monthText] += 0;
                                $totalReceivables[$key] += 0;
                            }
                            $aReceivables["amountDue"] = $amountDue;
                            $aReceivables["amountPaid"] = $amountPaid;
                        }
                    }
                }
            }

            $aReceivableListing["agedReceivables1"] = $aReceivables;
            $aReceivableListing['error'] = false;
            $aReceivableListing['message'] = RECORD_FOUND;

            $receivableList = null;
            $receiveResponse = null;

            return $aReceivableListing;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function updateCacheLog($client_id, $metrics)
    {
        try {
            $loginResult = $this->getFieldByID('client_cache_log', 'client_id', $client_id, 'last_updated_date');
            if ($loginResult["error"] == true) {
                if ($metrics == true) {
                    $sql = "INSERT into client_cache_log(metrics_last_updated_date, client_id) VALUES(?,?)";
                } else {
                    $sql = "INSERT into client_cache_log(last_updated_date, client_id) VALUES(?,?)";
                }

            } else {
                if ($metrics == true) {
                    $sql = "UPDATE client_cache_log set metrics_last_updated_date = ?  WHERE client_id = ? ";
                } else {
                    $sql = "UPDATE client_cache_log set last_updated_date = ?  WHERE client_id = ? ";
                }
            }

            $this->conn->autocommit(false);
            $date = date("Y-m-d H:i:s");
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("si", $date, $client_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = UPDATE_CACHE_LOG_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = UPDATE_CACHE_LOG_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function updateLastLogin($client_id)
    {
        try {
            $this->conn->autocommit(false);
            $date = date('Y-m-d');
            if ($stmt = $this->conn->prepare("UPDATE client_master set last_login = ?  WHERE client_id = ? ")) {
                $stmt->bind_param("si", $date, $client_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = LOGIN_COUNT_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = LOGIN_COUNT_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function updateXeroAuthenticated($client_id, $is_authenticated)
    {
        try {
            $this->conn->autocommit(false);
            $date = date('Y-m-d');
            if ($stmt = $this->conn->prepare("UPDATE client_oauth_token set is_authenticated = ?, is_authorized = ?  WHERE client_id = ? ")) {
                $stmt->bind_param("iii", $is_authenticated, $is_authenticated, $client_id);
                // $result = $stmt->execute();
                // $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = LOGIN_COUNT_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = LOGIN_COUNT_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getSalesReport($client_id, $report_id, $client_master_id)
    {
        try {
            $a = array();
            $b = array();
            $c = array();
            $finalResponse = array();

            $db = new reportClientApplicationMap();
            $searchItems['client_id'] = $client_id;
            $searchItems['report_id'] = $report_id;
            $searchItems['auth_key'] = "";
            $response = $db->getAllReportClientApplicationMaps($searchItems);

            if (sizeof($response) > 0) {
                if ($response["error"] == false) {
                    $currencyResult = array();
                    $currencyResult = $this->getFieldByID('client_master_detail', 'client_master_id', $client_master_id, 'default_currency');

                    $application_id = $response["reportClientApplicationMapDetails"][0]["application_id"];
                    $a[] = $this->getWeightedPipeLine($response["reportClientApplicationMapDetails"], $client_id, $application_id, $currencyResult);
                    $b[] = $this->getSalesOrders($response["reportClientApplicationMapDetails"], $client_id, $application_id, $currencyResult, $client_master_id);
                    if ((sizeof($a) > 0) && sizeof($b) > 0) {
                        if ($a[0]["error"] == false && $b[0]["error"] == false) {
                            $c[] = $this->getTotalOfSales($a, $b);
                        } else {
                            $c["error"] = true;
                            $c["message"] = SERVER_NO_RESPONSE;
                            return $c;
                        }
                    } else {
                        $c["error"] = true;
                        $c["message"] = SERVER_NO_RESPONSE;
                        return $c;
                    }

                    $finalResponse = array_merge($a, $b, $c);
                }
            }

            return $finalResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function insertBudgetForClient($client_master_id, $client_category_id1, $sub_category_id1, $client_category_id2, $sub_category_id2, $csvData)
    {
        try {
            $delResp = array();
            $response = array();
            $name = $csvData['name'];
            $size = $csvData['size'];
            $file = $csvData['tmp_name'];
            $ext = $this->getExtension($name);
            $valid_formats = array("csv", "CSV");
            if (strlen(trim($sub_category_id2)) <= 0) {
                $sub_category_id2 = "";
            }
            if (strlen($name) > 0) {
                if (in_array($ext, $valid_formats)) {
                    $delResp = $this->deleteBudget($client_master_id, $sub_category_id1, $sub_category_id2);
                    if ($delResp["error"] == false) {
                        $this->conn->autocommit(false);
                        $handle = fopen($file, "r");
                        $c = 0;
                        $firstRow = true;
                        while (($filesop = fgetcsv($handle, 1000, ",")) !== false) {
                            if ($firstRow) {
                                $firstRow = false;
                            } else {
                                $line_type = trim($filesop[0]);
                                $item_head = trim($filesop[1]);
                                $line_item = "";
                                if ($filesop[2] != "") {
                                    $line_item = trim(preg_replace('/[(][^)]*[)]([^(]*$)/', '$2', $filesop[2]));
                                }
                                $month_jan = (trim($filesop[3]) != "") ? trim($filesop[3]) : '';
                                $month_feb = (trim($filesop[4]) != "") ? trim($filesop[4]) : '';
                                $month_march = (trim($filesop[5]) != "") ? trim($filesop[5]) : '';
                                $month_april = (trim($filesop[6]) != "") ? trim($filesop[6]) : '';
                                $month_may = (trim($filesop[7]) != "") ? trim($filesop[7]) : '';
                                $month_june = (trim($filesop[8]) != "") ? trim($filesop[8]) : '';
                                $month_july = (trim($filesop[9]) != "") ? trim($filesop[9]) : '';
                                $month_aug = (trim($filesop[10]) != "") ? trim($filesop[10]) : '';
                                $month_sept = (trim($filesop[11]) != "") ? trim($filesop[11]) : '';
                                $month_oct = (trim($filesop[12]) != "") ? trim($filesop[12]) : '';
                                $month_nov = (trim($filesop[13]) != "") ? trim($filesop[13]) : '';
                                $month_dec = (trim($filesop[14]) != "") ? trim($filesop[14]) : '';
                                $total_values = (trim($filesop[15]) != "") ? trim($filesop[15]) : '';
                                $sql = "INSERT INTO client_budget (client_master_id, client_category_id1, sub_category_id1, client_category_id2, sub_category_id2, line_type, item_head, line_item, month_jan, month_feb, month_march, month_april, month_may, month_june, month_july, month_aug, month_sept, month_oct, month_nov, month_dec, total_values) VALUES (?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                                if ($stmt = $this->conn->prepare($sql)) {
                                    $stmt->bind_param("iisisssssssssssssssss", $client_master_id, $client_category_id1, $sub_category_id1, $client_category_id2, $sub_category_id2, $line_type, $item_head, $line_item, $month_jan, $month_feb, $month_march, $month_april, $month_may, $month_june, $month_july, $month_aug, $month_sept, $month_oct, $month_nov, $month_dec, $total_values);
                                    $result = $stmt->execute();
                                    $stmt->close();
                                    if ($result) {
                                        $this->conn->commit();
                                        $response["error"] = false;
                                        $response["message"] = BUDGET_IMPORT_SUCCESS;

                                    } else {
                                        $response["error"] = true;
                                        $response["message"] = BUDGET_IMPORT_FAILURE;
                                    }
                                } else {
                                    $response["error"] = true;
                                    $response["message"] = QUERY_EXCEPTION;
                                }
                            }
                        }
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = INVALID_FORMAT;
                }
            } else {
                $response["error"] = true;
                $response["message"] = NO_FILE_SELECTED;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function deleteBudget($client_master_id, $sub_category_id1, $sub_category_id2)
    {
        try {
            $response = array();
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            if (strlen(trim($sub_category_id2)) > 0) {
                $sql = "DELETE FROM client_budget  WHERE client_master_id = ? AND sub_category_id1 = ? AND sub_category_id2 = ?";
            } else {
                $sql = "DELETE FROM client_budget  WHERE client_master_id = ? AND sub_category_id1 = ?";
            }
            if ($stmt = $this->conn->prepare($sql)) {
                if (strlen(trim($sub_category_id2)) > 0) {
                    $stmt->bind_param("iss", $client_master_id, $sub_category_id1, $sub_category_id2);
                } else {
                    $stmt->bind_param("is", $client_master_id, $sub_category_id1);
                }
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = DELETE_BUDGET_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = DELETE_BUDGET_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function hardDeleteBudget($client_master_id)
    {
        try {
            $response = array();
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            $sql = "DELETE FROM client_budget  WHERE client_master_id = ? ";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("i", $client_master_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = DELETE_BUDGET_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = DELETE_BUDGET_FAILURE;

                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getAllTrackingCategories($searchItems)
    {
        try {
            $response = array();

            $sql = 'SELECT tcm.track_client_sub_category_id, tcm.client_id, tcm.category_sub_id, ccm.client_category_id, ccm.category_id, category_name, ccm.is_active, cs.sub_category_id, sub_category_name
                            FROM client_category_master ccm
                            JOIN category_subcategory cs ON cs.client_category_id = ccm.client_category_id
                            JOIN track_client_sub_category tcm ON tcm.client_id = ccm.client_id and tcm.client_master_id = ccm.client_master_id and tcm.category_sub_id = cs.category_sub_id ';

            if (sizeof($searchItems) > 0) {
                $sql .= " WHERE ";
            }
            foreach ($searchItems as $key => $value) {
                switch ($key) {
                    case 'client_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "ccm.client_id = ? ";
                        break;
                    case 'client_master_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = 'ccm.client_master_id = ? ';
                        break;
                    case 'category_id':
                        $a_param_type[] = 's';
                        $a_bind_params[] = $value;
                        $query[] = "ccm.category_id = ? ";
                        break;
                    case 'is_active':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "ccm.is_active = ? ";
                        break;
                    case 'category_sub_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = " tcm.category_sub_id = ? ";
                        break;
                    case 'sub_category_id':
                        $a_param_type[] = 's';
                        $a_bind_params[] = $value;
                        $query[] = " cs.sub_category_id = ? ";
                        break;

                }
            }
            $sql .= implode(' AND ', $query);
            $sql .= "  ORDER BY sub_category_name ASC ";
            $param_type = '';
            $n = count($a_param_type);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $a_param_type[$i];
            }
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                $a_params[] = &$a_bind_params[$i];
            }

            $response["trackingCategories"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                call_user_func_array(array($stmt, 'bind_param'), $a_params);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $tmp = array();
                    $is_all_exist = false;
                    $all = array();
                    $i = 0;
                    $stmt->bind_result($track_client_sub_category_id, $client_id, $category_sub_id, $client_category_id, $category_id, $category_name, $is_active, $sub_category_id, $sub_category_name);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["track_client_sub_category_id"] = $track_client_sub_category_id;
                        $tmp["client_id"] = $client_id;
                        $tmp["category_sub_id"] = $category_sub_id;
                        $tmp["client_category_id"] = $client_category_id;
                        $tmp["category_id"] = $category_id;
                        $tmp["category_name"] = $category_name;
                        $tmp["is_active"] = $is_active;
                        $tmp["sub_category_id"] = $sub_category_id;
                        $tmp["sub_category_name"] = $sub_category_name;

                        if ($searchItems['groupby'] == 1) {
                            if (strtolower(trim($sub_category_name)) == strtolower(trim('All'))) {
                                if (sizeof($response["trackingCategories"][$category_name]) <= 1) {
                                    $response["trackingCategories"][$category_name][0] = $tmp;
                                } else {
                                    array_unshift($response["trackingCategories"][$category_name], $tmp);
                                }
                            } else {
                                $response["trackingCategories"][$category_name][] = $tmp;
                            }
                        } else {
                            if (strtolower(trim($sub_category_name)) == strtolower(trim('All'))) {
                                if (sizeof($response["trackingCategories"]) <= 1) {
                                    $response["trackingCategories"][0] = $tmp;
                                } else {
                                    $response["trackingCategories"][] = $tmp;
                                }
                            } else {
                                $response["trackingCategories"][] = $tmp;
                            }
                        }
                    }

                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getMasterTrackingCategories($searchItems)
    {
        try {
            $response = array();

            $sql = 'SELECT  ccm.client_id, cs.category_sub_id, ccm.client_category_id, ccm.category_id, category_name, ccm.is_active, cs.sub_category_id, sub_category_name
                            FROM client_category_master ccm
                            JOIN category_subcategory cs ON ccm.client_category_id = cs.client_category_id
                           ';
            if (sizeof($searchItems) > 0) {
                $sql .= " WHERE ";
            }
            foreach ($searchItems as $key => $value) {
                switch ($key) {
                    case 'client_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "ccm.client_id = ? ";
                        break;
                    case 'client_master_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "ccm.client_master_id = ? ";
                        break;
                    case 'is_active':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "ccm.is_active = ? ";
                        break;

                }
            }
            $sql .= implode(' AND ', $query);
            $sql .= "  ORDER BY sub_category_name ASC ";

            $param_type = '';
            $n = count($a_param_type);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $a_param_type[$i];
            }
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                $a_params[] = &$a_bind_params[$i];
            }

            $response["trackingCategories"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                call_user_func_array(array($stmt, 'bind_param'), $a_params);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $tmp = array();
                    $is_all_exist = false;
                    $all = array();
                    $i = 0;
                    $stmt->bind_result($client_id, $category_sub_id, $client_category_id, $category_id, $category_name, $is_active, $sub_category_id, $sub_category_name);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["client_id"] = $client_id;
                        $tmp["category_sub_id"] = $category_sub_id;
                        $tmp["client_category_id"] = $client_category_id;
                        $tmp["category_id"] = $category_id;
                        $tmp["category_name"] = $category_name;
                        $tmp["is_active"] = $is_active;
                        $tmp["sub_category_id"] = $sub_category_id;
                        $tmp["sub_category_name"] = $sub_category_name;

                        if ($searchItems['groupby'] == 1) {
                            if (strtolower(trim($sub_category_name)) == strtolower(trim('All'))) {
                                if (sizeof($response["trackingCategories"][$category_name]) <= 1) {
                                    $response["trackingCategories"][$category_name][0] = $tmp;
                                } else {
                                    array_unshift($response["trackingCategories"][$category_name], $tmp);
                                }
                            } else {
                                $response["trackingCategories"][$category_name][] = $tmp;
                            }
                        } else {
                            if (strtolower(trim($sub_category_name)) == strtolower(trim('All'))) {
                                if (sizeof($response["trackingCategories"]) <= 1) {
                                    $response["trackingCategories"][0] = $tmp;
                                } else {
                                    $response["trackingCategories"][] = $tmp;
                                }
                            } else {
                                $response["trackingCategories"][] = $tmp;
                            }
                        }
                    }

                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getMetrics($client_master_id)
    {
        try {
            $db1 = new utility();
            $result_date = $this->getFieldByID('client_master_detail', 'client_master_id', $client_master_id, 'report_start_date');
            $report_start_date = $result_date['field_key'];
            $metric_json_file_path = __DIR__ . "/metrics/metric_" . $client_master_id . "_" . $report_start_date . ".json";

            if (file_exists($metric_json_file_path)) {
                $json_file = file_get_contents($metric_json_file_path);
                return json_decode($json_file, true);
            } else {
                $response['error'] = true;
                $response['message'] = NO_RECORD_FOUND;
                return $response;
            }
        } catch (Exception $e) {
            $this->conn->rollback();
            return $e->getMessage();
        }
    }

    public function saveEmployeesForMetrics($client_master_id, $employees)
    {
        try {
            $result = $this->getFieldByID('client_master_detail', 'client_master_id', $client_master_id, 'report_start_date');
            $report_start_date = $result['field_key'];
            $path = __DIR__ . "/metrics/metric_" . $client_master_id . "_" . $report_start_date . ".json";
            if (file_exists($path)) {
                $json_file = file_get_contents($path);
                $jfo = json_decode($json_file, true);
            }

            $from_month = date("j", strtotime($report_start_date));
            $from_year = date("Y", strtotime($report_start_date));
            $to_year = date("Y", strtotime($report_start_date . " +1 year"));
            if ($from_month == 1) {
                $to_year = $from_year;
                $finance_year = $from_year;
            } else {
                $finance_year = $from_year . "-" . $to_year;
            }

            $month_arr = array("1" => 'jan', "2" => "feb", "3" => "mar", "4" => 'apr', "5" => "may", "6" => "jun", "7" => 'jul', "8" => "aug", "9" => "sep", "10" => 'oct', "11" => "nov", "12" => "dece");

            // Insert employees into metric_employee table
            $update_flag = 0;
            $count_sql = "SELECT * from client_employee_data where client_master_id ='" . $client_master_id . "' and  from_year='" . $from_year . "' AND to_year ='" . $to_year . "' ";
            if ($stmt = $this->conn->prepare($count_sql)) {
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows == 0) {
                    $sql = "INSERT INTO client_employee_data (client_master_id, from_year, to_year) values(?,?,?)";
                    if ($stmt = $this->conn->prepare($sql)) {
                        $stmt->bind_param("sii", $client_master_id, $from_year, $to_year);
                        $result = $stmt->execute();
                        $last_inserted_id = $stmt->insert_id;
                        $stmt->close();
                        if ($result) {
                            $get_result = $this->conn->commit();
                            foreach (json_decode($employees) as $res) {
                                $month_value = $res->month;
                                $month_colum = $month_arr[$month_value];
                                $employee_count = $res->value;
                                $update_sql = "UPDATE client_employee_data set $month_colum = ?  WHERE client_master_id = ? AND from_year = ? AND to_year = ?  ";
                                $update_stmt = $this->conn->prepare($update_sql);
                                $update_stmt->bind_param("iiii", $employee_count, $client_master_id, $from_year, $to_year);
                                $update_stmt->execute();
                                $this->conn->commit();
                            }
                            $update_flag = 1;
                        }
                    }
                } else {
                    foreach (json_decode($employees) as $res) {
                        $month_value = $res->month;
                        $month_colum = $month_arr[$month_value];
                        $employee_count = $res->value;
                        $update_sql = "UPDATE client_employee_data set $month_colum = ?  WHERE client_master_id = ? AND from_year = ? AND to_year = ?  ";
                        $update_stmt = $this->conn->prepare($update_sql);
                        $update_stmt->bind_param("iiii", $employee_count, $client_master_id, $from_year, $to_year);
                        $update_stmt->execute();
                        $this->conn->commit();
                    }
                    $update_flag = 1;
                }
            }
            if ($update_flag == 1) {
                $fetch_sql = "SELECT jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dece from client_employee_data where client_master_id ='" . $client_master_id . "' and  from_year='" . $from_year . "' AND to_year ='" . $to_year . "' ";
                if ($fetch_stmt = $this->conn->prepare($fetch_sql)) {
                    $fetch_stmt->execute();
                    $fetch_stmt->store_result();
                    if ($fetch_stmt->num_rows > 0) {
                        $fetch_stmt->bind_result($jan, $feb, $mar, $apr, $may, $jun, $jul, $aug, $sep, $oct, $nov, $dece);
                        while ($result = $fetch_stmt->fetch()) {
                            $employee_json = "";
                            if (!empty($jan)) {
                                $employee_json .= '{"month":1,"year":' . $finance_year . ',"value":"' . $jan . '"}';
                            }if (!empty($feb)) {
                                if (!empty($employee_json)) {
                                    $employee_json .= ",";
                                }
                                $employee_json .= '{"month":2,"year":' . $finance_year . ',"value":"' . $feb . '"}';
                            }if (!empty($mar)) {
                                if (!empty($employee_json)) {
                                    $employee_json .= ",";
                                }
                                $employee_json .= '{"month":3,"year":' . $finance_year . ',"value":"' . $mar . '"}';
                            }if (!empty($apr)) {
                                if (!empty($employee_json)) {
                                    $employee_json .= ",";
                                }
                                $employee_json .= '{"month":4,"year":' . $finance_year . ',"value":"' . $apr . '"}';
                            }if (!empty($may)) {
                                if (!empty($employee_json)) {
                                    $employee_json .= ",";
                                }
                                $employee_json .= '{"month":5,"year":' . $finance_year . ',"value":"' . $may . '"}';
                            }if (!empty($jun)) {
                                if (!empty($employee_json)) {
                                    $employee_json .= ",";
                                }
                                $employee_json .= '{"month":6,"year":' . $finance_year . ',"value":"' . $jun . '"}';
                            }if (!empty($jul)) {
                                if (!empty($employee_json)) {
                                    $employee_json .= ",";
                                }
                                $employee_json .= '{"month":7,"year":' . $finance_year . ',"value":"' . $jul . '"}';
                            }if (!empty($aug)) {
                                if (!empty($employee_json)) {
                                    $employee_json .= ",";
                                }
                                $employee_json .= '{"month":8,"year":' . $finance_year . ',"value":"' . $aug . '"}';
                            }if (!empty($sep)) {
                                if (!empty($employee_json)) {
                                    $employee_json .= ",";
                                }
                                $employee_json .= '{"month":9,"year":' . $finance_year . ',"value":"' . $sep . '"}';
                            }if (!empty($oct)) {
                                if (!empty($employee_json)) {
                                    $employee_json .= ",";
                                }
                                $employee_json .= '{"month":10,"year":' . $finance_year . ',"value":"' . $oct . '"}';
                            }if (!empty($nov)) {
                                if (!empty($employee_json)) {
                                    $employee_json .= ",";
                                }
                                $employee_json .= '{"month":11,"year":' . $finance_year . ',"value":"' . $nov . '"}';
                            }if (!empty($dece)) {
                                if (!empty($employee_json)) {
                                    $employee_json .= ",";
                                }
                                $employee_json .= '{"month":12,"year":' . $finance_year . ',"value":"' . $dece . '"}';
                            }
                        }
                    }
                }

                $finalResponse["BankTransactions"] = $jfo["BankTransactions"];
                $finalResponse["accountsSummarytotal"] = $jfo["accountsSummarytotal"];
                $finalResponse["agedPayablestotal"] = $jfo["agedPayablestotal"];
                $finalResponse["agedReceivablestotal"] = $jfo["agedReceivablestotal"];
                $finalResponse["estimatedPayrolltotal"] = $jfo["estimatedPayrolltotal"];

                $finalResponse["Account Receivables"] = $jfo["Account Receivables"];
                $finalResponse["Total Income"] = $jfo["Total Income"];
                $finalResponse["Accounts Receivable DSO"] = $jfo["Accounts Receivable DSO"];
                $finalResponse["Account Payables"] = $jfo["Account Payables"];
                $finalResponse["Total Cost of Sales"] = $jfo["Total Cost of Sales"];
                $finalResponse["Total Operating Expenses"] = $jfo["Total Operating Expenses"];
                $finalResponse["Total Employee Compensation"] = $jfo["Total Employee Compensation"];
                $finalResponse["Operating Profit"] = $jfo["Operating Profit"];
                $finalResponse["Days Payable Outstanding (DPO)"] = $jfo["Days Payable Outstanding (DPO)"];
                $finalResponse["Cash Conversion Cycle"] = $jfo["Cash Conversion Cycle"];

                if (!empty($employee_json)) {
                    $employee_result = "[" . $employee_json . "]";
                    $finalResponse["employees"] = $employee_result;
                } else {
                    $finalResponse["employees"] = $jfo["employees"];
                }

                $finalResponse["EBIT"] = $jfo["EBIT"];

                $fp = fopen($path, 'w');
                fwrite($fp, json_encode($finalResponse));
                fclose($fp);
                return $jfo;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function removeClientTrackCategories($client_id)
    {
        try {
            $response = array();
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            $sql = "DELETE FROM track_client_sub_category WHERE client_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("i", $client_id);
                $result = $stmt->execute();
                $stmt->close();

                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    $response["message"] = DELETE_TRACKING_CATEGORIES_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = DELETE_TRACKING_CATEGORIES_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function removeCategoriesSubcategories($client_id)
    {
        try {
            $response = array();
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            $sql = "DELETE FROM category_subcategory WHERE client_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("i", $client_id);
                $result = $stmt->execute();

                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    $response["message"] = DELETE_CATEGORIES_SUBCAT_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = DELETE_CATEGORIES_SUBCAT_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function removeClientCategories($client_id)
    {
        try {
            $response = array();
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            $sql = "DELETE FROM client_category_master WHERE client_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("i", $client_id);
                $result = $stmt->execute();
                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    $response["message"] = DELETE_CLIENT_CATEGORIES_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = DELETE_CLIENT_CATEGORIES_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getDashboardBudgetDetails($client_master_id, $sub_category_id1, $sub_category_id2, $client_id, $report_start_date)
    {
        try {
            $finalResponse = array();
            $db1 = new utility();
            $current_month = strtolower(date('M'));

            $path = __DIR__ . "/metrics/metric_" . $client_master_id . "_" . $report_start_date . ".json";
            $json_file = file_get_contents($path);
            $jfo = json_decode($json_file, true);

            $net_profit_array = $jfo['Net Profit'];
            $net_Operating_Profit = $jfo['Operating Profit'];

            $month_arr = array('jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec');
            $consoliate_month_arr = array();
            $consoliate_month_arr_ytd_my = array();
            $consoliate_month_arr_ytd = array();
            $month_arr_linechart = array();
            $month_arr_linechartValue = array();
            $month_arr_barchart = array();
            $budget_track_result = array();

            $startDate = date("Y-m-01", strtotime("-11 months"));
            $endDate = date('Y-m-d');
            $i = 0;
            while (strtotime($startDate) <= strtotime($endDate)) {
                $consolidate_month_arr[$i] = date('M', strtotime($startDate));
                $month_arr_linechart[$i] = date('M', strtotime($startDate)) . '-' . date('y', strtotime($startDate));
                $month_arr_linechartValue[$i] = date('n', strtotime($startDate)) . '-' . date('y', strtotime($startDate));
                $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
                $i++;
            }

            $startDate = date("Y-m-01", strtotime($report_start_date));
            $i = 0;
            while (strtotime($startDate) <= strtotime($endDate)) {
                $consoliate_month_arr_ytd_my[$i] = date('n', strtotime($startDate)) . '-' . date('y', strtotime($startDate));
                $consoliate_month_arr_ytd[$i] = date('M', strtotime($startDate));
                $month_arr_barchart[$i] = date('M', strtotime($startDate)) . '-' . date('y', strtotime($startDate));
                $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
                $i++;
            }

            array_pop($month_arr_linechart);
            array_pop($month_arr_linechartValue);

            $budget_track_result['dashboard']['Month Array'] = $month_arr_barchart;
            $budget_track_result['dashboard']['Month Array Last'] = $month_arr_linechart;

            /* Finding Actual & Target Values for Dashboard - Revenue, Expenses, EBITDA graphs */
            $netProfitActualBar = array();
            $actual_total_income = 0;
            $actual_total_cost_of_sales = 0;
            $actual_total_operating_expenses = 0;
            $actual_net_Operating_Profit = 0;

            unset($net_Operating_Profit['no_expand']);
            foreach ($consoliate_month_arr_ytd_my as $key => $value) {
                $keyTI = $this->searchIdFromArray($value, $jfo['Total Income']);
                if (sizeof($keyTI) > 0) {
                    $actual_total_income = $actual_total_income + $jfo['Total Income'][$keyTI]['amount'];
                }

                $keyTCS = $this->searchIdFromArray($value, $jfo['Total Cost of Sales']);
                if (sizeof($keyTCS) > 0) {
                    $actual_total_cost_of_sales = $actual_total_cost_of_sales + $jfo['Total Cost of Sales'][$keyTCS]['amount'];
                }

                $keyTOE = $this->searchIdFromArray($value, $jfo['Total Operating Expenses']);
                if (sizeof($keyTOE) > 0) {
                    $actual_total_operating_expenses = $actual_total_operating_expenses + $jfo['Total Operating Expenses'][$keyTOE]['amount'];
                }

                $keyNOP = $this->searchIdFromArray($value, $net_Operating_Profit);
                if (sizeof($keyNOP) > 0) {
                    $actual_net_Operating_Profit = $actual_net_Operating_Profit + $net_Operating_Profit[$keyNOP]['amount'];
                    array_push($netProfitActualBar, round($net_Operating_Profit[$keyNOP]['amount']));
                } else {
                    array_push($netProfitActualBar, 0);
                }
            }

            $searchParams = array();
            $budget_target_result = array();
            $searchParams["line_type"] = "summaryRow";
            $searchParams["sub_category_id1"] = "1000";
            $budget_target_result = $this->getDashboardBudgetDetailsFromDB($client_master_id, $searchParams);

            if ($budget_target_result['message'] != 'No record found') {
                $netProfitTarget = array();

                foreach ($budget_target_result['budgetSummary'] as $record) {
                    $actual_amount = 0;
                    $ytd_budget_amount = 0;
                    $total_values = $record['total_values'];
                    foreach ($consoliate_month_arr_ytd as $month_value) {
                        $ytd_budget_amount = $ytd_budget_amount + $record[strtolower($month_value)];
                        if ($record['item_head'] == 'Net Profit') {
                            array_push($netProfitTarget, round($record[strtolower($month_value)]));
                        }
                    }

                    if ($record['item_head'] == 'Total Income') {
                        $actual_amount = $actual_total_income;
                    } else if ($record['item_head'] == 'Total Operating Expenses') {
                        $actual_amount = $actual_total_cost_of_sales + $actual_total_operating_expenses;
                    } else if ($record['item_head'] == 'Net Profit') {
                        $actual_amount = $actual_net_Operating_Profit;
                    }
                    $budget_track_result['dashboard'][$record[item_head]] = array("actual_amount" => $actual_amount, "ytd_budget_amount" => $ytd_budget_amount, "total_budget_amount" => $total_values);
                }
                $budget_track_result['dashboard']['Net Profit Target'] = $netProfitTarget;
                $budget_track_result['dashboard']['Net Profit Actual'] = $netProfitActualBar;
            } else {
                $total_values = 0;
                $ytd_budget_amount = 0;

                $budget_track_result['dashboard']['Total Income'] = array("actual_amount" => $actual_total_income, "ytd_budget_amount" => $ytd_budget_amount, "total_budget_amount" => $total_values);

                $actual_amount = 0;
                $actual_amount = $actual_total_cost_of_sales + $actual_total_operating_expenses;
                $budget_track_result['dashboard']['Total Operating Expenses'] = array("actual_amount" => $actual_amount, "ytd_budget_amount" => $ytd_budget_amount, "total_budget_amount" => $total_values);

                $budget_track_result['dashboard']['Net Profit'] = array("actual_amount" => $actual_net_Operating_Profit, "ytd_budget_amount" => $ytd_budget_amount, "total_budget_amount" => $total_values);
                $budget_track_result['dashboard']['Net Profit Target'] = [];
                $budget_track_result['dashboard']['Net Profit Actual'] = $netProfitActualBar;
            }

            /* Finding Values for Dashboard - Revenue, Operating Expenses, Total Employee Compensation, EBITDA, Total bank Transaction graphs */
            $totalIncomeMonth = array();
            $totalOPEXMonth = array();
            $totalEmpCompMonth = array();
            $netProfitMonth = array();
            $totalBankTransMonth = array();

            foreach ($month_arr_linechartValue as $key => $value) {
                $getIncomeKey = $this->searchIdFromArray($value, $jfo['Total Income']);
                if (sizeof($getIncomeKey) > 0) {
                    array_push($totalIncomeMonth, round($jfo['Total Income'][$getIncomeKey]['amount']));
                } else {
                    if ($value != $current_month) {
                        array_push($totalIncomeMonth, 0);
                    }
                }

                $getOprExpKey = $this->searchIdFromArray($value, $jfo['Total Operating Expenses']);
                if (sizeof($getOprExpKey) > 0) {
                    array_push($totalOPEXMonth, round($jfo['Total Operating Expenses'][$getOprExpKey]['amount']));
                } else {
                    if ($value != $current_month) {
                        array_push($totalOPEXMonth, 0);
                    }
                }

                $getExpCompKey = $this->searchIdFromArray($value, $jfo['Total Employee Compensation']);
                if (sizeof($getExpCompKey) > 0) {
                    array_push($totalEmpCompMonth, round($jfo['Total Employee Compensation'][$getExpCompKey]['amount']));
                } else {
                    if ($value != $current_month) {
                        array_push($totalEmpCompMonth, 0);
                    }
                }

                $getOprProfKey = $this->searchIdFromArray($value, $net_Operating_Profit);
                if (sizeof($getOprProfKey) > 0) {
                    array_push($netProfitMonth, round($net_Operating_Profit[$getOprProfKey]['amount']));
                } else {
                    if ($value != $current_month) {
                        array_push($netProfitMonth, 0);
                    }
                }

                if (isset($jfo['BankTransactions']['Number of Total Transactions']['summaryRow'][$key + 1])) {
                    if ($value != $current_month) {
                        array_push($totalBankTransMonth, round($jfo['BankTransactions']['Number of Total Transactions']['summaryRow'][$key + 1]));
                    }
                } else {
                    if ($value != $current_month) {
                        array_push($totalBankTransMonth, 0);
                    }
                }
            }

            $budget_track_result['dashboard']['Total Income Monthwise'] = $totalIncomeMonth;
            $budget_track_result['dashboard']['Total Operating Expenses Monthwise'] = $totalOPEXMonth;
            $budget_track_result['dashboard']['Total Employee Compensation Monthwise'] = $totalEmpCompMonth;
            $budget_track_result['dashboard']['Net Profit Monthwise'] = $netProfitMonth;
            $budget_track_result['dashboard']['Total Bank Transaction Monthwise'] = $totalBankTransMonth;

            $budget_track_result['dashboard']['Total Income Monthwise SMA'] = $this->generateSMA($month_arr_linechart, $totalIncomeMonth);
            $budget_track_result['dashboard']['Total Operating Expenses Monthwise SMA'] = $this->generateSMA($month_arr_linechart, $totalOPEXMonth);
            $budget_track_result['dashboard']['Total Employee Compensation Monthwise SMA'] = $this->generateSMA($month_arr_linechart, $totalEmpCompMonth);
            $budget_track_result['dashboard']['Net Profit Monthwise SMA'] = $this->generateSMA($month_arr_linechart, $netProfitMonth);
            $budget_track_result['dashboard']['Total Bank Transaction Monthwise SMA'] = $this->generateSMA($month_arr_linechart, $totalBankTransMonth);

            /* Finding Top 10 Expenses Values for Dashboard */
            $budget_track_result['dashboard']['Top N Expenses'] = $jfo['Operating Expenses'];

            $account_receivable = 0;
            foreach ($jfo['Account Receivables'] as $receive_value) {
                $account_receivable = $account_receivable + $receive_value['amount'];
            }

            $account_payable = 0;
            foreach ($jfo['Account Payables'] as $payable_value) {
                $account_payable = $account_payable + $payable_value['amount'];
            }

            $total_cost_of_sales = 0;
            foreach ($jfo['Total Cost of Sales'] as $sales_value) {
                $total_cost_of_sales = $total_cost_of_sales + $sales_value['amount'];
            }

            //Average OPEX over last 3-months
            // $data = array();
            // $data = $this->getEarlierMonthYear($report_start_date);
            // $dataLength = sizeof($data);
            // $average_opex_total = 0;
            // $avg = 0;

            // if ($dataLength == 1) {
            //     $key = $this->searchIdFromArray($data[0], $jfo['Total Operating Expenses']);
            //     if ($key != null) {
            //         $average_opex_total = $jfo['Total Operating Expenses'][$key]['amount'];
            //     }
            //     $avg = $average_opex_total;
            // } elseif ($dataLength == 2) {
            //     foreach ($data as $key => $val) {
            //         $arrayid = $this->searchIdFromArray($val, $jfo['Total Operating Expenses']);
            //         if ($arrayid != null) {
            //             $average_opex_total = $average_opex_total + $jfo['Total Operating Expenses'][$arrayid]['amount'];
            //         }

            //     }
            //     $avg = $average_opex_total / 2;
            // } elseif ($dataLength >= 3) {
            //     $count = 0;
            //     foreach ($data as $key => $val) {
            //         $arrayid1 = $this->searchIdFromArray($val, $jfo['Total Operating Expenses']);
            //         if ($arrayid1 != null && $count < 3) {
            //             $average_opex_total = $average_opex_total + $jfo['Total Operating Expenses'][$arrayid1]['amount'];
            //             $count = $count + 1;
            //         }
            //     }
            //     $avg = $average_opex_total / 3;
            // }

            $avg = (sizeof($budget_track_result['dashboard']['Net Profit Monthwise SMA']) > 0) ? end($budget_track_result['dashboard']['Net Profit Monthwise SMA']) : 0;
            $budget_track_result['metrics'] = array("Total Accounts Receivable" => $account_receivable,
                "Total Accounts Payable" => $account_payable,
                "Total Cost Of Sales" => $total_cost_of_sales,
                "dso" => isset($jfo['Accounts Receivable DSO']) ? $jfo['Accounts Receivable DSO'] : 0,
                "dpo" => isset($jfo['Days Payable Outstanding (DPO)']) ? $jfo['Days Payable Outstanding (DPO)'] : 0,
                "ccc" => isset($jfo['Cash Conversion Cycle']) ? $jfo['Cash Conversion Cycle'] : 0,
                "agedPayablestotal" => isset($jfo['agedPayablestotal']) ? $jfo['agedPayablestotal'] : 0,
                "agedReceivablestotal" => isset($jfo['agedReceivablestotal']) ? $jfo['agedReceivablestotal'] : 0,
                "estimatedPayrolltotal" => isset($jfo['estimatedPayrolltotal']) ? $jfo['estimatedPayrolltotal'] : 0,
                "averageOpextotal" => round($avg),
                "accountsSummarytotal" => isset($jfo['accountsSummarytotal']) ? $jfo['accountsSummarytotal'] : 0,
                "totalCurrentAssetsLast" => isset($jfo['Total Current Assets Last']) ? $jfo['Total Current Assets Last']['amount'] : 0,
                "totalBankLast" => isset($jfo['Total Bank Last']) ? $jfo['Total Bank Last']['amount'] : 0,
                "totalCurrentLiabilitiesLast" => isset($jfo['Total Current Liabilities Last']) ? $jfo['Total Current Liabilities Last']['amount'] : 0,
            );

            $a = array();
            $b = array();
            $c = array();
            $zohoResponse = array();
            $searchItems = array();
            $db = new reportClientApplicationMap();
            $searchItems['client_id'] = $client_id;
            $searchItems['report_id'] = 2;
            $searchItems['auth_key'] = "";
            $response = $db->getAllReportClientApplicationMaps($searchItems);

            if (sizeof($response) > 0) {
                if ($response["error"] == false) {
                    $currencyResult = array();
                    $currencyResult = $this->getFieldByID('client_master_detail', 'client_master_id', $client_master_id, 'default_currency');

                    $application_id = $response["reportClientApplicationMapDetails"][0]["application_id"];
                    $a[] = $this->getWeightedPipeLine($response["reportClientApplicationMapDetails"], $client_id, $application_id, $currencyResult);
                    $b[] = $this->getSalesOrders($response["reportClientApplicationMapDetails"], $client_id, $application_id, $currencyResult, $client_master_id);

                    if ((sizeof($a) > 0) && sizeof($b) > 0) {
                        if ($a[0]["error"] == false && $b[0]["error"] == false) {
                            $c[] = $this->getTotalOfSales($a, $b);
                        } else {
                            $c["error"] = true;
                            $c["message"] = SERVER_NO_RESPONSE;
                            return $c;
                        }
                    } else {
                        $c["error"] = true;
                        $c["message"] = SERVER_NO_RESPONSE;
                        return $c;
                    }

                    $zohoResponse = array_merge($a, $b, $c);
                    if (sizeof($zohoResponse) > 0) {
                        $total_sales_orders = 0;
                        foreach ($zohoResponse[2]['totalSalesOrder'] as $key => $value) {
                            foreach ($value as $key => $val) {
                                if ($key == 'actual_amount') {
                                    $total_sales_orders = $total_sales_orders + $val;
                                }

                            }
                        }
                        $budget_track_result["sales"]["total_sales_orders"] = $total_sales_orders;

                        $total_pipeline = 0;
                        foreach ($zohoResponse[0]['pipelineReport'] as $key => $value) {
                            foreach ($value as $key => $val) {
                                if ($key == 'actual_amount') {
                                    $total_pipeline = $total_pipeline + $val;
                                }

                            }
                        }
                        $budget_track_result["sales"]["total_pipeline"] = $total_pipeline;

                        $total_books = 0;
                        $target = 0;
                        foreach ($zohoResponse[1]['salesOrderReport'] as $key => $value) {
                            foreach ($value as $key => $val) {
                                if ($key == 'actual_amount') {
                                    $total_books = $total_books + $val;
                                } elseif ($key == 'budget') {
                                    $target = $target + $val;
                                }

                            }
                        }
                        $budget_track_result["sales"]["total_books"] = $total_books;
                        $budget_track_result["sales"]["target"] = $target;
                    }
                    $budget_track_result["sales"]["isavailable"] = true;
                } else {
                    $budget_track_result["sales"]["isavailable"] = false;
                }
            } else {
                $budget_track_result["sales"]["isavailable"] = false;
            }

            if (count($budget_track_result) > 0) {
                $budget_track_result["error"] = false;
                $finalResponse = json_encode($budget_track_result);
            } else {
                $finalResponse = json_encode(array('error' => true, 'message' => NO_RECORD_FOUND));
            }
            return $finalResponse;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getDashboardBudgetDetailsFromDB($client_master_id, $searchItems)
    {
        try {
            $searchItems['client_master_id'] = $client_master_id;
            $sql = "SELECT client_budget_id, c.client_master_id, org_name, c.client_category_id1, c.sub_category_id1, c.client_category_id2, c.sub_category_id2, line_type, item_head, line_item, month_jan, month_feb, month_march, month_april, month_may, month_june, month_july, month_aug, month_sept, month_oct, month_nov, month_dec, total_values FROM client_budget c WHERE  ";
            foreach ($searchItems as $key => $value) {
                switch ($key) {
                    case 'client_master_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "c.client_master_id = ? ";
                        break;
                    case 'line_type':
                        $a_param_type[] = 's';
                        $a_bind_params[] = $value;
                        $query[] = "c.line_type = ? ";
                        break;
                    case 'sub_category_id1':
                        $a_param_type[] = 's';
                        $a_bind_params[] = $value;
                        $query[] = "c.sub_category_id1 = ? ";
                        break;
                }
            }
            $sql .= implode(' AND ', $query);
            $param_type = '';
            $n = count($a_param_type);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $a_param_type[$i];
            }
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                $a_params[] = &$a_bind_params[$i];
            }
            $response["budgetSummary"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                call_user_func_array(array($stmt, 'bind_param'), $a_params);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($client_budget_id, $client_master_id, $org_name, $client_category_id1, $sub_category_id1, $client_category_id2, $sub_category_id2, $line_type, $item_head, $line_item, $month_jan, $month_feb, $month_march, $month_april, $month_may, $month_june, $month_july, $month_aug, $month_sept, $month_oct, $month_nov, $month_dec, $total_values);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["client_budget_id"] = $client_budget_id;
                        $tmp["client_master_id"] = $client_master_id;
                        $tmp["item_head"] = trim($item_head);
                        $tmp["jan"] = $this->formatDigits($month_jan);
                        $tmp["feb"] = $this->formatDigits($month_feb);
                        $tmp["mar"] = $this->formatDigits($month_march);
                        $tmp["apr"] = $this->formatDigits($month_april);
                        $tmp["may"] = $this->formatDigits($month_may);
                        $tmp["jun"] = $this->formatDigits($month_june);
                        $tmp["jul"] = $this->formatDigits($month_july);
                        $tmp["aug"] = $this->formatDigits($month_aug);
                        $tmp["sep"] = $this->formatDigits($month_sept);
                        $tmp["oct"] = $this->formatDigits($month_oct);
                        $tmp["nov"] = $this->formatDigits($month_nov);
                        $tmp["dec"] = $this->formatDigits($month_dec);
                        $tmp["total_values"] = $this->formatDigits($total_values);
                        $response["budgetSummary"][] = $tmp;
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getEarlierMonthNumber($report_start_date)
    {
        try {
            $data = array();
            $startDate = date("Y-m-01", strtotime($report_start_date));
            $endDate = date('Y-m-d');
            while (strtotime($startDate) <= strtotime($endDate)) {
                array_push($data, date("m", strtotime($startDate)));
                $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            }
            return array_reverse($data);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getEarlierMonthYear($report_start_date)
    {
        try {
            $data = array();
            $startDate = date("Y-m-01", strtotime($report_start_date));
            $endDate = date('Y-m-d');
            while (strtotime($startDate) <= strtotime($endDate)) {
                array_push($data, date("m-y", strtotime($startDate)));
                $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            }
            /* To remove current month record from array */
            array_pop($data);
            return array_reverse($data);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /*Searches array match for Month & Year, finally returns key*/
    public function searchIdFromArray($id, $array)
    {
        $result = null;
        $monthYear = explode("-", $id);
        $month = (int) $monthYear[0];
        $year = $monthYear[1];
        foreach ($array as $key => $val) {
            if ((int) $val['month'] == $month && $val['year'] == $year) {
                $result = $key;
                return $result;
            }
        }
        return $result;
    }

    public function searchInnerIdFromArray($id, $array)
    {
        $result = null;
        $inner_key = $id;
        foreach ($array as $key => $val) {
            if ($inner_key == $key) {
                $result = $key;
                return $result;
            }
        }
        return $result;
    }

    public function array_find_deep_by_key($array, $search)
    {
        $result = array();
        $search = strtolower(trim($search));
        foreach ($array as $k1 => $v1) {
            if (strtolower(trim($k1)) === $search) {
                return $v1;
            } else if (is_array($v1)) {
                foreach ($v1 as $k2 => $v2) {
                    if (strtolower(trim($k2)) === $search) {
                        return $v2;
                    } else if (is_array($v2)) {
                        foreach ($v2 as $k3 => $v3) {
                            if (strtolower(trim($k3)) === $search) {
                                return $v3;
                            } else if (is_array($v3)) {
                                foreach ($v3 as $k4 => $v4) {
                                    if (strtolower(trim($k4)) === $search) {
                                        return $v4;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function getFlashReport($client_id, $data, $ajax, $params, $loginDate, $report_start_date, $default_currency, $last_three_months, $dates, $currencies, $organisations, $client_master_id, $trackingCategories)
    {
        try {

            $dir = __DIR__ . "/flashreport/" . $client_master_id . "/";
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $filename = $client_master_id . "_flashreport_" . $loginDate . ".json";

            $path = $dir . $filename;

            $a = array();
            $b = array();
            $c = array();
            $d = array();
            $e = array();
            $f = array();
            $g = array();
            $x = array();
            // $z = array();

            $finalResponse = array();
            if (isset($data['XeroOAuth'])) {
                $XeroOAuth = $data['XeroOAuth'];
                $XeroOAuth->config['access_token'] = utf8_encode($data['oauth_token']);
                $XeroOAuth->config['access_token_secret'] = utf8_encode($data['oauth_token_secret']);
                $territoryBrand = array();
                $bankSummaryList = array();
                $agedPayablesList = array();
                $agedReceivablesList = array();
                $estimatedPayrollList = array();
                $bankTransactionList = array();
                // $trackReceivablesList = array();

                $territoryBrand = $trackingCategories;
               /*  $territoryBrand = $this->getTrackingCategoriesFromXero($client_id, $XeroOAuth, $ajax, $client_master_id); */
                if (isset($territoryBrand['expiry'])) {
                    return $territoryBrand;
                } else {
                    $a[] = $territoryBrand;
                }

                $bankSummaryList = $this->getBankSummary($client_id, $XeroOAuth, $ajax, $params, $client_master_id);
                if (isset($bankSummaryList['expiry'])) {
                    return $bankSummaryList;
                } else {
                    $b[] = $bankSummaryList;
                }

                $agedPayablesList = $this->getAgedPayables($client_id, $XeroOAuth, $ajax, $params, $default_currency, $report_start_date, $last_three_months, $client_master_id);
                if (isset($agedPayablesList['expiry'])) {
                    return $agedPayablesList;
                } else {
                    $c[] = $agedPayablesList;
                }

                $agedReceivablesList = $this->getAgedReceivables($client_id, $XeroOAuth, $ajax, $params, $default_currency, $report_start_date, $last_three_months, $client_master_id);
                if (isset($agedReceivablesList['expiry'])) {
                    return $agedReceivablesList;
                } else {
                    $d[] = $agedReceivablesList;
                }

                $territory_brand = array();
                $territory_brand = $a[0]['territorybrand'];
                $estimatedPayrollList = $this->getEstimatedPayRoll($client_id, $XeroOAuth, $territory_brand, $ajax, $params, $dates, $client_master_id);
                if (isset($estimatedPayrollList['expiry'])) {
                    return $estimatedPayrollList;
                } else {
                    $e[] = $estimatedPayrollList;
                }

                $f[] = $currencies;
                $g[] = $organisations;

                $bankTransactionList = $this->getBankTransaction($client_id, $XeroOAuth, $ajax, $params, $report_start_date, $dates, $client_master_id);
                if (isset($bankTransactionList['expiry'])) {
                    return $bankTransactionList;
                } else {
                    $x[] = $bankTransactionList;
                }

                // $trackReceivablesList = $this->getTrackMetricReceivables($client_id, $XeroOAuth, $ajax, $params, $currencyResult);
                // if (isset($trackReceivablesList['expiry'])) {
                //     return $trackReceivablesList;
                // } else {
                //     $z[] = $trackReceivablesList;
                // }

                $pageResult = array();
                $pageResult = $this->getOauthTokenByClientMaster($client_id, $client_master_id);

                $finalResponse = array_merge($a, $b, $c, $d, $e, $f, $g, $x);
                $finalResponse["error"] = false;
                $finalResponse["message"] = RECORD_FOUND;
                $finalResponse["path"] = $path;
                $finalResponse["last_updated_time"] = date('Y-m-d H:i:s');
                $finalResponse["default_currency"] = $default_currency;
                $finalResponse["generated_date"] = (strlen(trim($pageResult['field_key'])) > 0) ? strtotime($pageResult['field_key']) : "";
              
                file_put_contents($path, json_encode($finalResponse));
                chmod($path, 0777);
                $this->updateCacheLog($client_id, false);
            } else {
                $finalResponse["error"] = true;
                $finalResponse["message"] = XERO_AUTH_PROBLEM;
            }
            $pageResult = null;
            return $finalResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getBankSummary($client_id, $XeroOAuth, $ajax, $params, $client_master_id)
    {
        try {
            $bankSummary = array();
            $response = $this->makeRequest($client_id, 'getBankSummary', $XeroOAuth, 'GET', BANK_SUMMARY_API, array(), "", "json",$client_master_id);
            $bankResponse = $XeroOAuth->response;
            if ($bankResponse['code'] == 302) {
                $bankResponse["error"] = true;
                $bankResponse["message"] = XERO_HAVING_PROBLEM;
                return $bankResponse;
            } else if ($bankResponse['code'] == 401) {
                if (strpos($bankResponse['response'], 'token_expired') !== false || strpos($bankResponse['response'], 'token_rejected') !== false) {
                    if (isset($ajax) && ($ajax == true)) {
                        $bankSummary["expiry"] = true;
                        $bankSummary['error'] = true;
                        $bankSummary['message'] = TOKEN_EXPIRED;
                        return $bankSummary;
                    } else {
                        $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                        if (isset($resp['authurl'])) {
                            $authurl = $resp['authurl'];
                            header('Location:' . $authurl);die();
                        }
                    }
                }
            } else if ($bankResponse['code'] == 200) {
                $totalCashOnHand = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
                $aSummaryList = array();
                $bankDetails = array();

                $aSummaryList = json_decode($XeroOAuth->response['response'], true);
                $company_name = $aSummaryList['Reports'][0]['ReportTitles'][1];
                $bankDetails = $aSummaryList['Reports'][0]['Rows'];

                if (sizeof($bankDetails) > 0) {
                    foreach ($bankDetails[0]['Cells'] as $ky => $vy) {
                        if (in_array("Closing Balance", $vy)) {
                            $closingIndex = $ky;
                            break;
                        }
                    }
                    $rowDetails = $bankDetails[1]['Rows'];
                    foreach ($rowDetails as $k => $v) {
                        $total = 0;
                        foreach ($v['Cells'] as $k1 => $v1) {
                            if ($k1 == 0) {
                                $keyName = $v1['Value'];
                                $tmp['bank_account_name'] = $v1['Value'];
                            }
                            if ($k1 == $closingIndex) {
                                $tmp['closing_balance'] = round($v1['Value']);
                            }
                        }
                        $bankSummary["accountsSummary"]["org_name"] = $company_name;
                        $bankSummary["accountsSummary"][$keyName] = $tmp;
                        $bankSummary["error"] = false;
                        $bankSummary["message"] = RECORD_FOUND;
                    }
                }
            }
            $aSummaryList = null;
            $bankDetails = null;
            return $bankSummary;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAgedPayables($client_id, $XeroOAuth, $ajax, $params, $currencyResult, $report_start_date, $last_three_months, $client_master_id)
    {
        try {
            $aPayableListing = array();
            $endDate = new DateTime();
            $response = $this->makeRequest($client_id, 'getAgedPayables', $XeroOAuth, 'GET', INVOICE_API, array('Where' => 'Type=="ACCPAY" AND STATUS=="AUTHORISED" AND DATE <=  DateTime(' . $endDate->format('Y, m, d') . ')'), "", "json",$client_master_id);
            $payResponse = $XeroOAuth->response;
            if ($payResponse['code'] == 302) {
                $payResponse["error"] = true;
                $payResponse["message"] = XERO_HAVING_PROBLEM;
                return $payResponse;
            } else if ($payResponse['code'] == 401) {
                if (strpos($payResponse['response'], 'token_expired') !== false || strpos($payResponse['response'], 'token_rejected') !== false) {
                    if (isset($ajax) && ($ajax == true)) {
                        $aPayableListing["expiry"] = true;
                        $aPayableListing['error'] = true;
                        $aPayableListing['message'] = TOKEN_EXPIRED;
                        return $aPayableListing;
                    } else {
                        $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                        if (isset($resp['authurl'])) {
                            $authurl = $resp['authurl'];
                            header('Location:' . $authurl);die();
                        }
                    }
                }
            } else if ($payResponse['code'] == 200) {
                $payableList = array();
                $sum = array();

                $payableList = json_decode($XeroOAuth->response['response'], true);

                if (sizeof($payableList['Invoices']) > 0) {
                    $payables = $payableList['Invoices'];

                    foreach ($payables as $k => $v) {
                        if (($v['Status'] == 'AUTHORISED')) {
                            $contactID = $v['Contact']['ContactID'];
                            $tmp[$contactID]['Name'] = $v['Contact']['Name'];
                            if ($v['CurrencyCode'] != $currencyResult) {
                                if (isset($v['CurrencyRate'])) {
                                    $v['AmountDue'] = $v['AmountDue'] / $v['CurrencyRate'];
                                }

                            }
                            if (strlen($tmp[$contactID]["Older"]) <= 0) {
                                $tmp[$contactID]["Older"] = 0;
                            }

                            foreach ($last_three_months as $k2 => $v2) {
                                if (strlen($tmp[$contactID][$v2]) <= 0) {
                                    $tmp[$contactID][$v2] = 0;
                                }

                                if (strlen($sum[$v2]) <= 0) {
                                    $sum[$v2] = 0;
                                }
                            }

                            $inv_obj_date = isset($v['DateString']) ? $v['DateString'] : $v['Date'];
                            $monthText = date("F", strtotime($inv_obj_date));
                            $monthDiff = abs($this->dateDifference(date('Y-m-01'), $inv_obj_date));

                            if (($monthDiff <= 3)) {
                                $tmp[$contactID][$monthText] += $v['AmountDue'];
                                $sum[$monthText] += $v['AmountDue'];
                                $tmp[$contactID]["Total"] += $v['AmountDue'];
                            } else {
                                if ((date('Y-m-d', strtotime($v['DateString']))) <= (date('Y-m-t', strtotime("last day of")))) {
                                    $yearValue = "Older";
                                    $tmp[$contactID][$yearValue] += $v['AmountDue'];
                                    $sum[$yearValue] += $v['AmountDue'];
                                    $tmp[$contactID]["Total"] += $v['AmountDue'];
                                }
                            }
                            $sum["Total"] += $v['AmountDue'];
                        }
                    }
                    $final = array();
                    $sumHeader = array();
                    $sumHeader['total_payables'] = $sum;
                    $final[] = array_merge($tmp, $sumHeader);

                    $aPayableListing["agedPayables"] = $final;
                    $aPayableListing['error'] = false;
                    $aPayableListing['message'] = RECORD_FOUND;
                }

            }
            $final = null;
            $sumHeader = null;
            $payableList = null;
            $payResponse = null;

            return $aPayableListing;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAgedReceivables($client_id, $XeroOAuth, $ajax, $params, $currencyResult, $report_start_date, $last_three_months, $client_master_id)
    {
        try {
            $dt = new DateTime();
            $lastDate = $dt->format('Y-m-d');
            $endDate = new DateTime();
            $aReceivableListing = array();
            $response = $this->makeRequest($client_id, 'getAgedReceivables', $XeroOAuth, 'GET', INVOICE_API, array('Where' => 'Type=="ACCREC" AND STATUS=="AUTHORISED" AND DATE <=  DateTime(' . $endDate->format('Y, m, d') . ')'), "", "json",$client_master_id);
            $receiveResponse = $XeroOAuth->response;

            if ($receiveResponse['code'] == 302) {
                $receiveResponse["error"] = true;
                $receiveResponse["message"] = XERO_HAVING_PROBLEM;
                return $receiveResponse;
            } else if ($receiveResponse['code'] == 401) {
                if (strpos($receiveResponse['response'], 'token_expired') !== false || strpos($receiveResponse['response'], 'token_rejected') !== false) {
                    if (isset($ajax) && ($ajax == true)) {
                        $aReceivableListing["expiry"] = true;
                        $aReceivableListing['error'] = true;
                        $aReceivableListing['message'] = TOKEN_EXPIRED;
                        return $aReceivableListing;
                    } else {
                        $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                        if (isset($resp['authurl'])) {
                            $authurl = $resp['authurl'];
                            header('Location:' . $authurl);die();
                        }
                    }
                }
            } else if ($receiveResponse['code'] == 200) {
                $receivableList = array();
                $receivableList = json_decode($XeroOAuth->response['response'], true);

                if (sizeof($receivableList['Invoices']) > 0) {
                    $receivables = $receivableList['Invoices'];
                    $sum = array();
                    foreach ($receivables as $k => $v) {
                        if ($v['Status'] == 'AUTHORISED') {
                            $contactID = $v['Contact']['ContactID'];
                            $tmp[$contactID]['Name'] = $v['Contact']['Name'];
                            if ($v['CurrencyCode'] != $currencyResult) {
                                if (isset($v['CurrencyRate'])) {
                                    $v['AmountDue'] = $v['AmountDue'] / $v['CurrencyRate'];
                                }
                            }
                            if (strlen($tmp[$contactID]["Older"]) <= 0) {
                                $tmp[$contactID]["Older"] = 0;
                            }

                            foreach ($last_three_months as $k2 => $v2) {
                                if (strlen($tmp[$contactID][$v2]) <= 0) {
                                    $tmp[$contactID][$v2] = 0;
                                }

                                if (strlen($sum[$v2]) <= 0) {
                                    $sum[$v2] = 0;
                                }
                            }

                            $inv_obj_date = isset($v['DateString']) ? $v['DateString'] : $v['Date'];
                            $monthText = date("F", strtotime($inv_obj_date));
                            $monthDiff = abs($this->dateDifference(date('Y-m-01'), $inv_obj_date));

                            if (($monthDiff <= 3)) {
                                $tmp[$contactID][$monthText] += $v['AmountDue'];
                                $sum[$monthText] += $v['AmountDue'];
                                $tmp[$contactID]["Total"] += $v['AmountDue'];
                            } else {
                                if ((date('Y-m-d', strtotime($inv_obj_date))) <= (date('Y-m-t', strtotime("last day of")))) {
                                    $yearValue = "Older";
                                    $tmp[$contactID][$yearValue] += $v['AmountDue'];
                                    $sum[$yearValue] += $v['AmountDue'];
                                    $tmp[$contactID]["Total"] += $v['AmountDue'];
                                }
                            }
                            $tmp[$contactID]['History'][$monthText][] = [
                                "AmountDue" => $v['AmountDue'],
                                "AmountPaid" => $v['AmountPaid'],
                                "AmountCredited" => $v['AmountCredited'],
                                "InvoiceID" => $v['InvoiceID'],
                                "InvoiceNumber" => $v['InvoiceNumber'],
                                "Reference" => $v['Reference'],
                                "DateString" => $v['DateString'],
                                "Date" => $v['Date'],
                                "DueDateString" => $v['DueDateString'],
                                "DueDate" => $v['DueDate'],
                                "Status" => $v['Status'],
                                "LineAmountTypes" => $v['LineAmountTypes'],
                                "LineItems" => $v['LineItems'],
                                "SubTotal" => $v['SubTotal'],
                                "TotalTax" => $v['TotalTax'],
                                "Total" => $v['Total'],
                                "UpdatedDateUTC" => $v['UpdatedDateUTC'],
                                "CurrencyCode" => $v['CurrencyCode']
                            ];
                            $sum["Total"] += $v['AmountDue'];
                        }
                    }
                    $final = array();
                    $sumHeader = array();
                    $sumHeader['total_receivables'] = $sum;
                    $final[] = array_merge($tmp, $sumHeader);
                    $aReceivableListing["agedReceivables"] = $final;
                    $aReceivableListing['error'] = false;
                    $aReceivableListing['message'] = RECORD_FOUND;
                }
            }
            $final = null;
            $sumHeader = null;
            $receivableList = null;
            $receiveResponse = null;

            return $aReceivableListing;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getEstimatedPayRoll($client_id, $XeroOAuth, $territoryListing, $ajax, $params, $dates, $client_master_id)
    {
        try {
            $aEstimatedPayrollListing = array();
            $aTerritory = array();
            $aTerritory = $territoryListing[1];
            $tracking_category_id = $aTerritory['TrackingCategoryID'];
            $dtLength = count($dates);
            $fromDate = $dates[$dtLength - 4]["firstdate"];
            $lastDate = $dates[$dtLength - 4]["lastdate"];

            $response = $this->makeRequest($client_id, 'getEstimatedPayRoll', $XeroOAuth, 'GET', PROFIT_LOSS_API, array("fromDate" => $fromDate, "toDate" => $lastDate, "trackingCategoryID" => $tracking_category_id), "", "json",$client_master_id);
            $estimatedResponse = $XeroOAuth->response;
            if ($estimatedResponse['code'] == 302) {
                $estimatedResponse["error"] = true;
                $estimatedResponse["message"] = XERO_HAVING_PROBLEM;
                return $estimatedResponse;
            } else if ($estimatedResponse['code'] == 401) {
                if (strpos($estimatedResponse['response'], 'token_expired') !== false || strpos($estimatedResponse['response'], 'token_rejected') !== false) {
                    if (isset($ajax) && ($ajax == true)) {
                        $aEstimatedPayrollListing["expiry"] = true;
                        $aEstimatedPayrollListing['error'] = true;
                        $aEstimatedPayrollListing['message'] = TOKEN_EXPIRED;
                        return $aEstimatedPayrollListing;
                    } else {
                        $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                        if (isset($resp['authurl'])) {
                            $authurl = $resp['authurl'];
                            header('Location:' . $authurl);die();
                        }
                    }
                }
            } else if ($estimatedResponse['code'] == 200) {
                $estimatedList = array();
                $final = array();
                $success = false;

                $estimatedList = json_decode($XeroOAuth->response['response'], true);

                if (sizeof($estimatedList['Reports']) > 0) {
                    $company_name = $estimatedList['Reports'][0]['ReportTitles'][1];
                    $estimatedPayroll = $estimatedList['Reports'][0]["Rows"];

                    foreach ($estimatedPayroll as $k => $v) {
                        if ($v['RowType'] == "Header") {
                            $final[0][] = $v['Cells'];
                        }
                        if ($v['Title'] == "Employee Compensation & Related Expenses") {
                            foreach ($v['Rows'] as $k1 => $v1) {
                                $final[0][$v1['Cells'][0]['Value']] = $v1['Cells'];
                            }
                            $success = true;
                        }
                    }

                   // $aEstimatedPayrollListing["estimatedPayroll"]["org_name"] = $company_name;
                    $aEstimatedPayrollListing["fromToDate"] = date('d-M-Y', strtotime($fromDate)) . " to " . date('d-M-Y', strtotime($lastDate));
                    if ($success == true) {
                        $aEstimatedPayrollListing["estimatedPayroll"] = $final;
                        $aEstimatedPayrollListing['error'] = false;
                        $aEstimatedPayrollListing['message'] = RECORD_FOUND;
                    } else {
                        $aEstimatedPayrollListing["estimatedPayroll"] = [];
                        $aEstimatedPayrollListing['error'] = true;
                        $aEstimatedPayrollListing['message'] = NO_RECORD_FOUND;
                    }
                }
            }

            return $aEstimatedPayrollListing;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getCurrencies($client_id, $XeroOAuth, $ajax, $client_master_id)
    {
        try {
            $currencyList = array();
            $currencies = array();

            $rresponse = $this->makeRequest($client_id, 'getCurrencies', $XeroOAuth,'GET', $XeroOAuth->url('Currencies', 'core'), array(), "", "json",$client_master_id);
            $response = $XeroOAuth->response;
            if ($response['code'] == 302) {
                $response["error"] = true;
                $response["message"] = XERO_HAVING_PROBLEM;
                return $response;
            } else if ($response['code'] == 401) {
                if (strpos($response['response'], 'token_expired') !== false || strpos($response['response'], 'token_rejected') !== false) {
                    if (isset($ajax) && ($ajax == true)) {
                        $currencies["expiry"] = true;
                        $currencies['error'] = true;
                        $currencies['message'] = TOKEN_EXPIRED;
                        return $currencies;
                    } else {
                        $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                        if (isset($resp['authurl'])) {
                            $authurl = $resp['authurl'];
                            header('Location:' . $authurl);die();
                        }
                    }
                }
            } else if ($response['code'] == 200) {
                $currencyList = json_decode($XeroOAuth->response['response'], true);
                if (sizeof($currencyList) > 0) {
                    $resp = array();
                    $currencies['currencyList'] = $currencyList['Currencies'];
                    $currencies["error"] = false;
                    $currencies["message"] = RECORD_FOUND;
                }

            }
            return $currencies;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getOrganisation($client_id, $XeroOAuth, $ajax, $client_master_id)
    {
        try {
            $orgList = array();
            $organisations = array();

            $rresponse = $this->makeRequest($client_id, 'getOrganisation', $XeroOAuth, 'GET', $XeroOAuth->url('Organisation', 'core'), array(), "", "json",$client_master_id);
            $response = $XeroOAuth->response;

            if ($response['code'] == 302) {
                $response["error"] = true;
                $response["message"] = XERO_HAVING_PROBLEM;
                return $response;
            } else if ($response['code'] == 401) {
                if (strpos($response['response'], 'token_expired') !== false || strpos($response['response'], 'token_rejected') !== false) {
                    if (isset($ajax) && ($ajax == true)) {
                        $organisations["expiry"] = true;
                        $organisations['error'] = true;
                        $organisations['message'] = TOKEN_EXPIRED;
                        return $organisations;
                    } else {
                        $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                        if (isset($resp['authurl'])) {
                            $authurl = $resp['authurl'];
                            header('Location:' . $authurl);die();
                        }
                    }
                }
            } else if ($response['code'] == 200) {
                $orgList = json_decode($XeroOAuth->response['response'], true);
                if (sizeof($orgList) > 0) {
                    $resp = array();
                    $organisations['orgDetail'] = $orgList['Organisations'];
                    $resp = array();
                    $organisations["error"] = false;
                    $organisations["message"] = RECORD_FOUND;
                }
            }
            return $organisations;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getBankTransaction($client_id, $XeroOAuth, $ajax, $params, $report_start_date, $dates, $client_master_id)
    {
        try {
            $bankSummary = array();
            $finalResponse = array();

            foreach ($dates as $k2 => $v2) {
                if (is_numeric($k2)) {
                    $fDate = new DateTime($v2["firstdate"]);
                    $eDate = new DateTime($v2["lastdate"]);
                    $response = $this->makeRequest($client_id, 'getBankTransaction', $XeroOAuth, 'GET', BANK_TRANSACTION_API, array("Where" => 'Date >=  DateTime(' . $fDate->format('Y, m, d') . ')' . ' && Date < DateTime(' . $eDate->format('Y, m, d') . ')'), "", "json",$client_master_id);
                    $bankResponse = $XeroOAuth->response;
                    if ($bankResponse['code'] == 302) {
                        $bankResponse["error"] = true;
                        $bankResponse["message"] = XERO_HAVING_PROBLEM;
                        return $bankResponse;
                    } else if ($bankResponse['code'] == 401) {
                        if (strpos($bankResponse['response'], 'token_expired') !== false || strpos($bankResponse['response'], 'token_rejected') !== false) {
                            if (isset($ajax) && ($ajax == true)) {
                                $bankSummary["expiry"] = true;
                                $bankSummary['error'] = true;
                                $bankSummary['message'] = TOKEN_EXPIRED;
                                return $bankSummary;
                            } else {
                                $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                                if (isset($resp['authurl'])) {
                                    $authurl = $resp['authurl'];
                                    header('Location:' . $authurl);die();
                                }
                            }
                        }
                    } else if ($bankResponse['code'] == 200) {
                        $aSummaryList = json_decode($XeroOAuth->response['response'], true);
                        $month = date("n", strtotime($v2['firstdate']));

                        if (is_numeric($k2) && $k2 != 0 && ($month != date('n'))) {
                            if (sizeof($aSummaryList["BankTransactions"]) > 0) {
                                foreach ($aSummaryList["BankTransactions"] as $k => $v) {
                                    $finalResponse["BankTransactions"]["items"][$v["BankAccount"]["Name"]][$k2] += 1;
                                    $finalResponse["BankTransactions"]["Number of Total Transactions"]["summaryRow"][$k2] += 1;
                                    if (!isset($finalResponse["BankTransactions"]["items"][$v["BankAccount"]["Name"]][$k2])) {
                                        $finalResponse["BankTransactions"]["items"][$v["BankAccount"]["Name"]][$k2] = '-';
                                    }
                                }
                            } else {
                                $finalResponse["BankTransactions"]["Number of Total Transactions"]["summaryRow"][$k2] = '-';
                            }
                        }
                    }
                }
            }

            if (sizeof($finalResponse) > 0) {
                foreach ($dates as $k1 => $v1) {
                    if (is_numeric($k1)) {
                        $month = date("n", strtotime($v1['firstdate']));
                        if (is_numeric($k1) && $k1 != 0 && $month != date('n')) {
                            foreach ($finalResponse["BankTransactions"]["items"] as $k3 => $v3) {
                                if (!isset($finalResponse["BankTransactions"]["items"][$k3][$k1])) {
                                    $finalResponse["BankTransactions"]["items"][$k3][$k1] = '-';
                                }
                            }
                        }
                    }
                }
                $finalResponse["error"] = false;
                $finalResponse["message"] = RECORD_FOUND;
            } else {
                $finalResponse["error"] = true;
                $finalResponse["message"] = NO_RECORD_FOUND;
            }
            return $finalResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getPNLReport($client_id, $data, $ajax, $sParams, $loginDate, $is_admin, $report_start_date, $last_three_months, $dates, $currencies, $organisations, $client_master_id, $default_currency)
    {
        try {

            $utility = new utility();
            $res_client_master_detail = $utility->getClientMasterDetailByID('client_master_detail', 'client_master_id', $client_master_id);

            if ($res_client_master_detail["error"] == false) {
                $company_name = $res_client_master_detail['company_name'];
            } else {
                $company_name = "";
            }

            $finalResponse = array();
            if (isset($data['XeroOAuth'])) {
                $XeroOAuth = $data['XeroOAuth'];
                $XeroOAuth->config['access_token'] = utf8_encode($data['oauth_token']);
                $XeroOAuth->config['access_token_secret'] = utf8_encode($data['oauth_token_secret']);

                $territoryBrand = array();
                $catResponse = array();

                if ($is_admin == 1) {
                    /* $territoryBrand = $this->getTrackingCategoriesFromXero($client_id, $XeroOAuth, $ajax, $client_master_id);
                    if (isset($territoryBrand['expiry'])) {
                        return $territoryBrand;
                    } */

                    $searchItems = array();
                    // creating new invite
                    $searchItems['client_id'] = $client_id;
                    $searchItems['client_master_id'] = $client_master_id;
                    $searchItems["groupby"] = 1;
                    $catResponse = $this->getAllTrackingCategories($searchItems);

                    if ($catResponse["error"] == false) {
                        $groupParam = array();
                        foreach ($catResponse["trackingCategories"] as $k => $v) {
                            foreach ($v as $k1 => $v1) {
                                if ($v1["category_name"] != "All") {
                                    $groupParam[$v1["category_name"]][] = $v1["sub_category_id"];
                                }
                            }
                        }

                        $mParams = array();
                        $finalPath = array();
                        if (sizeof($groupParam) == 0 && sizeof($catResponse) > 0) {
                            $dir = __DIR__ . "/plreport/" . $client_master_id . "/";
                            if (!file_exists($dir)) {
                                mkdir($dir, 0777, true);
                            }
                            $finalPath[] = $dir . $client_master_id . "_plreport_" . $loginDate . "_" . TERRITORY_ID . ".json";
                            $mParams["sub_category_id1"][] = TERRITORY_ID;
                        } else if (sizeof($groupParam) > 0) {
                            $dir = __DIR__ . "/plreport/" . $client_master_id . "/";
                            if (!file_exists($dir)) {
                                mkdir($dir, 0777, true);
                            }
                            switch (sizeof($groupParam)) {
                                case 1:
                                    $arrayKeys = array_keys($groupParam);
                                    $array1 = $groupParam[$arrayKeys[0]];
                                    foreach ($array1 as $k2 => $v2) {
                                        $mParams["sub_category_id1"][] = $v2;
                                        $path = $dir . $client_master_id . "_plreport_" . $loginDate;
                                        $path .= "_" . $v2;
                                        if (isset($sParams["mtd"])) {
                                            $path .= "_" . $sParams["mtd"];
                                        }
                                        $path .= ".json";
                                        $finalPath[] = $path;
                                    }

                                    $mParams["sub_category_id1"][] = TERRITORY_ID;
                                    break;
                                case 2:
                                    $arrayKeys = array_keys($groupParam);
                                    $array1 = $groupParam[$arrayKeys[0]];
                                    $array2 = $groupParam[$arrayKeys[1]];

                                    foreach ($array1 as $k2 => $v2) {
                                        foreach ($array2 as $k3 => $v3) {
                                            $mParams["sub_category_id1"][] = $v2;
                                            $mParams["sub_category_id2"][] = $v3;

                                            $path = $dir . $client_master_id . "_plreport_" . $loginDate;
                                            $path .= "_" . $v2;
                                            $path .= "_" . $v3;

                                            if (isset($sParams["mtd"])) {
                                                $path .= "_" . $sParams["mtd"];
                                            }
                                            $path .= ".json";
                                            $finalPath[] = $path;
                                        }
                                    }
                                    $mParams["sub_category_id1"][] = TERRITORY_ID;
                                    $mParams["sub_category_id2"][] = TERRITORY_ID;
                                    break;
                                case 3:
                                    break;
                            }
                        }

                        // $finalPath = array();
                        // $finalPath[] = $dir . $client_master_id . "_plreport_" . $loginDate . "_1000.json";
                        // $finalPath[] = $dir . $client_master_id . "_plreport_" . $loginDate . "_1000_1000.json";

                        $a = array();
                        $e = array();
                        $a[] = $catResponse;
                        $e[] = $currencies;

                        $aBalanceSheet = array();
                        $b = array();
                        $aBalanceSheet = $this->getBalanceSheet($client_id, $XeroOAuth, $ajax, $organisations, $dates, $client_master_id);
                        if (isset($aBalanceSheet['expiry'])) {
                            return $aBalanceSheet;
                        } else {
                            $b[] = $aBalanceSheet;
                        }

                        $gInvoiceListData = array();
                        $g = array();
                        $gInvoiceListData = $this->getInvoiceListDatareport($client_id, $XeroOAuth, $ajax, $organisations, $dates, $client_master_id);
                        if (isset($gInvoiceListData['expiry'])) {
                            return $gInvoiceListData;
                        } else {
                            $g[] = $gInvoiceListData;
                        }

                        $budgetparams["groupBy"] = 1;
                        $totalbudgetSummarys = array();
                        $totalbudgetSummarys = $this->getBudgetDetailsFromDB($client_master_id, $budgetparams);

                        $consolidate_month_arr = array();
                        // $startDate = date("Y-m-01", strtotime("-11 months"));
                        $startDate = date("Y-m-01", strtotime($report_start_date));
                        $endDate = date('Y-m-d');
                        while (strtotime($startDate) <= strtotime($endDate)) {
                            $consolidate_month_arr[] = $utility->getCurrentMonthField(date('m', strtotime($startDate)));
                            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
                        }
                        $consolidate_month_arr = array_reverse($consolidate_month_arr);

                        /* Loop Execution for Each category Combination */
                        for ($i = 0; $i < sizeof($finalPath); $i++) {

                            if (isset($mParams["sub_category_id1"])) {
                                $params["sub_category_id1"] = $mParams["sub_category_id1"][$i];
                            }
                            if (isset($mParams["sub_category_id2"])) {
                                $params["sub_category_id2"] = $mParams["sub_category_id2"][$i];
                            }
                            if (isset($sParams["mtd"])) {
                                $params["mtd"] = $sParams["mtd"];
                            }

                            $budgetSummarys = array();
                            if (sizeof($totalbudgetSummarys["budgetSummary"]) > 0) {
                                foreach ($totalbudgetSummarys["budgetSummary"] as $ky => $vy) {
                                    if (isset($params["sub_category_id2"])) {
                                        if ($vy["territory_id"] == $params["sub_category_id1"] && $vy["brand_id"] == $params["sub_category_id2"]) {
                                            array_push($budgetSummarys, $vy);
                                        }
                                    } else if ($vy["territory_id"] == $params["sub_category_id1"]) {
                                        array_push($budgetSummarys, $vy);
                                    }
                                }
                            }

                            $aPNLList = array();
                            $c = array();
                            $aPNLList = $this->getConsolidatedPNL($client_id, $XeroOAuth, $ajax, $params, $is_admin, $dates, $budgetSummarys, $consolidate_month_arr, $report_start_date, $client_master_id);
                            if (isset($aPNLList['expiry'])) {
                                return $aPNLList;
                            } else {
                                $c[] = $aPNLList;
                            }

                            $aPNLTrend = array();
                            $d = array();
                            $aPNLTrend = $this->getTrendPNL($client_id, $XeroOAuth, $ajax, $params, $is_admin, $dates, $report_start_date, $client_master_id);
                            if (isset($aPNLTrend['expiry'])) {
                                return $aPNLTrend;
                            } else {
                              
                                $d[] = $aPNLTrend;
                            }

                            $f = array();
                            if ((sizeof($aPNLTrend) > 0) && (sizeof($aBalanceSheet) > 0)) {
                                if (($aPNLTrend["error"] == false) && ($aBalanceSheet["error"] == false)) {
                                    $f[] = array("cashFlowStatement" => $this->getCashFlowStatement($aPNLTrend, $aBalanceSheet, $client_id, $dates));
                                } else {
                                    $f[] = $aPNLTrend;
                                }
                            } else {
                                $f[] = array("error" => true, "message" => NO_RECORD_FOUND);
                            }

                            $finalResponse = array();
                            $finalResponse = array_merge($a, $b, $c, $d, $e, $f, $g);
                            $finalResponse["error"] = false;
                            $finalResponse["message"] = RECORD_FOUND;
                            $finalResponse["last_updated_time"] = date('Y-m-d H:i:s');
                            $finalResponse["default_currency"] = $default_currency;
                            $finalResponse["company_name"] = $company_name;
                            $finalResponse["report_start_date"] = $report_start_date;
                            $finalResponse["client_master_id"] = $client_master_id;
                            $pageResult = $this->getFieldByID('client_oauth_token', 'client_id', $client_id, 'generated_date');
                            $finalResponse["generated_date"] = (strlen(trim($pageResult['field_key'])) > 0) ? strtotime($pageResult['field_key']) : "";
                            file_put_contents($finalPath[$i], json_encode($finalResponse));
                            chmod($finalPath[$i], 0777);
                            $insert_Update_PDF_Generate_Date = $this->ReportGenerateDateforPDF($params["sub_category_id1"], isset($params["sub_category_id2"]) ? isset($params["sub_category_id2"]) : 0, $client_master_id);
                        }
                        $this->updateCacheLog($client_id, false);
                    }
                }
            } else {
                $finalResponse["error"] = true;
                $finalResponse["message"] = XERO_AUTH_PROBLEM;
            }
            $utility = null;
            return $finalResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getBalanceSheet($client_id, $XeroOAuth, $ajax, $organisations, $dates, $client_master_id)
    {
        try {
            $balanceSheetListing = array();
            $balanceSheet = array();
            $aBalanceSheet = array();
            $bsSummary = array();
            foreach ($dates as $k => $v) {

                if (is_numeric($k)) {
                    $response = $this->makeRequest($client_id, 'getBalanceSheet', $XeroOAuth, 'GET', BALANCE_SHEET_API, array("date" => $v["lastdate"]), "", "json",$client_master_id);
                    $balanceSheetResponse = $XeroOAuth->response;
                    if ($balanceSheetResponse['code'] == 302) {
                        $balanceSheetResponse["error"] = true;
                        $balanceSheetResponse["message"] = XERO_HAVING_PROBLEM;
                        return $balanceSheetResponse;
                    } else if ($balanceSheetResponse['code'] == 401) {
                        if (strpos($balanceSheetResponse['response'], 'token_expired') !== false || strpos($balanceSheetResponse['response'], 'token_rejected') !== false) {
                            $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                            if (isset($resp['authurl'])) {
                                $authurl = $resp['authurl'];
                                header('Location:' . $authurl);die();
                            }
                        }
                    } else if ($balanceSheetResponse['code'] == 200) {
                        $balanceSheet = array();
                        $aBalanceSheet = json_decode($XeroOAuth->response['response'], true);
                        $company_name = $aBalanceSheet['Reports'][0]['ReportTitles'][1];
                        $balanceSheet = $aBalanceSheet['Reports'][0]['Rows'];

                        if (sizeof($balanceSheet) > 0) {
                            foreach ($balanceSheet as $k1 => $v1) {
                                if ($v1["RowType"] == "Header") {
                                    $date_of_month = $v1["Cells"][1]["Value"];
                                    $date = date_create_from_format('j M Y', $date_of_month);
                                    $month = date_format($date, 'n');
                                    $year = date_format($date, 'y');
                                    $day = date_format($date, 'd');
                                }
                                switch ($v1["RowType"]) {
                                    case "Section":
                                        $title = strtolower(trim($v1["Title"]));
                                        foreach ($v1["Rows"] as $k2 => $v2) {
                                            if (sizeof($v2["Cells"]) > 0) {
                                                if ($v2["RowType"] == "Row") {
                                                    foreach ($v2["Cells"] as $k3 => &$v3) {
                                                        if ($k3 % 2 == 0 && strlen($v3["Value"]) > 0) {
                                                        } else {
                                                            if (strlen($title) > 0) {
                                                                $bsSummary[$title][$v2["Cells"][$k3 - 1]["Value"]][$k] = array('date' => $date_of_month, 'amount' => $v3["Value"], 'month' => $month, 'year' => $year,'id' => $month .'-'.$year);
                                                            } else {
                                                                $title1 = strtolower(trim($v2["Cells"][0]["Value"]));
                                                                $bsSummary[$title1]["no_expand"] = true;
                                                                $bsSummary[$title1][$k] = array('date' => $date_of_month, 'amount' => $v2["Cells"][1]["Value"], 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                                                            }
                                                        }
                                                        unset($v3["Attributes"]);
                                                    }
                                                } else if ($v2["RowType"] == "SummaryRow") {
                                                    foreach ($v2["Cells"] as $k3 => &$v3) {
                                                        if (strlen($title) > 0) {
                                                            if ($v1["Title"] == "Equity") {
                                                                $bsSummary[$title]["summaryRow"]["equity_gray"] = true;
                                                            }
                                                            $bsSummary[$title]["summaryRow"][$v2["Cells"][0]["Value"]][$k] = array('date' => $date_of_month, 'amount' => $v2["Cells"][1]["Value"], 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                                                        } else {
                                                            $title1 = strtolower(trim($v2["Cells"][0]["Value"]));
                                                            $bsSummary[$title1]["no_expand"] = true;
                                                            $bsSummary[$title1][$k] = array('date' => $date_of_month, 'amount' => $v2["Cells"][1]["Value"], 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            $finalArray = array();

            if (isset($bsSummary[strtolower("Bank")])) {
                $finalArray["Bank"] = $bsSummary[strtolower("Bank")];
            } else if (isset($bsSummary[strtolower("Cash and Cash Equivalents")])) {
                $finalArray["Bank"] = $bsSummary[strtolower("Cash and Cash Equivalents")];
                $totalBank = $finalArray["Bank"]['summaryRow']['Total Cash and Cash Equivalents'];
                unset($finalArray["Bank"]['summaryRow']['Total Cash and Cash Equivalents']);
                $finalArray["Bank"]['summaryRow']['Total Bank'] = $totalBank;
            }

            // if (sizeof($bsSummary[strtolower("Bank")]) > 0) {
            //     $finalArray["Bank"] = $bsSummary[strtolower("Bank")];
            // }
            if (sizeof($bsSummary[strtolower("Current Assets")]) > 0) {
                $finalArray["Current Assets"] = $bsSummary[strtolower("Current Assets")];
            }

            if (sizeof($bsSummary[strtolower("Fixed Assets")]) > 0) {
                $finalArray["Fixed Assets"] = $bsSummary[strtolower("Fixed Assets")];
            } else if (sizeof($bsSummary[strtolower("Property, Plant and Equipment")]) > 0) {
                $finalArray["Fixed Assets"] = $bsSummary[strtolower("Property, Plant and Equipment")];
                $totalFixedAseets = $finalArray["Fixed Assets"]['summaryRow']['Total Property, Plant and Equipment'];
                unset($finalArray["Fixed Assets"]['summaryRow']['Total Property, Plant and Equipment']);
                $finalArray["Fixed Assets"]['summaryRow']['Total Fixed Assets'] = $totalFixedAseets;
            }

            // if (sizeof($bsSummary[strtolower("Fixed Assets")]) > 0) {
            //     $finalArray["Fixed Assets"] = $bsSummary[strtolower("Fixed Assets")];
            // }
            if (sizeof($bsSummary[strtolower("Non-Current Assets")]) > 0) {
                $finalArray["Non-Current Assets"] = $bsSummary[strtolower("Non-Current Assets")];
            }
            if (sizeof($bsSummary[strtolower("Total Assets")]) > 0) {
                $finalArray["Total Assets"] = $bsSummary[strtolower("Total Assets")];
            }
            if (sizeof($bsSummary[strtolower("Current Liabilities")]) > 0) {
                $finalArray["Current Liabilities"] = $bsSummary[strtolower("Current Liabilities")];
            }
            if (sizeof($bsSummary[strtolower("Non-Current Liabilities")]) > 0) {
                $finalArray["Non-Current Liabilities"] = $bsSummary[strtolower("Non-Current Liabilities")];
            }
            if (sizeof($bsSummary[strtolower("Total Liabilities")]) > 0) {
                $finalArray["Total Liabilities"] = $bsSummary[strtolower("Total Liabilities")];
            }
            if (isset($bsSummary[strtolower("Net Assets")])) {
                if (sizeof($bsSummary[strtolower("Net Assets")]) > 0) {
                    $finalArray["Net Assets"] = $bsSummary[strtolower("Net Assets")];
                }
            }
            if (sizeof($bsSummary[strtolower("Equity")]) > 0) {
                $finalArray["Equity"] = $bsSummary[strtolower("Equity")];
            }
            if (sizeof($bsSummary[strtolower("Total Liabilities and Equity")]) > 0) {
                $finalArray["Total Liabilities and Equity"] = $bsSummary[strtolower("Total Liabilities and Equity")];
            }

            if (sizeof($finalArray) > 0) {
                $balanceSheetListing["error"] = false;
                $balanceSheetListing["message"] = RECORD_FOUND;
                $balanceSheetListing["balanceSheet"]["org_name"] = $company_name;
                $balanceSheetListing["balanceSheet"] = $finalArray;
            }
            return $balanceSheetListing;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getConsolidatedPNL($client_id, $XeroOAuth, $ajax, $params, $is_admin, $dates, $budgetSummarys, $consolidate_month_arr, $report_start_date, $client_master_id)
    {
        try {
            $aPNLReport = array();
            $trackParams = array();
            $aParams = array();
            $bParams = array();
            $userAdmin = array();
            $searchItems = array();
            $goAhead = true;

            $searchItems['client_id'] = $client_id;
            $searchItems['client_master_id'] = $client_master_id;

            if ($is_admin == 1) {
                $goAhead = true;
                if (sizeof($params) > 0) {
                    if (isset($params["sub_category_id1"])) {
                        if ($params['sub_category_id1'] != TERRITORY_ID) {
                            $searchItems['sub_category_id'] = $params["sub_category_id1"];
                            $aParams = $this->getAllTrackingCategories($searchItems);
                        } else {
                            $aParams["error"] = true;
                        }
                    } else {
                        $aParams["error"] = true;
                    }
                    if (isset($params["sub_category_id2"])) {
                        if ($params['sub_category_id2'] != TERRITORY_ID) {
                            $searchItems['sub_category_id'] = $params["sub_category_id2"];
                            $bParams = $this->getAllTrackingCategories($searchItems);
                        } else {
                            $bParams["error"] = true;
                        }
                    } else {
                        $bParams["error"] = true;
                    }
                }
            } else {
                $netParams = array();
                $searchItems["groupby"] = 1;
                $netParams = $this->getAllTrackingCategories($searchItems);
                if ($netParams["error"] == false) {
                    $keys = array_keys($netParams["trackingCategories"]);
                    $a = sizeof($keys);
                    if ($a == 1) {
                        $aParams["trackingCategories"][0]["category_id"] = $netParams["trackingCategories"][$keys[0]][0]["category_id"];
                        $aParams["error"] = false;
                        $params["sub_category_id1"] = $netParams["trackingCategories"][$keys[0]][0]["sub_category_id"];
                        $bParams["error"] = true;
                        $goAhead = true;
                    } else {
                        $aParams["error"] = false;
                        $bParams["error"] = false;
                        $aParams["trackingCategories"][0]["category_id"] = $netParams["trackingCategories"][$keys[0]][0]["category_id"];
                        $bParams["trackingCategories"][0]["category_id"] = $netParams["trackingCategories"][$keys[1]][0]["category_id"];
                        $params["sub_category_id1"] = $netParams["trackingCategories"][$keys[0]][0]["sub_category_id"];
                        $params["sub_category_id2"] = $netParams["trackingCategories"][$keys[1]][0]["sub_category_id"];
                        $goAhead = true;
                    }
                } else {
                    $aParams["error"] = true;
                    $bParams["error"] = true;
                    $goAhead = false;
                }
            }

            if ($goAhead == true) {
                if (($aParams["error"] == false) && ($bParams["error"] == false)) {
                    $trackParams = array("trackingCategoryID" => $aParams["trackingCategories"][0]["category_id"], "trackingOptionID" => $params["sub_category_id1"], "trackingCategoryID2" => $bParams["trackingCategories"][0]["category_id"], "trackingOptionID2" => $params["sub_category_id2"]);
                } else if (($aParams["error"] == false) && ($bParams["error"] == true)) {
                    $trackParams = array("trackingCategoryID" => $aParams["trackingCategories"][0]["category_id"], "trackingOptionID" => $params["sub_category_id1"]);
                } else if (($aParams["error"] == true) && ($bParams["error"] == false)) {
                    $trackParams = array("trackingCategoryID" => $bParams["trackingCategories"][0]["category_id"], "trackingOptionID" => $params["sub_category_id2"]);
                }

                $response = $this->makeRequest($client_id, 'getConsolidatedPNL', $XeroOAuth, 'GET', PROFIT_LOSS_API, $trackParams, "", "json",$client_master_id);
                $pnlResponsemtd = $XeroOAuth->response;
                if ($pnlResponsemtd['code'] == 302) {
                    $pnlResponse["error"] = true;
                    $pnlResponse["message"] = XERO_HAVING_PROBLEM;
                    return $pnlResponse;
                } else if ($pnlResponsemtd['code'] == 401) {
                    if (strpos($pnlResponsemtd['response'], 'token_expired') !== false || strpos($pnlResponsemtd['response'], 'token_rejected') !== false) {
                        if (isset($ajax) && ($ajax == true)) {
                            $aPNLReport["expiry"] = true;
                            $aPNLReport['error'] = true;
                            $aPNLReport['message'] = TOKEN_EXPIRED;
                            return $aPNLReport;
                        } else {
                            $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                            if (isset($resp['authurl'])) {
                                $authurl = $resp['authurl'];
                                header('Location:' . $authurl);die();
                            }
                        }
                    }
                } else if ($pnlResponsemtd['code'] == 200) {
                    $aPLSummaryListMTD = array();
                    $aPLSummaryListMTD = json_decode($XeroOAuth->response['response'], true);
                    $params["mtd"] = 1;
                    $aPNLReport["consolidatedPNL_mtd"] = $this->processConsolidatedPNL($aPLSummaryListMTD, $budgetSummarys, $params, $consolidate_month_arr);

                    //////To fetch YTD & YTY record
                    /*
                    The below line of code commented to fetch upto last date of current month &
                    only current financial report month alone
                    based on client requirement on 05-02-2018
                     */
                    $startDate = date('Y-m-01', strtotime($report_start_date));
                    $endDate = date('Y-m-t');
                    $dateArray = array("fromDate" => $startDate, "toDate" => $endDate);

                    if (sizeof($dateArray) > 0) {
                        $trackParams = array_merge($trackParams, $dateArray);
                    }

                    $response = $this->makeRequest($client_id, 'getConsolidatedPNL', $XeroOAuth, 'GET', PROFIT_LOSS_API, $trackParams, "", "json",$client_master_id);
                    $pnlResponseytd = $XeroOAuth->response;
                    if ($pnlResponseytd['code'] == 302) {
                        $pnlResponse["error"] = true;
                        $pnlResponse["message"] = XERO_HAVING_PROBLEM;
                        return $pnlResponse;
                    } else if ($pnlResponseytd['code'] == 401) {
                        if (strpos($pnlResponseytd['response'], 'token_expired') !== false || strpos($pnlResponseytd['response'], 'token_rejected') !== false) {
                            if (isset($ajax) && ($ajax == true)) {
                                $aPNLReport["expiry"] = true;
                                $aPNLReport['error'] = true;
                                $aPNLReport['message'] = TOKEN_EXPIRED;
                                return $aPNLReport;
                            } else {
                                $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                                if (isset($resp['authurl'])) {
                                    $authurl = $resp['authurl'];
                                    header('Location:' . $authurl);die();
                                }
                            }
                        }
                    } else if ($pnlResponseytd['code'] == 200) {
                        $aPLSummaryList = array();
                        $aPLSummaryList = json_decode($XeroOAuth->response['response'], true);
                        $params["mtd"] = 0;
                        $aPNLReport["consolidatedPNL_yty"] = $this->processConsolidatedPNL($aPLSummaryList, $budgetSummarys, $params, $consolidate_month_arr);
                        $params["mtd"] = 2;
                        $aPNLReport["consolidatedPNL_ytd"] = $this->processConsolidatedPNL($aPLSummaryList, $budgetSummarys, $params, $consolidate_month_arr);
                    } else {
                        $aPNLReport["error"] = true;
                        $aPNLReport["message"] = NO_RECORD_FOUND;
                    }
                } else {
                    $aPNLReport["error"] = true;
                    $aPNLReport["message"] = NO_RECORD_FOUND;
                }
            } else {
                $aPNLReport["error"] = true;
                $aPNLReport["message"] = NO_RECORD_FOUND;
            }
            $aPLSummaryList = null;
            $aParams = null;
            $bParams = null;
            $netParams = null;
            $trackParams = null;

            return $aPNLReport;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function processConsolidatedPNL($aPLSummaryList, $budgetSummarys, $params, $consolidate_month_arr)
    {
        try {
            $utility = new utility();
            $group = new group();
            $aPNLReport = array();
            $pnlDetails = array();
            $pnlSummary = array();

            $company_name = $aPLSummaryList['Reports'][0]['ReportTitles'][1];
            $pnlDetails = $aPLSummaryList['Reports'][0]['Rows'];

            /*$filename = "../include/logs/log_" . $client_id . "_consolidated.txt";
            $content = "DATE TIME : "  . date("Y-m-d h:i:s") . " \n";
            $content .= " Report : Trend P & L" . " \n";
            $content .= " Duration : " . $trackParams . " \n";
            $content .= " DATA : " . json_encode($pnlDetails);
            $content .= "\n" . "________________________________________________________________" . "\n";
            file_put_contents($filename, trim($content . "\n"), FILE_APPEND);
             */

            if (sizeof($pnlDetails) > 0 && isset($pnlDetails[1])) {
                $budget = array();
                if (sizeof($budgetSummarys) > 0) {
                    foreach ($budgetSummarys as $ky => $vy) {

                        if (strlen(trim($vy["line_item"])) > 0) {
                            if ($params["mtd"] == 1) {
                                $month_name = $utility->getCurrentMonthField(date('m'));

                                if (trim($vy["line_type"]) == "Row") {
                                    $budget[trim($vy["item_head"])][trim($vy["line_item"])] = round($vy[$month_name]); // trim(str_replace('$', '', $vy["month_aug"]));
                                } elseif (trim($vy["line_type"]) == "summaryRow") {
                                    $budget["summaryRow"][trim($vy["item_head"])] = round($vy[$month_name]);
                                }
                            } else if ($params["mtd"] == 2) {
                                $ytd_budget_amount = 0;
                                foreach ($consolidate_month_arr as $month_value) {
                                    $ytd_budget_amount = $ytd_budget_amount + $vy[$month_value];
                                }
                                $budget[trim($vy["item_head"])][trim($vy["line_item"])] = round($ytd_budget_amount);
                            } else {
                                $budget[trim($vy["item_head"])][trim($vy["line_item"])] = round($vy["total_values"]);
                            }
                        }
                    }
                }

                foreach ($pnlDetails as $k => $v) {
                    switch ($v["RowType"]) {
                        case "Section":
                            foreach ($v["Rows"] as $k1 => $v1) {
                                if ($v1["RowType"] == "Header") {
                                    $date_of_month = $v1["Cells"][1]["Value"];
                                    $date = date_create_from_format('j M y', $date_of_month);
                                    $month = date_format($date, 'n');
                                    $year = date_format($date, 'y');
                                    $day = date_format($date, 'd');
                                }

                                $summary = array();
                                $cellValues = array();
                                if (sizeof($v1["Cells"]) > 0) {
                                    if ($v1["RowType"] == "Row") {
                                        if (strlen($v["Title"]) > 0) {
                                            if ($v["Title"] == LESS_OPERATING_EXPENSES) {
                                                $budgetTitle = "Operating Expenses";
                                            } elseif ($v["Title"] == "Non-operating Income") {
                                                $budgetTitle = "Other Income";
                                            } elseif ($v["Title"] == OPERATING_EXPENSES) {
                                                $v["Title"] = LESS_OPERATING_EXPENSES;
                                                $budgetTitle = "Operating Expenses";
                                            } else {
                                                $budgetTitle = trim($v["Title"]);
                                            }
                                            if (isset($budget[$budgetTitle][$v1["Cells"][0]["Value"]])) {
                                                if (in_array(trim($v1["Cells"][0]["Value"]), array_keys($budget[$budgetTitle]))) {
                                                    $v1["Cells"][2]["Value"] = round($budget[$budgetTitle][$v1["Cells"][0]["Value"]]);
                                                    unset($budget[$budgetTitle][$v1["Cells"][0]["Value"]]);
                                                } else {
                                                    $v1["Cells"][2]["Value"] = 0; //"-";
                                                }
                                            } else {
                                                $v1["Cells"][2]["Value"] = 0; //"-";
                                            }
                                            $v1["Cells"][3]["Value"] = 0.0;
                                            $v1["Cells"][4]["Value"] = 0.0;
                                            foreach ($v1["Cells"] as $k2 => &$v2) {
                                                unset($v2["Attributes"]);
                                                if (($k2 % 2) == 0 && $k2 != 0) {
                                                    $v1["Cells"][3]["Value"] = round(abs($v1["Cells"][1]["Value"]) - abs($v1["Cells"][2]["Value"]));
                                                    if ($v1["Cells"][2]["Value"] != 0 || $v1["Cells"][2]["Value"] != '0.0') {
                                                        $v1["Cells"][4]["Value"] = round(($v1["Cells"][3]["Value"] / abs($v1["Cells"][2]["Value"])) * 100);
                                                    } else {
                                                        $v1["Cells"][4]["Value"] = 0;
                                                    }
                                                }
                                            }
                                        } else {
                                            $v["Title"] = trim($v1["Cells"][0]["Value"]);
                                            $key = $v1["Cells"][0]["Value"];
                                            if (isset($budget["summaryRow"][$v["Title"]])) {
                                                if (in_array($key, array_keys($budget["summaryRow"]))) {
                                                    $v1["Cells"][2]["Value"] = round($budget["summaryRow"][$key]);
                                                } else {
                                                    $v1["Cells"][2]["Value"] = 0; //"-";
                                                }
                                            } else {
                                                $v1["Cells"][2]["Value"] = 0;
                                            }
                                            $v1["Cells"][3]["Value"] = round($v1["Cells"][1]["Value"] - $v1["Cells"][2]["Value"]);
                                            if ($v1["Cells"][2]["Value"] != 0 || $v1["Cells"][2]["Value"] != '0.0') {
                                                $v1["Cells"][4]["Value"] = round(($v1["Cells"][3]["Value"] / $v1["Cells"][2]["Value"]) * 100);
                                            } else {
                                                $v1["Cells"][4]["Value"] = 0;
                                            }
                                            $v1["Cells"]["no_expand"] = true;
                                        }
                                        $pnlSummary[$v["Title"]][$v1["Cells"][0]["Value"]] = $v1["Cells"];
                                    } else if ($v1["RowType"] == "SummaryRow") {
                                        $key = trim($v1["Cells"][0]["Value"]);
                                        $total = 0;

                                        if (isset($budget[$budgetTitle][$v1["Cells"][0]["Value"]])) {
                                            if (in_array(trim($v1["Cells"][0]["Value"]), array_keys($budget[$budgetTitle]))) {
                                                $v1["Cells"][2]["Value"] = round($budget[$budgetTitle][$v1["Cells"][0]["Value"]]);
                                                unset($budget[$budgetTitle][$v1["Cells"][0]["Value"]]);
                                            } else {
                                                $v1["Cells"][2]["Value"] = 0; //"-";
                                            }
                                        } else {
                                            $v1["Cells"][2]["Value"] = 0; //"-";
                                        }
                                        $v1["Cells"][3]["Value"] = round(abs($v1["Cells"][1]["Value"]) - abs($v1["Cells"][2]["Value"]));
                                        if ($v1["Cells"][2]["Value"] != 0 || $v1["Cells"][2]["Value"] != '0.0') {
                                            $v1["Cells"][4]["Value"] = round(($v1["Cells"][3]["Value"] / abs($v1["Cells"][2]["Value"])) * 100);
                                        } else {
                                            $v1["Cells"][4]["Value"] = 0;
                                        }
                                        //    $v1["Cells"]["no_expand"] = true;

                                        if (strlen(trim($v["Title"])) > 0) {
                                            $pnlSummary[$v["Title"]]["summaryRow"][$v1["Cells"][0]["Value"]] = $v1["Cells"];
                                        } else {
                                            if ($v1["Cells"][0]["Value"] != TOTAL_OPERATING_EXPENSES) {
                                                $pnlSummary[$v1["Cells"][0]["Value"]]["no_expand"] = true;
                                                $pnlSummary[$v1["Cells"][0]["Value"]][$k] = array('date' => $date_of_month, 'amount' => $v1["Cells"], 'month' => $month, 'year' => $year);
                                            } else {
                                                $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$v1["Cells"][0]["Value"]] = $v1["Cells"];
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }

                $emp = "Employee Compensation & Related Expenses";

                foreach ($budget as $k2 => $v2) {
                    $summaryBudget = 0;
                    $varpercentage = 0;
                    $new = 0;
                    if ($k2 == "Operating Expenses") {
                        $k2 = LESS_OPERATING_EXPENSES;
                    } elseif ($k2 == "Other Income") {
                        $k2 = "Non-operating Income";
                    }
                    foreach ($v2 as $k3 => $v3) {
                        if ($v3 == 0) {
                            $varpercentage = 0;
                        } else {
                            $varpercentage = '-100';
                        }

                        if ($v3 > 0) {
                            $pnlSummary[$k2][] = array(array("Value" => $k3), array("Value" => 0), array("Value" => $v3), array("Value" => '-' . $v3), array("Value" => $varpercentage));
                        }
                    }
                }

                if (count($pnlSummary) > 0) {

                    if (isset($pnlSummary[$emp])) {
                        $pnlSummary[LESS_OPERATING_EXPENSES][$emp] = $pnlSummary[$emp];
                        unset($pnlSummary[$emp]);
                        if (sizeof($pnlSummary[LESS_OPERATING_EXPENSES][$emp]) > 0) {
                            $expKey = "Total Employee Compensation & Related Expenses";
                            $lessKey = "Total Operating Expenses";
                            $summate = round($pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$lessKey][2]["Value"] + $pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][$expKey][2]["Value"]);
                            $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$lessKey][2]["Value"] = round($pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$lessKey][2]["Value"] + $pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][$expKey][2]["Value"]);
                            $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$lessKey][3]["Value"] = round($pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$lessKey][1]["Value"] - $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$lessKey][2]["Value"]);
                            if ($pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$lessKey][2]["Value"] != '0') {
                                $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$lessKey][4]["Value"] = round(($pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$lessKey][3]["Value"] / $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$lessKey][2]["Value"]) * 100);
                            } else {
                                $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$lessKey][4]["Value"] = 0;
                            }
                            $pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][$expKey][3]["Value"] = round($pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][$expKey][1]["Value"] - $pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][$expKey][2]["Value"]);
                            if ($pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][$expKey][2]["Value"] != '0') {
                                $pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][$expKey][4]["Value"] = round(($pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][$expKey][3]["Value"] / $pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][$expKey][2]["Value"]) * 100);
                            } else {
                                $pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][$expKey][4]["Value"] = 0;
                            }
                        }
                    }

                    $netValues = array(GROSS_PROFIT, OPERATING_PROFIT, NET_PROFIT);

                    foreach ($netValues as $k => $v) {
                        switch ($v) {
                            case GROSS_PROFIT:
                                $incomeActualValue = (isset($pnlSummary[INCOME])) ? $pnlSummary[INCOME]["summaryRow"]["Total Income"][1]["Value"] : 0;
                                $saleActualValue = (isset($pnlSummary["Less Cost of Sales"])) ? $pnlSummary["Less Cost of Sales"]["summaryRow"]["Total Cost of Sales"][1]["Value"] : 0;
                                $incomeBudgetValue = (isset($pnlSummary[INCOME])) ? $pnlSummary[INCOME]["summaryRow"]["Total Income"][2]["Value"] : 0;
                                $saleBudgetValue = (isset($pnlSummary["Less Cost of Sales"])) ? $pnlSummary["Less Cost of Sales"]["summaryRow"]["Total Cost of Sales"][2]["Value"] : 0;
                                $pnlSummary[$v][$v][1]["Value"] = $incomeActualValue - $saleActualValue;
                                $pnlSummary[$v][$v][2]["Value"] = $incomeBudgetValue - $saleBudgetValue;
                                break;
                            case OPERATING_PROFIT:
                                if (!isset($pnlSummary[OPERATING_PROFIT])) {
                                    $pnlSummary[$v][$v][0]["Value"] = OPERATING_PROFIT;
                                    $pnlSummary[$v][$v][1]["Value"] = 0;
                                }
                                if (!isset($pnlSummary[LESS_OPERATING_EXPENSES][$emp])) {
                                    $grossActualValue = (isset($pnlSummary[GROSS_PROFIT])) ? $pnlSummary[GROSS_PROFIT]["Gross Profit"][1]["Value"] : 0;
                                    $lessOpActualValue = (isset($pnlSummary[LESS_OPERATING_EXPENSES])) ? $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"]["Total Operating Expenses"][1]["Value"] : 0;
                                    $grossBudgetValue = (isset($pnlSummary[GROSS_PROFIT])) ? $pnlSummary[GROSS_PROFIT]["Gross Profit"][2]["Value"] : 0;
                                    $lessOpBudgetValue = (isset($pnlSummary[LESS_OPERATING_EXPENSES])) ? $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"]["Total Operating Expenses"][2]["Value"] : 0;
                                    $pnlSummary[$v][$v][1]["Value"] = $grossActualValue - $lessOpActualValue;
                                    $pnlSummary[$v][$v][2]["Value"] = $grossBudgetValue - $lessOpBudgetValue;
                                } else {
                                    $pnlSummary[$v][$v][1]["Value"] = $pnlSummary[GROSS_PROFIT]["Gross Profit"][1]["Value"] - $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"]["Total Operating Expenses"][1]["Value"];
                                    $pnlSummary[$v][$v][2]["Value"] = $pnlSummary[GROSS_PROFIT]["Gross Profit"][2]["Value"] - $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"]["Total Operating Expenses"][2]["Value"];
                                }
                                break;
                            case NET_PROFIT:
                                if (!isset($pnlSummary[NET_PROFIT])) {
                                    $pnlSummary[$v][$v][0]["Value"] = NET_PROFIT;
                                    $pnlSummary[$v][$v][1]["Value"] = 0;
                                }
                                $ActualoperatingProfit = (isset($pnlSummary["Operating Profit"])) ? $pnlSummary["Operating Profit"]["Operating Profit"][1]["Value"] : 0;
                                $nonOpIncomeActualValue = (isset($pnlSummary["Non-operating Income"])) ? $pnlSummary["Non-operating Income"]["summaryRow"]["Total Non-operating Income"][1]["Value"] : 0;
                                $nonOpExpenseActualValue = (isset($pnlSummary["Non-operating Expenses"])) ? $pnlSummary["Non-operating Expenses"]["summaryRow"]["Total Non-operating Expenses"][1]["Value"] : 0;

                                $BudgetoperatingProfit = (isset($pnlSummary["Operating Profit"])) ? $pnlSummary["Operating Profit"]["Operating Profit"][2]["Value"] : 0;
                                $nonOpIncomeBudgetValue = (isset($pnlSummary["Non-operating Income"])) ? $pnlSummary["Non-operating Income"]["summaryRow"]["Total Non-operating Income"][2]["Value"] : 0;
                                $nonOpExpenseBudgetValue = (isset($pnlSummary["Non-operating Expenses"])) ? $pnlSummary["Non-operating Expenses"]["summaryRow"]["Total Non-operating Expenses"][2]["Value"] : 0;
                                $pnlSummary[$v][$v][1]["Value"] = $ActualoperatingProfit + $nonOpIncomeActualValue - $nonOpExpenseActualValue;
                                $pnlSummary[$v][$v][2]["Value"] = $BudgetoperatingProfit + $nonOpIncomeBudgetValue - $nonOpExpenseBudgetValue;
                                break;
                        }
                        $pnlSummary[$v][$v][3]["Value"] = $pnlSummary[$v][$v][1]["Value"] - $pnlSummary[$v][$v][2]["Value"];
                        if ($pnlSummary[$v][$v][2]["Value"] != '0') {
                            if ($pnlSummary[$v][$v][3]["Value"] > 0) {
                                $pnlSummary[$v][$v][4]["Value"] = round(abs($pnlSummary[$v][$v][3]["Value"] / $pnlSummary[$v][$v][2]["Value"] * 100));
                            } else {
                                $pnlSummary[$v][$v][4]["Value"] = round($pnlSummary[$v][$v][3]["Value"] / $pnlSummary[$v][$v][2]["Value"] * 100);
                            }
                        } else {
                            $pnlSummary[$v][$v][4]["Value"] = 0;
                        }
                    }

                    $finalArray = array();
                    $finalArray[INCOME] = (sizeof($pnlSummary[INCOME]) > 0) ? $pnlSummary[INCOME] : [];
                    $finalArray["Less Cost of Sales"] = (sizeof($pnlSummary["Less Cost of Sales"]) > 0) ? $pnlSummary["Less Cost of Sales"] : [];
                    $finalArray[GROSS_PROFIT] = (sizeof($pnlSummary[GROSS_PROFIT]) > 0) ? $pnlSummary[GROSS_PROFIT] : [];
                    $finalArray["Less Operating Expenses"] = (sizeof($pnlSummary[LESS_OPERATING_EXPENSES]) > 0) ? $pnlSummary[LESS_OPERATING_EXPENSES] : [];
                    $finalArray[OPERATING_PROFIT] = (sizeof($pnlSummary[OPERATING_PROFIT]) > 0) ? $pnlSummary[OPERATING_PROFIT] : [];
                    $finalArray[NON_OPERATING_INCOME] = (sizeof($pnlSummary[NON_OPERATING_INCOME]) > 0) ? $pnlSummary[NON_OPERATING_INCOME] : [];
                    $finalArray[NON_OPERATING_EXPENSE] = (sizeof($pnlSummary[NON_OPERATING_EXPENSE]) > 0) ? $pnlSummary[NON_OPERATING_EXPENSE] : [];
                    $finalArray[NET_PROFIT] = (sizeof($pnlSummary[NET_PROFIT]) > 0) ? $pnlSummary[NET_PROFIT] : [];
                    $totalIncome = isset($pnlSummary[INCOME]["summaryRow"]["Total Income"][1]["Value"]) ? $pnlSummary[INCOME]["summaryRow"]["Total Income"][1]["Value"] : 0;

                    $finalArrayData = $group->utf8ize($finalArray);

                    $aPNLReport["error"] = false;
                    $aPNLReport["message"] = RECORD_FOUND;
                    $aPNLReport["TotalIncome"] = $totalIncome;
                    $aPNLReport["org_name"] = $company_name;

                    if ($params["mtd"] == 1) {
                        $aPNLReport["consolidatedPNL_mtd"] = $finalArrayData;
                    } else if ($params["mtd"] == 2) {
                        $aPNLReport["consolidatedPNL_ytd"] = $finalArrayData;
                    } else {
                        $aPNLReport["consolidatedPNL_yty"] = $finalArrayData;
                    }
                }
            } else {
                $aPNLReport["error"] = true;
                $aPNLReport["message"] = NO_RECORD_FOUND;
            }
            $pnlDetails = null;
            $pnlSummary = null;
            $utility = null;
            return $aPNLReport;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getBudgetDetailsFromDB($client_master_id, $searchItems)
    {
        try {
            $searchItems['client_master_id'] = $client_master_id;

            $sql = "SELECT client_budget_id, c.client_master_id, org_name, c.client_category_id1, c.sub_category_id1, c.client_category_id2, c.sub_category_id2, line_type, item_head, line_item, month_jan, month_feb, month_march, month_april, month_may, month_june, month_july, month_aug, month_sept, month_oct, month_nov, month_dec, total_values FROM client_budget c WHERE  ";

            foreach ($searchItems as $key => $value) {
                switch ($key) {
                    case 'client_master_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "c.client_master_id = ? ";
                        break;
                    case 'sub_category_id2':
                        $a_param_type[] = 's';
                        $a_bind_params[] = $value;
                        $query[] = "c.sub_category_id2 = ? ";
                        break;
                    case 'sub_category_id1':
                        $a_param_type[] = 's';
                        $a_bind_params[] = $value;
                        $query[] = "c.sub_category_id1 = ? ";
                        break;
                    case 'line_type':
                        $a_param_type[] = 's';
                        $a_bind_params[] = $value;
                        $query[] = "c.line_type = ? ";
                        break;
                    case 'item_head':
                        $a_param_type[] = 's';
                        $a_bind_params[] = $value;
                        $query[] = "c.item_head = ? ";
                        break;
                }
            }
            $sql .= implode(' AND ', $query);
            $param_type = '';
            $n = count($a_param_type);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $a_param_type[$i];
            }
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                $a_params[] = &$a_bind_params[$i];
            }

            $response["budgetSummary"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                call_user_func_array(array($stmt, 'bind_param'), $a_params);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($client_budget_id, $client_master_id, $org_name, $client_category_id1, $sub_category_id1, $client_category_id2, $sub_category_id2, $line_type, $item_head, $line_item, $month_jan, $month_feb, $month_march, $month_april, $month_may, $month_june, $month_july, $month_aug, $month_sept, $month_oct, $month_nov, $month_dec, $total_values);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["client_budget_id"] = $client_budget_id;
                        $tmp["client_master_id"] = $client_master_id;
                        $tmp["org_name"] = $org_name;
                        $tmp["territory_id"] = $sub_category_id1;
                        $tmp["brand_id"] = $sub_category_id2;
                        $tmp["line_type"] = trim($line_type);
                        $tmp["item_head"] = trim($item_head);
                        $tmp["line_item"] = trim($line_item);
                        $tmp["month_jan"] = $this->formatDigits($month_jan);
                        $tmp["month_feb"] = $this->formatDigits($month_feb);
                        $tmp["month_march"] = $this->formatDigits($month_march);
                        $tmp["month_april"] = $this->formatDigits($month_april);
                        $tmp["month_may"] = $this->formatDigits($month_may);
                        $tmp["month_june"] = $this->formatDigits($month_june);
                        $tmp["month_july"] = $this->formatDigits($month_july);
                        $tmp["month_aug"] = $this->formatDigits($month_aug);
                        $tmp["month_sept"] = $this->formatDigits($month_sept);
                        $tmp["month_oct"] = $this->formatDigits($month_oct);
                        $tmp["month_nov"] = $this->formatDigits($month_nov);
                        $tmp["month_dec"] = $this->formatDigits($month_dec);
                        $tmp["total_values"] = $this->formatDigits($total_values);
                        $tmp["brand_name"] = $this->getSubCategoryName($client_category_id2, $sub_category_id2);
                        $tmp["territory_name"] = $this->getSubCategoryName($client_category_id1, $sub_category_id1);
                        $response["budgetSummary"][] = $tmp;
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getSubCategoryName($client_category_id, $sub_category_id)
    {
        try {
            $sql = "SELECT sub_category_name FROM category_subcategory WHERE client_category_id = ? and sub_category_id = ?";

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("is", $client_category_id, $sub_category_id);
                if ($stmt->execute()) {
                    $stmt->store_result();
                    $stmt->bind_result($sub_category_name);
                    $stmt->fetch();
                    $stmt->close();
                    return $sub_category_name;
                } else {
                    return null;
                }
            } else {
                return null;
            }
            return $sub_category_name;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function formatDigits($value)
    {
        if (strpos($value, '(') !== false) {
            $value = trim(preg_replace('/[(]/s', '-', $value));
            $value = trim(preg_replace('/[)]/s', '', $value));
        }
        return trim(preg_replace('/[^0-9.-]/s', '', $value));
    }

    public function getTrendPNL($client_id, $XeroOAuth, $ajax, $params, $is_admin, $dates, $report_start_date, $client_master_id)
    {
        try {
            $aPNLTrendReport = array();
            $pnlResponse = array();
            $pnlSummary = array();
            $dateArray = array();
            $trackParams = array();
            $aParams = array();
            $bParams = array();
            $searchItems = array();
            $goAhead = true;

            $searchItems['client_id'] = $client_id;
            $searchItems['client_master_id'] = $client_master_id;

            if ($is_admin == 1) {
                $goAhead = true;
                if (sizeof($params) > 0) {
                    if (isset($params["sub_category_id1"])) {
                        if ($params['sub_category_id1'] != TERRITORY_ID) {
                            $searchItems['sub_category_id'] = $params["sub_category_id1"];
                            $aParams = $this->getAllTrackingCategories($searchItems);
                        } else {
                            $aParams["error"] = true;
                        }
                    } else {
                        $aParams["error"] = true;
                    }
                    if (isset($params["sub_category_id2"])) {
                        if ($params['sub_category_id2'] != TERRITORY_ID) {
                            $searchItems['sub_category_id'] = $params["sub_category_id2"];
                            $bParams = $this->getAllTrackingCategories($searchItems);
                        } else {
                            $bParams["error"] = true;
                        }
                    } else {
                        $bParams["error"] = true;
                    }
                }
            } else {

                $netParams = array();
                $searchItems["groupby"] = 1;
                $netParams = $this->getAllTrackingCategories($searchItems);

                if ($netParams["error"] == false) {
                    $keys = array_keys($netParams["trackingCategories"]);
                    $a = sizeof($keys);
                    if ($a == 1) {
                        $aParams["trackingCategories"][0]["category_id"] = $netParams["trackingCategories"][$keys[0]][0]["category_id"];
                        $aParams["error"] = false;
                        $params["sub_category_id1"] = $netParams["trackingCategories"][$keys[0]][0]["sub_category_id"];
                        $bParams["error"] = true;
                        $goAhead = true;
                    } else {
                        $aParams["error"] = false;
                        $bParams["error"] = false;
                        $aParams["trackingCategories"][0]["category_id"] = $netParams["trackingCategories"][$keys[0]][0]["category_id"];
                        $bParams["trackingCategories"][0]["category_id"] = $netParams["trackingCategories"][$keys[1]][0]["category_id"];
                        $params["sub_category_id1"] = $netParams["trackingCategories"][$keys[0]][0]["sub_category_id"];
                        $params["sub_category_id2"] = $netParams["trackingCategories"][$keys[1]][0]["sub_category_id"];
                        $goAhead = true;
                    }
                } else {
                    $aParams["error"] = true;
                    $bParams["error"] = true;
                    $goAhead = false;
                }
            }

            if ($goAhead == true) {

                if (($aParams["error"] == false) && ($bParams["error"] == false)) {
                    $trackParams = array("trackingCategoryID" => $aParams["trackingCategories"][0]["category_id"], "trackingOptionID" => $params["sub_category_id1"], "trackingCategoryID2" => $bParams["trackingCategories"][0]["category_id"], "trackingOptionID2" => $params["sub_category_id2"]);
                } else if (($aParams["error"] == false) && ($bParams["error"] == true)) {
                    $trackParams = array("trackingCategoryID" => $aParams["trackingCategories"][0]["category_id"], "trackingOptionID" => $params["sub_category_id1"]);
                } else if (($aParams["error"] == true) && ($bParams["error"] == false)) {
                    $trackParams = array("trackingCategoryID" => $bParams["trackingCategories"][0]["category_id"], "trackingOptionID" => $params["sub_category_id2"]);
                }

                foreach ($dates as $k => $v) {
                    if (is_numeric($k)) {

                       

                        $dateArray = array("fromDate" => $v["firstdate"], "toDate" => $v["lastdate"]);
                        if (sizeof($trackParams) > 0) {
                            $dateArray = array_merge($dateArray, $trackParams);
                        }

                        $response = $this->makeRequest($client_id, 'getTrendPNL', $XeroOAuth,'GET', PROFIT_LOSS_API, $dateArray, "", "json",$client_master_id);
                        $pnlResponse = $XeroOAuth->response;
                        /*$filename = "../include/logs/log_" . $client_id . "_trend.txt";
                        $content = "DATE TIME : "  . date("Y-m-d h:i:s") . " \n";
                        $content .= " Report : Trend P & L" . " \n";
                        $content .= " PARAMS : " . json_encode($dateArray). " \n";
                        $content .= " DATA : " . json_encode($pnlResponse);
                        $content .= "\n" . "________________________________________________________________" . "\n";
                        file_put_contents($filename, trim($content . "\n"), FILE_APPEND);*/

                        if ($pnlResponse['code'] == 302) {
                            $pnlResponse["error"] = true;
                            $pnlResponse["message"] = XERO_HAVING_PROBLEM;
                            return $pnlResponse;
                        } else if ($pnlResponse['code'] == 401) {
                            if (strpos($pnlResponse['response'], 'token_expired') !== false || strpos($pnlResponse['response'], 'token_rejected') !== false) {
                                if (isset($ajax) && ($ajax == true)) {
                                    $aPNLTrendReport["expiry"] = true;
                                    $aPNLTrendReport['error'] = true;
                                    $aPNLTrendReport['message'] = TOKEN_EXPIRED;
                                    return $aPNLTrendReport;
                                } else {
                                    $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                                    if (isset($resp['authurl'])) {
                                        $authurl = $resp['authurl'];
                                        header('Location:' . $authurl);die();
                                    }
                                }
                            }
                        } else if ($pnlResponse['code'] == 200) {
                            $aPLSummaryList = array();
                            $pnlDetails = array();
                            $aPLSummaryList = json_decode($XeroOAuth->response['response'], true);
                            $company_name = $aPLSummaryList['Reports'][0]['ReportTitles'][1];
                            $pnlDetails = $aPLSummaryList['Reports'][0]['Rows'];
                            $oper = 0;

                            if (sizeof($pnlDetails) > 0) {

                                /*
					$filename = "../include/logs/log_" . $client_id . "_trend.txt";
	                                $content = "DATE TIME : "  . date("Y-m-d h:i:s") . " \n";
	                                $content .= " Report : Trend P & L" . " \n";
	                                $content .= " MONTH : " . $v["firstdate"] . " \n";
	                                $content .= " DATA : " . json_encode($pnlDetails);
	                                $content .= "\n" . "________________________________________________________________" . "\n";
	                                file_put_contents($filename, trim($content . "\n"), FILE_APPEND);
                                */

                                foreach ($pnlDetails as $k1 => $v1) {
                                    if ($v1["Title"] == NON_OPERATING_EXPENSE) {
                                        foreach ($v1["Rows"] as $k5 => $v5) {
                                            if ($v5["RowType"] == "SummaryRow") {
                                                $oper += -$v5["Cells"][1]["Value"];
                                            }
                                        }
                                    }
                                    if ($v1["Title"] == NON_OPERATING_INCOME) {
                                        foreach ($v1["Rows"] as $k5 => $v5) {
                                            if ($v5["RowType"] == "SummaryRow") {
                                                $oper += $v5["Cells"][1]["Value"];
                                            }
                                        }
                                    }

                                    if ($v1["RowType"] == "Header") {
                                        $date_of_month = $v1["Cells"][1]["Value"];
                                        $date = date_create_from_format('j M y', $date_of_month);
                                        $month = date_format($date, 'n');
                                        $year = date_format($date, 'y');
                                        $day = date_format($date, 'd');
                                    }
                                    switch ($v1["RowType"]) {
                                        case "Section":
                                            foreach ($v1["Rows"] as $k2 => $v2) {
                                                if (sizeof($v2["Cells"]) > 0) {
                                                    if ($v2["RowType"] == "Row") {
                                                        foreach ($v2["Cells"] as $k3 => &$v3) {
                                                            if ($k3 % 2 == 0 && strlen($v3["Value"]) > 0) {
                                                            } else {
                                                                if (strlen(trim($v1["Title"])) > 0) {
                                                                    if ($v1["Title"] == EMPLOYEE_COMPENSATION) {
                                                                        $pnlSummary[LESS_OPERATING_EXPENSES][$v1["Title"]][$v2["Cells"][$k3 - 1]["Value"]][$k] = array('date' => $date_of_month, 'amount' => $v3["Value"], 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                                                                    } else if ($v1["Title"] == OPERATING_EXPENSES) {
                                                                        $pnlSummary[LESS_OPERATING_EXPENSES][$v2["Cells"][$k3 - 1]["Value"]][$k] = array('date' => $date_of_month, 'amount' => $v3["Value"], 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);

                                                                        if ($v1["Title"] == OPERATING_EXPENSES) {
                                                                            if ($k != 0) {
                                                                                $date_of_ind_rec = date_create($date_of_month);
                                                                                $date_of_ind_rec = date_format($date_of_ind_rec, "Y-m-01");
                                                                                if ($date_of_ind_rec >= $report_start_date) {
                                                                                    $pnlSummary[OPERATING_EXPENSES][$v2["Cells"][$k3 - 1]["Value"]] += $v3["Value"];
                                                                                }
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $pnlSummary[$v1["Title"]][$v2["Cells"][$k3 - 1]["Value"]][$k] = array('date' => $date_of_month, 'amount' => $v3["Value"], 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                                                                        /*Code added by Ramesh on 06-11-2017
                                                                         * For Top 10 expenses of YTD
                                                                         */
                                                                        if ($v1["Title"] == LESS_OPERATING_EXPENSES) {
                                                                            if ($k != 0) {
                                                                                $date_of_ind_rec = date_create($date_of_month);
                                                                                $date_of_ind_rec = date_format($date_of_ind_rec, "Y-m-01");
                                                                                if ($date_of_ind_rec >= $report_start_date) {
                                                                                    $pnlSummary[OPERATING_EXPENSES][$v2["Cells"][$k3 - 1]["Value"]] += $v3["Value"];
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    $pnlSummary[$v2["Cells"][0]["Value"]]["no_expand"] = true;
                                                                    $pnlSummary[$v2["Cells"][0]["Value"]][$k] = array('date' => $date_of_month, 'amount' => $v3["Value"], 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                                                                }
                                                            }
                                                            unset($v3["Attributes"]);
                                                        }
                                                    } else if ($v2["RowType"] == "SummaryRow") {
                                                        foreach ($v2["Cells"] as $k3 => &$v3) {
                                                            if (strlen(trim($v1["Title"])) > 0) {
                                                                if ($v1["Title"] == EMPLOYEE_COMPENSATION) {
                                                                    $pnlSummary[LESS_OPERATING_EXPENSES][$v1["Title"]]["summaryRow"][$v2["Cells"][0]["Value"]][$k] = array('date' => $date_of_month, 'amount' => $v3["Value"], 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                                                                    if ($k != 0) {
                                                                        $date_of_ind_rec = date_create($date_of_month);
                                                                        $date_of_ind_rec = date_format($date_of_ind_rec, "Y-m-01");
                                                                        if ($date_of_ind_rec >= $report_start_date) {
                                                                            $pnlSummary[OPERATING_EXPENSES][$v2["Cells"][0]["Value"]] += $v3["Value"];
                                                                        }
                                                                    }
                                                                } else if ($v1["Title"] == OPERATING_EXPENSES) {
                                                                    $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$v2["Cells"][0]["Value"]][$k] = array('date' => $date_of_month, 'amount' => ($v3["Value"]), 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                                                                } else if ($v1["Title"] == OTHER_INCOME_EXPENSE) {
                                                                    $v2["Cells"][0]["Value"] = TOTAL_OTHER_INCOME_EXPENSE;
                                                                    $pnlSummary[$v1["Title"]]["summaryRow"][$v2["Cells"][0]["Value"]][$k] = array('date' => $date_of_month, 'amount' => $oper, 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                                                                } else {
                                                                    $pnlSummary[$v1["Title"]]["summaryRow"][$v2["Cells"][0]["Value"]][$k] = array('date' => $date_of_month, 'amount' => ($v3["Value"]), 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                                                                }
                                                            } else {
                                                                if ($v2["Cells"][0]["Value"] != TOTAL_OPERATING_EXPENSES) {
                                                                    $pnlSummary[$v2["Cells"][0]["Value"]]["no_expand"] = true;
                                                                    $pnlSummary[$v2["Cells"][0]["Value"]][$k] = array('date' => $date_of_month, 'amount' => $v3["Value"], 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                                                                } else {
                                                                    $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$v2["Cells"][0]["Value"]][$k] = array('date' => $date_of_month, 'amount' => $v3["Value"], 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $aPNLTrendReport["error"] = true;
                $aPNLTrendReport["message"] = NO_RECORD_FOUND;
                $aParams = null;
                $bParams = null;
                $searchItems = null;
                $searchParams = null;
                $netParams = null;
                $trackParams = null;
                return $aPNLTrendReport;
            }

            ////Added this code for populating Operating profit for current month if not return from API service
            $GPCount = sizeof($pnlSummary[GROSS_PROFIT]);
            $OPCount = sizeof($pnlSummary[OPERATING_PROFIT]);
            if (isset($pnlSummary[LESS_OPERATING_EXPENSES][summaryRow]["Total Operating Expenses"])) {
                $TOE_array = $pnlSummary[LESS_OPERATING_EXPENSES][summaryRow]["Total Operating Expenses"];
                $TOECount = sizeof($pnlSummary[LESS_OPERATING_EXPENSES][summaryRow]["Total Operating Expenses"]);
            } else {
                $TOECount = 0;
            }

            if (($OPCount < $GPCount) || ($OPCount == 0)) {
                foreach ($pnlSummary[GROSS_PROFIT] as $key => $value) {
                    if ($key == 'no_expand') {
                        $pnlSummary[OPERATING_PROFIT][$key] = $value;
                    } else {
                        if ($TOECount > 0) {
                            $date = $value['date'];
                            $month = $value['month'];
                            $year = $value['year'];
                            $getInnKey = $this->searchIdFromArray($month . '-' . $year, $TOE_array);
                            if (sizeof($getInnKey) > 0) {
                                $total_amount = $value['amount'] - $TOE_array[$getInnKey]['amount'];
                                $pnlSummary[OPERATING_PROFIT][$key] = array('date' => $date, 'amount' => $total_amount, 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                            } else {
                                $pnlSummary[OPERATING_PROFIT][$key] = array('date' => $date, 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                            }
                        } else {
                            $pnlSummary[OPERATING_PROFIT][$key] = array('date' => $date, 'amount' => $value['amount'], 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                        }
                    }
                }
            }

            ////Added this code for populating Net profit for current month if not return from API service
            $NPCount = sizeof($pnlSummary[NET_PROFIT]);
            if (isset($pnlSummary[NON_OPERATING_INCOME][summaryRow]["Total Non-operating Income"])) {
                $NOI_array = $pnlSummary[NON_OPERATING_INCOME][summaryRow]["Total Non-operating Income"];
                $NOICount = sizeof($pnlSummary[NON_OPERATING_INCOME][summaryRow]["Total Non-operating Income"]);
            } else {
                $NOICount = 0;
            }
            if (isset($pnlSummary[NON_OPERATING_EXPENSE][summaryRow]["Total Non-operating Expenses"])) {
                $NOE_array = $pnlSummary[NON_OPERATING_EXPENSE][summaryRow]["Total Non-operating Expenses"];
                $NOECount = sizeof($pnlSummary[NON_OPERATING_EXPENSE][summaryRow]["Total Non-operating Expenses"]);
            } else {
                $NOECount = 0;
            }

            if ($NPCount <= 0) {
                foreach ($pnlSummary[OPERATING_PROFIT] as $key => $value) {
                    if ($key == 'no_expand') {
                        $pnlSummary[NET_PROFIT][$key] = $value;
                    } else {
                        $date = $value['date'];
                        $month = $value['month'];
                        $year = $value['year'];
                        $total_amount = 0;
                        $OP_amount = $value['amount'];
                        $NOI_amount = 0;
                        $NOE_amount = 0;

                        if ($NOICount > 0) {
                            $getNOIKey = $this->searchIdFromArray($month . '-' . $year, $NOI_array);
                            if (sizeof($getNOIKey) > 0) {
                                $NOI_amount = $NOI_array[$getNOIKey]['amount'];
                            }
                        }

                        if ($NOECount > 0) {
                            $getNOEKey = $this->searchIdFromArray($month . '-' . $year, $NOE_array);
                            if (sizeof($getNOEKey) > 0) {
                                $NOE_amount = $NOE_array[$getNOEKey]['amount'];
                            }
                        }

                        $total_amount = $NOI_amount - (-($NOE_amount + $OP_amount));
                        $pnlSummary[NET_PROFIT][$key] = array('date' => $date, 'amount' => $total_amount, 'month' => $month, 'year' => $year, 'id' => $month . '-' . $year);
                    }
                }
            }

            if (sizeof($pnlSummary) > 0) {
                if (isset($pnlSummary[EMPLOYEE_COMPENSATION])) {
                    unset($pnlSummary[EMPLOYEE_COMPENSATION]);
                }

                $finalArray = array();
                $tmp = array();
                $finalArray[INCOME] = (sizeof($pnlSummary[INCOME]) > 0) ? $pnlSummary[INCOME] : [];
                $finalArray["Less Cost of Sales"] = (sizeof($pnlSummary["Less Cost of Sales"]) > 0) ? $pnlSummary["Less Cost of Sales"] : [];
                $finalArray[GROSS_PROFIT] = (sizeof($pnlSummary[GROSS_PROFIT]) > 0) ? $pnlSummary[GROSS_PROFIT] : [];
                $finalArray[LESS_OPERATING_EXPENSES] = (sizeof($pnlSummary[LESS_OPERATING_EXPENSES]) > 0) ? $pnlSummary[LESS_OPERATING_EXPENSES] : [];
                $finalArray[OPERATING_PROFIT] = (sizeof($pnlSummary[OPERATING_PROFIT]) > 0) ? $pnlSummary[OPERATING_PROFIT] : [];
                $finalArray[NON_OPERATING_INCOME] = (sizeof($pnlSummary[NON_OPERATING_INCOME]) > 0) ? $pnlSummary[NON_OPERATING_INCOME] : [];
                $finalArray[NON_OPERATING_EXPENSE] = (sizeof($pnlSummary[NON_OPERATING_EXPENSE]) > 0) ? $pnlSummary[NON_OPERATING_EXPENSE] : [];
                $finalArray[NET_PROFIT] = (sizeof($pnlSummary[NET_PROFIT]) > 0) ? $pnlSummary[NET_PROFIT] : [];

                $quarters = $this->buildQuartelyMonthArray();

                $finalArrayQuartely = array();
                $array_flag = 1;
                foreach ($finalArray as $k1 => $v1) {
                    foreach ($v1 as $k2 => $v2) {
                        if (is_array($v2) && !array_key_exists('date', $v2)) {
                            foreach ($v2 as $k3 => $v3) {
                                if (is_array($v3) && !array_key_exists('date', $v3)) {
                                    foreach ($v3 as $k4 => $v4) {
                                        if (is_array($v4) && !array_key_exists('date', $v4)) {
                                            $finalArrayQuartely[$k1][$k2][$k3][$k4] = $this->buildQuartelyRecord($v4, $quarters);
                                        } else {
                                            $array_flag = 0;
                                            break;
                                        }
                                    }
                                    if ($array_flag == 0) {
                                        $finalArrayQuartely[$k1][$k2][$k3] = $this->buildQuartelyRecord($v3, $quarters);
                                        $array_flag = 1;
                                    }
                                } else {
                                    $array_flag = 0;
                                    break;
                                }
                            }
                            if ($array_flag == 0) {
                                $finalArrayQuartely[$k1][$k2] = $this->buildQuartelyRecord($v2, $quarters);
                                $array_flag = 1;
                            }
                        } else {
                            $array_flag = 0;
                            break;
                        }
                    }
                    if ($array_flag == 0) {
                        $finalArrayQuartely[$k1] = $this->buildQuartelyRecord($v1, $quarters);
                        $array_flag = 1;
                    }
                }

                if (sizeof($pnlSummary[OPERATING_EXPENSES]) > 0) {
                    arsort($pnlSummary[OPERATING_EXPENSES]);
                    $finalArray[OPERATING_EXPENSES] = $pnlSummary[OPERATING_EXPENSES];
                } else {
                    $finalArray[OPERATING_EXPENSES] = [];
                }

                $finalArray["trendPNL_Quartely"] = $finalArrayQuartely;

                $aPNLTrendReport["error"] = false;
                $aPNLTrendReport["message"] = RECORD_FOUND;
                $aPNLTrendReport["trendPNL"]["org_name"] = $company_name;
                $aPNLTrendReport["trendPNL"] = $finalArray;
            }

            $aBrand = null;
            $aTerritory = null;
            $dateArray = null;
            $trackParams = null;
            return $aPNLTrendReport;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function buildQuartelyMonthArray()
    {
        $current_month_no = date("n");
        $quarters = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]];

        $key = $this->searchForId($current_month_no, $quarters);

        $startDate = date('Y-') . $quarters[$key][0] . '-01';
        $endDate = date("Y-m-01", strtotime("-12 months"));

        $finalquarters = array();
        $i = 4;
        while (strtotime($endDate) <= strtotime($startDate)) {
            $month_no = date('n', strtotime($startDate));
            $key = $this->searchForId($month_no, $quarters);
            $monthyear = array();
            foreach ($quarters[$key] as $key1 => $val1) {
                $monthyear[] = array('month' => $val1, 'year' => date('y', strtotime($startDate)));
            }
            $finalquarters[$i] = $monthyear;
            $startDate = date('Y-m-01', strtotime($startDate . '- 3 month'));
            $i = $i - 1;
        }
        return $finalquarters;
    }

    public function searchForId($id, $array)
    {
        foreach ($array as $key => $val) {
            $key_to_return = $key;
            foreach ($val as $key1 => $val1) {
                if ($val1 == $id) {
                    return $key_to_return;
                }
            }
        }
        return null;
    }

    /*Searches array match for Month & Year, finally returns key*/
    public function searchIdFrom2dimensionalArray($id, $array)
    {
        $monthYear = explode("-", $id);
        $month = (int) $monthYear[0];
        $year = (int) $monthYear[1];
        foreach ($array as $key => $val) {
            $key_to_return = $key;
            foreach ($val as $key1 => $val1) {
                if ($val1['month'] == $month && $val1['year'] == $year) {
                    return $key_to_return;
                }
            }
        }
        return null;
    }

    public function buildQuartelyRecord($record, $quarters)
    {
        try {
            $data_result_array = array();
            $amount_quar1 = 0;
            $amount_quar2 = 0;
            $amount_quar3 = 0;
            $amount_quar4 = 0;

            foreach ($record as $key => $value) {
                if ($key != 0) {
                    $formated_date = date("n-y", strtotime($value['date']));
                    $quarter_flag = $this->searchIdFrom2dimensionalArray($formated_date, $quarters);

                    if ($quarter_flag == 1) {
                        $amount_quar1 = $amount_quar1 + $value['amount'];
                    } else if ($quarter_flag == 2) {
                        $amount_quar2 = $amount_quar2 + $value['amount'];
                    } else if ($quarter_flag == 3) {
                        $amount_quar3 = $amount_quar3 + $value['amount'];
                    } else if ($quarter_flag == 4) {
                        $amount_quar4 = $amount_quar4 + $value['amount'];
                    }
                } else if (strcasecmp($key, 'no_expand') == 0) {
                    $data_result_array[$key] = $value;
                }
            }

            if ($amount_quar1 != 0) {
                $data_result_array[1] = $amount_quar1;
            } else {
                $data_result_array[1] = $amount_quar1;
            }

            if ($amount_quar2 != 0) {
                $data_result_array[2] = $amount_quar2;
            } else {
                $data_result_array[2] = $amount_quar2;
            }

            if ($amount_quar3 != 0) {
                $data_result_array[3] = $amount_quar3;
            } else {
                $data_result_array[3] = $amount_quar3;
            }

            if ($amount_quar4 != 0) {
                $data_result_array[4] = $amount_quar4;
            } else {
                $data_result_array[4] = $amount_quar4;
            }
            return $data_result_array;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    // public function buildQuartelyRecord($record, $report_start_date)
    // {
    //     try {
    //         $data_result_array = array();
    //         $amount_quar1 = 0;
    //         $amount_quar2 = 0;
    //         $amount_quar3 = 0;
    //         $amount_quar4 = 0;

    //         $startDate = date("Y-m-01");

    //         foreach ($record as $key => $value) {
    //             if ($key != 0) {
    //                 $formated_date = date("Y-m", strtotime($value['date'])) . "-01";
    //                 $quarter_flag = abs($this->dateDifference($startDate, $formated_date));

    //                 if ($quarter_flag <= 3) {
    //                     $amount_quar1 = $amount_quar1 + $value['amount'];
    //                 } else if ($quarter_flag <= 6) {
    //                     $amount_quar2 = $amount_quar2 + $value['amount'];
    //                 } else if ($quarter_flag <= 9) {
    //                     $amount_quar3 = $amount_quar3 + $value['amount'];
    //                 } else if ($quarter_flag <= 12) {
    //                     $amount_quar4 = $amount_quar4 + $value['amount'];
    //                 }
    //             } else if (strcasecmp($key, 'no_expand') == 0) {
    //                 $data_result_array[$key] = $value;
    //             }
    //         }

    //         if ($amount_quar1 != 0) {
    //             $data_result_array[1] = $amount_quar1;
    //         }

    //         if ($amount_quar2 != 0) {
    //             $data_result_array[2] = $amount_quar2;
    //         }

    //         if ($amount_quar3 != 0) {
    //             $data_result_array[3] = $amount_quar3;
    //         }

    //         if ($amount_quar4 != 0) {
    //             $data_result_array[4] = $amount_quar4;
    //         }

    //         return $data_result_array;
    //     } catch (Exception $e) {
    //         echo $e->getMessage();
    //     }
    // }

    public function dateDifference($date_1, $date_2)
    {
        $ts1 = strtotime($date_1);
        $ts2 = strtotime($date_2);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
        return $diff;
    }

    public function CurrentQuarter($n)
    {
        $quarter_result = 0;
        if ($n < 4) {
            $quarter_result = 1;
        } elseif ($n > 3 && $n < 7) {
            $quarter_result = 2;
        } elseif ($n > 6 && $n < 10) {
            $quarter_result = 3;
        } elseif ($n > 9) {
            $quarter_result = 4;
        }
        return $quarter_result;
    }

    public function getCashFlowStatement($profitNLoss, $balanceSheet, $client_id = null, $dates)
    {
        try {

            $response = array();

            $netIncomeActual = array();
            $netIncome = array();
            $depreciationActual = array();
            $depreciation = array();
            $saleoffixedassetsActual = array();
            $saleoffixedassets = array();
            $tradeReceivableActual = array();
            $tradeReceivable = array();
            $totalOtherAssetsActual = array();
            $totalOtherAssets = array();
            $accountsPayableActual = array();
            $accountsPayable = array();
            $otherCurrentLiabilitiesActual = array();
            $otherCurrentLiabilities = array();
            $plantDepActual = array();
            $plantDep = array();
            $interCompanyReceivablesActual = array();
            $interCompanyReceivables = array();
            $interCompanyPayablesActual = array();
            $interCompanyPayables = array();
            $otherMovementsinWorkCap = array();

            $finalOtherAssets = array();
            $finalCurrentLiabilities = array();
            $finalInterCompanyReceivables = array();
            $finalInterCompanyPayables = array();
            $finalplantDep = array();
            $netCashFlow = array();

            $plantActual = array();
            $plant = array();
            $officeEquipmentActual = array();
            $officeEquipment = array();
            $intangibleAssetsActual = array();
            $intangibleAssets = array();
            $investmentInSubsidaryActual = array();
            $investmentInSubsidary = array();
            $otherNonCurrentLiabilitiesActual = array();
            $otherNonCurrentLiabilities = array();

            $finalplant = array();
            $finalofficeEquipment = array();
            $finalPPPE = array();
            $finalIntangibleAssets = array();
            $finalInvestmentInSubsidary = array();
            $finalOtherNonCurrentLiabilities = array();

            $advancesFromParentCompanyActual = array();
            $advancesFromParentCompany = array();
            $shareCapitalActual = array();
            $shareCapital = array();

            $abcCashActual = array();
            $abcCash = array();
            $fixedAssets = array();

            $finalCurrentAssets = array();
            $finalAdvances = array();
            $finalShareCapital = array();
            $totalCashFromFinanceActivities = array();
            $netIncreaseInCash = array();
            $cashAndCashEq = array();
            $netCashEquivalent = array();

            $netIncomeActual = $profitNLoss["trendPNL"]["Net Profit"];

            if (isset($profitNLoss["trendPNL"][NON_OPERATING_EXPENSE]["Depreciation & Amortisation"])) {
                $depreciationActual = $profitNLoss["trendPNL"][NON_OPERATING_EXPENSE]["Depreciation & Amortisation"];
            } else {
                $depreciationActual = $profitNLoss["trendPNL"][NON_OPERATING_EXPENSE]["Depreciation"];
            }
            $saleoffixedassetsActual = $profitNLoss["trendPNL"][NON_OPERATING_INCOME]["Other Income - Gain (Loss) on Sale of Fixed Assets"];
            $tradeReceivableActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Accounts Receivable');

            $totalOtherAssetsActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Other assets');
            $accountsPayableActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Accounts Payable');
            $otherCurrentLiabilitiesActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Other Current Liabilities');
            $plantActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Plant & Equipment');
            $officeEquipmentActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Office Equipment');
            $interCompanyReceivablesActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Intercompany Receivables');
            $interCompanyPayablesActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Intercompany Payables');

            $plantDepActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Plant & Equipment: Less Accumulated Depreciation');
            if (sizeof($plantDepActual) == 0) {
                $plantDepActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Office Equipment: Less Accumulated Depreciation');
            }

            $intangibleAssetsActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Intangible Assets');
            $investmentInSubsidaryActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Investment in Subsidiary');
            $otherNonCurrentLiabilitiesActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Total Non-Current Liabilities');

            $advancesFromParentCompanyActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Owner Funds Introduced');
            $shareCapitalActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Share Capital');

            $abcCashActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Total Bank');

            /*********Need to clear doubt abcCash array************/
            $a = 0;
            $b = 0;
            $c = 0;
            $d = 0;
            $e = 0;
            $f = 0;
            $g = 0;
            $h = 0;
            $i = 0;
            $j = 0;
            $l = 0;
            $m = 0;
            $n = 0;
            $o = 0;
            $p = 0;
            $q = 0;
            $r = 0;
            $s = 0;
            $t = 0;

            foreach ($dates as $k => $v) {
                if (is_numeric($k)) {

                    $month = date('n', strtotime($v['firstdate']));
                    $year = date('y', strtotime($v['firstdate']));

                    if ($month != "" && $year != "") {
                        $id = $month . '-' . $year;
                    } else {
                        $id = '';
                    }

                    if ((sizeof($netIncomeActual) > 0) && ($a == 0)) {
                        $netIncomeKey = $this->searchIdFromArray($month . '-' . $year, $netIncomeActual);
                        if (sizeof($netIncomeKey) > 0) {
                            $netIncome[$k] = array('date' => $netIncomeActual[$netIncomeKey]['date'], 'amount' => $netIncomeActual[$netIncomeKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $netIncome[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $a = 1;
                        $netIncome[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($depreciationActual) > 0) && ($b == 0)) {
                        $depreciationKey = $this->searchIdFromArray($month . '-' . $year, $depreciationActual);
                        if (sizeof($depreciationKey) > 0) {
                            $depreciation[$k] = array('date' => $depreciationActual[$depreciationKey]['date'], 'amount' => $depreciationActual[$depreciationKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $depreciation[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $b = 1;
                        $depreciation[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($saleoffixedassetsActual) > 0) && ($t == 0)) {
                        $saleoffixedassetsKey = $this->searchIdFromArray($month . '-' . $year, $saleoffixedassetsActual);
                        if (sizeof($saleoffixedassetsKey) > 0) {
                            $saleoffixedassets[$k] = array('date' => $saleoffixedassetsActual[$saleoffixedassetsKey]['date'], 'amount' => $saleoffixedassetsActual[$saleoffixedassetsKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $saleoffixedassets[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $t = 1;
                        $saleoffixedassets[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($tradeReceivableActual) > 0) && ($c == 0)) {
                        $tradeReceivableKey = $this->searchIdFromArray($month . '-' . $year, $tradeReceivableActual);
                        if (count($tradeReceivableKey) > 0) {
                            $tradeReceivable[$k] = array('date' => $tradeReceivableActual[$tradeReceivableKey]['date'], 'amount' => $tradeReceivableActual[$tradeReceivableKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $tradeReceivable[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $c = 1;
                        $tradeReceivable[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($totalOtherAssetsActual) > 0) && ($d == 0)) {
                        $totalOtherAssetsKey = $this->searchIdFromArray($month . '-' . $year, $totalOtherAssetsActual);
                        if (sizeof($totalOtherAssetsKey) > 0) {
                            $totalOtherAssets[$k] = array('date' => $totalOtherAssetsActual[$totalOtherAssetsKey]['date'], 'amount' => $totalOtherAssetsActual[$totalOtherAssetsKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $totalOtherAssets[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $d = 1;
                        $totalOtherAssets[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($accountsPayableActual) > 0) && ($q == 0)) {
                        $accountsPayableKey = $this->searchIdFromArray($month . '-' . $year, $accountsPayableActual);
                        if (sizeof($accountsPayableKey) > 0) {
                            $accountsPayable[$k] = array('date' => $accountsPayableActual[$accountsPayableKey]['date'], 'amount' => $accountsPayableActual[$accountsPayableKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $accountsPayable[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $q = 1;
                        $accountsPayable[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($otherCurrentLiabilitiesActual) > 0) && ($r == 0)) {
                        $otherCurrentLiabilitiesKey = $this->searchIdFromArray($month . '-' . $year, $otherCurrentLiabilitiesActual);
                        if (sizeof($otherCurrentLiabilitiesKey) > 0) {
                            $otherCurrentLiabilities[$k] = array('date' => $otherCurrentLiabilitiesActual[$otherCurrentLiabilitiesKey]['date'], 'amount' => $otherCurrentLiabilitiesActual[$otherCurrentLiabilitiesKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $otherCurrentLiabilities[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $r = 1;
                        $otherCurrentLiabilities[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($interCompanyReceivablesActual) > 0) && ($o == 0)) {
                        $interCompanyReceivablesKey = $this->searchIdFromArray($month . '-' . $year, $interCompanyReceivablesActual);
                        if (sizeof($interCompanyReceivablesKey) > 0) {
                            $interCompanyReceivables[$k] = array('date' => $interCompanyReceivablesActual[$interCompanyReceivablesKey]['date'], 'amount' => $interCompanyReceivablesActual[$interCompanyReceivablesKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $interCompanyReceivables[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $o = 1;
                        $interCompanyReceivables[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($interCompanyPayablesActual) > 0) && ($p == 0)) {
                        $interCompanyPayablesKey = $this->searchIdFromArray($month . '-' . $year, $interCompanyPayablesActual);
                        if (sizeof($interCompanyPayablesKey) > 0) {
                            $interCompanyPayables[$k] = array('date' => $interCompanyPayablesActual[$interCompanyPayablesKey]['date'], 'amount' => $interCompanyPayablesActual[$interCompanyPayablesKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $interCompanyPayables[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $p = 1;
                        $interCompanyPayables[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($plantDepActual) > 0) && ($f == 0)) {
                        $plantDepKey = $this->searchIdFromArray($month . '-' . $year, $plantDepActual);
                        if (sizeof($plantDepKey) > 0) {
                            $plantDep[$k] = array('date' => $plantDepActual[$plantDepKey]['date'], 'amount' => $plantDepActual[$plantDepKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $plantDep[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $f = 1;
                        $plantDep[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($plantActual) > 0) && ($g == 0)) {
                        $plantKey = $this->searchIdFromArray($month . '-' . $year, $plantActual);
                        if (sizeof($plantKey) > 0) {
                            $plant[$k] = array('date' => $plantActual[$plantKey]['date'], 'amount' => $plantActual[$plantKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $plant[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $g = 1;
                        $plant[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($officeEquipmentActual) > 0) && ($s == 0)) {
                        $officeEquipmentKey = $this->searchIdFromArray($month . '-' . $year, $officeEquipmentActual);
                        if (sizeof($officeEquipmentKey) > 0) {
                            $officeEquipment[$k] = array('date' => $officeEquipmentActual[$officeEquipmentKey]['date'], 'amount' => $officeEquipmentActual[$officeEquipmentKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $officeEquipment[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $s = 1;
                        $officeEquipment[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($intangibleAssetsActual) > 0) && ($h == 0)) {
                        $intangibleAssetsKey = $this->searchIdFromArray($month . '-' . $year, $intangibleAssetsActual);
                        if (sizeof($intangibleAssetsKey) > 0) {
                            $intangibleAssets[$k] = array('date' => $intangibleAssetsActual[$intangibleAssetsKey]['date'], 'amount' => $intangibleAssetsActual[$intangibleAssetsKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $intangibleAssets[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $h = 1;
                        $intangibleAssets[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($investmentInSubsidaryActual) > 0) && ($i == 0)) {
                        $investmentInSubsidaryKey = $this->searchIdFromArray($month . '-' . $year, $investmentInSubsidaryActual);
                        if (sizeof($investmentInSubsidaryKey) > 0) {
                            $investmentInSubsidary[$k] = array('date' => $investmentInSubsidaryActual[$investmentInSubsidaryKey]['date'], 'amount' => $investmentInSubsidaryActual[$investmentInSubsidaryKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $investmentInSubsidary[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $i = 1;
                        $investmentInSubsidary[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($otherNonCurrentLiabilitiesActual) > 0) && ($j == 0)) {
                        $otherNonCurrentLiabilitiesKey = $this->searchIdFromArray($month . '-' . $year, $otherNonCurrentLiabilitiesActual);
                        if (sizeof($otherNonCurrentLiabilitiesKey) > 0) {
                            $otherNonCurrentLiabilities[$k] = array('date' => $otherNonCurrentLiabilitiesActual[$otherNonCurrentLiabilitiesKey]['date'], 'amount' => $otherNonCurrentLiabilitiesActual[$otherNonCurrentLiabilitiesKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $otherNonCurrentLiabilities[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $j = 1;
                        $otherNonCurrentLiabilities[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($advancesFromParentCompanyActual) > 0) && ($l == 0)) {
                        $advancesFromParentCompanyKey = $this->searchIdFromArray($month . '-' . $year, $advancesFromParentCompanyActual);
                        if (sizeof($advancesFromParentCompanyKey) > 0) {
                            $advancesFromParentCompany[$k] = array('date' => $advancesFromParentCompanyActual[$advancesFromParentCompanyKey]['date'], 'amount' => $advancesFromParentCompanyActual[$advancesFromParentCompanyKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $advancesFromParentCompany[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $l = 1;
                        $advancesFromParentCompany[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($shareCapitalActual) > 0) && ($m == 0)) {
                        $shareCapitalKey = $this->searchIdFromArray($month . '-' . $year, $shareCapitalActual);
                        if (sizeof($shareCapitalKey) > 0) {
                            $shareCapital[$k] = array('date' => $shareCapitalActual[$shareCapitalKey]['date'], 'amount' => $shareCapitalActual[$shareCapitalKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $shareCapital[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $m = 1;
                        $shareCapital[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($abcCashActual) > 0) && ($n == 0)) {
                        $abcCashKey = $this->searchIdFromArray($month . '-' . $year, $abcCashActual);
                        if (sizeof($abcCashKey) > 0) {
                            $abcCash[$k] = array('date' => $abcCashActual[$abcCashKey]['date'], 'amount' => $abcCashActual[$abcCashKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $abcCash[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $n = 1;
                        $abcCash[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }
                }
            }

            foreach ($dates as $k => $v) {
                if (is_numeric($k)) {

                    $month = date('n', strtotime($v['firstdate']));
                    $year = date('y', strtotime($v['firstdate']));

                    if ($month != "" && $year != "") {
                        $id = $month . '-' . $year;
                    } else {
                        $id = '';
                    }

                    if ($k != 0) {
                        $finalTradeReceivable[$k] = array('amount' => (round($tradeReceivable[$k - 1]['amount'] - $tradeReceivable[$k]['amount'], 2)), 'month' => $tradeReceivable[$k]['month'], 'year' => $tradeReceivable[$k]['year'], 'id' => $tradeReceivable[$k]['month'] . '-' . $tradeReceivable[$k]['year']);
                    } else {
                        $finalTradeReceivable[$k] = array('amount' => (round($tradeReceivable[$k]['amount'], 2)), 'month' => $tradeReceivable[$k]['month'], 'year' => $tradeReceivable[$k]['year'], 'id' => $tradeReceivable[$k]['month'] . '-' . $tradeReceivable[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalOtherAssets[$k] = array('amount' => (round($totalOtherAssets[$k - 1]['amount'] - $totalOtherAssets[$k]['amount'], 2)), 'month' => $totalOtherAssets[$k]['month'], 'year' => $totalOtherAssets[$k]['year'], 'id' => $totalOtherAssets[$k]['month'] . '-' . $totalOtherAssets[$k]['year']);
                    } else {
                        $finalOtherAssets[$k] = array('amount' => (round($totalOtherAssets[$k]['amount'], 2)), 'month' => $totalOtherAssets[$k]['month'], 'year' => $totalOtherAssets[$k]['year'], 'id' => $totalOtherAssets[$k]['month'] . '-' . $totalOtherAssets[$k]['year']);
                    }

                    $finalCurrentLiabilities[$k] = array('amount' => (round(($accountsPayable[$k]['amount'] + $otherCurrentLiabilities[$k]['amount']) - ($accountsPayable[$k - 1]['amount'] + $otherCurrentLiabilities[$k - 1]['amount']), 2)), 'month' => $accountsPayable[$k]['month'], 'year' => $accountsPayable[$k]['year'], 'id' => $accountsPayable[$k]['month'] . '-' . $accountsPayable[$k]['year']);

                    if ($k != 0) {
                        $finalInterCompanyReceivables[$k] = array('amount' => (round($interCompanyReceivables[$k - 1]['amount'] - $interCompanyReceivables[$k]['amount'], 2)), 'month' => $interCompanyReceivables[$k]['month'], 'year' => $interCompanyReceivables[$k]['year'], 'id' => $interCompanyReceivables[$k]['month'] . '-' . $interCompanyReceivables[$k]['year']);
                    } else {
                        $finalInterCompanyReceivables[$k] = array('amount' => (round($interCompanyReceivables[$k]['amount'], 2)), 'month' => $interCompanyReceivables[$k]['month'], 'year' => $interCompanyReceivables[$k]['year'], 'id' => $interCompanyReceivables[$k]['month'] . '-' . $interCompanyReceivables[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalInterCompanyPayables[$k] = array('amount' => (round($interCompanyPayables[$k]['amount'] - $interCompanyPayables[$k - 1]['amount'], 2)), 'month' => $interCompanyPayables[$k]['month'], 'year' => $interCompanyPayables[$k]['year'], 'id' => $interCompanyPayables[$k]['month'] . '-' . $interCompanyPayables[$k]['year']);
                    } else {
                        $finalInterCompanyPayables[$k] = array('amount' => (round($interCompanyPayables[$k]['amount'], 2)), 'month' => $interCompanyPayables[$k]['month'], 'year' => $interCompanyPayables[$k]['year'], 'id' => $interCompanyPayables[$k]['month'] . '-' . $interCompanyPayables[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalplantDep[$k] = array('amount' => (round($plantDep[$k - 1]['amount'] - $plantDep[$k]['amount'], 2)), 'month' => $plantDep[$k]['month'], 'year' => $plantDep[$k]['year'], 'id' => $plantDep[$k]['month'] . '-' . $plantDep[$k]['year']);
                    } else {
                        $finalplantDep[$k] = array('amount' => (round($plantDep[$k]['amount'], 2)), 'month' => $plantDep[$k]['month'], 'year' => $plantDep[$k]['year'], 'id' => $plantDep[$k]['month'] . '-' . $plantDep[$k]['year']);
                    }

                    $otherMovementsinWorkCap[$k] = array('amount' => (round($saleoffixedassets[$k]['amount'] + $finalInterCompanyReceivables[$k]['amount'] + ($finalplantDep[$k]['amount'] - $depreciation[$k]['amount']) + $finalInterCompanyPayables[$k]['amount'], 2)), 'month' => $depreciation[$k]['month'], 'year' => $depreciation[$k]['year'], 'id' => $depreciation[$k]['month'] . '-' . $depreciation[$k]['year']);

                    $netCashFlow[$k] = array('amount' => (round($netIncome[$k]['amount'] + $depreciation[$k]['amount'] + $finalTradeReceivable[$k]['amount'] + $finalOtherAssets[$k]['amount'] + $finalCurrentLiabilities[$k]['amount'] + $otherMovementsinWorkCap[$k]['amount'], 2)), 'month' => $netIncome[$k]['month'], 'year' => $netIncome[$k]['year'], 'id' => $netIncome[$k]['month'] . '-' . $netIncome[$k]['year']);

                    if ($k != 0) {
                        $finalplant[$k] = array('amount' => (round($plant[$k - 1]['amount'] - $plant[$k]['amount'], 2)), 'month' => $plant[$k]['month'], 'year' => $plant[$k]['year'], 'id' => $plant[$k]['month'] . '-' . $plant[$k]['year']);
                    } else {
                        $finalplant[$k] = array('amount' => (round($plant[$k]['amount'], 2)), 'month' => $plant[$k]['month'], 'year' => $plant[$k]['year'], 'id' => $plant[$k]['month'] . '-' . $plant[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalofficeEquipment[$k] = array('amount' => (round($officeEquipment[$k - 1]['amount'] - $officeEquipment[$k]['amount'], 2)), 'month' => $officeEquipment[$k]['month'], 'year' => $officeEquipment[$k]['year'], 'id' => $officeEquipment[$k]['month'] . '-' . $officeEquipment[$k]['year']);
                    } else {
                        $finalofficeEquipment[$k] = array('amount' => (round($officeEquipment[$k]['amount'], 2)), 'month' => $officeEquipment[$k]['month'], 'year' => $officeEquipment[$k]['year'], 'id' => $officeEquipment[$k]['month'] . '-' . $officeEquipment[$k]['year']);
                    }

                    $finalPPPE[$k] = array('amount' => ($finalplant[$k]['amount'] + $finalofficeEquipment[$k]['amount']), 'month' => $finalplant[$k]['month'], 'year' => $finalplant[$k]['year'], 'id' => $finalplant[$k]['month'] . '-' . $finalplant[$k]['year']);

                    if ($k != 0) {
                        $finalIntangibleAssets[$k] = array('amount' => (round($intangibleAssets[$k - 1]['amount'] - $intangibleAssets[$k]['amount'], 2)), 'month' => $intangibleAssets[$k]['month'], 'year' => $intangibleAssets[$k]['year'], 'id' => $intangibleAssets[$k]['month'] . '-' . $intangibleAssets[$k]['year']);
                    } else {
                        $finalIntangibleAssets[$k] = array('amount' => (round($intangibleAssets[$k]['amount'], 2)), 'month' => $intangibleAssets[$k]['month'], 'year' => $intangibleAssets[$k]['year'], 'id' => $intangibleAssets[$k]['month'] . '-' . $intangibleAssets[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalInvestmentInSubsidary[$k] = array('amount' => (round($investmentInSubsidary[$k - 1]['amount'] - $investmentInSubsidary[$k]['amount'], 2)), 'month' => $investmentInSubsidary[$k]['month'], 'year' => $investmentInSubsidary[$k]['year'], 'id' => $investmentInSubsidary[$k]['month'] . '-' . $investmentInSubsidary[$k]['year']);
                    } else {
                        $finalInvestmentInSubsidary[$k] = array('amount' => (round($investmentInSubsidary[$k]['amount'], 2)), 'month' => $investmentInSubsidary[$k]['month'], 'year' => $investmentInSubsidary[$k]['year'], 'id' => $investmentInSubsidary[$k]['month'] . '-' . $investmentInSubsidary[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalOtherNonCurrentLiabilities[$k] = array('amount' => (round($otherNonCurrentLiabilities[$k]['amount'] - $otherNonCurrentLiabilities[$k - 1]['amount'], 2)), 'month' => $otherNonCurrentLiabilities[$k]['month'], 'year' => $otherNonCurrentLiabilities[$k]['year'], 'id' => $otherNonCurrentLiabilities[$k]['month'] . '-' . $otherNonCurrentLiabilities[$k]['year']);
                    } else {
                        $finalOtherNonCurrentLiabilities[$k] = array('amount' => (round($otherNonCurrentLiabilities[$k]['amount'], 2)), 'month' => $otherNonCurrentLiabilities[$k]['month'], 'year' => $otherNonCurrentLiabilities[$k]['year'], 'id' => $otherNonCurrentLiabilities[$k]['month'] . '-' . $otherNonCurrentLiabilities[$k]['year']);
                    }

                    $finalCurrentAssets[$k] = array('amount' => (round($finalPPPE[$k]['amount'] + $finalIntangibleAssets[$k]['amount'] + $finalInvestmentInSubsidary[$k]['amount'] + $finalOtherNonCurrentLiabilities[$k]['amount'], 2)), 'month' => $finalplant[$k]['month'], 'year' => $finalplant[$k]['year'], 'id' => $finalplant[$k]['month'] . '-' . $finalplant[$k]['year']);

                    if ($k != 0) {
                        $finalAdvances[$k] = array('amount' => ($advancesFromParentCompany[$k]['amount'] - $advancesFromParentCompany[$k - 1]['amount']), 'month' => $advancesFromParentCompany[$k]['month'], 'year' => $advancesFromParentCompany[$k]['year'], 'id' => $advancesFromParentCompany[$k]['month'] . '-' . $advancesFromParentCompany[$k]['year']);
                    } else {
                        $finalAdvances[$k] = array('amount' => ($advancesFromParentCompany[$k]['amount']), 'month' => $advancesFromParentCompany[$k]['month'], 'year' => $advancesFromParentCompany[$k]['year'], 'id' => $advancesFromParentCompany[$k]['month'] . '-' . $advancesFromParentCompany[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalShareCapital[$k] = array('amount' => ($shareCapital[$k]['amount'] - $shareCapital[$k - 1]['amount']), 'month' => $shareCapital[$k]['month'], 'year' => $shareCapital[$k]['year'], 'id' => $shareCapital[$k]['month'] . '-' . $shareCapital[$k]['year']);
                    } else {
                        $finalShareCapital[$k] = array('amount' => ($shareCapital[$k]['amount']), 'month' => $shareCapital[$k]['month'], 'year' => $shareCapital[$k]['year'], 'id' => $shareCapital[$k]['month'] . '-' . $shareCapital[$k]['year']);
                    }

                    $totalCashFromFinanceActivities[$k] = array('amount' => ($finalAdvances[$k]['amount'] + $finalShareCapital[$k]['amount']), 'month' => $finalAdvances[$k]['month'], 'year' => $finalAdvances[$k]['year'], 'id' => $finalAdvances[$k]['month'] . '-' . $finalAdvances[$k]['year']);

                    $netIncreaseInCash[$k] = array('amount' => ($netCashFlow[$k]['amount'] + $finalCurrentAssets[$k]['amount'] + $totalCashFromFinanceActivities[$k]['amount']), 'month' => $month, 'year' => $year, 'id' => $id);

                    if (!isset($abcCash[$k - 1])) {
                        $cashAndCashEq[$k] = array('amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    } else {
                        $cashAndCashEq[$k] = array('amount' => $abcCash[$k - 1]['amount'], 'month' => $abcCash[$k]['month'], 'year' => $abcCash[$k]['year'], 'id' => $abcCash[$k]['month'] . '-' . $abcCash[$k]['year']);
                    }

                    $netCashEquivalent[$k] = array('amount' => ($cashAndCashEq[$k]['amount'] + $netIncreaseInCash[$k]['amount']), 'month' => $month, 'year' => $year, 'id' => $id);
                }
            }

            $response["Cash Flows from Operating Activities"][] = array("title" => "Net Profit", "value" => $netIncome);
            $response["Cash Flows from Operating Activities"][] = array("title" => "Depreciation & Amortisation", "value" => $depreciation);
            $response["Cash Flows from Operating Activities"][] = array("title" => "Accounts Receivable", "value" => $finalTradeReceivable);
            $response["Cash Flows from Operating Activities"][] = array("title" => "Other Assets", "value" => $finalOtherAssets);
            $response["Cash Flows from Operating Activities"][] = array("title" => "Accounts Payables & Other Current Liabilities", "value" => $finalCurrentLiabilities);
            $response["Cash Flows from Operating Activities"][] = array("title" => "Other Movements in Working Capital", "value" => $otherMovementsinWorkCap);
            $response["Cash Flows from Operating Activities"]["summaryRow"] = array("title" => "Net Cash Flow from operating activities", "value" => $netCashFlow);
            $response["Cash Flows from Investing Activities"][] = array("title" => "Purchase of Property, Plant & Equipment", "value" => $finalPPPE);
            $response["Cash Flows from Investing Activities"][] = array("title" => "Investment in intangible Assets", "value" => $finalIntangibleAssets);
            $response["Cash Flows from Investing Activities"][] = array("title" => "Investment in Subsidiaries", "value" => $finalInvestmentInSubsidary);
            $response["Cash Flows from Investing Activities"][] = array("title" => "Other Non-Current Liabilities", "value" => $finalOtherNonCurrentLiabilities);
            $response["Cash Flows from Investing Activities"]["summaryRow"] = array("title" => "Total Cash Flows from investing activities", "value" => $finalCurrentAssets);
            $response["Cash Flows from Financial Activities"][] = array("title" => "Owner Funds Introduced", "value" => $finalAdvances);
            $response["Cash Flows from Financial Activities"][] = array("title" => "Share Capital", "value" => $finalShareCapital);
            $response["Cash Flows from Financial Activities"]["summaryRow"] = array("title" => "Total Cash Flows from Financing activities", "value" => $totalCashFromFinanceActivities);
            $response["Cash and Cash Equivalents"][] = array("title" => "Net Increase (decrease) in cash & cash equivalents", "value" => $netIncreaseInCash);
            $response["Cash and Cash Equivalents"][] = array("title" => "Cash & cash equivalents at beginning of period", "value" => $cashAndCashEq);
            $response["Cash and Cash Equivalents"]["summaryRow"] = array("title" => "Net cash & cash equivalents at end of period", "value" => $netCashEquivalent, "no_expand" => true);

            $netIncomeActual = null;
            $netIncome = null;
            $depreciationActual = null;
            $depreciation = null;
            $saleoffixedassetsActual = null;
            $saleoffixedassets = null;
            $tradeReceivableActual = null;
            $tradeReceivable = null;
            $totalOtherAssetsActual = null;
            $totalOtherAssets = null;
            $accountsPayableActual = null;
            $accountsPayable = null;
            $otherCurrentLiabilitiesActual = null;
            $otherCurrentLiabilities = null;
            $plantDepActual = null;
            $plantDep = null;
            $interCompanyReceivablesActual = null;
            $interCompanyReceivables = null;
            $interCompanyPayablesActual = null;
            $interCompanyPayables = null;
            $otherMovementsinWorkCap = null;

            $finalOtherAssets = null;
            $finalCurrentLiabilities = null;
            $finalInterCompanyReceivables = null;
            $finalInterCompanyPayables = null;
            $netCashFlow = null;

            $plantActual = null;
            $plant = null;
            $officeEquipmentActual = null;
            $officeEquipment = null;
            $intangibleAssetsActual = null;
            $intangibleAssets = null;
            $investmentInSubsidaryActual = null;
            $investmentInSubsidary = null;
            $otherNonCurrentLiabilitiesActual = null;
            $otherNonCurrentLiabilities = null;

            $finalplant = null;
            $finalofficeEquipment = null;
            $finalPPPE = null;
            $finalIntangibleAssets = null;
            $finalInvestmentInSubsidary = null;
            $finalOtherNonCurrentLiabilities = null;

            $advancesFromParentCompanyActual = null;
            $advancesFromParentCompany = null;
            $shareCapitalActual = null;
            $shareCapital = null;

            $abcCashActual = null;
            $abcCash = null;
            $fixedAssets = null;

            $finalCurrentAssets = null;
            $finalAdvances = null;
            $finalShareCapital = null;
            $totalCashFromFinanceActivities = null;
            $netIncreaseInCash = null;
            $cashAndCashEq = null;
            $netCashEquivalent = null;

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getGroupCashFlowStatement($profitNLoss, $balanceSheet, $client_id = null, $dates)
    {
        try {

            $response = array();

            $netIncomeActual = array();
            $netIncome = array();
            $depreciationActual = array();
            $depreciation = array();
            $saleoffixedassetsActual = array();
            $saleoffixedassets = array();
            $tradeReceivableActual = array();
            $tradeReceivable = array();
            $totalOtherAssetsActual = array();
            $totalOtherAssets = array();
            $accountsPayableActual = array();
            $accountsPayable = array();
            $otherCurrentLiabilitiesActual = array();
            $otherCurrentLiabilities = array();
            $plantDepActual = array();
            $plantDep = array();
            $interCompanyReceivablesActual = array();
            $interCompanyReceivables = array();
            $interCompanyPayablesActual = array();
            $interCompanyPayables = array();
            $otherMovementsinWorkCap = array();

            $finalOtherAssets = array();
            $finalCurrentLiabilities = array();
            $finalInterCompanyReceivables = array();
            $finalInterCompanyPayables = array();
            $finalplantDep = array();
            $netCashFlow = array();

            $plantActual = array();
            $plant = array();
            $officeEquipmentActual = array();
            $officeEquipment = array();
            $intangibleAssetsActual = array();
            $intangibleAssets = array();
            $investmentInSubsidaryActual = array();
            $investmentInSubsidary = array();
            $otherNonCurrentLiabilitiesActual = array();
            $otherNonCurrentLiabilities = array();

            $finalplant = array();
            $finalofficeEquipment = array();
            $finalPPPE = array();
            $finalIntangibleAssets = array();
            $finalInvestmentInSubsidary = array();
            $finalOtherNonCurrentLiabilities = array();

            $advancesFromParentCompanyActual = array();
            $advancesFromParentCompany = array();
            $shareCapitalActual = array();
            $shareCapital = array();

            $abcCashActual = array();
            $abcCash = array();
            $fixedAssets = array();

            $finalCurrentAssets = array();
            $finalAdvances = array();
            $finalShareCapital = array();
            $totalCashFromFinanceActivities = array();
            $netIncreaseInCash = array();
            $cashAndCashEq = array();
            $netCashEquivalent = array();

            $netIncomeActual = $profitNLoss["trendPNL"]["Net Profit"];

            if (isset($profitNLoss["trendPNL"][NON_OPERATING_EXPENSE]["Depreciation & Amortisation"])) {
                $depreciationActual = $profitNLoss["trendPNL"][NON_OPERATING_EXPENSE]["Depreciation & Amortisation"];
            } else {
                $depreciationActual = $profitNLoss["trendPNL"][NON_OPERATING_EXPENSE]["Depreciation"];
            }
            $saleoffixedassetsActual = $profitNLoss["trendPNL"][NON_OPERATING_INCOME]["Other Income - Gain (Loss) on Sale of Fixed Assets"];
            $tradeReceivableActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Accounts Receivable');

            $totalOtherAssetsActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Other assets');
            $accountsPayableActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Accounts Payable');
            $otherCurrentLiabilitiesActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Other Current Liabilities');
            $plantActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Plant & Equipment');
            $officeEquipmentActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Office Equipment');
            $interCompanyReceivablesActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Intercompany Receivables');
            $interCompanyPayablesActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Intercompany Payables');

            $plantDepActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Plant & Equipment: Less Accumulated Depreciation');
            if (sizeof($plantDepActual) == 0) {
                $plantDepActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Office Equipment: Less Accumulated Depreciation');
            }

            $intangibleAssetsActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Intangible Assets');
            $investmentInSubsidaryActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Investment in Subsidiary');
            $otherNonCurrentLiabilitiesActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Total Non-Current Liabilities');

            $advancesFromParentCompanyActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Owner Funds Introduced');
            $shareCapitalActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Share Capital');

            $abcCashActual = $this->array_find_deep_by_key($balanceSheet['balanceSheet'], 'Total Bank');

            /*********Need to clear doubt abcCash array************/
            $a = 0;
            $b = 0;
            $c = 0;
            $d = 0;
            $e = 0;
            $f = 0;
            $g = 0;
            $h = 0;
            $i = 0;
            $j = 0;
            $l = 0;
            $m = 0;
            $n = 0;
            $o = 0;
            $p = 0;
            $q = 0;
            $r = 0;
            $s = 0;
            $t = 0;

            foreach ($dates as $k => $v) {
                if (is_numeric($k)) {

                    $month = date('n', strtotime($v['firstdate']));
                    $year = date('y', strtotime($v['firstdate']));

                    if ($month != "" && $year != "") {
                        $id = $month . '-' . $year;
                    } else {
                        $id = '';
                    }

                    if ((sizeof($netIncomeActual) > 0) && ($a == 0)) {
                        $netIncomeKey = $this->searchIdFromArray($month . '-' . $year, $netIncomeActual);
                        if (sizeof($netIncomeKey) > 0) {
                            $netIncome[$k] = array('date' => $netIncomeActual[$netIncomeKey]['date'], 'amount' => $netIncomeActual[$netIncomeKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $netIncome[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $a = 1;
                        $netIncome[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($depreciationActual) > 0) && ($b == 0)) {
                        $depreciationKey = $this->searchIdFromArray($month . '-' . $year, $depreciationActual);
                        if (sizeof($depreciationKey) > 0) {
                            $depreciation[$k] = array('date' => $depreciationActual[$depreciationKey]['date'], 'amount' => $depreciationActual[$depreciationKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $depreciation[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $b = 1;
                        $depreciation[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($saleoffixedassetsActual) > 0) && ($t == 0)) {
                        $saleoffixedassetsKey = $this->searchIdFromArray($month . '-' . $year, $saleoffixedassetsActual);
                        if (sizeof($saleoffixedassetsKey) > 0) {
                            $saleoffixedassets[$k] = array('date' => $saleoffixedassetsActual[$saleoffixedassetsKey]['date'], 'amount' => $saleoffixedassetsActual[$saleoffixedassetsKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $saleoffixedassets[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $t = 1;
                        $saleoffixedassets[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($tradeReceivableActual) > 0) && ($c == 0)) {
                        $tradeReceivableKey = $this->searchIdFromArray($month . '-' . $year, $tradeReceivableActual);
                        if (sizeof($tradeReceivableKey) > 0) {
                            $tradeReceivable[$k] = array('date' => $tradeReceivableActual[$tradeReceivableKey]['date'], 'amount' => $tradeReceivableActual[$tradeReceivableKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $tradeReceivable[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $c = 1;
                        $tradeReceivable[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($totalOtherAssetsActual) > 0) && ($d == 0)) {
                        $totalOtherAssetsKey = $this->searchIdFromArray($month . '-' . $year, $totalOtherAssetsActual);
                        if (sizeof($totalOtherAssetsKey) > 0) {
                            $totalOtherAssets[$k] = array('date' => $totalOtherAssetsActual[$totalOtherAssetsKey]['date'], 'amount' => $totalOtherAssetsActual[$totalOtherAssetsKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $totalOtherAssets[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $d = 1;
                        $totalOtherAssets[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($accountsPayableActual) > 0) && ($q == 0)) {
                        $accountsPayableKey = $this->searchIdFromArray($month . '-' . $year, $accountsPayableActual);
                        if (sizeof($accountsPayableKey) > 0) {
                            $accountsPayable[$k] = array('date' => $accountsPayableActual[$accountsPayableKey]['date'], 'amount' => $accountsPayableActual[$accountsPayableKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $accountsPayable[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $q = 1;
                        $accountsPayable[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($otherCurrentLiabilitiesActual) > 0) && ($r == 0)) {
                        $otherCurrentLiabilitiesKey = $this->searchIdFromArray($month . '-' . $year, $otherCurrentLiabilitiesActual);
                        if (sizeof($otherCurrentLiabilitiesKey) > 0) {
                            $otherCurrentLiabilities[$k] = array('date' => $otherCurrentLiabilitiesActual[$otherCurrentLiabilitiesKey]['date'], 'amount' => $otherCurrentLiabilitiesActual[$otherCurrentLiabilitiesKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $otherCurrentLiabilities[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $r = 1;
                        $otherCurrentLiabilities[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($interCompanyReceivablesActual) > 0) && ($o == 0)) {
                        $interCompanyReceivablesKey = $this->searchIdFromArray($month . '-' . $year, $interCompanyReceivablesActual);
                        if (sizeof($interCompanyReceivablesKey) > 0) {
                            $interCompanyReceivables[$k] = array('date' => $interCompanyReceivablesActual[$interCompanyReceivablesKey]['date'], 'amount' => $interCompanyReceivablesActual[$interCompanyReceivablesKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $interCompanyReceivables[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $o = 1;
                        $interCompanyReceivables[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($interCompanyPayablesActual) > 0) && ($p == 0)) {
                        $interCompanyPayablesKey = $this->searchIdFromArray($month . '-' . $year, $interCompanyPayablesActual);
                        if (sizeof($interCompanyPayablesKey) > 0) {
                            $interCompanyPayables[$k] = array('date' => $interCompanyPayablesActual[$interCompanyPayablesKey]['date'], 'amount' => $interCompanyPayablesActual[$interCompanyPayablesKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $interCompanyPayables[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $p = 1;
                        $interCompanyPayables[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($plantDepActual) > 0) && ($f == 0)) {
                        $plantDepKey = $this->searchIdFromArray($month . '-' . $year, $plantDepActual);
                        if (sizeof($plantDepKey) > 0) {
                            $plantDep[$k] = array('date' => $plantDepActual[$plantDepKey]['date'], 'amount' => $plantDepActual[$plantDepKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $plantDep[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $f = 1;
                        $plantDep[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($plantActual) > 0) && ($g == 0)) {
                        $plantKey = $this->searchIdFromArray($month . '-' . $year, $plantActual);
                        if (sizeof($plantKey) > 0) {
                            $plant[$k] = array('date' => $plantActual[$plantKey]['date'], 'amount' => $plantActual[$plantKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $plant[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $g = 1;
                        $plant[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($officeEquipmentActual) > 0) && ($s == 0)) {
                        $officeEquipmentKey = $this->searchIdFromArray($month . '-' . $year, $officeEquipmentActual);
                        if (sizeof($officeEquipmentKey) > 0) {
                            $officeEquipment[$k] = array('date' => $officeEquipmentActual[$officeEquipmentKey]['date'], 'amount' => $officeEquipmentActual[$officeEquipmentKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $officeEquipment[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $s = 1;
                        $officeEquipment[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($intangibleAssetsActual) > 0) && ($h == 0)) {
                        $intangibleAssetsKey = $this->searchIdFromArray($month . '-' . $year, $intangibleAssetsActual);
                        if (sizeof($intangibleAssetsKey) > 0) {
                            $intangibleAssets[$k] = array('date' => $intangibleAssetsActual[$intangibleAssetsKey]['date'], 'amount' => $intangibleAssetsActual[$intangibleAssetsKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $intangibleAssets[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $h = 1;
                        $intangibleAssets[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($investmentInSubsidaryActual) > 0) && ($i == 0)) {
                        $investmentInSubsidaryKey = $this->searchIdFromArray($month . '-' . $year, $investmentInSubsidaryActual);
                        if (sizeof($investmentInSubsidaryKey) > 0) {
                            $investmentInSubsidary[$k] = array('date' => $investmentInSubsidaryActual[$investmentInSubsidaryKey]['date'], 'amount' => $investmentInSubsidaryActual[$investmentInSubsidaryKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $investmentInSubsidary[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $i = 1;
                        $investmentInSubsidary[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($otherNonCurrentLiabilitiesActual) > 0) && ($j == 0)) {
                        $otherNonCurrentLiabilitiesKey = $this->searchIdFromArray($month . '-' . $year, $otherNonCurrentLiabilitiesActual);
                        if (sizeof($otherNonCurrentLiabilitiesKey) > 0) {
                            $otherNonCurrentLiabilities[$k] = array('date' => $otherNonCurrentLiabilitiesActual[$otherNonCurrentLiabilitiesKey]['date'], 'amount' => $otherNonCurrentLiabilitiesActual[$otherNonCurrentLiabilitiesKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $otherNonCurrentLiabilities[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $j = 1;
                        $otherNonCurrentLiabilities[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($advancesFromParentCompanyActual) > 0) && ($l == 0)) {
                        $advancesFromParentCompanyKey = $this->searchIdFromArray($month . '-' . $year, $advancesFromParentCompanyActual);
                        if (sizeof($advancesFromParentCompanyKey) > 0) {
                            $advancesFromParentCompany[$k] = array('date' => $advancesFromParentCompanyActual[$advancesFromParentCompanyKey]['date'], 'amount' => $advancesFromParentCompanyActual[$advancesFromParentCompanyKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $advancesFromParentCompany[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $l = 1;
                        $advancesFromParentCompany[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($shareCapitalActual) > 0) && ($m == 0)) {
                        $shareCapitalKey = $this->searchIdFromArray($month . '-' . $year, $shareCapitalActual);
                        if (sizeof($shareCapitalKey) > 0) {
                            $shareCapital[$k] = array('date' => $shareCapitalActual[$shareCapitalKey]['date'], 'amount' => $shareCapitalActual[$shareCapitalKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $shareCapital[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $m = 1;
                        $shareCapital[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }

                    if ((sizeof($abcCashActual) > 0) && ($n == 0)) {
                        $abcCashKey = $this->searchIdFromArray($month . '-' . $year, $abcCashActual);
                        if (sizeof($abcCashKey) > 0) {
                            $abcCash[$k] = array('date' => $abcCashActual[$abcCashKey]['date'], 'amount' => $abcCashActual[$abcCashKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $abcCash[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        }
                    } else {
                        $n = 1;
                        $abcCash[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    }
                }
            }

            foreach ($dates as $k => $v) {
                if (is_numeric($k)) {

                    $month = date('n', strtotime($v['firstdate']));
                    $year = date('y', strtotime($v['firstdate']));

                    if ($month != "" && $year != "") {
                        $id = $month . '-' . $year;
                    } else {
                        $id = '';
                    }

                    if ($k != 0) {
                        $finalTradeReceivable[$k] = array('amount' => (round($tradeReceivable[$k - 1]['amount'] - $tradeReceivable[$k]['amount'], 2)), 'month' => $tradeReceivable[$k]['month'], 'year' => $tradeReceivable[$k]['year'], 'id' => $tradeReceivable[$k]['month'] . '-' . $tradeReceivable[$k]['year']);
                    } else {
                        $finalTradeReceivable[$k] = array('amount' => (round($tradeReceivable[$k]['amount'], 2)), 'month' => $tradeReceivable[$k]['month'], 'year' => $tradeReceivable[$k]['year'], 'id' => $tradeReceivable[$k]['month'] . '-' . $tradeReceivable[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalOtherAssets[$k] = array('amount' => (round($totalOtherAssets[$k - 1]['amount'] - $totalOtherAssets[$k]['amount'], 2)), 'month' => $totalOtherAssets[$k]['month'], 'year' => $totalOtherAssets[$k]['year'], 'id' => $totalOtherAssets[$k]['month'] . '-' . $totalOtherAssets[$k]['year']);
                    } else {
                        $finalOtherAssets[$k] = array('amount' => (round($totalOtherAssets[$k]['amount'], 2)), 'month' => $totalOtherAssets[$k]['month'], 'year' => $totalOtherAssets[$k]['year'], 'id' => $totalOtherAssets[$k]['month'] . '-' . $totalOtherAssets[$k]['year']);
                    }

                    $finalCurrentLiabilities[$k] = array('amount' => (round(($accountsPayable[$k]['amount'] + $otherCurrentLiabilities[$k]['amount']) - ($accountsPayable[$k - 1]['amount'] + $otherCurrentLiabilities[$k - 1]['amount']), 2)), 'month' => $accountsPayable[$k]['month'], 'year' => $accountsPayable[$k]['year'], 'id' => $accountsPayable[$k]['month'] . '-' . $accountsPayable[$k]['year']);

                    if ($k != 0) {
                        $finalInterCompanyReceivables[$k] = array('amount' => (round($interCompanyReceivables[$k - 1]['amount'] - $interCompanyReceivables[$k]['amount'], 2)), 'month' => $interCompanyReceivables[$k]['month'], 'year' => $interCompanyReceivables[$k]['year'], 'id' => $interCompanyReceivables[$k]['month'] . '-' . $interCompanyReceivables[$k]['year']);
                    } else {
                        $finalInterCompanyReceivables[$k] = array('amount' => (round($interCompanyReceivables[$k]['amount'], 2)), 'month' => $interCompanyReceivables[$k]['month'], 'year' => $interCompanyReceivables[$k]['year'], 'id' => $interCompanyReceivables[$k]['month'] . '-' . $interCompanyReceivables[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalInterCompanyPayables[$k] = array('amount' => (round($interCompanyPayables[$k]['amount'] - $interCompanyPayables[$k - 1]['amount'], 2)), 'month' => $interCompanyPayables[$k]['month'], 'year' => $interCompanyPayables[$k]['year'], 'id' => $interCompanyPayables[$k]['month'] . '-' . $interCompanyPayables[$k]['year']);
                    } else {
                        $finalInterCompanyPayables[$k] = array('amount' => (round($interCompanyPayables[$k]['amount'], 2)), 'month' => $interCompanyPayables[$k]['month'], 'year' => $interCompanyPayables[$k]['year'], 'id' => $interCompanyPayables[$k]['month'] . '-' . $interCompanyPayables[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalplantDep[$k] = array('amount' => (round($plantDep[$k - 1]['amount'] - $plantDep[$k]['amount'], 2)), 'month' => $plantDep[$k]['month'], 'year' => $plantDep[$k]['year'], 'id' => $plantDep[$k]['month'] . '-' . $plantDep[$k]['year']);
                    } else {
                        $finalplantDep[$k] = array('amount' => (round($plantDep[$k]['amount'], 2)), 'month' => $plantDep[$k]['month'], 'year' => $plantDep[$k]['year'], 'id' => $plantDep[$k]['month'] . '-' . $plantDep[$k]['year']);
                    }

                    $otherMovementsinWorkCap[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    //$otherMovementsinWorkCap[$k] = array('amount' => (round($saleoffixedassets[$k]['amount'] + $finalInterCompanyReceivables[$k]['amount'] + ($finalplantDep[$k]['amount'] - $depreciation[$k]['amount']) + $finalInterCompanyPayables[$k]['amount'], 2)), 'month' => $depreciation[$k]['month'], 'year' => $depreciation[$k]['year'],'id' => $depreciation[$k]['month'] .'-'. $depreciation[$k]['year']);

                    $netCashFlow[$k] = array('amount' => (round($netIncome[$k]['amount'] + $depreciation[$k]['amount'] + $finalTradeReceivable[$k]['amount'] + $finalOtherAssets[$k]['amount'] + $finalCurrentLiabilities[$k]['amount'] + $otherMovementsinWorkCap[$k]['amount'], 2)), 'month' => $netIncome[$k]['month'], 'year' => $netIncome[$k]['year'], 'id' => $netIncome[$k]['month'] . '-' . $netIncome[$k]['year']);

                    if ($k != 0) {
                        $finalplant[$k] = array('amount' => (round($plant[$k - 1]['amount'] - $plant[$k]['amount'], 2)), 'month' => $plant[$k]['month'], 'year' => $plant[$k]['year'], 'id' => $plant[$k]['month'] . '-' . $plant[$k]['year']);
                    } else {
                        $finalplant[$k] = array('amount' => (round($plant[$k]['amount'], 2)), 'month' => $plant[$k]['month'], 'year' => $plant[$k]['year'], 'id' => $plant[$k]['month'] . '-' . $plant[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalofficeEquipment[$k] = array('amount' => (round($officeEquipment[$k - 1]['amount'] - $officeEquipment[$k]['amount'], 2)), 'month' => $officeEquipment[$k]['month'], 'year' => $officeEquipment[$k]['year'], 'id' => $officeEquipment[$k]['month'] . '-' . $officeEquipment[$k]['year']);
                    } else {
                        $finalofficeEquipment[$k] = array('amount' => (round($officeEquipment[$k]['amount'], 2)), 'month' => $officeEquipment[$k]['month'], 'year' => $officeEquipment[$k]['year'], 'id' => $officeEquipment[$k]['month'] . '-' . $officeEquipment[$k]['year']);
                    }

                    $finalPPPE[$k] = array('amount' => ($finalplant[$k]['amount'] + $finalofficeEquipment[$k]['amount']), 'month' => $finalplant[$k]['month'], 'year' => $finalplant[$k]['year'], 'id' => $finalplant[$k]['month'] . '-' . $finalplant[$k]['year']);

                    if ($k != 0) {
                        $finalIntangibleAssets[$k] = array('amount' => (round($intangibleAssets[$k - 1]['amount'] - $intangibleAssets[$k]['amount'], 2)), 'month' => $intangibleAssets[$k]['month'], 'year' => $intangibleAssets[$k]['year'], 'id' => $intangibleAssets[$k]['month'] . '-' . $intangibleAssets[$k]['year']);
                    } else {
                        $finalIntangibleAssets[$k] = array('amount' => (round($intangibleAssets[$k]['amount'], 2)), 'month' => $intangibleAssets[$k]['month'], 'year' => $intangibleAssets[$k]['year'], 'id' => $intangibleAssets[$k]['month'] . '-' . $intangibleAssets[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalInvestmentInSubsidary[$k] = array('amount' => (round($investmentInSubsidary[$k - 1]['amount'] - $investmentInSubsidary[$k]['amount'], 2)), 'month' => $investmentInSubsidary[$k]['month'], 'year' => $investmentInSubsidary[$k]['year'], 'id' => $investmentInSubsidary[$k]['month'] . '-' . $investmentInSubsidary[$k]['year']);
                    } else {
                        $finalInvestmentInSubsidary[$k] = array('amount' => (round($investmentInSubsidary[$k]['amount'], 2)), 'month' => $investmentInSubsidary[$k]['month'], 'year' => $investmentInSubsidary[$k]['year'], 'id' => $investmentInSubsidary[$k]['month'] . '-' . $investmentInSubsidary[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalOtherNonCurrentLiabilities[$k] = array('amount' => (round($otherNonCurrentLiabilities[$k]['amount'] - $otherNonCurrentLiabilities[$k - 1]['amount'], 2)), 'month' => $otherNonCurrentLiabilities[$k]['month'], 'year' => $otherNonCurrentLiabilities[$k]['year'], 'id' => $otherNonCurrentLiabilities[$k]['month'] . '-' . $otherNonCurrentLiabilities[$k]['year']);
                    } else {
                        $finalOtherNonCurrentLiabilities[$k] = array('amount' => (round($otherNonCurrentLiabilities[$k]['amount'], 2)), 'month' => $otherNonCurrentLiabilities[$k]['month'], 'year' => $otherNonCurrentLiabilities[$k]['year'], 'id' => $otherNonCurrentLiabilities[$k]['month'] . '-' . $otherNonCurrentLiabilities[$k]['year']);
                    }

                    $finalCurrentAssets[$k] = array('amount' => (round($finalPPPE[$k]['amount'] + $finalIntangibleAssets[$k]['amount'] + $finalInvestmentInSubsidary[$k]['amount'] + $finalOtherNonCurrentLiabilities[$k]['amount'], 2)), 'month' => $finalplant[$k]['month'], 'year' => $finalplant[$k]['year'], 'id' => $finalplant[$k]['month'] . '-' . $finalplant[$k]['year']);

                    if ($k != 0) {
                        $finalAdvances[$k] = array('amount' => ($advancesFromParentCompany[$k]['amount'] - $advancesFromParentCompany[$k - 1]['amount']), 'month' => $advancesFromParentCompany[$k]['month'], 'year' => $advancesFromParentCompany[$k]['year'], 'id' => $advancesFromParentCompany[$k]['month'] . '-' . $advancesFromParentCompany[$k]['year']);
                    } else {
                        $finalAdvances[$k] = array('amount' => ($advancesFromParentCompany[$k]['amount']), 'month' => $advancesFromParentCompany[$k]['month'], 'year' => $advancesFromParentCompany[$k]['year'], 'id' => $advancesFromParentCompany[$k]['month'] . '-' . $advancesFromParentCompany[$k]['year']);
                    }

                    if ($k != 0) {
                        $finalShareCapital[$k] = array('amount' => ($shareCapital[$k]['amount'] - $shareCapital[$k - 1]['amount']), 'month' => $shareCapital[$k]['month'], 'year' => $shareCapital[$k]['year'], 'id' => $shareCapital[$k]['month'] . '-' . $shareCapital[$k]['year']);
                    } else {
                        $finalShareCapital[$k] = array('amount' => ($shareCapital[$k]['amount']), 'month' => $shareCapital[$k]['month'], 'year' => $shareCapital[$k]['year'], 'id' => $shareCapital[$k]['month'] . '-' . $shareCapital[$k]['year']);
                    }

                    $totalCashFromFinanceActivities[$k] = array('amount' => ($finalAdvances[$k]['amount'] + $finalShareCapital[$k]['amount']), 'month' => $finalAdvances[$k]['month'], 'year' => $finalAdvances[$k]['year'], 'id' => $finalAdvances[$k]['month'] . '-' . $finalAdvances[$k]['year']);

                    $netIncreaseInCash[$k] = array('amount' => ($netCashFlow[$k]['amount'] + $finalCurrentAssets[$k]['amount'] + $totalCashFromFinanceActivities[$k]['amount']), 'month' => $month, 'year' => $year, 'id' => $id);

                    if (!isset($abcCash[$k - 1])) {
                        $cashAndCashEq[$k] = array('amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                    } else {
                        $cashAndCashEq[$k] = array('amount' => $abcCash[$k - 1]['amount'], 'month' => $abcCash[$k]['month'], 'year' => $abcCash[$k]['year'], 'id' => $abcCash[$k]['month'] . '-' . $abcCash[$k]['year']);
                    }

                    $netCashEquivalent[$k] = array('amount' => ($cashAndCashEq[$k]['amount'] + $netIncreaseInCash[$k]['amount']), 'month' => $month, 'year' => $year, 'id' => $id);
                }
            }

            $response["Cash Flows from Operating Activities"][] = array("title" => "Net Profit", "value" => $netIncome);
            $response["Cash Flows from Operating Activities"][] = array("title" => "Depreciation & Amortisation", "value" => $depreciation);
            $response["Cash Flows from Operating Activities"][] = array("title" => "Accounts Receivable", "value" => $finalTradeReceivable);
            $response["Cash Flows from Operating Activities"][] = array("title" => "Other Assets", "value" => $finalOtherAssets);
            $response["Cash Flows from Operating Activities"][] = array("title" => "Accounts Payables & Other Current Liabilities", "value" => $finalCurrentLiabilities);
            $response["Cash Flows from Operating Activities"][] = array("title" => "Other Movements in Working Capital", "value" => $otherMovementsinWorkCap);
            $response["Cash Flows from Operating Activities"]["summaryRow"] = array("title" => "Net Cash Flow from operating activities", "value" => $netCashFlow);
            $response["Cash Flows from Investing Activities"][] = array("title" => "Purchase of Property, Plant & Equipment", "value" => $finalPPPE);
            $response["Cash Flows from Investing Activities"][] = array("title" => "Investment in intangible Assets", "value" => $finalIntangibleAssets);
            $response["Cash Flows from Investing Activities"][] = array("title" => "Investment in Subsidiaries", "value" => $finalInvestmentInSubsidary);
            $response["Cash Flows from Investing Activities"][] = array("title" => "Other Non-Current Liabilities", "value" => $finalOtherNonCurrentLiabilities);
            $response["Cash Flows from Investing Activities"]["summaryRow"] = array("title" => "Total Cash Flows from investing activities", "value" => $finalCurrentAssets);
            $response["Cash Flows from Financial Activities"][] = array("title" => "Owner Funds Introduced", "value" => $finalAdvances);
            $response["Cash Flows from Financial Activities"][] = array("title" => "Share Capital", "value" => $finalShareCapital);
            $response["Cash Flows from Financial Activities"]["summaryRow"] = array("title" => "Total Cash Flows from Financing activities", "value" => $totalCashFromFinanceActivities);
            $response["Cash and Cash Equivalents"][] = array("title" => "Net Increase (decrease) in cash & cash equivalents", "value" => $netIncreaseInCash);
            $response["Cash and Cash Equivalents"][] = array("title" => "Cash & cash equivalents at beginning of period", "value" => $cashAndCashEq);
            $response["Cash and Cash Equivalents"]["summaryRow"] = array("title" => "Net cash & cash equivalents at end of period", "value" => $netCashEquivalent, "no_expand" => true);

            $netIncomeActual = null;
            $netIncome = null;
            $depreciationActual = null;
            $depreciation = null;
            $saleoffixedassetsActual = null;
            $saleoffixedassets = null;
            $tradeReceivableActual = null;
            $tradeReceivable = null;
            $totalOtherAssetsActual = null;
            $totalOtherAssets = null;
            $accountsPayableActual = null;
            $accountsPayable = null;
            $otherCurrentLiabilitiesActual = null;
            $otherCurrentLiabilities = null;
            $plantDepActual = null;
            $plantDep = null;
            $interCompanyReceivablesActual = null;
            $interCompanyReceivables = null;
            $interCompanyPayablesActual = null;
            $interCompanyPayables = null;
            $otherMovementsinWorkCap = null;

            $finalOtherAssets = null;
            $finalCurrentLiabilities = null;
            $finalInterCompanyReceivables = null;
            $finalInterCompanyPayables = null;
            $netCashFlow = null;

            $plantActual = null;
            $plant = null;
            $officeEquipmentActual = null;
            $officeEquipment = null;
            $intangibleAssetsActual = null;
            $intangibleAssets = null;
            $investmentInSubsidaryActual = null;
            $investmentInSubsidary = null;
            $otherNonCurrentLiabilitiesActual = null;
            $otherNonCurrentLiabilities = null;

            $finalplant = null;
            $finalofficeEquipment = null;
            $finalPPPE = null;
            $finalIntangibleAssets = null;
            $finalInvestmentInSubsidary = null;
            $finalOtherNonCurrentLiabilities = null;

            $advancesFromParentCompanyActual = null;
            $advancesFromParentCompany = null;
            $shareCapitalActual = null;
            $shareCapital = null;

            $abcCashActual = null;
            $abcCash = null;
            $fixedAssets = null;

            $finalCurrentAssets = null;
            $finalAdvances = null;
            $finalShareCapital = null;
            $totalCashFromFinanceActivities = null;
            $netIncreaseInCash = null;
            $cashAndCashEq = null;
            $netCashEquivalent = null;

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getInvoiceListDatareport($client_id, $XeroOAuth, $ajax, $organisations, $dates, $client_master_id)
    {
        try {
            $invoiceBynameListing = array();
            $balanceSheet = array();
            $aBalanceSheet = array();
            $bsSummary = array();
            $finalArr = array();
            $finalActualArr = array();
            // $finalGroupTotalArr = array();
            $GetAllValue = array();
            $rowid = 0;
            foreach ($dates as $k => $v) {
                if (is_numeric($k) && $k > 0) {
                    $frmDate = date("Y, m, d", strtotime($v['firstdate']));
                    $toDate = date("Y, m, d", strtotime($v['lastdate']));
                    // $finalGroupTotalArr[$rowid] = array('amount' => 0, 'month' => date("n", strtotime($v['firstdate'])), 'year' => date("y", strtotime($v['firstdate'])));
                    $response = $this->makeRequest($client_id, 'getInvoiceListDatareport', $XeroOAuth, 'GET', INVOICE_CONTACT_BY_NAME, array('Where' => 'Type=="ACCREC" AND Date >= DateTime(' . $frmDate . ') AND Date <= DateTime(' . $toDate . ')'), "", "json",$client_master_id);
                     $invoiceContactList = $XeroOAuth->response;
                    if ($invoiceContactList['code'] == 302) {
                        $invoiceContactList["error"] = true;
                        $invoiceContactList["message"] = XERO_HAVING_PROBLEM;
                        return $invoiceContactList;
                    } else if ($invoiceContactList['code'] == 401) {
                        if (strpos($invoiceContactList['response'], 'token_expired') !== false || strpos($invoiceContactList['response'], 'token_rejected') !== false) {
                            if (isset($ajax) && ($ajax == true)) {
                                $invoiceBynameListing["expiry"] = true;
                                $invoiceBynameListing['error'] = true;
                                $invoiceBynameListing['message'] = TOKEN_EXPIRED;
                                return $invoiceBynameListing;
                            } else {
                                $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                                if (isset($resp['authurl'])) {
                                    $authurl = $resp['authurl'];
                                    header('Location:' . $authurl);die();
                                }
                            }
                        }
                    } else if ($invoiceContactList['code'] == 200) {
                        $balanceSheet = array();
                        $invoiceContactName = json_decode($XeroOAuth->response['response'], true);
                        $GetAllValue[] = $invoiceContactName['Invoices'];
                    }
                    $rowid = $rowid + 1;
                }
            }
            $totArr = array();
            foreach ($GetAllValue as $key => $val) {
                foreach ($val as $innKey => $innval) {
                    preg_match("#/Date\((\d{10})\d{3}(.*?)\)/#", $innval['Date'], $match);
                    $month = date("n", $match[1]);
                    $total = $innval['Total'];
                    $monthYr = date("j M Y", $match[1]);
                    $year = date("y", $match[1]);
                    $totArr[$innval['Contact']['Name']][] = array('month' => $month, 'amount' => $total, 'year' => $year);
                }
            }
            $consArr = array();
            $keys = 0;
            foreach ($totArr as $a => $totArrPush) {
                $consArr[$keys]['company_name'] = $a;
                if (count($totArrPush)) {
                    $amountTotal = array();
                    foreach ($totArrPush as $pKey => $amountVal) {
                        $amountTotal[$amountVal['month'] . '_' . $amountVal['year']] += $amountVal['amount'];
                    }
                }
                if (count($amountTotal) > 0) {
                    $finalGroupArr = array();
                    $i = 0;
                    $client_total_amount = 0;
                    foreach ($amountTotal as $mKey => $amtVal) {
                        $explodeMonthYear = explode('_', $mKey);
                        if (isset($explodeMonthYear[0]) && isset($explodeMonthYear[1])) {
                            $year = $explodeMonthYear[1];
                            $month = $explodeMonthYear[0];
                            $finalGroupArr[$i] = array('amount' => $amtVal, 'month' => $month, 'year' => $year);
                            $client_total_amount = $client_total_amount + $amtVal;
                            // $array_id = $this->searchIdFromArray($month . '-' . $year, $finalGroupTotalArr);
                            // if ($array_id != null) {
                            //     $finalGroupTotalArr[$array_id]["amount"] = $finalGroupTotalArr[$array_id]["amount"] + $amtVal;
                            // }

                            $i++;
                        }
                    }
                    $finalGroupArr['total'] = array('amount' => $client_total_amount);
                    // $finalGroupTotalArr['total'] = array('amount' => 0);
                }
                $consArr[$keys]['value'] = $finalGroupArr;
                $keys++;
            }

            // if (sizeof($finalGroupTotalArr) > 0) {
            //     $amountTotal = 0;
            //     foreach ($finalGroupTotalArr as $key => $val) {
            //         if ($key != 'total') {
            //             $amountTotal = $amountTotal + $val['amount'];
            //         }
            //     }
            //     $finalGroupTotalArr['total']['amount'] = $amountTotal;
            // }

            if (sizeof($consArr) > 0) {
                // $consArr[$keys]['company_name'] = "Total";
                // $consArr[$keys]['value'] = $finalGroupTotalArr;
                $invoiceBynameListing["error"] = false;
                $invoiceBynameListing["message"] = RECORD_FOUND;
                $invoiceBynameListing["invoiceByName"] = $consArr;
            } else {
                $invoiceBynameListing["error"] = true;
                $invoiceBynameListing["message"] = NO_RECORD_FOUND;
            }
            return $invoiceBynameListing;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function GenerateMetrics($client_master_id, $client_id, $report_start_date, $sub_category_id1, $sub_category_id2)
    {
        try {
            $metricResponse = array();
            $db1 = new utility();

            $start_month = date('m', $report_start_date);
            $now = new DateTime('now');
            $end_month = $now->format('m');
            $last_month = $end_month - 1;
            $mon_diff = $last_month - $start_month;
            if ($mon_diff == 1) {
                $days = 30;
            } else if ($mon_diff == 2) {
                $days = 60;
            } else {
                $days = 90;
            }

            $metric_path = __DIR__ . "/metrics/metric_" . $client_master_id . "_" . $report_start_date . ".json";

            $flash_report_file = $db1->MetricscheckCacheFileExist($client_master_id, 'flashreport', $sub_category_id1, $sub_category_id2);
            $flash_json_file = file_get_contents($flash_report_file);
            $flash_obj = json_decode($flash_json_file, true);

            $metricResponse["BankTransactions"] = null;
            $metricResponse["accountsSummarytotal"] = 0;
            $metricResponse["agedPayablestotal"] = 0;
            $metricResponse["agedReceivablestotal"] = 0;
            $metricResponse["estimatedPayrolltotal"] = 0;

            if (file_exists($flash_report_file)) {
                $metricResponse["BankTransactions"] = $flash_obj[7]["BankTransactions"];

                foreach ($flash_obj[1]['accountsSummary'] as $record) {
                    if ($record['bank_account_name'] == 'Total') {
                        $metricResponse["accountsSummarytotal"] = $record['closing_balance'];
                    }
                }

                foreach ($flash_obj[2]['agedPayables'] as $record) {
                    $metricResponse["agedPayablestotal"] = $record['total_payables']['Total'];
                }

                foreach ($flash_obj[3]['agedReceivables'] as $record) {
                    $metricResponse["agedReceivablestotal"] = $record['total_receivables']['Total'];
                }

                foreach ($flash_obj[4]['estimatedPayroll'][0] as $record) {
                    if ($record[0]['Value'] == 'Total Employee Compensation & Related Expenses') {
                        foreach ($record as $subrecord) {
                            $metricResponse["estimatedPayrolltotal"] = $subrecord['Value'];
                        }
                    }
                }
            }

            $pnl_report_file = $db1->MetricscheckCacheFileExist($client_master_id, 'plreport', $sub_category_id1, $sub_category_id2);
            $pnl_json_file = file_get_contents($pnl_report_file);
            $pnl_obj = json_decode($pnl_json_file, true);

            if (file_exists($pnl_report_file)) {

                //Account Receiveable
                $metricResponse["Account Receivables"] = $pnl_obj[1]['balanceSheet']['Current Assets']['Accounts Receivable'];

                //Account Payable
                $metricResponse["Account Payables"] = $pnl_obj[1]['balanceSheet']['Current Liabilities']['Accounts Payable'];

                //Total Income
                $metricResponse["Total Income"] = $pnl_obj[3]['trendPNL']['Income']['summaryRow']['Total Income'];

                $income_total_ebit = 0;
                foreach ($metricResponse["Total Income"] as $key => $val) {
                    $income_total_ebit = $income_total_ebit + $val["amount"];
                }

                //Operating Profit
                $metricResponse["Operating Profit"] = $pnl_obj[3]['trendPNL']['Operating Profit'];
                $operating_profit_total_ebit = 0;
                foreach ($metricResponse["Operating Profit"] as $key => $val) {
                    $operating_profit_total_ebit = $operating_profit_total_ebit + $val["amount"];
                }

                // EBIT Calculation
                $ebit = number_format($operating_profit_total_ebit / $income_total_ebit, 2);
                $metricResponse["EBIT"] = is_numeric($ebit) ? $ebit : 0;

                //Total Cost of Sales
                $metricResponse["Total Cost of Sales"] = $pnl_obj[3]['trendPNL']['Less Cost of Sales']['summaryRow']['Total Cost of Sales'];

                //Total Operating Expenses
                $metricResponse["Total Operating Expenses"] = $pnl_obj[3]['trendPNL']['Less Operating Expenses']['summaryRow']['Total Operating Expenses'];

                //Total Employee Compensation & Related Expenses
                $metricResponse["Total Employee Compensation"] = $pnl_obj[3]['trendPNL']['Less Operating Expenses']['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'];

                //Net Profit
                $metricResponse["Net Profit"] = $pnl_obj[3]['trendPNL']['Net Profit'];

                //Top 10 Operating Expenses
                $metricResponse["Operating Expenses"][] = array_slice($pnl_obj[3]['trendPNL']['Operating Expenses'], 0, TOP_N_RECORDS);

                //Total Other Income (Expenses)
                // $metricResponse["Total Other Income (Expenses)"] = $pnl_obj[3]['trendPNL']['Other Income (Expenses)']['summaryRow']['Total Other Income (Expenses)'];

                $data = array();
                $data = $this->getEarlierMonthNumber($report_start_date);
                $dataLength = sizeof($data);

                // Account Receivable DSO calculation
                if (sizeof($metricResponse["Account Receivables"]) > 0) {
                    $account_receivable = array();
                    $total_income = array();
                    foreach ($metricResponse["Account Receivables"] as $key => $val) {
                        if ($key != 0) {
                            $array_id = $val['month'];
                            $account_receivable[$array_id] = $val['amount'];
                        }
                    }

                    foreach ($metricResponse["Total Income"] as $key => $val) {
                        if ($key != 0) {
                            $array_id = $val['month'];
                            $total_income[$array_id] = $val['amount'];
                        }
                    }

                    $account_receivable_total = 0;
                    $income_total = 0;

                    if ($dataLength == 1) {
                        $key = (int) $data[0];
                        $account_receivable_total = $account_receivable[$key];
                        $a = $account_receivable_total;
                        $income_total = $total_income[$key];
                        $b = $income_total / 30;
                        $c = $a / $b;
                    } elseif ($dataLength == 2) {
                        $key = (int) $data[0];
                        $key1 = (int) $data[1];
                        $account_receivable_total = $account_receivable[$key] + $account_receivable[$key1];
                        $a = $account_receivable_total / 2;
                        $income_total = $total_income[$key] + $total_income[$key1];
                        $b = $income_total / 60;
                        $c = $a / $b;
                    } elseif ($dataLength == 3) {
                        $key = (int) $data[0];
                        $key1 = (int) $data[1];
                        $key2 = (int) $data[2];
                        $account_receivable_total = $account_receivable[$key] + $account_receivable[$key1];
                        $a = $account_receivable_total / 2;
                        $income_total = $total_income[$key] + $total_income[$key1] + $total_income[$key2];
                        $b = $income_total / 90;
                        $c = $a / $b;
                    } elseif ($dataLength >= 4) {
                        $key = (int) $data[0];
                        $key1 = (int) $data[1];
                        $key2 = (int) $data[2];
                        $key3 = (int) $data[3];
                        $account_receivable_total = $account_receivable[$key] + $account_receivable[$key3];
                        $a = $account_receivable_total / 2;
                        $income_total = $total_income[$key] + $total_income[$key1] + $total_income[$key2];
                        $b = $income_total / 90;
                        $c = $a / $b;
                    }

                    $metricResponse["Accounts Receivable DSO"] = round(is_infinite($c) ? 0 : (is_nan($c) ? 0 : $c));
                } else {
                    $metricResponse["Accounts Receivable DSO"] = 0;
                }

                // Days Payable Outstanding (DPO) calculation
                if (sizeof($metricResponse["Account Payables"]) > 0) {
                    $account_payable_total = 0;
                    $cost_of_sales_total = 0;
                    $operating_expenses_total = 0;
                    $operating_profit_total = 0;
                    $employee_compensation_total = 0;

                    $account_payables = array();
                    $total_cost_of_sales = array();
                    $total_operating_expenses = array();
                    $total_employee_compensation = array();
                    foreach ($metricResponse["Account Payables"] as $key => $val) {
                        if ($key != 0) {
                            $array_id = $val['month'];
                            $account_payables[$array_id] = $val['amount'];
                        }
                    }

                    foreach ($metricResponse["Total Cost of Sales"] as $key => $val) {
                        if ($key != 0) {
                            $array_id = $val['month'];
                            $total_cost_of_sales[$array_id] = $val['amount'];
                        }
                    }

                    foreach ($metricResponse["Total Operating Expenses"] as $key => $val) {
                        if ($key != 0) {
                            $array_id = $val['month'];
                            $total_operating_expenses[$array_id] = $val['amount'];
                        }
                    }

                    foreach ($metricResponse["Total Employee Compensation"] as $key => $val) {
                        if ($key != 0) {
                            $array_id = $val['month'];
                            $total_employee_compensation[$array_id] = $val['amount'];
                        }
                    }

                    if ($dataLength == 1) {
                        $key = (int) $data[0];
                        $account_payable_total = $account_payables[$key];
                        $d = $account_payable_total;
                        $cost_of_sales_total = $total_cost_of_sales[$key];
                        $operating_expenses_total = $total_operating_expenses[$key];
                        $employee_compensation_total = $total_employee_compensation[$key];
                        $e = $cost_of_sales_total + $operating_expenses_total - $employee_compensation_total;
                        $f = $e / 30;
                        $g = $d / $f;
                    } elseif ($dataLength == 2) {
                        $key = (int) $data[0];
                        $key1 = (int) $data[1];
                        $account_payable_total = $account_payables[$key] + $account_payables[$key1];
                        $d = $account_payable_total / 2;
                        $cost_of_sales_total = $total_cost_of_sales[$key] + $total_cost_of_sales[$key1];
                        $operating_expenses_total = $total_operating_expenses[$key] + $total_operating_expenses[$key1];
                        $employee_compensation_total = $total_employee_compensation[$key] + $total_employee_compensation[$key1];
                        $e = $cost_of_sales_total + $operating_expenses_total - $employee_compensation_total;
                        $f = $e / 60;
                        $g = $d / $f;
                    } elseif ($dataLength == 3) {
                        $key = (int) $data[0];
                        $key1 = (int) $data[1];
                        $key2 = (int) $data[2];
                        $account_payable_total = $account_payables[$key] + $account_payables[$key1];
                        $d = $account_payable_total / 2;
                        $cost_of_sales_total = $total_cost_of_sales[$key] + $total_cost_of_sales[$key1] + $total_cost_of_sales[$key2];
                        $operating_expenses_total = $total_operating_expenses[$key] + $total_operating_expenses[$key1] + $total_operating_expenses[$key2];
                        $employee_compensation_total = $total_employee_compensation[$key] + $total_employee_compensation[$key1] + $total_employee_compensation[$key2];
                        $e = $cost_of_sales_total + $operating_expenses_total - $employee_compensation_total;
                        $f = $e / 90;
                        $g = $d / $f;
                    } elseif ($dataLength >= 4) {
                        $key = (int) $data[0];
                        $key1 = (int) $data[1];
                        $key2 = (int) $data[2];
                        $key3 = (int) $data[3];
                        $account_payable_total = $account_payables[$key] + $account_payables[$key3];
                        $d = $account_payable_total / 2;
                        $cost_of_sales_total = $total_cost_of_sales[$key] + $total_cost_of_sales[$key1] + $total_cost_of_sales[$key2];
                        $operating_expenses_total = $total_operating_expenses[$key] + $total_operating_expenses[$key1] + $total_operating_expenses[$key2];
                        $employee_compensation_total = $total_employee_compensation[$key] + $total_employee_compensation[$key1] + $total_employee_compensation[$key2];
                        $e = $cost_of_sales_total + $operating_expenses_total - $employee_compensation_total;
                        $f = $e / 90;
                        $g = $d / $f;
                    }

                    $metricResponse["Days Payable Outstanding (DPO)"] = round(is_infinite($g) ? 0 : (is_nan($g) ? 0 : $g));
                } else {
                    $metricResponse["Days Payable Outstanding (DPO)"] = 0;
                }

                // Cash conversion cycle calculation
                $metricResponse["Cash Conversion Cycle"] = $metricResponse["Days Payable Outstanding (DPO)"] - $metricResponse["Accounts Receivable DSO"];

                $metricResponse["employees"] = $jfo["employees"];

                /* Current Ratio Calculation (Current Ratio = Current Assets / Current Liabilities) */
                // $metricResponse["Total Current Assets Last"] = end($pnl_obj[1]['balanceSheet']['Current Assets']['summaryRow']['Total Current Assets']);
                // $metricResponse["Total Bank Last"] = end($pnl_obj[1]['balanceSheet']['Bank']['summaryRow']['Total Bank']);
                // $metricResponse["Total Current Liabilities Last"] = end($pnl_obj[1]['balanceSheet']['Current Liabilities']['summaryRow']['Total Current Liabilities']);

                $metricResponse["Total Current Assets Last"] = end($this->array_find_deep_by_key($pnl_obj[1]['balanceSheet'], 'Total Current Assets'));
                $metricResponse["Total Bank Last"] = end($this->array_find_deep_by_key($pnl_obj[1]['balanceSheet'], 'Total Bank'));
                $metricResponse["Total Current Liabilities Last"] = end($this->array_find_deep_by_key($pnl_obj[1]['balanceSheet'], 'Total Current Liabilities'));
                $metricResponse["last_updated_time"] = date('Y-m-d H:i:s');
            } else {
                $metricResponse["error"] = true;
                $metricResponse["message"] = NO_RECORD_FOUND;
            }

            file_put_contents($metric_path, json_encode($metricResponse));
            chmod($metric_path, 0777);
            $this->updateCacheLog($client_id, true);
            return $metricResponse;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function generateSMA($month_array, $value_array)
    {
        try {
            $response = array();
            foreach ($month_array as $key => $value) {
                $amount = 0;
                $count = 0;
                $finalamount = 0;
                for ($i = 0; $i <= $key; $i++) {
                    $amount = $amount + $value_array[$i];
                    $count++;
                }
                $finalamount = $amount / $count;
                array_push($response, round($finalamount));
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getPdfReportClients()
    {
        try {
            $current_day = date("w");
            $current_datetime = date("Y-m-d H:i:s");
            $sql = "SELECT cm.name,cm.email,cd.client_master_id, cm.client_id,rc.pdf_run_time, rc.every_week_on,cd.report_start_date,cd.cushion_value, cd.company_name
            FROM client_master_detail cd
            INNER JOIN client_user_mapping cum ON cum.client_master_id = cd.client_master_id
            INNER JOIN client_master cm ON cm.client_id=cum.client_id
            INNER JOIN report_pdf_to_client rc ON cd.client_master_id=rc.client_master_id
            WHERE cd.pdf_report_status = 1 and cm.is_active  = 1 and rc.is_deleted = 0 and cm.is_admin =1
                and rc.every_week_on = $current_day and  rc.last_ran_status = 0
                and CASE WHEN rc.last_ran_time is null THEN '" . $current_datetime . "' else rc.last_ran_time end <= '" . $current_datetime . "'
                and cd.client_master_id   NOT IN (SELECT cl.client_master_id
                                        FROM client_master_detail as cmd
                                        INNER JOIN master_groups_company_list as  cl ON cl.group_id = cmd.client_master_id
                                        WHERE cmd.is_group = 1
                                        GROUP BY cl.group_id )
                                        UNION
                                        SELECT distinct cm.name,cm.email,cd.client_master_id, cm.client_id,rc.pdf_run_time, rc.every_week_on,cd.report_start_date,
                                            cd.cushion_value, cd.company_name
                                            FROM client_master_detail cd
                                            INNER JOIN client_group_mapping cgm ON cgm.group_id = cd.client_master_id and cgm.is_default = 1
                                            INNER JOIN client_master cm ON cm.client_id=cgm.client_id
                                            INNER JOIN report_pdf_to_client rc ON rc.client_master_id = cd.client_master_id
                                            WHERE cd.is_group = 1 and cd.pdf_report_status = 1 and cm.is_active  = 1 and rc.is_deleted = 0 and cm.is_admin = 1
                                            and rc.every_week_on = $current_day and  rc.last_ran_status = 0
                                            and CASE WHEN rc.last_ran_time is null THEN '" . $current_datetime . "' else rc.last_ran_time end <= '" . $current_datetime . "'";

            $response["ClientList"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $update_array = array();
                    $stmt->bind_result($name, $email, $client_master_id, $client_id, $pdf_run_time, $every_week_on, $report_start_date, $cushion_value, $company_name);
                    while ($stmt->fetch()) {

                        $metric_path = __DIR__ . "/metrics/metric_" . $client_master_id . "_" . $report_start_date . ".json";
                        if (file_exists($metric_path)) {
                            $current_time = date("H:i");
                            if ($pdf_run_time <= $current_time) {
                                $tmp = array();
                                $tmp["name"] = $name;
                                $tmp["company_name"] = $company_name;
                                $tmp["email"] = $email;
                                $tmp["client_master_id"] = $client_master_id;
                                $tmp["client_id"] = $client_id;
                                $tmp["pdf_run_time"] = $pdf_run_time;
                                $tmp["every_week_on"] = $every_week_on;
                                $tmp['report_start_date'] = $report_start_date;
                                $tmp['metrics_path'] = $metric_path;
                                $tmp['cushion_value'] = ($cushion_value == null) ? 10000 : $cushion_value;
                                $response["ClientList"][] = $tmp;
                                $update_array[] = $client_master_id;
                            }
                        }
                    }
                    $update_array = array_unique($update_array);
                    foreach ($update_array as $value) {
                        $sql1 = "UPDATE report_pdf_to_client set last_ran_status = 1, last_ran_time = '" . $current_datetime . "' where client_master_id = $value";
                        if ($stmt1 = $this->conn->prepare($sql1)) {
                            $result = $stmt1->execute();
                            $stmt1->close();
                        }
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function UpdateRegistrationNumber($client_id, $registration_number)
    {
        try {
            $this->conn->autocommit(false);
            $client_master_sql = $this->getFieldByID("client_master", 'client_id', $client_id, 'client_master_id');
            $client_master_id = $client_master_sql["field_key"];

            $reg_number = "";
            $sql = "SELECT client_master_id FROM client_master_detail WHERE client_master_id = ? AND registration_number = ? ";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("is", $client_master_id, $registration_number);
                if ($stmt->execute()) {
                    $num_rows = $stmt->num_rows;
                    $stmt->close();
                    if ($num_rows == 0) {
                        $sql1 = "UPDATE client_master_detail set registration_number = ? where client_master_id = ?";
                        if ($stmt1 = $this->conn->prepare($sql1)) {
                            $stmt1->bind_param("si", $registration_number, $client_master_id);
                            $result = $stmt1->execute();
                            $stmt1->close();
                            if ($result) {
                                $this->conn->commit();
                            }
                        }
                    }
                }
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function SaveAuditActivities($client_id, $audit_type, $audit_date, $client_master_id)
    {
        try {
            $this->conn->autocommit(false);
            $date = date("Y-m-d H:i:s");
            $is_active = 1;
            $response = array();

            $audit_record = $this->getAuditActivityData($client_master_id, $audit_type);

            if ($audit_record["error"] == true) {
                $sql = "INSERT INTO client_audit_activities(client_master_id, audit_type, audit_alert_date) values(?, ?, ?)";
                $e = 1;
            } else {
                $sql = "UPDATE client_audit_activities set audit_alert_date = ? where client_master_id = ? AND  audit_type = ?";
                $e = 2;
            }
            if ($stmt = $this->conn->prepare($sql)) {
                if ($e == 1) {
                    $stmt->bind_param("iss", $client_master_id, $audit_type, $audit_date);
                } else {
                    $stmt->bind_param("sis", $audit_date, $client_master_id, $audit_type);
                }
                $result = $stmt->execute();
                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                }
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getTodayAuditAlerts()
    {
        try {
            $current_date = date("Y-m-d");
            $sql = "SELECT cm.email,cmd.company_name,mem.subject,mem.message,cmd.registration_number
					from client_master cm
                    INNER JOIN client_user_mapping cum ON cum.client_id = cm.client_id
					INNER JOIN client_master_detail cmd ON cmd.client_master_id = cum.client_master_id and cmd.audit_activity_report =1
					INNER JOIN client_audit_activities ca ON ca.client_master_id = cum.client_master_id
					INNER JOIN master_email_message mem ON mem.message_key = ca.audit_type
                    WHERE cm.is_active = 1 and DATE_FORMAT(ca.audit_alert_date, '%m-%d') = DATE_FORMAT('" . $current_date . "', '%m-%d') ORDER BY ca.client_master_id asc";
            $response["CompanyList"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($email, $company_name, $subject, $message, $registration_number);
                    while ($stmt->fetch()) {
                        $tmp = array();
                        $tmp["email"] = $email;
                        $tmp["company_name"] = $company_name;
                        $tmp["registration_number"] = $registration_number;
                        $tmp["subject"] = $subject;
                        $tmp["message"] = $message;
                        $response["CompanyList"][] = $tmp;
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getClientPdfDetails($client_id, $pdf_type, $sub_category_id1 = false, $sub_category_id2 = false, $client_master_id, $is_group = null)
    {
        try {
            $response = array();
            if ($is_group == 1) {
                $sql = "SELECT cd.company_name, cm.name, cm.email, cd.client_master_id, cm.client_id, cd.report_start_date, cd.cushion_value
                FROM client_master_detail cd
                INNER JOIN client_group_mapping as cgm ON cgm.group_id = cd.client_master_id
                INNER JOIN client_master cm ON cm.client_id = cgm.client_id
                WHERE cgm.group_id =  $client_master_id  and cm.is_active  = 1 and cgm.client_id =$client_id ";
            } else {
                $sql = "SELECT cd.company_name, cm.name, cm.email, cd.client_master_id, cm.client_id, cd.report_start_date, cd.cushion_value
                FROM client_master_detail cd
                INNER JOIN client_user_mapping cum ON cum.client_master_id = cd.client_master_id
                INNER JOIN client_master cm ON cm.client_id = cum.client_id
                WHERE cum.client_master_id =  $client_master_id  and cm.is_active  = 1 and cum.client_id =$client_id ";
            }

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($company_name, $name, $email, $client_master_id, $client_id, $report_start_date, $cushion_value);
                    while ($stmt->fetch()) {
                        $metric_path = __DIR__ . "/metrics/metric_" . $client_master_id . "_" . $report_start_date . ".json";
                        $suffix = "";
                        if ($pdf_type == "flash_report") {
                            $report_dir_path = __DIR__ . "/flashreport/" . $client_master_id . "/";
                            $prefix = $client_master_id . "_flashreport_";
                        } else if ($pdf_type == "pl_report") {
                            $report_dir_path = __DIR__ . "/plreport/" . $client_master_id . "/";
                            $prefix = $client_master_id . "_plreport_";
                        }
                        if (!empty($sub_category_id1) || !empty($sub_category_id2)) {
                            $suffix = "";
                            $suffix .= $sub_category_id1;
                            if (strlen(trim($sub_category_id2)) > 0) {
                                $suffix .= "_" . $sub_category_id2;
                            }
                            $suffix .= ".json";
                        }
                        $latest_filename = $this->latestFileAtPath($report_dir_path, $prefix, $suffix);
                        $report_path = $report_dir_path . $latest_filename;

                        if (file_exists($metric_path) || file_exists($report_path)) {
                            $response["company_name"] = $company_name;
                            $response["name"] = $name;
                            $response["email"] = $email;
                            $response["client_master_id"] = $client_master_id;
                            $response["client_id"] = $client_id;
                            //  $response["every_week_on"] = $every_week_on;
                            $response['report_start_date'] = $report_start_date;
                            $response['metrics_path'] = $metric_path;
                            $response["report_path"] = $report_path;
                            $response['cushion_value'] = $cushion_value;
                            $response["error"] = false;
                        }
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getReportValues($path, $position, $type, $company_name, $pdf, $result_message = null)
    {
        $report_obj = json_decode(file_get_contents($path), true);
        $last_updated_on = date("d M Y", strtotime($report_obj['last_updated_time']));

        $title_border = array('B' => array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(99, 177, 69)));
        $line_border = array('B' => array('width' => 0.25, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(89, 89, 89)));
        $pdf->writeHTML($result_message, true, false, false, false, '');

        if ($position == 1) {
            $pdf->SetFont('', '', '11');
            $pdf->Cell(100, 7, "", "0", 0, 'L', 0);
            $pdf->Ln();
            $pdf->Cell(100, 7, "All values are represented in SGD", "0", 0, 'L', 0);
            $pdf->Ln();

            $pdf->SetFont('', 'B', '12');
            $pdf->Cell(280, 7, "TOTAL CASH ON HAND", $title_border, 0, 'L', 0);
            $pdf->Ln();
            $pdf->SetFont('');

            $pdf->SetFont('', 'B', '11');
            $pdf->Cell(100, 7, "Bank Summary", "0", 0, 'L', 0);
            $pdf->Ln();
            $pdf->SetFont('');

            $pdf->SetFont('', '', '9');
            $pdf->Cell(100, 6, $company_name . ' | As at ' . $last_updated_on, "0", 0, 'L', 0);
            $pdf->Ln();
            $pdf->SetFont('');
        } else if ($position == 2) {
            if (isset($report_obj[$position]["agedPayables"])) {

                $pdf->Cell(100, 7, "", "0", 0, 'L', 0);
                $pdf->Ln();
                $pdf->SetFont('', 'B', '12');
                $pdf->Cell(280, 7, "CURRENT ACCOUNTS PAYABLE DUE", $title_border, 0, 'L', 0);
                $pdf->Ln();
                $pdf->SetFont('');

                $pdf->SetFont('', 'B', '11');
                $pdf->Cell(100, 7, "Aged Payables", "0", 0, 'L', 0);
                $pdf->Ln();
                $pdf->SetFont('');

                $pdf->SetFont('', '', '9');
                $pdf->Cell(100, 6, $company_name . ' | As at ' . $last_updated_on, "0", 0, 'L', 0);
                $pdf->Ln();
                $pdf->SetFont('');
            }
        } else if ($position == 3) {
            if (isset($report_obj[$position]["agedReceivables"])) {
                $pdf->Cell(100, 7, "", "0", 0, 'L', 0);
                $pdf->Ln();
                $pdf->SetFont('', 'B', '12');
                $pdf->Cell(280, 7, "CURRENT ACCOUNTS RECEIVABLE DUE", $title_border, 0, 'L', 0);
                $pdf->Ln();
                $pdf->SetFont('');

                $pdf->SetFont('', 'B', '11');
                $pdf->Cell(100, 7, "Aged Receivables", "0", 0, 'L', 0);
                $pdf->Ln();
                $pdf->SetFont('');

                $pdf->SetFont('', '', '9');
                $pdf->Cell(100, 6, $company_name . ' | As at ' . $last_updated_on, "0", 0, 'L', 0);
                $pdf->Ln();
                $pdf->SetFont('');
            }
        } else if ($position == 4) {
            $date_value = $report_obj['4']['fromToDate'];

            $pdf->Cell(100, 7, "", "0", 0, 'L', 0);
            $pdf->Ln();
            $pdf->SetFont('', 'B', '12');
            $pdf->Cell(280, 7, "ESTIMATED PAYROLL", $title_border, 0, 'L', 0);
            $pdf->Ln();
            $pdf->SetFont('');

            $pdf->SetFont('', 'B', '11');
            $pdf->Cell(100, 7, "Estimated Payroll", "0", 0, 'L', 0);
            $pdf->Ln();
            $pdf->SetFont('');

            $pdf->SetFont('', '', '9');
            $pdf->Cell(100, 6, $company_name . ' | All Terittories | ' . $date_value, "0", 0, 'L', 0);
            $pdf->Ln();
            $pdf->SetFont('');
        }

        if ($position == 1) {
            $pdf->SetFont('', 'B', '10');
            $pdf->Cell(140, 8, "Bank Summary", $title_border, 0, 'L', 0);
            $pdf->Cell(140, 8, "Closing Balance", $title_border, 0, 'R', 0);
            $pdf->Ln();
            $pdf->SetFont('', '', '9');
        } else if ($position == 3) {
            if (isset($report_obj[$position]["agedReceivables"])) {
                $header_value_arr = array("Name");
                $pdf->SetFont('', 'B', '10');
                $pdf->Cell(100, 8, "Receivable", $title_border, 0, 'L', 0);
                foreach ($report_obj[$position]["agedReceivables"][0]["total_receivables"] as $key => $value) {
                    if ($key != "Older" && $key != "Total") {
                        $pdf->Cell(30, 8, $key, $title_border, 0, 'R', 0);
                        $header_value_arr[] = $key;
                    }
                }
                $header_value_arr[] = "Older";
                $pdf->Cell(30, 8, "Older", $title_border, 0, 'R', 0);
                $header_value_arr[] = "Total";
                $pdf->Cell(30, 8, "Total", $title_border, 0, 'R', 0);
                $pdf->Ln();
                $pdf->SetFont('', '', '9');
            }
        } else if ($position == 2) {
            if (isset($report_obj[$position]["agedPayables"])) {
                $header_value_arr = array("Name");
                $pdf->SetFont('', 'B', '10');
                $pdf->Cell(100, 8, "Payables", $title_border, 0, 'L', 0);
                foreach ($report_obj[$position]["agedPayables"][0]["total_payables"] as $key => $value) {
                    if ($key != "Older" && $key != "Total") {
                        $pdf->Cell(30, 8, $key, $title_border, 0, 'R', 0);
                        $header_value_arr[] = $key;
                    }
                }
                $header_value_arr[] = "Older";
                $pdf->Cell(30, 8, "Older", $title_border, 0, 'R', 0);
                $header_value_arr[] = "Total";
                $pdf->Cell(30, 8, "Total", $title_border, 0, 'R', 0);
                $pdf->Ln();
                $pdf->SetFont('', '', '9');
            }
        } else if ($position == 4) {
            $pdf->SetFont('', 'B', '9');
            $pdf->Cell('140', 8, "Payroll", $title_border, 0, 'L', 0);
            
            $total_count = count($report_obj[$position]["estimatedPayroll"][0][0]) - 1;

            if ($total_count > 6) {
                $artay_divide_value = 230;
            } else {
                $artay_divide_value = 230 / $total_count;
            }

            foreach ($report_obj[$position]["estimatedPayroll"][0] as $key => $inner_array) {
                unset($inner_array[0]);
                if (is_numeric($key)) {
                    foreach ($inner_array as $key => $inner_value) {

                        if ($total_count > 1) {
                            if ($inner_value['Value'] == "Total") {
                                $pdf->SetFont('', 'B');
                                $pdf->Cell(140, 8, $inner_value['Value'], $title_border, 0, 'R', 0);
                            }
                        } else {
                            $pdf->SetFont('', 'B');
                            $pdf->Cell(140, 8, $inner_value['Value'], $title_border, 0, 'R', 0);
                        }
                    }
                }
            }
            $pdf->Ln();
            $pdf->SetFont('', '', '9');
        }

        if ($position == 1) {
            unset($report_obj[$position]["accountsSummary"]['org_name']);
            foreach ($report_obj[$position]["accountsSummary"] as $key => $value) {
                $append_flag = 0;
                if ($append_flag == 0) {
                    $text_style = "";
                    if ($value['bank_account_name'] == "Total") {
                        $text_style = "font-weight:bold;";
                        $pdf->SetFont('', 'B', '10');
                        $pdf->SetTextColor("", "", "");
                        $pdf->Cell(140, 7, $value['bank_account_name'], $title_border, 0, 'L', 0);
                        if ($value['closing_balance'] < 0) {
                            $pdf->SetTextColor(191, 43, 48);
                            $row_value = $this->format_number($value['closing_balance'], 0);
                        } else {
                            $pdf->SetTextColor("", "", "");
                            $row_value = "$" . number_format(round($value['closing_balance']));
                        }
                        $pdf->Cell(140, 7, $row_value, $title_border, 0, 'R', 0);
                        $pdf->Ln();
                        $pdf->SetFont('', '', '9');

                    } else {
                        $pdf->SetFont('', '', '9');
                        $pdf->SetTextColor("", "", "");
                        $pdf->Cell(140, 7, $value['bank_account_name'], $line_border, 0, 'L', 0);
                        if ($value['closing_balance'] < 0) {
                            $pdf->SetTextColor(191, 43, 48);
                            $row_value = $this->format_number($value['closing_balance'], 0);
                        } else {
                            $pdf->SetTextColor("", "", "");
                            $row_value = "$" . number_format(round($value['closing_balance']));
                        }
                        $pdf->Cell(140, 7, $row_value, $line_border, 0, 'R', 0);
                        $pdf->Ln();
                    }
                }
            }
        } else if ($position == 3) {
            $agedReceivables_nameArray = $report_obj[$position]["agedReceivables"][0];

            array_multisort(array_map(function ($element) {
                return $element["Name"];
            }, $agedReceivables_nameArray), SORT_ASC, $agedReceivables_nameArray);

            $agedReceivables_array[0] = $agedReceivables_nameArray;
            $total_receivables = $agedReceivables_array[0]['total_receivables'];
            unset($agedReceivables_array[0]['total_receivables']);
            $agedReceivables_array[0]['total_receivables'] = $total_receivables;

            foreach ($agedReceivables_array as $key => $value) {
                foreach ($value as $key => $receivable_record) {
                    foreach ($header_value_arr as $receive_rows) {
                        $receivable_final = $receivable_record[$receive_rows];
                        $text_style = "";
                        if ($key == "total_receivables") {
                            $text_style = "font-weight:bold; border-bottom: 2px solid #63b145;";
                        }
                        if (empty($receivable_final) && $receive_rows == "Name") {
                            $receivable_final = "Total Receivables";
                            $line_border = $title_border;
                            $pdf->SetFont('', 'B', '10');
                        }
                        if ($receive_rows != "Name") {
                            if ($receivable_final < 0) {
                                $pdf->SetTextColor(191, 43, 48);
                                $result_value = $this->format_number($receivable_final, 0);
                            } else {
                                $pdf->SetTextColor("", "", "");
                                $result_value = "$" . number_format(round($receivable_final));
                            }
                            $text_align = "R";
                            $width = 30;
                        } else {
                            $result_value = $receivable_final;
                            $text_align = "L";
                            $width = 100;
                        }
                        $pdf->Cell($width, 7, $result_value, $line_border, 0, $text_align, 0);
                    }
                    $pdf->Ln();
                }
            }
        } else if ($position == 2) {
            $agedReceivables_nameArray = $report_obj[$position]["agedPayables"][0];

            array_multisort(array_map(function ($element) {
                return $element["Name"];
            }, $agedReceivables_nameArray), SORT_ASC, $agedReceivables_nameArray);

            $agedPayables_array[0] = $agedReceivables_nameArray;
            $total_payables = $agedPayables_array[0]['total_payables'];
            unset($agedPayables_array[0]['total_payables']);
            $agedPayables_array[0]['total_payables'] = $total_payables;

            foreach ($agedPayables_array as $key => $value) {
                foreach ($value as $key => $receivable_record) {
                    foreach ($header_value_arr as $receive_rows) {
                        $receivable_final = $receivable_record[$receive_rows];
                        $text_style = "";
                        if ($key == "total_payables") {
                            $text_style = "font-weight:bold; border-bottom: 2px solid #63b145;";
                        }
                        if (empty($receivable_final) && $receive_rows == "Name") {
                            $receivable_final = "Total Payables";
                            $line_border = $title_border;
                            $pdf->SetFont('', 'B', '10');
                        }
                        if ($receive_rows != "Name") {
                            if ($receivable_final < 0) {
                                $pdf->SetTextColor(191, 43, 48);
                                $result_value = $this->format_number($receivable_final, 0);
                            } else {
                                $pdf->SetTextColor("", "", "");
                                $result_value = "$" . number_format(round($receivable_final));
                            }
                            $text_align = "R";
                            $width = 30;
                        } else {
                            $result_value = $receivable_final;
                            $text_align = "L";
                            $width = 100;
                        }
                        $pdf->Cell($width, 7, $result_value, $line_border, 0, $text_align, 0);
                    }$pdf->Ln();
                }
            }
        } else if ($position == 4) {
            $total_count = count($report_obj[$position]["estimatedPayroll"][0][0]) - 1;
            $artay_divide_value = 230 / $total_count;

            foreach ($report_obj[$position]["estimatedPayroll"][0] as $firstKey => $inner_array) {
                if (!is_numeric($firstKey)) {
                    $text_align = "left";
                    $text_style = "";
                    if ($total_count == $i) {
                        $line_border = $title_border;
                    }
                    $result_message .= '<tr>';
                    $end_array = end($inner_array);
                    if ($total_count > 1) {
                        if ($receivable_final < 0) {
                            $pdf->SetTextColor(191, 43, 48);
                            $result_value = $this->format_number_exportXls($end_array['Value'], 0);
                        } else {
                            $pdf->SetTextColor("", "", "");
                            $result_value = number_format(round($end_array['Value']));
                        }

                        foreach ($inner_array as $key => $inner_value) {
                            if ($key == 1) {
                                $text_align = "right";
                            }
                            if ($inner_value['Value'] == "Total Employee Compensation & Related Expenses") {
                                $pdf->SetFont('', 'B', '9');
                                $line_border = $title_border;
                            }
                            if ($key == 0) {
                                $pdf->Cell(50, 7, $inner_value['Value'], $line_border, 0, 'L', 0);
                            }
                        }
                        if ($total_count > 1) {
                            $pdf->Cell(230, 7, $result_value, $line_border, 0, 'R', 0);
                        }
                        $pdf->Ln();
                    } else {
                        if ($receivable_final < 0) {
                            $pdf->SetTextColor(191, 43, 48);
                            $result_value = $this->format_number($end_array['Value'], 0);
                        } else {
                            $pdf->SetTextColor("", "", "");
                            $result_value = "$" . number_format(round($end_array['Value']));
                        }
                        foreach ($inner_array as $key => $inner_value) {
                            if ($key == 1) {
                                $text_align = "right";
                            }
                            if ($inner_value['Value'] == "Total Employee Compensation & Related Expenses") {
                                $line_border = $title_border;
                                $pdf->SetFont('', 'B', '9');
                            }

                            if ($key > 0) {
                                $pdf->Cell(140, 7, $result_value, $line_border, 0, 'R', 0);
                            } else {
                                $pdf->Cell(140, 7, $inner_value['Value'], $line_border, 0, 'L', 0);
                            }
                        }
                        $pdf->Ln();
                    }
                }
            }
        }

        if ($position == 3) {
            if (isset($report_obj[$position]["agedReceivables"])) {
                $pdf->SetFont('', '', '10');
                $pdf->Cell(200, 10, "Note: If an invoice is in a foreign currency, the amount will be converted to the reporting currency using the rate applied to the invoice when it was entered into the system.", "0", 0, 'L', 0);
            }
        }
        return $pdf;
    }

    public function getInvoiceListData($client_id, $client_master_id, $data)
    {
        $customResponse = array();
        try {
            $XeroOAuth = $data['XeroOAuth'];
            $XeroOAuth->config['access_token'] = utf8_encode($data['oauth_token']);
            $XeroOAuth->config['access_token_secret'] = utf8_encode($data['oauth_token_secret']);
            $report_start_date = $this->getReportFromDate($client_master_id);
            $frmDate = date("Y, m, d", strtotime($report_start_date));
            $toyrDate = date("Y-m-d", strtotime($report_start_date . " +1 year"));
            $toDate = date('Y, m, d', strtotime('-1 day', strtotime($toyrDate)));
            $response = $this->makeRequest($client_id, 'getInvoiceListData', $XeroOAuth, 'GET', INVOICE_CONTACT_BY_NAME, array('Where' => 'Type=="ACCREC" AND Date >= DateTime(' . $frmDate . ') AND Date <= DateTime(' . $toDate . ')'), "", "json",$client_master_id);
            $invoiceContactList = json_decode($XeroOAuth->response['response'], true);
            if ($response['code'] == 302) {
                $response["error"] = true;
                $response["message"] = XERO_HAVING_PROBLEM;
                return $response;
            }
            if ($response['code'] == 401) {
                if (strpos($response['response'], 'token_expired') !== false || strpos($response['response'], 'token_rejected') !== false) {
                    $resp = $this->XeroRefreshToken($client_id, $client_master_id);
                    if (isset($resp['authurl'])) {
                        $authurl = $resp['authurl'];
                        header('Location:' . $authurl);die();
                    }
                }
            }
            if ($response['code'] == 200) {
                if (count($invoiceContactList['Invoices']) > 0) {
                    $receivablesInvoice = $invoiceContactList['Invoices'];
                    $amountTotal = array();
                    $contactNameArr = array();
                    foreach ($receivablesInvoice as $k => $value) {
                        $contactNameArr[$value['Contact']['Name']] = $value['Contact']['Name'];
                    }
                    $finalArr = array();
                    sort($contactNameArr);
                    foreach ($contactNameArr as $key => $value) {
                        $amountTotal = array();
                        $amountPush = array();
                        $finalMonthArr = array();
                        foreach ($receivablesInvoice as $k => $dataValue) {
                            if ($value == $dataValue['Contact']['Name']) {
                                preg_match("#/Date\((\d{10})\d{3}(.*?)\)/#", $dataValue['Date'], $match);
                                $month = date("n", $match[1]);
                                if ($dataValue['Total'] > 0) {
                                    $amountTotal[$month] += $dataValue['Total'];
                                }
                            }
                        }
                        $finalMonthArr[] = $amountTotal;
                        $finalArr[] = array('company_name' => $value, 'month_amount' => $finalMonthArr);
                    }
                    $customResponse["result"] = $finalArr;
                    $customResponse["error"] = false;
                } else {
                    $customResponse["error"] = true;
                    $customResponse["message"] = NO_INVOICE_LIST;
                }
                return $customResponse;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getPLReportValues($path, $position, $report_start_date, $report_type, $reportDate, $pdf, $message, $sorting = 0)
    {
        $report_obj = json_decode(file_get_contents($path), true);
        $last_updated_on = date("d M Y", strtotime($report_obj['last_updated_time']));
        $curMonth = date("m", time());
        $curQuarter = ceil($curMonth / 3);
        $quarter_array = array();
        $current_date = date("Y-m-d");
        $added_month = $report_start_date;
        for ($i = 1; $i <= $curQuarter; $i++) {
            $add_month_count = $i * 3;
            $add_up_month = date("Y-m-d", strtotime("+" . $add_month_count . " month", strtotime($added_month)));
            $final_addon_date = date('Y-m-d', strtotime('-1 day', strtotime($add_up_month)));
            if ($final_addon_date > $current_date) {
                $quarter_array[$i] = date("M y", strtotime($current_date));
            } else {
                $quarter_array[$i] = date("M y", strtotime($final_addon_date));
            }
        }

        $title_border = array('B' => array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(99, 177, 69)));
        $line_border = array('B' => array('width' => 0.25, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(89, 89, 89)));

        $balance_and_stmt_report_date_array = array();
        $pnl_report_date_array = array();
        $month_data_value = $report_start_date;
        for ($i = 1; $i <= 12; $i++) {
            $month_data_count = $i * 1;
            $month_data_val = date("Y-m-d", strtotime("+" . $month_data_count . " month", strtotime($month_data_value)));
            $final_addon_month = date('Y-m-d', strtotime('-1 day', strtotime($month_data_val)));
            $date_num_value = date("m", strtotime($final_addon_month));
            if ($final_addon_month > $current_date) {
                $pnl_report_date_array[] = date("M y", strtotime($current_date));
                break;
            } else {
                $balance_and_stmt_report_date_array[] = date("d M y", strtotime($final_addon_month));
                $pnl_report_date_array[] = date("M y", strtotime($final_addon_month));
            }
        }

        $message .= '<table style="width:100%">
                        <tr><td style="width: 100%;font-size: 8px;text-align:center;"> As at ' . $last_updated_on . '</td></tr>
                    </table>';
        $pdf->writeHTML($message, true, false, false, false, '');

        if ($position == 1) {
            $balance_sheet_array = $report_obj[$position]["balanceSheet"];
            $final_array = array();

            foreach ($balance_sheet_array as $key => $sheet_value) {
                foreach ($sheet_value['summaryRow'] as $inner_key => $value) {
                    if ($inner_key != 'equity_gray') {
                        $final_array[$key]['header'][$inner_key] = $value;
                    }
                }
                foreach ($sheet_value as $inner_key => $value) {
                    if ($inner_key != 'summaryRow' && !is_numeric($inner_key) && $inner_key != "no_expand") {
                        $final_array[$key]['rows'][$inner_key] = $value;
                    } else if ($inner_key != 'summaryRow' && (is_numeric($inner_key) || $inner_key == "no_expand")) {
                        $final_array[$key]['header'][$key][$inner_key] = $value;
                    }
                }
            }

            if (count($final_array) > 0) {
                $w = array(65, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20);

                // Header
                $header = array_reverse($reportDate);
                $pdf->SetFont('', 'B');
                $num_headers = count($header);
                $pdf->Cell($w[0], 7, "", $title_border, 0, 'C', 0);
                for ($i = 0; $i < $num_headers; ++$i) {
                    $pdf->Cell($w[($i + 1)], 7, $header[$i], $title_border, 0, 'R', 0);
                }
                $pdf->SetFont('');
                $pdf->Ln();

                foreach ($final_array as $main_key => $main_value) {
                    $total_record_value = array();
                    $no_expand = 0;

                    $pdf->setCellPaddings($left = '0', $top = '', $right = '', $bottom = '');
                    $pdf->SetFont('', 'B');

                    if ($main_key == "Total Assets" || $main_key == "Total Liabilities" || $main_key == "Net Assets") {
                    } else {
                        $pdf->SetTextColor('', '', '');
                        for ($i = 0; $i < $num_headers; ++$i) {
                            if ($i == 0) {
                                $pdf->Cell($w[$i], 7, $main_key, 0, 0, 'L', 0);
                            } else {
                                $pdf->Cell($w[$i], 7, '', 0, 0, 'L', 0);
                            }
                        }
                        $pdf->Ln();
                    }

                    foreach ($main_value as $second_level_key => $second_level_value) {
                        if ($second_level_key == "header") {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                if (array_key_exists('no_expand', $third_level_value)) {
                                    $no_expand = 1;
                                    unset($third_level_value['no_expand']);
                                }
                                if ($no_expand == 1) {
                                    $this->buildPDFforTrend($third_level_key, $third_level_value, $pdf, true, false, $w, $title_border, $line_border, $reportDate);
                                } else {
                                    $total_record_key = $third_level_key;
                                    foreach ($third_level_value as $key => $row_value) {
                                        $total_record_value[] = $row_value;
                                    }
                                }
                            }
                        } else if ($second_level_key == "rows") {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                $this->buildPDFforTrend($third_level_key, $third_level_value, $pdf, false, false, $w, $title_border, $line_border, $reportDate);
                            }
                        }
                    }

                    if (count($total_record_value) > 0) {
                        $this->buildPDFforTrend($total_record_key, $total_record_value, $pdf, true, true, $w, $title_border, $line_border, $reportDate);
                    }
                }
            }
        } else if ($position == 2) {
            $consolidate_pnl_array = $report_obj[$position][$report_type][$report_type];
            $consolidate_final_array = array();
            foreach ($consolidate_pnl_array as $key => $consolidate_inner_value) {
                $header_flag = 0;
                foreach ($consolidate_inner_value['summaryRow'] as $inner_key => $value) {
                    if (count($consolidate_inner_value['summaryRow'][$inner_key]) > 4) {
                        foreach ($value as $inner_key1 => $value1) {
                            $consolidate_final_array[$key]['header'][] = $value1["Value"];
                            $header_flag = 1;
                        }
                    }
                }

                $temp_array = array();
                foreach ($consolidate_inner_value as $inner_key => $consolidate_final_value) {
                    if ($inner_key != "summaryRow" && $inner_key != "Employee Compensation & Related Expenses") {
                        foreach ($consolidate_final_value as $last_key => $value) {
                            if (is_numeric($last_key)) {
                                if ($header_flag == 1) {
                                    $consolidate_final_array[$key]['rows'][$inner_key][] = $value['Value'];
                                } else {
                                    $temp_array[] = $value['Value'];
                                }
                            } else {
                                $temp_array[$last_key] = $value;
                            }
                        }
                        if ($header_flag == 0) {
                            $consolidate_final_array[$key]['header'] = $temp_array;
                        }

                    } else if ($inner_key == "Employee Compensation & Related Expenses") {
                        $emp_rows_arr = array();
                        $emp_comp = $consolidate_final_value;
                        foreach ($emp_comp as $emp_comp_key => $emp_comp_value) {
                            if (is_numeric($emp_comp_key)) {
                                foreach ($emp_comp_value as $last_key => $value) {
                                    if (is_numeric($last_key)) {
                                        if ($header_flag == 1) {
                                            $emp_rows_arr[$emp_comp_key][] = $value['Value'];
                                        } else {
                                            $temp_array[] = $value['Value'];
                                        }
                                    } else {
                                        $temp_array[$last_key] = $value;
                                    }
                                }
                            } else if ($emp_comp_key == "summaryRow") {
                                foreach ($emp_comp_value[0] as $last_key => $value) {
                                    if (is_numeric($last_key)) {
                                        if ($header_flag == 1) {
                                            $consolidate_final_array[$key]['rows'][$inner_key]['header'][] = $value['Value'];
                                        } else {
                                            $temp_array[] = $value['Value'];
                                        }
                                    } else {
                                        $temp_array[$last_key] = $value;
                                    }
                                }
                            }
                        }
                        $consolidate_final_array[$key]['rows'][$inner_key]['rows'] = $emp_rows_arr;
                    }
                }
            }

            if (count($consolidate_final_array) > 0) {
                //Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=0, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M')
                $w = array(60, 35, 30, 35, 30);

                $pdf->SetFont('', 'B');
                $pdf->Cell($w[0], 7, '', $title_border, 0, 'R', 0, '', 0);
                $pdf->Cell($w[1], 7, 'Actual', $title_border, 0, 'R', 0, '', 0);
                $pdf->Cell($w[2], 7, 'Budget', $title_border, 0, 'R', 0, '', 0);
                $pdf->Cell($w[3], 7, 'Variance', $title_border, 0, 'R', 0, '', 0);
                $pdf->Cell($w[4], 7, 'Variance %', $title_border, 0, 'R', 0, '', 0);
                $pdf->Ln();
                $pdf->SetFont('');

                foreach ($consolidate_final_array as $main_key => $main_value) {
                    $total_value = array();
                    $emp_comp_total_value = array();
                    foreach ($main_value as $second_level_key => $second_level_value) {
                        $no_expand = 0;
                        if ($second_level_key == "header") {
                            if (array_key_exists('no_expand', $second_level_value)) {
                                $no_expand = 1;
                                unset($second_level_value['no_expand']);
                            }

                            if ($no_expand == 1) {
                                $this->buildPDFforConsolidated($second_level_value, $pdf, true, false, $w, $title_border, $line_border);
                            } else {
                                $pdf->setCellPaddings($left = '0', $top = '', $right = '', $bottom = '');
                                $j = 0;
                                foreach ($second_level_value as $key => $row_value) {
                                    if (!is_array($row_value)) {
                                        if ($key == 0) {
                                            $pdf->SetFont('', 'B');
                                            $pdf->Cell($w[$j], 7, str_replace("Total ", "", $row_value), 0, 0, 'L', 0, '', 0);
                                            array_push($total_value, $row_value);
                                        } else {
                                            $pdf->SetFont('');
                                            $pdf->Cell($w[$j], 7, '', 0, 0, 'L', 0, '', 0);
                                            array_push($total_value, $row_value);
                                        }
                                    }
                                    $j++;
                                }
                                $pdf->Ln();
                            }
                        } else if ($second_level_key == "rows") {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                if (!is_numeric($third_level_key) && $third_level_key == "Employee Compensation & Related Expenses") {
                                    foreach ($third_level_value as $fourth_level_key => $forth_level_value) {
                                        if ($fourth_level_key == "header") {
                                            $pdf->setCellPaddings($left = '10', $top = '', $right = '', $bottom = '');
                                            $k = 0;
                                            foreach ($forth_level_value as $key => $row_value) {
                                                if (!is_array($row_value)) {
                                                    if ($key == 0) {
                                                        $pdf->SetFont('', 'B');
                                                        $pdf->Cell($w[$k], 7, str_replace("Total ", "", $row_value), 0, 0, 'L', 0, '', 0);
                                                        array_push($emp_comp_total_value, $row_value);
                                                    } else {
                                                        $pdf->SetFont('');
                                                        $pdf->Cell($w[$k], 7, '', 0, 0, 'L', 0, '', 0);
                                                        array_push($emp_comp_total_value, $row_value);
                                                    }
                                                }
                                                $j++;
                                            }
                                            $pdf->Ln();
                                        } else if ($fourth_level_key == "rows") {
                                            foreach ($forth_level_value as $fifth_level_key => $fifth_level_value) {
                                                $this->buildPDFforConsolidated($fifth_level_value, $pdf, false, false, $w, $title_border, $line_border, 10);
                                            }
                                        }
                                    }
                                } else {
                                    $this->buildPDFforConsolidated($third_level_value, $pdf, false, false, $w, $title_border, $line_border);
                                }
                            }
                        }
                    }
                    if (count($emp_comp_total_value) > 0) {
                        $this->buildPDFforConsolidated($emp_comp_total_value, $pdf, true, true, $w, $title_border, $line_border, 10);
                    }
                    if (count($total_value) > 0) {
                        $this->buildPDFforConsolidated($total_value, $pdf, true, true, $w, $title_border, $line_border);
                    }
                }
            }
        } else if ($position == 3) {
            $trend_pnl_array = $report_obj[$position]["trendPNL"];
            $trend_pl_month_final_array = array();
            foreach ($trend_pnl_array as $key => $trend_month_inner_value) {
                if ($key != "trendPNL_Quartely") {
                    if (is_array($trend_month_inner_value['summaryRow'])) {
                        foreach ($trend_month_inner_value['summaryRow'] as $inner_key => $value) {
                            $trend_pl_month_final_array[$key]['header'][$inner_key] = $value;
                        }
                    } else {
                        $trend_pl_month_final_array[$key]['header'][$key] = $trend_month_inner_value;
                    }

                    if ($key == "Gross Profit" || $key == "Operating Profit" || $key == "Net Profit") {
                    } else {
                        foreach ($trend_month_inner_value as $inner_key => $trend_month_final_value) {
                            if ($inner_key != 'summaryRow') {
                                $inner_check_array = $trend_month_inner_value[$inner_key]['summaryRow'];
                                if (is_array($inner_check_array)) {
                                    $trend_pl_month_final_array[$inner_key]['header'] = $inner_check_array;
                                    foreach ($trend_month_final_value as $last_key => $value) {
                                        if ($last_key != 'summaryRow') {
                                            $key_minus_flag = 0;
                                            if (!array_key_exists("0", $value)) {
                                                $key_minus_flag = 1;
                                            }
                                            $new_value_array = array();
                                            foreach ($value as $l_key => $new_value) {
                                                if ($key_minus_flag == 1) {
                                                    $l_key = $l_key - 1;
                                                }
                                                $new_value_array[$l_key] = $new_value;
                                            }
                                            $trend_pl_month_final_array[$inner_key]['rows'][$last_key] = $new_value_array;
                                        }
                                    }
                                } else {
                                    $key_minus_flag = 0;
                                    if (!array_key_exists("0", $trend_month_final_value)) {
                                        $key_minus_flag = 1;
                                    }
                                    foreach ($trend_month_final_value as $last_key => $value) {
                                        if ($key_minus_flag == 1) {
                                        }
                                        $trend_pl_month_final_array[$key]['rows'][$inner_key][$last_key] = $value;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            unset($trend_pl_month_final_array['Operating Expenses']);

            if (count($trend_pl_month_final_array) > 0) {
                $w = array(73, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18);

                // Header
                $header = array_reverse($reportDate);
                $pdf->SetFont('', 'B');
                $num_headers = count($header) + 1;
                $pdf->Cell($w[0], 7, "", $title_border, 0, 'C', 0);
                for ($i = 0; $i < $num_headers; ++$i) {
                    if ($i == 0) {
                        $pdf->Cell($w[($i + 1)], 7, "Current Month", $title_border, 0, 'R', 0);
                    } else {
                        $pdf->Cell($w[($i + 1)], 7, substr($header[$i], 3), $title_border, 0, 'R', 0);
                    }
                }
                $pdf->SetFont('');
                $pdf->Ln();
                $total_operating_record_value = array();
                foreach ($trend_pl_month_final_array as $main_key => $main_value) {
                    $total_record_value = array();
                    $no_expand = 0;
                    $build_heading = 1;

                    $pdf->setCellPaddings($left = '0', $top = '', $right = '', $bottom = '');
                    if ($main_key == "Gross Profit" || $main_key == "Operating Profit" || $main_key == "Net Profit") {
                        $build_heading = 0;
                    } else if ($main_key == "Employee Compensation & Related Expenses") {
                        $pdf->setCellPaddings($left = '5', $top = '', $right = '', $bottom = '');
                        $build_heading = 1;
                    }

                    if ($build_heading == 1) {
                        $pdf->SetFont('', 'B');
                        foreach ($main_value as $second_level_key => $second_level_value) {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                $array_size_count = sizeof($third_level_value);
                            }
                        }
                        $pdf->SetTextColor('', '', '');
                        if ($array_size_count > 0) {
                            for ($i = 0; $i < $num_headers; ++$i) {
                                if ($i == 0) {
                                    $pdf->Cell($w[$i], 7, $main_key, 0, 0, 'L', 0);

                                } else {
                                    $pdf->Cell($w[$i], 7, '', 0, 0, 'L', 0);
                                }
                            }
                            $pdf->Ln();
                        }
                    }

                    foreach ($main_value as $second_level_key => $second_level_value) {
                        if ($second_level_key == "header") {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                if (sizeof($third_level_value) > 0) {
                                    if ($main_key == "Gross Profit" || $main_key == "Operating Profit" || $main_key == "Net Profit") {
                                        unset($third_level_value['no_expand']);
                                        $no_expand = 1;
                                        $this->buildPDFforTrend($third_level_key, $third_level_value, $pdf, true, false, $w, $title_border, $line_border, $reportDate);
                                    } else {
                                        if ($third_level_key == "Total Operating Expenses") {
                                            if (isset($trend_pl_month_final_array["Employee Compensation & Related Expenses"])) {
                                                $total_employee_key = $third_level_key;
                                                foreach ($third_level_value as $key => $row_value) {
                                                    $total_operating_record_value[] = $row_value;
                                                }
                                            } else {
                                                $total_record_key = $third_level_key;
                                                foreach ($third_level_value as $key => $row_value) {
                                                    $total_record_value[] = $row_value;
                                                }
                                            }
                                        } else {
                                            $total_record_key = $third_level_key;
                                            foreach ($third_level_value as $key => $row_value) {
                                                $total_record_value[] = $row_value;
                                            }
                                        }
                                    }
                                }
                            }
                        } else if ($second_level_key == "rows") {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                if ($main_key == "Employee Compensation & Related Expenses") {
                                    $this->buildPDFforTrend($third_level_key, $third_level_value, $pdf, false, false, $w, $title_border, $line_border, $reportDate, 5);
                                } else {
                                    $this->buildPDFforTrend($third_level_key, $third_level_value, $pdf, false, false, $w, $title_border, $line_border, $reportDate);
                                }
                            }
                        }
                    }

                    if (count($total_record_value) > 0) {
                        if ($main_key == "Employee Compensation & Related Expenses") {
                            $this->buildPDFforTrend($total_record_key, $total_record_value, $pdf, true, false, $w, $title_border, $line_border, $reportDate, 8);
                            $this->buildPDFforTrend($total_employee_key, $total_operating_record_value, $pdf, true, true, $w, $title_border, $line_border, $reportDate, 8);
                        } else {
                            $this->buildPDFforTrend($total_record_key, $total_record_value, $pdf, true, true, $w, $title_border, $line_border, $reportDate);
                        }
                    }
                }
            }
        } elseif ($position == 4) {
            $trend_pl_quarter_final_array = array();
            $trend_pnl_quartely_array = $report_obj[3]["trendPNL"]['trendPNL_Quartely'];

            foreach ($trend_pnl_quartely_array as $key => $trend_quarter_inner_value) {
                if ($key != "trendPNL_Quartely") {
                    if (is_array($trend_quarter_inner_value['summaryRow'])) {
                        foreach ($trend_quarter_inner_value['summaryRow'] as $inner_key => $value) {
                            $trend_pl_quarter_final_array[$key]['header'][$inner_key] = $value;
                        }
                    } else {
                        $trend_pl_quarter_final_array[$key]['header'][$key] = $trend_quarter_inner_value;
                    }

                    if ($key == "Gross Profit" || $key == "Operating Profit" || $key == "Net Profit") {
                    } else {
                        foreach ($trend_quarter_inner_value as $inner_key => $trend_quarter_final_value) {
                            if ($inner_key != 'summaryRow') {
                                $inner_check_array = $trend_quarter_inner_value[$inner_key]['summaryRow'];
                                if (is_array($inner_check_array)) {
                                    $trend_pl_quarter_final_array[$inner_key]['header'] = $inner_check_array;
                                    foreach ($trend_quarter_final_value as $last_key => $value) {
                                        if ($last_key != 'summaryRow') {
                                            $key_minus_flag = 0;
                                            if (!array_key_exists("0", $value)) {
                                                $key_minus_flag = 1;
                                            }
                                            $new_value_array = array();
                                            foreach ($value as $l_key => $new_value) {
                                                if ($key_minus_flag == 1) {
                                                    $l_key = $l_key - 1;
                                                }
                                                $new_value_array[$l_key] = $new_value;
                                            }
                                            $trend_pl_quarter_final_array[$inner_key]['rows'][$last_key] = $new_value_array;
                                        }
                                    }
                                } else {
                                    $key_minus_flag = 0;
                                    if (!array_key_exists("0", $trend_quarter_final_value)) {
                                        $key_minus_flag = 1;
                                    }
                                    foreach ($trend_quarter_final_value as $last_key => $value) {
                                        if ($key_minus_flag == 1) {
                                        }
                                        $trend_pl_quarter_final_array[$key]['rows'][$inner_key][$last_key] = $value;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (count($trend_pl_quarter_final_array) > 0) {
                $w = array(60, 35, 30, 35, 30);

                // Header
                $quarter_array = $this->getQuartelyMonthArray();
                foreach ($quarter_array as $key => $value) {
                    $header[] = $value;
                }
                $pdf->SetFont('', 'B');
                $num_headers = count($header);
                $pdf->Cell($w[0], 7, "", $title_border, 0, 'C', 0);
                for ($i = 0; $i < $num_headers; ++$i) {
                    $pdf->Cell($w[($i + 1)], 7, $header[$i], $title_border, 0, 'R', 0);
                }
                $pdf->SetFont('');
                $pdf->Ln();

                foreach ($trend_pl_quarter_final_array as $main_key => $main_value) {
                    $total_record_value = array();
                    $no_expand = 0;
                    $build_heading = 1;

                    $pdf->setCellPaddings($left = '0', $top = '', $right = '', $bottom = '');
                    if ($main_key == "Gross Profit" || $main_key == "Operating Profit" || $main_key == "Net Profit") {
                        $build_heading = 0;
                    } else if ($main_key == "Employee Compensation & Related Expenses") {
                        $pdf->setCellPaddings($left = '5', $top = '', $right = '', $bottom = '');
                        $build_heading = 1;
                    }

                    if ($build_heading == 1) {
                        $pdf->SetFont('', 'B');
                        for ($i = 0; $i < $num_headers; ++$i) {
                            if ($i == 0) {
                                $pdf->Cell($w[$i], 7, $main_key, 0, 0, 'L', 0);
                            } else {
                                $pdf->Cell($w[$i], 7, '', 0, 0, 'L', 0);
                            }
                        }
                        $pdf->Ln();
                    }

                    foreach ($main_value as $second_level_key => $second_level_value) {
                        if ($second_level_key == "header") {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                if ($main_key == "Gross Profit" || $main_key == "Operating Profit" || $main_key == "Net Profit") {
                                    unset($third_level_value['no_expand']);
                                    $no_expand = 1;
                                    $this->buildPDFforTrendQuarterly($third_level_key, $third_level_value, $pdf, true, false, $w, $title_border, $line_border, $reportDate);
                                } else {
                                    $total_record_key = $third_level_key;
                                    foreach ($second_level_value as $key => $row_value) {
                                        $total_record_value = $row_value;
                                    }
                                }
                            }
                        } else if ($second_level_key == "rows") {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                if ($main_key == "Employee Compensation & Related Expenses") {
                                    $this->buildPDFforTrendQuarterly($third_level_key, $third_level_value, $pdf, false, false, $w, $title_border, $line_border, $reportDate, 5);
                                } else {
                                    $this->buildPDFforTrendQuarterly($third_level_key, $third_level_value, $pdf, false, false, $w, $title_border, $line_border, $reportDate);
                                }
                            }
                        }
                    }

                    if (count($total_record_value) > 0) {
                        if ($main_key == "Employee Compensation & Related Expenses") {
                            $this->buildPDFforTrendQuarterly($total_record_key, $total_record_value, $pdf, true, true, $w, $title_border, $line_border, $reportDate, 8);
                        } else {
                            $this->buildPDFforTrendQuarterly($total_record_key, $total_record_value, $pdf, true, true, $w, $title_border, $line_border, $reportDate);
                        }
                    }
                }
            }
        } elseif ($position == 6) {
            $contact_by_nameArray = $report_obj[$position]["invoiceByName"];

            if ($sorting == 0) {
                array_multisort(array_map(function ($element) {
                    return $element["value"]["total"]["amount"];
                }, $contact_by_nameArray), SORT_ASC, $contact_by_nameArray);
            } else if ($sorting == 1) {
                array_multisort(array_map(function ($element) {
                    return $element["value"]["total"]["amount"];
                }, $contact_by_nameArray), SORT_DESC, $contact_by_nameArray);
            } else if ($sorting == 2) {
                array_multisort(array_map(function ($element) {
                    return $element["value"]["total"]["amount"];
                }, $contact_by_nameArray), SORT_DESC, $contact_by_nameArray);
                $contact_by_nameArray = array_slice($contact_by_nameArray, 0, 10);
            }

            $total_array = [];
            $totalamtVal = 0;
            $loop = 0;
            foreach ($contact_by_nameArray as $keyArr => $arrVal) {
                foreach (array_reverse($reportDate) as $key => $value) {
                    $getMonth = date("n", strtotime($value));
                    $getYear = date("y", strtotime($value));
                    $getInnKey = null;
                    $getInnKey = $this->searchIdFromArray($getMonth . '-' . $getYear, $arrVal['value']);
                    $amtVal = 0;
                    if (sizeof($getInnKey) > 0) {
                        $amtVal = $arrVal['value'][$getInnKey]['amount'];
                    } else {
                        $amtVal = 0;
                    }

                    if ($loop == 0) {
                        $obj = array('amount' => $amtVal, 'month' => $getMonth, 'year' => $getYear);
                        array_push($total_array, $obj);
                    } else {
                        $getTotalInnKey = $this->searchIdFromArray($getMonth . '-' . $getYear, $total_array);
                        if (sizeof($getTotalInnKey) > 0) {
                            $totalamtVal = $total_array[$getTotalInnKey]['amount'];
                            $totalamtVal = $totalamtVal + $amtVal;
                            $total_array[$getTotalInnKey]['amount'] = $totalamtVal;
                        }
                    }
                }
                $loop++;
            }

            $total_array["total"]["amount"] = 0;
            array_push($contact_by_nameArray, array('company_name' => 'Total', 'value' => $total_array));

            foreach ($contact_by_nameArray as $keyArr => $arrVal) {
                $totalNetValue = 0;
                if ($arrVal['company_name'] == "Total") {
                    foreach ($arrVal['value'] as $key => $val) {
                        if ($key != "total") {
                            $totalNetValue = $totalNetValue + $val['amount'];
                        }
                    }
                    $contact_by_nameArray[$keyArr]['value']['total']['amount'] = $totalNetValue;
                } else {
                    if (isset($contact_by_nameArray[$keyArr]['value']['total']['amount'])) {
                        if ($contact_by_nameArray[$keyArr]['value']['total']['amount'] > 0) {
                            $totalNetValue = $totalNetValue + $contact_by_nameArray[$keyArr]['value']['total']['amount'];
                        }
                    }
                }
            }

            if (count($contact_by_nameArray) > 0) {
                $w = array(70, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15);

                // Header
                $header = array_reverse($reportDate);
                $pdf->SetFont('', 'B');
                $num_headers = count($header);
                $pdf->Cell($w[0], 7, "", $title_border, 0, 'C', 0);
                for ($i = 0; $i < $num_headers; ++$i) {
                    $pdf->Cell($w[($i + 1)], 7, substr($header[$i], 3), $title_border, 0, 'R', 0);
                }
                $pdf->Cell($w[13], 7, "$", $title_border, 0, 'R', 0);
                $pdf->Cell($w[14], 7, "%", $title_border, 0, 'R', 0);
                $pdf->Ln();

                foreach ($contact_by_nameArray as $keyArr => $arrVal) {
                    if ($arrVal['company_name'] == "Total") {
                        $pdf->SetFont('', 'B');
                        $border = $title_border;
                    } else {
                        $pdf->SetFont('');
                        $border = $line_border;
                    }
                    $pdf->Cell($w[0], 7, wordwrap($arrVal['company_name'], 15, "\n", true), $border, 0, 'L', 0);
                    // $pdf->MultiCell($w[0], 10, $arrVal['company_name'], $border,'L', 0, 0, '', '', true, 0, false, true, 10,'M');
                    foreach (array_reverse($reportDate) as $key => $value) {
                        $getMonth = date("n", strtotime($value));
                        $getYear = date("y", strtotime($value));
                        $getInnKey = null;
                        $getInnKey = $this->searchIdFromArray($getMonth . '-' . $getYear, $arrVal['value']);
                        $amtVal = '';
                        if (sizeof($getInnKey) > 0) {
                            $amtVal = $this->format_number($arrVal['value'][$getInnKey]['amount'], 1);
                            $pdf->Cell($w[1], 7, $amtVal, $border, 0, 'R', 0);
                        } else {
                            $pdf->Cell($w[1], 7, "$0", $border, 0, 'R', 0);
                        }

                        if ($key == count($reportDate) - 1) {
                            $totalAmt = '$0';
                            $calPercent = 0;
                            if (!empty($arrVal['value']['total']['amount'])) {
                                $totalAmt = $this->format_number($arrVal['value']['total']['amount'], 1);
                                $netAmt = $arrVal['value']['total']['amount'];
                                if ($totalNetValue > 0) {
                                    $calPercent = (($netAmt / round($totalNetValue)) * 100);
                                }
                            }
                            $pdf->Cell($w[1], 7, $this->format_number($netAmt, 1), $border, 0, 'R', 0);
                            $pdf->Cell($w[1], 7, round($calPercent) . "%", $border, 0, 'R', 0);
                        }
                    }
                    $pdf->Ln();
                }
            }
        } elseif ($position == 5) {
            $cashFlowStatementArr = $report_obj[$position]["cashFlowStatement"];

            if (count($cashFlowStatementArr) > 0) {
                $finalValue = array();
                foreach ($cashFlowStatementArr as $key => $value) {
                    foreach ($value as $key1 => $value1) {
                        $title = "";
                        $title = $value1['title'];
                        unset($value1['value']['no_expand']);
                        if (is_numeric($key1)) {
                            $finalValue[$key]['rows'][$title] = $value1['value'];
                        } else if ($key1 == 'summaryRow') {
                            $finalValue[$key]['header'][$title] = $value1['value'];
                        }
                    }
                }

                if (count($finalValue) > 0) {
                    $w = array(70, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20);

                    // Header
                    $header = array_reverse($reportDate);
                    $pdf->SetFont('', 'B');
                    $num_headers = count($header);
                    $pdf->Cell($w[0], 7, "", $title_border, 0, 'C', 0);
                    for ($i = 0; $i < $num_headers; ++$i) {
                        $pdf->Cell($w[($i + 1)], 7, substr($header[$i], 3), $title_border, 0, 'R', 0);
                    }
                    $pdf->SetFont('');
                    $pdf->Ln();

                    foreach ($finalValue as $main_key => $main_value) {
                        $total_record_value = array();
                        $no_expand = 0;

                        $pdf->setCellPaddings($left = '0', $top = '', $right = '', $bottom = '');
                        $pdf->SetFont('', 'B');
                        $pdf->SetTextColor('', '', '');

                        for ($i = 0; $i < $num_headers; ++$i) {
                            if ($i == 0) {
                                $pdf->Cell($w[$i], 7, $main_key, 0, 0, 'L', 0);
                            } else {
                                $pdf->Cell($w[$i], 7, '', 0, 0, 'L', 0);
                            }
                        }
                        $pdf->Ln();

                        foreach ($main_value as $second_level_key => $second_level_value) {
                            if ($second_level_key == "header") {
                                foreach ($second_level_value as $third_level_key => $third_level_value) {
                                    $total_record_key = $third_level_key;
                                    foreach ($third_level_value as $key => $row_value) {
                                        $total_record_value[] = $row_value;
                                    }
                                }
                            } else if ($second_level_key == "rows") {
                                foreach ($second_level_value as $third_level_key => $third_level_value) {
                                    $this->buildPDFforTrend($third_level_key, $third_level_value, $pdf, false, false, $w, $title_border, $line_border, $reportDate);
                                }
                            }
                        }

                        if (count($total_record_value) > 0) {
                            $this->buildPDFforTrend($total_record_key, $total_record_value, $pdf, true, true, $w, $title_border, $line_border, $reportDate);
                        }
                    }
                }
            }
        }
        return $result_message;
    }

    public function buildPDFforConsolidated($value, $pdf, $header, $summary, $w, $title_border, $line_border, $left_indentation = null)
    {
        if ($summary) {
            $pdf->setCellPaddings($left = '5', $top = '', $right = '', $bottom = '');
        } else {
            $pdf->setCellPaddings($left = '0', $top = '', $right = '', $bottom = '');
        }

        if ($left_indentation != null) {
            $pdf->setCellPaddings($left = $left_indentation, $top = '', $right = '', $bottom = '');
        }

        if ($header) {
            $pdf->SetFont('', 'B');
        } else {
            $pdf->SetFont('');
        }

        $j = 0;
        foreach ($value as $key => $row_value) {
            if (!is_array($row_value)) {
                if (is_numeric($key)) {
                    $type = 1;
                    $pdf->SetTextColor(0, 0, 0);
                    if ($row_value < 0) {
                        $pdf->SetTextColor(191, 43, 48);
                        $type = 0;
                    }
                    if ($row_value == "Operating Profit") {
                        $row_value = "EBITDA";
                    }
                    if ($key == 4) {
                        $row_value = $this->format_number($row_value, 2);
                    } else if ($key != 0 && $key != 4) {
                        $row_value = $this->format_number($row_value, $type);
                    }
                    if ($key == 0) {
                        $pdf->Cell($w[$j], 7, $row_value, $header ? $title_border : $line_border, 0, 'L', 0, '', 0);
                    } else {
                        $pdf->Cell($w[$j], 7, $row_value, $header ? $title_border : $line_border, 0, 'R', 0, '', 0);
                    }
                } else {
                    $pdf->Cell($w[$j], 7, $row_value, $header ? $title_border : $line_border, 0, 'L', 0, '', 0);
                }
            }
            $j++;
        }
        $pdf->Ln();
    }

    public function buildPDFforTrendxls($key, $value, $objPHPExcel, $row, $reportDate, $bold = null, $header = null, $row_border = null)
    {
        if ($key == "Operating Profit") {
            $key = "EBITDA";
        }

        if ($header) {
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
        }
        if ($key == "Operating Profit") {
            $key = "EBITDA";
        }

        $final_row_array = array();
        $col = 1;
        foreach (array_reverse($reportDate) as $date_key => $date_value) {
            $month = date("n", strtotime($date_value));
            $year = date("y", strtotime($date_value));
            $getInnKey = $this->searchIdFromArray($month . '-' . $year, $value);
            if (sizeof($getInnKey) > 0) {
                $final_row_array[$date_key] = $value[$getInnKey]['amount'];
            } else {
                $final_row_array[$date_key] = "0";
            }
        }

        $sum_value = 0;
        $sum_inc = 1;
        $sum_dec = 1;

        if ($bold != null || $bold != "") {
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
        } else {
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
        }
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, trim($key));
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
        if ($row_border == 1) {
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
        }
        $col = 2;

        foreach ($final_row_array as $final_data_key => $final_data_value) {
            if ($final_data_value < 0) {
                $style = array('font' => array('size' => 10, 'bold' => true, 'color' => array('rgb' => 'A52A2A')));

                //apply the style on column A row 1 to Column B row 1
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->applyFromArray($style);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                $final_data_value_display = $this->format_number_exportXls($final_data_value, 0);
                $sum_value = $sum_value - abs($final_data_value);
                $sum_dec++;
            } else if (is_numeric($final_data_value)) {
                $final_data_value_display = $this->format_number_exportXls($final_data_value, 1);
                $sum_value = $sum_value + $final_data_value;
                $sum_inc++;
            } else {
                $final_data_value_display = $final_data_value;
            }
            if ($bold != null || $bold != "") {
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
            } else {
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(8);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
            }
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, trim($final_data_value_display));
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
            if ($row_border == 1) {
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                    ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
            }
            $col++;
        }
    }

    public function buildPDFforTrend($key, $value, $pdf, $header, $summary, $w, $title_border, $line_border, $reportDate, $left_indentation = null)
    {
        if ($summary) {
            $pdf->setCellPaddings($left = '5', $top = '', $right = '', $bottom = '');
        } else {
            $pdf->setCellPaddings($left = '0', $top = '', $right = '', $bottom = '');
        }

        if ($left_indentation != null) {
            $pdf->setCellPaddings($left = $left_indentation, $top = '', $right = '', $bottom = '');
        }

        if ($header) {
            $pdf->SetFont('', 'B');
        } else {
            $pdf->SetFont('');
        }

        if ($key == "Operating Profit") {
            $key = "EBITDA";
        }

        $final_row_array = array();
        foreach (array_reverse($reportDate) as $date_key => $date_value) {
            $month = date("n", strtotime($date_value));
            $year = date("y", strtotime($date_value));
            $getInnKey = $this->searchIdFromArray($month . '-' . $year, $value);
            if (sizeof($getInnKey) > 0) {
                $final_row_array[$date_key] = $value[$getInnKey]['amount'];
            } else {
                $final_row_array[$date_key] = "$0";
            }
        }

        $sum_value = 0;
        $sum_inc = 1;
        $sum_dec = 1;
        $j = 1;

        $pdf->SetTextColor('', '', '');
        $pdf->Cell($w[0], 7, trim($key), $header ? $title_border : $line_border, 0, 'L', 0);

        foreach ($final_row_array as $final_data_key => $final_data_value) {
            if ($final_data_value < 0) {
                $pdf->SetTextColor(191, 43, 48);
                $final_data_value_display = $this->format_number($final_data_value, 0);
                $sum_value = $sum_value - abs($final_data_value);
                $sum_dec++;
            } else if (is_numeric($final_data_value)) {
                $pdf->SetTextColor('', '', '');
                $final_data_value_display = $this->format_number($final_data_value, 1);
                $sum_value = $sum_value + $final_data_value;
                $sum_inc++;
            } else {
                $pdf->SetTextColor('', '', '');
                $final_data_value_display = $final_data_value;
            }
            $pdf->Cell($w[$j], 7, trim($final_data_value_display), $header ? $title_border : $line_border, 0, 'R', 0);
            $j++;
        }
        $pdf->Ln();
    }

    public function buildPDFforTrendQuarterlyxls($key, $value, $objPHPExcel, $row, $bold = null, $row_border = null)
    {

        if ($key == "Operating Profit") {
            $key = "EBITDA";
        }

        $final_row_array = array();

        $sum_value = 0;
        $sum_inc = 1;
        $sum_dec = 1;
        $col = 1;

        if ($bold != null || $bold != "") {
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
        } else {
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
        }
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, trim($key));
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
        if ($row_border == 1) {
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
        }
        $col = 2;
        foreach (array_reverse($value) as $final_data_key => $final_data_value) {
            if ($final_data_value < 0) {
                $style = array('font' => array('size' => 10, 'bold' => true, 'color' => array('rgb' => 'A52A2A')));

                //apply the style on column A row 1 to Column B row 1
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->applyFromArray($style);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                $final_data_value_display = $this->format_number_exportXls($final_data_value, 0);
                $sum_value = $sum_value - abs($final_data_value);
                $sum_dec++;
            } else if (is_numeric($final_data_value)) {
                $final_data_value_display = $this->format_number_exportXls($final_data_value, 1);
                $sum_value = $sum_value + $final_data_value;
                $sum_inc++;
            } else {
                $final_data_value_display = $final_data_value;
            }
            if ($bold != null || $bold != "") {
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
            } else {
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(8);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
            }
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, trim($final_data_value_display));
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
            if ($row_border == 1) {
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                    ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
            }
            $col++;
        }
    }

    public function buildPDFforTrendQuarterly($key, $value, $pdf, $header, $summary, $w, $title_border, $line_border, $reportDate, $left_indentation = null)
    {
        if ($summary) {
            $pdf->setCellPaddings($left = '5', $top = '', $right = '', $bottom = '');
        } else {
            $pdf->setCellPaddings($left = '0', $top = '', $right = '', $bottom = '');
        }

        if ($left_indentation != null) {
            $pdf->setCellPaddings($left = $left_indentation, $top = '', $right = '', $bottom = '');
        }

        if ($header) {
            $pdf->SetFont('', 'B');
        } else {
            $pdf->SetFont('');
        }

        if ($key == "Operating Profit") {
            $key = "EBITDA";
        }

        $final_row_array = array();

        $sum_value = 0;
        $sum_inc = 1;
        $sum_dec = 1;
        $j = 1;

        $pdf->Cell($w[0], 7, trim($key), $header ? $title_border : $line_border, 0, 'L', 0);

        foreach (array_reverse($value) as $final_data_key => $final_data_value) {
            if ($final_data_value < 0) {
                $pdf->SetTextColor(191, 43, 48);
                $final_data_value_display = $this->format_number($final_data_value, 0);
                $sum_value = $sum_value - abs($final_data_value);
                $sum_dec++;
            } else if (is_numeric($final_data_value)) {
                $pdf->SetTextColor('', '', '');
                $final_data_value_display = $this->format_number($final_data_value, 1);
                $sum_value = $sum_value + $final_data_value;
                $sum_inc++;
            } else {
                $pdf->SetTextColor('', '', '');
                $final_data_value_display = $final_data_value;
            }
            $pdf->Cell($w[$j], 7, trim($final_data_value_display), $header ? $title_border : $line_border, 0, 'R', 0);
            $j++;
        }
        $pdf->Ln();
    }

    public function getQuartelyMonthArray()
    {
        $report_end_date = date("Y-m-01", strtotime("-12 months"));
        $current_month_no = date('n');
        $quarters = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]];
        $key = $this->searchForId($current_month_no, $quarters);
        $startDate = date('Y-') . $quarters[$key][2] . '-01';
        $finalquarters = array();
        $i = 3;
        while ($report_end_date <= $startDate) {
            if ($i >= 0) {
                $year = date('y', strtotime($startDate));
                $month = date('M', strtotime($startDate));
                $finalquarters[$i] = $month . '-' . $year;
            }
            $startDate = date('Y-m-d', strtotime("-3 months", strtotime($startDate)));
            $i = $i - 1;
        }
        $finalquarters[3] = "QTD";
        return $finalquarters;
    }

    public function format_number($value, $type = null)
    {
        if ($type == 0) {
            $response = "($" . number_format(round(abs($value))) . ")";
        } else if ($type == 1) {
            $response = "$" . number_format(round(abs($value)));
        } else if ($type == 2) {
            $response = "(" . number_format(round(abs($value))) . "%)";
        }
        return $response;
    }

    public function format_number_exportXls($value, $type = null)
    {
        if ($type == 0) {
            $response = (int)round($value);
        } else if ($type == 1) {
            $response = (int)round(abs($value));
        } else if ($type == 2) {
            $response = (int)round(abs($value));
        }
        return $response;
    }

    public function getReportMonths($report_start_date)
    {
        $get_month = strtolower(date("M", strtotime($report_start_date)));
        $last_month = strtolower(date('M', strtotime('last month')));
        $current_month = strtolower(date('M'));
        $current_month_number = date('n');
        $last_month_number = date('n', strtotime('last month'));
        $report_start_month_number = date("n", strtotime($report_start_date));

        if ($report_start_month_number != $current_month_number) {
            $month_arr = array('jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec');
            $consoliate_month_arr = array();

            if ($report_start_month_number <= $current_month_number) {
                for ($i = $current_month_number; $i >= $report_start_month_number; $i--) {
                    $consoliate_month_arr[$i - 1] = $month_arr[$i - 1];
                }
            } else {
                for ($i = $last_month_number; $i >= 0; $i--) {
                    if ($current_month != $month_arr[$i]) {
                        $consoliate_month_arr[$i] = $month_arr[$i];
                    }
                }
                for ($i = 11; $i > ($report_start_month_number - 1); $i--) {
                    if ($current_month != $month_arr[$i]) {
                        $consoliate_month_arr[$i] = $month_arr[$i];
                    }
                }
            }
        }
        return $consoliate_month_arr;
    }

    public function getAccountCategoriesData($group_id = null, $client_master_id, $data)
    {
        try {
            $XeroOAuth = $data['XeroOAuth'];
            $XeroOAuth->config['access_token'] = utf8_encode($data['oauth_token']);
            $XeroOAuth->config['access_token_secret'] = utf8_encode($data['oauth_token_secret']);

            $rresponse = $this->makeRequest(null, 'getAccountCategoriesData', $XeroOAuth, 'GET', CHART_OF_ACCOUNT_CATEGORY_API, array('Where' => 'STATUS=="ACTIVE"'), "", "json",$client_master_id);
             $response = $XeroOAuth->response;
            if ($response['code'] == 302) {
                $response["error"] = true;
                $response["message"] = XERO_HAVING_PROBLEM;
                return $response;
            }
            if ($response['code'] == 401) {
                if (strpos($response['response'], 'token_expired') !== false || strpos($response['response'], 'token_rejected') !== false) {
                    $result = $this->getoAdminAuthKeys();
                    $resp = $this->AdminrequestToken($result);
                    if (isset($resp['authurl'])) {
                        $authurl = $resp['authurl'];
                    }
                }
            }
            if ($response['code'] == 200) {
                $accountcategoriesList = json_decode($XeroOAuth->response['response'], true);
                $profit_loss_array = array("REVENUE", "EXPENSE");
                $balance_sheet_array = array("ASSET", "LIABILITY", "EQUITY");
                foreach ($accountcategoriesList['Accounts'] as $accounts_record) {
                    $code = $accounts_record['Code'];
                    $name = $accounts_record['Name'];
                    $type = $accounts_record['Type'];
                    $class = $accounts_record['Class'];

                    $category_flag = 0;
                    if (in_array($class, $profit_loss_array)) {
                        $category_flag = 1;
                    } else if (in_array($class, $balance_sheet_array)) {
                        $category_flag = 2;
                    }

                    if ($category_flag == 1) {
                        if ($type != "") {
                            switch ($type) {
                                case "REVENUE":
                                    $account_type_rename = "Revenue";
                                    $order = 1;
                                    break;
                                case "DIRECTCOSTS":
                                    $account_type_rename = "Cost Of Sales";
                                    $order = 2;
                                    break;
                                case "OTHERINCOME":
                                    $account_type_rename = "Other Income";
                                    $order = 3;
                                    break;
                                case "EXPENSE":
                                    $account_type_rename = "Expenses";
                                    $order = 4;
                                    break;
                                case "DEPRECIATN":
                                    $account_type_rename = "Depreciation";
                                    $order = 5;
                                    break;
                            }
                        }
                    } else {
                        if ($type != "") {
                            switch ($type) {
                                case "BANK":
                                    $account_type_rename = "Bank";
                                    $order = 1;
                                    break;
                                case "CURRENT":
                                    $account_type_rename = "Current Assets";
                                    $order = 2;
                                    break;
                                case "FIXED":
                                    $account_type_rename = "Fixed Assets";
                                    $order = 3;
                                    break;
                                case "CURRLIAB":
                                    $account_type_rename = "Current Liabilities";
                                    $order = 4;
                                    break;
                                case "LIABILITY":
                                    $account_type_rename = "Liability";
                                    $order = 5;
                                    break;
                                case "TERMLIAB":
                                    $account_type_rename = "Non-current Liability";
                                    $order = 6;
                                    break;
                                case "EQUITY":
                                    $account_type_rename = "Equity";
                                    $order = 7;
                                    break;
                                case "PREPAYMENT":
                                    $account_type_rename = "Prepayment";
                                    $order = 8;
                                    break;
                                case "PAYGLIABILITY":
                                    $account_type_rename = "PAYG Liability";
                                    $order = 9;
                                    break;
                                case "INVENTORY":
                                    $account_type_rename = "Inventory Asset";
                                    $order = 10;
                                    break;
                                case "NONCURRENT":
                                    $account_type_rename = "Non-current Asset";
                                    $order = 11;
                                    break;
                                case "OVERHEADS":
                                    $account_type_rename = "Overhead";
                                    $order = 12;
                                    break;
                                case "SUPERANNUATIONEXPENSE":
                                    $account_type_rename = "Superannuation Expense";
                                    $order = 13;
                                    break;
                                case "SUPERANNUATIONLIABILITY":
                                    $account_type_rename = "Superannuation Liability";
                                    $order = 14;
                                    break;
                                case "WAGESEXPENSE":
                                    $account_type_rename = "Wages Expense";
                                    $order = 15;
                                    break;
                            }
                        }
                    }

                    if (!empty($code)) {
                        $this->insertAccountCategories($client_master_id, $code, $name, $type, $category_flag, $account_type_rename, $order);
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function insertAccountCategories($client_master_id, $account_code, $account_name, $type, $category_flag, $account_type_rename, $order)
    {
        try {
            $this->conn->autocommit(false);
            $account_category_record = $this->getAccountDataCheck($client_master_id, $account_code);
            if ($account_category_record["error"] == true) {
                $sql = "INSERT INTO  master_companies_account_categories  (client_master_id, account_code, account_name, account_type, account_category, account_type_alias, account_order) VALUES (?,?,?,?,?,?,?)";
                $e = 1;
            } else {
                $id = $account_category_record['field_key'];
                $sql = "UPDATE  master_companies_account_categories set account_name = ?, account_type = ?, account_category = ?, account_type_alias = ?, account_order = ? where client_master_id = ?  and id = ? ";
                $e = 2;
            }

            if ($stmt = $this->conn->prepare($sql)) {
                if ($e == 1) {
                    $stmt->bind_param("isssiss", $client_master_id, $account_code, $account_name, $type, $category_flag, $account_type_rename, $order);
                } else {
                    $stmt->bind_param("ssisiii", $account_name, $type, $category_flag, $account_type_rename, $order, $client_master_id, $account_category_record['field_key']);
                }
                $result = $stmt->execute();
                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function insertGroupAccountCategories($group_id, $client_master_id, $account_code, $account_name, $type, $category_flag, $account_type_rename, $account_order)
    {
        try {
            $this->conn->autocommit(false);
            $account_category_record["error"] = true;
            if ($account_category_record["error"] == true) {
                $sql = "INSERT INTO  group_companies_account_categories (group_id, client_master_id, account_code, account_name, account_type, account_category,  account_type_alias, account_order) VALUES (?,?,?,?,?,?,?,?)";
                $e = 1;
            }
            if ($stmt = $this->conn->prepare($sql)) {
                if ($e == 1) {
                    $stmt->bind_param("iisssiss", $group_id, $client_master_id, $account_code, $account_name, $type, $category_flag, $account_type_rename, $account_order);
                }
                $result = $stmt->execute();
                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function deleteGroupClientData($client_master_id, $group_id)
    {
        try {

            $response = array();
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            $childSQL = 'DELETE FROM group_companies_account_categories WHERE client_master_id = ? and group_id = ?';
            if ($childStmt = $this->conn->prepare($childSQL)) {
                $childStmt->bind_param('ii', $client_master_id, $group_id);
                $result = $childStmt->execute();
                $childStmt->close();
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                } else {
                    $response["error"] = true;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getEliMinateNameByID($client_master_id) {
        $sql = "SELECT client_master_id, group_id, account_name FROM group_companies_account_categories WHERE client_master_id = ? and is_eliminated = 1";

        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param("i", $client_master_id);
            $stmt->execute();
            $stmt->bind_result($client_master_id, $group_id, $account_name);
            $account_name1 = array();
            while ($result = $stmt->fetch()) {
                $account_name1[] = array('account_name' => $account_name, "client_master_id" => $client_master_id, "group_id" => $group_id);
            }
            $response["error"] = false;
            $response["name"] = $account_name1;
            return $response;
        } else {
            $response["error"] = true;
            $response["message"] = QUERY_EXCEPTION;
            return $response;
        }
    }

    public function insertGroupAccountCategories1($group_id, $client_master_id, $account_code, $account_name, $type, $category_flag, $account_type_rename, $account_order)
    {
        try {
            $this->conn->autocommit(false);
            $account_category_record = $this->getGroupAccountDataCheck($client_master_id, $group_id, $account_code);

            if ($account_category_record["error"] == true) {
                $sql = "INSERT INTO  group_companies_account_categories (group_id, client_master_id, account_code, account_name, account_type, account_category,  account_type_alias, account_order) VALUES (?,?,?,?,?,?,?,?)";
                $e = 1;
            } else {
                $sql = "UPDATE  group_companies_account_categories set account_name = ?, account_type = ?, account_category = ?,  account_type_alias = ?, account_order = ? where client_master_id = ? AND  group_id = ? and id = ?";
                $e = 2;
            }
            if ($stmt = $this->conn->prepare($sql)) {
                if ($e == 1) {
                    $stmt->bind_param("iisssiss", $group_id, $client_master_id, $account_code, $account_name, $type, $category_flag, $account_type_rename, $account_order);
                } else {
                    $stmt->bind_param("ssisssii", $account_name, $type, $category_flag, $account_type_rename, $account_order, $client_master_id, $group_id, $account_category_record['field_key']);
                }
                $result = $stmt->execute();
                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function get1oAuthAdminKeys()
    {
        try {
            $publicKeys = array();
            $publicKeys = $this->getXeroKeys();

            if (sizeof($publicKeys) > 0) {
                if ($publicKeys['error'] == false) {
                    $aSignatures = array();
                    $aSignatures = $this->configureSignature($publicKeys);
                    $XeroOAuth = new XeroOAuth(array_merge(array(
                        'application_type' => XRO_APP_TYPE,
                        'oauth_callback' => utf8_encode(OAUTH_CALLBACK),
                        'user_agent' => utf8_encode(XERO_ACCOUNT),
                    ), $aSignatures));

                    $initialCheck = $XeroOAuth->diagnostics();
                    $checkErrors = count($initialCheck);
                    if ($checkErrors > 0) {
                        // you could handle any config errors here, or keep on truckin if you like to live dangerously
                        foreach ($initialCheck as $check) {
                            $response['error'] = true;
                            $response['message'] = 'Error: ' . $check . PHP_EOL;
                        }
                    } else {
                        $here = XeroOAuth::php_self();
                        $oAuthValues = array();
                        $response['XeroOAuth'] = $XeroOAuth;
                    }

                } else {
                    $response = $publicKeys;
                }
            } else {
                $response['error'] = true;
                $response['message'] = ISSUE_ON_KEYS;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getoAuthAdminKeysold()
    {
        try {
            $publicKeys = array();
            $publicKeys = $this->getXeroKeys();

            if (sizeof($publicKeys) > 0) {
                if ($publicKeys['error'] == false) {
                    $aSignatures = array();
                    $aSignatures = $this->configureSignature($publicKeys);
                    $XeroOAuth = new XeroOAuth(array_merge(array(
                        'application_type' => XRO_APP_TYPE,
                        'oauth_callback' => utf8_encode(OAUTH_ADMIN_CALLBACK),
                        'user_agent' => utf8_encode(XERO_ACCOUNT),
                    ), $aSignatures));

                    $data_auth = $XeroOAuth;

                    $initialCheck = $XeroOAuth->diagnostics();
                    $checkErrors = count($initialCheck);
                    if ($checkErrors > 0) {
                        // you could handle any config errors here, or keep on truckin if you like to live dangerously
                        foreach ($initialCheck as $check) {
                            $response['error'] = true;
                            $response['message'] = 'Error: ' . $check . PHP_EOL;
                        }
                    } else {
                        $here = XeroOAuth::php_self();
                        session_start();
                        $oauthSession = $this->retrieveSession();
                        if (isset($_REQUEST['oauth_verifier'])) {
                            $XeroOAuth->config['access_token'] = $_SESSION['oauth']['oauth_token'];
                            $XeroOAuth->config['access_token_secret'] = $_SESSION['oauth']['oauth_token_secret'];

                            $code = $this->makeRequest(null, null,'getoAuthAdminKeysold',$XeroOAuth,'GET', $XeroOAuth->url('AccessToken', ''), array(
                                'oauth_verifier' => $_REQUEST['oauth_verifier'],
                                'oauth_token' => $_REQUEST['oauth_token'],
                            ));

                            if ($XeroOAuth->response['code'] == 200) {
                                $response = $XeroOAuth->extract_params($XeroOAuth->response['response']);
                                $session = $this->persistSession($response);

                                unset($_SESSION['oauth']);
                                header("Location: {$here}");
                            } else {
                                $response['XeroOAuth'] = $XeroOAuth;
                            }
                            // start the OAuth dance
                        } elseif (isset($_REQUEST['authenticate']) || isset($_REQUEST['authorize'])) {
                            $params = array(
                                'oauth_callback' => OAUTH_ADMIN_CALLBACK,
                            );
                            $response = $this->makeRequest(null, 'getoAuthAdminKeysold', $XeroOAuth, 'GET', $XeroOAuth->url('RequestToken', ''), $params);

                            if ($XeroOAuth->response['code'] == 200) {
                                $scope = "";
                                $_SESSION['oauth'] = $XeroOAuth->extract_params($XeroOAuth->response['response']);
                                $authurl = $XeroOAuth->url("Authorize", '') . "?oauth_token={$_SESSION['oauth']['oauth_token']}&scope=" . $scope;
                                header('Location:' . $authurl);die();
                            } else {
                                $response['XeroOAuth'] = $XeroOAuth;
                            }

                        }
                    }
                } else {
                    $response['final'] = $publicKeys;
                }
            } else {
                $response['error'] = true;
                $response['message'] = ISSUE_ON_KEYS;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function outputError($XeroOAuth)
    {
        echo 'Error: ' . $XeroOAuth . PHP_EOL;
        pr($XeroOAuth);
    }

    public function retrieveSession()
    {
        if (isset($_SESSION['access_token'])) {
            $response['oauth_token'] = $_SESSION['access_token'];
            $response['oauth_token_secret'] = $_SESSION['oauth_token_secret'];
            $response['oauth_session_handle'] = $_SESSION['session_handle'];
            return $response;
        } else {
            return false;
        }
    }

    public function persistSession($response)
    {
        if (isset($response)) {
            $_SESSION['access_token'] = $response['oauth_token'];
            $_SESSION['oauth_token_secret'] = $response['oauth_token_secret'];
            if (isset($response['oauth_session_handle'])) {
                $_SESSION['session_handle'] = $response['oauth_session_handle'];
            }

        } else {
            return false;
        }
    }

    public function getoAdminAuthKeys()
    {
        try {
            $publicKeys = array();
            $publicKeys = $this->getXeroKeys();

            if (sizeof($publicKeys) > 0) {
                if ($publicKeys['error'] == false) {
                    $aSignatures = array();
                    $aSignatures = $this->configureSignature($publicKeys);
                    $XeroOAuth = new XeroOAuth(array_merge(array(
                        'application_type' => XRO_APP_TYPE,
                        'oauth_callback' => utf8_encode(OAUTH_ADMIN_CALLBACK),
                        'user_agent' => utf8_encode(XERO_ACCOUNT),
                    ), $aSignatures));

                    $initialCheck = $XeroOAuth->diagnostics();
                    $checkErrors = count($initialCheck);
                    if ($checkErrors > 0) {
                        // you could handle any config errors here, or keep on truckin if you like to live dangerously
                        foreach ($initialCheck as $check) {
                            $response['error'] = true;
                            $response['message'] = 'Error: ' . $check . PHP_EOL;
                        }
                    } else {
                        $here = XeroOAuth::php_self();
                        $oAuthValues = array();
                        $response['XeroOAuth'] = $XeroOAuth;
                    }

                } else {
                    $response = $publicKeys;
                }
            } else {
                $response['error'] = true;
                $response['message'] = ISSUE_ON_KEYS;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function AdminrequestToken($data)
    {
        try {
            $response = array();

            if (isset($data['XeroOAuth'])) {
                $here = XeroOAuth::php_self();
                $XeroOAuth = $data['XeroOAuth'];
                $params = array(
                    'oauth_callback' => OAUTH_ADMIN_CALLBACK,
                );

                $response = $this->makeRequest(null, 'AdminrequestToken', $XeroOAuth, 'GET', $XeroOAuth->url('RequestToken', ''), $params);
                if ($XeroOAuth->response['code'] == 302) {
                    $response["error"] = true;
                    $response["message"] = XERO_HAVING_PROBLEM;
                    return $response;
                }
                if ($XeroOAuth->response['code'] == 200) {
                    $scope = "";
                    $xResponse = array();
                    $xResponse = $XeroOAuth->extract_params($XeroOAuth->response['response']);
                    $oauth_token = $xResponse['oauth_token'];
                    $oauth_token_secret = $xResponse['oauth_token_secret'];
                    $resp = array();
                    $response = $this->AdminsaveAuthKeys($oauth_token, $oauth_token_secret, $oauth_token, '');
                    if ($response['error'] == false) {
                        $authurl = $XeroOAuth->url("Authorize", '') . "?oauth_token={$oauth_token}&scope=" . $scope;
                        $response['authurl'] = $authurl;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = $XeroOAuth;
                }
            } else {
                $response = $data;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function AdminsaveAuthKeys($oauth_token, $oauth_token_secret, $req_oauth_token, $org)
    {
        try {
            $this->conn->autocommit(false);
            $date = date("Y-m-d H:i:s");
            $admin_id = 1;
            $response = array();
            //return $oauth_token."/".$oauth_token_secret."/".$admin_id;
            $sql = "UPDATE admin_oauth_token set oauth_token = ?, oauth_token_secret = ?, req_oauth_token = ?, org = ? where admin_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ssssi", $oauth_token, $oauth_token_secret, $req_oauth_token, $org, $admin_id);
                $result = $stmt->execute();
                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    $response["message"] = KEY_GENERATE_SUCCESS;

                } else {
                    $response["error"] = true;
                    $response["message"] = KEY_GENERATE_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getAdminAccessToken($data, $oauthVerifier, $oauth_token, $org)
    {
        try {
            $here = XeroOAuth::php_self();
            $XeroOAuth = $data['XeroOAuth'];

            $XeroOAuth->config['access_token'] = utf8_encode($data['req_oauth_token']);
            $XeroOAuth->config['access_token_secret'] = utf8_encode($data['oauth_token_secret']);
            $code = $this->makeRequest(null,'getAdminAccessToken',$XeroOAuth,'GET', $XeroOAuth->url('AccessToken', ''), array(
                'oauth_verifier' => utf8_encode($oauthVerifier),
                'oauth_token' => utf8_encode($oauth_token),
            ));

            if ($XeroOAuth->response['code'] == 302) {
                $response["error"] = true;
                $response["message"] = XERO_HAVING_PROBLEM;
                return $response;
            }
            if ($XeroOAuth->response['code'] == 200) {

                $response = $XeroOAuth->extract_params($XeroOAuth->response['response']);
                $oauth_new_token = $response['oauth_token'];
                $oauth_token_secret = $response['oauth_token_secret'];
                $resp = array();
                $response = $this->saveAdminAuthKeys($oauth_new_token, $oauth_token_secret, '', $org);
            } else {
                $response['error'] = true;
                $response['message'] = $XeroOAuth;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function ReportGenerateDateforPDF($category1, $category2, $client_master_id)
    {
        $response = array();
        try {
            $curDate = date('Y-m-d H:i:s');
            $sql = "SELECT client_master_id FROM pdf_report_generated_date WHERE category1 = ? and category2 = ? and client_master_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ssi", $category1, $category2, $client_master_id);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $sql = "UPDATE pdf_report_generated_date set report_generate_date = ? WHERE category1 = ? and category2 = ?  and client_master_id = ?";
                    if ($stmt = $this->conn->prepare($sql)) {
                        $stmt->bind_param("sssi", $curDate, $category1, $category2, $client_master_id);
                        $result = $stmt->execute();
                        $this->conn->commit();
                        $stmt->close();
                        $response['error'] = false;
                        $response["message"] = REPORT_GEN_UPDATE_SUCCESS;
                    } else {
                        $response["error"] = true;
                        $response["message"] = QUERY_EXCEPTION;
                    }
                } else {
                    $sql1 = "INSERT INTO pdf_report_generated_date (client_master_id, report_generate_date, category1, category2)
                                values($client_master_id, '$curDate','$category1', '$category2')";
                    if ($stmt1 = $this->conn->prepare($sql1)) {
                        $result = $stmt1->execute();
                        $stmt1->close();
                        if ($result) {
                            $this->conn->commit();
                            $response['error'] = false;
                            $response["message"] = REPORT_GEN_ADD_SUCCESS;
                        } else {
                            $response['error'] = true;
                            $response["message"] = "report added failed";
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = QUERY_EXCEPTION;
                    }
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getCategoryNameById($category1, $category2, $client_master_id)
    {
        try {
            $response = array();

            if ($category1 != "0") {
                $sql = "SELECT cs.sub_category_name FROM category_subcategory as cs
                        LEFT JOIN client_category_master as ccm on ccm.client_category_id = cs.client_category_id
                        where ccm.client_master_id = ? AND cs.sub_category_id = ?
                        GROUP BY cs.sub_category_name";
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param("is", $client_master_id, $category1);
                    $stmt->execute();
                    $stmt->store_result();
                    if ($stmt->num_rows > 0) {
                        $stmt->bind_result($sub_category_name);
                        while ($result = $stmt->fetch()) {
                            $response['sub_category_name1'] = $sub_category_name;
                        }

                        $response["error"] = false;
                    } else {
                        $response["record"] = 'no';
                    }
                } else {

                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            }
            if ($category2 != "0") {
                $sql1 = "SELECT cs.sub_category_name FROM category_subcategory as cs
                            LEFT JOIN client_category_master as ccm on ccm.client_category_id = cs.client_category_id
                            where ccm.client_master_id = ? AND cs.sub_category_id= ?
                            GROUP BY cs.sub_category_name";
                if ($stmt = $this->conn->prepare($sql1)) {
                    $stmt->bind_param("is", $client_master_id, $category2);
                    $stmt->execute();
                    $stmt->store_result();
                    if ($stmt->num_rows > 0) {
                        $stmt->bind_result($sub_category_name);
                        while ($result = $stmt->fetch()) {
                            $response['sub_category_name2'] = $sub_category_name;
                        }
                        $response["error"] = true;
                    }
                } else {

                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getPdfFileExists($client_master_id, $pdf_type, $category1, $category2 = null)
    {
        try {
            $response = array();
            $response["record"] = 'no';
            $fileNameType = '';
            if ($pdf_type == 'flash_report') {
                $sql = "SELECT category1, category2 FROM pdf_report_generated_date
                                    WHERE  client_master_id = ? AND flash_report_pdf_date >= 0 AND
                                    category1 = ? AND category2 = ?";
                $fileNameType = '_flashreport';

            } elseif ($pdf_type == 'pl_report') {
                $sql = "SELECT category1, category2 FROM pdf_report_generated_date
                                    WHERE client_master_id = ? AND financial_report_pdf_date >= report_generate_date AND
                                    category1 = ? AND category2 = ?";
                $fileNameType = '_plreport';
                if ($category2 > 0) {
                } else {
                    $category2 = "";
                }
            }
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("iss", $client_master_id, $category1, $category2);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($client_master_id, $category1, $category2);
                    $sql1 = "SELECT company_name FROM client_master_detail where client_master_id = ?";
                    if ($stmt1 = $this->conn->prepare($sql1)) {
                        $stmt1->bind_param("i", $client_master_id);
                        $stmt1->execute();
                        $stmt1->store_result();
                        if ($stmt1->num_rows > 0) {
                            $stmt1->bind_result($company_name);
                            while ($result = $stmt1->fetch()) {

                                $company_name = str_replace(" ", "_", $company_name);
                            }

                            if ($pdf_type == 'flash_report') {
                                $pdf_name = $company_name . $fileNameType . '.pdf';
                            } else {
                                if ($category2 != '' && $category2 != null) {
                                    $pdf_name = $company_name . $fileNameType . $category1 . '_' . $category2 . '.pdf';
                                } else {
                                    $pdf_name = $company_name . $fileNameType . $category1 . '.pdf';
                                }
                            }
                        }
                    }
                    $path = "api/include/report/" . $client_master_id . '/' . $pdf_name;

                    if (file_exists($path)) {
                        $response["file_name"] = $path;
                        $response["record"] = 'yes';
                    } else {
                        $response["record"] = 'no';
                    }

                }
                $response["error"] = true;
            } else {

                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function insertUpdatePdfGenerateDate($client_master_id, $category1, $category2, $reportType)
    {
        try {
            $response = array();
            $curdate = date('Y-m-d H:i:s');
            if ($reportType == 'flash_report') {
                $sql = "UPDATE  pdf_report_generated_date set flash_report_pdf_date = ? WHERE  client_master_id = ? AND category1 = ? AND category2 = ?";
            } elseif ($reportType == 'pl_report') {
                $sql = "UPDATE  pdf_report_generated_date set financial_report_pdf_date = ? WHERE client_master_id = ? AND category1 = ? AND category2 = ?";
            }

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("siss", $curdate, $client_master_id, $category1, $category2);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                $response['error'] = false;
                $response["message"] = REPORT_GEN_UPDATE_SUCCESS;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function restFlashSummary()
    {
        $sql1 = "UPDATE report_pdf_to_client set last_ran_status = 0";
        if ($stmt1 = $this->conn->prepare($sql1)) {
            $result = $stmt1->execute();
            $stmt1->close();
        }
    }

    public function getMappedTrackingCategories($searchItems)
    {
        try {
            $response = array();

            $sql = 'SELECT tcm.track_client_sub_category_id, tcm.client_id, tcm.category_sub_id, ccm.client_category_id, ccm.category_id,
                            category_name, ccm.is_active, cs.sub_category_id, sub_category_name
                    FROM track_client_sub_category tcm
                    JOIN category_subcategory cs ON cs.category_sub_id = tcm.category_sub_id
                    JOIN client_category_master ccm ON ccm.client_category_id = cs.client_category_id ';

            if (sizeof($searchItems) > 0) {
                $sql .= " WHERE ";
            }
            foreach ($searchItems as $key => $value) {
                switch ($key) {
                    case 'client_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "tcm.client_id = ? ";
                        break;
                    case 'client_master_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = 'tcm.client_master_id = ? ';
                        break;
                    case 'is_active':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "ccm.is_active = ? ";
                        break;
                }
            }
            $sql .= implode(' AND ', $query);
            $sql .= "  ORDER BY sub_category_name ASC ";

            $param_type = '';
            $n = count($a_param_type);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $a_param_type[$i];
            }
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                $a_params[] = &$a_bind_params[$i];
            }

            $response["trackingCategories"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                call_user_func_array(array($stmt, 'bind_param'), $a_params);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $tmp = array();
                    $is_all_exist = false;
                    $all = array();
                    $i = 0;
                    $stmt->bind_result($track_client_sub_category_id, $client_id, $category_sub_id, $client_category_id, $category_id, $category_name, $is_active, $sub_category_id, $sub_category_name);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["track_client_sub_category_id"] = $track_client_sub_category_id;
                        $tmp["client_id"] = $client_id;
                        $tmp["category_sub_id"] = $category_sub_id;
                        $tmp["client_category_id"] = $client_category_id;
                        $tmp["category_id"] = $category_id;
                        $tmp["category_name"] = $category_name;
                        $tmp["is_active"] = $is_active;
                        $tmp["sub_category_id"] = $sub_category_id;
                        $tmp["sub_category_name"] = $sub_category_name;

                        if ($searchItems['groupby'] == 1) {
                            if (strtolower(trim($sub_category_name)) == strtolower(trim('All'))) {
                                if (sizeof($response["trackingCategories"][$category_name]) <= 1) {
                                    $response["trackingCategories"][$category_name][0] = $tmp;
                                } else {
                                    array_unshift($response["trackingCategories"][$category_name], $tmp);
                                }
                            } else {
                                $response["trackingCategories"][$category_name][] = $tmp;
                            }
                        } else {
                            if (strtolower(trim($sub_category_name)) == strtolower(trim('All'))) {
                                if (sizeof($response["trackingCategories"]) <= 1) {
                                    $response["trackingCategories"][0] = $tmp;
                                } else {
                                    $response["trackingCategories"][] = $tmp;
                                }
                            } else {
                                $response["trackingCategories"][] = $tmp;
                            }
                        }
                    }

                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getPLReportValuesxls($path, $position, $report_start_date, $report_type, $reportDate, $objPHPExcel, $sorting = null)
    {
        $report_obj = json_decode(file_get_contents($path), true);
        $last_updated_on = date("d M Y", strtotime($report_obj['last_updated_time']));
        $curMonth = date("m", time());
        $curQuarter = ceil($curMonth / 3);
        $quarter_array = array();
        $current_date = date("Y-m-d");
        $added_month = $report_start_date;
        for ($i = 1; $i <= $curQuarter; $i++) {
            $add_month_count = $i * 3;
            $add_up_month = date("Y-m-d", strtotime("+" . $add_month_count . " month", strtotime($added_month)));
            $final_addon_date = date('Y-m-d', strtotime('-1 day', strtotime($add_up_month)));
            if ($final_addon_date > $current_date) {
                $quarter_array[$i] = date("M y", strtotime($current_date));
            } else {
                $quarter_array[$i] = date("M y", strtotime($final_addon_date));
            }
        }

        $balance_and_stmt_report_date_array = array();
        $pnl_report_date_array = array();
        $month_data_value = $report_start_date;
        for ($i = 1; $i <= 12; $i++) {
            $month_data_count = $i * 1;
            $month_data_val = date("Y-m-d", strtotime("+" . $month_data_count . " month", strtotime($month_data_value)));
            $final_addon_month = date('Y-m-d', strtotime('-1 day', strtotime($month_data_val)));
            $date_num_value = date("m", strtotime($final_addon_month));
            if ($final_addon_month > $current_date) {
                $pnl_report_date_array[] = date("M y", strtotime($current_date));
                break;
            } else {
                $balance_and_stmt_report_date_array[] = date("d M y", strtotime($final_addon_month));
                $pnl_report_date_array[] = date("M y", strtotime($final_addon_month));
            }
        }
        if ($position == 2) {
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', "As at  $last_updated_on");
            $objPHPExcel->getActiveSheet()->mergeCells('A3:E3');
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $consolidate_pnl_array = $report_obj[$position][$report_type][$report_type];
            $consolidate_final_array = array();

            foreach ($consolidate_pnl_array as $key => $consolidate_inner_value) {
                $header_flag = 0;
                foreach ($consolidate_inner_value['summaryRow'] as $inner_key => $value) {
                    if (count($consolidate_inner_value['summaryRow'][$inner_key]) > 4) {
                        foreach ($value as $inner_key1 => $value1) {
                            $consolidate_final_array[$key]['header'][] = $value1["Value"];
                            $header_flag = 1;
                        }
                    }
                }

                $temp_array = array();
                foreach ($consolidate_inner_value as $inner_key => $consolidate_final_value) {
                    if ($inner_key != "summaryRow" && $inner_key != "Employee Compensation & Related Expenses") {
                        foreach ($consolidate_final_value as $last_key => $value) {
                            if (is_numeric($last_key)) {
                                if ($header_flag == 1) {
                                    $consolidate_final_array[$key]['rows'][$inner_key][] = $value['Value'];
                                } else {
                                    $temp_array[] = $value['Value'];
                                }
                            } else {
                                $temp_array[$last_key] = $value;
                            }
                        }
                        if ($header_flag == 0) {
                            $consolidate_final_array[$key]['header'] = $temp_array;
                        }
                    } else if ($inner_key == "Employee Compensation & Related Expenses") {
                        $emp_rows_arr = array();
                        $emp_comp = $consolidate_final_value;
                        foreach ($emp_comp as $emp_comp_key => $emp_comp_value) {
                            if (is_numeric($emp_comp_key)) {
                                foreach ($emp_comp_value as $last_key => $value) {
                                    if (is_numeric($last_key)) {
                                        if ($header_flag == 1) {
                                            $emp_rows_arr[$emp_comp_key][] = $value['Value'];
                                        } else {
                                            $temp_array[] = $value['Value'];
                                        }
                                    } else {
                                        $temp_array[$last_key] = $value;
                                    }
                                }
                            } else if ($emp_comp_key == "summaryRow") {
                                foreach ($emp_comp_value[0] as $last_key => $value) {
                                    if (is_numeric($last_key)) {
                                        if ($header_flag == 1) {
                                            $consolidate_final_array[$key]['rows'][$inner_key]['header'][] = $value['Value'];
                                        } else {
                                            $temp_array[] = $value['Value'];
                                        }
                                    } else {
                                        $temp_array[$last_key] = $value;
                                    }
                                }
                            }
                        }
                        $consolidate_final_array[$key]['rows'][$inner_key]['rows'] = $emp_rows_arr;
                    }
                }
            }

            if (count($consolidate_final_array) > 0) {
                $objPHPExcel->getActiveSheet()->getStyle("B7:E7")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("B7:E7")->getFont()->setSize(11);
                $coulmnArray = ['Actual', 'Budget', 'Variance', 'Variance %'];
                $col = 2;
                foreach ($coulmnArray as $row_value) {
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, 5)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, 5)->getStyle()->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, 5)->getStyle()->getFont()->setSize(9);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 5, $row_value);
                    $col++;
                }

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 8, "");
                $temp_array = array();
                $row = 6;
                foreach ($consolidate_final_array as $main_key => $main_value) {
                    $total_value = array();
                    $emp_comp_total_value = array();

                    foreach ($main_value as $second_level_key => $second_level_value) {
                        $no_expand = 0;
                        if ($second_level_key == "header") {
                            if (array_key_exists('no_expand', $second_level_value)) {
                                $no_expand = 1;
                                unset($second_level_value['no_expand']);
                            }

                            if ($no_expand == 1) {
                                $bold_data = 1;
                                $this->buildPDFforConsolidatedxls($second_level_value, $objPHPExcel, $row, $bold_data, 1);
                                $row++;
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                                $row++;
                            } else {
                                $col = 1;
                                foreach ($second_level_value as $key => $row_value) {
                                    if (!is_array($row_value)) {
                                        if ($key == 0) {
                                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                                            $replace_value = str_replace("Total ", "", $row_value);
                                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $replace_value);
                                            array_push($total_value, $row_value);
                                        } else {
                                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, '');
                                            array_push($total_value, $row_value);
                                        }
                                    }
                                    $col++;
                                }
                                $row++;
                            }
                        } else if ($second_level_key == "rows") {

                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                if (!is_numeric($third_level_key) && $third_level_key == "Employee Compensation & Related Expenses") {
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                                    $row++;
                                    foreach ($third_level_value as $fourth_level_key => $forth_level_value) {
                                        if ($fourth_level_key == "header") {
                                            $col = 1;
                                            foreach ($forth_level_value as $key => $row_value) {
                                                if (!is_array($row_value)) {
                                                    if ($key == 0) {
                                                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                                                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat();
                                                        $replace_value = str_replace("Total ", "", $row_value);
                                                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(8);
                                                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $replace_value);
                                                        array_push($emp_comp_total_value, $row_value);
                                                    } else {
                                                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(8);
                                                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat();
                                                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, '');
                                                        array_push($emp_comp_total_value, $row_value);
                                                    }
                                                }
                                                $col++;
                                            }
                                            $row++;
                                        } else if ($fourth_level_key == "rows") {
                                            foreach ($forth_level_value as $fifth_level_key => $fifth_level_value) {
                                                $this->buildPDFforConsolidatedxls($fifth_level_value, $objPHPExcel, $row);
                                                $row++;
                                            }
                                        }
                                    }
                                } else {
                                    $this->buildPDFforConsolidatedxls($third_level_value, $objPHPExcel, $row);
                                    $row++;
                                }
                            }
                        }
                    }

                    if (count($emp_comp_total_value) > 0) {
                        $bold_data = 1;
                        $this->buildPDFforConsolidatedxls($emp_comp_total_value, $objPHPExcel, $row, $bold_data, 1);
                        $row++;
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                        $row++;
                    }
                    if (count($total_value) > 0) {
                        $bold_data = 1;
                        $this->buildPDFforConsolidatedxls($total_value, $objPHPExcel, $row, $bold_data, 1);
                        $row++;
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                        $row++;
                    }
                }
            }
        } else if ($position == 3) {
            $objPHPExcel->getActiveSheet()->getStyle("A3:G3")->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', "As at  $last_updated_on");
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 4, "");
            $objPHPExcel->getActiveSheet()->mergeCells('A3:M3');
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $trend_pnl_array = $report_obj[$position]["trendPNL"];
            $trend_pl_month_final_array = array();
            foreach ($trend_pnl_array as $key => $trend_month_inner_value) {
                if ($key != "trendPNL_Quartely") {
                    if (is_array($trend_month_inner_value['summaryRow'])) {
                        foreach ($trend_month_inner_value['summaryRow'] as $inner_key => $value) {
                            $trend_pl_month_final_array[$key]['header'][$inner_key] = $value;
                        }
                    } else {
                        $trend_pl_month_final_array[$key]['header'][$key] = $trend_month_inner_value;
                    }

                    if ($key == "Gross Profit" || $key == "Operating Profit" || $key == "Net Profit") {
                    } else {
                        foreach ($trend_month_inner_value as $inner_key => $trend_month_final_value) {
                            if ($inner_key != 'summaryRow') {
                                $inner_check_array = $trend_month_inner_value[$inner_key]['summaryRow'];
                                if (is_array($inner_check_array)) {
                                    $trend_pl_month_final_array[$inner_key]['header'] = $inner_check_array;
                                    foreach ($trend_month_final_value as $last_key => $value) {
                                        if ($last_key != 'summaryRow') {
                                            $key_minus_flag = 0;
                                            if (!array_key_exists("0", $value)) {
                                                $key_minus_flag = 1;
                                            }
                                            $new_value_array = array();
                                            foreach ($value as $l_key => $new_value) {
                                                if ($key_minus_flag == 1) {
                                                    $l_key = $l_key - 1;
                                                }
                                                $new_value_array[$l_key] = $new_value;
                                            }
                                            $trend_pl_month_final_array[$inner_key]['rows'][$last_key] = $new_value_array;
                                        }
                                    }
                                } else {
                                    $key_minus_flag = 0;
                                    if (!array_key_exists("0", $trend_month_final_value)) {
                                        $key_minus_flag = 1;
                                    }
                                    foreach ($trend_month_final_value as $last_key => $value) {
                                        if ($key_minus_flag == 1) {
                                        }
                                        $trend_pl_month_final_array[$key]['rows'][$inner_key][$last_key] = $value;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            unset($trend_pl_month_final_array['Operating Expenses']);

            if (count($trend_pl_month_final_array) > 0) {
                $row = 5;
                $header = array_reverse($reportDate);
                $num_headers = count($header);
                $objPHPExcel->getActiveSheet()->getStyle("B5:M5")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("B5:M5")->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, "");
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, "Current Month");
                for ($col = 3; $col < $num_headers + 2; ++$col) {
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, substr($header[$col - 2], 3));
                }
                $row++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "");
                $row++;
                $total_operating_record_value = array();
                foreach ($trend_pl_month_final_array as $main_key => $main_value) {
                    $total_record_value = array();
                    $no_expand = 0;
                    $build_heading = 1;
                    if ($main_key == "Gross Profit" || $main_key == "Operating Profit" || $main_key == "Net Profit") {
                        $build_heading = 0;
                    } else if ($main_key == "Employee Compensation & Related Expenses") {
                        $build_heading = 1;
                    }

                    if ($build_heading == 1) {
                        $col = 1;
                        if (sizeof($main_value) > 1) {
                            for ($col = 1; $col <= $num_headers; ++$col) {
                                if ($col == 1) {
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");

                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $main_key);
                                } else {
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                                }

                            }$row++;
                        } else {
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                        }
                    }

                    foreach ($main_value as $second_level_key => $second_level_value) {
                        if ($second_level_key == "header") {
                            $col = 1;
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                if ($main_key == "Gross Profit" || $main_key == "Operating Profit" || $main_key == "Net Profit") {
                                    unset($third_level_value['no_expand']);
                                    $no_expand = 1;
                                    $bold = 1;
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                        ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                                    $this->buildPDFforTrendxls($third_level_key, $third_level_value, $objPHPExcel, $row, $reportDate, $bold, true, 1);
                                    $row++;
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                                    $row++;
                                } else {
                                    if ($third_level_key == "Total Operating Expenses") {
                                        if (isset($trend_pl_month_final_array["Employee Compensation & Related Expenses"])) {
                                            $total_employee_key = $third_level_key;
                                            foreach ($third_level_value as $key => $row_value) {
                                                $total_operating_record_value[] = $row_value;
                                            }
                                        } else {
                                            $total_record_key = $third_level_key;
                                            foreach ($third_level_value as $key => $row_value) {
                                                $total_record_value[] = $row_value;
                                            }
                                        }
                                    } else {
                                        $total_record_key = $third_level_key;
                                        foreach ($third_level_value as $key => $row_value) {
                                            $total_record_value[] = $row_value;
                                        }
                                    }
                                }
                            }
                            $col++;
                        } else if ($second_level_key == "rows") {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                if ($main_key == "Employee Compensation & Related Expenses") {
                                    $this->buildPDFforTrendxls($third_level_key, $third_level_value, $objPHPExcel, $row, $reportDate);
                                    $row++;
                                } else {
                                    $this->buildPDFforTrendxls($third_level_key, $third_level_value, $objPHPExcel, $row, $reportDate);
                                    $row++;
                                }
                            }
                        }
                    }

                    if (count($total_record_value) > 0) {
                        if ($main_key == "Employee Compensation & Related Expenses") {
                            $bold = 1;
                            $this->buildPDFforTrendxls($total_record_key, $total_record_value, $objPHPExcel, $row, $reportDate, $bold, true, 1);
                            $row++;
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                            $this->buildPDFforTrendxls($total_employee_key, $total_operating_record_value, $objPHPExcel, $row, $reportDate, $bold, true, 1);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                            $row++;
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                            $row++;
                        } else {
                            $bold = 1;
                            $this->buildPDFforTrendxls($total_record_key, $total_record_value, $objPHPExcel, $row, $reportDate, $bold, true, 1);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                            $row++;
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                            $row++;
                        }
                    }
                }
            }
        } elseif ($position == 4) {
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', "As at  $last_updated_on");
            $objPHPExcel->getActiveSheet()->mergeCells('A3:E3');
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $trend_pl_quarter_final_array = array();
            $trend_pnl_quartely_array = $report_obj[3]["trendPNL"]['trendPNL_Quartely'];

            foreach ($trend_pnl_quartely_array as $key => $trend_quarter_inner_value) {
                if ($key != "trendPNL_Quartely") {
                    if (is_array($trend_quarter_inner_value['summaryRow'])) {
                        foreach ($trend_quarter_inner_value['summaryRow'] as $inner_key => $value) {
                            $trend_pl_quarter_final_array[$key]['header'][$inner_key] = $value;
                        }
                    } else {
                        $trend_pl_quarter_final_array[$key]['header'][$key] = $trend_quarter_inner_value;
                    }

                    if ($key == "Gross Profit" || $key == "Operating Profit" || $key == "Net Profit") {
                    } else {
                        foreach ($trend_quarter_inner_value as $inner_key => $trend_quarter_final_value) {
                            if ($inner_key != 'summaryRow') {
                                $inner_check_array = $trend_quarter_inner_value[$inner_key]['summaryRow'];
                                if (is_array($inner_check_array)) {
                                    $trend_pl_quarter_final_array[$inner_key]['header'] = $inner_check_array;
                                    foreach ($trend_quarter_final_value as $last_key => $value) {
                                        if ($last_key != 'summaryRow') {
                                            $key_minus_flag = 0;
                                            if (!array_key_exists("0", $value)) {
                                                $key_minus_flag = 1;
                                            }
                                            $new_value_array = array();
                                            foreach ($value as $l_key => $new_value) {
                                                if ($key_minus_flag == 1) {
                                                    $l_key = $l_key - 1;
                                                }
                                                $new_value_array[$l_key] = $new_value;
                                            }
                                            $trend_pl_quarter_final_array[$inner_key]['rows'][$last_key] = $new_value_array;
                                        }
                                    }
                                } else {
                                    $key_minus_flag = 0;
                                    if (!array_key_exists("0", $trend_quarter_final_value)) {
                                        $key_minus_flag = 1;
                                    }
                                    foreach ($trend_quarter_final_value as $last_key => $value) {
                                        if ($key_minus_flag == 1) {
                                        }
                                        $trend_pl_quarter_final_array[$key]['rows'][$inner_key][$last_key] = $value;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (count($trend_pl_quarter_final_array) > 0) {
                // Header
                $row = 5;
                $quarter_array = $this->getQuartelyMonthArray();
                foreach ($quarter_array as $key => $value) {
                    $header[] = $value;
                }
                $num_headers = count($header);
                $objPHPExcel->getActiveSheet()->getStyle("B5:M5")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("B5:M5")->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, "");

                for ($col = 2; $col <= $num_headers + 3; ++$col) {
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $header[$col - 2]);
                }
                $row++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "");
                $row++;

                foreach ($trend_pl_quarter_final_array as $main_key => $main_value) {
                    $total_record_value = array();
                    $no_expand = 0;
                    $build_heading = 1;

                    if ($main_key == "Gross Profit" || $main_key == "Operating Profit" || $main_key == "Net Profit") {
                        $build_heading = 0;
                    } else if ($main_key == "Employee Compensation & Related Expenses") {
                        $build_heading = 1;
                    }

                    if ($build_heading == 1) {
                        $col = 1;
                        for ($col = 1; $col <= $num_headers; ++$col) {
                            if ($col == 1) {
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $main_key);
                            } else {
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                            }
                        }
                        $row++;
                    }

                    foreach ($main_value as $second_level_key => $second_level_value) {
                        if ($second_level_key == "header") {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                if ($main_key == "Gross Profit" || $main_key == "Operating Profit" || $main_key == "Net Profit") {
                                    unset($third_level_value['no_expand']);
                                    $no_expand = 1;
                                    $bold = 1;
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                        ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                                    $this->buildPDFforTrendQuarterlyxls($third_level_key, $third_level_value, $objPHPExcel, $row, true, 1);
                                    $row++;
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                                    $row++;
                                } else {
                                    $total_record_key = $third_level_key;
                                    foreach ($second_level_value as $key => $row_value) {
                                        $total_record_value = $row_value;
                                    }
                                }
                            }
                        } else if ($second_level_key == "rows") {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                if ($main_key == "Employee Compensation & Related Expenses") {
                                    $this->buildPDFforTrendQuarterlyxls($third_level_key, $third_level_value, $objPHPExcel, $row);
                                    $row++;
                                } else {
                                    $this->buildPDFforTrendQuarterlyxls($third_level_key, $third_level_value, $objPHPExcel, $row);
                                    $row++;
                                }
                            }
                        }
                    }

                    if (count($total_record_value) > 0) {
                        $bold = 1;
                        if ($main_key == "Employee Compensation & Related Expenses") {
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                            $this->buildPDFforTrendQuarterlyxls($total_record_key, $total_record_value, $objPHPExcel, $row, $bold, true);
                            $row++;
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "");
                            $row++;
                        } else {
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                            $this->buildPDFforTrendQuarterlyxls($total_record_key, $total_record_value, $objPHPExcel, $row, $bold, true);
                            $row++;
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "");
                            $row++;
                        }
                    }
                }

            }
        } else if ($position == 1) {
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', "As at  $last_updated_on");
            $objPHPExcel->getActiveSheet()->mergeCells('A3:L3');
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $row = 5;
            $balance_sheet_array = $report_obj[$position]["balanceSheet"];
            $final_array = array();

            foreach ($balance_sheet_array as $key => $sheet_value) {
                foreach ($sheet_value['summaryRow'] as $inner_key => $value) {
                    if ($inner_key != 'equity_gray') {
                        $final_array[$key]['header'][$inner_key] = $value;
                    }
                }
                foreach ($sheet_value as $inner_key => $value) {
                    if ($inner_key != 'summaryRow' && !is_numeric($inner_key) && $inner_key != "no_expand") {
                        $final_array[$key]['rows'][$inner_key] = $value;
                    } else if ($inner_key != 'summaryRow' && (is_numeric($inner_key) || $inner_key == "no_expand")) {
                        $final_array[$key]['header'][$key][$inner_key] = $value;
                    }
                }
            }

            if (count($final_array) > 0) {
                // Header
                $header = array_reverse($reportDate);
                $num_headers = count($header);
                $header = array_reverse($reportDate);
                $num_headers = count($header);
                $objPHPExcel->getActiveSheet()->getStyle("B5:M5")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("B5:M5")->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, "");

                for ($col = 2; $col < $num_headers + 3; ++$col) {
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $header[$col - 2]);
                }
                $row++;

                foreach ($final_array as $main_key => $main_value) {
                    $total_record_value = array();
                    $no_expand = 0;
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                    $col = 1;
                    if ($main_key == "Total Assets" || $main_key == "Total Liabilities" || $main_key == "Net Assets") {
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                        $row++;
                    } else {

                        for ($col = 1; $col <= $num_headers; ++$col) {
                            if ($col == 1) {
                                if ($main_key == "Bank") {
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $main_key);
                                    $row++;
                                } else {
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                                    $row++;
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $main_key);
                                    $row++;
                                }
                            } else {
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                            }
                        }

                    }

                    foreach ($main_value as $second_level_key => $second_level_value) {
                        $col = 1;
                        if ($second_level_key == "header") {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                if (array_key_exists('no_expand', $third_level_value)) {
                                    $no_expand = 1;
                                    unset($third_level_value['no_expand']);
                                }
                                if ($no_expand == 1) {
                                    $bold = 1;
                                    $this->buildPDFforTrendxls($third_level_key, $third_level_value, $objPHPExcel, $row, $reportDate, $bold, "", 1);
                                    $row++;
                                    if ($third_level_key != "Total Liabilities") {
                                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");

                                        $row++;
                                    }
                                } else {
                                    $total_record_key = $third_level_key;
                                    foreach ($third_level_value as $key => $row_value) {
                                        $total_record_value[] = $row_value;
                                    }
                                }
                            }
                            $col++;
                        } else if ($second_level_key == "rows") {
                            foreach ($second_level_value as $third_level_key => $third_level_value) {
                                $this->buildPDFforTrendxls($third_level_key, $third_level_value, $objPHPExcel, $row, $reportDate);
                                $row++;
                            }

                        }
                    }
                    if (count($total_record_value) > 0) {
                        $bold = 1;
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                        $this->buildPDFforTrendxls($total_record_key, $total_record_value, $objPHPExcel, $row, $reportDate, $bold, "", 1);
                        $row++;
                    }
                }
            }
        } else if ($position == 5) {
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', "As at  $last_updated_on");
            $objPHPExcel->getActiveSheet()->mergeCells('A3:K3');
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $row = 5;

            $cashFlowStatementArr = $report_obj[$position]["cashFlowStatement"];

            if (count($cashFlowStatementArr) > 0) {
                $finalValue = array();
                foreach ($cashFlowStatementArr as $key => $value) {
                    foreach ($value as $key1 => $value1) {
                        $title = "";
                        $title = $value1['title'];
                        unset($value1['value']['no_expand']);
                        if (is_numeric($key1)) {
                            $finalValue[$key]['rows'][$title] = $value1['value'];
                        } else if ($key1 == 'summaryRow') {
                            $finalValue[$key]['header'][$title] = $value1['value'];
                        }
                    }
                }

                if (count($finalValue) > 0) {
                    // Header
                    $header = array_reverse($reportDate);
                    $num_headers = count($header);
                    $objPHPExcel->getActiveSheet()->getStyle("B5:M5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("B5:M5")->getFont()->setSize(9);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, "");

                    for ($col = 2; $col <= $num_headers + 1; ++$col) {
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, substr($header[$col - 2], 3));
                    }
                    $row++;

                    foreach ($finalValue as $main_key => $main_value) {
                        $total_record_value = array();
                        $no_expand = 0;

                        for ($col = 1; $col <= $num_headers; ++$col) {
                            if ($col == 1) {
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $main_key);
                                $row++;
                            } else {
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                            }
                        }

                        foreach ($main_value as $second_level_key => $second_level_value) {
                            if ($second_level_key == "header") {
                                foreach ($second_level_value as $third_level_key => $third_level_value) {
                                    $total_record_key = $third_level_key;
                                    foreach ($third_level_value as $key => $row_value) {
                                        $total_record_value[] = $row_value;
                                    }
                                }

                            } else if ($second_level_key == "rows") {
                                foreach ($second_level_value as $third_level_key => $third_level_value) {
                                    $this->buildPDFforTrendxls($third_level_key, $third_level_value, $objPHPExcel, $row, $reportDate, false);
                                    $row++;
                                }

                            }
                        }

                        if (count($total_record_value) > 0) {
                            $this->buildPDFforTrendxls($total_record_key, $total_record_value, $objPHPExcel, $row, $reportDate, true, "", 1);
                            $row++;
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                            $row++;
                        }
                    }
                }
            }
        } elseif ($position == 6) {
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', "As at  $last_updated_on");
            $objPHPExcel->getActiveSheet()->mergeCells('A3:N3');
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $contact_by_nameArray = $report_obj[$position]["invoiceByName"];

            if ($sorting == 0) {
                array_multisort(array_map(function ($element) {
                    return $element["value"]["total"]["amount"];
                }, $contact_by_nameArray), SORT_ASC, $contact_by_nameArray);
            } else if ($sorting == 1) {
                array_multisort(array_map(function ($element) {
                    return $element["value"]["total"]["amount"];
                }, $contact_by_nameArray), SORT_DESC, $contact_by_nameArray);
            } else if ($sorting == 2) {
                array_multisort(array_map(function ($element) {
                    return $element["value"]["total"]["amount"];
                }, $contact_by_nameArray), SORT_DESC, $contact_by_nameArray);
                $contact_by_nameArray = array_slice($contact_by_nameArray, 0, 10);
            }

            $total_array = [];
            $totalamtVal = 0;
            $loop = 0;
            foreach ($contact_by_nameArray as $keyArr => $arrVal) {
                foreach (array_reverse($reportDate) as $key => $value) {
                    $getMonth = date("n", strtotime($value));
                    $getYear = date("y", strtotime($value));
                    $getInnKey = null;
                    $getInnKey = $this->searchIdFromArray($getMonth . '-' . $getYear, $arrVal['value']);
                    $amtVal = 0;
                    if (sizeof($getInnKey) > 0) {
                        $amtVal = $arrVal['value'][$getInnKey]['amount'];
                    } else {
                        $amtVal = 0;
                    }

                    if ($loop == 0) {
                        $obj = array('amount' => $amtVal, 'month' => $getMonth, 'year' => $getYear);
                        array_push($total_array, $obj);
                    } else {
                        $getTotalInnKey = $this->searchIdFromArray($getMonth . '-' . $getYear, $total_array);
                        if (sizeof($getTotalInnKey) > 0) {
                            $totalamtVal = $total_array[$getTotalInnKey]['amount'];
                            $totalamtVal = $totalamtVal + $amtVal;
                            $total_array[$getTotalInnKey]['amount'] = $totalamtVal;
                        }
                    }
                }
                $loop++;
            }

            $total_array["total"]["amount"] = 0;
            array_push($contact_by_nameArray, array('company_name' => 'Total', 'value' => $total_array));

            foreach ($contact_by_nameArray as $keyArr => $arrVal) {
                $totalNetValue = 0;
                if ($arrVal['company_name'] == "Total") {
                    foreach ($arrVal['value'] as $key => $val) {
                        if ($key != "total") {
                            $totalNetValue = $totalNetValue + $val['amount'];
                        }

                    }
                    $contact_by_nameArray[$keyArr]['value']['total']['amount'] = $totalNetValue;
                } else {
                    if (isset($contact_by_nameArray[$keyArr]['value']['total']['amount'])) {
                        if ($contact_by_nameArray[$keyArr]['value']['total']['amount'] > 0) {
                            $totalNetValue = $totalNetValue + $contact_by_nameArray[$keyArr]['value']['total']['amount'];
                        }
                    }
                }
            }

            if (count($contact_by_nameArray) > 0) {
                $objPHPExcel->getActiveSheet()->getStyle("B5:O5")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("B5:O5")->getFont()->setSize(9);
                //header
                $header = array_reverse($reportDate);
                $num_headers = count($header);
                $row = 5;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, "");
                for ($col = 2; $col < $num_headers + 2; ++$col) {
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, substr($header[$col - 2], 3));
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                }
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(13, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(14, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $row, "$");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $row, "%");
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(13, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(14, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                $row++;
                foreach ($contact_by_nameArray as $keyArr => $arrVal) {
                    $col = 1;
                    if ($arrVal['company_name'] == "Total") {
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                            ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                    }
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $arrVal['company_name']);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $col = 2;
                    foreach (array_reverse($reportDate) as $key => $value) {

                        $getMonth = date("n", strtotime($value));
                        $getYear = date("y", strtotime($value));
                        $getInnKey = null;
                        $getInnKey = $this->searchIdFromArray($getMonth . '-' . $getYear, $arrVal['value']);
                        $amtVal = '';

                        if (sizeof($getInnKey) > 0) {
                            if ($arrVal['company_name'] == "Total") {
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                    ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                            }

                            $amtVal = $this->format_number_exportXls($arrVal['value'][$getInnKey]['amount'], 1);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(8);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $amtVal);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                        } else {
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(8);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, '0');
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                        }

                        if ($key == count($reportDate) - 1) {
                            $totalAmt = '0';
                            $calPercent = 0;
                            if (!empty($arrVal['value']['total']['amount'])) {
                                $totalAmt = $this->format_number_exportXls($arrVal['value']['total']['amount'], 1);
                                $netAmt = $arrVal['value']['total']['amount'];
                                if ($totalNetValue > 0) {
                                    $calPercent = (($netAmt / round($totalNetValue)) * 100);
                                }
                            }
                            if ($arrVal['company_name'] == "Total") {
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(13, $row)->getStyle()->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(13, $row)->getStyle()->getFont()->setSize(9);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(14, $row)->getStyle()->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(14, $row)->getStyle()->getFont()->setSize(9);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(14, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(13, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                            }
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(13, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(14, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(13, $row)->getStyle()->getFont()->setSize(8);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(14, $row)->getStyle()->getFont()->setSize(8);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $row, $this->format_number_exportXls($netAmt, 1));
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $row, round($calPercent) . "%");
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(14, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(13, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                        }
                        $col++;
                    }
                    $row++;
                }
            }
        }
        return $result_message;
    }

    public function getReportValuesxls($path, $position, $type, $company_name, $objPHPExcel, $row)
    {
        $report_obj = json_decode(file_get_contents($path), true);
        $last_updated_on = date("d M Y", strtotime($report_obj['last_updated_time']));
        $col = 0;

        if ($position == 1) {
            $rowArray = ['TOTAL CASH ON HAND', 'Bank Summary', $company_name . ' | As at ' . $last_updated_on, '', 'Bank Accounts', 'Closing Balance'];
            foreach ($rowArray as $row_value) {
                if ($row_value == 'TOTAL CASH ON HAND' || $row_value == 'Bank Summary' || $row_value == 'Bank Accounts' || $row_value == 'Closing Balance') {
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(9);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setSize(9);
                } else {
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(false);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(8);
                }

                if ($row_value != "Closing Balance") {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $row_value);
                    $row++;
                } else {
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row - 1)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row - 1, $row_value);
                }
            }
            unset($report_obj[$position]["accountsSummary"]['org_name']);
            foreach ($report_obj[$position]["accountsSummary"] as $key => $value) {
                $append_flag = 0;
                // if ($append_flag == 1) {
                if ($append_flag == 0) {
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setSize(8);
                    if ($value['bank_account_name'] == "Total") {
                        for ($col = 1; $col <= 7; ++$col) {
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                        }
                        if ($value['closing_balance'] < 0) {
                            $final_value = $this->format_number_exportXls($value['closing_balance'], 0);
                            $style = array('font' => array('size' => 9, 'bold' => true, 'color' => array('rgb' => 'A52A2A')));
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->applyFromArray($style);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                        } else {
                            $final_value = (int)round($value['closing_balance']);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                        }
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $value['bank_account_name']);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $final_value);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                        $row++;
                    } else {
                        if ($value['closing_balance'] < 0) {
                            $final_value = $this->format_number_exportXls($value['closing_balance'], 0);
                            $style = array('font' => array('size' => 9, 'bold' => true, 'color' => array('rgb' => 'A52A2A')));
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->applyFromArray($style);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                        } else {
                            $final_value = (int)round($value['closing_balance']);
                        }
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(false);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setBold(false);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $value['bank_account_name']);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $final_value);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                        $row++;
                    }
                }
            }
        } else if ($position == 2) {
            if (isset($report_obj[$position]["agedPayables"])) {
                $rowArray = ['CURRENT ACCOUNTS PAYABLE DUE', 'Aged Payable', $company_name . ' | As at ' . $last_updated_on];
                foreach ($rowArray as $row_value) {
                    if ($row_value == 'CURRENT ACCOUNTS PAYABLE DUE' || $row_value == 'Aged Payable') {
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(9);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $row_value);
                    } else {
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(false);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(8);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $row_value);
                    }
                    $row++;
                }

                $header_value_arr = array("Name");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "");
                $row++;
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "Payables");

                $col = 2;
                foreach ($report_obj[$position]["agedPayables"][0]["total_payables"] as $key => $value) {
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    if ($key != "Older" && $key != "Total") {
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $key);
                        $header_value_arr[] = $key;
                        $col++;
                    }
                }
                $header_value_arr[] = "Older";
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "Older");
                $col++;

                $header_value_arr[] = "Total";
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "Total");
                $col++;
                $row++;

                $agedReceivables_nameArray = $report_obj[$position]["agedPayables"][0];

                array_multisort(array_map(function ($element) {
                    return $element["Name"];
                }, $agedReceivables_nameArray), SORT_ASC, $agedReceivables_nameArray);

                $agedPayables_array[0] = $agedReceivables_nameArray;
                $total_payables = $agedPayables_array[0]['total_payables'];
                unset($agedPayables_array[0]['total_payables']);
                $agedPayables_array[0]['total_payables'] = $total_payables;

                foreach ($agedPayables_array as $key => $value) {
                    foreach ($value as $key => $receivable_record) {
                        $col = 1;
                        foreach ($header_value_arr as $receive_rows) {
                            $receivable_final = $receivable_record[$receive_rows];
                            if ($key == "total_payables") {
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                    ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                            }
                            if (empty($receivable_final) && $receive_rows == "Name") {
                                $receivable_final = "Total Payables";
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                    ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                            }
                            if ($receive_rows != "Name") {
                                if ($receivable_final < 0) {
                                    $receivable_final_value = $this->format_number_exportXls($receivable_final, 0);
                                    $style = array('font' => array('size' => 9, 'bold' => true, 'color' => array('rgb' => 'A52A2A')));
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->applyFromArray($style);
                                } else {
                                    $receivable_final_value = (int)round($receivable_final);
                                }
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                                $result_value = $receivable_final_value;
                                $text_align = "R";
                                $width = 30;
                            } else {
                                $result_value = $receivable_final;
                                $text_align = "L";
                                $width = 100;
                            }
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(8);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $result_value);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                            $col++;
                        }$row++;
                    }

                }
            }
        } else if ($position == 3) {
            if (isset($report_obj[$position]["agedReceivables"])) {
                $rowArray = ['CURRENT ACCOUNTS RECEIVABLE DUE', 'Aged Receivable', $company_name . ' | As at ' . $last_updated_on];
                foreach ($rowArray as $row_value) {
                    if ($row_value == 'CURRENT ACCOUNTS RECEIVABLE DUE' || $row_value == 'Aged Receivable') {
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(9);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $row_value);
                    } else {
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(false);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(8);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $row_value);

                    }
                    $row++;
                }

                $header_value_arr = array("Name");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "");
                $row++;
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "Receivables");

                $col = 2;
                foreach ($report_obj[$position]["agedReceivables"][0]["total_receivables"] as $key => $value) {
                    if ($key != "Older" && $key != "Total") {
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $key);
                        $header_value_arr[] = $key;
                        $col++;
                    }
                }
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                $header_value_arr[] = "Older";
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "Older");
                $col++;
                $header_value_arr[] = "Total";
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "Total");
                $row++;

                $agedReceivables_nameArray = $report_obj[$position]["agedReceivables"][0];
                array_multisort(array_map(function ($element) {
                    return $element["Name"];
                }, $agedReceivables_nameArray), SORT_ASC, $agedReceivables_nameArray);

                $agedReceivables_array[0] = $agedReceivables_nameArray;
                $total_receivables = $agedReceivables_array[0]['total_receivables'];
                unset($agedReceivables_array[0]['total_receivables']);
                $agedReceivables_array[0]['total_receivables'] = $total_receivables;

                foreach ($agedReceivables_array as $key => $value) {
                    foreach ($value as $key => $receivable_record) {
                        $col = 1;
                        foreach ($header_value_arr as $receive_rows) {
                            $receivable_final = $receivable_record[$receive_rows];
                            $text_style = "";
                            if ($key == "total_receivables") {
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                    ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                            }
                            if (empty($receivable_final) && $receive_rows == "Name") {
                                $receivable_final = "Total Receivables";
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                                    ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                            }
                            if ($receive_rows != "Name") {
                                if ($receivable_final < 0) {
                                    $receivable_final_value = $this->format_number_exportXls($receivable_final, 0);
                                    $style = array('font' => array('size' => 9, 'bold' => true, 'color' => array('rgb' => 'A52A2A')));
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->applyFromArray($style);
                                } else {
                                    $receivable_final_value = (int)round($receivable_final);
                                }
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                                $result_value = $receivable_final_value;
                                $text_align = "R";
                                $width = 30;
                            } else {
                                $result_value = $receivable_final;
                                $text_align = "L";
                                $width = 100;
                            }
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(8);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $result_value);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                            $col++;
                        }$row++;
                    }
                }
            }
        } else if ($position == 4) {
            $date_value = $report_obj['4']['fromToDate'];
            $rowArray = ['ESTIMATED PAYROLL', 'Estimated Payroll', $company_name . ' | All Terittories | ' . $date_value];
            foreach ($rowArray as $row_value) {
                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(15);
                if ($row_value == 'ESTIMATED PAYROLL' || $row_value == 'Estimated Payroll') {
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $row_value);
                } else {
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(false);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $row_value);
                }
                $row++;
            }

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "");
            $row++;
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "Payroll");
            $col = 2;
            $total_count = count($report_obj[$position]["estimatedPayroll"][0][0]) - 1;
            foreach ($report_obj[$position]["estimatedPayroll"][0] as $key => $inner_array) {
                if (is_numeric($key)) {

                    foreach ($inner_array as $key1 => $inner_value) {
                        if ($total_count > 1) {
                            if ($inner_value == "Total") {
                                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getAlignment()->setWrapText(true);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setSize(9);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $inner_value['Value']);
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getAlignment()->setWrapText(true);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setSize(8);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $inner_value['Value']);
                        }
                    }
                }
            }
            $row++;

            $total_count = count($report_obj[$position]["estimatedPayroll"][0]);
            foreach ($report_obj[$position]["estimatedPayroll"][0] as $key => $inner_array) {
                if (!is_numeric($key)) {
                    $text_align = "left";
                    $text_style = "";
                    if ($total_count == $i) {
                        $line_border = $title_border;
                    }
                    $end_array = end($inner_array);
                    $col = 1;
                    if ($total_count > 1) {
                        foreach ($inner_array as $key => $inner_value) {
                            if ($key == 1) {
                            }
                            if ($inner_value['Value'] == "Total Employee Compensation & Related Expenses") {
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(8);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setSize(8);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');

                            }
                            if ($total_count > 1) {
                                if ($inner_value['Value'] < 0) {
                                    $receivable_final_value = $this->format_number_exportXls($inner_value['Value'], 0);
                                    $style = array('font' => array('size' => 9, 'bold' => true, 'color' => array('rgb' => 'A52A2A')));
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->applyFromArray($style);
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                                } else {
                                    $receivable_final_value = (int)round($inner_value['Value']);
                                }
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setSize(8);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(15);
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, (int)round($end_array['Value']));
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                                $col++;
                            }
                            if ($key == 0) {
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(8);
                                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(15);
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $inner_value['Value']);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                                $col++;
                            }
                        }$row++;
                    } else {
                        foreach ($inner_array as $key => $inner_value) {
                            if ($key == 1) {
                            }
                            if ($inner_value['Value'] == "Total Employee Compensation & Related Expenses") {
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(8);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setSize(8);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');

                            }
                            if ($key > 0) {
                                if ($inner_value['Value'] < 0) {
                                    $receivable_final_value = $this->format_number_exportXls($inner_value['Value'], 0);
                                    $style = array('font' => array('size' => 9, 'bold' => true, 'color' => array('rgb' => 'A52A2A')));
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->applyFromArray($style);
                                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                                } else {
                                    $receivable_final_value = (int)round($inner_value['Value']);
                                }
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getFont()->setSize(8);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(15);
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $receivable_final_value);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                                $col++;
                            } else {
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getFont()->setSize(8);
                                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(15);
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $inner_value['Value']);
                                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                                $col++;
                            }
                        }$row++;
                    }

                }
            }

        }
        return $row;
    }

    public function buildPDFforConsolidatedxls($value, $objPHPExcel, $row, $bold_data = null, $row_border = null)
    {

        $col = 1;
        foreach ($value as $key => $row_value) {
            if ($bold_data != null || $bold_data != "") {
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(9);
                 $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
            }
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
            if (!is_array($row_value)) {
                if (is_numeric($key)) {
                    $type = 1;
                    if ($row_value < 0) {
                        $style = array('font' => array('size' => 9, 'bold' => true, 'color' => array('rgb' => 'A52A2A')));
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->applyFromArray($style);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                        $type = 0;
                    }
                    if ($row_value == "Operating Profit") {
                        $row_value = "EBITDA";
                    }
                    if ($key == 4) {
                        $row_value = $this->format_number_exportXls($row_value, 2);
                    } else if ($key != 0 && $key != 4) {
                        $row_value = $this->format_number_exportXls($row_value, $type);
                    }
                    if ($row_border == 1) {
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');
                    }
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $row_value);
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getNumberFormat()->setFormatCode('#,##0');

                } else {
                    if ($row_border == 1) {
                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()
                            ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "");
                    $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $row_value);
                }
            }
            $col++;
        }
    }

    public function getgroupDashboardBudgetDetails($client_master_id, $sub_category_id1, $sub_category_id2, $client_id, $report_start_date)
    {
        try {
            $finalResponse = array();
            $db1 = new utility();
            $current_month = strtolower(date('M'));

            $path = __DIR__ . "/metrics/metric_" . $client_master_id . "_" . $report_start_date . ".json";
            $json_file = file_get_contents($path);
            $jfo = json_decode($json_file, true);

            $net_profit_array = $jfo['Net Profit'];
            $net_Operating_Profit = $jfo['Operating Profit'];

            $month_arr = array('jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec');
            $consoliate_month_arr = array();
            $consoliate_month_arr_ytd_my = array();
            $consoliate_month_arr_ytd = array();
            $month_arr_linechart = array();
            $month_arr_barchart = array();
            $budget_track_result = array();

            $startDate = date("Y-m-01", strtotime("-11 months"));
            $endDate = date('Y-m-d');
            $i = 0;
            while (strtotime($startDate) <= strtotime($endDate)) {
                $consolidate_month_arr[$i] = date('M', strtotime($startDate));
                $month_arr_linechart[$i] = date('M', strtotime($startDate)) . '-' . date('y', strtotime($startDate));
                $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
                $i++;
            }
            //$startDate = date("Y-m-01", strtotime("-12 months"));
            $startDate = date("Y-m-01", strtotime($report_start_date));
            $i = 0;
            while (strtotime($startDate) <= strtotime($endDate)) {
                $consoliate_month_arr_ytd_my[$i] = date('n', strtotime($startDate)) . '-' . date('y', strtotime($startDate));
                $consoliate_month_arr_ytd[$i] = date('M', strtotime($startDate));
                $month_arr_barchart[$i] = date('M', strtotime($startDate)) . '-' . date('y', strtotime($startDate));
                $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
                $i++;
            }

            array_pop($month_arr_linechart);
            $budget_track_result['dashboard']['Month Array'] = $month_arr_barchart;
            $budget_track_result['dashboard']['Month Array Last'] = $month_arr_linechart;

            /* Finding Actual & Target Values for Dashboard - Revenue, Expenses, EBITDA graphs */
            $netProfitActualBar = array();
            $actual_total_income = 0;
            $actual_total_cost_of_sales = 0;
            $actual_total_operating_expenses = 0;
            $actual_net_Operating_Profit = 0;

            unset($net_Operating_Profit['no_expand']);
            foreach ($consoliate_month_arr_ytd_my as $key => $value) {
                $keyTI = $this->searchIdFromArray($value, $jfo['Total Income']);
                if (sizeof($keyTI) > 0) {
                    $actual_total_income = $actual_total_income + $jfo['Total Income'][$keyTI]['amount'];
                }

                $keyTCS = $this->searchIdFromArray($value, $jfo['Total Cost of Sales']);
                if (sizeof($keyTCS) > 0) {
                    $actual_total_cost_of_sales = $actual_total_cost_of_sales + $jfo['Total Cost of Sales'][$keyTCS]['amount'];
                }

                $keyTOE = $this->searchIdFromArray($value, $jfo['Total Operating Expenses']);
                if (sizeof($keyTOE) > 0) {
                    $actual_total_operating_expenses = $actual_total_operating_expenses + $jfo['Total Operating Expenses'][$keyTOE]['amount'];
                }

                $keyNOP = $this->searchIdFromArray($value, $net_Operating_Profit);
                if (sizeof($keyNOP) > 0) {
                    $actual_net_Operating_Profit = $actual_net_Operating_Profit + $net_Operating_Profit[$keyNOP]['amount'];
                    array_push($netProfitActualBar, round($net_Operating_Profit[$keyNOP]['amount']));
                } else {
                    array_push($netProfitActualBar, 0);
                }
            }

            $searchParams = array();
            $budget_target_result = array();
            $searchParams["line_type"] = "summaryRow";
            $searchParams["sub_category_id1"] = "1000";
            $budget_target_result = $this->getDashboardBudgetDetailsFromDB($client_master_id, $searchParams);

            if ($budget_target_result['message'] != 'No record found') {
                $netProfitTarget = array();

                foreach ($budget_target_result['budgetSummary'] as $record) {
                    $actual_amount = 0;
                    $ytd_budget_amount = 0;
                    $total_values = $record['total_values'];
                    foreach ($consoliate_month_arr_ytd as $month_value) {
                        $ytd_budget_amount = $ytd_budget_amount + $record[strtolower($month_value)];
                        if ($record['item_head'] == 'Net Profit') {
                            array_push($netProfitTarget, round($record[strtolower($month_value)]));
                        }
                    }

                    if ($record['item_head'] == 'Total Income') {
                        $actual_amount = $actual_total_income;
                    } else if ($record['item_head'] == 'Total Operating Expenses') {
                        $actual_amount = $actual_total_cost_of_sales + $actual_total_operating_expenses;
                    } else if ($record['item_head'] == 'Net Profit') {
                        $actual_amount = $actual_net_Operating_Profit;
                    }
                    $budget_track_result['dashboard'][$record[item_head]] = array("actual_amount" => $actual_amount, "ytd_budget_amount" => $ytd_budget_amount, "total_budget_amount" => $total_values);
                }
                $budget_track_result['dashboard']['Net Profit Target'] = $netProfitTarget;
                $budget_track_result['dashboard']['Net Profit Actual'] = $netProfitActualBar;
            } else {
                $total_values = 0;
                $ytd_budget_amount = 0;

                $budget_track_result['dashboard']['Total Income'] = array("actual_amount" => $actual_total_income, "ytd_budget_amount" => $ytd_budget_amount, "total_budget_amount" => $total_values);

                $actual_amount = 0;
                $actual_amount = $actual_total_cost_of_sales + $actual_total_operating_expenses;
                $budget_track_result['dashboard']['Total Operating Expenses'] = array("actual_amount" => $actual_amount, "ytd_budget_amount" => $ytd_budget_amount, "total_budget_amount" => $total_values);

                $budget_track_result['dashboard']['Net Profit'] = array("actual_amount" => $actual_net_Operating_Profit, "ytd_budget_amount" => $ytd_budget_amount, "total_budget_amount" => $total_values);
                $budget_track_result['dashboard']['Net Profit Target'] = [];
                $budget_track_result['dashboard']['Net Profit Actual'] = $netProfitActualBar;
            }

            /* Finding Values for Dashboard - Revenue, Operating Expenses, Total Employee Compensation, EBITDA, Total bank Transaction graphs */
            $totalIncomeMonth = array();
            $totalOPEXMonth = array();
            $totalEmpCompMonth = array();
            $netProfitMonth = array();
            $totalBankTransMonth = array();

            foreach ($month_arr_linechart as $key => $value) {
                if (isset($jfo['Total Income'][$key])) {
                    if ($value != $current_month) {
                        array_push($totalIncomeMonth, round($jfo['Total Income'][$key]['amount']));
                    }
                } else {
                    if ($value != $current_month) {
                        array_push($totalIncomeMonth, 0);
                    }
                }

                if (isset($jfo['Total Operating Expenses'][$key])) {
                    if ($value != $current_month) {
                        array_push($totalOPEXMonth, round($jfo['Total Operating Expenses'][$key]['amount']));
                    }
                } else {
                    if ($value != $current_month) {
                        array_push($totalOPEXMonth, 0);
                    }
                }

                if (isset($jfo['Total Employee Compensation'][$key])) {
                    if ($value != $current_month) {
                        array_push($totalEmpCompMonth, round($jfo['Total Employee Compensation'][$key]['amount']));
                    }
                } else {
                    if ($value != $current_month) {
                        array_push($totalEmpCompMonth, 0);
                    }
                }

                if (isset($net_Operating_Profit[$key])) {
                    if ($value != $current_month) {
                        array_push($netProfitMonth, round($net_Operating_Profit[$key]['amount']));
                    }
                } else {
                    if ($value != $current_month) {
                        array_push($netProfitMonth, 0);
                    }
                }

                if (isset($jfo['BankTransactions']['Number of Total Transactions']['summaryRow'][$key])) {
                    if ($value != $current_month) {
                        array_push($totalBankTransMonth, round($jfo['BankTransactions']['Number of Total Transactions']['summaryRow'][$key]));
                    }
                } else {
                    if ($value != $current_month) {
                        array_push($totalBankTransMonth, 0);
                    }
                }
            }

            $budget_track_result['dashboard']['Total Income Monthwise'] = $totalIncomeMonth;
            $budget_track_result['dashboard']['Total Operating Expenses Monthwise'] = $totalOPEXMonth;
            $budget_track_result['dashboard']['Total Employee Compensation Monthwise'] = $totalEmpCompMonth;
            $budget_track_result['dashboard']['Net Profit Monthwise'] = $netProfitMonth;
            $budget_track_result['dashboard']['Total Bank Transaction Monthwise'] = $totalBankTransMonth;

            $budget_track_result['dashboard']['Total Income Monthwise SMA'] = $this->generateSMA($month_arr_linechart, $totalIncomeMonth);
            $budget_track_result['dashboard']['Total Operating Expenses Monthwise SMA'] = $this->generateSMA($month_arr_linechart, $totalOPEXMonth);
            $budget_track_result['dashboard']['Total Employee Compensation Monthwise SMA'] = $this->generateSMA($month_arr_linechart, $totalEmpCompMonth);
            $budget_track_result['dashboard']['Net Profit Monthwise SMA'] = $this->generateSMA($month_arr_linechart, $netProfitMonth);
            $budget_track_result['dashboard']['Total Bank Transaction Monthwise SMA'] = $this->generateSMA($month_arr_linechart, $totalBankTransMonth);

            /* Finding Top 10 Expenses Values for Dashboard */
            $budget_track_result['dashboard']['Top N Expenses'] = $jfo['Operating Expenses'];

            $account_receivable = 0;
            foreach ($jfo['Account Receivables'] as $receive_value) {
                $account_receivable = $account_receivable + $receive_value['amount'];
            }

            $account_payable = 0;
            foreach ($jfo['Account Payables'] as $payable_value) {
                $account_payable = $account_payable + $payable_value['amount'];
            }

            $total_cost_of_sales = 0;
            foreach ($jfo['Total Cost of Sales'] as $sales_value) {
                $total_cost_of_sales = $total_cost_of_sales + $sales_value['amount'];
            }

            //Average OPEX over last 3-months
            // $data = array();
            // $data = $this->getEarlierMonthYear($report_start_date);
            // $dataLength = sizeof($data);
            // $average_opex_total = 0;
            // $avg = 0;

            // if ($dataLength == 1) {
            //     $key = $this->searchIdFromArray($data[0], $jfo['Total Operating Expenses']);
            //     if ($key != null) {
            //         $average_opex_total = $jfo['Total Operating Expenses'][$key]['amount'];
            //     }
            //     $avg = $average_opex_total;
            // } elseif ($dataLength == 2) {
            //     foreach ($data as $key => $val) {
            //         $arrayid = $this->searchIdFromArray($val, $jfo['Total Operating Expenses']);
            //         if ($arrayid != null) {
            //             $average_opex_total = $average_opex_total + $jfo['Total Operating Expenses'][$arrayid]['amount'];
            //         }

            //     }
            //     $avg = $average_opex_total / 2;
            // } elseif ($dataLength >= 3) {
            //     $count = 0;
            //     foreach ($data as $key => $val) {
            //         $arrayid1 = $this->searchIdFromArray($val, $jfo['Total Operating Expenses']);
            //         if ($arrayid1 != null && $count < 3) {
            //             $average_opex_total = $average_opex_total + $jfo['Total Operating Expenses'][$arrayid1]['amount'];
            //             $count = $count + 1;
            //         }
            //     }
            //     $avg = $average_opex_total / 3;
            // }

            $avg = (sizeof($budget_track_result['dashboard']['Net Profit Monthwise SMA']) > 0) ? end($budget_track_result['dashboard']['Net Profit Monthwise SMA']) : 0;

            $budget_track_result['metrics'] = array("Total Accounts Receivable" => $account_receivable,
                "Total Accounts Payable" => $account_payable,
                "Total Cost Of Sales" => $total_cost_of_sales,
                "dso" => isset($jfo['Accounts Receivable DSO']) ? $jfo['Accounts Receivable DSO'] : 0,
                "dpo" => isset($jfo['Days Payable Outstanding (DPO)']) ? $jfo['Days Payable Outstanding (DPO)'] : 0,
                "ccc" => isset($jfo['Cash Conversion Cycle']) ? $jfo['Cash Conversion Cycle'] : 0,
                "agedPayablestotal" => isset($jfo['agedPayablestotal']) ? $jfo['agedPayablestotal'] : 0,
                "agedReceivablestotal" => isset($jfo['agedReceivablestotal']) ? $jfo['agedReceivablestotal'] : 0,
                "estimatedPayrolltotal" => isset($jfo['estimatedPayrolltotal']) ? $jfo['estimatedPayrolltotal'] : 0,
                "averageOpextotal" => round($avg),
                "accountsSummarytotal" => isset($jfo['accountsSummarytotal']) ? $jfo['accountsSummarytotal'] : 0,
                "totalCurrentAssetsLast" => isset($jfo['Total Current Assets Last']) ? $jfo['Total Current Assets Last']['amount'] : 0,
                "totalBankLast" => isset($jfo['Total Bank Last']) ? $jfo['Total Bank Last']['amount'] : 0,
                "totalCurrentLiabilitiesLast" => isset($jfo['Total Current Liabilities Last']) ? $jfo['Total Current Liabilities Last']['amount'] : 0,
            );

            $a = array();
            $b = array();
            $c = array();
            $zohoResponse = array();
            $searchItems = array();
            $db = new reportClientApplicationMap();
            $searchItems['client_id'] = $client_id;
            $searchItems['report_id'] = 2;
            $searchItems['auth_key'] = "";
            $response = $db->getAllReportClientApplicationMaps($searchItems);

            if (sizeof($response) > 0) {
                if ($response["error"] == false) {
                    $currencyResult = array();
                    $currencyResult = $this->getFieldByID('client_master_detail', 'client_master_id', $client_master_id, 'default_currency');

                    $application_id = $response["reportClientApplicationMapDetails"][0]["application_id"];
                    $a[] = $this->getWeightedPipeLine($response["reportClientApplicationMapDetails"], $client_id, $application_id, $currencyResult);
                    $b[] = $this->getSalesOrders($response["reportClientApplicationMapDetails"], $client_id, $application_id, $currencyResult, $client_master_id);

                    if ((sizeof($a) > 0) && sizeof($b) > 0) {
                        if ($a[0]["error"] == false && $b[0]["error"] == false) {
                            $c[] = $this->getTotalOfSales($a, $b);
                        } else {
                            $c["error"] = true;
                            $c["message"] = SERVER_NO_RESPONSE;
                            return $c;
                        }
                    } else {
                        $c["error"] = true;
                        $c["message"] = SERVER_NO_RESPONSE;
                        return $c;
                    }

                    $zohoResponse = array_merge($a, $b, $c);
                    if (sizeof($zohoResponse) > 0) {
                        $total_sales_orders = 0;
                        foreach ($zohoResponse[2]['totalSalesOrder'] as $key => $value) {
                            foreach ($value as $key => $val) {
                                if ($key == 'actual_amount') {
                                    $total_sales_orders = $total_sales_orders + $val;
                                }

                            }
                        }
                        $budget_track_result["sales"]["total_sales_orders"] = $total_sales_orders;

                        $total_pipeline = 0;
                        foreach ($zohoResponse[0]['pipelineReport'] as $key => $value) {
                            foreach ($value as $key => $val) {
                                if ($key == 'actual_amount') {
                                    $total_pipeline = $total_pipeline + $val;
                                }

                            }
                        }
                        $budget_track_result["sales"]["total_pipeline"] = $total_pipeline;

                        $total_books = 0;
                        $target = 0;
                        foreach ($zohoResponse[1]['salesOrderReport'] as $key => $value) {
                            foreach ($value as $key => $val) {
                                if ($key == 'actual_amount') {
                                    $total_books = $total_books + $val;
                                } elseif ($key == 'budget') {
                                    $target = $target + $val;
                                }

                            }
                        }
                        $budget_track_result["sales"]["total_books"] = $total_books;
                        $budget_track_result["sales"]["target"] = $target;
                    }
                    $budget_track_result["sales"]["isavailable"] = true;
                } else {
                    $budget_track_result["sales"]["isavailable"] = false;
                }
            } else {
                $budget_track_result["sales"]["isavailable"] = false;
            }

            if (count($budget_track_result) > 0) {
                $budget_track_result["error"] = false;
                $finalResponse = json_encode($budget_track_result);
            } else {
                $finalResponse = json_encode(array('error' => true, 'message' => NO_RECORD_FOUND));
            }
            return $finalResponse;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }
    public function cronCheck($client_master_id){
        try{
            $this->conn->autocommit(false);
            $date = date("Y-m-d H:i:s");
            $sql = "UPDATE cron_jobs SET end_time = :end_time, status = :status  WHERE client_master_id = :client_master_id";
            $statement = $this->conn->prepare($sql);
            $statement->bindValue(":end_time", $date);
            $statement->bindValue(":status",0);
            $statement->bindValue(":client_master_id",$client_master_id);
            $count = $statement->execute();
            $this->conn->commit();

        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }
    /**
     * close the database connection
     */
    public function __destruct()
    {
        // close the database connection
        $this->db->closeconnection();
    }

}
