<?php

class flashMerger
{

    private $files;

    public function __construct(array $files)
    {
        $this->files = $files;
    }

    public function agedPayablesReadJson($path, $flasCur)
    {
        $date = date('d-m-Y');
        $endDate = date("d-m-Y", strtotime($date));
        $startDate = date('d-m-Y', strtotime("-3 months"));

        $date = array();
        $i = 0;
        while (strtotime($startDate) <= strtotime($endDate)) {
            $date[$i] = date('F', strtotime($startDate));
            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            $i++;
        }
        $finalDate = array_reverse($date);

        $jsondata = json_decode(file_get_contents($path), true);
        foreach ($jsondata[2]['agedPayables'] as $key => $value) {
            foreach ($value as $key1 => $v1) {
                $jsondata[2]['agedPayables'][$key][$key1]['Older'] = $this->amountCal($v1['Older'], $flasCur);
                $jsondata[2]['agedPayables'][$key][$key1]['Total'] = $this->amountCal($v1['Total'], $flasCur);
                $jsondata[2]['agedPayables'][$key][$key1][$finalDate[3]] = $this->amountCal($v1[$finalDate[3]], $flasCur);
                $jsondata[2]['agedPayables'][$key][$key1][$finalDate[2]] = $this->amountCal($v1[$finalDate[2]], $flasCur);
                $jsondata[2]['agedPayables'][$key][$key1][$finalDate[1]] = $this->amountCal($v1[$finalDate[1]], $flasCur);
                $jsondata[2]['agedPayables'][$key][$key1][$finalDate[0]] = $this->amountCal($v1[$finalDate[0]], $flasCur);
            }
        }

        return $jsondata;
    }

    public function agedReceivablesReadJson($path, $flasCur)
    {
        $date = date('d-m-Y');
        $endDate = date("d-m-Y", strtotime($date));
        $startDate = date('d-m-Y', strtotime("-3 months"));

        $date = array();
        $i = 0;
        while (strtotime($startDate) <= strtotime($endDate)) {
            $date[$i] = date('F', strtotime($startDate));
            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            $i++;
        }
        $finalDate = array_reverse($date);

        $jsondata = json_decode(file_get_contents($path), true);

        foreach ($jsondata[3]['agedReceivables'] as $key => $value) {
            foreach ($value as $key1 => $v1) {
                $jsondata[3]['agedReceivables'][$key][$key1]['Older'] = $this->amountCal($v1['Older'], $flasCur);
                $jsondata[3]['agedReceivables'][$key][$key1]['Total'] = $this->amountCal($v1['Total'], $flasCur);
                $jsondata[3]['agedReceivables'][$key][$key1][$finalDate[3]] = $this->amountCal($v1[$finalDate[3]], $flasCur);
                $jsondata[3]['agedReceivables'][$key][$key1][$finalDate[2]] = $this->amountCal($v1[$finalDate[2]], $flasCur);
                $jsondata[3]['agedReceivables'][$key][$key1][$finalDate[1]] = $this->amountCal($v1[$finalDate[1]], $flasCur);
                $jsondata[3]['agedReceivables'][$key][$key1][$finalDate[0]] = $this->amountCal($v1[$finalDate[0]], $flasCur);
            }
        }
        return $jsondata;
    }

    public function estimateReadJson($path, $flasCur)
    {

        $jsondata = json_decode(file_get_contents($path), true);

        foreach ($jsondata[4]['estimatedPayroll'] as $key => $value) {
            foreach ($value as $k1 => $v1) {
                if (is_numeric(($v1[1]['Value']))) {
                    $value = $v1[1]['Value'];
                    $jsondata[4]['estimatedPayroll'][$key][$k1][1]['Value'] = $this->amountCal($value, $flasCur);
                }
            }
        }

        return $jsondata;
    }

    public function flashReadJson($path, $flasCur)
    {

        $jsondata = json_decode(file_get_contents($path), true);
        foreach ($jsondata[1]['accountsSummary'] as $key => $value) {
            if (is_array($value)) {
                $jsondata[1]['accountsSummary'][$key]['closing_balance'] = $this->amountCal($value['closing_balance'], $flasCur);
            }
        }

        foreach ($jsondata[7]['BankTransactions'] as $key => $value) {
            foreach ($value as $k1 => $v1) {
                foreach ($v1 as $k2 => $v2) {
                    $jsondata[7]['BankTransactions'][$key][$k1][$k2] = $v2;
                }
            }
        }
        return $jsondata;
    }

    public function amountCal($keyValue, $cur)
    {
        $finalAmt = $keyValue / $cur;
        return $finalAmt;
    }

    //flash json
    public function flashIndex($group_id, $flashCur)
    {

        $files = $this->files;
        if (count($files) == 10) {
            $path1 = $files[0];
            $json1 = $this->agedPayablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedPayablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedPayablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedPayablesReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->agedPayablesReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->agedPayablesReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->agedPayablesReadJson($path7, $flashCur[6]);
            $path8 = $files[7];
            $json8 = $this->agedPayablesReadJson($path8, $flashCur[7]);
            $path9 = $files[8];
            $json9 = $this->agedPayablesReadJson($path9, $flashCur[8]);
            $path10 = $files[9];
            $json10 = $this->agedPayablesReadJson($path10, $flashCur[9]);

            $collection = collect($json1[2]['agedPayables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[2]['agedPayables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[2]['agedPayables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[2]['agedPayables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[2]['agedPayables']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[2]['agedPayables']);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7[2]['agedPayables']);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8[2]['agedPayables']);
            $collapsed8 = $collection8->collapse()->toArray();

            $collection9 = collect($json9[2]['agedPayables']);
            $collapsed9 = $collection9->collapse()->toArray();

            $collection10 = collect($json10[2]['agedPayables']);
            $collapsed10 = $collection10->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8, $collapsed9, $collapsed10);
        }
        if (count($files) == 9) {
            $path1 = $files[0];
            $json1 = $this->agedPayablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedPayablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedPayablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedPayablesReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->agedPayablesReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->agedPayablesReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->agedPayablesReadJson($path7, $flashCur[6]);
            $path8 = $files[7];
            $json8 = $this->agedPayablesReadJson($path8, $flashCur[7]);
            $path9 = $files[8];
            $json9 = $this->agedPayablesReadJson($path9, $flashCur[8]);

            $collection = collect($json1[2]['agedPayables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[2]['agedPayables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[2]['agedPayables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[2]['agedPayables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[2]['agedPayables']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[2]['agedPayables']);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7[2]['agedPayables']);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8[2]['agedPayables']);
            $collapsed8 = $collection8->collapse()->toArray();

            $collection9 = collect($json9[2]['agedPayables']);
            $collapsed9 = $collection9->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8, $collapsed9);
        }
        if (count($files) == 8) {
            $path1 = $files[0];
            $json1 = $this->agedPayablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedPayablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedPayablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedPayablesReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->agedPayablesReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->agedPayablesReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->agedPayablesReadJson($path7, $flashCur[6]);
            $path8 = $files[7];
            $json8 = $this->agedPayablesReadJson($path8, $flashCur[7]);

            $collection = collect($json1[2]['agedPayables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[2]['agedPayables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[2]['agedPayables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[2]['agedPayables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[2]['agedPayables']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[2]['agedPayables']);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7[2]['agedPayables']);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8[2]['agedPayables']);
            $collapsed8 = $collection8->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8);
        }
        if (count($files) == 7) {
            $path1 = $files[0];
            $json1 = $this->agedPayablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedPayablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedPayablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedPayablesReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->agedPayablesReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->agedPayablesReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->agedPayablesReadJson($path7, $flashCur[6]);

            $collection = collect($json1[2]['agedPayables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[2]['agedPayables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[2]['agedPayables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[2]['agedPayables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[2]['agedPayables']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[2]['agedPayables']);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7[2]['agedPayables']);
            $collapsed7 = $collection7->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7);
        }
        if (count($files) == 6) {
            $path1 = $files[0];
            $json1 = $this->agedPayablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedPayablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedPayablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedPayablesReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->agedPayablesReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->agedPayablesReadJson($path6, $flashCur[5]);

            $collection = collect($json1[2]['agedPayables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[2]['agedPayables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[2]['agedPayables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[2]['agedPayables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[2]['agedPayables']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[2]['agedPayables']);
            $collapsed6 = $collection6->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6);
        }
        if (count($files) == 5) {
            $path1 = $files[0];
            $json1 = $this->agedPayablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedPayablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedPayablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedPayablesReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->agedPayablesReadJson($path5, $flashCur[4]);

            $collection = collect($json1[2]['agedPayables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[2]['agedPayables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[2]['agedPayables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[2]['agedPayables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[2]['agedPayables']);
            $collapsed5 = $collection5->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5);
        }
        if (count($files) == 4) {
            $path1 = $files[0];
            $json1 = $this->agedPayablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedPayablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedPayablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedPayablesReadJson($path4, $flashCur[3]);

            $collection = collect($json1[2]['agedPayables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[2]['agedPayables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[2]['agedPayables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[2]['agedPayables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4);
        }
        if (count($files) == 3) {
            $path1 = $files[0];
            $json1 = $this->agedPayablesReadJson($path1, $flashCur[0]);

            $path2 = $files[1];
            $json2 = $this->agedPayablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedPayablesReadJson($path3, $flashCur[2]);

            $collection = collect($json1[2]['agedPayables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[2]['agedPayables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[2]['agedPayables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3);

        }
        if (count($files) == 2) {

            $path1 = $files[0];
            $json1 = $this->agedPayablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedPayablesReadJson($path2, $flashCur[1]);

            $collection = collect($json1[2]['agedPayables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[2]['agedPayables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2);

        }
        if (count($files) == 1) {
            $path1 = $files[0];
            $json1 = $this->agedPayablesReadJson($path1, $flashCur[0]);
            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $ry = $collapsed;
        }
        return $ry;
    }

    // receivable json
    public function flashRecivableIndex($group_id, $flashCur)
    {
        $files = $this->files;
        if (count($files) == 10) {
            $path1 = $files[0];
            $json1 = $this->agedReceivablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedReceivablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedReceivablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedReceivablesReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->agedReceivablesReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->agedReceivablesReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->agedReceivablesReadJson($path7, $flashCur[6]);
            $path8 = $files[7];
            $json8 = $this->agedReceivablesReadJson($path8, $flashCur[7]);
            $path9 = $files[8];
            $json9 = $this->agedReceivablesReadJson($path9, $flashCur[8]);
            $path10 = $files[9];
            $json10 = $this->agedReceivablesReadJson($path10, $flashCur[9]);

            $collection = collect($json1[3]['agedReceivables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[3]['agedReceivables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[3]['agedReceivables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[3]['agedReceivables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[3]['agedReceivables']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[3]['agedReceivables']);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7[3]['agedReceivables']);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8[3]['agedReceivables']);
            $collapsed8 = $collection8->collapse()->toArray();

            $collection9 = collect($json9[3]['agedReceivables']);
            $collapsed9 = $collection9->collapse()->toArray();

            $collection10 = collect($json10[3]['agedReceivables']);
            $collapsed10 = $collection10->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8, $collapsed9, $collapsed10);
        }
        if (count($files) == 9) {
            $path1 = $files[0];
            $json1 = $this->agedReceivablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedReceivablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedReceivablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedReceivablesReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->agedReceivablesReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->agedReceivablesReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->agedReceivablesReadJson($path7, $flashCur[6]);
            $path8 = $files[7];
            $json8 = $this->agedReceivablesReadJson($path8, $flashCur[7]);
            $path9 = $files[8];
            $json9 = $this->agedReceivablesReadJson($path9, $flashCur[8]);

            $collection = collect($json1[3]['agedReceivables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[3]['agedReceivables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[3]['agedReceivables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[3]['agedReceivables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[3]['agedReceivables']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[3]['agedReceivables']);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7[3]['agedReceivables']);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8[3]['agedReceivables']);
            $collapsed8 = $collection8->collapse()->toArray();

            $collection9 = collect($json9[3]['agedReceivables']);
            $collapsed9 = $collection9->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8, $collapsed9);
        }
        if (count($files) == 8) {
            $path1 = $files[0];
            $json1 = $this->agedReceivablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedReceivablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedReceivablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedReceivablesReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->agedReceivablesReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->agedReceivablesReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->agedReceivablesReadJson($path7, $flashCur[6]);
            $path8 = $files[7];
            $json8 = $this->agedReceivablesReadJson($path8, $flashCur[7]);

            $collection = collect($json1[3]['agedReceivables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[3]['agedReceivables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[3]['agedReceivables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[3]['agedReceivables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[3]['agedReceivables']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[3]['agedReceivables']);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7[3]['agedReceivables']);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8[3]['agedReceivables']);
            $collapsed8 = $collection8->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8);
        }
        if (count($files) == 7) {
            $path1 = $files[0];
            $json1 = $this->agedReceivablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedReceivablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedReceivablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedReceivablesReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->agedReceivablesReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->agedReceivablesReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->agedReceivablesReadJson($path7, $flashCur[6]);

            $collection = collect($json1[3]['agedReceivables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[3]['agedReceivables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[3]['agedReceivables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[3]['agedReceivables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[3]['agedReceivables']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[3]['agedReceivables']);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7[3]['agedReceivables']);
            $collapsed7 = $collection7->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7);
        }
        if (count($files) == 6) {
            $path1 = $files[0];
            $json1 = $this->agedReceivablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedReceivablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedReceivablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedReceivablesReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->agedReceivablesReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->agedReceivablesReadJson($path6, $flashCur[5]);

            $collection = collect($json1[3]['agedReceivables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[3]['agedReceivables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[3]['agedReceivables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[3]['agedReceivables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[3]['agedReceivables']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[3]['agedReceivables']);
            $collapsed6 = $collection6->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6);
        }
        if (count($files) == 5) {
            $path1 = $files[0];
            $json1 = $this->agedReceivablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedReceivablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedReceivablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedReceivablesReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->agedReceivablesReadJson($path5, $flashCur[4]);

            $collection = collect($json1[3]['agedReceivables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[3]['agedReceivables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[3]['agedReceivables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[3]['agedReceivables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[3]['agedReceivables']);
            $collapsed5 = $collection5->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5);
        }
        if (count($files) == 4) {
            $path1 = $files[0];
            $json1 = $this->agedReceivablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedReceivablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedReceivablesReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->agedReceivablesReadJson($path4, $flashCur[3]);

            $collection = collect($json1[3]['agedReceivables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[3]['agedReceivables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[3]['agedReceivables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[3]['agedReceivables']);
            $collapsed4 = $collection4->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4);
        }
        if (count($files) == 3) {
            $path1 = $files[0];
            $json1 = $this->agedReceivablesReadJson($path1, $flashCur[0]);

            $path2 = $files[1];
            $json2 = $this->agedReceivablesReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->agedReceivablesReadJson($path3, $flashCur[2]);

            $collection = collect($json1[3]['agedReceivables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[3]['agedReceivables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[3]['agedReceivables']);
            $collapsed3 = $collection3->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3);

        }
        if (count($files) == 2) {

            $path1 = $files[0];
            $json1 = $this->agedReceivablesReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->agedReceivablesReadJson($path2, $flashCur[1]);

            $collection = collect($json1[3]['agedReceivables']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[3]['agedReceivables']);
            $collapsed2 = $collection2->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2);

        }
        if (count($files) == 1) {
            $path1 = $files[0];
            $json1 = $this->agedReceivablesReadJson($path1, $flashCur[0]);
            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $ry = $collapsed;
        }
        return $ry;
    }

    public function flashEstimatePayrollIndex($group_id, $flashCur)
    {
        $files = $this->files;
        if (count($files) == 10) {
            $path1 = $files[0];
            $json1 = $this->estimateReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->estimateReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->estimateReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->estimateReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->estimateReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->estimateReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->estimateReadJson($path7, $flashCur[6]);
            $path8 = $files[7];
            $json8 = $this->estimateReadJson($path8, $flashCur[7]);
            $path9 = $files[8];
            $json9 = $this->estimateReadJson($path9, $flashCur[8]);
            $path10 = $files[9];
            $json10 = $this->estimateReadJson($path10, $flashCur[9]);

            $collection = collect($json1[4]['estimatedPayroll']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[4]['estimatedPayroll']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[4]['estimatedPayroll']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[4]['estimatedPayroll']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[4]['estimatedPayroll']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[4]['estimatedPayroll']);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7[4]['estimatedPayroll']);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8[4]['estimatedPayroll']);
            $collapsed8 = $collection8->collapse()->toArray();

            $collection9 = collect($json9[4]['estimatedPayroll']);
            $collapsed9 = $collection9->collapse()->toArray();

            $collection10 = collect($json10[4]['estimatedPayroll']);
            $collapsed10 = $collection10->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8, $collapsed9, $collapsed10);
        }
        if (count($files) == 9) {
            $path1 = $files[0];
            $json1 = $this->estimateReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->estimateReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->estimateReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->estimateReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->estimateReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->estimateReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->estimateReadJson($path7, $flashCur[6]);
            $path8 = $files[7];
            $json8 = $this->estimateReadJson($path8, $flashCur[7]);
            $path9 = $files[8];
            $json9 = $this->estimateReadJson($path9, $flashCur[8]);

            $collection = collect($json1[4]['estimatedPayroll']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[4]['estimatedPayroll']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[4]['estimatedPayroll']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[4]['estimatedPayroll']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[4]['estimatedPayroll']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[4]['estimatedPayroll']);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7[4]['estimatedPayroll']);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8[4]['estimatedPayroll']);
            $collapsed8 = $collection8->collapse()->toArray();

            $collection9 = collect($json9[4]['estimatedPayroll']);
            $collapsed9 = $collection9->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8, $collapsed9);
        }
        if (count($files) == 8) {
            $path1 = $files[0];
            $json1 = $this->estimateReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->estimateReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->estimateReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->estimateReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->estimateReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->estimateReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->estimateReadJson($path7, $flashCur[6]);
            $path8 = $files[7];
            $json8 = $this->estimateReadJson($path8, $flashCur[7]);

            $collection = collect($json1[4]['estimatedPayroll']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[4]['estimatedPayroll']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[4]['estimatedPayroll']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[4]['estimatedPayroll']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[4]['estimatedPayroll']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[4]['estimatedPayroll']);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7[4]['estimatedPayroll']);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8[4]['estimatedPayroll']);
            $collapsed8 = $collection8->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8);
        }
        if (count($files) == 7) {
            $path1 = $files[0];
            $json1 = $this->estimateReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->estimateReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->estimateReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->estimateReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->estimateReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->estimateReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->estimateReadJson($path7, $flashCur[6]);

            $collection = collect($json1[4]['estimatedPayroll']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[4]['estimatedPayroll']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[4]['estimatedPayroll']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[4]['estimatedPayroll']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[4]['estimatedPayroll']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[4]['estimatedPayroll']);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7[4]['estimatedPayroll']);
            $collapsed7 = $collection7->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7);
        }
        if (count($files) == 6) {
            $path1 = $files[0];
            $json1 = $this->estimateReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->estimateReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->estimateReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->estimateReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->estimateReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->estimateReadJson($path6, $flashCur[5]);

            $collection = collect($json1[4]['estimatedPayroll']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[4]['estimatedPayroll']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[4]['estimatedPayroll']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[4]['estimatedPayroll']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[4]['estimatedPayroll']);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6[4]['estimatedPayroll']);
            $collapsed6 = $collection6->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6);
        }
        if (count($files) == 5) {
            $path1 = $files[0];
            $json1 = $this->estimateReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->estimateReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->estimateReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->estimateReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->estimateReadJson($path5, $flashCur[4]);

            $collection = collect($json1[4]['estimatedPayroll']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[4]['estimatedPayroll']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[4]['estimatedPayroll']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[4]['estimatedPayroll']);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5[4]['estimatedPayroll']);
            $collapsed5 = $collection5->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5);
        }
        if (count($files) == 4) {
            $path1 = $files[0];
            $json1 = $this->estimateReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->estimateReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->estimateReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->estimateReadJson($path4, $flashCur[3]);

            $collection = collect($json1[4]['estimatedPayroll']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[4]['estimatedPayroll']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[4]['estimatedPayroll']);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4[4]['estimatedPayroll']);
            $collapsed4 = $collection4->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4);
        }
        if (count($files) == 3) {
            $path1 = $files[0];
            $json1 = $this->estimateReadJson($path1, $flashCur[0]);

            $path2 = $files[1];
            $json2 = $this->estimateReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->estimateReadJson($path3, $flashCur[2]);

            $collection = collect($json1[4]['estimatedPayroll']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[4]['estimatedPayroll']);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3[4]['estimatedPayroll']);
            $collapsed3 = $collection3->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3);

        }
        if (count($files) == 2) {

            $path1 = $files[0];
            $json1 = $this->estimateReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->estimateReadJson($path2, $flashCur[1]);

            $collection = collect($json1[4]['estimatedPayroll']);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2[4]['estimatedPayroll']);
            $collapsed2 = $collection2->collapse()->toArray();

            $ry['estimatedPayroll'] = array_merge_recursive($collapsed, $collapsed2);

        }
        if (count($files) == 1) {
            $path1 = $files[0];
            $json1 = $this->estimateReadJson($path1, $flashCur[0]);
            $collection = collect($json1[4]['estimatedPayroll']);
            $collapsed = $collection->collapse()->toArray();

            $ry = $collapsed;
        }
        return $ry;
    }

    public function flashBankTransactionIndex($group_id, $flashCur)
    {
        $files = $this->files;
        if (count($files) == 10) {
            $path1 = $files[0];
            $json1 = $this->flashReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->flashReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->flashReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->flashReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->flashReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->flashReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->flashReadJson($path7, $flashCur[6]);
            $path8 = $files[7];
            $json8 = $this->flashReadJson($path8, $flashCur[7]);
            $path9 = $files[8];
            $json9 = $this->flashReadJson($path9, $flashCur[8]);
            $path10 = $files[9];
            $json10 = $this->flashReadJson($path10, $flashCur[9]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8);
            $collapsed8 = $collection8->collapse()->toArray();

            $collection9 = collect($json9);
            $collapsed9 = $collection9->collapse()->toArray();

            $collection10 = collect($json10);
            $collapsed10 = $collection10->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8, $collapsed9, $collapsed10);
        }
        if (count($files) == 9) {
            $path1 = $files[0];
            $json1 = $this->flashReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->flashReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->flashReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->flashReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->flashReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->flashReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->flashReadJson($path7, $flashCur[6]);
            $path8 = $files[7];
            $json8 = $this->flashReadJson($path8, $flashCur[7]);
            $path9 = $files[8];
            $json9 = $this->flashReadJson($path9, $flashCur[8]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8);
            $collapsed8 = $collection8->collapse()->toArray();

            $collection9 = collect($json9);
            $collapsed9 = $collection9->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8, $collapsed9);
        }
        if (count($files) == 8) {
            $path1 = $files[0];
            $json1 = $this->flashReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->flashReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->flashReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->flashReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->flashReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->flashReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->flashReadJson($path7, $flashCur[6]);
            $path8 = $files[7];
            $json8 = $this->flashReadJson($path8, $flashCur[7]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8);
            $collapsed8 = $collection8->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8);
        }
        if (count($files) == 7) {
            $path1 = $files[0];
            $json1 = $this->flashReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->flashReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->flashReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->flashReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->flashReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->flashReadJson($path6, $flashCur[5]);
            $path7 = $files[6];
            $json7 = $this->flashReadJson($path7, $flashCur[6]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7);
            $collapsed7 = $collection7->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7);
        }
        if (count($files) == 6) {
            $path1 = $files[0];
            $json1 = $this->flashReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->flashReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->flashReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->flashReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->flashReadJson($path5, $flashCur[4]);
            $path6 = $files[5];
            $json6 = $this->flashReadJson($path6, $flashCur[5]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6);
        }
        if (count($files) == 5) {
            $path1 = $files[0];
            $json1 = $this->flashReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->flashReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->flashReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->flashReadJson($path4, $flashCur[3]);
            $path5 = $files[4];
            $json5 = $this->flashReadJson($path5, $flashCur[4]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5);
        }
        if (count($files) == 4) {
            $path1 = $files[0];
            $json1 = $this->flashReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->flashReadJson($path2, $flashCur[1]);
            $path3 = $files[2];
            $json3 = $this->flashReadJson($path3, $flashCur[2]);
            $path4 = $files[3];
            $json4 = $this->flashReadJson($path4, $flashCur[3]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4);
        }
        if (count($files) == 3) {
            $path1 = $files[0];
            $json1 = $this->flashReadJson($path1, $flashCur[0]);

            $path2 = $files[1];
            $json2 = $this->flashReadJson($path2, $flashCur[1]);

            $path3 = $files[2];
            $json3 = $this->flashReadJson($path3, $flashCur[2]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3);

        }
        if (count($files) == 2) {

            $path1 = $files[0];
            $json1 = $this->flashReadJson($path1, $flashCur[0]);
            $path2 = $files[1];
            $json2 = $this->flashReadJson($path2, $flashCur[1]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2);

        }
        if (count($files) == 1) {
            $path1 = $files[0];
            $json1 = $this->flashReadJson($path1, $flashCur[0]);
            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $ry = $collapsed;
        }
        return $ry;
    }

    public function Agedpayables($mergerValues)
    {
        $date = date('d-m-Y');
        $endDate = date("d-m-Y", strtotime($date));
        $startDate = date('d-m-Y', strtotime("-3 months"));

        $date = array();
        $i = 0;
        while (strtotime($startDate) <= strtotime($endDate)) {
            $date[$i] = date('F', strtotime($startDate));
            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            $i++;
        }

        $finalDate = array_reverse($date);
        foreach ($mergerValues as $key => $value) {
            if (sizeof($mergerValues[$key]['Total']) > 1) {
                $mergerValues[$key]['Name'] = $value['Name'][0];
                $mergerValues[$key]['Older'] = 0;
                foreach ($value['Older'] as $v0) {
                    $mergerValues[$key]['Older'] += $v0;
                }

                $mergerValues[$key][$finalDate[3]] = 0;
                foreach ($value[$finalDate[3]] as $v1) {
                    $mergerValues[$key][$finalDate[3]] += $v1;
                }

                $mergerValues[$key][$finalDate[2]] = 0;
                foreach ($value[$finalDate[2]] as $v2) {
                    $mergerValues[$key][$finalDate[2]] += $v2;
                }

                $mergerValues[$key][$finalDate[1]] = 0;
                foreach ($value[$finalDate[1]] as $v3) {
                    $mergerValues[$key][$finalDate[1]] += $v3;
                }

                $mergerValues[$key][$finalDate[0]] = 0;
                foreach ($value[$finalDate[0]] as $v4) {
                    $mergerValues[$key][$finalDate[0]] += $v4;
                }

                $mergerValues[$key]['Total'] = 0;
                foreach ($value['Total'] as $v5) {
                    $mergerValues[$key]['Total'] += $v5;
                }
            } else {
                $mergerValues[$key]['Name'] = $value['Name'];
                $mergerValues[$key]['Older'] = $value['Older'];
                $mergerValues[$key][$finalDate[3]] = $value[$finalDate[3]];
                $mergerValues[$key][$finalDate[2]] = $value[$finalDate[2]];
                $mergerValues[$key][$finalDate[1]] = $value[$finalDate[1]];
                $mergerValues[$key][$finalDate[0]] = $value[$finalDate[0]];
            }
        }
        $mergerValues1 = $mergerValues;

        return $mergerValues1;

    }
    public function Agedrecivables($mergerValues)
    {

        $date = date('d-m-Y');
        $endDate = date("d-m-Y", strtotime($date));
        $startDate = date('d-m-Y', strtotime("-3 months"));

        $date = array();
        $i = 0;
        while (strtotime($startDate) <= strtotime($endDate)) {
            $date[$i] = date('F', strtotime($startDate));
            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            $i++;
        }

        $finalDate = array_reverse($date);
        foreach ($mergerValues as $key => $value) {
            if (sizeof($mergerValues[$key]['Total']) > 1) {
                $mergerValues[$key]['Name'] = $value['Name'][0];
                $mergerValues[$key]['Older'] = 0;
                foreach ($value['Older'] as $v0) {
                    $mergerValues[$key]['Older'] += $v0;
                }

                $mergerValues[$key][$finalDate[3]] = 0;
                foreach ($value[$finalDate[3]] as $v1) {
                    $mergerValues[$key][$finalDate[3]] += $v1;
                }

                $mergerValues[$key][$finalDate[2]] = 0;
                foreach ($value[$finalDate[2]] as $v2) {
                    $mergerValues[$key][$finalDate[2]] += $v2;
                }

                $mergerValues[$key][$finalDate[1]] = 0;
                foreach ($value[$finalDate[1]] as $v3) {
                    $mergerValues[$key][$finalDate[1]] += $v3;
                }

                $mergerValues[$key][$finalDate[0]] = 0;
                foreach ($value[$finalDate[0]] as $v4) {
                    $mergerValues[$key][$finalDate[0]] += $v4;
                }

                $mergerValues[$key]['Total'] = 0;
                foreach ($value['Total'] as $v5) {
                    $mergerValues[$key]['Total'] += $v5;
                }
            } else {
                $mergerValues[$key]['Name'] = $value['Name'];
                $mergerValues[$key]['Older'] = $value['Older'];
                $mergerValues[$key][$finalDate[3]] = $value[$finalDate[3]];
                $mergerValues[$key][$finalDate[2]] = $value[$finalDate[2]];
                $mergerValues[$key][$finalDate[1]] = $value[$finalDate[1]];
                $mergerValues[$key][$finalDate[0]] = $value[$finalDate[0]];
            }
        }
        $mergerValues1 = $mergerValues;
        return $mergerValues1;

    }

    public function Accountsummary($value)
    {
        $data = $value['accountsSummary'];
        $b = array();
        foreach ($data as $key => $value) {
            if (!is_numeric($key) && $key != "org_name") {

                if (sizeof($value['closing_balance']) > 1) {
                    $val = 0;
                    foreach ($value['closing_balance'] as $value) {
                        $val += $value;
                        $data1[$key]['bank_account_name'] = $key;
                        $data1[$key]['closing_balance'] = $val;
                    }

                } else {
                    $data1[$key]['bank_account_name'] = $key;
                    $data1[$key]['closing_balance'] = $value['closing_balance'];
                }
            }
        }
        $finalArr = $data1;
        return $finalArr;
    }

    public function EstimatepayRoll($value)
    {
        if ($value['estimatedPayroll']) {
            $data = $value['estimatedPayroll'];
        } else {
            $data = $value;
        }

        $b = array();
        foreach ($data as $key => $value) {

            if (!is_numeric($key)) {
                if (isset($value[0]['Value'])) {
                    $val = 0;
                    foreach ($value as $v) {
                        if (is_numeric($v['Value'])) {
                            $val += $v['Value'];
                            $data1[$key][0]['Value'] = $value[0]['Value'];
                            $data1[$key][0]['Attributes'] = $v['Attributes'];
                            $data1[$key][1]['Value'] = $val;
                            $data1[$key][1]['Attributes'] = $v['Attributes'];
                        }
                    }
                }
            } else {
                $data1[$key][0]['Value'] = $value[0]['Value'];
                $data1[$key][1]['Value'] = $value[1]['Value'];
            }
        }

        $finalArr = $data1;
        return $finalArr;
    }

    public function BankRowValues($value)
    {
        $data = $value['BankTransactions']['items'];
        foreach ($data as $key => $value) {
            if (sizeof($value) > 11) {
                $array_divide = array_chunk($value, 11);
                $aar['1'] = 0;
                $aar['2'] = 0;
                $aar['3'] = 0;
                $aar['4'] = 0;
                $aar['5'] = 0;
                $aar['6'] = 0;
                $aar['7'] = 0;
                $aar['8'] = 0;
                $aar['9'] = 0;
                $aar['10'] = 0;
                $aar['11'] = 0;
                foreach ($array_divide as $value) {
                    $aar['1'] += $value[0];
                    $aar['2'] += $value[1];
                    $aar['3'] += $value[2];
                    $aar['4'] += $value[3];
                    $aar['5'] += $value[4];
                    $aar['6'] += $value[5];
                    $aar['7'] += $value[6];
                    $aar['8'] += $value[7];
                    $aar['9'] += $value[8];
                    $aar['10'] += $value[9];
                    $aar['11'] += $value[10];
                    $data[$key] = $aar;
                }

            } else {
                $data[$key] = $value;
            }
            $finalArr['items'] = $data;
        }
        return $finalArr;

    }

    public function bankTransaction($mergerValues)
    {
        $group = new group();
        $bankTransaction = $mergerValues['BankTransactions']['Number of Total Transactions']['summaryRow'];
        $array_divide = array_chunk($bankTransaction, 11);
        $value0 = 0;
        $value1 = 0;
        $value2 = 0;
        $value3 = 0;
        $value4 = 0;
        $value5 = 0;
        $value6 = 0;
        $value7 = 0;
        $value8 = 0;
        $value9 = 0;
        $value10 = 0;
        foreach ($array_divide as $value) {
            $value0 += $value[0];
            $value1 += $value[1];
            $value2 += $value[2];
            $value3 += $value[3];
            $value4 += $value[4];
            $value5 += $value[5];
            $value6 += $value[6];
            $value7 += $value[7];
            $value8 += $value[8];
            $value9 += $value[9];
            $value10 += $value[10];
        }

        $final_trans = '{';
        $final_trans .= '"1":"' . round($value0) . '",' . PHP_EOL;
        $final_trans .= '"2":"' . round($value1) . '",' . PHP_EOL;
        $final_trans .= '"3":"' . round($value2) . '",' . PHP_EOL;
        $final_trans .= '"4":"' . round($value3) . '",' . PHP_EOL;
        $final_trans .= '"5":"' . round($value4) . '",' . PHP_EOL;
        $final_trans .= '"6":"' . round($value5) . '",' . PHP_EOL;
        $final_trans .= '"7":"' . round($value6) . '",' . PHP_EOL;
        $final_trans .= '"8":"' . round($value7) . '",' . PHP_EOL;
        $final_trans .= '"9":"' . round($value8) . '",' . PHP_EOL;
        $final_trans .= '"10":"' . round($value9) . '",' . PHP_EOL;
        $final_trans .= '"11":"' . round($value10) . '"' . PHP_EOL;
        $final_trans .= '}';
        return $final_trans;
    }

    public function finalFlashRecord($accountsSummary, $payable, $recivable, $estimatedPayroll, $bankTransaction, $bankTransactionSummary, $group_id, $toCur)
    {

        $flash_total_array = '{';

        $flash_total_array .= '"0":{';
        $flash_total_array .= '"territorybrand": [';
        $flash_total_array .= ']';
        $flash_total_array .= '},';

        $flash_total_array .= '"1":{';
        $flash_total_array .= '"accountsSummary":';
        $flash_total_array .= json_encode($accountsSummary);
        $flash_total_array .= '},';

        $flash_total_array .= '"2":{';
        $flash_total_array .= '"agedPayables":[';
        $flash_total_array .= json_encode($payable);
        $flash_total_array .= ']';
        $flash_total_array .= '},';

        $flash_total_array .= '"3":{';
        $flash_total_array .= '"agedReceivables":[';
        $flash_total_array .= json_encode($recivable);
        $flash_total_array .= ']';
        $flash_total_array .= '},';

        $flash_total_array .= '"4":{';
        $flash_total_array .= '"estimatedPayroll":[';
        $flash_total_array .= json_encode($estimatedPayroll);
        $flash_total_array .= ']';
        $flash_total_array .= '},';

        $flash_total_array .= '"5":{';
        $flash_total_array .= '"currencyList": [';
        $flash_total_array .= ']';
        $flash_total_array .= '},';

        $flash_total_array .= '"6":{';
        $flash_total_array .= '"orgDetail": [';
        $flash_total_array .= ']';
        $flash_total_array .= '},';

        $bankmergedata = json_encode($bankTransaction['items']);
        $bankmergedata .= ',';
        $bankmergedata .= '"Number of Total Transactions": {';
        $bankmergedata .= '"summaryRow": ';
        $bankmergedata .= $bankTransactionSummary;
        $bankmergedata .= '}';

        $flash_total_array .= '"7":{';
        $flash_total_array .= '"BankTransactions":{"items":';
        $flash_total_array .= $bankmergedata;
        $flash_total_array .= '}';
        $flash_total_array .= '},';

        $date = date("Y-m-d H:i:s");
        $flash_total_array .= '"last_updated_time":' . '"' . $date . '"';
        $flash_total_array .= ',';
        $flash_total_array .= '"default_currency":' . '"' . $toCur . '"';
        $flash_total_array .= '}';

        if (!file_exists(BASE_PATH . '/flashreport')) {
            mkdir(BASE_PATH . '/flashreport', 0777, true);
        }
        if (!file_exists(BASE_PATH . '/flashreport/' . $group_id)) {
            mkdir(BASE_PATH . '/flashreport/' . $group_id, 0777, true);
        }

        $loginDate = date('Y-m-d');
        $file_name = $group_id . "_flashreport_" . $loginDate . ".json";
        $path = BASE_PATH . '/flashreport/' . $group_id . '/' . $file_name;
        $response['error'] = false;
        $response['message'] = RECORD_FOUND;
        file_put_contents($path, $flash_total_array);
        $response['error'] = false;
        $response['message'] = RECORD_FOUND;
        return $response;
    }

}
