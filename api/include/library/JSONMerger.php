<?php
/**
 * JSON MErger
 */

class JSONMerger
{
    private $files;

    public function __construct(array $files)
    {
        $this->files = $files;
        $this->currency = $currency_value;
    }

    protected function array_merge_recursive_distinct(array &$array1, array &$array2)
    {
        $merged = $array1;
        foreach ($array2 as $key => &$value) {
            if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
                $merged[$key] = $this->array_merge_recursive_distinct($merged[$key], $value);
            } else {
                $merged[$key] = $value;
            }
        }
        return $merged;
    }

    public function getTrendAvgCurByDate($basecur, $toCur)
    {

        $group = new group();
        $dates = array();
        $startDate = date("Y-m-01", strtotime("-12 months"));
        $endDate = date('Y-m-d');
        $i = 0;
        while (strtotime($startDate) <= strtotime($endDate)) {
            $dates[] = array("firstdate" => date("Y-m-01", strtotime($startDate)), "lastdate" => date("Y-m-t", strtotime($startDate)));
            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            $i++;
        }

        // $currency = 1;
        foreach ($dates as $key => $month_array) {
            if (is_array($month_array)) {
                //trendpnl currency conversion
                $month = date('n', strtotime($month_array['lastdate']));
                $year = date('y', strtotime($month_array['lastdate']));
                $byear = date('Y', strtotime($month_array['lastdate']));
                $trendcurrency_type = "Trendpnl";
                $trendavgCurrencyArray[] = $group->checkYearOfTrend($trendcurrency_type, $basecur, $toCur, $month, $year, $byear);
            }
            //  $currency++;
        }

        return $trendavgCurrencyArray;
    }

    public function getBlncAvgCurByDate($basecur, $toCur)
    {

        $group = new group();
        $dates = array();
        $startDate = date("Y-m-01", strtotime("-12 months"));
        $endDate = date('Y-m-d');
        $i = 0;
        while (strtotime($startDate) <= strtotime($endDate)) {
            $dates[] = array("firstdate" => date("Y-m-01", strtotime($startDate)), "lastdate" => date("Y-m-t", strtotime($startDate)));
            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            $i++;
        }
        // echo "<pre>";print_r($dates);exit;
        // $currency = 1;
        foreach ($dates as $key => $month_array) {
            if (is_array($month_array)) {
                //balancesheet currency conversion
                $current = date('Y-m-t');
                $blnccurrency_type = "balanceSheet";
                if ($month_array['lastdate'] == $current) {
                    $date = date('Y-m-d');
                    $lastdate = date('Y-m-d', strtotime($date . ' -1 day'));
                } else {
                    $lastdate = $month_array['lastdate'];
                }
                $month = date('n', strtotime($month_array['lastdate']));
                $year = date('Y', strtotime($month_array['lastdate']));
                $blncavgCurrencyArray[] = $group->checkYearOfBlncSheet($blnccurrency_type, $basecur, $toCur, $month, $year, $lastdate, $month_array['lastdate']);
            }
            //  $currency++;
        }
        return $blncavgCurrencyArray;
    }

    public function withoutAvgCurrency($basecur, $toCur)
    {
        $context = stream_context_create(array(
            'http' => array(
                'header' => "Authorization: Basic " . base64_encode(ACCOUNT_ID . ':' . ACCOUNT_API_KEY),
            ),
        ));
        $data = file_get_contents(CURRENCY_URL . '/?from=' . $toCur . '&to=' . $basecur . '&amount=1', false, $context);
        $data = json_decode($data, true);
        $currency = $data['to'][0]['mid'];
        //  $currency = 1;
        return $currency;
    }

    public function balanceSheetIndex($group_ids, $eliminate_path)
    {
        $group = new group();
        $getcurrencyBygroupID = $group->getcurrencyBygroupID($group_ids);
        $toCur = $getcurrencyBygroupID['default_currency'];

        $files = $this->files;

        if (count($files) == 10) {
            $path1 = $files[0];
            $json1 = $this->readBalanceSheetJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readBalanceSheetJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readBalanceSheetJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readBalanceSheetJson($path4, $toCur, $eliminate_path[3]);
            $path5 = $files[4];
            $json5 = $this->readBalanceSheetJson($path5, $toCur, $eliminate_path[4]);
            $path6 = $files[5];
            $json6 = $this->readBalanceSheetJson($path6, $toCur, $eliminate_path[5]);
            $path7 = $files[6];
            $json7 = $this->readBalanceSheetJson($path7, $toCur, $eliminate_path[6]);
            $path8 = $files[7];
            $json8 = $this->readBalanceSheetJson($path8, $toCur, $eliminate_path[7]);
            $path9 = $files[8];
            $json9 = $this->readBalanceSheetJson($path9, $toCur, $eliminate_path[8]);
            $path10 = $files[9];
            $json10 = $this->readBalanceSheetJson($path10, $toCur, $eliminate_path[9]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8);
            $collapsed8 = $collection8->collapse()->toArray();

            $collection9 = collect($json9);
            $collapsed9 = $collection9->collapse()->toArray();

            $collection10 = collect($json10);
            $collapsed10 = $collection10->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8, $collapsed9, $collapsed10);
        }

        if (count($files) == 9) {
            $path1 = $files[0];
            $json1 = $this->readBalanceSheetJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readBalanceSheetJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readBalanceSheetJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readBalanceSheetJson($path4, $toCur, $eliminate_path[3]);
            $path5 = $files[4];
            $json5 = $this->readBalanceSheetJson($path5, $toCur, $eliminate_path[4]);
            $path6 = $files[5];
            $json6 = $this->readBalanceSheetJson($path6, $toCur, $eliminate_path[5]);
            $path7 = $files[6];
            $json7 = $this->readBalanceSheetJson($path7, $toCur, $eliminate_path[6]);
            $path8 = $files[7];
            $json8 = $this->readBalanceSheetJson($path8, $toCur, $eliminate_path[7]);
            $path9 = $files[8];
            $json9 = $this->readBalanceSheetJson($path9, $toCur, $eliminate_path[8]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8);
            $collapsed8 = $collection8->collapse()->toArray();

            $collection9 = collect($json9);
            $collapsed9 = $collection9->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8, $collapsed9);
        }

        if (count($files) == 8) {
            $path1 = $files[0];
            $json1 = $this->readBalanceSheetJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readBalanceSheetJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readBalanceSheetJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readBalanceSheetJson($path4, $toCur, $eliminate_path[3]);
            $path5 = $files[4];
            $json5 = $this->readBalanceSheetJson($path5, $toCur, $eliminate_path[4]);
            $path6 = $files[5];
            $json6 = $this->readBalanceSheetJson($path6, $toCur, $eliminate_path[5]);
            $path7 = $files[6];
            $json7 = $this->readBalanceSheetJson($path7, $toCur, $eliminate_path[6]);
            $path8 = $files[7];
            $json8 = $this->readBalanceSheetJson($path8, $toCur, $eliminate_path[7]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8);
            $collapsed8 = $collection8->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7, $collapsed8);
        }

        if (count($files) == 7) {
            $path1 = $files[0];
            $json1 = $this->readBalanceSheetJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readBalanceSheetJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readBalanceSheetJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readBalanceSheetJson($path4, $toCur, $eliminate_path[3]);
            $path5 = $files[4];
            $json5 = $this->readBalanceSheetJson($path5, $toCur, $eliminate_path[4]);
            $path6 = $files[5];
            $json6 = $this->readBalanceSheetJson($path6, $toCur, $eliminate_path[5]);
            $path7 = $files[6];
            $json7 = $this->readBalanceSheetJson($path7, $toCur, $eliminate_path[6]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7);
            $collapsed7 = $collection7->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapsed7);
        }

        if (count($files) == 6) {
            $path1 = $files[0];
            $json1 = $this->readBalanceSheetJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readBalanceSheetJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readBalanceSheetJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readBalanceSheetJson($path4, $toCur, $eliminate_path[3]);
            $path5 = $files[4];
            $json5 = $this->readBalanceSheetJson($path5, $toCur, $eliminate_path[4]);
            $path6 = $files[5];
            $json6 = $this->readBalanceSheetJson($path6, $toCur, $eliminate_path[5]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6);
        }

        if (count($files) == 5) {
            $path1 = $files[0];
            $json1 = $this->readBalanceSheetJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readBalanceSheetJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readBalanceSheetJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readBalanceSheetJson($path4, $toCur, $eliminate_path[3]);
            $path5 = $files[4];
            $json5 = $this->readBalanceSheetJson($path5, $toCur, $eliminate_path[4]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5);
        }
        if (count($files) == 4) {
            $path1 = $files[0];
            $json1 = $this->readBalanceSheetJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readBalanceSheetJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readBalanceSheetJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readBalanceSheetJson($path4, $toCur, $eliminate_path[3]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4);
        }
        if (count($files) == 3) {
            $path1 = $files[0];
            $json1 = $this->readBalanceSheetJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readBalanceSheetJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readBalanceSheetJson($path3, $toCur, $eliminate_path[2]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3);
        }
        if (count($files) == 2) {
            $path1 = $files[0];
            $json1 = $this->readBalanceSheetJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readBalanceSheetJson($path2, $toCur, $eliminate_path[1]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2);
        }
        if (count($files) == 1) {
            $path1 = $files[0];
            $json1 = $this->readBalanceSheetJson($path1, $toCur, $eliminate_path[0]);
            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $ry = $collapsed;
        }
        return $ry;
    }

    public function readBalanceSheetJson($filePath, $toCur, $eliminate_path)
    {

        $group = new group();
        if (sizeof($eliminate_path) > 0) {
            $jsondata = json_decode(file_get_contents($filePath), true);
            foreach ($eliminate_path as $value) {
                if (is_array($value)) {
                    $jsondata[1]['balanceSheet'] = $group->ArrayUnset($jsondata[1]['balanceSheet'], $value['account_name']);
                }
            }
            $json = $jsondata;
        } else {
            $json = json_decode(file_get_contents($filePath), true);
        }

        if ($json['default_currency'] !== $toCur) {
            $case = 0;
            $trendavgCurrencyArray = $this->getBlncAvgCurByDate($json['default_currency'], $toCur);
            $withoutAvgCurrency = $this->withoutAvgCurrency($json['default_currency'], $toCur);
            $final_jsondata = $this->balanceSheetArrayReplace($json, 'amount', $trendavgCurrencyArray, $withoutAvgCurrency);
        } else {
            $currency = $toCur;
            $final_jsondata = $json;
        }

        return $final_jsondata;
    }

    public function balanceSheetArrayReplace($Array, $Find, $trendavgCurrencyArray, $withoutAvgCurrency)
    {
        $group = new group();
        if (is_array($Array)) {
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    if ($Key === 'Operating Expenses') {
                        foreach ($Val as $key2 => $value2) {
                            if (is_numeric($value2)) {
                                $Array[$Key][$key2] = floatval($Array[$Key][$key2]) / $withoutAvgCurrency;
                            }
                        }
                    } else {
                        $Array[$Key] = $this->balanceSheetArrayReplace($Array[$Key], $Find, $trendavgCurrencyArray, $withoutAvgCurrency);
                    }
                } else {
                    if ($Key == $Find) {
                        $final_currency = $this->getLastdateCurrency($Array['date'], $trendavgCurrencyArray, $Array[$Key]);
                        $Array[$Key] = $final_currency;
                    }
                }
            }
        }
        return $Array;
    }

    public function getLastdateCurrency($findDate, $trendavgCurrencyArray, $amount)
    {
        foreach ($trendavgCurrencyArray as $date) {
            if ($date['lastdate'] == $findDate) {
                $final_amount = $amount / $date['avgCur'];
                return $final_amount;
            }
        }
    }

    public function trendIndex($group_ids, $eliminate_path)
    {
        $group = new group();
        $getcurrencyBygroupID = $group->getcurrencyBygroupID($group_ids);
        $toCur = $getcurrencyBygroupID['default_currency'];
        $files = $this->files;

        if (count($files) == 10) {
            $path1 = $files[0];
            $json1 = $this->readTrendJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readTrendJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readTrendJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readTrendJson($path4, $toCur, $eliminate_path[3]);
            $path5 = $files[4];
            $json5 = $this->readTrendJson($path5, $toCur, $eliminate_path[4]);
            $path6 = $files[5];
            $json6 = $this->readTrendJson($path6, $toCur, $eliminate_path[5]);
            $path7 = $files[6];
            $json7 = $this->readTrendJson($path7, $toCur, $eliminate_path[6]);
            $path8 = $files[7];
            $json8 = $this->readTrendJson($path8, $toCur, $eliminate_path[7]);
            $path9 = $files[8];
            $json9 = $this->readTrendJson($path9, $toCur, $eliminate_path[8]);
            $path10 = $files[9];
            $json10 = $this->readTrendJson($path10, $toCur, $eliminate_path[9]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8);
            $collapsed8 = $collection8->collapse()->toArray();

            $collection9 = collect($json9);
            $collapsed9 = $collection9->collapse()->toArray();

            $collection10 = collect($json10);
            $collapsed10 = $collectio10->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapse7, $collapsed8, $collapsed9, $collapsed10);
        }
        if (count($files) == 9) {
            $path1 = $files[0];
            $json1 = $this->readTrendJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readTrendJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readTrendJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readTrendJson($path4, $toCur, $eliminate_path[3]);
            $path5 = $files[4];
            $json5 = $this->readTrendJson($path5, $toCur, $eliminate_path[4]);
            $path6 = $files[5];
            $json6 = $this->readTrendJson($path6, $toCur, $eliminate_path[5]);
            $path7 = $files[6];
            $json7 = $this->readTrendJson($path7, $toCur, $eliminate_path[6]);
            $path8 = $files[7];
            $json8 = $this->readTrendJson($path8, $toCur, $eliminate_path[7]);
            $path9 = $files[8];
            $json9 = $this->readTrendJson($path9, $toCur, $eliminate_path[8]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8);
            $collapsed8 = $collection8->collapse()->toArray();

            $collection9 = collect($json9);
            $collapsed9 = $collection9->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapse7, $collapsed8, $collapsed9);
        }
        if (count($files) == 8) {
            $path1 = $files[0];
            $json1 = $this->readTrendJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readTrendJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readTrendJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readTrendJson($path4, $toCur, $eliminate_path[3]);
            $path5 = $files[4];
            $json5 = $this->readTrendJson($path5, $toCur, $eliminate_path[4]);
            $path6 = $files[5];
            $json6 = $this->readTrendJson($path6, $toCur, $eliminate_path[5]);
            $path7 = $files[6];
            $json7 = $this->readTrendJson($path7, $toCur, $eliminate_path[6]);
            $path8 = $files[7];
            $json8 = $this->readTrendJson($path8, $toCur, $eliminate_path[7]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7);
            $collapsed7 = $collection7->collapse()->toArray();

            $collection8 = collect($json8);
            $collapsed8 = $collection8->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapse7, $collapsed8);
        }
        if (count($files) == 7) {
            $path1 = $files[0];
            $json1 = $this->readTrendJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readTrendJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readTrendJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readTrendJson($path4, $toCur, $eliminate_path[3]);
            $path5 = $files[4];
            $json5 = $this->readTrendJson($path5, $toCur, $eliminate_path[4]);
            $path6 = $files[5];
            $json6 = $this->readTrendJson($path6, $toCur, $eliminate_path[5]);
            $path7 = $files[6];
            $json7 = $this->readTrendJson($path7, $toCur, $eliminate_path[6]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $collection7 = collect($json7);
            $collapsed7 = $collection7->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6, $collapse7);
        }

        if (count($files) == 6) {
            $path1 = $files[0];
            $json1 = $this->readTrendJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readTrendJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readTrendJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readTrendJson($path4, $toCur, $eliminate_path[3]);
            $path5 = $files[4];
            $json5 = $this->readTrendJson($path5, $toCur, $eliminate_path[4]);
            $path6 = $files[5];
            $json6 = $this->readTrendJson($path6, $toCur, $eliminate_path[5]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $collection6 = collect($json6);
            $collapsed6 = $collection6->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5, $collapsed6);
        }
        if (count($files) == 5) {
            $path1 = $files[0];
            $json1 = $this->readTrendJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readTrendJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readTrendJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readTrendJson($path4, $toCur, $eliminate_path[3]);
            $path5 = $files[4];
            $json5 = $this->readTrendJson($path5, $toCur, $eliminate_path[4]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $collection5 = collect($json5);
            $collapsed5 = $collection5->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4, $collapsed5);
        }
        if (count($files) == 4) {
            $path1 = $files[0];
            $json1 = $this->readTrendJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readTrendJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readTrendJson($path3, $toCur, $eliminate_path[2]);
            $path4 = $files[3];
            $json4 = $this->readTrendJson($path4, $toCur, $eliminate_path[3]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $collection4 = collect($json4);
            $collapsed4 = $collection4->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3, $collapsed4);
        }
        if (count($files) == 3) {
            $path1 = $files[0];
            $json1 = $this->readTrendJson($path1, $toCur, $eliminate_path[0]);

            $path2 = $files[1];
            $json2 = $this->readTrendJson($path2, $toCur, $eliminate_path[1]);
            $path3 = $files[2];
            $json3 = $this->readTrendJson($path3, $toCur, $eliminate_path[2]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $collection3 = collect($json3);
            $collapsed3 = $collection3->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2, $collapsed3);

        }
        if (count($files) == 2) {
            $path1 = $files[0];
            $json1 = $this->readTrendJson($path1, $toCur, $eliminate_path[0]);
            $path2 = $files[1];
            $json2 = $this->readTrendJson($path2, $toCur, $eliminate_path[1]);

            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $collection2 = collect($json2);
            $collapsed2 = $collection2->collapse()->toArray();

            $ry = array_merge_recursive($collapsed, $collapsed2);
        }
        if (count($files) == 1) {
            $path1 = $files[0];
            $json1 = $this->readTrendJson($path1, $toCur, $eliminate_path[0]);
            $collection = collect($json1);
            $collapsed = $collection->collapse()->toArray();

            $ry = $collapsed;
        }
        return $ry;
    }

    public function readTrendJson($filePath, $toCur, $eliminate_path)
    {
        $group = new group();
        if (sizeof($eliminate_path) > 0) {
            $jsondata = json_decode(file_get_contents($filePath), true);
            foreach ($eliminate_path as $value) {
                if (is_array($value)) {
                    $jsondata[3]['trendPNL'] = $group->ArrayUnset($jsondata[3]['trendPNL'], $value['account_name']);
                }
            }
            $json = $jsondata;
        } else {
            $json = json_decode(file_get_contents($filePath), true);
        }

        if ($json['default_currency'] !== $toCur) {
            $case = 1;
            $blncavgCurrencyArray = $this->getTrendAvgCurByDate($json['default_currency'], $toCur);
            $withoutAvgCurrency = $this->withoutAvgCurrency($json['default_currency'], $toCur);
            $final_jsondata = $this->trendArrayReplace($json, 'amount', $blncavgCurrencyArray, $withoutAvgCurrency);
        } else {
            $currency = $toCur;
            $final_jsondata = $json;
        }
        return $final_jsondata;
    }

    public function trendArrayReplace($Array, $Find, $blncavgCurrencyArray, $withoutAvgCurrency)
    {
        $group = new group();
        if (is_array($Array)) {
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    if ($Key === 'Operating Expenses') {
                        foreach ($Val as $key2 => $value2) {
                            if (is_numeric($value2)) {
                                $Array[$Key][$key2] = floatval($Array[$Key][$key2]) / $withoutAvgCurrency;
                            }
                        }
                    } else {
                        $Array[$Key] = $this->trendArrayReplace($Array[$Key], $Find, $blncavgCurrencyArray, $withoutAvgCurrency);
                    }
                } else {
                    if ($Key == $Find) {
                        $final_currency = $this->getMonthYearCurrency($Array['month'], $Array['year'], $blncavgCurrencyArray, $Array[$Key]);
                        $Array[$Key] = $final_currency;
                    }
                }
            }
        }
        return $Array;
    }

    public function getMonthYearCurrency($findMonth, $findYear, $blncavgCurrencyArray, $amount)
    {
        foreach ($blncavgCurrencyArray as $date) {
            if ($date['month'] == $findMonth && $date['year'] == $findYear) {
                $final_amount = $amount / $date['avgCur'];
                return $final_amount;
            }
        }
    }

}
