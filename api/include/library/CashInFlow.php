<?php
/**
 * Cash In Flow
 */
class CashInFlow
{

    private $cashFlow;

    public function __construct(array $cashFlow)
    {

        $this->cashFlow = $cashFlow;
    }

    public function CashFlow()
    {
      /* operating Activities */
        $OperatingCashInFlow = $this->cashFlow['Cash Flows from Operating Activities'];
        $Ovalues = array();
        foreach ($OperatingCashInFlow as $key => $Ovalue) {
            if (is_numeric($key)) {
                $Ovalues[] = $Ovalue['value'];

            }
        }
        $OperatingCashInFlowsData = $this->TotalOperatingActivities($Ovalues);
        unset($OperatingCashInFlow['summaryRow']);
        $OperatingCashInFlowFinaldata['Cash Flows from Operating Activities'] = array_merge($OperatingCashInFlow, $OperatingCashInFlowsData);

        /* Investing Activities */
        $investingcashInFlow = $this->cashFlow['Cash Flows from Investing Activities'];
        $invvalue = array();
        foreach ($investingcashInFlow as $key => $investingValue) {
            if (is_numeric($key)) {
                $invvalue[] = $investingValue['value'];

            }
        }
        $investingActivitiesData = $this->TotalInvestingActivities($invvalue);
        unset($investingcashInFlow['summaryRow']);
        $investingActivitiesFinaldata['Cash Flows from Investing Activities'] = array_merge($investingcashInFlow, $investingActivitiesData);
 // echo "<pre>";
        // print_R($CashEquivalentsFinaldata);
      /*Financial Activities  */
        $financialcashInFlow = $this->cashFlow['Cash Flows from Financial Activities'];
        $fvalue = array();
        foreach ($financialcashInFlow as $key => $financialValue) {
            if (is_numeric($key)) {
                $fvalue[] = $financialValue['value'];

            }
        }
        $financialData = $this->TotalFinacialActivities($fvalue);
        unset($financialcashInFlow['summaryRow']);
        $financialFinaldata['Cash Flows from Financial Activities'] = array_merge($financialcashInFlow, $financialData);

        /*Cash and Cash Equivalents  */  
        $CashEquivalents = $this->cashFlow['Cash and Cash Equivalents'];
        $evalue = array();
        foreach ($CashEquivalents as $key => $equivalentValue) {
            if (is_numeric($key)) {
                $evalue[] = $equivalentValue['value'];

            }
        }
        $CashEquivalentsData = $this->Totalcashequivalents($evalue);
        unset($CashEquivalents['summaryRow']);
        $CashEquivalentsFinaldata['Cash and Cash Equivalents'] = array_merge($CashEquivalents, $CashEquivalentsData);

        $finalCashFlow = array_merge($OperatingCashInFlowFinaldata, $investingActivitiesFinaldata, $financialFinaldata, $CashEquivalentsFinaldata);
        // echo "<pre>";
        // print_R($CashEquivalentsFinaldata);
        return $finalCashFlow;
    }

    public function TotalOperatingActivities($OprActvalue)
    {
        $array3 = array();
        foreach ($OprActvalue as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            //  "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }

        }

        $finalArr['summaryRow'] = ['title' => "Net Cash Flow from operating activities", "value" => collect($array3)->collapse()->toArray()];
        return $finalArr;
    }

    public function TotalInvestingActivities($OprActvalue)
    {
        $array3 = array();
        foreach ($OprActvalue as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();

            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            //  "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }

        }
        $finalArr['summaryRow'] = ['title' => "Total Cash Flows from investing activities", "value" => collect($array3)->collapse()->toArray()];
        return $finalArr;
    }

    public function TotalFinacialActivities($OprActvalue)
    {
        $array3 = array();
        foreach ($OprActvalue as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();

            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            //  "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }

        }
        $finalArr['summaryRow'] = ['title' => "Total Cash Flows from Financing activities", "value" => collect($array3)->collapse()->toArray()];
        return $finalArr;
    }

    public function Totalcashequivalents($OprActvalue)
    {
        $array3 = array();
        foreach ($OprActvalue as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();

            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            //  "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }

        }
        $finalArr['summaryRow'] = ['title' => "Net cash & cash equivalents at end of period", "value" => collect($array3)->collapse()->toArray()];
        return $finalArr;
    }

    public function netDecreaseValue($opValue, $InValue, $cashValue)
    {

        if (sizeof($opValue) > 0) {
            $opValueArray = collect($opValue)->groupBy('id')->toArray();
        }
        if (sizeof($InValue) > 0) {
            $InValueArray = collect($InValue)->groupBy('id')->toArray();
        }
        if (sizeof($cashValue) > 0) {
            $cashValueArray = collect($cashValue)->groupBy('id')->toArray();
        }

        $result = [];
        foreach ($opValueArray as $opValuekey => $value) {
            if (is_array($value)) {
                if (isset($opValuekey)) {
                    if ($value[0]['id'] == $InValueArray[$opValuekey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] + ($InValueArray[$opValuekey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],

                    ];
                }

            }

            $result[] = $value;
        }
        $finalresult = collect($result)->collapse()->toArray();
        $finalresultArray = collect($finalresult)->groupBy('id')->toArray();

        $result1 = [];
        foreach ($cashValueArray as $finalresultKey => $value) {
            if (is_array($value)) {
                if (isset($finalresultKey)) {
                    if ($value[0]['id'] == $finalresultArray[$finalresultKey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] + ($finalresultArray[$finalresultKey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],

                    ];
                }
            }
            $result1[] = $value;
        }
        $finalresult1 = collect($result1)->collapse()->toArray();
        return $finalresult1;
    }

}
