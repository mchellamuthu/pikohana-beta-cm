<?php
include './TrendPNL.php';
// namespace Library;

/**
 * trendPNL
 */
class BalanceSheet
{
    // public $income;
    // public $Le;
    private $jsondata;
    private $NetProfit;
    public function __construct($jsondata, $NetProfit = null)
    {
        $this->jsondata = $jsondata;
        $this->NetProfit = $NetProfit;
    }

    public function Bank()
    {
        $trendPNL = $this->jsondata['balanceSheet'];
        $income = $trendPNL['Bank'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {
            foreach ($income as $key => $value) {
                /**get All Income values except SummeryRow**/
                if ($key !== 'summaryRow') {
                    $amounts = [];
                    $grouped = collect($value)->groupBy('id')->toArray();
                    foreach ($grouped as $gkey => $gvalue) {
                        $collection = collect($gvalue);
                        $amount = $collection->sum('amount');
                        $amounts[$gkey] = $amount;
                    }

                    // if($key != "Cash in Banks"){
                    $data[$key] = collect($value)->unique('id')->map(function ($item) use ($amounts) {
                        $amount_data = $amounts[$item['id']];
                        return [
                            "date" => $item['date'],
                            "amount" => round($amount_data),
                            "month" => $item['month'],
                            "year" => $item['year'],
                            "id" => $item['id']
                        ];

                    })->all();
                    // }
                }
            }
        }

        return $data;
    }

    public function TotalBank($bankRowData)
    {
        $array3 = array();
        foreach ($bankRowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }
        }
        $finalArr['summaryRow']['Total Bank'] = collect($array3)->collapse()->toArray();
        return $finalArr;
    }
    public function CurrentAssets()
    {
        $trendPNL = $this->jsondata['balanceSheet'];
        $income = $trendPNL['Current Assets'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {
            foreach ($income as $key => $value) {
                /**get All Income values except SummeryRow**/
                if ($key !== 'summaryRow') {
                    $amounts = [];
                    $grouped = collect($value)->groupBy('id')->toArray();
                    foreach ($grouped as $gkey => $gvalue) {
                        $collection = collect($gvalue);
                        $amount = $collection->sum('amount');
                        $amounts[$gkey] = $amount;
                    }
                    $data[$key] = collect($value)->unique('id')->map(function ($item) use ($amounts) {
                        $amount_data = $amounts[$item['id']];
                        return [
                            "date" => $item['date'],
                            "amount" => round($amount_data),
                            "month" => $item['month'],
                            "year" => $item['year'],
                            "id" => $item['id']
                        ];
                    })->all();
                }
            }
        }

        return $data;
    }

    public function TotalCurrentAssets($CurrentAssetsRowData)
    {
        $array3 = array();
        foreach ($CurrentAssetsRowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }
        }
        $finalArr['summaryRow']['Total Current Assets'] = collect($array3)->collapse()->toArray();
        return $finalArr;
    }
    public function FixedAssets()
    {
        $trendPNL = $this->jsondata['balanceSheet'];
        $income = $trendPNL['Fixed Assets'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {
            foreach ($income as $key => $value) {
                /**get All Income values except SummeryRow**/
                if ($key !== 'summaryRow') {
                    $amounts = [];
                    $grouped = collect($value)->groupBy('id')->toArray();
                    foreach ($grouped as $gkey => $gvalue) {
                        $collection = collect($gvalue);
                        $amount = $collection->sum('amount');
                        $amounts[$gkey] = $amount;
                    }
                    $data[$key] = collect($value)->unique('id')->map(function ($item) use ($amounts) {
                        $amount_data = $amounts[$item['id']];
                        return [
                            "date" => $item['date'],
                            "amount" => round($amount_data),
                            "month" => $item['month'],
                            "year" => $item['year'],
                            "id" => $item['id']
                        ];
                    })->all();
                }
            }
        }

        return $data;
    }

    public function TotalFixedAssets($fixedAssetsrowData)
    {
        $array3 = array();
        foreach ($fixedAssetsrowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }
        }
        $finalArr['summaryRow']['Total Fixed Assets'] = collect($array3)->collapse()->toArray();
        return $finalArr;
    }

    public function CurrentLiabilities()
    {
        $trendPNL = $this->jsondata['balanceSheet'];
        $income = $trendPNL['Current Liabilities'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {
            foreach ($income as $key => $value) {
                /**get All Income values except SummeryRow**/
                if ($key !== 'summaryRow') {
                    $amounts = [];
                    $grouped = collect($value)->groupBy('id')->toArray();
                    foreach ($grouped as $gkey => $gvalue) {
                        $collection = collect($gvalue);
                        $amount = $collection->sum('amount');
                        $amounts[$gkey] = $amount;
                    }
                    $data[$key] = collect($value)->unique('id')->map(function ($item) use ($amounts) {
                        $amount_data = $amounts[$item['id']];
                        return [
                            "date" => $item['date'],
                            "amount" => round($amount_data),
                            "month" => $item['month'],
                            "year" => $item['year'],
                            "id" => $item['id']
                        ];
                    })->all();
                }
            }
        }

        return $data;
    }

    public function totalCurrentLiabities($curerntLiabities)
    {
        $array3 = array();
        foreach ($curerntLiabities as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year']
                        ];
                    }
                }
            }
        }
        $finalArr['summaryRow']['Total Current Liabilities'] = collect($array3)->collapse()->toArray();
        return $finalArr;
    }

    public function TotalAssets()
    {
        $trendPNL = $this->jsondata['balanceSheet'];
        $grossProfit = $trendPNL['Total Assets'];
        $filtered = collect($grossProfit)->reject(function ($value, $key) {
            return $key == 'no_expand';
        })->toArray();

        // $filtered->all();
        $grouped = collect($filtered)->groupBy('id')->toArray();
        foreach ($grouped as $gkey => $gvalue) {
            $collection = collect($gvalue);
            $amount = $collection->sum('amount');
            $amounts[$gkey] = $amount;
        }
        $data = collect($filtered)->unique('id')->map(function ($item) use ($amounts) {
            $amount_data = $amounts[$item['id']];
            return [
                "date" => $item['date'],
                "amount" => round($amount_data),
                "month" => $item['month'],
                "year" => $item['year'],
                "id" => $item['id']
            ];
        })->toArray();
        $datacollect = collect($data)->put('no_expand', true)->toArray();
        return $datacollect;
    }

    public function TotalLiabilities()
    {
        $trendPNL = $this->jsondata['balanceSheet'];
        $grossProfit = $trendPNL['Total Liabilities'];
        $filtered = collect($grossProfit)->reject(function ($value, $key) {
            return $key == 'no_expand';
        })->toArray();

        // $filtered->all();
        $grouped = collect($filtered)->groupBy('id')->toArray();
        foreach ($grouped as $gkey => $gvalue) {
            $collection = collect($gvalue);
            $amount = $collection->sum('amount');
            $amounts[$gkey] = $amount;
        }
        $data = collect($filtered)->unique('id')->map(function ($item) use ($amounts) {
            $amount_data = $amounts[$item['id']];
            return [
                "date" => $item['date'],
                "amount" => round($amount_data),
                "month" => $item['month'],
                "year" => $item['year'],
                "id" => $item['id']
            ];
        })->toArray();
        $datacollect = collect($data)->put('no_expand', true)->toArray();
        return $datacollect;
    }

    public function NetAssets()
    {
        $trendPNL = $this->jsondata['balanceSheet'];
        $grossProfit = $trendPNL['Net Assets'];
        $filtered = collect($grossProfit)->reject(function ($value, $key) {
            return $key == 'no_expand';
        })->toArray();

        // $filtered->all();
        $grouped = collect($filtered)->groupBy('id')->toArray();
        foreach ($grouped as $gkey => $gvalue) {
            $collection = collect($gvalue);
            $amount = $collection->sum('amount');
            $amounts[$gkey] = $amount;
        }
        $data = collect($filtered)->unique('id')->map(function ($item) use ($amounts) {
            $amount_data = $amounts[$item['id']];
            return [
                "date" => $item['date'],
                "amount" => round($amount_data),
                "month" => $item['month'],
                "year" => $item['year'],
                "id" => $item['id']
            ];
        })->toArray();
        $datacollect = collect($data)->put('no_expand', true)->toArray();
        return $datacollect;
    }

    public function Equity()
    {
        $trendPNL = $this->jsondata['balanceSheet'];
        $income = $trendPNL['Equity'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {
            foreach ($income as $key => $value) {
                /**get All Income values except SummeryRow**/
                if ($key !== 'summaryRow') {
                    if ($key == 'Current Year Earnings') {
                        $netProfit = $this->NetProfit;
                        unset($netProfit['no_expand']);
                        $net_amount = 0;
                        $data['Current Year Earnings'] = collect($netProfit)->unique('id')->map(function ($item) {
                            return [
                                "date" => $item['date'],
                                "amount" => $item['amount'],
                                "month" => $item['month'],
                                "year" => $item['year'],
                                "id" => $item['id']
                            ];
                        })->all();

                    } else {

                        $amounts = [];
                        $grouped = collect($value)->groupBy('id')->toArray();
                        foreach ($grouped as $gkey => $gvalue) {
                            $collection = collect($gvalue);
                            $amount = $collection->sum('amount');
                            $amounts[$gkey] = $amount;
                        }
                        $data[$key] = collect($value)->unique('id')->map(function ($item) use ($amounts) {
                            $amount_data = $amounts[$item['id']];
                            return [
                                "date" => $item['date'],
                                "amount" => round($amount_data),
                                "month" => $item['month'],
                                "year" => $item['year'],
                                "id" => $item['id']
                            ];
                        })->all();
                    }
                }
            }
        }

        return $data;
    }

    public function totalEquity($equityRowData)
    {
        $array3 = array();
        foreach ($equityRowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {

                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }

                }
            }
        }
        $finalArr['summaryRow'] = ['equity_gray' => true, 'Total Equity' => collect($array3)->collapse()->toArray()];
        return $finalArr;
    }

    public function NonCurrentAssets()
    {
        $trendPNL = $this->jsondata['balanceSheet'];
        $income = $trendPNL['Non-Current Assets'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {
            foreach ($income as $key => $value) {

                /**get All Income values except SummeryRow**/
                if ($key !== 'summaryRow') {
                    $amounts = [];
                    $grouped = collect($value)->groupBy('id')->toArray();
                    foreach ($grouped as $gkey => $gvalue) {
                        $collection = collect($gvalue);
                        $amount = $collection->sum('amount');
                        $amounts[$gkey] = $amount;
                    }
                    $data[$key] = collect($value)->unique('id')->map(function ($item) use ($amounts) {
                        $amount_data = $amounts[$item['id']];
                        return [
                            "date" => $item['date'],
                            "amount" => round($amount_data),
                            "month" => $item['month'],
                            "year" => $item['year'],
                            "id" => $item['id']
                        ];
                    })->all();
                }
            }
        }

        return $data;
    }

    public function TotalNonCurrentAssets($nonCurrentAssetsRowData)
    {
        $array3 = array();
        foreach ($nonCurrentAssetsRowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year']

                        ];
                    }
                }
            }
        }
        $finalArr['summaryRow']['Total Non-current Assets'] = collect($array3)->collapse()->toArray();
        return $finalArr;
    }

    public function NonCurrentLiabilities()
    {
        $trendPNL = $this->jsondata['balanceSheet'];
        $income = $trendPNL['Non-Current Liabilities'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {
            foreach ($income as $key => $value) {
                /**get All Income values except SummeryRow**/
                if ($key !== 'summaryRow') {
                    $amounts = [];
                    $grouped = collect($value)->groupBy('id')->toArray();
                    foreach ($grouped as $gkey => $gvalue) {
                        $collection = collect($gvalue);
                        $amount = $collection->sum('amount');
                        $amounts[$gkey] = $amount;
                    }
                    $data[$key] = collect($value)->unique('id')->map(function ($item) use ($amounts) {
                        $amount_data = $amounts[$item['id']];
                        return [
                            "date" => $item['date'],
                            "amount" => round($amount_data),
                            "month" => $item['month'],
                            "year" => $item['year'],
                            "id" => $item['id']
                        ];
                    })->all();
                }
            }
        }
        return $data;
    }

    public function totalNonCurrentLiabilities($nonCurrliabitiesRowData)
    {
        $array3 = array();
        foreach ($nonCurrliabitiesRowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year']
                        ];
                    }
                }
            }
        }
        $finalArr['summaryRow']['Total Non-Current Liabilities'] = collect($array3)->collapse()->toArray();
        return $finalArr;
    }

    public function totalAssetsData($totalBankData, $totalCurrentAssetsData, $totalFixedAssetsData, $totalNonCurrentAssetsData)
    {
        if (sizeof($totalBankData) > 0) {
            $totalbank = $totalBankData['summaryRow']['Total Bank'];
            $totalbankArray = collect($totalbank)->groupBy('id')->toArray();
        }
        if (sizeof($totalCurrentAssetsData) > 0) {
            $currentAssets = $totalCurrentAssetsData['summaryRow']['Total Current Assets'];
            $currentAssetsArray = collect($currentAssets)->groupBy('id')->toArray();
        }
        if (sizeof($totalFixedAssetsData) > 0) {
            $fixedassets = $totalFixedAssetsData['summaryRow']['Total Fixed Assets'];
            $fixedassetsArray = collect($fixedassets)->groupBy('id')->toArray();
        }
        if (sizeof($totalNonCurrentAssetsData) > 0) {
            $noncurrentAssets = $totalNonCurrentAssetsData['summaryRow']['Total Non-current Assets'];
            $noncurrentAssetsArray = collect($noncurrentAssets)->groupBy('id')->toArray();
        }

        $result = [];
        foreach ($totalbankArray as $totalbankakey => $value) {
            if (is_array($value)) {
                if (isset($totalbankakey)) {
                    if ($value[0]['id'] == $currentAssetsArray[$totalbankakey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] + ($currentAssetsArray[$totalbankakey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],

                    ];
                }

            }

            $result[] = $value;
        }
        $finalresult = collect($result)->collapse()->toArray();
        $finalresultArray = collect($finalresult)->groupBy('id')->toArray();

        $result1 = [];
        foreach ($fixedassetsArray as $fixedassetsakey => $value) {
            if (is_array($value)) {
                if (isset($fixedassetsakey)) {
                    if ($value[0]['id'] == $noncurrentAssetsArray[$fixedassetsakey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] + ($noncurrentAssetsArray[$fixedassetsakey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],

                    ];
                }
            }
            $result1[] = $value;
        }
        $finalresult1 = collect($result1)->collapse()->toArray();
        $finalresult1Array = collect($finalresult1)->groupBy('id')->toArray();

        $result3 = [];
        foreach ($finalresultArray as $resultakey => $value) {
            if (is_array($value)) {
                $result3[$resultakey]["no_expand"] = true;
                if (isset($resultakey)) {
                    if ($value[0]['id'] == $finalresult1Array[$resultakey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] + ($finalresult1Array[$resultakey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],
                    ];
                }
            }
            $result3[] = $value;
        }
        $result3Array = collect($result3)->collapse()->toArray();
        return $result3Array;
    }

    public function totalLiabilitiess($totalLiabitiesData, $totalnonCurrLiabitiesData)
    {
        if (sizeof($totalLiabitiesData) > 0) {
            $totalLiabities = $totalLiabitiesData['summaryRow']['Total Current Liabilities'];
            $totalLiabitiesArray = collect($totalLiabities)->groupBy('id')->toArray();
        }
        if (sizeof($totalnonCurrLiabitiesData) > 0) {
            $nonCurrentlia = $totalnonCurrLiabitiesData['summaryRow']['Total Non-Current Liabilities'];
            $nonCurrentliaArray = collect($nonCurrentlia)->groupBy('id')->toArray();
        }

        $result = [];
        foreach ($totalLiabitiesArray as $akey => $value) {
            if (is_array($value)) {
                $result[$akey]["no_expand"] = true;
                if (isset($akey)) {
                    if ($value[0]['id'] == $nonCurrentliaArray[$akey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] + ($nonCurrentliaArray[$akey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],
                    ];
                }
            }

            $result[] = $value;
        }
        $finalresult = collect($result)->collapse()->toArray();
        return $finalresult;
    }

    public function totalnetAssets($totalAssets, $totlLiabilities)
    {
        if (sizeof($totalAssets) > 0) {
            unset($totalAssets['no_expand']);
            $totalLiabitiesArray = collect($totalAssets)->groupBy('id')->toArray();
        }
        if (sizeof($totlLiabilities) > 0) {
            unset($totlLiabilities['no_expand']);
            $nonCurrentliaArray = collect($totlLiabilities)->groupBy('id')->toArray();
        }

        $result = [];
        foreach ($totalLiabitiesArray as $akey => $value) {
            if (is_array($value)) {
                $result[$akey]["no_expand"] = true;
                if (isset($akey)) {
                    if ($value[0]['id'] == $nonCurrentliaArray[$akey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] - ($nonCurrentliaArray[$akey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],

                    ];
                }
            }

            $result[] = $value;
        }
        $finalresult = collect($result)->collapse()->toArray();
        return $finalresult;
    }

    public function AdjustmentValue($cashInBankData, $netCashNetOfPeriod)
    {
        if (sizeof($cashInBankData) > 0) {
            unset($cashInBankData['no_expand']);
            $cashInBankDataArray = collect($cashInBankData)->groupBy('id')->toArray();
        }
        if (sizeof($netCashNetOfPeriod) > 0) {
            unset($netCashNetOfPeriod['no_expand']);
            $netCashNetOfPeriodArray = collect($netCashNetOfPeriod)->groupBy('id')->toArray();
        }

        $result = [];
        foreach ($cashInBankDataArray as $akey => $value) {
            if (is_array($value)) {
                // $result[$akey]["no_expand"] = true;
                if (isset($akey)) {
                    if ($value[0]['id'] == $netCashNetOfPeriodArray[$akey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] - ($netCashNetOfPeriodArray[$akey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],

                    ];
                }
            }

            $result[] = $value;
        }
        $finalresult = collect($result)->collapse()->toArray();
        return $finalresult;
    }

}
