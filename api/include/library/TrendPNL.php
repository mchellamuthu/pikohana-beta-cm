<?php

// namespace Library;

/**
 * trendPNL
 */
class TrendPNL
{
    // public $income;
    // public $Le;
    private $jsondata;
    public function __construct($jsondata)
    {
        $this->jsondata = $jsondata;
    }

    public function income()
    {
        $trendPNL = $this->jsondata['trendPNL'];
        $income = $trendPNL['Income'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {

            foreach ($income as $key => $value) {
                /**get All Income values except SummeryRow**/
                if ($key !== 'summaryRow') {
                    $amounts = [];
                    $grouped = collect($value)->groupBy('id')->toArray();
                    foreach ($grouped as $gkey => $gvalue) {
                        $collection = collect($gvalue);
                        $amount = $collection->sum('amount');
                        $amounts[$gkey] = $amount;
                    }

                    $data[$key] = collect($value)->unique('id')->map(function ($item) use ($amounts) {
                        $amount_data = $amounts[$item['id']];

                        return [
                            "date" => $item['date'],
                            "amount" => round($amount_data),
                            "month" => $item['month'],
                            "year" => $item['year'],
                            "id" => $item['id'],
                        ];
                    })->all();
                    $collection = collect($data[$key])->groupBy('year')->toArray();
                    $profitArray = array();
                    foreach ($collection as $profit) {
                        if (is_array($profit)) {
                            $coll = collect($profit)->sortBy('month')->toArray();
                            $profitArray[] = $coll;
                        }
                    }

                    $finalNetProfit = collect($profitArray)->collapse()->toArray();
                    $data[$key] = $finalNetProfit;
                }
            }
        }

        return $data;
    }

    public function totalIncome($incomeRowData)
    {
        $array3 = array();
        foreach ($incomeRowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }
        }

        foreach ($array3 as $key => $value) {
            unset($array3[$key][1]);
        }
        $finalArr['summaryRow']['Total Income'] = collect($array3)->collapse()->toArray();
        return $finalArr;
    }

    public function LessOperatingExpenses()
    {
        $trendPNL = $this->jsondata['trendPNL'];
        $income = $trendPNL['Less Operating Expenses'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {

            foreach ($income as $key => $value) {
                /**get All Income values except SummeryRow**/
                if ($key !== 'summaryRow' && $key !== 'Employee Compensation & Related Expenses') {
                    $amounts = [];
                    $grouped = collect($value)->groupBy('id')->toArray();
                    foreach ($grouped as $gkey => $gvalue) {
                        $collection = collect($gvalue);
                        $amount = $collection->sum('amount');
                        $amounts[$gkey] = $amount;
                    }
                    $data[$key] = collect($value)->unique('id')->map(function ($item) use ($amounts) {
                        $amount_data = $amounts[$item['id']];
                        return [
                            "date" => $item['date'],
                            "amount" => round($amount_data),
                            "month" => $item['month'],
                            "year" => $item['year'],
                            "id" => $item['id'],
                        ];
                    })->all();
                } else {

                }
            }
        }
        return $data;
    }
    public function LessOperatingExpensesAndEmp()
    {
        $trendPNL = $this->jsondata['trendPNL'];
        $income = $trendPNL['Less Operating Expenses'];
        foreach ($income as $key => $value) {
            /**get All Income values except SummeryRow**/
            if ($key !== 'summaryRow' && $key !== 'Employee Compensation & Related Expenses') {

            } else {

                foreach ($value as $ekey => $evalue) {
                    /**get All Income values except SummeryRow**/
                    if ($ekey !== 'summaryRow') {
                        $eamounts = [];
                        $egrouped = collect($evalue)->groupBy('id')->toArray();
                        foreach ($egrouped as $egkey => $egvalue) {
                            $ecollection = collect($egvalue);
                            $eamount = $ecollection->sum('amount');
                            $eamounts[$egkey] = $eamount;
                        }
                        $edata[$ekey] = collect($evalue)->unique('id')->map(function ($item) use ($eamounts) {
                            $eamount_data = $eamounts[$item['id']];
                            return [
                                "date" => $item['date'],
                                "amount" => round($eamount_data),
                                "month" => $item['month'],
                                "year" => $item['year'],
                                "id" => $item['id'],
                            ];
                        })->all();
                    }
                    unset($edata['Total Operating Expenses']);
                    $data[$key] = $edata;
                }

            }
        }
        $emprowData = $edata;
        $array3 = array();
        unset($emprowData['Total Operating Expenses']);

        foreach ($emprowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {

                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }
        }

        $data['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'] = collect($array3)->collapse()->toArray();

        return $data;
    }

    public function totalEmpComp($emprowData)
    {
        $array3 = array();
        // echo "<pre>";print_r($emprowData);exit;
        foreach ($emprowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }
        }
        foreach ($array3 as $key => $value) {
            unset($array3[$key][1]);
        }
        $finalArr['summaryRow']['Total Employee Compensation & Related Expenses'] = collect($array3)->collapse()->toArray();

        return $finalArr;
    }
    public function LessCostofSales()
    {
        $trendPNL = $this->jsondata['trendPNL'];
        $income = $trendPNL['Less Cost of Sales'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {
            foreach ($income as $key => $value) {
                /**get All Income values except SummeryRow**/
                if ($key !== 'summaryRow') {
                    $amounts = [];
                    $grouped = collect($value)->groupBy('id')->toArray();
                    foreach ($grouped as $gkey => $gvalue) {
                        $collection = collect($gvalue);
                        $amount = $collection->sum('amount');
                        $amounts[$gkey] = $amount;
                    }
                    $data[$key] = collect($value)->unique('id')->map(function ($item) use ($amounts) {
                        $amount_data = $amounts[$item['id']];
                        return [
                            "date" => $item['date'],
                            "amount" => round($amount_data),
                            "month" => $item['month'],
                            "year" => $item['year'],
                            "id" => $item['id'],
                        ];
                    })->all();
                }
            }
        }
        return $data;
    }

    public function totalLessCostofSales($lessCostRowData)
    {
        $array3 = array();
        foreach ($lessCostRowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }
        }
        foreach ($array3 as $key => $value) {
            unset($array3[$key][1]);
        }
        $finalArr['summaryRow']['Total Cost of Sales'] = collect($array3)->collapse()->toArray();
        return $finalArr;
    }

    public function totalLessOper($lessOPRowData)
    {
        $array3 = array();
        foreach ($lessOPRowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }
        }
        foreach ($array3 as $key => $value) {
            unset($array3[$key][1]);
        }
        $finalArr['summaryRow']['Total Operating Expenses'] = collect($array3)->collapse()->toArray();
        return $finalArr;
    }

    public function totalLessOperAndEmp($lesoperTotalData, $employeeTotal)
    {

        if (sizeof($lesoperTotalData) > 0) {
            unset($lesoperTotalData['no_expand']);
            $lesoperTotalDataArray = collect($lesoperTotalData['summaryRow']['Total Operating Expenses'])->groupBy('id')->toArray();
        }
        if (sizeof($employeeTotal) > 0) {
            unset($employeeTotal['no_expand']);
            $employeeTotalArray = collect($employeeTotal)->groupBy('id')->toArray();
        }

        $result = [];
        foreach ($lesoperTotalDataArray as $akey => $value) {
            if (is_array($value)) {
                $result[$akey]["no_expand"] = true;
                if (isset($akey)) {
                    if ($value[0]['id'] == $employeeTotalArray[$akey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] + ($employeeTotalArray[$akey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],

                    ];
                }
            }

            $result[] = $value;
        }
        foreach ($array3 as $key => $value) {
            unset($array3[$key][1]);
        }
        $finalArr['summaryRow']['Total Operating Expenses'] = collect($result)->collapse()->toArray();
        return $finalArr;
    }

    public function GrossProfit()
    {
        $trendPNL = $this->jsondata['trendPNL'];
        $grossProfit = $trendPNL['Gross Profit'];
        $filtered = collect($grossProfit)->reject(function ($value, $key) {
            return $key == 'no_expand';
        })->toArray();

        // $filtered->all();
        $grouped = collect($filtered)->groupBy('id')->toArray();
        foreach ($grouped as $gkey => $gvalue) {
            $collection = collect($gvalue);
            $amount = $collection->sum('amount');
            $amounts[$gkey] = $amount;
        }
        $data = collect($filtered)->unique('id')->map(function ($item) use ($amounts) {
            $amount_data = $amounts[$item['id']];
            return [
                "date" => $item['date'],
                "amount" => round($amount_data),
                "month" => $item['month'],
                "year" => $item['year'],
                "id" => $item['id'],
            ];
        })->toArray();
        $datacollect = collect($data)->put('no_expand', true)->toArray();
        return $datacollect;
    }
    public function OperatingProfit()
    {
        $trendPNL = $this->jsondata['trendPNL'];
        $grossProfit = $trendPNL['Operating Profit'];
        $filtered = collect($grossProfit)->reject(function ($value, $key) {
            return $key == 'no_expand';
        })->toArray();

        // $filtered->all();
        $grouped = collect($filtered)->groupBy('id')->toArray();
        foreach ($grouped as $gkey => $gvalue) {
            $collection = collect($gvalue);
            $amount = $collection->sum('amount');
            $amounts[$gkey] = $amount;
        }
        $data = collect($filtered)->unique('id')->map(function ($item) use ($amounts) {
            $amount_data = $amounts[$item['id']];
            return [
                "date" => $item['date'],
                "amount" => round($amount_data),
                "month" => $item['month'],
                "year" => $item['year'],
                "id" => $item['id'],
            ];
        })->toArray();
        $datacollect = collect($data)->put('no_expand', true)->toArray();
        return $datacollect;
    }
    public function NonOperatingIncome()
    {
        $trendPNL = $this->jsondata['trendPNL'];
        $income = $trendPNL['Non-operating Income'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {
            foreach ($income as $key => $value) {
                /**get All Income values except SummeryRow**/
                if ($key !== 'summaryRow') {
                    $amounts = [];
                    $grouped = collect($value)->groupBy('id')->toArray();
                    foreach ($grouped as $gkey => $gvalue) {
                        $collection = collect($gvalue);
                        $amount = $collection->sum('amount');
                        $amounts[$gkey] = $amount;
                    }
                    $data[$key] = collect($value)->unique('id')->map(function ($item) use ($amounts) {
                        $amount_data = $amounts[$item['id']];
                        return [
                            "date" => $item['date'],
                            "amount" => round($amount_data),
                            "month" => $item['month'],
                            "year" => $item['year'],
                            "id" => $item['id'],
                        ];
                    })->all();
                }
            }
        }

        return $data;
    }

    public function totalNonOperatingIncome($nonOperIncomeRowData)
    {
        $array3 = array();
        foreach ($nonOperIncomeRowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }
        }
        foreach ($array3 as $key => $value) {
            unset($array3[$key][1]);
        }
        $finalArr['summaryRow']['Total Non-operating Income'] = collect($array3)->collapse()->toArray();
        return $finalArr;
    }
    public function OperatingExpenses()
    {
        $trendPNL = $this->jsondata['trendPNL'];
        $income = $trendPNL['Operating Expenses'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {
            $data = array();
            foreach ($income as $key => $value) {
                if (is_array($value)) {
                    $amount = 0;
                    foreach ($value as $amt) {
                        $amount += $amt;
                        $data[$key] = $amount;
                    }
                } else {
                    $data[$key] = $value;
                }
            }
        }
        return $data;
    }

    public function NonOperatingExpenses()
    {
        $trendPNL = $this->jsondata['trendPNL'];
        $income = $trendPNL['Non-operating Expenses'];
        if (sizeof($income) == 0) {
            $data = $income;
        } else {
            foreach ($income as $key => $value) {
                /**get All Income values except SummeryRow**/
                if ($key !== 'summaryRow') {
                    $amounts = [];
                    $grouped = collect($value)->groupBy('id')->toArray();
                    foreach ($grouped as $gkey => $gvalue) {
                        $collection = collect($gvalue);
                        $amount = $collection->sum('amount');
                        $amounts[$gkey] = $amount;
                    }
                    $data[$key] = collect($value)->unique('id')->map(function ($item) use ($amounts) {
                        $amount_data = $amounts[$item['id']];
                        return [
                            "date" => $item['date'],
                            "amount" => round($amount_data),
                            "month" => $item['month'],
                            "year" => $item['year'],
                            "id" => $item['id'],
                        ];
                    })->all();
                }
            }
        }

        return $data;
    }

    public function totalNonOperatingExp($nonOperexpRowData)
    {
        $array3 = array();
        foreach ($nonOperexpRowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if (isset($array3[$arrkey])) {
                        if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                            $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                        }
                    } else {
                        $array3[$arrkey][0] = [
                            "id" => $arrvalue[0]['id'],
                            "amount" => $arrvalue[0]['amount'],
                            "date" => $arrvalue[0]['date'],
                            "month" => $arrvalue[0]['month'],
                            "year" => $arrvalue[0]['year'],

                        ];
                    }
                }
            }
        }
        foreach ($array3 as $key => $value) {
            unset($array3[$key][1]);
        }
        $finalArr['summaryRow']['Total Non-operating Expenses'] = collect($array3)->collapse()->toArray();
        return $finalArr;
    }

    public function totalGrossProfit($incomeTotalData, $lesscostTotalData)
    {
        if (sizeof($incomeTotalData) > 0) {
            unset($incomeTotalData['no_expand']);
            $incomeTotalDataArray = collect($incomeTotalData['summaryRow']['Total Income'])->groupBy('id')->toArray();
        }
        if (sizeof($lesscostTotalData) > 0) {
            unset($lesscostTotalData['no_expand']);
            $lesscostTotalDataArray = collect($lesscostTotalData['summaryRow']['Total Cost of Sales'])->groupBy('id')->toArray();
        }

        $result = [];
        foreach ($incomeTotalDataArray as $akey => $value) {
            if (is_array($value)) {
                $result[$akey]["no_expand"] = true;
                if (isset($akey)) {
                    if ($value[0]['id'] == $lesscostTotalDataArray[$akey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] - ($lesscostTotalDataArray[$akey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],

                    ];
                }
            }

            $result[] = $value;
        }

        $finalresult = collect($result)->collapse()->toArray();
        return $finalresult;
    }

    public function NetProfit()
    {
        $trendPNL = $this->jsondata['trendPNL'];
        $grossProfit = $trendPNL['Net Profit'];
        $filtered = collect($grossProfit)->reject(function ($value, $key) {
            return $key == 'no_expand';
        })->toArray();

        // $filtered->all();
        $grouped = collect($filtered)->groupBy('id')->toArray();
        foreach ($grouped as $gkey => $gvalue) {
            $collection = collect($gvalue);
            $amount = $collection->sum('amount');
            $amounts[$gkey] = $amount;
        }
        $data = collect($filtered)->unique('id')->map(function ($item) use ($amounts) {
            $amount_data = $amounts[$item['id']];
            return [
                "date" => $item['date'],
                "amount" => round($amount_data),
                "month" => $item['month'],
                "year" => $item['year'],
                "id" => $item['id'],
            ];
        })->toArray();
        $datacollect = collect($data)->put('no_expand', true)->toArray();
        return $datacollect;
    }
    
    public function array_merge_map_recursive($r1, $r2, $callback)
    {
        $merged = array();
        foreach ($r1 as $k => $v) {
            if (!isset($r2[$k])) {
                $merged[$k] = $v;
            } elseif (!is_array($v)) {
                $merged[$k] = $callback($k, $v, $r2[$k]);
            } else {
                $merged[$k] = $this->array_merge_map_recursive($v, $r2[$k], $callback);
            }
        }
        return $merged;
    }
    
    public function EBITA($grossProfit, $lesoperTotalData)
    {
        $group = new group();
        $db1 = new dashboardReport();
        // if (sizeof($grossProfit) > 0) {
        //     unset($grossProfit['no_expand']);
        //     $grossProfitArray = collect($grossProfit)->groupBy('id')->toArray();
        // }
        // if (sizeof($lesoperTotalData) > 0) {
        //     unset($lesoperTotalData['no_expand']);
        //     $lesoperTotalDataArray = collect($lesoperTotalData['summaryRow']['Total Operating Expenses'])->groupBy('id')->toArray();
        // }

        $month_arr_linechart = array();
        $startDate = date("Y-m-01", strtotime("-11 months"));
        $endDate = date('Y-m-d');
        $i = 0;
        while (strtotime($startDate) <= strtotime($endDate)) {
            $consolidate_month_arr[$i] = date('M', strtotime($startDate));
            $month_arr_linechart[$i] = date('n', strtotime($startDate)) . '-' . date('y', strtotime($startDate));
            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            $i++;
        }
        $grossArray = array();
        $lesoperArray = array();
        foreach ($month_arr_linechart as $key => $value) {
            $monthYear = explode('-', $value);
            $month = $monthYear[0];
            $year = $monthYear[1];

            $keyTI = $db1->searchIdFromArray($value, $grossProfit);
            if (sizeof($keyTI) > 0) {
                $grossArray[$key] = $grossProfit[$keyTI];
            } else {
                $grossArray[$key] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $value);
            }

            $lesoperkeyTI = $db1->searchIdFromArray($value, $lesoperTotalData['summaryRow']['Total Operating Expenses']);
            if (sizeof($lesoperkeyTI) > 0) {
                $lesoperArray[$key] = $lesoperTotalData['summaryRow']['Total Operating Expenses'][$lesoperkeyTI];
            } else {
                $lesoperArray[$key] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $value);
            }
        }

        if (sizeof($grossArray) > 0) {
            unset($grossArray['no_expand']);
            $grossProfitArray = collect($grossArray)->groupBy('id')->toArray();
        }

        if (sizeof($lesoperArray) > 0) {
            unset($lesoperArray['no_expand']);
            $lesoperTotalDataArray = collect($lesoperArray)->groupBy('id')->toArray();
        }
        $result = [];
        foreach ($grossProfitArray as $akey => $value) {
            if (is_array($value)) {
                $result[$akey]["no_expand"] = true;
                if (isset($akey)) {
                    if ($value[0]['id'] == $lesoperTotalDataArray[$akey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] - ($lesoperTotalDataArray[$akey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],

                    ];
                }
            }

            $result[] = $value;
        }
        $finalresult = collect($result)->collapse()->toArray();
        return $finalresult;
    }

    public function netProfit1($finalEBITDA, $nonOprincomeTotalData, $nonOprExpTotalData)
    {
        if (sizeof($finalEBITDA) > 0) {
            $finalEBITDAArray = collect($finalEBITDA)->groupBy('id')->toArray();
        }
        if (sizeof($nonOprincomeTotalData) > 0) {
            $nonOprincome = $nonOprincomeTotalData['summaryRow']['Total Non-operating Income'];
            $nonOprincomeTotalDataArray = collect($nonOprincome)->groupBy('id')->toArray();
        }
        if (sizeof($nonOprExpTotalData) > 0) {
            $nonOprExp = $nonOprExpTotalData['summaryRow']['Total Non-operating Expenses'];
            $nonOprExpTotalDataArray = collect($nonOprExp)->groupBy('id')->toArray();
        }

        $result = [];
        foreach ($finalEBITDAArray as $totalbankakey => $value) {
            if (is_array($value)) {
                if (isset($totalbankakey)) {
                    if ($value[0]['id'] == $nonOprincomeTotalDataArray[$totalbankakey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] + ($nonOprincomeTotalDataArray[$totalbankakey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],

                    ];
                }

            }

            $result[] = $value;
        }
        $finalresult = collect($result)->collapse()->toArray();
        $finalresultArray = collect($finalresult)->groupBy('id')->toArray();

        $result1 = [];
        foreach ($finalresultArray as $fixedassetsakey => $value) {
            if (is_array($value)) {
                if (isset($fixedassetsakey)) {
                    if ($value[0]['id'] == $nonOprExpTotalDataArray[$fixedassetsakey][0]['id']) {
                        $value[0]['amount'] = $value[0]['amount'] - ($nonOprExpTotalDataArray[$fixedassetsakey][0]['amount']);
                    }
                } else {
                    $value[0] = [
                        "id" => $value[0]['id'],
                        "amount" => $value[0]['amount'],
                        "date" => $value[0]['date'],
                        "month" => $value[0]['month'],
                        "year" => $value[0]['year'],

                    ];
                }
            }
            $result1[] = $value;
        }
        $finalresult1 = collect($result1)->collapse()->toArray();

        return $finalresult1;
    }
}
