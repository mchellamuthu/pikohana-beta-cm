<?php

  $DB_HOST = "localhost";
  $DB_NAME     = "pikohana_reportdb";
  $DB_USERNAME = "pikohana_user";
  $DB_PASSWORD = "pikohana_user";
  
	$connection = mysqli_connect($DB_HOST, $DB_USERNAME, $DB_PASSWORD, $DB_NAME) or die(mysqli_error());
	
		
	// GET DATA FROM MYSQL TABLE	
	$sql = "SELECT client_master_id, report_start_date FROM client_master_detail WHERE TIMESTAMPDIFF( MONTH , report_start_date, CURDATE( ) ) >=12";	
	
	 $result = mysqli_query($connection, $sql) or die(mysqli_error());  
	
	 $rows = mysqli_num_rows($result);

	 if ($rows >0) {
		while ($row = mysqli_fetch_array($result)){
			
			 $client_master_id = $row["client_master_id"];
				
			if(!empty($client_master_id)){
				updateReportStartDate($client_master_id, $connection);
			} 
		}
	 }	
	
	function updateReportStartDate($client_master_id, $connection){		
		//update database to set is_sent=1\n
		$sql = "UPDATE client_master_detail SET report_start_date=CURDATE() WHERE client_master_id=$client_master_id";	
		$result = mysqli_query($connection, $sql) or die(mysqli_error($connection)); 	
		if ($result) {
		    echo "$client_master_id updated successfully";
		    mysqli_close($connection);
		} else {
		    echo "Error updating record: " . mysqli_error($connection);
		}
		
	}
		
?>