<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
    require_once dirname(__FILE__) . '/DbConnect.php';
class dashboardReport extends utility {

   // private $conn;

    function __construct() {
    
        // opening db connection
        $this->db = new DbConnect();
       $this->conn = $this->db->connect();
    }

	public function doCurl($url, $params)
	{
		try {

			 header("Content-type: application/json");
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $url);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
              curl_setopt($ch, CURLOPT_TIMEOUT, 30);
              curl_setopt($ch, CURLOPT_POST, 1);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
              $result = curl_exec($ch);
              curl_close($ch);
			  $resp = json_decode($result, true);
            
              return $resp;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}
	}
	
	 /**
     * Fetching all industry_type
     *
     */
    public function getKPIData($client_id, $report_id) {
		try{
			
			$response = array();
			$searchItems['client_id'] = $client_id;
			//$searchItems['is_active'] = 1;
			$searchItems['report_id'] = $report_id;
			$objReportMap = new reportClientApplicationMap();
			$resp = array();
			$kpiResponse = array();
			$response = $objReportMap->getAllReportClientApplicationMaps($searchItems);
		
			if (sizeof($response) > 0) {
				if ($response['error'] == false) {
					foreach ($response['reportClientApplicationMapDetails'] as $k=>$v) {
						$company_name = $v['company_name'];
						if (strlen(trim($company_name)) > 0) {
							$path_url = KPI_ZOHO_FETCH_URL;
							$token = $v['report_auth_key'];
							$api_url = $v['application_api_url'] . $path_url;
							 $param= "authtoken=".$token."&scope=crmapi&criteria=(Company Name:".$company_name.")";
							$curlResult = $this->doCurl($api_url, $param);
							if (sizeof($curlResult) > 0) {
							//Check error code from curl if api url is not valid
								$curlResponse = isset($curlResult['response']['result']['CustomModule1']['row'][0]) ? $curlResult['response']['result']['CustomModule1']['row'][0]['FL'] : $curlResult['response']['result']['CustomModule1']['row']['FL'];
								
								if (sizeof($curlResponse) > 0) {
									foreach ($curlResponse as $k=>$v) {
										$key = str_replace(' ', '_', strtolower($v['val']));
										$key = preg_replace("/\([^)]+\)/","",$key);
										$resp[$key] = $v['content'];
									}
								} else {
									$kpiResponse['error']= true;
									$kpiResponse['message'] = NO_RECORD_FOUND;
									return $kpiResponse;
								}
							} else {
								$kpiResponse['error']= true;
								$kpiResponse['message'] = SERVER_NO_RESPONSE;
								return $kpiResponse;
							}
						} else {
							$kpiResponse['error']= true;
							$kpiResponse['message'] = CONFIGURE_COMPANY;
							return $kpiResponse;
						} 
					}
				}
			}
			
			$kpiResponse['kpiReport'] = $resp;
			if (sizeof($kpiResponse['kpiReport']) > 0) {
				$kpiResponse['error']= false;
				$kpiResponse['message'] = RECORD_FOUND;
				$kpiResponse["company_name"] = $company_name;
			} else {
				$kpiResponse['error']= true;
				$kpiResponse['message'] = NO_RECORD_FOUND;
			}
			return $kpiResponse; 
		} catch (Exception $e) {
			echo $e->getMessage();
		}
    }
	
	public function getWeightedPipeLine($response, $client_id, $application_id) {
		try{
			
			//$response = array();

			$resp = array();
			$pipelineResponse = array();
			$temp = array();
			$tmp = array();
			$aFormatted = array();
			foreach ($response as $k=>$v) {
				$company_name = $v['company_name'];
				$path_url = WEIGHTED_PIPELINE_ZOHO_FETCH_URL;
				$token = $v['auth_key'];
				$api_url = $v['application_api_url'] . $path_url;
				$param= "authtoken=".$token."&scope=crmapi&fromIndex=1&toIndex=200";
				$curlResult = $this->doCurl($api_url, $param);
				
				if (sizeof($curlResult) > 0) {
					$flArray = array();
					$aPipelineRow = array();
					$curlResponse = $curlResult['response']['result']['Potentials']['row'];
					if (((sizeof($curlResponse)) > 0) && (isset($curlResponse[0]['FL']))) {
						for ($i=0;$i<sizeof($curlResponse); $i++) {
							$flArray = $curlResponse[$i]['FL'];
							foreach ($flArray as $k=>$v) {
								$key = str_replace(' ', '_', strtolower($v['val']));
								$key = preg_replace("/\([^)]+\)/","",$key);
								if ($key == 'teritory')  $key = 'territory';
								$resp[$key] = $v['content'];
							}
								$aPipelineRow[$i]	= $resp;	
						}
					}
				} else {
					$pipelineResponse["error"] = true;
					$pipelineResponse["message"] = SERVER_NO_RESPONSE;
					return $pipelineResponse;
				} 
			}

			if (sizeof($aPipelineRow) > 0) {
				foreach ($aPipelineRow as $ky=>$vl) {
				//echo '<pre>'; print_r($vl);echo '</pre>';
					if ($vl["stage"] != 'Closed Won') {
						$tmp[$vl["territory"]][$vl["brand"]][] = $vl;
					}

				}

				foreach ($tmp as $k=>$v) {
				
					$temp["territory"] = $k;
					
					$i = 0;
					foreach ($v as $key=>$val) {
						$temp["brand"] = $key;
						$temp["total_amount"] = 0;
						foreach ($val as $k2=>$v2) {
							
							$temp["total_amount"] += ($v2["currency"] !="SGD") ? round($v2["expected_revenue"] / $v2["exchange_rate"]) : round($v2["expected_revenue"]);	
							$val[$k2]["amount_in_sgd"] = ($v2["currency"] !="SGD") ? round($v2["amount"] / $v2["exchange_rate"]) : round($v2["amount"]);
							$val[$k2]["expected_revenue_in_sgd"] = ($v2["currency"] !="SGD") ? round($v2["expected_revenue"] / $v2["exchange_rate"]) : round($v2["expected_revenue"]);
						}
						$temp["currency"] = "SGD";
						$temp["reports"] = $val;
						$aFormatted[] = $temp;
					}
					
				}
		
				$pipelineResponse['pipelineReport'] = $aFormatted;
				if (sizeof($pipelineResponse['pipelineReport']) > 0) {
					$pipelineResponse['error']= false;
					$pipelineResponse['message'] = RECORD_FOUND;
				} else {
					$pipelineResponse['error']= true;
					$pipelineResponse['message'] = NO_RECORD_FOUND;
				}
			} else {
				$pipelineResponse['error']= true;
				$pipelineResponse['message'] = NO_RECORD_FOUND;
			}
			return $pipelineResponse; 
		} catch (Exception $e) {
			echo $e->getMessage();
		}
    }
   
   
   public function getSalesOrders($response, $client_id, $application_id) {
		try{
			$temp = array();
			$tmp = array();
			$aFormatted = array();
			$salesOrdersResponse = array();
			
						
			foreach ($response as $k=>$v) {
				$company_name = $v['company_name'];
				$path_url = SALES_ORDERS_ZOHO_FETCH_URL;
				$token = $v['auth_key'];
				$api_url = $v['application_api_url'] . $path_url;
				$thisYear = date('Y'). '-01-01';
				$param= "authtoken=".$token."&scope=crmapi&fromIndex=1&toIndex=200&criteria=(SO Issued Date:THIS YEAR)";
			
				$curlResult = $this->doCurl($api_url, $param);
				if (sizeof($curlResult)>0) {
				
					$flArray = array();
					$aSalesOrderRow = array();
					$curlResponse = $curlResult['response']['result']['SalesOrders']['row'];
					if ((sizeof($curlResponse) > 0) && isset($curlResponse[0]['FL'])) {
						
						for ($i=0;$i<sizeof($curlResponse); $i++) {
							$flArray = $curlResponse[$i]['FL'];
								foreach ($flArray as $k=>$v) {
									$key = str_replace(' ', '_', strtolower($v['val']));
									$key = preg_replace("/\([^)]+\)/","",$key);
									if ($key == "so_issued_date") {
										if ($v["content"] >= $thisYear) {
											$resp[$key] = $v['content'];
										}
									} else {
										$resp[$key] = $v['content'];
									} 
									
									
									
								}
							$aSalesOrderRow[]	= $resp;
						}
					}
				} else {
					$salesOrdersResponse['error']= true;
					$salesOrdersResponse['message'] = SERVER_NO_RESPONSE;
					return $salesOrdersResponse;
				}
						 
			}
			
			$budgetSummarys = array();
			$budgetSummarys = $this->getTerritoryBrandWiseTotalIncome($client_id);
				// echo 'qqq'; print_r($budgetSummarys); echo '</pre>';
			if (sizeof($budgetSummarys["budgetDetails"]) > 0) {
				// echo '<pre>hai'; print_r($budgetSummarys); echo '</pre>'; die();
				$amount = 0;
				foreach ($budgetSummarys["budgetDetails"] as $ky=>$vy) {
					if (strlen(trim($vy["territory_name"])) > 0) {
						$amount = round(trim(str_replace('$', '', $vy["total_values"])));
						$budget[trim($vy["territory_name"])][trim($vy["brand_name"])] = $amount;
					}
				}
							
			}
			
			if (sizeof($aSalesOrderRow) > 0) {
				foreach ($aSalesOrderRow as $ky=>$vl) {
					
					if ($vl["status"] == "Delivered") {
						$total = round($vl["grand_total"] / $vl["exchange_rate"]);
						$vl["grand_total"] = ($vl["currency"] != DEFAULT_CURRENCY) ?  $total : round($vl["grand_total"]);
						$tmp[$vl["territory"]][$vl["brand"]][] = $vl;
					}
				} 
				
				if (sizeof($tmp) > 0) {
					foreach ($tmp as $k=>$v) {
						$temp["territory"] = $k;
						
						foreach ($v as $key=>$val) {
							$temp["brand"] = $key;
							$temp["reports"] = $val;
							$temp["actual_amount"] = 0; 
							$temp["budget"] = 0;
							$temp["variance"] = 0;
							$temp["varpercent"] = 0;
							foreach ($val as $k1=>$v1) {
								
							//	$total = round($v1["grand_total"] / $v1["exchange_rate"]);
								$temp["actual_amount"] += $v1["grand_total"];
								
							}
							
							$temp["budget"] = isset($budget[$k][$key]) ? $budget[$k][$key] : '0';
							$temp["variance"] = ($temp["budget"] != '0') ? round($temp["actual_amount"] - $temp["budget"]) : round($temp["actual_amount"]) ;
							$temp["varpercent"] = ($temp["budget"] != 0 || $temp["budget"] != 0.00) ? round($temp["variance"] / $temp["budget"], 2) . '%' : 0;
							$aFormatted[] = $temp;
						}
						
					}
				}
			}
			
	//	echo "FOARMTE"; echo '<pre>'; print_r($aFormatted); echo '</pre>'; die();
			$salesOrdersResponse['salesOrderReport'] = $aFormatted;
			if (sizeof($salesOrdersResponse['salesOrderReport']) > 0) {
				$salesOrdersResponse['error']= false;
				$salesOrdersResponse['message'] = RECORD_FOUND;
			} else {
				$salesOrdersResponse['error']= true;
				$salesOrdersResponse['message'] = NO_RECORD_FOUND;
			}
			return $salesOrdersResponse; 
		} catch (Exception $e) {
			echo $e->getMessage();
		}
    }
	
	
	 public function getTotalOfSales($weighPipeline, $salesOrder) {
		try{
			
			$aFormatted = array();
			$totalSalesOrdersResponse = array();
			
			$aWeighted = $weighPipeline[0]["pipelineReport"];
			$aSalesOrder = $salesOrder[0]["salesOrderReport"];
			foreach ($aWeighted as $k1=>$v1) { 
				$weighted[$v1["territory"]][$v1["brand"]] = $v1;
			
			}
		
			foreach ($aSalesOrder as $k => $v) {
				$tmp = array();
				$tmp["territory"] = $v["territory"];
				$tmp["brand"] = $v["brand"];
				$tmp["actual_amount"] = round($v["actual_amount"] +  $weighted[$tmp["territory"]][$tmp["brand"]]["total_amount"]);
				$tmp["budget_amount"] = round($v["budget"]);
				$tmp["variance"] = round($tmp["actual_amount"] - $tmp["budget_amount"]);
				$tmp["varpercent"] = ($tmp["budget_amount"] != 0 || $tmp["budget_amount"] != 0.00) ? round($tmp["variance"] / $tmp["budget_amount"], 2) . '%' : 0;
				$aFormatted[] = $tmp;
				
			}
			
		
			$totalSalesOrdersResponse['totalSalesOrder'] = $aFormatted;
			if (sizeof($totalSalesOrdersResponse['totalSalesOrder']) > 0) {
				$totalSalesOrdersResponse['error']= false;
				$totalSalesOrdersResponse['message'] = RECORD_FOUND;
			} else {
				$totalSalesOrdersResponse['error']= true;
				$totalSalesOrdersResponse['message'] = NO_RECORD_FOUND;
			}
			return $totalSalesOrdersResponse; 
		} catch (Exception $e) {
			echo $e->getMessage();
		}
    }
	
	public function getXeroKeys() {
		try {
			$objReportMap = new application();
			$resp = array();
			$searchItems = array();
			$searchItems['application_id'] = 1;
			$response = $objReportMap->getAllApplications($searchItems);
			
			if (sizeof($response) > 0) {
					if ($response['error'] == false) {
						foreach ($response['applicationDetails'] as $k=>$v) {
								$resp['customer_key'] = $v['customer_key'];
								$resp['secret_key'] = $v['secret_key'];
							}
							$resp['error'] = false;
							$resp['message'] = KEYS_FOUND;
					} else {
						$resp['error'] = true;
						$resp['message'] = NO_AUTH_KEYS;
						
					}
			} else {
				$resp = $response;
			}
			return $resp;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	public function configureSignature($keys) {
		try {
			$signatures = array (
				'consumer_key' => $keys['customer_key'],
				'shared_secret' => $keys['secret_key'],
				// API versions
				'core_version' => '2.0',
				'payroll_version' => '1.0',
				'file_version' => '1.0' 
					);

			if (XRO_APP_TYPE == "Private" || XRO_APP_TYPE == "Partner") {
				$signatures ['rsa_private_key'] = BASE_PATH . '/xero/certs/privatekey.pem';
				$signatures ['rsa_public_key'] = BASE_PATH . '/xero/certs/publickey.cer';
			}
			if (XRO_APP_TYPE == "Partner") {
				$signatures ['curl_ssl_cert'] = BASE_PATH . '/xero/certs/entrust-cert-RQ3.pem';
				$signatures ['curl_ssl_password'] = '1234';
				$signatures ['curl_ssl_key'] = BASE_PATH . '/xero/certs/entrust-private-RQ3.pem';
			}
		return $signatures;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	public function getAuthFromDB($client_id) {
		try {
				$sql = "SELECT client_id, oauth_token, oauth_token_secret, is_authorized, req_oauth_token, page_name FROM client_oauth_token WHERE client_id = ?";
			
			if ($stmt = $this->conn->prepare($sql)) {
				$stmt->bind_param("i", $client_id);
				$stmt->execute();			
				$stmt->store_result();
				//$stmt->close();
				if($stmt->num_rows > 0){
					$stmt->bind_result($client_id, $oauth_token, $oauth_token_secret, $is_authorized, $req_oauth_token, $page_name);
					while ($result = $stmt->fetch()) {
						$response["error"] = false;
						$response["message"] = RECORD_FOUND;
						$response["client_id"] = $client_id;
						$response["oauth_token"] = $oauth_token;
						$response["oauth_token_secret"] = $oauth_token_secret;
						$response['is_authorized'] = $is_authorized;
						$response['req_oauth_token'] = $req_oauth_token;
						$response['page_name'] = $page_name;
					}
				}else{
					$response["error"] = true;
					$response["message"] = NO_RECORD_FOUND;
				}
								
			} else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
			}	
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}		
	}
	
	public function getoAuthKeys($client_id) {
		try {
			$publicKeys = array();
			$publicKeys = $this->getXeroKeys();
			
			if (sizeof($publicKeys) > 0) {
				if ($publicKeys['error'] == false) {
					$aSignatures = array();
					$aSignatures = $this->configureSignature($publicKeys);
					$XeroOAuth = new XeroOAuth ( array_merge ( array (
						'application_type' => XRO_APP_TYPE,
						'oauth_callback' => utf8_encode(OAUTH_CALLBACK),
						'user_agent' => utf8_encode(XERO_ACCOUNT)
					), $aSignatures ) );
					
					$initialCheck = $XeroOAuth->diagnostics ();
					$checkErrors = count ( $initialCheck );
					if ($checkErrors > 0) {
						// you could handle any config errors here, or keep on truckin if you like to live dangerously
						foreach ( $initialCheck as $check ) {
							$response['error'] = true;
							$response['message'] = 'Error: ' . $check . PHP_EOL;
						}
					} else {
						$here = XeroOAuth::php_self ();
						$oAuthValues = array();
						$oAuthValues = $this->getAuthFromDB($client_id);
						if (sizeof($oAuthValues) > 0) {
							if ($oAuthValues['error'] == false) {
								$oAuthValues['XeroOAuth'] = $XeroOAuth;
								return $oAuthValues;
							} else {
								$oAuthValues['XeroOAuth'] = $XeroOAuth;
								$response = $oAuthValues;
							}
						} else {
							$response['error'] = true;
							$response['message'] = NO_AUTH_KEYS;
							$response['XeroOAuth'] = $XeroOAuth;
						}
						
					}
					
				} else {
					$response = $publicKeys;
				}
			} else {
				$response['error'] = true;
				$response['message'] = ISSUE_ON_KEYS;
			}
			return $response;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	 public function getAccessToken($client_id, $data, $oauthVerifier, $oauth_token, $org) {
		 try {
			$here = XeroOAuth::php_self ();
			$XeroOAuth = $data['XeroOAuth'];

			
			$XeroOAuth->config ['access_token'] = utf8_encode($data['req_oauth_token']);
			$XeroOAuth->config ['access_token_secret'] = utf8_encode($data['oauth_token_secret']);
			$code = $XeroOAuth->request ( 'GET', $XeroOAuth->url ( 'AccessToken', '' ), array (
				'oauth_verifier' => utf8_encode($oauthVerifier),
				'oauth_token' => utf8_encode($oauth_token)
			) );
			
			if ($XeroOAuth->response['code'] == 302)  {
				$response["error"] = true;
				$response["message"] = XERO_HAVING_PROBLEM;
				return $response;
			} 
			if ($XeroOAuth->response ['code'] == 200) {
				
				$response = $XeroOAuth->extract_params ( $XeroOAuth->response ['response'] );
				$oauth_new_token = $response['oauth_token'];
				$oauth_token_secret = $response['oauth_token_secret'];
				$resp = array();
				$response = $this->saveAuthKeys($client_id, $oauth_new_token, $oauth_token_secret, 1, $org, '', "");
				//header ( "Location: {$here}" );
			} else {
				$response['error'] = true;
				$response['message'] = $XeroOAuth;
			//	outputError ( $XeroOAuth );
			}
			return $response;
		 } catch (Exception $e) {
			echo $e->getMessage();
		}
	 }
	 
	 public function saveAuthKeys($client_id, $oauth_token, $oauth_token_secret, $is_authorized, $org, $req_oauth_token, $page_name) {
		try {
			$this->conn->autocommit(false);
			$date =  date("Y-m-d h:i:s");
			$is_active = 1;
			$response = array();
			$tokenResult = $this->getFieldByID('client_oauth_token', 'client_id', $client_id, 'oauth_token');
			if ($tokenResult["error"] == true) {
				$sql = "INSERT INTO client_oauth_token(client_id, oauth_token, oauth_token_secret, generated_date, is_authorized, req_oauth_token, page_name) values(?, ?, ?, ?,?, ?, ?)";
				$e=1;
			} else {
				if (strlen(trim($page_name)) <= 0) {
					$sql = "UPDATE client_oauth_token set oauth_token = ?, oauth_token_secret = ?, is_authorized = ?, generated_date = ?, org = ? where client_id = ?";
				} else {
					$sql = "UPDATE client_oauth_token set oauth_token = ?, oauth_token_secret = ?, is_authorized = ?, generated_date = ?, org = ?, page_name = ? where client_id = ?";
				}
				$e=2;
			}
				if ($stmt = $this->conn->prepare($sql)) {
					if ($e==1) {
						$stmt->bind_param("isssiss", $client_id, $oauth_token, $oauth_token_secret, $date, $is_authorized, $req_oauth_token, $page_name);
					} else {
						if (strlen(trim($page_name)) <= 0) {
							$stmt->bind_param("ssissi", $oauth_token, $oauth_token_secret, $is_authorized, $date, $org, $client_id);
						} else {
							$stmt->bind_param("ssisssi", $oauth_token, $oauth_token_secret, $is_authorized, $date, $org, $page_name, $client_id);
						}
					}
					$result = $stmt->execute();
					
					$stmt->close();
					if ($result) {
						$this->conn->commit();
						$response["error"] = false;
						$response["message"] = KEY_GENERATE_SUCCESS;
						
					} else {
						$response["error"] = true;
						$response["message"] = KEY_GENERATE_FAILURE;
					}
				} else {
					$response["error"] = true;
					$response["message"] = QUERY_EXCEPTION;
				}
			
		
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}	
    }

	
	 public function updateAuthorized($client_id, $is_authorized) {
		try {
			$this->conn->autocommit(false);
			$date =  date("Y-m-d h:i:s");
			$is_authorized = 1;
			$response = array();
			$sql = "UPDATE client_oauth_token set is_authorized = ?, generated_date = ? where client_id = ?";
			if ($stmt = $this->conn->prepare($sql)) {
				$stmt->bind_param("isi", $is_authorized, $date, $client_id);
			}  else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
			}
					$result = $stmt->execute();
					
					$stmt->close();
					if ($result) {
						$this->conn->commit();
						$response["error"] = false;
						$response["message"] = UPDATE_AUTHORIZE_SUCCESS;
						
					} else {
						$response["error"] = true;
						$response["message"] = UPDATE_AUTHORIZE_FAILURE;
					}
				
			
		
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}	
    }

	 
	  public function requestToken($client_id, $data, $page_name) {
		 try {
			 $response = array();
			
			 if (isset($data['XeroOAuth'])) {
				$here = XeroOAuth::php_self ();
				$XeroOAuth = $data['XeroOAuth'];
				$params = array (
					'oauth_callback' => OAUTH_CALLBACK
				);
		
		
				$response = $XeroOAuth->request ( 'GET', $XeroOAuth->url ( 'RequestToken', '' ), $params );
				if ($XeroOAuth->response['code'] == 302)  {
					$response["error"] = true;
					$response["message"] = XERO_HAVING_PROBLEM;
					return $response;
				}	
				if ($XeroOAuth->response ['code'] == 200) {
					
					$scope = "";
					$xResponse = array();
					$xResponse = $XeroOAuth->extract_params ( $XeroOAuth->response ['response'] );
					$oauth_token = $xResponse['oauth_token'];
					$oauth_token_secret = $xResponse['oauth_token_secret'];
					$resp = array();
					$response = $this->saveAuthKeys($client_id, $oauth_token, $oauth_token_secret, 0, "", $oauth_token, $page_name);
					if ($response['error'] == false) {
						$authurl = $XeroOAuth->url ( "Authorize", '' ) . "?oauth_token={$oauth_token}&scope=" . $scope;
						$response['authurl'] = $authurl;
						
					
					}
				} else {
					$response['error'] = true;
					$response['message'] = $XeroOAuth;
				}
			 } else {
				 $response = $data;
			 }
			
			return $response;
		 } catch (Exception $e) {
			echo $e->getMessage();
		}
	 }
	 
	function array_flatten($array) {

	   $return = array();
	   foreach ($array as $key => $value) {
		   if (is_array($value)){ $return = array_merge($return, $this->array_flatten($value));}
		   else {$return[$key] = $value;}
	   }
	   return $return;

	}
	
		
	public function getBankSummary($client_id, $XeroOAuth, $ajax, $params) {
		try {
				$bankSummary = array();
				$bankResponse = $XeroOAuth->request('GET', BANK_SUMMARY_API, array(), "", "json");
				if ($bankResponse['code'] == 302)  {
					$bankResponse["error"] = true;
					$bankResponse["message"] = XERO_HAVING_PROBLEM;
					return $bankResponse;
				}
				if ($bankResponse['code'] == 401) {
					if (strpos($bankResponse['response'], 'token_expired') !== false || strpos($bankResponse['response'], 'token_rejected') !== false) {
						if (isset($ajax) && ($ajax == true)) {
							$bankSummary["expiry"] = true;
							$bankSummary['error'] = true;
							$bankSummary['message'] = TOKEN_EXPIRED;
							return $bankSummary;
						} else {
							$result = $this->getoAuthKeys($client_id);
							$resp = $this->requestToken($client_id, $result, "");
							if (isset($resp['authurl'])) {
								$authurl = $resp['authurl'];
								header('Location:' . $authurl); die();
							}
						}
					}
				}
				if ($bankResponse['code'] == 200) {
					$totalCashOnHand = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
					$aSummaryList = array();
					$bankDetails = array();
					
					$aSummaryList = json_decode($XeroOAuth->response['response'], true);
					//echo '<pre>'; print_r($aSummaryList);echo '</pre>';
					$company_name = $aSummaryList['Reports'][0]['ReportTitles'][1];
					$bankDetails = $aSummaryList['Reports'][0]['Rows'];
					if (sizeof($bankDetails) > 0) {
						
						$rowDetails = $bankDetails[1]['Rows'];	
						foreach ($rowDetails as $k=>$v) {
							$total = 0;
							foreach ($v['Cells'] as $k1=>$v1) {
								switch ($k1) {
										case 0:
											$tmp['bank_account_name'] = $v1['Value'];
										break;
										case 1:
											$tmp['opening_balance'] = round($v1['Value']);
										break;
										case 2:
											$tmp['cash_received'] = round($v1['Value']);
										break;
										case 3:
											$tmp['cash_spent'] = round($v1['Value']);
										break;
										case 4:
											$tmp['fx_gain'] = round($v1['Value']);
										break;
										case 5:
											$tmp['closing_balance'] = round($v1['Value']);
											
										break;
										
									}

							} 
							$bankSummary["accountsSummary"]["org_name"] = $company_name;
							$bankSummary["accountsSummary"][] = $tmp;
							
							$bankSummary["error"] = false;
							$bankSummary["message"] = RECORD_FOUND;
						}
					}
				}
				$aSummaryList = null;
				$bankDetails = null;
				return $bankSummary;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	public function getBudgetSummary($client_id, $XeroOAuth, $ajax) {
		try {
				$budgetSummary = array();
				$budgetResponse = $XeroOAuth->request('GET', BUDGET_SUMMARY_API, array("periods"=>1), "", "json");
				if ($budgetResponse['code'] == 302)  {
					$budgetResponse["error"] = true;
					$budgetResponse["message"] = XERO_HAVING_PROBLEM;
					return $budgetResponse;
				}
				if ($budgetResponse['code'] == 401) {
					if (strpos($budgetResponse['response'], 'token_expired') !== false  || strpos($budgetResponse['response'], 'token_rejected') !== false) {
						if (isset($ajax) && ($ajax == true)) {
							$budgetSummary["expiry"] = true;
							$budgetSummary['error'] = true;
							$budgetSummary['message'] = TOKEN_EXPIRED;
							return $budgetSummary;
						} else {
							$result = $this->getoAuthKeys($client_id);
							$resp = $this->requestToken($client_id, $result, "");
							if (isset($resp['authurl'])) {
								$authurl = $resp['authurl'];
								header('Location:' . $authurl); die();
							}
						}
					}
				}
				if ($budgetResponse['code'] == 200) {
					$aSummaryList = array();
					$budgetDetails = array();
					
					$aSummaryList = json_decode($XeroOAuth->response['response'], true);
					$company_name = $aSummaryList['Reports'][0]['ReportTitles'][1];
					$budgetDetails = $aSummaryList['Reports'][0]['Rows'];
					if (sizeof($budgetDetails) > 0) {
						foreach ($budgetDetails as $k=>$v) {
							if ($v["RowType"] == "Section") {
								if (sizeof($v["Rows"]) > 0) {
									if (($v["Rows"][0]["RowType"] == "SummaryRow") && (sizeof ($v["Rows"][0]["Cells"]) > 0)) {
										$key = $v["Rows"][0]["Cells"][0]["Value"];
										$value = $v["Rows"][0]["Cells"][1]["Value"];
										$key = str_replace(" " , "_", strtolower($key));
										$tmp[$key] = $value;
									}
								}
							}
						}
					}	
					$budgetSummary["budgetSummary"] = $tmp;
					$budgetSummary["budgetSummary"]["org_name"] = $company_name;
					$budgetSummary["error"] = false;
					$budgetSummary["message"] = RECORD_FOUND;
				}
	
				$aSummaryList = null;
				$budgetDetails = null;
				return $budgetSummary;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	 
	public function getConsolidatedPNL($client_id, $XeroOAuth, $ajax, $params) {
		try {
				$aPNLReport = array();
				$trackParams = array();
				$aBrand = array();
				$aTerritory = array();
				if (sizeof($params) > 0) {
					if (isset($params["brand_id"])) {
						if ($params["brand_id"] != BRAND_ID) {
							$aBrand = $this->getCategoryIDFromTrack($client_id, $params["brand_id"], "Brand");
						} else {
							$aBrand["error"] = true;
						}
					}
					if (isset($params["territory_id"])) {
						if ($params["territory_id"] != TERRITORY_ID) {
							$aTerritory = $this->getCategoryIDFromTrack($client_id, $params["territory_id"], "Territory");
						}  else {
							$aTerritory["error"] = true;
						}
					}
				
					if (($aTerritory["error"] == false) && ($aBrand["error"] == false)) {
						$trackParams = array("trackingCategoryID"=> $aTerritory["category_id"], "trackingOptionID"=>$params["territory_id"], "trackingCategoryID2"=>$aBrand["category_id"], "trackingOptionID2"=>$params["brand_id"]);
					} else if (($aTerritory["error"] == false) && ($aBrand["error"] == true)) {
						$trackParams = array("trackingCategoryID"=> $aTerritory["category_id"], "trackingOptionID"=>$params["territory_id"]);
					} else if (($aTerritory["error"] == true) && ($aBrand["error"] == false)) {
						$trackParams =  array("trackingCategoryID"=>$aBrand["category_id"], "trackingOptionID"=>$params["brand_id"]);
					}
				}
				
				if (isset($params["mtd"]) && $params["mtd"] == 0) {
					$getDates = $this->getDayTillMonth();
					$dateArray = array("fromDate"=>$getDates[1]["firstdate"], "toDate"=>$getDates[sizeof($getDates)-1]["lastdate"]);
					if (sizeof($dateArray) > 0) {
						$trackParams = array_merge($trackParams, $dateArray);
					}
				}
				
				
				$pnlResponse = $XeroOAuth->request('GET', PROFIT_LOSS_API, $trackParams, "", "json");
				if ($pnlResponse['code'] == 302)  {
					$pnlResponse["error"] = true;
					$pnlResponse["message"] = XERO_HAVING_PROBLEM;
					return $pnlResponse;
				}
				if ($pnlResponse['code'] == 401) {
					if (strpos($pnlResponse['response'], 'token_expired') !== false || strpos($pnlResponse['response'], 'token_rejected') !== false) {
						if (isset($ajax) && ($ajax == true)) {
							$aPNLReport["expiry"] = true;
							$aPNLReport['error'] = true;
							$aPNLReport['message'] = TOKEN_EXPIRED;
							return $aPNLReport;
						} else {
							$result = $this->getoAuthKeys($client_id);
							$resp = $this->requestToken($client_id, $result, "");
							if (isset($resp['authurl'])) {
								$authurl = $resp['authurl'];
								header('Location:' . $authurl); die();
							}
						}
					}
				}
				if ($pnlResponse['code'] == 200) {
					$aPLSummaryList = array();
					$pnlDetails = array();
					$pnlSummary = array();
					$aPLSummaryList = json_decode($XeroOAuth->response['response'], true);
					
					$company_name = $aPLSummaryList['Reports'][0]['ReportTitles'][1];
					$pnlDetails = $aPLSummaryList['Reports'][0]['Rows'];
			
					if (sizeof($pnlDetails) > 0 && isset($pnlDetails[1])) {
						$budgetSummarys = array();
						$budget = array();
						if (!isset($params['brand_id']) || !isset($params['territory_id'] )) {
							$params["brand_id"] = BRAND_ID;
							$params["territory_id"] = TERRITORY_ID;
						}
						$params["groupBy"] = 1;
						
						$budgetSummarys = $this->getBudgetDetailsFromDB($client_id, $params);
					//	print_r($budgetSummarys); 
						if (sizeof($budgetSummarys["budgetSummary"]) > 0) {
							foreach ($budgetSummarys["budgetSummary"] as $ky=>$vy) {
								if (strlen(trim($vy["line_item"])) > 0) {
									if ($params["mtd"] == 1) {
										$month_name = $this->getCurrentMonthField(date('m'));
										if (trim($vy["line_type"]) == "Row") {
											$budget[trim($vy["item_head"])][trim($vy["line_item"])] = round($vy[$month_name]); // trim(str_replace('$', '', $vy["month_aug"]));
										} elseif (trim($vy["line_type"]) == "summaryRow") {
											$budget["summaryRow"][trim($vy["item_head"])] = round($vy[$month_name]);
										}
									} else {
										$budget[trim($vy["item_head"])][trim($vy["line_item"])] = round($vy["total_values"]);
									}
								}
								
							}
						}
		
						foreach($pnlDetails as $k=>$v) {
							switch ($v["RowType"]) {
								case "Section":
								foreach($v["Rows"] as $k1=>$v1) {
									$summary = array();
									$cellValues = array();
									if (sizeof($v1["Cells"]) > 0 ){
										if ($v1["RowType"] == "Row") {
											if (strlen($v["Title"]) > 0) {
											//	print_r($v["Title"]);
												if ($v["Title"] == LESS_OPERATING_EXPENSES) {
													$budgetTitle = "Operating Expenses";
											
												} elseif ($v["Title"] == "Non-operating Income") {
													$budgetTitle = "Other Income";
											
												} else {
													$budgetTitle = trim($v["Title"]);
												}
												if (isset($budget[$budgetTitle][$v1["Cells"][0]["Value"]])) {
													
													if (in_array( trim($v1["Cells"][0]["Value"]), array_keys($budget[$budgetTitle])))	 {
												
														$v1["Cells"][2]["Value"] = round($budget[$budgetTitle][$v1["Cells"][0]["Value"]]);
														
														unset($budget[$budgetTitle][$v1["Cells"][0]["Value"]]);	
													} else {
														$v1["Cells"][2]["Value"] = 0; //"-";
													}
												} else {
														$v1["Cells"][2]["Value"] = 0; //"-";
												}
												$v1["Cells"][3]["Value"] = 0.0;
												$v1["Cells"][4]["Value"] = 0.0;
												foreach($v1["Cells"] as $k2=>&$v2) {
													unset($v2["Attributes"]);
													if (($k2 % 2) == 0 && $k2 !=0) {
														
														$v1["Cells"][3]["Value"] = round(abs($v1["Cells"][1]["Value"]) - abs($v1["Cells"][2]["Value"]));
														if ($v1["Cells"][2]["Value"] != 0 || $v1["Cells"][2]["Value"] !='0.0') {
															$v1["Cells"][4]["Value"] = round(($v1["Cells"][3]["Value"] / abs($v1["Cells"][2]["Value"])) * 100); 
														} else {
															$v1["Cells"][4]["Value"] = 0;
														}
													}
			
												}
											} else {
												$v["Title"] =  trim($v1["Cells"][0]["Value"]);
												$key = $v1["Cells"][0]["Value"];
												if (isset($budget["summaryRow"][$v["Title"]])) {
													if (in_array($key, array_keys($budget["summaryRow"])))	 {
														$v1["Cells"][2]["Value"] = round($budget["summaryRow"][$key]);
													
													} else {
														$v1["Cells"][2]["Value"] = 0; //"-";
													}
												}	else {
													$v1["Cells"][2]["Value"] = 0;
												}
												$v1["Cells"][3]["Value"] = round($v1["Cells"][1]["Value"] - $v1["Cells"][2]["Value"]);
												if ($v1["Cells"][2]["Value"] != 0 || $v1["Cells"][2]["Value"] !='0.0') {
													$v1["Cells"][4]["Value"] = round(($v1["Cells"][3]["Value"] / $v1["Cells"][2]["Value"]) * 100); 
												} else {
													$v1["Cells"][4]["Value"] = 0;
												}
												$v1["Cells"]["no_expand"] = true;
												
											} 
											$pnlSummary[$v["Title"]][] = $v1["Cells"];
										
										} else if ($v1["RowType"] == "SummaryRow") {
												
											$key = trim($v1["Cells"][0]["Value"]);
											$total = 0;
											if (strlen(trim($v["Title"])) > 0) {
													$pnlSummary[$v["Title"]]["summaryRow"][] = $v1["Cells"];
											} else {
										
												if ($v1["Cells"][0]["Value"] != TOTAL_OPERATING_EXPENSES) {
													$pnlSummary[$v1["Cells"][0]["Value"]]["no_expand"] = true;
													$pnlSummary[$v1["Cells"][0]["Value"]][] = $v1["Cells"];
												} else {
													$pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][] = $v1["Cells"];
													 
												}

											}

										}
									}
								}
								break;

							}
						}
			
				$emp = "Employee Compensation & Related Expenses";
				foreach ($budget as $k2=>$v2) {
					
					$summaryBudget = 0;
					$varpercentage = 0;
					$new = 0;
					if ($k2 == "Operating Expenses") {
							$k2 = LESS_OPERATING_EXPENSES;
					} elseif ($k2 == "Other Income") {
							$k2 = "Non-operating Income";
					}
						foreach ($v2 as $k3=>$v3) {
							if ($v3 == 0) { 
								$varpercentage = 0;
							} else {
								$varpercentage = '-100';
							}
						
								$pnlSummary[$k2][] = array(array("Value"=>$k3), array("Value"=> 0), array("Value"=>$v3), array("Value"=> '-'.$v3), array("Value"=> $varpercentage));
							if (sizeof($pnlSummary[$k2]["summaryRow"]) <= 0) {
								$pnlSummary[$k2]["summaryRow"][] = array(array("Value"=>$k2), array("Value"=> 0), array("Value"=>0), array("Value"=> 0), array("Value"=> 0));
							}
				
						}
					
					
				}

				if (sizeof ($pnlSummary) > 0) {
				
					foreach ($pnlSummary as $k=>$v) {
						foreach ($v as $k1=>$v1) {
							$pnlSummary[$k]["summaryRow"][0][2]["Value"] += round($v1[2]["Value"]);
						}
											
						
							$pnlSummary[$k]["summaryRow"][0][3]["Value"] = round($pnlSummary[$k]["summaryRow"][0][1]["Value"] - $pnlSummary[$k]["summaryRow"][0][2]["Value"]);
							if ($pnlSummary[$k]["summaryRow"][0][2]["Value"] != '0') {
								$pnlSummary[$k]["summaryRow"][0][4]["Value"] = round(($pnlSummary[$k]["summaryRow"][0][3]["Value"] / $pnlSummary[$k]["summaryRow"][0][2]["Value"]) * 100);
							} else {
								$pnlSummary[$k]["summaryRow"][0][4]["Value"] = 0;
							}
						 
					}
					if (isset($pnlSummary[$emp])) {
						$pnlSummary[LESS_OPERATING_EXPENSES][$emp] = $pnlSummary[$emp];
						unset($pnlSummary[$emp]);
						if (sizeof ($pnlSummary[LESS_OPERATING_EXPENSES][$emp]) > 0) {
							$summate = round($pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][2]["Value"] + $pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][0][2]["Value"]);
							$pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][2]["Value"] = round($pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][2]["Value"] + $pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][0][2]["Value"]);
							$pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][3]["Value"] = round($pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][1]["Value"] - $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][2]["Value"]);
							if ($pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][2]["Value"] != '0') {
								$pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][4]["Value"] = round(($pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][3]["Value"] / $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][2]["Value"]) * 100);
							} else {
								$pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][4]["Value"] = 0;
							}
							$pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][0][3]["Value"] = round($pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][0][1]["Value"] - $pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][0][2]["Value"]);
							if ($pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][0][2]["Value"] != '0') {
								$pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][0][4]["Value"] = round(($pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][0][3]["Value"] / $pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][0][2]["Value"]) * 100);
							} else {
								$pnlSummary[LESS_OPERATING_EXPENSES][$emp]["summaryRow"][0][4]["Value"] = 0;
							}
						}
					}
					$netValues = array(GROSS_PROFIT, OPERATING_PROFIT, NET_PROFIT);
					foreach ($netValues as $k=>$v) {
						switch ($v) {
							case GROSS_PROFIT:
								$incomeValue = (isset($pnlSummary[INCOME])) ?  $pnlSummary[INCOME]["summaryRow"][0][2]["Value"] : 0;
								$saleValue = (isset($pnlSummary["Less Cost of Sales"])) ?  $pnlSummary["Less Cost of Sales"]["summaryRow"][0][2]["Value"] : 0;
								$pnlSummary[$v][0][2]["Value"] = $incomeValue - $saleValue;
								//	print_r($pnlSummary[$k]);
								break;
								case OPERATING_PROFIT:
									if (!isset($pnlSummary[LESS_OPERATING_EXPENSES][$emp])) {
										$grossValue = (isset($pnlSummary[GROSS_PROFIT])) ?  $pnlSummary[GROSS_PROFIT][0][2]["Value"] : 0;
										$lessOpValue = (isset($pnlSummary[LESS_OPERATING_EXPENSES])) ?  $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][2]["Value"]: 0;
										$pnlSummary[$v][0][2]["Value"] = $grossValue - $lessOpValue;
									} else {
										$pnlSummary[$v][0][2]["Value"] = $pnlSummary[GROSS_PROFIT][0][2]["Value"] - $pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][0][2]["Value"];
									}
									
								break;
								case NET_PROFIT:
									$operatingProfit = (isset($pnlSummary["Operating Profit"])) ?  $pnlSummary["Operating Profit"][0][2]["Value"] : 0;
									$nonOpIncomeValue = (isset($pnlSummary["Non-operating Income"])) ?  $pnlSummary["Non-operating Income"]["summaryRow"][0][2]["Value"] : 0;
									$nonOpExpenseValue = (isset($pnlSummary["Non-operating Expenses"])) ?  $pnlSummary["Non-operating Expenses"]["summaryRow"][0][2]["Value"] : 0;
									$pnlSummary[$v][0][2]["Value"] = $operatingProfit + $nonOpIncomeValue - $nonOpExpenseValue;
								break;	
								
							}
							$pnlSummary[$v][0][3]["Value"] = $pnlSummary[$v][0][1]["Value"] - $pnlSummary[$v][0][2]["Value"];
							if ($pnlSummary[$v][0][2]["Value"] != '0') {
								if ($pnlSummary[$v][0][3]["Value"] > 0) {
									$pnlSummary[$v][0][4]["Value"] = round(abs($pnlSummary[$v][0][3]["Value"] / $pnlSummary[$v][0][2]["Value"] * 100));
								} else {
									$pnlSummary[$v][0][4]["Value"] = round($pnlSummary[$v][0][3]["Value"] / $pnlSummary[$v][0][2]["Value"] * 100);
								}
							} else {
								$pnlSummary[$v][0][4]["Value"] = 0;
							}
						}
	
						$finalArray = array();
						$finalArray[INCOME] = (sizeof($pnlSummary[INCOME]) > 0) ? $pnlSummary[INCOME] : [];
						$finalArray["Less Cost of Sales"] = (sizeof($pnlSummary["Less Cost of Sales"]) > 0) ? $pnlSummary["Less Cost of Sales"] : [];
						$finalArray[GROSS_PROFIT] = (sizeof($pnlSummary[GROSS_PROFIT]) > 0) ?  $pnlSummary[GROSS_PROFIT] : [];
						$finalArray["Less Operating Expenses"] = (sizeof($pnlSummary[LESS_OPERATING_EXPENSES]) > 0) ?  $pnlSummary[LESS_OPERATING_EXPENSES] : [];
						$finalArray[OPERATING_PROFIT] = (sizeof($pnlSummary[OPERATING_PROFIT]) > 0) ? $pnlSummary[OPERATING_PROFIT] : [];
						$tmp[NON_OPERATING_INCOME] = (sizeof($pnlSummary[NON_OPERATING_INCOME]) > 0) ? $pnlSummary[NON_OPERATING_INCOME] : [];
						$tmp[NON_OPERATING_EXPENSE] = (sizeof($pnlSummary[NON_OPERATING_EXPENSE]) > 0) ? $pnlSummary[NON_OPERATING_EXPENSE] : [];
						if ((sizeof($tmp[NON_OPERATING_INCOME]) > 0) && sizeof($tmp[NON_OPERATING_EXPENSE] > 0 )) {
							$finalArray[TOTAL_OTHER_INCOME_EXPENSE] = array_merge($tmp[NON_OPERATING_INCOME], $tmp[NON_OPERATING_EXPENSE]);
							$finalArray[TOTAL_OTHER_INCOME_EXPENSE]["summaryRow"][0]= array(array("Value"=>TOTAL_OTHER_INCOME_EXPENSE), array("Value"=>($tmp[NON_OPERATING_INCOME]["summaryRow"][0][1]["Value"] + $tmp[NON_OPERATING_EXPENSE]["summaryRow"][0][1]["Value"])), array("Value"=>($tmp[NON_OPERATING_INCOME]["summaryRow"][0][2]["Value"] + $tmp[NON_OPERATING_EXPENSE]["summaryRow"][0][2]["Value"])), array("Value"=>($tmp[NON_OPERATING_INCOME]["summaryRow"][0][3]["Value"] + $tmp[NON_OPERATING_EXPENSE]["summaryRow"][0][3]["Value"])), array("Value"=> 0));
							if ( $finalArray[TOTAL_OTHER_INCOME_EXPENSE]["summaryRow"][0][2]["Value"] != 0) {
								if ($finalArray[TOTAL_OTHER_INCOME_EXPENSE]["summaryRow"][0][3]["Value"] > 0) {
									$finalArray[TOTAL_OTHER_INCOME_EXPENSE]["summaryRow"][0][4]["Value"] = round(abs($finalArray[TOTAL_OTHER_INCOME_EXPENSE]["summaryRow"][0][3]["Value"] / $finalArray[TOTAL_OTHER_INCOME_EXPENSE]["summaryRow"][0][2]["Value"] * 100));
								} else {
									$finalArray[TOTAL_OTHER_INCOME_EXPENSE]["summaryRow"][0][4]["Value"] = round($finalArray[TOTAL_OTHER_INCOME_EXPENSE]["summaryRow"][0][3]["Value"] / $finalArray[TOTAL_OTHER_INCOME_EXPENSE]["summaryRow"][0][2]["Value"] * 100);
								}
							} else {
								 $finalArray[TOTAL_OTHER_INCOME_EXPENSE]["summaryRow"][0][4]["Value"]  = 0;
							}
							unset($tmp[NON_OPERATING_INCOME]["summaryRow"]);
							unset($tmp[NON_OPERATING_EXPENSE]["summaryRow"]);
						} elseif ((sizeof($tmp[NON_OPERATING_INCOME]) > 0) && sizeof($tmp[NON_OPERATING_EXPENSE] <= 0 )) {
							$tmp[NON_OPERATING_INCOME]["summaryRow"][0][0]["Value"] = TOTAL_OTHER_INCOME_EXPENSE;
							$finalArray[TOTAL_OTHER_INCOME_EXPENSE] = $tmp[NON_OPERATING_INCOME];
						} elseif ((sizeof($tmp[NON_OPERATING_INCOME]) <= 0) && sizeof($tmp[NON_OPERATING_EXPENSE] > 0 )) {
							unset($tmp[NON_OPERATING_EXPENSE]["summaryRow"]);
							$tmp[NON_OPERATING_EXPENSE]["summaryRow"][0][0]["Value"] = TOTAL_OTHER_INCOME_EXPENSE;
							$finalArray[TOTAL_OTHER_INCOME_EXPENSE] = $tmp[NON_OPERATING_EXPENSE];
						} else {
							$finalArray[TOTAL_OTHER_INCOME_EXPENSE] = [];
						}
						$finalArray[NET_PROFIT] = (sizeof($pnlSummary[NET_PROFIT]) > 0) ? $pnlSummary[NET_PROFIT] : [];

						$aPNLReport["error"] = false;
						$aPNLReport["message"] = RECORD_FOUND;
						
						$pnlSummary["org_name"] = $company_name;
						$aPNLReport["consolidatedPNL"] = $finalArray;
				//	print_r($finalArray); die();
					}
				} else {
					$aPNLReport["error"] = true;
					$aPNLReport["message"] = NO_RECORD_FOUND;
				}
			}  else {
				$aPNLReport["error"] = true;
				$aPNLReport["message"] = NO_RECORD_FOUND;
			}
				$aPLSummaryList = null;
				$pnlDetails = null;
				$pnlSummary = null;
				return $aPNLReport;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	public function getContacts($client_id, $XeroOAuth) {
		try {
			$contactList = array();
			$response = $XeroOAuth->request('GET', $XeroOAuth->url('Contacts', 'core'), array(), "", "json");
			if ($response['code'] == 302)  {
				$response["error"] = true;
				$response["message"] = XERO_HAVING_PROBLEM;
				return $response;
			}
			if ($response['code'] == 401) {
				if (strpos($response['response'], 'token_expired') !== false  || strpos($response['response'], 'token_rejected') !== false) {
					$result = $this->getoAuthKeys($client_id);
					$resp = $this->requestToken($client_id, $result, "");
					if (isset($resp['authurl'])) {
						$authurl = $resp['authurl'];
						header('Location:' . $authurl); die();
					}
				}
			}
		   if ($response['code'] == 200) {
				$contacts = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
				$contactList = json_decode($XeroOAuth->response['response'], true); 
		   }
		   return $contactList;
			
		}  catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	public function getTerritoryAndBrand($client_id, $XeroOAuth, $ajax) {
		try {
			$territoryList = array();
			$categories = array();
			
			$response = $XeroOAuth->request('GET', $XeroOAuth->url('TrackingCategories', 'core'), array('IsActive'=>1), "", "json");
			if ($response['code'] == 302)  {
				$response["error"] = true;
				$response["message"] = XERO_HAVING_PROBLEM;
				return $response;
			}
			if ($response['code'] == 401) {
				if (strpos($response['response'], 'token_expired') !== false   || strpos($response['response'], 'token_rejected') !== false) {
					if (isset($ajax) && ($ajax == true)) {
						$categories["expiry"] = true;
						$categories['error'] = true;
						$categories['message'] = TOKEN_EXPIRED;
						return $categories;
					} else {
						$result = $this->getoAuthKeys($client_id);
						$resp = $this->requestToken($client_id, $result, "");
						if (isset($resp['authurl'])) {
							$authurl = $resp['authurl'];
							header('Location:' . $authurl); die();
						}
					}
				}
			}
		   if ($response['code'] == 200) {
			  $territoryList = json_decode($XeroOAuth->response['response'], true); 
			  if (sizeof($territoryList) > 0) {
				  $resp = array();
				 $resp = $this->insertTrackingCategories($client_id, $territoryList['TrackingCategories']);
				  $categories['territorybrand'] = $territoryList['TrackingCategories'];
				  $categories["error"] = false;
				  $categories["message"] = RECORD_FOUND;
			  }
		
		   }
		   return $categories;
		   
		}  catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	public function getCurrencies($client_id, $XeroOAuth, $ajax) {
		try {
			$currencyList = array();
			$currencies = array();
			
			$response = $XeroOAuth->request('GET', $XeroOAuth->url('Currencies', 'core'), array(), "", "json");
			if ($response['code'] == 302)  {
				$response["error"] = true;
				$response["message"] = XERO_HAVING_PROBLEM;
				return $response;
			}
			if ($response['code'] == 401) {
				if (strpos($response['response'], 'token_expired') !== false   || strpos($response['response'], 'token_rejected') !== false) {
					if (isset($ajax) && ($ajax == true)) {
						$currencies["expiry"] = true;
						$currencies['error'] = true;
						$currencies['message'] = TOKEN_EXPIRED;
						return $currencies;
					} else {
						$result = $this->getoAuthKeys($client_id);
						$resp = $this->requestToken($client_id, $result, "");
						if (isset($resp['authurl'])) {
							$authurl = $resp['authurl'];
							header('Location:' . $authurl); die();
						}
					}
				}
			}
		   if ($response['code'] == 200) {
			  $currencyList = json_decode($XeroOAuth->response['response'], true); 
			  if (sizeof($currencyList) > 0) {
				$resp = array();
				$currencies['currencyList'] = $currencyList['Currencies'];
				$currencies["error"] = false;
				$currencies["message"] = RECORD_FOUND;
			  }
		
		   }
		   return $currencies;
		   
		}  catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	public function getOrganisation($client_id, $XeroOAuth, $ajax) {
		try {
			$orgList = array();
			$organisations = array();
			
			$response = $XeroOAuth->request('GET', $XeroOAuth->url('Organisation', 'core'), array(), "", "json");
			if ($response['code'] == 302)  {
				$response["error"] = true;
				$response["message"] = XERO_HAVING_PROBLEM;
				return $response;
			}
			if ($response['code'] == 401) {
				if (strpos($response['response'], 'token_expired') !== false   || strpos($response['response'], 'token_rejected') !== false) {
					if (isset($ajax) && ($ajax == true)) {
						$organisations["expiry"] = true;
						$organisations['error'] = true;
						$organisations['message'] = TOKEN_EXPIRED;
						return $organisations;
					} else {
						$result = $this->getoAuthKeys($client_id);
						$resp = $this->requestToken($client_id, $result, "");
						if (isset($resp['authurl'])) {
							$authurl = $resp['authurl'];
							header('Location:' . $authurl); die();
						}
					}
				}
			}
		   if ($response['code'] == 200) {
			  $orgList = json_decode($XeroOAuth->response['response'], true); 
			  if (sizeof($orgList) > 0) {
				$resp = array();
				$organisations['orgDetail'] = $orgList['Organisations'];
				$resp = array();
			//	 $resp = $this->insertOrganisations($client_id,$organisations['orgList']);
				$organisations["error"] = false;
				$organisations["message"] = RECORD_FOUND;
			  }
		
		   }
		
		   return $organisations;
		   
		}  catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	public function insertTrackingCategories($client_id, $categories) {
		try {
			$isExist = $this->getFieldByID('territory_master', 'client_id', $client_id, 'category_id');
		
			if ($isExist["error"] == true) {
				$is_active = 1;
				$this->conn->autocommit(false);
			$categories[] = array("Name"=>"Territory", "TrackingCategoryID"=>"All", "Options"=>array(array("Name"=>"All", "TrackingOptionID"=>TERRITORY_ID)));
			$categories[] = array("Name"=>"Brand", "TrackingCategoryID"=>"All", "Options"=>array(array("Name"=>"All", "TrackingOptionID"=>BRAND_ID)));
		
		
				foreach ($categories as $k=>$v) {
					$category_id = $v["TrackingCategoryID"];
					switch ($v["Name"]) {
						case "Brand":
							$sql = "INSERT INTO brand_master (client_id, category_id, brand_id, brand_name, is_active) VALUES (?,?,?,?,?)";
						break;
						case "Territory":
							$sql = "INSERT INTO territory_master (client_id, category_id, territory_id, territory_name, is_active) VALUES (?,?,?,?,?)";
						break;	
					}
					foreach ($v["Options"] as $k1=>$v1) {
						if ($stmt = $this->conn->prepare($sql)) {
							$stmt->bind_param("isssi", $client_id, $category_id, $v1["TrackingOptionID"], $v1["Name"], $is_active);
							$result = $stmt->execute();
						
							$stmt->close();
							if ($result) {
								$this->conn->commit();
								$response["error"] = false;
								$response["message"] = ADD_CATEGORY_SUCCESS;
							} else {
								$response["error"] = true;
								$response["message"] = ADD_CATEGORY_FAILURE;
							}
						} else {
							$response["error"] = true;
							$response["message"] = QUERY_EXCEPTION;
						}
								
						
					}
					
				}
			}
			return $response;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	
	public function insertOrganisations($client_id, $organisations) {
		try {
			$isExist = $this->getFieldByID('client_organisation', 'client_id', $client_id, 'organisation_id'); 

			if ($isExist["error"] == true) {
				$is_active = 1;
				$this->conn->autocommit(false);
				foreach ($categories as $k=>$v) {
					$category_id = $v["TrackingCategoryID"];
					switch ($v["Name"]) {
						case "Brand":
							$sql = "INSERT INTO brand_master (client_id, category_id, brand_id, brand_name, is_active) VALUES (?,?,?,?,?)";
						break;
						case "Territory":
							$sql = "INSERT INTO territory_master (client_id, category_id, territory_id, territory_name, is_active) VALUES (?,?,?,?,?)";
						break;	
					}
					foreach ($v["Options"] as $k1=>$v1) {
						if ($stmt = $this->conn->prepare($sql)) {
							$stmt->bind_param("isssi", $client_id, $category_id, $v1["TrackingOptionID"], $v1["Name"], $is_active);
							$result = $stmt->execute();
							$stmt->close();
							if ($result) {
								$this->conn->commit();
								$response["error"] = false;
								$response["message"] = ADD_CATEGORY_SUCCESS;
							} else {
								$response["error"] = true;
								$response["message"] = ADD_CATEGORY_FAILURE;
							}
						} else {
							$response["error"] = true;
							$response["message"] = QUERY_EXCEPTION;
						}
								
						
					}
					
				}
			}
			return $response;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	public function getAgedPayables($client_id, $XeroOAuth, $ajax, $params) {
		try {
			$aPayableListing = array();
			$payResponse = $XeroOAuth->request('GET', INVOICE_API, array('Where' => 'Type=="ACCPAY" AND STATUS=="AUTHORISED"'), "", "json");
			if ($payResponse['code'] == 302)  {
				$payResponse["error"] = true;
				$payResponse["message"] = XERO_HAVING_PROBLEM;
				return $payResponse;
			}
			if ($payResponse['code'] == 401) {
				if (strpos($payResponse['response'], 'token_expired') !== false  || strpos($payResponse['response'], 'token_rejected') !== false) {
					if (isset($ajax) && ($ajax == true)) {
						$aPayableListing["expiry"] = true;
						$aPayableListing['error'] = true;
						$aPayableListing['message'] = TOKEN_EXPIRED;
						return $aPayableListing;
					} else {
						$result = $this->getoAuthKeys($client_id);
						$resp = $this->requestToken($client_id, $result, "");
						if (isset($resp['authurl'])) {
							$authurl = $resp['authurl'];
							header('Location:' . $authurl); die();
						}
					}
				}
			}
		   if ($payResponse['code'] == 200) {
				$payableList = array();
				
				$sum = array();
				$payableList = json_decode($XeroOAuth->response['response'], true);
				if (sizeof($payableList['Invoices']) > 0) {
					$payables = $payableList['Invoices'];
					$months = $this->lastThreeMonths();
					foreach ($payables as $k=>$v) {
						if ($v['Status'] == 'AUTHORISED') {
							$contactID = $v['Contact']['ContactID']; 
							$tmp[$contactID]['Name'] = $v['Contact']['Name'];
							if ($v['CurrencyCode'] != DEFAULT_CURRENCY) {
								$v['AmountDue'] = round($v['AmountDue'] / $v['CurrencyRate']); //$this->convertCurrency($v['AmountDue'], $v['CurrencyCode'], DEFAULT_CURRENCY);
							}
							if (strlen($tmp[$contactID]["Older"])<=0) $tmp[$contactID]["Older"] = 0;
							foreach ($months as $k2=>$v2) {
								if (strlen($tmp[$contactID][$v2])<=0) $tmp[$contactID][$v2] = 0;
								if (strlen($sum[$v2])<=0) $sum[$v2] = 0;
							}
							$month =  date("m",strtotime($v['DateString']));
							$monthText =  date("F",strtotime($v['DateString']));
							$year =  date("Y",strtotime($v['DateString']));
							$monthDiff =  date('m') - $month;
							if ($year == date('Y') && $monthDiff <=3 ) {
								$tmp[$contactID][$monthText] += $v['AmountDue'];
								$sum[$monthText]  += $v['AmountDue'];
							} else {
								$yearValue = "Older";
								$tmp[$contactID][$yearValue] += $v['AmountDue'];
								$sum[$yearValue]  += $v['AmountDue'];
							}
							$tmp[$contactID]["Total"] += $v['AmountDue'];
							$sum["Total"]  += $v['AmountDue'];
						}
						
					}
					$final = array();
					$sumHeader = array();
					$sumHeader['total_payables'] = $sum;
					$final[] = array_merge($tmp, $sumHeader);
					$aPayableListing["agedPayables"] = $final;
					$aPayableListing['error'] = false;
					$aPayableListing['message'] = RECORD_FOUND;
				}
					
		   }
					$final = null;
					$sumHeader = null;
					$payableList = null;
					$payResponse = null;
					
			return $aPayableListing;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	public function getAgedReceivables($client_id, $XeroOAuth, $ajax, $params) {
		try {
			$dt = new DateTime();
			
			$lastDate = $dt->format('Y-m-d'); 
			$aReceivableListing = array();
			$receiveResponse = $XeroOAuth->request('GET', INVOICE_API, array('Where' => 'Type=="ACCREC" AND STATUS=="AUTHORISED"'), "", "json");
		//	echo 'RESP'; print_r($receiveResponse); echo '</pre>'; die();
			if ($receiveResponse['code'] == 302)  {
				$receiveResponse["error"] = true;
				$receiveResponse["message"] = XERO_HAVING_PROBLEM;
				return $receiveResponse;
			}
			if ($receiveResponse['code'] == 401) {
				if (strpos($receiveResponse['response'], 'token_expired') !== false   || strpos($receiveResponse['response'], 'token_rejected') !== false) {
					if (isset($ajax) && ($ajax == true)) {
						$aReceivableListing["expiry"] = true;
						$aReceivableListing['error'] = true;
						$aReceivableListing['message'] = TOKEN_EXPIRED;
						return $aReceivableListing;
					} else {
						$result = $this->getoAuthKeys($client_id);
						$resp = $this->requestToken($client_id, $result, "");
						if (isset($resp['authurl'])) {
							$authurl = $resp['authurl'];
							header('Location:' . $authurl); die();
						}
					}
				}
			}
		   if ($receiveResponse['code'] == 200) {
				$receivableList = array();
				
				$receivableList = json_decode($XeroOAuth->response['response'], true);
				if (sizeof($receivableList['Invoices']) > 0) {
					$receivables = $receivableList['Invoices'];
					
					$months = $this->lastThreeMonths();
					$sum = array();
					foreach ($receivables as $k=>$v) {
						if ($v['Status'] == 'AUTHORISED') {
							$contactID = $v['Contact']['ContactID']; 
							$tmp[$contactID]['Name'] = $v['Contact']['Name'];
							if ($v['CurrencyCode'] != DEFAULT_CURRENCY) {
								$v['AmountDue'] = $v['AmountDue'] / $v['CurrencyRate']; //$this->convertCurrency($v['AmountDue'], $v['CurrencyCode'], DEFAULT_CURRENCY);
							}
							if (strlen($tmp[$contactID]["Older"])<=0) $tmp[$contactID]["Older"] = 0;
							foreach ($months as $k2=>$v2) {
								if (strlen($tmp[$contactID][$v2])<=0) $tmp[$contactID][$v2] = 0;
								if (strlen($sum[$v2])<=0) $sum[$v2] = 0;
							}
							$month =  date("m",strtotime($v['DateString']));
							$monthText =  date("F",strtotime($v['DateString']));
							$year =  date("Y",strtotime($v['DateString']));
							$monthDiff =  date('m') - $month;
							if ($year == date('Y') && $monthDiff <=3 ) {
								$tmp[$contactID][$monthText] += $v['AmountDue'];
								$sum[$monthText]  += $v['AmountDue'];
								
							} else {
								$yearValue = "Older";
								$tmp[$contactID][$yearValue] += $v['AmountDue'];
								$sum[$yearValue]  += $v['AmountDue'];
								
							}
							$tmp[$contactID]["Total"] += $v['AmountDue'];
							$sum["Total"]  += $v['AmountDue'];
						}
						
					}
					$final = array();
					$sumHeader = array();
					$sumHeader['total_receivables'] = $sum;
					$final[] = array_merge($tmp, $sumHeader);
					$aReceivableListing["agedReceivables"] = $final;
					$aReceivableListing['error'] = false;
					$aReceivableListing['message'] = RECORD_FOUND;
				}
					
		   }
				$final = null;
				$sumHeader = null;
				$receivableList = null;
				$receiveResponse = null;
					
			return $aReceivableListing;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	
	public function getEstimatedPayRoll($client_id, $XeroOAuth, $territoryListing, $ajax, $params) {
		try {
			$aEstimatedPayrollListing = array();
			$aTerritory = array();
			$aTerritory = $territoryListing[1];
			$tracking_category_id = $aTerritory['TrackingCategoryID'];
			$dates = $this->getDayTillMonth();
			
			$dtLength = count($dates);
			$fromDate = $dates[$dtLength-2]["firstdate"];
			$lastDate = $dates[$dtLength-2]["lastdate"];
		
			$estimatedResponse = $XeroOAuth->request('GET', PROFIT_LOSS_API, array("fromDate"=>$fromDate, "toDate"=>$lastDate, "trackingCategoryID"=>$tracking_category_id), "", "json");
			if ($estimatedResponse['code'] == 302)  {
				$estimatedResponse["error"] = true;
				$estimatedResponse["message"] = XERO_HAVING_PROBLEM;
				return $estimatedResponse;
			}
		//	echo '<pre>'; echo "ESIMATED"; print_r($estimatedResponse); echo '</pre>';  
			if ($estimatedResponse['code'] == 401) {
				if (strpos($estimatedResponse['response'], 'token_expired') !== false || strpos($estimatedResponse['response'], 'token_rejected') !== false) {
					if (isset($ajax) && ($ajax == true)) {
						$aEstimatedPayrollListing["expiry"] = true;
						$aEstimatedPayrollListing['error'] = true;
						$aEstimatedPayrollListing['message'] = TOKEN_EXPIRED;
						return $aEstimatedPayrollListing;
					} else {
						$result = $this->getoAuthKeys($client_id);
						$resp = $this->requestToken($client_id, $result, "");
						if (isset($resp['authurl'])) {
							$authurl = $resp['authurl'];
							header('Location:' . $authurl); die();
						}
					}
				}
			}
		   if ($estimatedResponse['code'] == 200) {
				$estimatedList = array();
				$final = array();
				
				$estimatedList = json_decode($XeroOAuth->response['response'], true);
				$success = false;
				if (sizeof($estimatedList['Reports']) > 0) {
					$company_name = $estimatedList['Reports'][0]['ReportTitles'][1];
					$estimatedPayroll = $estimatedList['Reports'][0]["Rows"];
				
					foreach ($estimatedPayroll as $k=>$v) {
						if ($v['RowType'] == "Header") {
							$final[] = $v['Cells'];
						}
						if ($v['Title'] == "Employee Compensation & Related Expenses") {
							foreach ($v['Rows'] as $k1=>$v1) {
								$final[] = $v1['Cells'];
							}
							$success = true;
						}
					}
				
					$aEstimatedPayrollListing["estimatedPayroll"]["org_name"] = $company_name;
					$aEstimatedPayrollListing["fromToDate"] = date('d-M-Y', strtotime($fromDate)) . " to " . date('d-M-Y', strtotime($lastDate));
					if ($success == true) {
						$aEstimatedPayrollListing["estimatedPayroll"] = $final;
						$aEstimatedPayrollListing['error'] = false;
						$aEstimatedPayrollListing['message'] = RECORD_FOUND;
					} else {
						$aEstimatedPayrollListing["estimatedPayroll"] = [];
						$aEstimatedPayrollListing['error'] = true;
						$aEstimatedPayrollListing['message'] = NO_RECORD_FOUND;
					}
				}
					
		   } 
		   
			return $aEstimatedPayrollListing;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	public function getBalanceSheet($client_id, $XeroOAuth, $ajax, $params) {
		try {
			$getDates = array();
			$getDates = $this->getDayTillMonth();
			$balanceSheetListing = array();
			$balanceSheet = array();
			$aBalanceSheet = array();
			$bsSummary = array();
			foreach ($getDates as $k => $v) {
				$balanceSheetResponse = $XeroOAuth->request('GET', BALANCE_SHEET_API, array("date"=> $v["lastdate"]) , "", "json");
				if ($balanceSheetResponse['code'] == 302)  {
					$balanceSheetResponse["error"] = true;
					$balanceSheetResponse["message"] = XERO_HAVING_PROBLEM;
					return $balanceSheetResponse;
			}	
				if ($balanceSheetResponse['code'] == 401) {
					if (strpos($balanceSheetResponse['response'], 'token_expired') !== false || strpos($balanceSheetResponse['response'], 'token_rejected') !== false) {
						$result = $this->getoAuthKeys($client_id);
						$resp = $this->requestToken($client_id, $result, "");
						if (isset($resp['authurl'])) {
							$authurl = $resp['authurl'];
							header('Location:' . $authurl); die();
						}
					}
					
					
				}
			
			   if ($balanceSheetResponse['code'] == 200) {
					$balanceSheet = array();
					$aBalanceSheet = json_decode($XeroOAuth->response['response'], true);
					$company_name = $aBalanceSheet['Reports'][0]['ReportTitles'][1];
					$balanceSheet = $aBalanceSheet['Reports'][0]['Rows'];
					if (sizeof($balanceSheet) > 0) {
						foreach($balanceSheet as $k1=>$v1) {
							switch ($v1["RowType"]) {
								case "Section":
									foreach($v1["Rows"] as $k2=>$v2) {
										if (sizeof($v2["Cells"]) > 0 ){
											if ($v2["RowType"] == "Row") {
												foreach($v2["Cells"] as $k3=>&$v3) {
													
													if ($k3 % 2 == 0 && strlen($v3["Value"]) > 0) {
													}  else {
														if (strlen(trim($v1["Title"])) > 0) {
														
																$bsSummary[$v1["Title"]][$v2["Cells"][$k3-1]["Value"]][$k] = $v3["Value"];
															
														} else {
															$bsSummary[$v2["Cells"][0]["Value"]]["no_expand"] = true;	
															$bsSummary[$v2["Cells"][0]["Value"]][$k] = $v2["Cells"][1]["Value"];
															
														}
													}
													unset($v3["Attributes"]);
											
												}
											} else if ($v2["RowType"] == "SummaryRow") {
												foreach($v2["Cells"] as $k3=>&$v3) {
													if (strlen(trim($v1["Title"])) > 0) {
													
														if ($v1["Title"] == "Equity") {
														
															$bsSummary[$v1["Title"]]["summaryRow"]["equity_gray"] = true; 
														}
														$bsSummary[$v1["Title"]]["summaryRow"][$v2["Cells"][0]["Value"]][$k] = $v2["Cells"][1]["Value"];
													} else {
														$bsSummary[$v2["Cells"][0]["Value"]]["no_expand"] = true;
														$bsSummary[$v2["Cells"][0]["Value"]][$k] = $v2["Cells"][1]["Value"];
														
													}
												}
											}
										}
									}
									break;
									
									
								}
							}
						}
					}
			}
			
			
			if (sizeof ($bsSummary) > 0) {
				$balanceSheetListing["error"] = false;
				$balanceSheetListing["message"] = RECORD_FOUND;
				$balanceSheetListing["balanceSheet"]["org_name"] = $company_name;
				$balanceSheetListing["balanceSheet"] = $bsSummary;
			}
			return $balanceSheetListing;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	
	public function getTrendPNL($client_id, $XeroOAuth, $ajax, $params) {
		try {
				$aPNLTrendReport = array();
				$getDates = array();
				$pnlResponse = array();
				$pnlSummary = array();
				$dateArray = array();
				$trackParams = array();
				$aBrand = array();
				$aTerritory = array();
				if (sizeof($params) > 0) {
					if (isset($params["brand_id"])) {
						if ($params["brand_id"] != BRAND_ID) {
							$aBrand = $this->getCategoryIDFromTrack($client_id, $params["brand_id"], "Brand");
						} else {
							$aBrand["error"] = true;
						}
					}
					if (isset($params["territory_id"])) {
						if ($params["territory_id"] != TERRITORY_ID) {
							$aTerritory = $this->getCategoryIDFromTrack($client_id, $params["territory_id"], "Territory");
						}  else {
							$aTerritory["error"] = true;
						}
					}
				
					if (($aTerritory["error"] == false) && ($aBrand["error"] == false)) {
						$trackParams = array("trackingCategoryID"=> $aTerritory["category_id"], "trackingOptionID"=>$params["territory_id"], "trackingCategoryID2"=>$aBrand["category_id"], "trackingOptionID2"=>$params["brand_id"]);
					} else if (($aTerritory["error"] == false) && ($aBrand["error"] == true)) {
						$trackParams = array("trackingCategoryID"=> $aTerritory["category_id"], "trackingOptionID"=>$params["territory_id"]);
					} else if (($aTerritory["error"] == true) && ($aBrand["error"] == false)) {
						$trackParams =  array("trackingCategoryID"=>$aBrand["category_id"], "trackingOptionID"=>$params["brand_id"]);
					}
				}
					
				$getDates = $this->getDayTillMonth();

				foreach ($getDates as $k => $v) {
				$dateArray = array("fromDate"=>$v["firstdate"], "toDate"=>$v["lastdate"]);
					if (sizeof($trackParams) > 0) {
						$dateArray = array_merge($dateArray, $trackParams);
					}
				
	
					$pnlResponse = $XeroOAuth->request('GET', PROFIT_LOSS_API, $dateArray, "", "json");
					if ($pnlResponse['code'] == 302)  {
						$pnlResponse["error"] = true;
						$pnlResponse["message"] = XERO_HAVING_PROBLEM;
						return $pnlResponse;
					}
					if ($pnlResponse['code'] == 401) {
						if (strpos($pnlResponse['response'], 'token_expired') !== false    || strpos($pnlResponse['response'], 'token_rejected') !== false) {
							if (isset($ajax) && ($ajax == true)) {
								$aPNLTrendReport["expiry"] = true;
								$aPNLTrendReport['error'] = true;
								$aPNLTrendReport['message'] = TOKEN_EXPIRED;
								return $aPNLTrendReport;
							} else {
								$result = $this->getoAuthKeys($client_id);
								$resp = $this->requestToken($client_id, $result, "");
								if (isset($resp['authurl'])) {
									$authurl = $resp['authurl'];
									header('Location:' . $authurl); die();
								}
							}
						}
					}
				
					if ($pnlResponse['code'] == 200) {
						$aPLSummaryList = array();
						$pnlDetails = array();
						$aPLSummaryList = json_decode($XeroOAuth->response['response'], true);
						$company_name = $aPLSummaryList['Reports'][0]['ReportTitles'][1];
						$pnlDetails = $aPLSummaryList['Reports'][0]['Rows'];
					 
						if (sizeof($pnlDetails) > 0) {
			//	echo "rEEPORT"; print_r($pnlDetails); 
							foreach($pnlDetails as $k1=>$v1) {
								switch ($v1["RowType"]) {
									case "Section":
									foreach($v1["Rows"] as $k2=>$v2) {
										if (sizeof($v2["Cells"]) > 0 ){
											if ($v2["RowType"] == "Row") {
												foreach($v2["Cells"] as $k3=>&$v3) {
													
													if ($k3 % 2 == 0 && strlen($v3["Value"]) > 0) {
													}  else {

														if (strlen(trim($v1["Title"])) > 0) {
															if ($v1["Title"] == EMPLOYEE_COMPENSATION) {
																$pnlSummary[LESS_OPERATING_EXPENSES][$v1["Title"]][$v2["Cells"][$k3-1]["Value"]][$k]= $v3["Value"];	
															} else {	
																if ($v1["Title"] == NON_OPERATING_INCOME || $v1["Title"] == NON_OPERATING_EXPENSE) $v1["Title"] = OTHER_INCOME_EXPENSE;
																$pnlSummary[$v1["Title"]][$v2["Cells"][$k3-1]["Value"]][$k] = $v3["Value"];
															}
															
															
														} else {
															$pnlSummary[$v2["Cells"][0]["Value"]]["no_expand"] = true;
															$pnlSummary[$v2["Cells"][0]["Value"]][$k] = $v3["Value"];
															
														}
													}
													unset($v3["Attributes"]);
											
												}
											} else if ($v2["RowType"] == "SummaryRow") {
												foreach($v2["Cells"] as $k3=>&$v3) {
													if (strlen(trim($v1["Title"])) > 0) {
														if ($v1["Title"] == EMPLOYEE_COMPENSATION) {
															$pnlSummary[LESS_OPERATING_EXPENSES][$v1["Title"]]["summaryRow"][$v2["Cells"][0]["Value"]][$k] = $v3["Value"];
														} 
														
														if ($v1["Title"] == OTHER_INCOME_EXPENSE) $v2["Cells"][0]["Value"] = TOTAL_OTHER_INCOME_EXPENSE;
															$pnlSummary[$v1["Title"]]["summaryRow"][$v2["Cells"][0]["Value"]][$k] += $v3["Value"];
														
														
													} else {
														if ($v2["Cells"][0]["Value"] != TOTAL_OPERATING_EXPENSES) {
															$pnlSummary[$v2["Cells"][0]["Value"]]["no_expand"] = true;
															$pnlSummary[$v2["Cells"][0]["Value"]][$k] = $v3["Value"];
														} else {
															$pnlSummary[LESS_OPERATING_EXPENSES]["summaryRow"][$v2["Cells"][0]["Value"]][$k] = $v3["Value"];
														}
													}
												}
											}
										}
									}
									break;
									
									
								}
							}
						}
					}
				}
			//	echo '<pre>'; print_r($pnlSummary); echo '</pre>'; die();
				if (sizeof ($pnlSummary) > 0) {
					$aPNLTrendReport["error"] = false;
					$aPNLTrendReport["message"] = RECORD_FOUND;
					$aPNLTrendReport["trendPNL"]["org_name"] = $company_name;
					$aPNLTrendReport["trendPNL"] = $pnlSummary;
				}	
				
				$aBrand = null;
				$aTerritory = null;
				$dateArray = null;
				$trackParams = null;
			
//	echo '<pre>'; print_r($pnlSummary); echo '</pre>'; 
				return $aPNLTrendReport;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	
	 public function getFlashReport($client_id, $data, $ajax, $params) {
		try {
				$a = array();
				$b = array();
				$c = array();
				$d = array();
				$e = array();
				$f = array();
				$g = array();
				$finalResponse = array();
				if (isset($data['XeroOAuth'])) {
					$XeroOAuth = $data['XeroOAuth'];
					$XeroOAuth->config ['access_token'] = utf8_encode($data['oauth_token']);
					$XeroOAuth->config ['access_token_secret'] = utf8_encode($data['oauth_token_secret']);

					$territoryBrand = array();
					$bankSummaryList = array();
					$agedReceivablesList = array();
					$agedPayablesList = array();
					$estimatedPayrollList = array();
					$currencies = array();
					$organisations = array();
					
					$territoryBrand = $this->getTerritoryAndBrand($client_id, $XeroOAuth, $ajax);
					if (isset($territoryBrand['expiry'])) {
						return $territoryBrand;
					} else {
						$a[] = $territoryBrand;
					}
					
					$bankSummaryList = $this->getBankSummary($client_id, $XeroOAuth, $ajax, $params);
					if (isset($bankSummaryList['expiry'])) {
						return $bankSummaryList;
					} else {
						$b[] = $bankSummaryList;
					}

					$agedPayablesList = $this->getAgedPayables($client_id, $XeroOAuth, $ajax, $params);
					if (isset($agedPayablesList['expiry'])) {
						return $agedPayablesList;
					} else {
						$c[] = $agedPayablesList;
					}
					
					$agedReceivablesList = $this->getAgedReceivables($client_id, $XeroOAuth, $ajax, $params);
					if (isset($agedReceivablesList['expiry'])) {
						return $agedReceivablesList;
					} else {
						$d[] = $agedReceivablesList;
					}
					
					$territory_brand = array();
					$territory_brand = $a[0]['territorybrand'];
					$estimatedPayrollList = $this->getEstimatedPayRoll($client_id, $XeroOAuth, $territory_brand, $ajax, $params); 
					if (isset($estimatedPayrollList['expiry'])) {
						return $estimatedPayrollList;
					} else {
						$e[] = $estimatedPayrollList;
					}
					
					$currencies = $this->getCurrencies($client_id, $XeroOAuth, $ajax);
					if (isset($currencies['expiry'])) {
						return $currencies;
					} else {
						$f[] = $currencies;
					}
					
					$organisations = $this->getOrganisation($client_id, $XeroOAuth, $ajax);
					if (isset($organisations['expiry'])) {
						return $organisations;
					} else {
						$g[] = $organisations;
					}
					
					$finalResponse = array_merge($a, $b,$c,$d,$e, $f, $g); 
					$finalResponse["error"] = false;
					$finalResponse["message"] = RECORD_FOUND;
				} else {
					$finalResponse["error"] = true;
					$finalResponse["message"] = XERO_AUTH_PROBLEM;
				}

			  return $finalResponse;
		 } catch (Exception $e) {
			echo $e->getMessage();
		}
	 }
	 
	 public function getPNLReport($client_id, $data, $ajax, $params) {
		try {
				$a = array();
				$b = array();
				$c = array();
				$d = array();
				$e = array();
				$f = array();
				$g = array();
				$finalResponse = array();
				if (isset($data['XeroOAuth'])) {
					$XeroOAuth = $data['XeroOAuth'];
					$XeroOAuth->config ['access_token'] = utf8_encode($data['oauth_token']);
					$XeroOAuth->config ['access_token_secret'] = utf8_encode($data['oauth_token_secret']);

					$territoryBrand = array();
					$aBalanceSheet = array();
					$aPNLList = array();
					$aPNLTrend = array();
					$currencies = array();
					
					$territoryBrand = $this->getTerritoryAndBrand($client_id, $XeroOAuth, $ajax);
					if (isset($territoryBrand['expiry'])) {
						return $territoryBrand;
					} else {
						$a[] = $territoryBrand;
					}
					
					
					
					$aBalanceSheet = $this->getBalanceSheet($client_id, $XeroOAuth, $ajax, $params);
						
					if (isset($aBalanceSheet['expiry'])) {
						return $aBalanceSheet;
					} else {
						$b[] = $aBalanceSheet;
					}
		
					$aPNLList = $this->getConsolidatedPNL($client_id, $XeroOAuth, $ajax, $params);
					if (isset($aPNLList['expiry'])) {
						return $aPNLList;
					} else {
						$c[] = $aPNLList;
					}
					
					$aPNLTrend = $this->getTrendPNL($client_id, $XeroOAuth, $ajax, $params); 
					if (isset($aPNLTrend['expiry'])) {
						return $aPNLTrend;
					} else {
						$d[] = $aPNLTrend;
					}
					
					$currencies = $this->getCurrencies($client_id, $XeroOAuth, $ajax);
					if (isset($currencies['expiry'])) {
						return $currencies;
					} else {
						$e[] = $currencies;
					}
					
					if ((sizeof($aPNLTrend) > 0) && (sizeof($aBalanceSheet) > 0)) {
						if (($aPNLTrend["error"] == false) && ($aBalanceSheet["error"] == false)) {
							$f[] = array("cashFlowStatement"=>$this->getCashFlowStatement($aPNLTrend, $aBalanceSheet));
						} else {
							$f[] = $aPNLTrend;
						}
					} else {
						$f[] = array("error"=>true, "message"=> NO_RECORD_FOUND);
					
					}
					
					$finalResponse = array_merge($a, $b, $c, $d, $e, $f); 
					$finalResponse["error"] = false;
					$finalResponse["message"] = RECORD_FOUND;
				} else {
					$finalResponse["error"] = true;
					$finalResponse["message"] = XERO_AUTH_PROBLEM;
				}
			  return $finalResponse;
		 } catch (Exception $e) {
			echo $e->getMessage();
		}
	 }
	 
	 public function getCashFlowStatement($profitNLoss, $balanceSheet) {
		 try {
			 
			// echo 'PROFIT N LOSS <pre>'; print_r($profitNLoss); echo '</pre>';
			
			 $getDates = $this->getDayTillMonth();
		 
			$response = array();
			$netIncome = array();
			$depreciation = array();
			$goodWill = array();
			$disposal = array();
			$tradeReceivable = array();
			$totalCurrentAssets = array();
			$netAssets = array();
			$totalLiabilities = array();
			$netCashFlow = array();
			$purchasePropertyEquipment = array();
			$advancesFromParentCompany = array();
			$proceedsFromPlant = array();
			$totalCashFromFinanceActivities = array();
			$netIncreaseInCash = array();
			$cashAndCashEq = array();
			$netCashEquivalent = array();
			$finalGoodWill = array();
			$finalTradeReceivable = array();
			$finalNetAssets = array();
			$finalCurrentLiabilities = array();
			$finalCurrentAssets = array();
			$finalAdvances = array();
			$finalProceedsFromPlant = array();
			$fixedAssets = array();
			
			$netIncome = $profitNLoss["trendPNL"]["Net Profit"];
			$depreciation = $profitNLoss["trendPNL"][OTHER_INCOME_EXPENSE]["Depreciation & Amortisation"];
			$goodWill = $balanceSheet["balanceSheet"]["Non-current Assets"]["Goodwill"];
			$disposal = $profitNLoss["trendPNL"][OTHER_INCOME_EXPENSE]["Gain on disposal of plant & equipment"];
			$tradeReceivable = $balanceSheet["balanceSheet"]["Current Assets"]["Accounts Receivable"];
			
			$totalCurrentAssets = $balanceSheet["balanceSheet"]["Current Assets"]["summaryRow"]["Total Current Assets"];
			$totalLiabilities = $balanceSheet["balanceSheet"]["Current Liabilities"]["summaryRow"]["Total Current Liabilities"];
			$fixedAssets = $balanceSheet["balanceSheet"]["Fixed Assets"];
			if (sizeof($fixedAssets) > 0) {
				$i = 0;	
				foreach ($fixedAssets as $k1=>$v1) {
					if (($i%2==0) && ($k1 != "summaryRow")) {
						foreach ($v1 as $k2=>$v2) {
							$purchasePropertyEquipment[$k2]+=$v2;
						}
					}
					$i++;
				}
			}
		
			$advancesFromParentCompany = $balanceSheet["balanceSheet"]["Equity"]["Owner Funds Introduced"];
			$abcCash = $balanceSheet["balanceSheet"]["Bank"]["summaryRow"]["Total Bank"];
			if (sizeof($abcCash) > 0) {
				for($i=0;$i<sizeof($abcCash); $i++) {
					$cashAndCashEq[0] = $abcCash[0];
					$cashAndCashEq[$i+1] = $abcCash[$i];
				}
			}
		
			//echo '<pre>'; print_r($cashAndCashEq); echo '</pre>'; die();	
		
			 $a = 0;
			 $b = 0;
			 $c = 0;
			 $d = 0;
			 $e = 0;
			 $f = 0;
			 $g = 0;
			 $h = 0;
			 $i = 0;
			 $j = 0;
			 $l = 0;
			 
			foreach ($getDates as $k=>$v) {
				if ((sizeof($netIncome) > 0) && ($a == 0)) {
					if (!isset($netIncome[$k])) {
						$netIncome[$k] = 0;
					}
				} else {
					$a = 1;
					$netIncome[$k] = '-';
				}
				if ((sizeof($depreciation) > 0) && ($b == 0)) {
					if (!isset($depreciation[$k])) {
						$depreciation[$k] = 0;
					}
				} else {
					$b = 1;
					$depreciation[$k] = '-';
				}
				if ((sizeof($disposal) > 0) && ($c == 0)) {
					if (!isset($disposal[$k])) {
						$disposal[$k] = 0;
					}
				}  else {
					$c = 1;
					$disposal[$k] = '-';
				}
				if ((sizeof($goodWill) > 0) && ($d == 0)) {
		
					if (!isset($goodWill[$k])) {
						$goodWill[$k] = '-';
						$finalGoodWill[$k] = $goodWill[$k-1] - $goodWill[$k];
					} else {
						if ($k != 0) {
							$finalGoodWill[$k] = $goodWill[$k-1] - $goodWill[$k];
						} else {
							$finalGoodWill[$k] = $goodWill[$k];
						}
					}
				} else {
					$d = 1;
					$goodWill[$k] = '-';
					$finalGoodWill[$k] = $goodWill[$k-1] - $goodWill[$k];
				}
				if ((sizeof($tradeReceivable) > 0) && ($e == 0)) {
					if (!isset($tradeReceivable[$k])) {
						$tradeReceivable[$k] = '-';
						$finalTradeReceivable[$k] = $tradeReceivable[$k-1] - $tradeReceivable[$k];
					} else {
						if ($k != 0) {
							$finalTradeReceivable[$k] = $tradeReceivable[$k-1] - $tradeReceivable[$k];
						} else {
							$finalTradeReceivable[$k] = $tradeReceivable[$k];
						}
					}
				} else {
					$e = 1;
					$tradeReceivable[$k] = '-';
					$finalTradeReceivable[$k] = $tradeReceivable[$k-1] - $tradeReceivable[$k];
				}
				if ((sizeof($totalCurrentAssets) > 0) && ($f == 0)) {
					if (!isset($totalCurrentAssets[$k])) {
						$totalCurrentAssets[$k] = 0;
					}
				} else {
					$f = 1;
					$totalCurrentAssets[$k] = '-';
				}
				if ($totalCurrentAssets[$k] != '-' && $tradeReceivable[$k] != '-') {
					$netAssets[$k] = $totalCurrentAssets[$k] - $tradeReceivable[$k];
				} else {
					$netAssets[$k]  = '-';
				}
				
				$finalNetAssets[$k] = $netAssets[$k-1] - $netAssets[$k];
				
				if ((sizeof($totalLiabilities) > 0) && ($g == 0)) {
					if (!isset($totalLiabilities[$k])) {
						$totalLiabilities[$k] = '-';
						$finalCurrentLiabilities[$k] = $totalLiabilities[$k] - $totalLiabilities[$k-1];
					} else {
						if ($k != 0) {
							$finalCurrentLiabilities[$k] = $totalLiabilities[$k] - $totalLiabilities[$k-1];
						} else {
							$finalCurrentLiabilities[$k] = $totalLiabilities[$k];
						}
					}
				} else {
					$g = 1;
					$totalLiabilities[$k] = '-';
					$finalCurrentLiabilities[$k] = $totalLiabilities[$k] - $totalLiabilities[$k-1];
				}
			
				
				$netCashFlow[$k] =$netIncome[$k] + $depreciation[$k] + $disposal[$k] + $finalGoodWill[$k] + $finalTradeReceivable[$k] + $finalNetAssets[$k] + $finalCurrentLiabilities[$k]; 
				if ((sizeof($purchasePropertyEquipment) > 0) && ($h == 0)) {
					if (!isset($purchasePropertyEquipment[$k])) {
						$purchasePropertyEquipment[$k] = '-';
						$finalCurrentAssets[$k] = $purchasePropertyEquipment[$k-1] - $purchasePropertyEquipment[$k];
					} else {
						if ($k != 0) {
							$finalCurrentAssets[$k] = $purchasePropertyEquipment[$k-1] - $purchasePropertyEquipment[$k];
						} else {
							$finalCurrentAssets[$k] = $purchasePropertyEquipment[$k];
						}
					}
				} else {
					$h = 1;
					$purchasePropertyEquipment[$k] = '-';
					$finalCurrentAssets[$k] = $purchasePropertyEquipment[$k-1] - $purchasePropertyEquipment[$k];
				}
				
				
				
				if ((sizeof($advancesFromParentCompany) > 0) && ($i == 0)) {
					if (!isset($advancesFromParentCompany[$k])) {
						$advancesFromParentCompany[$k] = '-';
						$finalAdvances[$k] = $advancesFromParentCompany[$k] - $advancesFromParentCompany[$k-1];
					} else {
						if ($k != 0) {
							$finalAdvances[$k] = $advancesFromParentCompany[$k] - $advancesFromParentCompany[$k-1];
						} else {
							$finalAdvances[$k] = $advancesFromParentCompany[$k];
						}
					}
				} else {
					$i = 1;
					$advancesFromParentCompany[$k] = '-';
					$finalAdvances[$k] = $advancesFromParentCompany[$k] - $advancesFromParentCompany[$k-1];
				}
				
				if ((sizeof($proceedsFromPlant) > 0) && ($j == 0)) {
					if (!isset($proceedsFromPlant[$k])) {
						$proceedsFromPlant[$k] = '-';
						$finalProceedsFromPlant[$k] = $proceedsFromPlant[$k-1] - $proceedsFromPlant[$k];
					} else {
						if ($k != 0) {
							$finalProceedsFromPlant[$k] = $proceedsFromPlant[$k-1] - $proceedsFromPlant[$k];
						} else {
							$finalProceedsFromPlant[$k] = $proceedsFromPlant[$k];
						}
					}
				
				} else {
					$j = 1;
					$proceedsFromPlant[$k] = '-';
					$finalProceedsFromPlant[$k] = $proceedsFromPlant[$k-1] - $proceedsFromPlant[$k];
				}
				$totalCashFromFinanceActivities[$k] = $finalAdvances[$k] + $finalProceedsFromPlant[$k];
				$netIncreaseInCash[$k] = $netCashFlow[$k] + $finalCurrentAssets[$k] + $totalCashFromFinanceActivities[$k]; 
				
				if ((sizeof($cashAndCashEq) > 0) && ($l == 0)) {
					if (!isset($cashAndCashEq[$k])) {
						$cashAndCashEq[$k] = 0;
					} 
					
				} else {
					$l = 1;
					$cashAndCashEq[$k] = '-';
				}
				
				$netCashEquivalent[$k] = $cashAndCashEq[$k] + $netIncreaseInCash[$k];
			}
			
			$response["Cash Flows from Operating Activities"][]= array("title"=> "Net Income", "value" => $netIncome);
			$response["Cash Flows from Operating Activities"][] = array("title"=>"Depreciation & amortization expense", "value" => $depreciation);
			$response["Cash Flows from Operating Activities"][] = array("title"=>"Gain on disposal of plant & equipment", "value" => $disposal);
			$response["Cash Flows from Operating Activities"][] = array("title"=>"Write off of intangible assets", "value" => $finalGoodWill);
			$response["Cash Flows from Operating Activities"][] = array("title"=>"Trade receivables", "value" => $finalTradeReceivable);
			$response["Cash Flows from Operating Activities"][] = array("title"=> "Other Assets", "value"=>$finalNetAssets);
			$response["Cash Flows from Operating Activities"][] = array("title"=> "Trade & other payables", "value" => $finalCurrentLiabilities);
			$response["Cash Flows from Operating Activities"]["summaryRow"] = array("title"=>"Net Cash Flow from operating activities", "value"=> $netCashFlow);
			$response["Cash Flows from Investing Activities"][] = array("title"=>"Purchase of property, plant and equipment", "value"=> $finalCurrentAssets);
			$response["Cash Flows from Investing Activities"]["summaryRow"] = array("title"=>"Total Cash Flows from investing activities", "value"=> $finalCurrentAssets);
			$response["Cash Flows from Financial Activities"][] = array("title"=>"Advances from parent company" ,"value" => $finalAdvances);
			$response["Cash Flows from Financial Activities"][] = array("title"=>"Proceeds from sale of plant & equipment" ,"value" => $finalProceedsFromPlant);
			$response["Cash Flows from Financial Activities"]["summaryRow"] = array("title"=>"Total Cash Flows from Financing activities" ,"value" => $totalCashFromFinanceActivities);
			$response["Cash and Cash Equivalents"][] = array("title"=>"Net Increase (decrease) in cash & cash equivalents" ,"value" => $netIncreaseInCash); 
			$response["Cash and Cash Equivalents"][] = array("title"=>"Cash & cash equivalents at beginning of period" ,"value" => $cashAndCashEq); 
			$response["Cash and Cash Equivalents"]["summaryRow"] = array("title"=>"Net cash & cash equivalents at end of period" ,"value" => $netCashEquivalent, "no_expand"=>true); 
	
			$netIncome = null;
			$depreciation = null;
			$goodWill = null;
			$disposal = null;
			$tradeReceivable = null;
			$totalCurrentAssets = null;
			$netAssets = null;
			$totalLiabilities = null;
			$netCashFlow = null;
			$purchasePropertyEquipment = null;
			$advancesFromParentCompany = null;
			$proceedsFromPlant = null;
			$totalCashFromFinanceActivities = null;
			$netIncreaseInCash = null;
			$cashAndCashEq = null;
			$netCashEquivalent = null;
			$finalGoodWill = null;
			$finalTradeReceivable = null;
			$finalNetAssets = null;
			$finalCurrentAssets = null;
			$finalCurrentLiabilities = null;
			$finalAdvances = null;
			$finalProceedsFromPlant = null;
			$fixedAssets = null;
			return $response;
		 } catch (Exception $e) {
			echo $e->getMessage();
		}
		 
	 }
	  
	  public function getSalesReport($client_id, $report_id) {
		try {
				$a = array();
				$b = array();
				$c = array();
				$finalResponse = array();

				$db = new reportClientApplicationMap();
				$searchItems['client_id'] = $client_id;
				$searchItems['report_id'] = $report_id;
				$searchItems['auth_key'] = "";
				$response =  $db->getAllReportClientApplicationMaps($searchItems);
				
				if (sizeof($response) > 0) {
					if ($response["error"] == false) {
						$application_id = $response["reportClientApplicationMapDetails"][0]["application_id"];
						$a[] = $this->getWeightedPipeLine($response["reportClientApplicationMapDetails"], $client_id, $application_id);
						$b[] = $this->getSalesOrders($response["reportClientApplicationMapDetails"], $client_id, $application_id);
						if ((sizeof($a) > 0) && sizeof($b) > 0) {
							if ($a[0]["error"] == false && $b[0]["error"] == false) {
								$c[] = $this->getTotalOfSales($a, $b);
							} else {
								$c["error"] = true;
								$c["message"] = SERVER_NO_RESPONSE;
								return $c;
							}
						} else {
							$c["error"] = true;
							$c["message"] = SERVER_NO_RESPONSE;
							return $c;
						}
						
						$finalResponse = array_merge($a, $b, $c); 
						//$finalResponse = $a;
					}
				}
			
				
					
			  return $finalResponse;
		 } catch (Exception $e) {
			echo $e->getMessage();
		}
	 }
	 
	 
	public function insertBudgetForClient($client_id, $territory_id, $brand_id, $csvData) {
		try {
			$delResp = array();
			$response = array();
			$name = $csvData['name'];
			$size = $csvData['size'];
			$file  = $csvData['tmp_name'];
			$ext = $this->getExtension($name);
			$valid_formats = array("csv", "CSV");
		
			if(strlen($name) > 0)
			{
				if(in_array($ext,$valid_formats)) {
					$delResp = $this->deleteBudget($client_id, $territory_id, $brand_id);
					if ($delResp["error"] == false) {
						$this->conn->autocommit(false);
						$handle = fopen($file, "r");
						$c = 0;
						$firstRow = true;
						while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
						{
							if ($firstRow) {
								$firstRow = false;
							} else {
								 $line_type = trim($filesop[0]);
								 $item_head = trim($filesop[1]);
								 $line_item = preg_replace('/[(][^)]*[)]([^(]*$)/', '$2', $filesop[2]);
								 $month_jan = trim($filesop[3]);
								 $month_feb = trim($filesop[4]);
								 $month_march = trim($filesop[5]);
								 $month_april = trim($filesop[6]);
								 $month_may = trim($filesop[7]);
								 $month_june = trim($filesop[8]);
								 $month_july = trim($filesop[9]);
								 $month_aug = trim($filesop[10]);
								 $month_sept = trim($filesop[11]);
								 $month_oct = trim($filesop[12]);
								 $month_nov = trim($filesop[13]);
								 $month_dec = trim($filesop[14]);
								 $total_values = trim($filesop[15]);
								 $sql = "INSERT INTO client_budget (client_id, territory_id, brand_id, line_type, item_head, line_item, month_jan, month_feb, month_march, month_april, month_may, month_june, month_july, month_aug, month_sept, month_oct, month_nov, month_dec, total_values) VALUES (?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
								 if ($stmt = $this->conn->prepare($sql)) {
									$stmt->bind_param("issssssssssssssssss", $client_id, $territory_id, $brand_id, $line_type, $item_head, $line_item, $month_jan, $month_feb, $month_march, $month_april, $month_may, $month_june, $month_july, $month_aug, $month_sept, $month_oct, $month_nov, $month_dec, $total_values);
									$result = $stmt->execute();
									$stmt->close();
									if ($result) {
										$this->conn->commit();
										$response["error"] = false;
										$response["message"] = BUDGET_IMPORT_SUCCESS;
										
									} else {
										$response["error"] = true;
										$response["message"] = BUDGET_IMPORT_FAILURE;
									}
								} else {
									$response["error"] = true;
									$response["message"] = QUERY_EXCEPTION;
								}
							}
						}
					}
				} else {
					$response["error"] = true;
					$response["message"] = INVALID_FORMAT;
				}
			} else {
				$response["error"] = true;
				$response["message"] = NO_FILE_SELECTED;
			}	
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}
		
	}
	
	
	public function getBudgetDetailsFromDB($client_id, $searchItems) {
		try {
			$searchItems['client_id'] = $client_id;
			
		//	$searchItems['line_type'] = "Row";
			$sql = "SELECT client_budget_id, c.client_id, org_name, t.territory_id, b.brand_id, territory_name, brand_name, line_type, item_head, line_item, month_jan, month_feb, month_march, month_april, month_may, month_june, month_july, month_aug, month_sept, month_oct, month_nov, month_dec, total_values FROM client_budget c JOIN territory_master t ON c.territory_id=t.territory_id JOIN brand_master b on c.brand_id=b.brand_id WHERE  ";
						
			foreach ($searchItems as $key=>$value) {
					switch($key) {
						 case 'client_id':
							$a_param_type[] = 'i';
							$a_bind_params[] = $value;
							$query[] = "c.client_id = ? ";
						break;
						case 'territory_id':
							$a_param_type[] = 's';
							$a_bind_params[] = $value;
							$query[] = "c.territory_id = ? ";
						break;
						case 'brand_id':
							$a_param_type[] = 's';
							$a_bind_params[] = $value;
							$query[] = "c.brand_id = ? ";
						break;
						case 'line_type':
							$a_param_type[] = 's';
							$a_bind_params[] = $value;
							$query[] = "c.line_type = ? ";
						break;
						
					}
			}
			$sql .= implode(' AND ', $query);
			
			
			$param_type = '';
			$n = count($a_param_type);
			for($i = 0; $i < $n; $i++) {
				$param_type .= $a_param_type[$i];
			}
			$a_params[] = & $param_type;
			for($i = 0; $i < $n; $i++) {
				$a_params[] = & $a_bind_params[$i];
			}
			

			$response["budgetSummary"] = array();
			if ($stmt = $this->conn->prepare($sql)) {
				call_user_func_array(array($stmt, 'bind_param'), $a_params);
				
				$stmt->execute();
				 $stmt->store_result();
				if( $stmt->num_rows > 0 ) {
					$stmt->bind_result($client_budget_id, $client_id, $org_name, $territory_id, $brand_id, $territory_name, $brand_name, $line_type, $item_head, $line_item, $month_jan, $month_feb, $month_march, $month_april, $month_may, $month_june, $month_july, $month_aug, $month_sept, $month_oct, $month_nov, $month_dec, $total_values);
					while ($result = $stmt->fetch()) {
						$tmp = array();
						$tmp["client_budget_id"] = $client_budget_id;
						$tmp["client_id"] = $client_id;
						$tmp["org_name"] = $org_name;
						$tmp["territory_id"] = $territory_id;
						$tmp["brand_id"] = $brand_id;
						$tmp["line_type"] = trim($line_type);
						$tmp["item_head"] = trim($item_head);
						$tmp["line_item"] = trim($line_item);
						$tmp["month_jan"] =  $this->formatDigits($month_jan);
						$tmp["month_feb"] = $this->formatDigits($month_feb);
						$tmp["month_march"] =  $this->formatDigits($month_march);
						$tmp["month_april"] = $this->formatDigits($month_april);
						$tmp["month_may"] = $this->formatDigits($month_may);
						$tmp["month_june"] = $this->formatDigits($month_june);
						$tmp["month_july"] = $this->formatDigits($month_july);
						$tmp["month_aug"] = $this->formatDigits($month_aug);
						$tmp["month_sept"] = $this->formatDigits($month_sept);
						$tmp["month_oct"] = $this->formatDigits($month_oct);
						$tmp["month_nov"] = $this->formatDigits($month_nov);
						$tmp["month_dec"] = $this->formatDigits($month_dec);
						$tmp["total_values"] = $this->formatDigits($total_values);
						$tmp["territory_name"] = trim($territory_name);
						$tmp["brand_name"] = trim($brand_name);
						
						
					    $response["budgetSummary"][] = $tmp;
					}
						$response["error"] = false;
						$response["message"] = RECORD_FOUND;
					} else {
						$response["error"] = true;
						$response["message"] = NO_RECORD_FOUND;
					}
			} else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
			}	
			return $response;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
    }
	
	public function formatDigits($value) {
		return trim(preg_replace('/[^0-9.-]/s', '', $value));
	}
	
	 public function deleteBudget($client_id, $territory_id, $brand_id) {
		try {
			$response = array();
			$date =  date("Y-m-d h:i:s");
			$this->conn->autocommit(false);
		
				if ($stmt = $this->conn->prepare("DELETE FROM client_budget  WHERE client_id = ? AND territory_id = ? AND brand_id = ?")) {
					$stmt->bind_param("iss", $client_id, $territory_id, $brand_id);
				//$stmt->bind_param("isi", $is_active, $date, $admin_user_id);
				$result = $stmt->execute();
				$this->conn->commit();
				$stmt->close();
				if ($result) {
					$response["error"] = false;
					$response["message"] = DELETE_BUDGET_SUCCESS;
				} else {
					$response["error"] = true;
					$response["message"] = DELETE_BUDGET_FAILURE;
					
				}
			} else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
			}	
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}		
    }
	
	public function getTerritoryAndBrandFromDB($client_id) {
		try {
			$a = array();
			$b = array();
			$final = array();
			$a = $this->getTerritoryFromDB($client_id);
			$b = $this->getBrandFromDB($client_id);
			$final["territoryBrand"] = array_merge($a, $b);
			return $final;
			
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}	
	}
	
	public function getTerritoryFromDB($client_id) {
		try {
			$response = array();
			$is_active = 1;
			$sql = "SELECT client_id, category_id, territory_id, territory_name, is_active FROM territory_master WHERE client_id = ? AND is_active = ? order by client_territory_id DESC";
			$response["territoryDetails"] = array();
			if ($stmt = $this->conn->prepare($sql)) {
				$stmt->bind_param("ii", $client_id, $is_active);
				$stmt->execute();
				$stmt->store_result();
				if( $stmt->num_rows > 0 ) {
					$tmp = array();
					
					$stmt->bind_result($client_id, $category_id, $territory_id, $territory_name, $is_active);
					while ($result = $stmt->fetch()) {
					//	$tmp = array();
						$tmp["client_id"] = $client_id;
						$tmp["category_id"] = $category_id;
						$tmp["territory_id"] = $territory_id;
						$tmp["territory_name"] = $territory_name;
						$tmp["is_active"] = $is_active;
						$response["territoryDetails"][] = $tmp;
					}
				
						$response["error"] = false;
						$response["message"] = RECORD_FOUND;
					} else {
						$response["error"] = true;
						$response["message"] = NO_RECORD_FOUND;
					}
				} else {
						$response["error"] = true;
						$response["message"] = QUERY_EXCEPTION;
				}	
			return $response;
			
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}	
	}
	
	public function getBrandFromDB($client_id) {
		try {
			$response = array();
			$is_active = 1;
			$sql = "SELECT client_id, category_id, brand_id, brand_name, is_active FROM brand_master WHERE client_id = ? AND is_active = ? order by client_brand_id DESC ";
			$response["brandDetails"] = array();
			if ($stmt = $this->conn->prepare($sql)) {
				$stmt->bind_param("ii", $client_id, $is_active);
				$stmt->execute();
				$stmt->store_result();
				if( $stmt->num_rows > 0 ) {
					$tmp = array();
					
					$stmt->bind_result($client_id, $category_id, $brand_id, $brand_name, $is_active);
					while ($result = $stmt->fetch()) {
						//$tmp = array();
						$tmp["client_id"] = $client_id;
						$tmp["category_id"] = $category_id;
						$tmp["brand_id"] = $brand_id;
						$tmp["brand_name"] = $brand_name;
						$tmp["is_active"] = $is_active;
						$response["brandDetails"][] = $tmp;
					}
						
						$response["error"] = false;
						$response["message"] = RECORD_FOUND;
					} else {
						$response["error"] = true;
						$response["message"] = NO_RECORD_FOUND;
					}
				} else {
						$response["error"] = true;
						$response["message"] = QUERY_EXCEPTION;
				}	
			return $response;
			
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}	
	}
	
	
		public function getTerritoryBrandWiseTotalIncome($client_id) {
			try {
				$response = array();
				$item_head  = 'Total Income';
				$sql = "SELECT c.client_id, org_name, c.territory_id, c.brand_id, territory_name, brand_name, total_values FROM client_budget c JOIN territory_master t on t.territory_id=c.territory_id JOIN brand_master b ON b.brand_id = c.brand_id WHERE c.client_id = ? AND item_head = ? ";
				$response["budgetDetails"] = array();
				if ($stmt = $this->conn->prepare($sql)) {
					$stmt->bind_param("is", $client_id, $item_head);
					$stmt->execute();
					$stmt->store_result();
					if( $stmt->num_rows > 0 ) {
						$stmt->bind_result($client_id, $org_name, $territory_id, $brand_id, $territory_name, $brand_name, $total_values);
						while ($result = $stmt->fetch()) {
							$tmp = array();
							$tmp["client_id"] = $client_id;
							$tmp["territory_id"] = $territory_id;
							$tmp["brand_id"] = $brand_id;
							$tmp["territory_name"] = $territory_name;
							$tmp["brand_name"] = $brand_name;
							$tmp["org_name"] = $org_name;
							$tmp["total_values"] = $this->formatDigits($total_values);
							$response["budgetDetails"][] = $tmp;
						}
							$response["error"] = false;
							$response["message"] = RECORD_FOUND;
						} else {
							$response["error"] = true;
							$response["message"] = NO_RECORD_FOUND;
						}
					} else {
							$response["error"] = true;
							$response["message"] = QUERY_EXCEPTION;
					}	
				return $response;
				
			} catch (Exception $e) {
				$this->conn->rollback();
				echo $e->getMessage();
			}	
		}
		
		
	function getCategoryIDFromTrack($client_id, $track_id, $track_type) {
		try {
			if ($track_type == "Brand") {
				$sql = "SELECT category_id FROM brand_master WHERE client_id = ? AND brand_id = ? ";
			} else {
				$sql = "SELECT category_id FROM territory_master WHERE client_id = ? AND territory_id = ? ";
			}
			if ($stmt = $this->conn->prepare($sql)) {
				$stmt->bind_param("is", $client_id, $track_id);
				$stmt->execute();			
				$stmt->store_result();
				//$stmt->close();
				if($stmt->num_rows > 0){
					$stmt->bind_result($category_id);
					while ($result = $stmt->fetch()) {
						$response["error"] = false;
						$response["message"] = RECORD_FOUND;
						$response["category_id"] = $category_id;
					
					}
				}else{
					$response["error"] = true;
					$response["message"] = NO_RECORD_FOUND;
				}
								
			} else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
			}	
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}		
	}
	
	/**
	 * close the database connection
	 */
	 public function __destruct() {
	 // close the database connection
		 $this->db->closeconnection();
	 }
	
}

?>