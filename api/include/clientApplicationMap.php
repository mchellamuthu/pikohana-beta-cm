<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
require_once dirname(__FILE__) . '/DbConnect.php';
class clientApplicationMap extends utility
{

    // private $conn;

    public function __construct()
    {

        // opening db connection
        $this->db = new DbConnect();
        $this->conn = $this->db->connect();
    }

    public function checkIfClientApplicationMapExist($client_id, $client_master_id, $application_id)
    {
        try {
            $sql = 'SELECT client_application_map_id, is_active
                    FROM client_application_map
                    WHERE client_id = ? AND application_id = ? AND client_master_id = ? ';
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param('iii', $client_id, $application_id, $client_master_id);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($client_application_map_id, $is_active);
                    while ($result = $stmt->fetch()) {
                        $response['error'] = false;
                        $response['message'] = RECORD_FOUND;
                        $response['is_active'] = $is_active;
                        $response['client_application_map_id'] = $client_application_map_id;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = NO_RECORD_FOUND;
                }
                $stmt->close();
            } else {
                $response['error'] = true;
                $response['message'] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function addNewClientApplicationMap($client_id, $client_master_id, $application_id, $auth_key, $is_active)
    {
        try {
            $checkResult = array();
            $checkResult = $this->checkIfClientApplicationMapExist($client_id, $client_master_id, $application_id);

            if ($checkResult['error'] == true) {
                $this->conn->autocommit(false);
                $date = date('Y-m-d h:i:s');
                $response = array();

                $sql = 'INSERT INTO client_application_map(client_id, client_master_id, application_id, auth_key, created_date, last_updated_on, is_active) values(?, ?, ?, ?, ?,?,?)';
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param('iiisssi', $client_id, $client_master_id, $application_id, $auth_key, $date, $date, $is_active);
                    $result = $stmt->execute();
                    $stmt->close();
                    if ($result) {
                        $client_application_map_id = $this->conn->insert_id;
                        $this->conn->commit();
                        $response['error'] = false;
                        $response['message'] = CLIENT_APPLICATION_MAP_CREATED_SUCCESS;
                        $response['client_application_map_id'] = $client_application_map_id;
                    } else {
                        $response['error'] = true;
                        $response['message'] = CLIENT_APPLICATION_MAP_CREATED_FAILURE;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = QUERY_EXCEPTION;
                }
            } else {
                $client_application_map_id = $checkResult['client_application_map_id'];
                $is_active = $checkResult['is_active'];
                if ($is_active == 0) {
                    $is_active = 1;
                    if ($stmt = $this->conn->prepare('UPDATE client_application_map set is_active = ?, auth_key = ? WHERE client_application_map_id = ?')) {
                        $stmt->bind_param('isi', $is_active, $auth_key, $client_application_map_id);
                        $result = $stmt->execute();

                        $stmt->close();
                        if ($result) {
                            $this->conn->commit();
                            $response['error'] = false;
                            $response['message'] = CLIENT_APPLICATION_MAP_CREATED_SUCCESS;
                            $response['client_application_map_id'] = $client_application_map_id;
                        } else {
                            $response['error'] = true;
                            $response['message'] = CLIENT_APPLICATION_MAP_CREATED_FAILURE;
                        }
                    } else {
                        $response['error'] = true;
                        $response['message'] = QUERY_EXCEPTION;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = ALREADY_EXIST;
                }
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    /**
     * Fetching all industry_type
     *
     */
    public function getAllClientApplicationMaps($searchItems, $groupby = '')
    {
        try {
            $a_params = [];
            $sql = "SELECT a.application_id, a.application_name, a.application_logo_path, a.application_api_url, a.application_type,
                            a.application_auth_key, a.app_site_url, a.is_system_token_required, a.is_user_auth_key_required,
                            am.client_application_map_id, am.auth_key, am.client_id, am.client_master_id, am.is_active
                    FROM client_application_map am
                    RIGHT JOIN application_master a ON am.application_id = a.application_id ";

            if (sizeof($searchItems) > 0) {
                $sql .= ' where ';

                foreach ($searchItems as $key => $value) {
                    switch ($key) {
                        case 'application_id':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = "a.application_id = ? ";
                            break;
                        case 'is_active':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = "am.is_active = ? ";
                            break;
                        case 'client_application_map_id':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = " am.client_application_map_id = ? ";
                            break;
                        case 'is_system_token_required':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = " a.is_system_token_required = ? ";
                            break;
                        case 'is_user_auth_key_required':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = " a.is_user_auth_key_required = ? ";
                            break;
                        case 'client_id':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = " am.client_id = ? ";
                            break;
                        case 'client_master_id':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = ' am.client_master_id = ? ';
                            break;
                    }
                }

                $sql .= implode(' AND ', $query);

                $param_type = '';
                $n = count($a_param_type);
                for ($i = 0; $i < $n; $i++) {
                    $param_type .= $a_param_type[$i];
                }
                $a_params[] = &$param_type;
                for ($i = 0; $i < $n; $i++) {
                    $a_params[] = &$a_bind_params[$i];
                }

            }

            if ($groupby == 'Y') {
                $sql .= " GROUP BY am.application_id";
            }

            $sql .= " ORDER BY am.is_active desc";

            $response["clientApplicationMapDetails"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                if (sizeof($searchItems) > 0) {

                    call_user_func_array(array($stmt, 'bind_param'), $a_params);
                }

                $stmt->execute();
                $stmt->store_result();

                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($application_id, $application_name, $application_logo_path, $application_api_url, $application_type,
                        $application_auth_key, $app_site_url, $is_system_token_required, $is_user_auth_key_required, $client_application_map_id,
                        $auth_key, $client_id, $client_master_id, $is_active);

                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["application_id"] = $application_id;
                        $tmp["application_name"] = $application_name;
                        $tmp["application_logo_path"] = $application_logo_path;
                        $tmp["application_api_url"] = $application_api_url;
                        $tmp["application_type"] = $application_type;
                        $tmp["application_auth_key"] = $application_auth_key;
                        $tmp["app_site_url"] = $app_site_url;
                        $tmp["is_system_token_required"] = $is_system_token_required;
                        $tmp["is_user_auth_key_required"] = $is_user_auth_key_required;
                        $tmp["client_application_map_id"] = $client_application_map_id;
                        $tmp["auth_key"] = $auth_key;
                        $tmp["client_id"] = $client_id;
                        $tmp["client_master_id"] = $client_master_id;
                        $tmp["is_active"] = $is_active;
                        $response["clientApplicationMapDetails"][] = $tmp;
                        $response["application_ids"][] = $application_id;
                    }
                    $stmt->close();
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAllClientApplicationWrapper($client_id, $client_master_id, $is_active)
    {
        try {
            $response = array();
            $newResponse = array();
            $searchItems = array();
            $abc = array();
            $searchParams = array();
            $searchParams['client_id'] = $client_id;
            $searchParams['client_master_id'] = $client_master_id;
            if (strlen(trim($is_active)) > 0) {
                $searchParams['is_active'] = $is_active;
            }
            $newResponse = $this->getAllClientApplicationMaps($searchParams, 'N');

            if ($newResponse['error'] == false) {
                $searchItems['more_applications'] = implode(',', $newResponse['application_ids']);
            } else {
                $searchItems['is_active'] = 1;
            }
            $objAppln = new application();
            $response = $objAppln->getAllApplications($searchItems);

            if ($response['error'] == false) {
                $newResponse["error"] = false;
                foreach ($response['applicationDetails'] as $k => $v) {
                    $v['client_application_map_id'] = null;
                    $v["client_id"] = $client_id;
                    $v["name"] = null;
                    $v["email"] = null;
                    $v["phone"] = null;
                    $newResponse['clientApplicationMapDetails'][] = $v;
                }
            }

            return $newResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function activeDeactiveReportMap($client_application_map_id, $is_active)
    {
        try {
            $response = array();

            $this->conn->autocommit(false);
            if ($stmt = $this->conn->prepare("UPDATE report_client_application_map set is_active = ? WHERE client_application_map_id = ?")) {
                $stmt->bind_param("ii", $is_active, $client_application_map_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = UPDATE_CLIENT_APPLICATION_MAP_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = UPDATE_CLIENT_APPLICATION_MAP_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function updateClientApplicationMap($client_application_map_id, $client_id, $application_id, $client_master_id, $auth_key, $is_active)
    {
        try {
            $response = array();
            $date = date('Y-m-d h:i:s');
            $this->conn->autocommit(false);
            $sql = 'UPDATE client_application_map set client_id = ?, client_master_id = ?, application_id = ?, auth_key = ?, last_updated_on = ? , is_active = ? WHERE client_application_map_id = ? ';
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param('iiissii', $client_id, $client_master_id, $application_id, $auth_key, $date, $is_active, $client_application_map_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response['error'] = false;
                    $response['message'] = UPDATE_CLIENT_APPLICATION_MAP_SUCCESS;
                } else {
                    $response['error'] = true;
                    $response['message'] = UPDATE_CLIENT_APPLICATION_MAP_FAILURE;
                }
            } else {
                $response['error'] = true;
                $response['message'] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    /**
     * Deleting a branch
     * @param String $industry_type_id id of the task to delete
     */
    public function deleteClientApplicationMap($client_application_map_id)
    {
        try {
            $response = array();
            $is_active = 0;
            $date = date("Y-m-d h:i:s");
            $objReportMap = new reportClientApplicationMap();
            $response = $objReportMap->deletedMappedReports($client_application_map_id);
            if ($response["error"] == false) {
                $this->conn->autocommit(false);
                if ($stmt = $this->conn->prepare("UPDATE client_application_map set is_active = ?, last_updated_on = ? WHERE client_application_map_id = ?")) {
                    $stmt->bind_param("isi", $is_active, $date, $client_application_map_id);
                    $result = $stmt->execute();
                    $this->conn->commit();
                    $stmt->close();
                    if ($result) {
                        $response["error"] = false;
                        $response["message"] = DELETE_CLIENT_APPLICATION_MAP_SUCCESS;
                    } else {
                        $response["error"] = true;
                        $response["message"] = DELETE_CLIENT_APPLICATION_MAP_FAILURE;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function hardDeleteClientApplicationMap($ids_to_delete, $client_id, $client_master_id)
    {
        try {
            $response = array();
            $is_active = 0;
            $date = date("Y-m-d h:i:s");
            if (sizeof($ids_to_delete) > 0) {
                $objReportMap = new reportClientApplicationMap();
                $response = $objReportMap->deletedAllMappedReports($ids_to_delete);
            }
            $this->conn->autocommit(false);
            if ($stmt = $this->conn->prepare("DELETE FROM client_application_map WHERE client_id = ? AND client_master_id = ?")) {
                $stmt->bind_param("ii", $client_id, $client_master_id);
                $result = $stmt->execute();

                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = DELETE_CLIENT_APPLICATION_MAP_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = DELETE_CLIENT_APPLICATION_MAP_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getCategoryByMasterID($masterId, $client_id, $is_admin)
    {
        $dash_db = new dashboardReport();
        $utility = new utility();

        if ($is_admin == 2) {
            $res_super_admin = $utility->getSuperAdminID($masterId);
            $user_id = $res_super_admin['client_id'];
        }

        $sql = "SELECT ccm.client_category_id, ccm.category_id, ccm.category_name FROM client_category_master as ccm WHERE client_master_id = $masterId and client_id = $user_id";

        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->execute();
            $stmt->store_result();
            if ($stmt->num_rows > 0) {
                $stmt->bind_result($client_category_id, $category_id, $category_name);
                $data = array();
                while ($result = $stmt->fetch()) {
                    $data[] = array('client_catID' => $client_category_id, 'catId' => $category_id, 'catName' => $category_name);

                }
                $subData = $dash_db->insertLocalTrackingCategories($data, $client_id, $masterId);
                $childResult = $dash_db->insertLocalSubCategories($client_id, $subData, $masterId);
                $response["error"] = false;
            } else {
                $response["error"] = true;
                $response['result'] = NO_COMPANY_FOUND;
            }
        } else {
            $response["error"] = true;
            $response["message"] = QUERY_EXCEPTION;
        }

    }

    public function setApplicationReports($client_id, $selectedClients, $application_ids, $status, $report_ids, $is_admin = null)
    {
        try {
            $success = false;
            $searchItems = array();
            $resClientApplicationIDs = array();
            $delResponse = array();

            foreach ($selectedClients as $value) {
                $client_master_id = $value['id'];
                $searchItems["client_id"] = $client_id;
                $searchItems["client_master_id"] = $client_master_id;
                if ($is_admin == 2) {
                    $getCategoryByMasterID = $this->getCategoryByMasterID($client_master_id, $client_id, $is_admin);
                }

                $resClientApplicationIDs = $this->getAllClientApplicationMaps($searchItems);
                foreach ($resClientApplicationIDs["clientApplicationMapDetails"] as $k2 => $v2) {
                    $ids_to_delete[] = $v2["client_application_map_id"];
                }
                $delResponse = $this->hardDeleteClientApplicationMap($ids_to_delete, $client_id, $client_master_id);
                $objReport = new reportClientApplicationMap();

                if (sizeof($application_ids) > 0) {
                    if (in_array(XERO, $application_ids)) {
                        $resp = array();
                        $resp = $this->addNewClientApplicationMap($client_id, $client_master_id, 1, '', 1);
                        if ($resp["error"] == false) {
                            if (sizeof($report_ids) > 0) {
                                if (in_array(1, $report_ids)) {
                                    $rResp = $objReport->addNewReportClientApplicationMap(1, $client_id, XERO, $client_master_id);
                                    if ($rResp["error"] == false) {
                                        $success = true;
                                    }
                                }
                                if (in_array(3, $report_ids)) {
                                    $rResp = $objReport->addNewReportClientApplicationMap(3, $client_id, XERO, $client_master_id);
                                    if ($rResp["error"] == false) {
                                        $success = true;
                                    }
                                }
                            } else {
                                $success = true;
                            }
                        }
                    }

                    if (in_array(ZOHO, $application_ids)) {
                        $goAhead = false;
                        $Zresp = array();

                        $utility = new utility();
                        $res_is_admin = $utility->getFieldByID('client_master', 'client_id', $client_id, 'is_admin');
                        if ($res_is_admin['error'] == false) {
                            $is_admin = $res_is_admin['field_key'];
                        }

                        if ($is_admin != 1) {
                            $superAdminID = $this->getSuperAdminID($client_master_id);
                            $authKey = $this->getMasterAuthKey($superAdminID['client_id']);
                        } else {
                            $authKey = $this->getMasterAuthKey($client_id);
                        }
                        $final_auth_key = (strlen(trim($authKey["auth_key"])) > 0) ? $authKey['auth_key'] : "";

                        if ($client_master_id == 13) {
                            $final_auth_key = HIGHEND_AUTHKEY;
                        }

                        $Zresp = $this->addNewClientApplicationMap($client_id, $client_master_id, 2, $final_auth_key, 1);
                        if ($Zresp["error"] == false) {
                            if (sizeof($report_ids) > 0) {
                                if (in_array(2, $report_ids)) {
                                    if ($final_auth_key != "") {
                                        $ZResp = $objReport->addNewReportClientApplicationMap(2, $client_id, ZOHO, $client_master_id);
                                    } else {
                                        $message = SALES_REPORT_CANNOT_BE_ENABLED;

                                        if (($key = array_search(2, $report_ids)) !== false) {
                                            unset($report_ids[$key]);
                                        }
                                    }
                                }
                                if (in_array(KPI, $report_ids)) {
                                    $ZResp = $objReport->addNewReportClientApplicationMap(KPI, $client_id, ZOHO, $client_master_id);
                                    $success = true;
                                }
                            } else {
                                $success = true;
                            }
                        }
                    }
                } else {
                    $success = true;
                }

                if ($success == true) {
                    $response["error"] = false;
                    $response["message"] = UPDATE_CLIENT_APPLICATION_MAP_SUCCESS . " " . $message;

                } else {
                    $response["error"] = true;
                    $response["message"] = UPDATE_CLIENT_APPLICATION_MAP_FAILURE;
                }
                $delResponse = null;
                $searchItems = null;
                $resClientApplicationIDs = null;
                $utility = null;
                $objReport = null;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getAllApplicationDetails($client_id, $client_master_id, $is_active)
    {
        try {
            $response = array();
            $newResponse = array();
            $searchItems = array();
            $objAppln = new application();
            $response = $objAppln->getAllApplications($searchItems);

            if ($response['error'] == false) {
                $newResponse["error"] = false;
                foreach ($response['applicationDetails'] as $k => $v) {
                    $newResponse['clientApplicationMapDetails'][] = $v;
                }
            }
            return $newResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * close the database connection
     */
    public function __destruct()
    {
        // close the database connection
        $this->db->closeconnection();
    }

}
