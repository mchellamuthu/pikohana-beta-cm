<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

require '../vendor/autoload.php';
require_once dirname(__FILE__) . '/DbConnect.php';

class utility extends DbConnect
{

    public $conn;
    public $db;

    public function __construct()
    {

        // opening db connection
        $this->db = new DbConnect();
        $this->conn = $this->db->connect();

    }

    public function latestFileAtPath($path, $prefix, $suffix)
    {
        try {
            $latest_ctime = 0;
            $latest_filename = '';
            $d = dir($path);
            if (file_exists($path)) {
                while (false !== ($entry = $d->read())) {
                    $filepath = "{$path}/{$entry}";
                    // could do also other checks than just checking whether the entry is a file
                    if ($suffix !== "") {
                        if ($this->startsWith($entry, $prefix) && $this->endsWith($entry, $suffix)) {
                            if (is_file($filepath) && filemtime($filepath) > $latest_ctime) {
                                $latest_ctime = filemtime($filepath);
                                $latest_filename = $entry;
                            }
                        }
                    } else if ($this->startsWith($entry, $prefix)) {
                        if (is_file($filepath) && filemtime($filepath) > $latest_ctime) {
                            $latest_ctime = filemtime($filepath);
                            $latest_filename = $entry;
                        }
                    }
                }
            }
            return $latest_filename;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        // echo "<br>latest_filename-----> $haystack     $needle  </br> <br></br>".(substr($haystack, 0, $length) === $needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    public function checkCacheFileExist($client_master_id, $pagename, $loginDate, $sub_category_id1, $sub_category_id2)
    {
        try {
            if ($pagename == 'flashreport') {
                $path = __DIR__ . "/" . $pagename . "/" . $client_master_id . "/";
                $prefix = $client_master_id . "_flashreport_";
                $file = $this->latestFileAtPath($path, $prefix, "");
                $path .= $file;
            } else if ($pagename == 'plreport') {
                $prefix = $client_master_id . "_plreport_";
                $suffix = "";
                $suffix .= "_" . $sub_category_id1;
                if (strlen(trim($sub_category_id2)) > 0) {
                    $suffix .= "_" . $sub_category_id2;
                }

                $suffix .= ".json";
                $path = __DIR__ . "/plreport/" . $client_master_id . "/";
                $file = $this->latestFileAtPath($path, $prefix, $suffix);
                $path .= $file;
            }
            if (file_exists($path)) {
                $json_file = file_get_contents($path);
                if (strlen($json_file) > 0) {
                    return $json_file;
                } else {
                    return "";
                }
            } else {
                return "";
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function MetricscheckCacheFileExist($client_master_id, $pagename, $sub_category_id1, $sub_category_id2)
    {
        try {
            if ($pagename == 'flashreport') {
                $path = __DIR__ . "/" . $pagename . "/" . $client_master_id . "/";
                $prefix = $client_master_id . "_flashreport_";
                $file = $this->latestFileAtPath($path, $prefix, "");
                $path .= $file;
            } else if ($pagename == 'plreport') {
                $prefix = $client_master_id . "_plreport_";
                if ($sub_category_id1 > 0) {
                    $suffix = "_1000.json";
                }

                if ($sub_category_id2 > 0) {
                    $suffix = "_1000" . $suffix;
                }

                $path = __DIR__ . "/" . $pagename . "/" . $client_master_id . "/";
                $file = $this->latestFileAtPath($path, $prefix, $suffix);
                $path .= $file;
            }
            if (file_exists($path)) {
                $json_file = file_get_contents($path);
                if (strlen($json_file) > 0) {
                    return $path;
                } else {
                    return "";
                }
            } else {
                return "";
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getFieldByID($table, $field_id, $field_value, $field_name)
    {
        try {
            $is_active = 1;
            $sql = "SELECT " . $field_name . " FROM " . $table . " WHERE " . $field_id . " = ? AND is_active = ?";

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("si", $field_value, $is_active);
                if ($stmt->execute()) {
                    $stmt->store_result();
                    $stmt->bind_result($field_name);
                    $stmt->fetch();
                    $num_rows = $stmt->num_rows;
                    $stmt->close();
                    if ($num_rows > 0) {
                        $response["error"] = false;
                        $response["field_key"] = $field_name;
                    } else {
                        $response["error"] = true;
                        $response["field_key"] = '';
                    }

                } else {
                    $response["error"] = true;

                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    
    public function getOauthTokenByClientMaster($client_id,$client_master_id)
    {
        try {
            // $is_active = 1;
            $sql = "SELECT * FROM client_oauth_token WHERE client_id = ? AND client_master_id = ?";

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ii", $client_id, $client_master_id);
                if ($stmt->execute()) {
                    $stmt->store_result();
                    $row = $stmt->fetch();
                    $num_rows = $stmt->num_rows;
                    $stmt->close();
                    if ($num_rows > 0) {
                        $response["error"] = false;
                        $response["row"] = $row;
                    } else {
                        $response["error"] = true;
                        $response["row"] = '';
                    }

                } else {
                    $response["error"] = true;

                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function isValidToken($table, $field_id, $field_value)
    {
        try {
            $result = $this->getFieldByID($table, $field_id, $field_value, 'generated_date');
            if (count($result) > 0) {
                $last_generated_date = $result['field_key'];
                $future_date_time = date('Y-m-d H:i:s', strtotime("+30 minute", strtotime($last_generated_date)));
                $date = date("Y-m-d H:i:s");
                if ($date <= $future_date_time) {
                    $response["error"] = false;
                    $response["isValid"] = 1;
                } else {
                    $response["error"] = true;
                    $response["isValid"] = 0;
                }
            } else {
                $response["error"] = true;
                $response["isValid"] = 0;
            }
        
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getUserID($api_key, $is_client)
    {
        try {
            if ($is_client == 0) {
                $sql = "SELECT admin_user_id FROM admin_users WHERE api_key = ?";
            } else {
                $sql = "SELECT client_id FROM client_master WHERE api_key = ?";
            }
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("s", $api_key);
                $stmt->execute();
                $stmt->store_result();
                //$stmt->close();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($user_id);
                    while ($result = $stmt->fetch()) {
                        $response["error"] = false;
                        $response["message"] = USER_RECORD_FOUND;
                        $response["user_id"] = $user_id;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }

            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getUserDetails($user_id, $is_client)
    {
        try {
            if ($is_client == 0) {
                $sql = "SELECT admin_name, admin_email,admin_phone FROM admin_users WHERE admin_user_id = ?";
            } else {
                $sql = "SELECT name, email, mobile FROM client_master WHERE client_id = ?";
            }
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("i", $user_id);
                $stmt->execute();
                $stmt->store_result();
                //$stmt->close();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($name, $email, $mobile);
                    while ($result = $stmt->fetch()) {
                        $response["error"] = false;
                        $response["message"] = USER_RECORD_FOUND;
                        $response["name"] = $name;
                        $response["email"] = $email;
                        $response["mobile"] = $mobile;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }

            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getMaxID($sql, $a_param_type, $a_bind_params)
    {
        try {
            $param_type = '';
            $n = count($a_param_type);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $a_param_type[$i];
            }
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                $a_params[] = &$a_bind_params[$i];
            }

            if ($stmt = $this->conn->prepare($sql)) {
                call_user_func_array(array($stmt, 'bind_param'), $a_params);
                $stmt->execute();

                $stmt->store_result();
                $num_rows = $stmt->num_rows;
                if ($num_rows > 0) {
                    $stmt->bind_result($max_id);
                    $stmt->fetch();
                    $stmt->close();
                    if (strlen($max_id) > 0) {
                        return $max_id;
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }
            } else {

                return -1;
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function str_lreplace($search, $replace, $subject)
    {
        $pos = strrpos($subject, $search);

        if ($pos !== false) {
            $subject = substr_replace($subject, $replace, $pos, strlen($search));
        }

        return $subject;
    }
    /**
     * Generating random Unique MD5 String for user Api key
     */
    public function generateApiKey()
    {
        return md5(uniqid(rand(), true));
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key, $is_client)
    {
        try {
            $is_active = 1;
            if ($is_client == 0) {
                $sql = "SELECT admin_email from admin_users WHERE api_key = ? AND is_active = ?";
            } else {
                $sql = "SELECT email from client_master WHERE api_key = ? AND is_active = ?";
            }
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("si", $api_key, $is_active);
                $stmt->execute();
                $stmt->store_result();

                $num_rows = $stmt->num_rows;
                $stmt->close();
                return $num_rows > 0;
            } else {
                return null;
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function generateString()
    {
        $string = '123456789ABCDEFGHJKLMNPQRSTUVWXYZ';
        $string_shuffled = str_shuffle($string);
        $str = substr($string_shuffled, 1, 8);
        return $str;
    }

    public function uploadImages($file, $module, $name_file)
    {

        $name = $file['name'];
        $size = $file['size'];
        $tmp = $file['tmp_name'];
        $ext = $this->getExtension($name);
        $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "pdf", "PNG", "JPG", "JPEG", "GIF", "BMP", "PDF");
        $response = array();
        if (strlen($name) > 0) {
            if (in_array($ext, $valid_formats)) {
                if ($size < (135 * 145)) {
                    $upload_dir = "../../media/$module/";
                    $actual_image_name = $upload_dir . $name_file . "." . $ext;
                    $db_save_path = PIC_BASE_URL . '/' . $module . '/' . $name_file . "." . $ext;
                    if (file_exists($actual_image_name)) {
                        unlink($actual_image_name);
                    }
                    if (move_uploaded_file($tmp, $actual_image_name)) {
                        $response["error"] = false;
                        $response["message"] = $db_save_path;
                    } else {
                        $response["error"] = true;
                        $response["message"] = UPLOAD_FAIL;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = IMAGE_OVERSIZE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = INVALID_FORMAT;
            }
        } else {
            $response["error"] = true;
            $response["message"] = NO_FILE_SELECTED;
        }
        return $response;
    }

    public function getExtension($str)
    {
        $i = strrpos($str, ".");
        if (!$i) {return "";}

        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

    public function saveMail($to, $subject, $body, $cc = "")
    {
        try {

            $this->conn->autocommit(false);
            $date = date("Y-m-d h:i:s");
            $is_sent = 0;
            $from = MAIL_FROM;
            if ($stmt = $this->conn->prepare("INSERT INTO send_mail (mail_from, mail_to, mail_subject, mail_body, mail_date, is_sent, mail_cc) values(?, ?, ?, ?, ?, ?, ?)")) {
                $stmt->bind_param("sssssis", $from, $to, $subject, $body, $date, $is_sent, $cc);
                $result = $stmt->execute();

                $stmt->close();

                if ($result) {
                    $this->conn->commit();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function lastThreeMonths()
    {
        return array(
            date('F', time()),
            date('F', strtotime('-1 month')),
            date('F', strtotime('-2 month')),
            date('F', strtotime('-3 month')),
        );
    }

    public function getReportFromDate($client_master_id)
    {
        try {
            $result = array();
            $result = $this->getFieldByID('client_master_detail', 'client_master_id', $client_master_id, 'report_start_date');
            if ($result['error'] == false) {
                return $result['field_key'];
            } else {
                return "";
            }
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getDayTillMonth($client_id)
    {
        $current_month = date('m');
        $res = array();
        $result = array();
        $res = $this->getFieldByID('client_master', 'client_id', $client_id, 'client_master_id');
        if ($res["error"] == false) {
            $client_master_id = $res['field_key'];
            $result = $this->getFieldByID('client_master_detail', 'client_master_id', $client_master_id, 'report_start_date');
        }

        $startDate = $result['field_key'] . ' -1 month';
        $endDate = date('Y-m-d');
        $i = 0;
        while (strtotime($startDate) <= strtotime($endDate)) {
            $dates[] = array("firstdate" => date("Y-m-01", strtotime($startDate)), "lastdate" => date("Y-m-t", strtotime($startDate)));
            $startDate = date('Y-m-01', strtotime($startDate .
                '+ 1 month'));
            $i++;
        }

        $dates[sizeof($dates) - 1]["lastdate"] = date("Y-m-d", strtotime($endDate));

        $dates["client_master_id"] = $client_master_id;
        $res1 = $this->getFieldByID('client_master', 'client_id', $client_id, 'is_admin');
        $dates["isAdmin"] = $res1["field_key"];
        return $dates;
    }

    public function getDayTillMonthNew($client_master_id, $report_start_date, $is_admin = null)
    {
        $startDate = date("Y-m-01", strtotime("-12 months"));
        $endDate = date('Y-m-d');
        $i = 0;
        while (strtotime($startDate) <= strtotime($endDate)) {
            $dates[] = array("firstdate" => date("Y-m-01", strtotime($startDate)), "lastdate" => date("Y-m-t", strtotime($startDate)));
            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            $i++;
        }

        /* The below line of code commented to fetch upto last date of current month - based on client requirement on 05-02-2018 */
        //$dates[sizeof($dates) - 1]["lastdate"] = date("Y-m-d", strtotime($endDate));
        $dates["client_master_id"] = $client_master_id;
        $dates["isAdmin"] = $is_admin;
        return $dates;
    }

    public function getDayTillPreviousMonth()
    {
        $startDate = date('Y-m-d', strtotime("-13 months"));
        $endDate = date('Y-m-d', strtotime('last day of last month'));

        $i = 0;
        while (strtotime($startDate) <= strtotime($endDate)) {
            $dates[] = array("firstdate" => date("Y-m-01", strtotime($startDate)), "lastdate" => date("Y-m-t", strtotime($startDate)));
            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            $i++;
        }
        return $dates;
    }

    public function compareTillMonth()
    {
        $current_month = date('m');
        $first_month = '01';
        $year = date('Y');
        $prevYear = $year - 1;
        $countDays = date('t');
        $lastYearDate = $prevYear . "-12-" . '01';
        $dates = array();
        $dates[] = array("firstdate" => date("Y-m-01", strtotime($lastYearDate)), "lastdate" => date("Y-m-t", strtotime($lastYearDate)));
        for ($i = $first_month; $i <= $current_month; $i++) {
            $query_date = $year . "-" . $i . '-' . '01';
            if ($i != 2) {
                $last_date = date("Y-m-d", strtotime($year . "-" . $i . '-' . $countDays));
            } else {
                $last_date = date("Y-m-t", strtotime($query_date));
            }
            $dates[] = array("firstdate" => date("Y-m-d", strtotime($query_date)), "lastdate" => $last_date);
        }
        return $dates;

    }

    public function getCurrentMonthField($month_num)
    {
        $month_name = "";
        switch ($month_num) {
            case 1:
                $month_name = "month_jan";
                break;
            case 2:
                $month_name = "month_feb";
                break;
            case 3:
                $month_name = "month_march";
                break;
            case 4:
                $month_name = "month_april";
                break;
            case 5:
                $month_name = "month_may";
                break;
            case 6:
                $month_name = "month_june";
                break;
            case 7:
                $month_name = "month_july";
                break;
            case 8:
                $month_name = "month_aug";
                break;
            case 9:
                $month_name = "month_sept";
                break;
            case 10:
                $month_name = "month_oct";
                break;
            case 11:
                $month_name = "month_nov";
                break;
            case 12:
                $month_name = "month_dec";
                break;
        }
        return $month_name;
    }

    public function convertCurrency($amount, $from, $to)
    {
        $url = "https://www.google.com/finance/converter?a=$amount&from=$from&to=$to";
        $data = file_get_contents($url);
        preg_match("/<span class=bld>(.*)<\/span>/", $data, $converted);
        $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
        return round($converted, 2);
    }

    public function getSuperAdminID($client_master_id)
    {
        try {
            $response = array();
            $is_default = 1;
            $is_admin = 1;
            $sql = "SELECT cm.client_id
                    FROM client_user_mapping cum
                    JOIN client_master cm ON cm.client_id = cum.client_id
                    where cm.is_active = 1 and cum.client_master_id = ? and cm.is_admin = ? LIMIT 1";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ii", $client_master_id, $is_admin);
                $stmt->execute();
                $stmt->bind_result($client_id);
                $stmt->store_result();
                $num_rows = $stmt->num_rows;
                if ($num_rows > 0) {
                    $stmt->fetch();
                    $stmt->close();
                    $response["error"] = false;
                    $response["client_id"] = $client_id;
                    $response["message"] = USER_RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = USER_DOES_NOT_EXIST;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getMasterAuthKey($client_id)
    {
        try {
            $response = array();
            $application_id = 2;
            if ($stmt = $this->conn->prepare("SELECT auth_key FROM client_application_map where client_id = ? AND application_id = ?")) {
                $stmt->bind_param("ii", $client_id, $application_id);
                $stmt->execute();
                $stmt->bind_result($auth_key);
                $stmt->store_result();
                $num_rows = $stmt->num_rows;

                if ($num_rows > 0) {
                    $stmt->fetch();
                    $stmt->close();
                    $response["error"] = false;
                    $response["auth_key"] = $auth_key;
                    $response["message"] = USER_RECORD_FOUND;

                } else {
                    $response["error"] = true;
                    $response["message"] = USER_DOES_NOT_EXIST;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function delete_old_files($pagename, $client_master_id)
    {
        try {
            if ($pagename == 'flashreport') {
                $path = __DIR__ . "/" . $pagename . "/" . $client_master_id . "/";
            } else if ($pagename == 'plreport') {
                $path = __DIR__ . "/" . $pagename . "/" . $client_master_id . "/";
            }
            if ($handle = opendir($path)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != '.' and $file != '..') {
                        $filelastmodified = filemtime($path . $file);
                        //24 hours in a day * 3600 seconds per hour
                        if ((time() - $filelastmodified) > 2 * 24 * 3600) {
                            unlink($path . $file);
                        }
                    }
                }
                closedir($handle);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAllDataByID($table, $field_id, $field_value)
    {
        try {
            $is_active = 1;
            $stmt = $this->conn->prepare("SELECT * FROM $table WHERE " . $field_id . " = ? AND is_active = ?");
            $stmt->bind_param("si", $field_value, $is_active);
            if ($stmt->execute()) {
                $result = "";
                foreach ($stmt->get_result() as $row) {
                    $result = $row;
                }
                $response["error"] = false;
                $response['data'] = $result;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getClientMasterByID($table, $field_id, $field_value)
    {
        try {
            $is_active = 1;
            $stmt = $this->conn->prepare("SELECT is_admin,client_master_id FROM $table WHERE " . $field_id . " = ? AND is_active = ?");
            $stmt->bind_param("si", $field_value, $is_active);
            if ($stmt->execute()) {
                $stmt->bind_result($is_admin, $client_master_id);
                while ($stmt->fetch()) {
                    $response["is_admin"] = $is_admin;
                    $response["client_master_id"] = $client_master_id;
                }
                $response["error"] = false;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getClientMasterDetailByID($table, $field_id, $field_value)
    {
        try {
            $is_active = 1;
            $stmt = $this->conn->prepare("SELECT company_name, report_start_date, default_currency FROM $table WHERE " . $field_id . " = ? AND is_active = ?");
            $stmt->bind_param("si", $field_value, $is_active);
            if ($stmt->execute()) {
                $stmt->bind_result($company_name, $report_start_date, $default_currency);
                while ($stmt->fetch()) {
                    $response["report_start_date"] = $report_start_date;
                    $response["default_currency"] = $default_currency;
                    $response["company_name"] = $company_name;
                }
                $response["error"] = false;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getCategoryCountByClientID($table, $client_id, $client_master_id)
    {
        try {
            $is_active = 1;
            $stmt = $this->conn->prepare("SELECT count(1) FROM $table WHERE client_category_id in (SELECT client_category_id FROM client_category_master WHERE client_id = ? AND client_master_id = ? AND is_active = ?) and sub_category_id like '1000' AND is_active = ?");
            $stmt->bind_param("iiii", $client_id, $client_master_id, $is_active, $is_active);

            if ($stmt->execute()) {
                $stmt->bind_result($count);
                while ($stmt->fetch()) {
                    $response["count"] = $count;
                }
                $response["error"] = false;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function isMetricsUpdated($field_value)
    {
        try {
            $is_active = 1;
            $stmt = $this->conn->prepare("SELECT last_updated_date, metrics_last_updated_date FROM client_cache_log WHERE client_id = ? AND is_active = ?");
            $stmt->bind_param("si", $field_value, $is_active);
            if ($stmt->execute()) {
                $stmt->bind_result($last_updated_date, $metrics_last_updated_date);
                while ($stmt->fetch()) {
                    $response["last_updated_date"] = $last_updated_date;
                    $response["metrics_last_updated_date"] = $metrics_last_updated_date;
                }
                if (!isset($metrics_last_updated_date) || trim($metrics_last_updated_date) === '') {
                    $response["isMetricsUpdated"] = false;
                } else {
                    if ($response["metrics_last_updated_date"] > $response["last_updated_date"]) {
                        $response["isMetricsUpdated"] = true;
                    } else {
                        $response["isMetricsUpdated"] = false;
                    }

                }
                $response["error"] = false;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function send_mail($to, $subject, $message_body, $from, $from_email, $pdf = false, $pdf_name = false, $cc_mail = false)
    {
        $mail = new PHPMailer(true);
        try {
            // $mail->SMTPDebug = 2; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host = SMTP_HOST; // Specify main and backup SMTP servers
            $mail->SMTPAuth = true; // Enable SMTP authentication
            $mail->Username = SMTP_USERNAME; // SMTP username
            $mail->Password = SMTP_PASSWORD; // SMTP password
            $mail->SMTPSecure = 'STARTTLS'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port = SMTP_PORT;

            $mail->setFrom($from_email, $from);
            $mail->addAddress($to); // Add a recipient

            //Content
            if (!empty($pdf_name)) {
                $mail->AddStringAttachment($pdf, $pdf_name, "base64", "application/pdf");
            }
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $message_body;

            if (!empty($cc_mail)) {
                $mail->AddCC(CC_MAIL_ID);
            }
            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }

    public function getAuditActivityData($client_master_id, $audit_type)
    {
        try {
            $sql = "SELECT audit_type FROM client_audit_activities WHERE client_master_id = ? AND audit_type = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("is", $client_master_id, $audit_type);
                if ($stmt->execute()) {
                    $stmt->store_result();
                    $stmt->bind_result($audit_type);
                    $stmt->fetch();
                    $num_rows = $stmt->num_rows;
                    $stmt->close();
                    if ($num_rows > 0) {
                        $response["error"] = false;
                        $response["field_key"] = $audit_type;
                    } else {
                        $response["error"] = true;
                        $response["field_key"] = '';
                    }
                } else {
                    $response["error"] = true;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function checkCategoryExistByClientID($client_id, $client_master_id)
    {
        try {
            $sql = "SELECT count(1)
                    FROM client_category_master
                    WHERE client_id = ? and client_master_id = ?";
            $stmt = $this->conn->prepare($sql);
            $stmt->bind_param("ii", $client_id, $client_master_id);
            if ($stmt->execute()) {
                $stmt->bind_result($count);
                while ($stmt->fetch()) {
                    $response["count"] = $count;
                }
                if ($count > 0) {
                    $response["error"] = false;
                } else {
                    $response["error"] = true;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAccountDataCheck($client_master_id, $account_code)
    {
        try {
            $sql = "SELECT id FROM  master_companies_account_categories WHERE client_master_id = ?  AND account_code = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("is", $client_master_id, $account_code);
                if ($stmt->execute()) {
                    $stmt->store_result();
                    $stmt->bind_result($id);
                    $stmt->fetch();
                    $num_rows = $stmt->num_rows;
                    $stmt->close();
                    if ($num_rows > 0) {
                        $response["error"] = false;
                        $response["field_key"] = $id;
                    } else {
                        $response["error"] = true;
                        $response["field_key"] = '';
                    }
                } else {
                    $response["error"] = true;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getGroupAccountDataCheck($client_master_id, $client_id, $account_code)
    {
        try {
            $sql = "SELECT id FROM  group_companies_account_categories WHERE client_master_id = ? AND group_id = ? AND account_code = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("iis", $client_master_id, $client_id, $account_code);
                if ($stmt->execute()) {
                    $stmt->store_result();
                    $stmt->bind_result($id);
                    $stmt->fetch();
                    $num_rows = $stmt->num_rows;
                    $stmt->close();
                    if ($num_rows > 0) {
                        $response["error"] = false;
                        $response["field_key"] = $id;
                    } else {
                        $response["error"] = true;
                        $response["field_key"] = '';
                    }
                } else {
                    $response["error"] = true;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getDayTillMonthPdfReport($type)
    {
        switch ($type) {
            case 'TrendPNL':
                $startDate = date("Y-m-01", strtotime("-11 months"));
                $endDate = date('Y-m-d');
                break;
            case 'BalanceSheet':
                $startDate = date("Y-m-01", strtotime("-11 months"));
                $endDate = date('Y-m-d', strtotime("-1 months"));
                break;
            case 'CashFlow':
                $startDate = date("Y-m-01", strtotime("-10 months"));
                $endDate = date('Y-m-d', strtotime("-1 months"));
                break;
        }
        $i = 0;
        while (strtotime($startDate) <= strtotime($endDate)) {
            $dates[] = date("t M y", strtotime($startDate));
            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            $i++;
        }
        return $dates;
    }

    public function dateAfterDays($days, $date, $type = 'months')
    {
        $data = $date . '-01';
        $datetime = new DateTime($date);

        $datetime->modify($days . ' ' . $type);
        if ($type == "months") {
            $finalate = $datetime->format('Y-m-t');
        } else {
            $finalate = $datetime->format('Y-m-d');
        }
        if ($type == "years") {
            $yeardate = new DateTime($finalate);
            $yeardate->modify('-1 day');
            $returndate = $yeardate->format('Y-m-d');
        } else {
            $returndate = $finalate;
        }
        return $returndate;
    }

    public function is_leap_year($year) {
        return ((($year % 4) == 0) && ((($year % 100) != 0) || (($year % 400) == 0)));
    }
    public function getTokenFromDB($client_id, $client_master_id)
    {
        try {
            $sql = "SELECT client_id,generated_date,org, client_master_id, oauth_token, oauth_token_secret, is_authorized, req_oauth_token, page_name, session_handle, expires FROM client_oauth_token WHERE client_id = ? AND client_master_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ii", $client_id, $client_master_id);
                $stmt->execute();
                $stmt->store_result();
                //$stmt->close();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($client_id, $generated_date,$org, $client_master_id, $oauth_token, $oauth_token_secret, $is_authorized, $req_oauth_token, $page_name, $session_handle, $expires);
                    while ($result = $stmt->fetch()) {
                        $response["error"] = false;
                        $response["message"] = RECORD_FOUND;
                        $response["client_id"] = $client_id;
                        $response["generated_date"] = $generated_date;
                        $response["org"] = $org;
                        $response["client_master_id"] = $client_master_id;
                        $response["oauth_token"] = $oauth_token;
                        $response["oauth_token_secret"] = $oauth_token_secret;
                        $response["session_handle"] = $session_handle;
                        $response["expires"] = $expires;
                        $response['is_authorized'] = $is_authorized;
                        $response['req_oauth_token'] = $req_oauth_token;
                        $response['page_name'] = $page_name;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }
    /**
     * Making Request for XeroAuth
     * @author Systimanx (chella@systimanx)
     * @param [type] $client_id
     * @param [type] $function_name
     * @param [type] $XeroAuth
     * @param [type] $method
     * @param [type] $url
     * @param array $params
     * @param string $xml
     * @param string $format
     * @return void
     */
    public function makeRequest($client_id=null, $function_name, $XeroAuth, $method, $url, $params = array(), $xml = "", $format = 'xml',$client_master_id=null)
    {
       /*  if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        if (!isset($_SESSION['API_HIT']) || empty($_SESSION['API_HIT'])) {
            $_SESSION['API_HIT'] = 1;
        } else {
            $_SESSION['API_HIT'] = $_SESSION['API_HIT'] + 1;
        }
        if ($_SESSION['API_HIT'] === 57){
            sleep(3);
            unset($_SESSION['API_HIT']);
        } */
        sleep(2);
        if(!empty($client_master_id)){
            $token = $this->getTokenFromDB($client_id,$client_master_id);
            $future_date_time = date('Y-m-d H:i:s', strtotime("+25 minute", strtotime($token['generated_date'])));
            $date = date("Y-m-d H:i:s");
            if($date >= $future_date_time){
                $refreshResponse = $XeroAuth->refreshToken($token['oauth_token'], $token['session_handle']);
                // print_r($refreshResponse); exit;
                if ($XeroAuth->response['code'] == 200) {
                    $oauth_new_token = $refreshResponse['oauth_token'];
                    $oauth_token_secret = $refreshResponse['oauth_token_secret'];
                    $session_handle = $refreshResponse['oauth_session_handle'];
                    $expires = time() + intval($refreshResponse['oauth_expires_in']);
                    $response = $this->saveToken($client_id, $oauth_new_token, $oauth_token_secret, 1, $token['org'], '', "", $session_handle, $expires, $client_master_id);
                    $newtoken = $this->getTokenFromDB($client_id, $client_master_id);
                    $XeroAuth->config['access_token'] = $newtoken['oauth_token'];
                    $XeroAuth->config['access_token_secret'] = $newtoken['oauth_token_secret'];                    
                } 
            }
            $response = $XeroAuth->request($method, $url, $params, $xml, $format);
        }else{
            $response = $XeroAuth->request($method,$url,$params,$xml,$format);
        }
       /*  dump($function_name);
        dump($response['response']); */
        $responseCode = $response['code'];
        try {

            $this->conn->autocommit(false);
            if ($stmt = $this->conn->prepare("INSERT INTO api_hits_history (client_id, method_name, api_method, api_endpoint, client_master_id,response_data,response_code) values(?, ?, ?, ?, ?,?,?)")) {
                $stmt->bind_param("isssisi", $client_id, $function_name, $method, $url, $client_master_id,json_encode($response),$responseCode);
                $result = $stmt->execute();

                $stmt->close();

                if ($result) {
                    $this->conn->commit();
                    return true;
                } else {
                    return false;
                    }
                } else {
                    return false;
                }

            } catch (Exception $e) {
                $this->conn->rollback();
                echo $e->getMessage();
            }
        

        return $response;

    }
    public function saveToken($client_id, $oauth_token, $oauth_token_secret, $is_authorized, $org, $req_oauth_token, $page_name, $session_handle = '', $expires = '', $client_master_id)
    {
        try {
            $this->conn->autocommit(false);
            $date = date("Y-m-d H:i:s");
            $is_active = 1;
            $response = array();
            //  $org = (empty($org)) ? 
            $tokenResult = $this->getTokenFromDB($client_id, $client_master_id);
            if ($tokenResult["error"] == true) {
                $sql = "INSERT INTO client_oauth_token(client_id, oauth_token, oauth_token_secret, generated_date, is_authorized, req_oauth_token, page_name,session_handle,expires,client_master_id) values(?, ?, ?, ?,?, ?, ?,?,?,?)";
                $e = 1;
            } else {
                if (strlen(trim($page_name)) <= 0) {
                    $sql = "UPDATE client_oauth_token set oauth_token = ?, oauth_token_secret = ?, is_authorized = ?, generated_date = ?, org = ?, session_handle = ?, expires = ? where client_id = ? AND  client_master_id = ?";
                } else {
                    $sql = "UPDATE client_oauth_token set oauth_token = ?, oauth_token_secret = ?, is_authorized = ?, generated_date = ?, org = ?, page_name = ?, session_handle = ?, expires = ? where client_id = ? AND  client_master_id = ?";
                }
                $e = 2;
            }
            if ($stmt = $this->conn->prepare($sql)) {
                if ($e == 1) {
                    $stmt->bind_param("isssissssi", $client_id, $oauth_token, $oauth_token_secret, $date, $is_authorized, $req_oauth_token, $page_name, $session_handle, $expires, $client_master_id);
                } else {
                    if (strlen(trim($page_name)) <= 0) {
                        $stmt->bind_param("ssissssii", $oauth_token, $oauth_token_secret, $is_authorized, $date, $org, $session_handle, $expires, $client_id, $client_master_id);
                    } else {
                        $stmt->bind_param("ssisssssii", $oauth_token, $oauth_token_secret, $is_authorized, $date, $org, $page_name, $session_handle, $expires, $client_id, $client_master_id);
                    }
                }
                $result = $stmt->execute();

                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    $response["message"] = KEY_GENERATE_SUCCESS;

                } else {
                    $response["error"] = true;
                    $response["message"] = KEY_GENERATE_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

   
    /**
     * close the database connection
     */
    public function __destruct()
    {
        // close the database connection
        $this->db->closeconnection();
    }

}
