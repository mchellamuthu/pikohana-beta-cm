<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
require_once dirname(__FILE__) . '/DbConnect.php';
class client extends utility
{

    // private $conn;
    public $db;

    public function __construct()
    {
        // opening db connection
        $this->db = new DbConnect();
        $this->conn = $this->db->connect();
    }

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function createClient($report_start_date, $report_end_date, $default_currency, $companyname, $weekly_monthly_report, $audit_activity_report, $mrr_graph)
    {
        try {
            $response = array();
            $this->conn->autocommit(false);
            $is_active = 1;

            $check_company_exist = $this->checkIfClientCompanyExist($companyname, $is_active);

            if ($check_company_exist['error'] == true) {
                $sql = "INSERT INTO client_master_detail (company_name, report_start_date, report_end_date, default_currency, is_active, created_date, last_updated_date, pdf_report_status, audit_activity_report, mrr_graph) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                $date = date("Y-m-d h:i:s");
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param("ssssissiii", $companyname, $report_start_date, $report_end_date, $default_currency, $is_active, $date, $date, $weekly_monthly_report, $audit_activity_report, $mrr_graph);
                    $result = $stmt->execute();

                    if ($stmt->errno == 1062) {
                        $response["error"] = true;
                        $response["message"] = DUPLICATE_ENTRY;
                        return $response;
                    }
                    $stmt->close();

                    if ($result) {
                        $this->conn->commit();
                        $response["error"] = false;
                        $response["message"] = COMPANY_CREATED_SUCCESSFULLY;
                    } else {
                        $response["error"] = true;
                        $response["message"] = CREATE_FAILED;
                    }

                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }

            } else {
                $response['error'] = true;
                $response['message'] = COMPANY_EXIST;
            }
            return $response;

        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function createClientUser($client_name, $mobile, $email, $is_admin, $password, $selectedClients, $selectedGroups = null, $client_master_id)
    {
        try {
            $response = array();
            $this->conn->autocommit(false);
            $userResult = array();
            require_once 'PassHash.php';
            $password_hash = PassHash::hash($password);
            $sql = "INSERT INTO client_master (name, mobile, email, is_active, password_hash, api_key, created_date, is_admin) values(?,?, ?, ?, ?, ?, ?, ?)";

            $userResult = $this->getFieldByID('client_master', 'email', $email, 'client_id');
            if ($userResult["error"] == true) {
                // if (strlen($is_admin) <= 0) {
                //     $is_admin = 0;
                // }
                $date = date("Y-m-d h:i:s");
                $is_active = 1;
                $api_key = $this->generateApiKey();
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param("sssisssi", $client_name, $mobile, $email, $is_active, $password_hash, $api_key, $date, $is_admin);
                    $result = $stmt->execute();

                    if ($stmt->errno == 1062) {
                        $response["error"] = true;
                        $response["message"] = DUPLICATE_ENTRY;
                        return $response;
                    }

                    if ($result) {
                        $stmt->close();
                        $client_id = $this->conn->insert_id;
                        $response["client_id"] = $client_id;
                        $response["error"] = false;

                        if (sizeof($selectedClients) > 0) {
                            $GetMappingDetails = $this->GetUserMappingDetails($client_id, $selectedClients, $client_master_id);

                            if ($GetMappingDetails['error'] == false) {
                                $this->conn->commit();
                                $application_ids = [1, 2];
                                $report_ids = [1, 3, 4];
                                // if ($role == 1) {
                                //     $application_ids = [1, 2];
                                //     $report_ids = [1, 3, 4];
                                // } else {
                                //     $application_ids = [2];
                                //     $report_ids = [4];
                                // }
                                $objAppln = new clientApplicationMap();
                                $setApplication = $objAppln->setApplicationReports($client_id, $selectedClients, $application_ids, true, $report_ids, $is_admin);
                                if (sizeof($selectedGroups) > 0) {
                                    $objAppln->setApplicationReports($client_id, $selectedGroups, $application_ids, true, $report_ids, $is_admin);
                                }
                               
                               
                                $response["error"] = false;
                                $response["message"] = CLIENT_USER_CREATED_SUCCESSFULLY;
                            } else {
                                $response["error"] = true;
                                $response["message"] = CLIENT_USER_CREATE_FAILED;
                            }
                        } else {
                            $storeMappingDetails = $this->storeUserMappingDetails($client_id, $client_master_id);

                            if ($storeMappingDetails['error'] == false) {
                                $this->conn->commit();
                                $application_ids = [1, 2];
                                $report_ids = [1, 3, 4];
                                // if ($role == 1) {
                                //     $application_ids = [1, 2];
                                //     $report_ids = [1, 3, 4];
                                // } else {
                                //     $application_ids = [2];
                                //     $report_ids = [4];
                                // }
                                $objAppln = new clientApplicationMap();
                                $setApplication = $objAppln->setApplicationReports($client_id, $selectedClients, $application_ids, true, $report_ids, $is_admin);
                                $objAppln->setApplicationReports($client_id, $selectedGroups, $application_ids, true, $report_ids, $is_admin);
                                $response["error"] = false;
                                $response["message"] = CLIENT_USER_CREATED_SUCCESSFULLY;
                            } else {
                                $response["error"] = true;
                                $response["message"] = CLIENT_USER_CREATE_FAILED;
                            }
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = CLIENT_USER_CREATE_FAILED;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            } else {
                // User with same email already existed in the db
                $response["error"] = true;
                $response["message"] = CLIENT_USER_ALREADY_EXISTED;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function GetUserMappingDetails($client_id, $selectedClients, $default_client)
    {
        foreach ($selectedClients as $key => $value) {
            $is_default = 0;
            $client_master_id = $value['id'];
            /* set as default client for user*/
            if ($client_master_id == $default_client) {
                $is_default = 1;
            }
            $sql = "INSERT INTO client_user_mapping (client_id, client_master_id, is_default) values(?, ?, ?)";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("iii", $client_id, $client_master_id, $is_default);
                $result = $stmt->execute();
                $response["error"] = false;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
        }
    }

    public function clientGroupMapping($client_id, $selectedGroups, $defaultGroup)
    {
        foreach ($selectedGroups as $key => $value) {
            $is_default = 0;
            $group_id = $value['id'];
            /* set as default client for user*/
            if ($defaultGroup == $group_id) {
                $is_default = 1;
            }
            $group_sql = "INSERT INTO client_group_mapping (client_id, group_id, is_default) values($client_id, $group_id, $is_default)";
            if ($group_stmt = $this->conn->prepare($group_sql)) {
                $result = $group_stmt->execute();
                if ($result) {
                    $group_stmt->close();
                    $this->conn->commit();
                }
                $response["error"] = false;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
        }
    }

    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password)
    {
        try {
            $is_active = 1;
            $response = array();

            $sql = "SELECT password_hash FROM client_master WHERE email = ? AND is_active = ?";
            // fetching user by user id
            if ($stmt = $this->conn->prepare($sql)) {

                $stmt->bind_param("si", $email, $is_active);

                $stmt->execute();

                $stmt->bind_result($password_hash);

                $stmt->store_result();

                if ($stmt->num_rows > 0) {
                    // Found user
                    // Now verify the password
                    $stmt->fetch();

                    $stmt->close();

                    if (PassHash::check_password($password_hash, $password)) {
                        // User password is correct
                        $user = array();
                        $user = $this->getClientByClientID($email);
                        $objAppln = new clientApplicationMap();
                        $db1 = new dashboardReport();
                        if ($user['error'] == false) {
                            if ($user['login_count'] == 0) {
                                if ($user['is_admin'] == 1) {
                                    foreach ($user['clients'] as $key => $value) {
                                        $client_master_id = $value['client_master_id'];
                                        $resp = array();
                                        $rResp = array();
                                        $resp = $objAppln->addNewClientApplicationMap($user['client_id'], $client_master_id, 1, "", 1);

                                        if ($resp["error"] == false) {
                                            $objReport = new reportClientApplicationMap();
                                            $aReports = array();
                                            $aReports = [FLASH, PNL];
                                            foreach ($aReports as $k => $v) {
                                                $rResp = $objReport->addNewReportClientApplicationMap($v, $user["client_id"], XERO, $client_master_id);
                                            }
                                        }

                                        $utility = new utility();
                                        $res_auth_key = $utility->getFieldByID('application_master', 'application_id', 2, 'application_auth_key');
                                        if ($res_auth_key['error'] == false) {
                                            $auth_key = $res_auth_key['field_key'];
                                        }

                                        $Zresp = array();
                                        $objZReport = array();
                                        $Zresp = $objAppln->addNewClientApplicationMap($user['client_id'], $client_master_id, 2, $auth_key, 1);
                                        if ($Zresp["error"] == false) {
                                            $objZReport = new reportClientApplicationMap();
                                            $aZReports = array();
                                            $aZReports = [KPI];
                                            foreach ($aZReports as $k => $v) {
                                                $ZResp = $objZReport->addNewReportClientApplicationMap($v, $user["client_id"], ZOHO, $client_master_id);
                                            }
                                        }
                                    }
                                }
                            }
                            $this->updateLoginCount($user['client_id'], $user['login_count']);
                            $searchItems = array();
                            foreach ($user['clients'] as $key => $value) {
                                $client_master_id = $value['client_master_id'];
                                $result = $objAppln->getAllClientApplicationWrapper($user['client_id'], $client_master_id, 1);
                                $user["clientApplicationMapDetails"] = ($result["error"] == false) ? $result['clientApplicationMapDetails'] : [];
                                // if ($is_admin != 1) {
                                //     $searchItems['client_id'] = $user['client_id'];
                                // } else {
                                //     $searchItems['client_master_id'] = $client_master_id;
                                // }

                                $searchItems['client_id'] = $user['client_id'];
                                // $searchItems['client_master_id'] = $client_master_id;
                                $searchItems['is_active'] = 1;
                                $searchItems["groupby"] = 1;
                                $rResponse = $db1->getAllTrackingCategories($searchItems);
                                $user["xeroCategories"] = ($rResponse["error"] == false) ? $rResponse['trackingCategories'] : [];
                            }

                            $response["clientDetails"] = $user;
                            $resp = null;
                            $rResp = null;
                            $aReports = null;
                            $Zresp = null;
                            $objZReport = null;
                            $aZReports = null;
                        } else {
                            return $user;
                        }
                    } else {
                        // user password is incorrect
                        $response["error"] = true;
                        $response["message"] = INCORRECT_CREDENTIALS;
                        return $response;
                    }
                } else {
                    $stmt->close();

                    // user not existed with the user id
                    $response["error"] = true;
                    $response["message"] = INCORRECT_CREDENTIALS;
                    return $response;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;

            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function updateLoginCount($client_id, $login_count)
    {

        try {
            $this->conn->autocommit(false);
            $login_count = $login_count + 1;
            $is_active = 1;
            $date = date('Y-m-d');
            if ($stmt = $this->conn->prepare("UPDATE client_master set login_count = ?, last_login = ?  WHERE client_id = ? ")) {
                $stmt->bind_param("isi", $login_count, $date, $client_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = LOGIN_COUNT_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = LOGIN_COUNT_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function updateCushionValue($client_id, $cushion_value)
    {

        try {
            $this->conn->autocommit(false);

            $is_active = 1;
            if ($stmt = $this->conn->prepare("UPDATE client_master_detail set cushion_value = ?  WHERE client_master_id = ? ")) {
                $stmt->bind_param("si", $cushion_value, $client_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = CUSHION_VALUE_UPDATED;
                } else {
                    $response["error"] = true;
                    $response["message"] = CUSHION_VALUE_NOT_UPDATED;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function clearToken($client_id)
    {

        try {
            $this->conn->autocommit(false);

            $is_active = 1;
            if ($stmt = $this->conn->prepare("DELETE FROM client_oauth_token where client_id = ?")) {
                $stmt->bind_param("i", $client_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = CLIENT_TOKEN_REMOVED;
                } else {
                    $response["error"] = true;
                    $response["message"] = CLIENT_TOKEN_NOT_REMOVED;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    /**
     * old password validation
     * @param String $client_id user id to check in db
     * @return boolean
     */
    public function getOldPasswordFromDB($client_id)
    {
        try {
            $response = array();

            $sql = "SELECT password_hash FROM client_master WHERE client_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("i", $client_id);
                $stmt->execute();
                $stmt->bind_result($oldpassword);
                //    $stmt->store_result();
                $stmt->fetch();

                $response["error"] = false;
                $response["message"] = OLD_PASSWORD_EXIST;
                $response["password"] = $oldpassword;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function changePassword($client_id, $oldpassword, $password)
    {
        try {
            $response = array();
            $this->conn->autocommit(false);
            require_once 'PassHash.php';
            // Generating password hash
            $password_hash = PassHash::hash($password);
            $old_password_hash = PassHash::hash($oldpassword);
            $pwdResult = array();
            $pwdResult = $this->getOldPasswordFromDB($client_id);
            $old_password_hash_from_db = $pwdResult["password"];

            if (PassHash::check_password($old_password_hash_from_db, $oldpassword)) {
                if ($old_password_hash != $password_hash) {

                    $sql = "UPDATE client_master SET password_hash = ? WHERE client_id = ?";

                    if ($stmt = $this->conn->prepare($sql)) {
                        $stmt->bind_param("si", $password_hash, $client_id);
                        $result = $stmt->execute();
                        $stmt->close();
                    } else {
                        $response["error"] = true;
                        $response["message"] = QUERY_EXCEPTION;
                    }

                    if ($result) {
                        $this->conn->commit();
                        $response["error"] = false;
                        $response["message"] = CHANGE_PASSWORD_SUCCESSFUL;
                    } else {
                        $response["error"] = true;
                        $response["message"] = CHANGE_PASSWORD_FAILURE;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = OLD_NEW_PASSWORD_SAME;
                }
            } else {
                $response["error"] = true;
                $response["message"] = OLD_PASSWORD_WRONG;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function sendResetKey($email)
    {
        try {
            $utility = new utility();
            $res_is_user = $utility->getFieldByID('client_master', 'email', $email, 'client_id');
            if ($res_is_user["error"] == false) {
                $client_id = $res_is_user["field_key"];

                $usql = "UPDATE client_master SET reset_key = ?  WHERE email = ? ";
                $this->conn->autocommit(false);
                $resetKey = $this->generateApiKey();
                $response = array();
                if ($stmt = $this->conn->prepare($usql)) {
                    $stmt->bind_param("ss", $resetKey, $email);
                    $result = $stmt->execute();

                    $stmt->close();
                    if ($result) {
                        $this->conn->commit();
                        $subject = RESET_EMAIL_TITLE;
                        $body = RESET_EMAIL_CONTENT;
                        $resetURL = RESET_BASE_CLIENT_URL . $resetKey;
                        $userResult = $this->getFieldByID('client_master', 'email', $email, 'name');
                        $mail_search = array('^user_first_name^', '^reset_url^');
                        $mail_replace = array($userResult['field_key'], $resetURL);
                        $body = str_replace($mail_search, $mail_replace, $body);
                        $subject = str_replace($mail_search, $mail_replace, $subject);
                        $res = $this->saveMail($email, $subject, $body);

                        if ($res == true) {
                            $response["error"] = false;
                            $response["message"] = RESET_PASSWORD_SUCCESSFUL;
                        } else {
                            $response["error"] = true;
                            $response["message"] = RESET_PASSWORD_FAILURE;
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = RESET_PASSWORD_FAILURE;
                    }

                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            } else {
                $response["error"] = true;
                $response["message"] = EMAIL_DOES_NOT_EXIST;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function resetPassword($reset_key, $new_password)
    {
        try {

            // $res_is_user = $utility->getFieldByID('client_master', 'client_id', $user_id, 'is_admin');
            // if ($res_is_user["error"] == false) {
            //     $is_admin = $res_is_user["field_key"];
            // }

            $userResult = $this->getFieldByID('client_master', 'reset_key', $reset_key, 'client_id');
            if ($userResult['error'] == false) {
                $client_id = $userResult['field_key'];
                $password_hash = PassHash::hash($new_password);
                $reset_key = '';
                $sql = "UPDATE client_master SET password_hash = ?, reset_key = ? WHERE client_id = ?";
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param("ssi", $password_hash, $reset_key, $client_id);
                    $result = $stmt->execute();
                    $stmt->close();
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    $response["message"] = CHANGE_PASSWORD_SUCCESSFUL;

                } else {
                    $response["error"] = true;
                    $response["message"] = CHANGE_PASSWORD_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = USER_NOT_ACTIVATED;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function resetPasswordByAdmin($client_id)
    {
        try {

            $new_password = $this->generateString();
            $password_hash = PassHash::hash($new_password);
            $sql = "UPDATE client_master SET password_hash = ? WHERE client_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("si", $password_hash, $client_id);
                $result = $stmt->execute();
                $stmt->close();
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            if ($result) {
                $this->conn->commit();
                $response["error"] = false;
                $response["message"] = CHANGE_PASSWORD_SUCCESSFUL;
                $response["new_password"] = $new_password;
            } else {
                $response["error"] = true;
                $response["message"] = CHANGE_PASSWORD_FAILURE;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    // public function getClientByClientID($client_id)
    // {
    //     try {
    //         $is_active = 1;
    //         $user = array();
    //         if (is_numeric($client_id)) {
    //             $sql = "SELECT cm.client_id, cum.client_master_id, name, email, mobile, client_photo, company_name, company_owner,
    //             company_address, company_logo, time_zone, last_login, password_hash, security_question, security_answer,
    //             notification_status, cd.is_active, cd.created_date, cd.last_updated_date, cd.last_updated_by, api_key, login_count,
    //              cushion_value, report_start_date, report_end_date, default_currency, is_admin FROM client_master cm
    //              JOIN client_user_mapping cum ON cum.client_id = cm.client_id
    //               JOIN client_master_detail cd  ON cum.client_master_id = cd.client_master_id  WHERE cum.client_id = ? AND cm.is_active = ?";
    //         } else {
    //             $sql = "SELECT cm.client_id, cum.client_master_id, name, email, mobile, client_photo, company_name, company_owner,
    //             company_address, company_logo, time_zone, last_login, password_hash, security_question, security_answer,
    //             notification_status, cd.is_active, cd.created_date, cd.last_updated_date, cd.last_updated_by, api_key, login_count,
    //              cushion_value, report_start_date, report_end_date, default_currency, is_admin FROM client_master cm
    //              JOIN client_user_mapping cum ON cum.client_id = cm.client_id
    //               JOIN client_master_detail cd  ON cum.client_master_id = cd.client_master_id  WHERE cm.email = ? AND cm.is_active = ?";
    //         }

    //         if ($stmt = $this->conn->prepare($sql)) {
    //             if (is_numeric($client_id)) {
    //                 $stmt->bind_param("ii", $client_id, $is_active);
    //             } else {
    //                 $stmt->bind_param("si", $client_id, $is_active);
    //             }

    //             if ($stmt->execute()) {
    //                 $stmt->bind_result($client_log_id, $client_master_id, $name, $email, $mobile, $client_photo, $company_name, $company_owner, $company_address, $company_logo, $time_zone, $last_login, $password_hash, $security_question, $security_answer, $notification_status, $is_active, $created_date, $last_updated_date, $last_updated_by, $api_key, $login_count, $cushion_value, $report_start_date, $report_end_date, $default_currency, $is_admin);
    //                 $stmt->fetch();
    //                 $user = array();
    //                 $user["client_id"] = $client_log_id;
    //                 $user["client_master_id"] = $client_master_id;
    //                 $user["name"] = $name;
    //                 $user["mobile"] = $mobile;
    //                 $user["client_phone"] = $mobile;
    //                 $user["email"] = $email;
    //                 $user["client_photo"] = $client_photo;
    //                 $user["company_name"] = $company_name;
    //                 $user["company_owner"] = $company_owner;
    //                 $user["company_address"] = $company_address;
    //                 $user["company_logo"] = $company_logo;
    //                 $user["time_zone"] = $time_zone;
    //                 $user["last_login"] = $last_login;
    //                 $user["password_hash"] = $password_hash;
    //                 $user["security_question"] = $security_question;
    //                 $user["security_answer"] = $security_answer;
    //                 $user["notification_status"] = $notification_status;
    //                 $user["created_date"] = $created_date;
    //                 $user["last_updated_date"] = $last_updated_date;
    //                 $user["last_updated_by"] = $last_updated_by;
    //                 $user["is_active"] = $is_active;
    //                 $user["api_key"] = $api_key;
    //                 $user["login_count"] = $login_count;
    //                 $user["cushion_value"] = $cushion_value;
    //                 $user["report_start_date"] = $report_start_date;
    //                 $user["report_end_date"] = $report_end_date;
    //                 $user["default_currency"] = $default_currency;
    //                 $user["is_admin"] = $is_admin;
    //                 $user["notifications"] = array();
    //                 $aNotifications = array();
    //                 $stmt->close();
    //                 $aNotifications = $this->getUnReadNotifications($user["client_id"]);
    //                 $user["notifications"] = $aNotifications['notificationDetails'];
    //                 $db = new clientApplicationMap();
    //                 $result = $db->getAllClientApplicationWrapper($client_id, $is_active);
    //                 $user["clientApplicationMapDetails"] = ($result["error"] == false) ? $result['clientApplicationMapDetails'] : [];
    //                 $db2 = new reportClientApplicationMap();
    //                 $searchReport = array();
    //                 $searchReport['client_id'] = $client_id;
    //                 $searchReport['is_active'] = 1;
    //                 $result1 = $db2->getAllReportClientApplicationMapWrapper($searchReport);
    //                 $user["reportClientApplicationMapDetails"] = ($result1["error"] == false) ? $result1['reportClientApplicationMapDetails'] : [];
    //                 $db1 = new dashboardReport();
    //                 //if ($is_admin == 0) {
    //                 $searchItems['client_id'] = $client_id;
    //                 //} else {
    //                 //    $searchItems['client_master_id'] = $client_id;
    //                 //}
    //                 $searchItems['is_active'] = 1;
    //                 $searchItems["groupby"] = 1;
    //                 $rResponse = $db1->getAllTrackingCategories($searchItems);
    //                 $user["xeroCategories"] = ($rResponse["error"] == false) ? $rResponse['trackingCategories'] : [];
    //             } else {
    //                 $user["error"] = true;
    //                 $user["message"] = QUERY_EXCEPTION . " - " . CLIENT_QUERY;
    //             }
    //         } else {
    //             $user["error"] = true;
    //             $user["message"] = QUERY_EXCEPTION . " - " . CLIENT_QUERY;
    //         }

    //         return $user;

    //     } catch (Exception $e) {
    //         $this->conn->rollback();
    //         echo $e->getMessage();
    //     }
    // }

    public function getClientByClientID($client_id, $master_id = null)
    {
        try {
            $is_active = 1;
            $user = array();

            if (is_numeric($client_id)) {
                $sql = 'SELECT cm.client_id, name, email, mobile, last_login, password_hash, api_key, login_count, is_admin,
                            CASE WHEN (cum.client_master_id is NULL) THEN cgm.group_id ELSE cum.client_master_id END AS client_master_id,		
                            CASE WHEN (cd.client_photo is NULL) THEN cd1.client_photo ELSE cd.client_photo END AS client_photo,
                            CASE WHEN (cd.company_name is NULL) THEN cd1.company_name ELSE cd.company_name END AS company_name,
                            CASE WHEN (cd.company_owner is NULL) THEN cd1.company_owner ELSE cd.company_owner END AS company_owner,
                            CASE WHEN (cd.company_address is NULL) THEN cd1.company_address ELSE cd.company_address END AS company_address,
                            CASE WHEN (cd.company_logo is NULL) THEN cd1.company_logo ELSE cd.company_logo END AS company_logo,
                            CASE WHEN (cd.time_zone is NULL) THEN cd1.time_zone ELSE cd.time_zone END AS time_zone,		 
                            CASE WHEN (cd.security_question is NULL) THEN cd1.security_question ELSE cd.security_question END AS security_question,
                            CASE WHEN (cd.security_answer is NULL) THEN cd1.security_answer ELSE cd.security_answer END AS security_answer,
                            CASE WHEN (cd.notification_status is NULL) THEN cd1.notification_status ELSE cd.notification_status END AS notification_status,
                            CASE WHEN (cd.is_active is NULL) THEN cd1.is_active ELSE cd.is_active END AS is_active,
                            CASE WHEN (cd.created_date is NULL) THEN cd1.created_date ELSE cd.created_date END AS created_date,
                            CASE WHEN (cd.last_updated_date is NULL) THEN cd1.last_updated_date ELSE cd.last_updated_date END AS last_updated_date,
                            CASE WHEN (cd.last_updated_by is NULL) THEN cd1.last_updated_by ELSE cd.last_updated_by END AS last_updated_by,
                            CASE WHEN (cd.cushion_value is NULL) THEN cd1.cushion_value ELSE cd.cushion_value END AS cushion_value,
                            CASE WHEN (cd.report_start_date is NULL) THEN cd1.report_start_date ELSE cd.report_start_date END AS report_start_date,
                            CASE WHEN (cd.report_end_date is NULL) THEN cd1.report_end_date ELSE cd.report_end_date END AS report_end_date,
                            CASE WHEN (cd.default_currency is NULL) THEN cd1.default_currency ELSE cd.default_currency END AS default_currency	 
                        FROM client_master cm 
                            LEFT JOIN client_user_mapping cum ON cum.client_id = cm.client_id and cum.is_default = 1
                            LEFT JOIN client_master_detail cd ON cum.client_master_id = cd.client_master_id 
                            LEFT JOIN client_group_mapping cgm ON cgm.client_id = cm.client_id and cgm.is_default = 1
                            LEFT JOIN client_master_detail cd1 ON cgm.group_id = cd1.client_master_id 
                        WHERE cm.client_id = ? AND cm.is_active = ?';

                $sql1 = 'SELECT *  FROM (
                                SELECT cum.client_id, cum.client_master_id, cd.company_name, cum.is_default, cd.default_currency, cd.is_group
                                    FROM client_user_mapping as cum
                                    JOIN client_master_detail as cd ON cd.client_master_id = cum.client_master_id
                                    where cum.client_id = ?
                                UNION
                                SELECT  cgm.client_id, cmd.client_master_id, cmd.company_name, cgm.is_default, cmd.default_currency,cmd.is_group
                                    FROM client_group_mapping as cgm
                                    JOIN client_master_detail as cmd ON cmd.client_master_id = cgm.group_id
                                    where cgm.client_id = ?
                            ) a ORDER BY is_default desc';
            } else {
                $sql = 'SELECT cm.client_id, name, email, mobile, last_login, password_hash, api_key, login_count, is_admin,
                            CASE WHEN (cum.client_master_id is NULL) THEN cgm.group_id ELSE cum.client_master_id END AS client_master_id,		
                            CASE WHEN (cd.client_photo is NULL) THEN cd1.client_photo ELSE cd.client_photo END AS client_photo,
                            CASE WHEN (cd.company_name is NULL) THEN cd1.company_name ELSE cd.company_name END AS company_name,
                            CASE WHEN (cd.company_owner is NULL) THEN cd1.company_owner ELSE cd.company_owner END AS company_owner,
                            CASE WHEN (cd.company_address is NULL) THEN cd1.company_address ELSE cd.company_address END AS company_address,
                            CASE WHEN (cd.company_logo is NULL) THEN cd1.company_logo ELSE cd.company_logo END AS company_logo,
                            CASE WHEN (cd.time_zone is NULL) THEN cd1.time_zone ELSE cd.time_zone END AS time_zone,		 
                            CASE WHEN (cd.security_question is NULL) THEN cd1.security_question ELSE cd.security_question END AS security_question,
                            CASE WHEN (cd.security_answer is NULL) THEN cd1.security_answer ELSE cd.security_answer END AS security_answer,
                            CASE WHEN (cd.notification_status is NULL) THEN cd1.notification_status ELSE cd.notification_status END AS notification_status,
                            CASE WHEN (cd.is_active is NULL) THEN cd1.is_active ELSE cd.is_active END AS is_active,
                            CASE WHEN (cd.created_date is NULL) THEN cd1.created_date ELSE cd.created_date END AS created_date,
                            CASE WHEN (cd.last_updated_date is NULL) THEN cd1.last_updated_date ELSE cd.last_updated_date END AS last_updated_date,
                            CASE WHEN (cd.last_updated_by is NULL) THEN cd1.last_updated_by ELSE cd.last_updated_by END AS last_updated_by,
                            CASE WHEN (cd.cushion_value is NULL) THEN cd1.cushion_value ELSE cd.cushion_value END AS cushion_value,
                            CASE WHEN (cd.report_start_date is NULL) THEN cd1.report_start_date ELSE cd.report_start_date END AS report_start_date,
                            CASE WHEN (cd.report_end_date is NULL) THEN cd1.report_end_date ELSE cd.report_end_date END AS report_end_date,
                            CASE WHEN (cd.default_currency is NULL) THEN cd1.default_currency ELSE cd.default_currency END AS default_currency	 
                        FROM client_master cm 
                            LEFT JOIN client_user_mapping cum ON cum.client_id = cm.client_id and cum.is_default = 1
                            LEFT JOIN client_master_detail cd ON cum.client_master_id = cd.client_master_id 
                            LEFT JOIN client_group_mapping cgm ON cgm.client_id = cm.client_id and cgm.is_default = 1
                            LEFT JOIN client_master_detail cd1 ON cgm.group_id = cd1.client_master_id 
                        WHERE cm.email = ? AND cm.is_active = ?';

                $sql1 = 'SELECT *  FROM (
                                SELECT cum.client_id, cum.client_master_id, cd.company_name, cum.is_default, cd.default_currency, cd.is_group
                                    FROM client_user_mapping as cum
                                    JOIN client_master_detail as cd ON cd.client_master_id = cum.client_master_id
                                    JOIN client_master cm ON cm.client_id = cum.client_id
                                    where cm.email = ? AND cm.is_active = ?
                                    UNION
                                SELECT  cgm.client_id, cmd.client_master_id, cmd.company_name, cgm.is_default, cmd.default_currency,cmd.is_group
                                    FROM client_group_mapping as cgm
                                    JOIN client_master_detail as cmd ON cmd.client_master_id = cgm.group_id
                                    JOIN client_master cm ON cm.client_id = cgm.client_id
                                    where cm.email = ? AND cm.is_active = ?
                            ) a ORDER BY is_default desc';
            }

            if ($stmt = $this->conn->prepare($sql)) {
                if (is_numeric($client_id)) {
                    $stmt->bind_param('ii', $client_id, $is_active);
                } else {
                    $stmt->bind_param('si', $client_id, $is_active);
                }

                if ($stmt->execute()) {
                    $stmt->bind_result($client_log_id, $name, $email, $mobile, $last_login, $password_hash, $api_key, $login_count, $is_admin, $client_master_id, $client_photo, $company_name, $company_owner, $company_address, $company_logo, $time_zone, $security_question, $security_answer, $notification_status, $is_active, $created_date, $last_updated_date, $last_updated_by, $cushion_value, $report_start_date, $report_end_date, $default_currency);
                    $stmt->fetch();

                    $user = array();
                    $user['client_id'] = $client_log_id;
                    $user['client_master_id'] = $client_master_id;
                    $user['name'] = $name;
                    $user['mobile'] = $mobile;
                    $user['client_phone'] = $mobile;
                    $user['email'] = $email;
                    $user['client_photo'] = $client_photo;
                    //$user['company_name'] = $company_name;
                    $user['company_owner'] = $company_owner;
                    $user['company_address'] = $company_address;
                    $user['company_logo'] = $company_logo;
                    $user['time_zone'] = $time_zone;
                    $user['last_login'] = $last_login;
                    $user['password_hash'] = $password_hash;
                    $user['security_question'] = $security_question;
                    $user['security_answer'] = $security_answer;
                    $user['notification_status'] = $notification_status;
                    $user['created_date'] = $created_date;
                    $user['last_updated_date'] = $last_updated_date;
                    $user['last_updated_by'] = $last_updated_by;
                    $user['is_active'] = $is_active;
                    $user['api_key'] = $api_key;
                    $user['login_count'] = $login_count;
                    $user['cushion_value'] = $cushion_value;
                    $user['report_start_date'] = $report_start_date;
                    $user['report_end_date'] = $report_end_date;
                    $user['default_currency'] = $default_currency;
                    $user['is_admin'] = $is_admin;
                    $user['notifications'] = array();
                    $stmt->close();

                    $aNotifications = array();
                    $aNotifications = $this->getUnReadNotifications($user['client_id']);
                    $user['notifications'] = $aNotifications['notificationDetails'];

                    $db = new clientApplicationMap();
                    $result = $db->getAllClientApplicationWrapper($client_id, $client_master_id, $is_active);
                    $user['clientApplicationMapDetails'] = ($result['error'] == false) ? $result['clientApplicationMapDetails'] : [];

                    $db2 = new reportClientApplicationMap();
                    $searchReport = array();
                    $searchReport['client_id'] = $client_id;
                    $searchReport["client_master_id"] = $client_master_id;
                    $searchReport['is_active'] = 1;
                    $result1 = $db2->getAllReportClientApplicationMapWrapper($searchReport);
                    $user['reportClientApplicationMapDetails'] = ($result1['error'] == false) ? $result1['reportClientApplicationMapDetails'] : [];

                    $db1 = new dashboardReport();
                    $searchItems['client_id'] = $client_id;
                    $searchItems["client_master_id"] = $client_master_id;
                    $searchItems['is_active'] = 1;
                    $searchItems['groupby'] = 1;
                    $rResponse = $db1->getMappedTrackingCategories($searchItems);
                    $user['xeroCategories'] = ($rResponse['error'] == false) ? $rResponse['trackingCategories'] : [];

                    if ($is_admin != 1) {
                        $superAdminID = $this->getSuperAdminID($client_master_id);
                        $master_client_id = $superAdminID['client_id'];
                    } else {
                        $master_client_id = $client_id;
                    }

                    $user["masterXeroCategories"] = array();
                    $searchMaster = array();
                    $searchMaster['client_id'] = $client_id;
                    $searchMaster["client_master_id"] = $master_id;
                    $searchMaster['is_active'] = 1;
                    $searchMaster["groupby"] = 1;

                    $rResponse = $db1->getMasterTrackingCategories($searchMaster);
                    $user["masterXeroCategories"] = ($rResponse["error"] == false) ? $rResponse['trackingCategories'] : [];

                    $user['error'] = false;
                } else {
                    $user['error'] = true;
                    $user['message'] = QUERY_EXCEPTION . ' - ' . CLIENT_QUERY;
                }
            }

            if ($stmte = $this->conn->prepare($sql1)) {
                if (is_numeric($client_id)) {
                    $stmte->bind_param('ii', $client_id, $client_id);
                } else {
                    $stmte->bind_param('sisi', $client_id, $is_active, $client_id, $is_active);
                }

                $stmte->execute();
                $stmte->store_result();
                if ($stmte->num_rows > 0) {
                    $stmte->bind_result($client_id, $client_master_id, $company_name, $is_default, $default_currency, $is_group);
                    while ($result = $stmte->fetch()) {
                        $temp = array();
                        $temp['client_id'] = $client_id;
                        $temp['client_master_id'] = $client_master_id;
                        $temp['company_name'] = $company_name;
                        $temp['is_default'] = $is_default;
                        $temp['is_group'] = $is_group;
                        $temp['default_currency'] = $default_currency;
                        $user['clients'][] = $temp;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $user['error'] = true;
                $user['message'] = QUERY_EXCEPTION . ' - ' . CLIENT_QUERY;
            }
            return $user;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function updateProfile($client_id, $name, $email, $mobile, $client_photo, $company_name, $company_owner, $company_address, $company_logo, $time_zone, $notification_status, $client_master_id)
    {
        try {
            $response = array();
            $utility = new utility();

            $this->conn->autocommit(false);
            $date = date("Y-m-d h:i:s");

            if (isset($client_photo) && isset($client_photo['name'])) {
                $module = 'client';
                $upload_result = array();
                $upload_result = $utility->uploadImages($client_photo, $module, $client_id);

                if ($upload_result["error"] == false) {
                    $profile_url = $upload_result["message"];
                } else {
                    return $upload_result;
                }
            } else if (isset($client_photo)) {
                $profile_url = $client_photo;
            }

            if (isset($company_logo) && isset($company_logo['name'])) {
                $module = 'company';
                $upload_result = array();
                $upload_result = $utility->uploadImages($company_logo, $module, $client_id);

                if ($upload_result["error"] == false) {
                    $company_logo_url = $upload_result["message"];
                } else {
                    return $upload_result;
                }
            } else if (isset($company_logo)) {
                $company_logo_url = $company_logo;
            }

            $sql = "UPDATE client_master_detail SET client_photo = ?, company_name = ?, company_owner = ?, company_address = ?, company_logo = ?, time_zone = ?, notification_status = ?, last_updated_date = ? WHERE client_master_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ssssssisi", $profile_url, $company_name, $company_owner, $company_address, $company_logo_url, $time_zone, $notification_status, $date, $client_master_id);
                $result = $stmt->execute();
                $stmt->close();
                if ($result) {
                    $superAdminID = $this->getSuperAdminID($client_master_id);
                    $is_admin = 2;
                    $res_is_admin = $utility->getFieldByID('client_master', 'client_id', $client_id, 'is_admin');
                    if ($res_is_admin['error'] == false) {
                        $is_admin = $res_is_admin['field_key'];
                    }
                    $childResult = $this->updateClientUserByAdmin($superAdminID['client_id'], $name, $mobile, $email, $is_admin, null, null, false);
                    if ($childResult["error"] == false) {
                        $this->conn->commit();
                        $response["error"] = false;
                        $response["message"] = UPDATE_PROFILE_SUCCESSFUL;
                        $aClientDetails = array();
                        $aClientDetails = $this->getClientByClientID($client_id);
                        $response["clientDetails"] = $aClientDetails;
                    } else {
                        $response["error"] = true;
                        $response["message"] = UPDATE_PROFILE_FAILURE;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }

            } else {
                $response['error'] = true;
                $response['message'] = QUERY_EXCEPTION;
            }

            $utility = null;
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }

    }

    public function updateNotification($client_id, $status)
    {
        try {
            $response = array();
            $this->conn->autocommit(false);
            if ($stmt = $this->conn->prepare("UPDATE client_master_detail set notification_status = ? WHERE client_master_id = ?")) {
                $stmt->bind_param("ii", $status, $client_id);
                $stmt->execute();
                $this->conn->commit();
                $num_affected_rows = $stmt->affected_rows;
                $stmt->close();
                $response["error"] = false;
                if ($status == 1) {
                    $response["message"] = NOTIFICATION_ENABLED_SUCCESS;
                } else {
                    $response["message"] = NOTIFICATION_DISABLED_SUCCESS;
                }

            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function archiveReport($report_history_id)
    {
        try {
            $response = array();
            $this->conn->autocommit(false);
            $is_archive = 1;
            if ($stmt = $this->conn->prepare("UPDATE report_history set is_archive = ? WHERE report_history_id = ?")) {
                $stmt->bind_param("ii", $is_archive, $report_history_id);
                $stmt->execute();
                $this->conn->commit();
                //TODO: unlink the file from folder
                $num_affected_rows = $stmt->affected_rows;
                $stmt->close();
                $response["error"] = false;
                $response["message"] = REPORT_ARCHIVED_SUCCESSFULLY;

            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function doShowReport($client_id, $start_date, $end_date)
    {
        try {
            //$is_active = 1;
            $is_archive = 0;
            $sql = "SELECT report_history_id, client_id, generated_date, r.report_id, r.report_name, history_url, is_archive FROM report_history rh JOIN report_master r ON rh.report_id=r.report_id WHERE generated_date >= ? AND generated_date <= ? AND client_id = ? AND is_archive = ? ";
            $response["reportDetails"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ssii", $start_date, $end_date, $client_id, $is_archive);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($report_history_id, $client_id, $generated_date, $report_id, $report_name, $history_url, $is_archive);
                    while ($result = $stmt->fetch()) {

                        $tmp = array();
                        $tmp["report_history_id"] = $report_history_id;
                        $tmp["client_id"] = $client_id;
                        $tmp["generated_date"] = $generated_date;
                        $tmp["report_id"] = $report_id;
                        $tmp["report_name"] = $report_name;
                        $tmp["history_url"] = $history_url;
                        $tmp["is_archive"] = $is_archive;
                        $response["reportDetails"][] = $tmp;
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function saveNotification($client_id, $notification_message, $is_read)
    {
        try {

            $this->conn->autocommit(false);
            $date = date("Y-m-d h:i:s");
            $is_active = 1;
            $response = array();
            if ($stmt = $this->conn->prepare("INSERT INTO client_notification(client_id, notification_message, is_read, created_date) values(?, ?, ?, ?)")) {
                $stmt->bind_param("isis", $client_id, $notification_message, $is_read, $date);
                $result = $stmt->execute();

                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    $response["message"] = SAVE_NOTIFICATION_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = SAVE_NOTIFICATION_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getUnReadNotifications($client_id)
    {
        try {
            //$is_active = 1;
            $is_read = 0;
            $sql = "SELECT notification_id,client_id, notification_message, is_read, created_date FROM client_notification WHERE client_id = ? AND is_read = ? ";
            $response["notificationDetails"] = array();

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ii", $client_id, $is_read);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($notification_id, $client_id, $notification_message, $is_read, $created_date);
                    while ($result = $stmt->fetch()) {

                        $tmp = array();
                        $tmp["notification_id"] = $notification_id;
                        $tmp["client_id"] = $client_id;
                        $tmp["notification_message"] = $notification_message;
                        $tmp["is_read"] = $is_read;
                        $tmp["created_date"] = $created_date;
                        $response["notificationDetails"][] = $tmp;
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {

                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function markAsRead($notification_id, $client_id, $is_read)
    {
        try {
            $response = array();
            $this->conn->autocommit(false);
            if ($stmt = $this->conn->prepare("UPDATE client_notification set is_read = ? WHERE notification_id = ? AND client_id = ?")) {
                $stmt->bind_param("iii", $is_read, $notification_id, $client_id);
                $stmt->execute();
                $this->conn->commit();
                $num_affected_rows = $stmt->affected_rows;
                $stmt->close();
                if ($num_affected_rows > 0) {
                    $response["error"] = false;
                    $response["message"] = NOTIFICATION_MARKED_AS_READ;
                } else {
                    $response["error"] = true;
                    $response["message"] = ALREADY_READ_OR_WRONG_RECORD;
                }

            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function updateSecurityQuestion($client_id, $security_question, $security_answer)
    {
        try {
            $response = array();
            $this->conn->autocommit(false);
            if ($stmt = $this->conn->prepare("UPDATE client_master_detail set security_question = ?, security_answer = ? WHERE client_master_id = ?")) {
                $stmt->bind_param("ssi", $security_question, $security_answer, $client_id);
                $stmt->execute();
                $this->conn->commit();
                $num_affected_rows = $stmt->affected_rows;
                $stmt->close();
                if ($num_affected_rows > 0) {
                    $response["error"] = false;
                    $response["message"] = UPDATE_SECURITY_QUESTION_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = UPDATE_SECURITY_QUESTION_FAILURE;
                }

            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getAllCompanies($searchItems)
    {
        try {
            $totResultArr = array();
            $sql = "SELECT client_master_id, company_name
					FROM client_master_detail";
            if (sizeof($searchItems) > 0) {
                $sql .= ' where ';

                foreach ($searchItems as $key => $value) {
                    switch ($key) {
                        case 'is_active':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = "cm.is_active = ? ";
                            break;
                    }
                }
                $sql .= implode(' AND ', $query);
            }
            $sql .= " ORDER BY name asc";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param(implode("", $a_param_type), implode(",", $a_bind_params));
                if ($stmt->execute()) {
                    $stmt->store_result();
                    $stmt->bind_result($client_master_id, $company_name);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["client_master_id"] = $client_master_id;
                        $tmp["company_name"] = $company_name;
                        $totResultArr[] = $tmp;
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                    $response["result"] = $totResultArr;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }

            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }

    public function getAllMappedClients($user_id)
    {
        try {
            $is_active = 1;
            $sql1 = "SELECT cum.client_master_id
                        FROM client_user_mapping as cum
                        JOIN client_master_detail as cd ON cd.client_master_id = cum.client_master_id
                        JOIN client_master as cm ON cm.client_id = cum.client_id
                        WHERE  cm.is_active = ? and cum.client_id=? group by cum.client_master_id";

            if ($stmt = $this->conn->prepare($sql1)) {
                $stmt->bind_param("ii", $is_active, $user_id);
                $stmt->execute();
                $stmt->bind_result($client_master_id);
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $response["clientDetails"] = array();
                    while ($result = $stmt->fetch()) {
                        $sql = "SELECT client_master_id, client_photo, company_name, company_owner, is_active,
                                            report_start_date, report_end_date, default_currency, pdf_report_status
                                    FROM client_master_detail cd where cd.client_master_id = ? ";
                        $sql .= " ORDER BY company_name asc";

                        if ($client_stmt = $this->conn->prepare($sql)) {
                            $client_stmt->bind_param('i', $client_master_id);
                            $client_stmt->execute();
                            $client_stmt->store_result();
                            if ($client_stmt->num_rows > 0) {
                                $client_stmt->bind_result($client_master_id, $client_photo, $company_name, $company_owner, $is_active, $report_start_date, $report_end_date, $default_currency, $weekly_monthly_report);
                                while ($result = $client_stmt->fetch()) {
                                    $tmp["client_master_id"] = $client_master_id;
                                    $tmp["client_photo"] = $client_photo;
                                    $tmp["company_name"] = $company_name;
                                    $tmp["company_owner"] = $company_owner;
                                    $tmp["is_active"] = $is_active;
                                    $tmp["report_start_date"] = $report_start_date;
                                    $tmp["report_end_date"] = $report_end_date;
                                    $tmp["default_currency"] = $default_currency;
                                    $tmp["weekly_monthly_report"] = $weekly_monthly_report;
                                    $response["clientDetails"][] = $tmp;
                                }
                                $response["error"] = false;
                                $response["message"] = RECORD_FOUND;
                            } else {
                                $response["error"] = true;
                                $response["message"] = NO_RECORD_FOUND;
                            }
                        } else {
                            $response["error"] = true;
                            $response["message"] = QUERY_EXCEPTION;
                        }
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAllGroups()
    {
        try {
            $is_group = 1;
            $sql = "SELECT client_master_id, company_name, company_logo, report_start_date,default_currency
                    FROM client_master_detail WHERE is_group = $is_group and is_active = 1 and is_group_deleted = 0";

            $response["groupDetails"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($client_master_id, $company_name, $company_logo, $report_start_date, $default_currency);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["group_id"] = $client_master_id;
                        $tmp["group_name"] = $company_name;
                        $tmp["group_logo"] = $company_logo;
                        $tmp["report_start_date"] = $report_start_date;
                        $tmp["default_currency"] = $default_currency;
                        $response["groupDetails"][] = $tmp;
                    }
                    // $response["error"] = false;
                    // $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAllClients($searchItems)
    {
        try {
            $sql = "SELECT client_master_id, client_photo, company_name, company_owner, is_active,
						    report_start_date, report_end_date, default_currency, pdf_report_status, audit_activity_report, mrr_graph
                    FROM client_master_detail cd";

            if (sizeof($searchItems) > 0) {
                $sql .= " WHERE ";
                foreach ($searchItems as $key => $value) {
                    switch ($key) {
                        case 'is_active':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = "cd.is_active = ? ";
                            break;
                        case 'client_master_id':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = "cd.client_master_id = ? ";
                            break;
                        case 'pdf_report_status':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = " cd.pdf_report_status = ? ";
                            break;
                        case 'is_group':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = "cd.is_group = ? ";
                            break;
                    }
                }
                $sql .= implode(' AND ', $query);
            }

            $sql .= " ORDER BY company_name asc";
            $param_type = '';
            $n = count($a_param_type);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $a_param_type[$i];
            }
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                $a_params[] = &$a_bind_params[$i];
            }

            $response["clientDetails"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                if ((sizeof($searchItems) > 0) && (sizeof($a_params) > 0)) {
                    call_user_func_array(array($stmt, 'bind_param'), $a_params);
                }

                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($client_master_id, $client_photo, $company_name, $company_owner, $is_active, $report_start_date, $report_end_date, $default_currency, $weekly_monthly_report, $audit_activity_report , $mrr_graph);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["client_master_id"] = $client_master_id;
                        $tmp["client_photo"] = $client_photo;
                        $tmp["company_name"] = $company_name;
                        $tmp["company_owner"] = $company_owner;
                        $tmp["is_active"] = $is_active;
                        $tmp["report_start_date"] = $report_start_date;
                        $tmp["report_end_date"] = $report_end_date;
                        $tmp["default_currency"] = $default_currency;
                        $tmp["weekly_monthly_report"] = $weekly_monthly_report;
                        $tmp["audit_activity_report"] = $audit_activity_report;
                        $tmp["mrr_graph"] = $mrr_graph;
                        $response["clientDetails"][] = $tmp;
                    }
                    $response_group = $this->getAllGroups();
                    $response['groupDetails'] = $response_group['groupDetails'];
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function updateClientByAdmin($client_master_id, $report_start_date, $report_end_date, $default_currency, $company_name, $weekly_monthly_report,  $audit_activity_report, $mrr_graph)
    {
        try {
            $this->conn->autocommit(false);
            $date = date("Y-m-d h:i:s");
            $is_active = 1;
            $response = array();
            if ($stmt = $this->conn->prepare("UPDATE client_master_detail SET company_name = ?, last_updated_date = ?, report_start_date = ?, report_end_date = ?, default_currency = ?, pdf_report_status = ?, audit_activity_report = ?, mrr_graph = ? WHERE client_master_id = ?")) {

                $stmt->bind_param("sssssiiii", $company_name, $date, $report_start_date, $report_end_date, $default_currency, $weekly_monthly_report, $audit_activity_report, $mrr_graph, $client_master_id);
                $result = $stmt->execute();
                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    $response["message"] = COMPANY_UPDATED_SUCCESSFULLY;

                    // $superAdminID = $this->getSuperAdminID($client_master_id);
                    // $childResult = $this->updateClientUserByAdmin($superAdminID['client_id'], $name, $mobile, $email, null, null, null, false);
                    // if ($childResult["error"] == false) {
                    //         $this->conn->commit();
                    //         $response["error"] = false;
                    //         $response["message"] = CLIENT_UPDATED_SUCCESSFULLY;
                    //     } else {
                    //         $response["error"] = true;
                    //         $response["message"] = CLIENT_UPDATE_FAILED;
                    //     }
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }

            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function updateClientUserByAdmin($client_id, $name, $mobile, $email, $is_admin, $client_master_id, $selectedClients, $deletedClients, $selectedGroups, $groupsToRemove)
    {
        try {
            $response = array();
            $this->conn->autocommit(false);

            $utility = new utility();
            $res_is_group = $utility->getFieldByID('client_master_detail', 'client_master_id', $client_master_id, 'is_group');
            if ($res_is_group['error'] == false) {
                $is_group = $res_is_group['field_key'];
            }

            if (sizeof($deletedClients) > 0 || sizeof($selectedClients) > 0) {
                $this->hardDeleteClientMapping($client_id, $deletedClients);
                $this->storeByselectedClient($client_id, $selectedClients);

                if($is_group == 0) {
                    $client_user_mapping = 'UPDATE client_user_mapping set is_default = ? where client_id = ? AND client_master_id = ?';
                    if ($stmt = $this->conn->prepare($client_user_mapping)) {
                        $is_default = 1;
                        $stmt->bind_param('iii', $is_default, $client_id, $client_master_id);
                        $stmt->execute();
                        $stmt->close();
                        $response['error'] = false;
                        $response['message'] = CLIENT;
                    }

                    $set_default_value = 'UPDATE client_user_mapping set is_default = ? where client_id = ? AND client_master_id != ?';
                    if ($stmte = $this->conn->prepare($set_default_value)) {
                        $default_value = 0;
                        $stmte->bind_param('iii', $default_value, $client_id, $client_master_id);
                        $stmte->execute();
                        $stmte->close();
                        $response['error'] = false;
                        $response['message'] = CLIENT;
                    }
                }
                else {
                    $client_user_mapping = 'UPDATE client_user_mapping set is_default = ? where client_id = ?';
                    if ($stmt = $this->conn->prepare($client_user_mapping)) {
                        $is_default = 0;
                        $stmt->bind_param('ii', $is_default, $client_id);
                        $stmt->execute();
                        $stmt->close();
                        $response['error'] = false;
                        $response['message'] = CLIENT;
                    }
                }
            }

            $res = $this->updateClientGroups($client_id, $selectedGroups, $groupsToRemove, $is_group, $client_master_id);

            $client_master = 'UPDATE client_master set name = ?, mobile = ?, email = ?, is_admin = ? where client_id = ?';
            if ($stmtChild = $this->conn->prepare($client_master)) {
                $stmtChild->bind_param('sssii', $name, $mobile, $email, $is_admin, $client_id);
                $childResult = $stmtChild->execute();
                $stmtChild->close();
                if ($childResult) {
                    $this->conn->commit();
                    $response['error'] = false;
                    $response['message'] = CLIENT_USER_UPDATED_SUCCESSFULLY;
                } else {
                    $response['error'] = true;
                    $response['message'] = CLIENT_USER_UPDATE_FAILED;
                }
            } else {
                $response['error'] = true;
                $response['message'] = QUERY_EXCEPTION;
            }

            $utility = null;
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function updateClientGroups($client_id, $selectedGroups, $groupsToRemove, $is_group, $group_id)
    {
        try {
            if (sizeof($groupsToRemove) > 0 || sizeof($selectedGroups) > 0) {
                $this->hardDeleteClientGroup($client_id, $groupsToRemove);
                $this->storeByselectedGroup($client_id, $selectedGroups);
                $response['error'] = false;
                $response['message'] = CLIENT;

                if($is_group == 1) {
                    $client_group_mapping = 'UPDATE client_group_mapping set is_default = ? where client_id = ? AND group_id = ?';
                    if ($stmt = $this->conn->prepare($client_group_mapping)) {
                        $is_default = 1;
                        $stmt->bind_param('iii', $is_default, $client_id, $group_id);
                        $stmt->execute();
                        $stmt->close();
                        $response['error'] = false;
                        $response['message'] = CLIENT;
                    }
                }
            }
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function updateClientUserApplicationByAdmin($client_id, $client_master_id, $application_ids, $category_sub_ids, $report_ids, $needReportUpdate)
    {
        try {
            $response = array();
            $this->conn->autocommit(false);
            if ($needReportUpdate == true) {
                $objAppln = new clientApplicationMap();
                $setApplication = array();
                if (sizeof($application_ids) > 0) {
                    if (!in_array(2, $application_ids)) {
                        $application_ids[] = 2;
                    }
                }

                $setApplication = $objAppln->setApplicationReports($client_id, $client_master_id, $application_ids, false, $report_ids, null);
                if ($setApplication["error"] == true) {
                    return $setApplication;
                } else {
                    $response["error"] = false;
                    $response["message"] = CLIENT_USER_UPDATED_SUCCESSFULLY;
                }
                $catResponse = array();
                $db = new dashboardReport();
                $catResponse = $db->setSubCategoriesForClientUsers($client_id, $category_sub_ids, $client_master_id);
                $response["error"] = false;
                $response["message"] = CLIENT_USER_UPDATED_SUCCESSFULLY;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function hardDeleteClientMapping($client_id, $deletedClients)
    {
        try {
            foreach ($deletedClients as $value) {
                $deletedClientsID = $value['id'];
                //delete mapping_id by deletedids from client_user_mapping
                $sql = 'DELETE FROM `client_user_mapping` WHERE `client_id` = ? and `client_master_id` = ?';
                if ($stmt = $this->conn->prepare($sql)) {

                    $stmt->bind_param('ii', $client_id, $deletedClientsID);
                    $result = $stmt->execute();
                    $this->conn->commit();
                    $stmt->close();
                    if ($result) {
                        $this->conn->commit();
                        $response['error'] = false;
                        $response['error'] = $result;
                        //get apllication map id
                        $applicationDetails = 'SELECT client_application_map_id FROM `client_application_map` WHERE `client_id` = ? and `client_master_id` = ?';
                        if ($appl_stmt = $this->conn->prepare($applicationDetails)) {
                            $appl_stmt->bind_param('ii', $client_id, $deletedClientsID);
                            $appl_stmt->execute();
                            $appl_stmt->bind_result($client_application_map_id);
                            $appl_stmt->store_result();

                            if ($appl_stmt->num_rows > 0) {
                                $appl_stmt->bind_result($client_application_map_id);
                                while ($result1 = $appl_stmt->fetch()) {
                                    $client_application_map_id = $client_application_map_id;
                                    //delete report_id by use application map id from report_client_application_map
                                    $deleteReport = 'DELETE  FROM `report_client_application_map` WHERE `client_application_map_id` = ?';
                                    if ($delete_report_stmt = $this->conn->prepare($deleteReport)) {
                                        $delete_report_stmt->bind_param('i', $client_application_map_id);
                                        $delete_result = $delete_report_stmt->execute();
                                        $this->conn->commit();
                                        $delete_report_stmt->close();
                                        if ($delete_result) {
                                            $response['error'] = false;
                                            $response['error'] = $result;
                                        }
                                    }
                                    //delete client_application_id by use application map id from client_application_map
                                    $deleteAppl = 'DELETE FROM `client_application_map` WHERE `client_application_map_id` = ?';
                                    if ($delete_apll_stmt = $this->conn->prepare($deleteAppl)) {
                                        $delete_apll_stmt->bind_param('i', $client_application_map_id);
                                        $result = $delete_apll_stmt->execute();
                                        $this->conn->commit();
                                        $delete_apll_stmt->close();
                                        if ($result) {
                                            $this->conn->commit();
                                            $response['error'] = false;
                                            $response['error'] = $result;
                                        }
                                    }

                                }

                            }
                        }

                    } else {
                        $response['error'] = true;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = QUERY_EXCEPTION;
                }
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function hardDeleteClientGroup($client_id, $groupsToRemove)
    {
        try {
            foreach ($groupsToRemove as $value) {
                $deletedGroupssID = $value['id'];
                //delete mapping_id by deletedids from client_user_mapping
                $sql = 'DELETE FROM `client_group_mapping` WHERE `client_id` = ? and `group_id` = ?';
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param('ii', $client_id, $deletedGroupssID);
                    $result = $stmt->execute();
                    $this->conn->commit();
                    $stmt->close();
                    if ($result) {
                        $this->conn->commit();
                        $response['error'] = false;
                        $response['error'] = $result;
                    } else {
                        $response['error'] = true;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = QUERY_EXCEPTION;
                }
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function storeByselectedClient($client_id, $selectedClients)
    {
        try {
            foreach ($selectedClients as $value) {
                $sql = 'SELECT * FROM `client_user_mapping` WHERE `client_id` = ? and `client_master_id` = ?';
                if ($stmt = $this->conn->prepare($sql)) {
                    $selectedClientsID = $value['id'];
                    $stmt->bind_param('ii', $client_id, $selectedClientsID);
                    if ($stmt->execute()) {
                        $stmt->store_result();
                        $stmt->bind_result($mapping_id);
                        $stmt->fetch();
                        $num_rows = $stmt->num_rows;
                        $stmt->close();
                        if ($num_rows == 0) {
                            $sql1 = 'INSERT INTO client_user_mapping(client_id,client_master_id,is_default) values(?,?,?)';
                            if ($stmt = $this->conn->prepare($sql1)) {
                                $default_value = 0;
                                $stmt->bind_param('iii', $client_id, $selectedClientsID, $default_value);
                                $result = $stmt->execute();
                                $stmt->close();
                                if ($result) {
                                    $this->conn->commit();
                                    $application_ids = [1, 2];
                                    $report_ids = [1, 3, 4];
                                    $objAppln = new clientApplicationMap();
                                    $id[0]['id'] = $selectedClientsID;
                                    $setApplication = $objAppln->setApplicationReports($client_id, $id, $application_ids, true, $report_ids, null);
                                    $response['error'] = false;
                                    $response['error'] = $result;
                                } else {
                                    $response['error'] = true;
                                }
                            } else {
                                $response['error'] = true;
                                $response['message'] = QUERY_EXCEPTION;
                            }
                        }
                    } else {
                        $response['error'] = true;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = QUERY_EXCEPTION;
                }
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function storeByselectedGroup($client_id, $selectedGroups)
    {
        try {
            foreach ($selectedGroups as $value) {
                $sql = 'SELECT * FROM `client_group_mapping` WHERE `client_id` = ? and `group_id` = ?';
                if ($stmt = $this->conn->prepare($sql)) {
                    $selectedGroupsID = $value['id'];
                    $stmt->bind_param('ii', $client_id, $selectedGroupsID);
                    if ($stmt->execute()) {
                        $stmt->store_result();
                        $stmt->bind_result($group_mapping_id);
                        $stmt->fetch();
                        $num_rows = $stmt->num_rows;
                        $stmt->close();
                        if ($num_rows == 0) {
                            $sql1 = 'INSERT INTO client_group_mapping(client_id,group_id,is_default) values(?,?,?)';
                            if ($stmt = $this->conn->prepare($sql1)) {
                                $default_value = 0;
                                $stmt->bind_param('iii', $client_id, $selectedGroupsID, $default_value);
                                $result = $stmt->execute();
                                $stmt->close();
                                if ($result) {
                                    $this->conn->commit();
                                    $response['error'] = false;
                                    $response['error'] = $result;
                                } else {
                                    $response['error'] = true;
                                }
                            } else {
                                $response['error'] = true;
                                $response['message'] = QUERY_EXCEPTION;
                            }
                        } else {
                            $sql1 = 'UPDATE client_group_mapping SET group_id =? ,is_default = ? WHERE client_id = ?';
                            if ($stmt = $this->conn->prepare($sql1)) {
                                $default_value = 0;
                                $stmt->bind_param('iii', $selectedGroupsID, $default_value, $client_id);
                                $result = $stmt->execute();
                                $stmt->close();
                                if ($result) {
                                    $this->conn->commit();
                                    $response['error'] = false;
                                    $response['error'] = $result;
                                } else {
                                    $response['error'] = true;
                                }
                            } else {
                                $response['error'] = true;
                                $response['message'] = QUERY_EXCEPTION;
                            }
                        }
                    } else {
                        $response['error'] = true;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = QUERY_EXCEPTION;
                }
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function accessClientUserByMaster($client_master_id, $is_active)
    {
        try {
            $response = array();
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            if ($stmt = $this->conn->prepare("UPDATE client_master_detail set is_active = ?, last_updated_date = ? WHERE client_master_id = ?")) {
                $stmt->bind_param("isi", $is_active, $date, $client_master_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    if ($is_active == 0) {
                        $response["message"] = CLIENT_DEACTIVATED_SUCCESS;
                    } else {
                        $response["message"] = CLIENT_ACTIVATED_SUCCESS;
                    }
                } else {
                    $response["error"] = true;
                    if ($is_active == 0) {
                        $response["message"] = CLIENT_DEACTIVATED_FAILURE;
                    } else {
                        $response["message"] = CLIENT_ACTIVATED_FAILURE;
                    }
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function accessClientUser($client_id, $is_active)
    {
        try {
            $response = array();
            $date = date("Y-m-d h:i:s");

            $this->conn->autocommit(false);
            if ($stmt = $this->conn->prepare("UPDATE client_master set is_active = ?, last_updated_date = ? WHERE client_id = ?")) {
                $stmt->bind_param("isi", $is_active, $date, $client_id);
                $result = $stmt->execute();
                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    if ($is_active == 0) {
                        $response["message"] = CLIENT_USER_DEACTIVATED_SUCCESS;
                    } else {
                        $response["message"] = CLIENT_USER_ACTIVATED_SUCCESS;
                    }
                } else {
                    $response["error"] = true;
                    if ($is_active == 0) {
                        $response["message"] = CLIENT_USER_DEACTIVATED_FAILURE;
                    } else {
                        $response["message"] = CLIENT_USER_ACTIVATED_FAILURE;
                    }
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function accessClient($client_master_id, $is_active)
    {
        try {
            $response = array();
            $response = $this->accessClientUserByMaster($client_master_id, $is_active);
            if ($response["error"] == false) {
                $date = date("Y-m-d h:i:s");
                $this->conn->autocommit(false);
                if ($stmt = $this->conn->prepare("UPDATE client_master_detail set is_active = ?, last_updated_date = ? WHERE client_master_id = ?")) {
                    $stmt->bind_param("isi", $is_active, $date, $client_master_id);
                    $result = $stmt->execute();
                    $this->conn->commit();
                    $stmt->close();
                    if ($result) {
                        $response["error"] = false;
                        if ($is_active == 0) {
                            $response["message"] = CLIENT_DEACTIVATED_SUCCESS;
                        } else {
                            $response["message"] = CLIENT_ACTIVATED_SUCCESS;
                        }
                    } else {
                        $response["error"] = true;
                        if ($is_active == 0) {
                            $response["message"] = CLIENT_DEACTIVATED_FAILURE;
                        } else {
                            $response["message"] = CLIENT_ACTIVATED_FAILURE;
                        }
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function bizfileUpload($client_master_id, $files)
    {
        try
        {
            $name = $files['bizfile_pdf']['name'];
            $size = $files['bizfile_pdf']['size'];
            $tmp = $files['bizfile_pdf']['tmp_name'];
            $ext = $this->getExtension($name);
            $valid_formats = array("pdf", "PDF");
            if (strlen($name) > 0) {
                if (in_array($ext, $valid_formats)) {
                    $uploadFileName = $client_master_id . '.pdf';
                    $biz_pdf_file_path = BIZ_FILE_PATH . "$uploadFileName";

                    //if (file_exists($biz_pdf_file_path)) {
                    //unlink($biz_pdf_file_path);
                    //}

                    if (move_uploaded_file($tmp, $biz_pdf_file_path)) {

                        /**Table insert/update start **/

                        $res = $this->getFieldByID('client_compliance_bizfile', 'client_master_id',
                            $client_master_id, 'client_master_id');

                        if ($res["error"] == false) {

                            $sql = "UPDATE client_compliance_bizfile SET file_path_name = ? WHERE client_master_id = ?";
                            if ($stmt = $this->conn->prepare($sql)) {

                                $stmt->bind_param("si", $uploadFileName, $client_master_id);
                                $result = $stmt->execute();
                                $stmt->close();
                                if ($result) {
                                    $response["error"] = false;
                                    $response["message"] = BIZ_UPLOAD_SUCCESS;
                                } else {
                                    $response["error"] = true;
                                    $response["message"] = BIZ_UPLOAD_FAILURE;
                                }
                            } else {
                                $response["error"] = true;
                                $response["message"] = QUERY_EXCEPTION;
                            }
                        } else {
                            $sql = "INSERT INTO client_compliance_bizfile(client_master_id,file_path_name) values(?,?)";
                            if ($stmt = $this->conn->prepare($sql)) {

                                $stmt->bind_param("is", $client_master_id, $uploadFileName);
                                $result = $stmt->execute();
                                $stmt->close();

                                $response["error"] = $result;
                                if ($result) {
                                    $this->conn->commit();
                                    $response["error"] = false;
                                    $response["message"] = BIZ_UPLOAD_SUCCESS;

                                } else {
                                    $response["error"] = true;
                                    $response["message"] = BIZ_UPLOAD_FAILURE;
                                }

                            } else {
                                $response["error"] = true;
                                $response["message"] = QUERY_EXCEPTION;
                            }

                        }
                        /**Table insert/update end  **/
                    } else {
                        $response["error"] = true;
                        $response["message"] = UPLOAD_FAIL;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = BIZ_FILE_FORMAT;
                }

            } else {
                $response["error"] = true;
                $response["message"] = NO_FILE_SELECTED;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function bizfileClient($client_master_id)
    {
        $res = $this->getFieldByID('client_compliance_bizfile', 'client_master_id', $client_master_id, 'file_path_name');
        return $res;
    }

    public function updateWeeklyreport($client_master_id, $report_run_date, $report_run_time)
    {
        $response = array();
        try
        {
            $sql = "SELECT client_master_id FROM report_pdf_to_client WHERE client_master_id = ? AND is_deleted = ?";
            $is_deleted = 0;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ii", $client_master_id, $is_deleted);
                $stmt->execute();
                $stmt->bind_result($client_master_id);
                $stmt->store_result();
                if ($stmt->num_rows > 0) {

                    if ($stmt = $this->conn->prepare("UPDATE report_pdf_to_client set pdf_run_time  = ? , every_week_on = ?  WHERE client_master_id = ? ")) {
                        $stmt->bind_param("sii", $report_run_time, $report_run_date, $client_master_id);
                        $result = $stmt->execute();
                        $this->conn->commit();
                        $stmt->close();
                        if ($result) {
                            $response["error"] = false;
                            $response["message"] = WEEKLY_REPORT_UPDATE_SUCCESS;
                        } else {
                            $response["error"] = true;
                            $response["message"] = WEEKLY_REPORT_UPDATE_FAIL;
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = QUERY_EXCEPTION;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = WEEKLY_REPORT_NO_RECORD;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;

            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function saveWeeklyreport($client_master_id, $report_run_date, $report_run_time)
    {
        $response = array();
        try
        {
            $is_deleted = 0;
            $sql = "SELECT client_master_id FROM report_pdf_to_client WHERE client_master_id = ? and is_deleted = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ii", $client_master_id, $is_deleted);
                $stmt->execute();
                $stmt->bind_result($client_master_id);
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $response["error"] = true;
                    $response["message"] = WEEKLY_REPORT_EXISTS;
                } else {
                    $sql1 = "INSERT INTO report_pdf_to_client (every_week_on, pdf_run_time, client_master_id) values(?,?,?)";
                    if ($stmt1 = $this->conn->prepare($sql1)) {
                        $stmt1->bind_param("isi", $report_run_date, $report_run_time, $client_master_id);
                        $result = $stmt1->execute();
                        $response["error"] = false;
                        $response["message"] = WEEKLY_REPORT_SUCCESS;
                    } else {
                        $response["error"] = true;
                        $response["message"] = QUERY_EXCEPTION;
                    }
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }
    public function listWeeklyreport()
    {
        $response = array();
        try
        {
            $sql = "SELECT  pc.client_master_id,
	                        pc.every_week_on,
	                        pc.pdf_run_time,
	                        pc.last_ran_time,
	                        pc.last_ran_status,
	                        pc.is_deleted ,
	                        cd.company_name
	                FROM report_pdf_to_client pc
						JOIN client_master_detail cd ON pc.client_master_id = cd.client_master_id
	                WHERE pc.is_deleted = ? AND cd.pdf_report_status = ?";
            $is_deleted = 0;
            $pdf_report_status = 1;
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ii", $is_deleted, $pdf_report_status);
                $stmt->execute();
                $stmt->bind_result($client_master_id);
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($client_master_id, $every_week_on, $pdf_run_time, $last_ran_time, $last_ran_status, $is_deleted, $company_name);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["client_master_id"] = $client_master_id;
                        $tmp["report_run_date"] = $every_week_on;
                        $tmp["report_run_time"] = $pdf_run_time;
                        $tmp["is_deleted"] = $is_deleted;
                        $tmp["company_name"] = $company_name;
                        $response["clientDetails"][] = $tmp;
                        $response["error"] = false;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = WEEKLY_REPORT_NO_RECORD;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function deleteWeeklyreport($client_master_id)
    {
        $response = array();
        try
        {
            $sql = "SELECT client_master_id FROM report_pdf_to_client WHERE client_master_id = ? ";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("i", $client_master_id);
                $stmt->execute();
                $stmt->bind_result($client_master_id);
                $stmt->store_result();
                $is_deleted = 1;
                if ($stmt->num_rows > 0) {
                    if ($stmt = $this->conn->prepare("UPDATE report_pdf_to_client set is_deleted  = ?  WHERE client_master_id = ? ")) {
                        $stmt->bind_param("ii", $is_deleted, $client_master_id);
                        $result = $stmt->execute();
                        $this->conn->commit();
                        $stmt->close();
                        if ($result) {
                            $response["error"] = false;
                            $response["message"] = WEEKLY_REPORT_DELETE_SUCCESS;
                        } else {
                            $response["error"] = true;
                            $response["message"] = WEEKLY_REPORT_DELETE_FAIL;
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = QUERY_EXCEPTION;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = WEEKLY_REPORT_NO_RECORD;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAllMappedUsers($user_id, $client_master_id)
    {
        $response = array();
        $is_active = 1;
        $user_data = array();
        try {
            $sql2 = "SELECT cum.client_id, cm.name, cum.client_master_id, cd.company_name, cum.is_default
                        FROM client_user_mapping as cum
                        JOIN client_master_detail as cd ON cd.client_master_id = cum.client_master_id
                        JOIN client_master as cm ON cm.client_id = cum.client_id
                        WHERE cum.client_master_id = ? group by cum.client_id, cm.name, cum.client_master_id, cd.company_name, cum.is_default";
            if ($sql2_stmt = $this->conn->prepare($sql2)) {
                $sql2_stmt->bind_param("i", $client_master_id);
                $sql2_stmt->execute();
                $sql2_stmt->bind_result($client_id, $name, $client_master_id, $company_name, $is_default);
                $sql2_stmt->store_result();
                if ($sql2_stmt->num_rows > 0) {
                    $company = array();
                    while ($result = $sql2_stmt->fetch()) {
                        $company[] = array('client_id' => $client_id, 'name' => $name, 'client_master_id' => $client_master_id, 'company_name' => $company_name, 'is_default' => $is_default);
                    }
                    $response["error"] = false;
                    $response["clientDetails"] = $company;
                } else {
                    $response["error"] = false;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAllUsers()
    {
        $response = array();
        $is_active = 1;
        $user_data = array();
        try {

            $sql = "SELECT name, client_id FROM client_master WHERE  is_active = ? ORDER BY name ASC";

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("i", $is_active);
                $stmt->execute();
                $stmt->bind_result($name, $client_id);
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    while ($result = $stmt->fetch()) {
                        $user_data[] = array("client_id" => $client_id, "name" => "$name");
                    }

                    $sql1 = "SELECT cum.client_id, cum.client_master_id, cd.company_name, cum.is_default
                                FROM client_user_mapping as cum
                                JOIN client_master_detail as cd ON cd.client_master_id = cum.client_master_id
                                JOIN client_master as cm ON cm.client_id = cum.client_id
                                WHERE  cm.is_active = ? group by cum.client_id, cum.client_master_id, cd.company_name, cum.is_default";

                    if ($stmt = $this->conn->prepare($sql1)) {
                        $stmt->bind_param("i", $is_active);
                        $stmt->execute();
                        $stmt->bind_result($client_id, $client_master_id, $company_name, $is_default);
                        $stmt->store_result();
                        if ($stmt->num_rows > 0) {
                            $company = array();
                            while ($result = $stmt->fetch()) {
                                $company[] = array("client_id" => $client_id, 'client_master_id' => $client_master_id, 'company_name' => $company_name, 'is_default' => $is_default);
                            }

                            foreach ($user_data as $key => $user) {
                                $data = array();
                                $client_id = $user['client_id'];
                                $data = $this->searchForId($client_id, $company);
                                if (sizeof($data) > 0) {
                                    foreach ($data as $key1 => $value1) {
                                        $user_data[$key]['company'][] = $company[$value1];
                                    }
                                }
                            }
                        } else {
                            $response["error"] = false;
                            $response["message"] = NO_RECORD_FOUND;
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = QUERY_EXCEPTION;
                    }

                    $response["error"] = false;
                    $response["clientDetails"] = $user_data;
                } else {
                    $response["error"] = false;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function searchForId($id, $array)
    {
        $found = array();
        foreach ($array as $key => $val) {
            if ($val['client_id'] === $id) {
                $found[] = $key;
            }
        }
        return $found;
    }

    public function getUsersDetails($client_id)
    {
        $user = array();
        try {
            $is_default = 1;
            $sql = "SELECT cm.client_id, name, email, mobile, cm.is_active, is_admin, ur.role_name,
                            CASE WHEN (cum.client_master_id is NULL) THEN cgm.group_id ELSE cum.client_master_id END AS client_master_id,
                            CASE WHEN (cd.company_name is NULL) THEN cd1.company_name ELSE cd.company_name END AS company_name
                        FROM client_master cm
                        LEFT JOIN client_user_mapping as cum ON cum.client_id = cm.client_id and cum.is_default = 1
                        LEFT JOIN client_master_detail as cd ON cd.client_master_id = cum.client_master_id
                        LEFT JOIN client_group_mapping as cgm ON cgm.client_id = cm.client_id and cgm.is_default = 1
                        LEFT JOIN client_master_detail as cd1 ON cd1.client_master_id = cgm.group_id
                        JOIN user_roles as ur ON ur.user_role_id = cm.is_admin
                        WHERE cm.client_id = ?";

            $sql1 = "SELECT cum.client_id, cum.client_master_id, cd.company_name, cum.is_default
                        FROM client_user_mapping as cum
                        JOIN client_master_detail as cd ON cd.client_master_id = cum.client_master_id
                        where cum.client_id = ? and cd.is_group = 0";

            $sql2 = "SELECT cd.client_master_id, cd.company_name, cd.company_logo, cd.report_start_date, cd.default_currency, cgm.is_default
                            FROM client_group_mapping as cgm
                            JOIN client_master_detail as cd ON cd.client_master_id = cgm.group_id
                            where cgm.client_id = ? and cd.is_group = 1";

            if ($stmte = $this->conn->prepare($sql)) {
                $stmte->bind_param("i", $client_id);
                $stmte->execute();
                $stmte->bind_result($client_id, $name, $email, $mobile, $is_active, $is_admin, $role_name, $client_master_id, $company_name);
                $stmte->store_result();
                $compResult = $stmte->fetch();
                $user = array();
                $user["error"] = false;
                $user["client_id"] = $client_id;
                $user["name"] = $name;
                $user["mobile"] = $mobile;
                $user["email"] = $email;
                $user["is_admin"] = $is_admin;
                $user["role_name"] = $role_name;
                $user["is_active"] = $is_active;
                $user["client_master_id"] = $client_master_id;
                $user["company_name"] = $company_name;
            }

            if ($stmt = $this->conn->prepare($sql1)) {
                $stmt->bind_param("i", $client_id);
                $stmt->execute();
                $stmt->bind_result($client_id, $client_master_id, $company_name, $is_default);
                $stmt->store_result();
                while ($compResult = $stmt->fetch()) {
                    $temp[] = array('client_id' => $client_id, 'client_master_id' => $client_master_id, "company_name" => "$company_name", "is_default" => $is_default);
                }
                $user['clients'] = $temp;
            }
            if ($group_stmt = $this->conn->prepare($sql2)) {
                $group_stmt->bind_param("i", $client_id);
                $group_stmt->execute();
                $group_stmt->bind_result($group_id, $group_name, $group_logo, $report_start_date, $default_currency, $is_default);
                $group_stmt->store_result();
                while ($result = $group_stmt->fetch()) {
                    $group_temp[] = array('group_id' => $group_id, 'group_name' => $group_name, "group_logo" => "$group_logo", "report_start_date" => $report_start_date, "default_currency" => $default_currency, "is_default" => $is_default);
                }
                if (count($group_temp) > 0) {
                    $user['group_details'] = $group_temp;
                } else {
                    $user['group_details'] = [];
                }

            } else {
                $user["error"] = true;
                $user["message"] = QUERY_EXCEPTION . " - " . CLIENT_QUERY;
            }
            return $user;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getApplicationDetails($client_id, $client_master_id)
    {
        $user = array();
        try {
            $aNotifications = $this->getUnReadNotifications($client_id);
            $user["notifications"] = $aNotifications['notificationDetails'];
            $db = new clientApplicationMap();
            $result = $db->getAllClientApplicationWrapper($client_id, $client_master_id, $is_active);
            if ($result["error"] == false) {
                $user["clientApplicationMapDetails"] = $result['clientApplicationMapDetails'];
                $user["application_ids"] = $result['application_ids'];
            } else {
                $user["clientApplicationMapDetails"] = [];
                $user["application_ids"] = [];
            }

            $db2 = new reportClientApplicationMap();
            $searchReport = array();
            $searchReport['client_id'] = $client_id;
            $searchReport["client_master_id"] = $client_master_id;
            $searchReport['is_active'] = 1;
            $result1 = $db2->getAllReportClientApplicationMapWrapper($searchReport);

            if ($result1["error"] == false) {
                $user["reportClientApplicationMapDetails"] = $result1['reportClientApplicationMapDetails'];
                $user["report_ids"] = $result1['report_ids'];
            } else {
                $user["reportClientApplicationMapDetails"] = [];
                $user["report_ids"] = [];
            }

            $db1 = new dashboardReport();
            $searchItems['client_id'] = $client_id;
            $searchItems["client_master_id"] = $client_master_id;
            $searchItems['is_active'] = 1;
            $searchItems["groupby"] = 1;

            $rResponse = $db1->getMappedTrackingCategories($searchItems);
            $user["xeroCategories"] = ($rResponse["error"] == false) ? $rResponse['trackingCategories'] : [];

            $utility = new utility();
            $res_is_admin = $utility->getFieldByID('client_master', 'client_id', $client_id, 'is_admin');
            if ($res_is_admin['error'] == false) {
                $is_admin = $res_is_admin['field_key'];
            }

            if ($is_admin != 1) {
                $superAdminID = $this->getSuperAdminID($client_master_id);
                $master_client_id = $superAdminID['client_id'];
            } else {
                $master_client_id = $client_id;
            }

            $user["masterXeroCategories"] = array();
            $searchMaster = array();
            $searchMaster['client_id'] = $master_client_id;
            $searchMaster["client_master_id"] = $client_master_id;
            $searchMaster['is_active'] = 1;
            $searchMaster["groupby"] = 1;
            $rResponse = $db1->getMasterTrackingCategories($searchMaster);
            $user["masterXeroCategories"] = ($rResponse["error"] == false) ? $rResponse['trackingCategories'] : [];

            $db = null;
            $db1 = null;
            $db2 = null;
            $utility = null;
            return $user;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAllClientUsers($searchItems)
    {
        try {
            $sql = "SELECT cm.client_id, name, email, mobile, cm.is_active, is_admin, ur.role_name, cum.client_master_id, cd.company_name,
                            cd.client_photo, report_start_date, report_end_date, default_currency, cd.pdf_report_status
                    FROM client_master cm
                    JOIN client_user_mapping as cum ON cum.client_id = cm.client_id
                    JOIN client_master_detail as cd ON cd.client_master_id = cum.client_master_id
                    JOIN user_roles as ur ON ur.user_role_id = cm.is_admin";

            if (sizeof($searchItems) > 0) {
                $sql .= ' where ';

                foreach ($searchItems as $key => $value) {
                    switch ($key) {
                        case 'client_id':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = "cm.client_id = ? ";
                            break;
                        case 'is_active':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = "cm.is_active = ? ";
                            break;
                        case 'client_master_id':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = "cd.client_master_id = ? ";
                            break;
                        case 'is_admin':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = " cm.is_admin = ? ";
                            break;
                        case 'pdf_report_status':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = " cd.pdf_report_status = ? ";
                            break;
                    }
                }
                $sql .= implode(' AND ', $query);
            }

            $sql .= " ORDER BY name asc";
            $param_type = '';
            $n = count($a_param_type);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $a_param_type[$i];
            }
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                $a_params[] = &$a_bind_params[$i];
            }

            $response["clientDetails"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                if ((sizeof($searchItems) > 0) && (sizeof($a_params) > 0)) {
                    call_user_func_array(array($stmt, 'bind_param'), $a_params);
                }

                $stmt->execute();
                $stmt->store_result();

                if ($stmt->num_rows > 0) {
                    $db = new clientApplicationMap();
                    $db1 = new dashboardReport();
                    $db2 = new reportClientApplicationMap();
                    $stmt->bind_result($client_id, $name, $email, $mobile, $is_active, $is_admin, $role_name, $client_master_id, $company_name, $client_photo, $report_start_date, $report_end_date, $default_currency, $weekly_monthly_report);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["client_id"] = $client_id;
                        $tmp["name"] = $name;
                        $tmp["email"] = $email;
                        $tmp["mobile"] = $mobile;
                        $tmp["is_active"] = $is_active;
                        $tmp["is_admin"] = $is_admin;
                        $tmp["role_name"] = $role_name;
                        $tmp["client_master_id"] = $client_master_id;
                        $tmp["company_name"] = $company_name;
                        $tmp["client_photo"] = $client_photo;
                        $tmp["report_start_date"] = $report_start_date;
                        $tmp["report_end_date"] = $report_end_date;
                        $tmp["default_currency"] = $default_currency;
                        $tmp["weekly_monthly_report"] = $weekly_monthly_report;

                        $result = $db->getAllClientApplicationWrapper($client_id, $client_master_id, 1);
                        if ($result["error"] == false) {
                            $tmp["clientApplicationMapDetails"] = $result['clientApplicationMapDetails'];
                            $tmp["application_ids"] = $result['application_ids'];
                        } else {
                            $tmp["clientApplicationMapDetails"] = [];
                            $tmp["application_ids"] = [];
                        }

                        $searchReport = array();
                        $searchReport['client_id'] = $client_id;
                        $searchReport["client_master_id"] = $client_master_id;
                        $searchReport['is_active'] = 1;
                        // if (sizeof($result2["report_ids"]) > 0) {
                        //     $searchReport["master_reports"] = implode(',', $result2["report_ids"]);
                        // }
                        $result1 = $db2->getAllReportClientApplicationMapWrapper($searchReport);
                        if ($result1["error"] == false) {
                            $tmp["reportClientApplicationMapDetails"] = $result1['reportClientApplicationMapDetails'];
                            $tmp["report_ids"] = $result1['report_ids'];
                        } else {
                            $tmp["reportClientApplicationMapDetails"] = [];
                            $tmp["report_ids"] = [];
                        }

                        $searchItems['client_id'] = $client_id;
                        $searchItems["client_master_id"] = $client_master_id;
                        $searchItems['is_active'] = 1;
                        $searchItems["groupby"] = 1;
                        $rResponse = $db1->getMappedTrackingCategories($searchItems);
                        $tmp["xeroCategories"] = ($rResponse["error"] == false) ? $rResponse['trackingCategories'] : [];

                        // $result = $db->getAllClientApplicationWrapper($client_id, $client_master_id, 1);
                        // $tmp["clientApplicationMapDetails"] = ($result["error"] == false) ? $result['clientApplicationMapDetails'] : [];
                        // $tmp["application_ids"] = $result['application_ids'];

                        // $searchReport = array();
                        // $searchReport['client_id'] = $client_id;
                        // $searchReport['client_master_id'] = $client_master_id;
                        // $searchReport['is_active'] = 1;
                        // if (sizeof($result2["report_ids"]) > 0) {
                        //     $searchReport["master_reports"] = implode(',', $result2["report_ids"]);
                        // }
                        // $result1 = $db2->getAllReportClientApplicationMapWrapper($searchReport);
                        // $tmp["reportClientApplicationMapDetails"] = ($result1["error"] == false) ? $result1['reportClientApplicationMapDetails'] : [];
                        // $tmp["report_ids"] = ($result1["error"] == false) ? $result1['report_ids'] : [];

                        // $searchParams = array();
                        // $super_client_id = array();
                        // $super_client_id = $this->getSuperAdminID($searchItems['client_master_id']);
                        // $searchParams['client_id'] = $super_client_id ['client_id'];
                        // $searchParams["client_master_id"] = $client_master_id;
                        // $searchParams['is_active'] = 1;
                        // $searchParams["groupby"] = 1;
                        // $rResponse = $db1->getAllTrackingCategories($searchParams);
                        // $tmp["xeroCategories"] = ($rResponse["error"] == false) ? $rResponse['trackingCategories'] : [];
                        $response["clientDetails"][] = $tmp;
                    }

                    // $application_details = $db->getAllApplicationDetails($client_id, $client_master_id, 1);
                    // $response["masterApplicationDetails"] = ($application_details["error"] == false) ? $application_details['clientApplicationMapDetails'] : [];

                    // $searchReport = array();
                    // $searchReport['client_id'] = $client_id;
                    // $searchReport['client_master_id'] = $client_master_id;
                    // $searchReport['is_active'] = 1;
                    // $report_details = $db2->getAllReportApplicationDetails($searchReport);
                    // $response["masterReportDetails"] = ($report_details["error"] == false) ? $report_details['reportClientApplicationMapDetails'] : [];

                    $response["masterXeroCategories"] = array();
                    $super_client_id = array();
                    $super_client_id = $this->getSuperAdminID($searchItems['client_master_id']);
                    $searchMaster = array();
                    $searchMaster['client_id'] = $super_client_id['client_id'];
                    $searchMaster["client_master_id"] = $searchItems["client_master_id"];
                    $searchMaster['is_active'] = 1;
                    $searchMaster["groupby"] = 1;
                    $rResponse = $db1->getMasterTrackingCategories($searchMaster);
                    $response["masterXeroCategories"] = ($rResponse["error"] == false) ? $rResponse['trackingCategories'] : [];

                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function checkIfClientCompanyExist($companyname, $is_active)
    {
        try {
            $sql = "SELECT company_name, is_active
                    FROM client_master_detail
                    WHERE company_name = '$companyname' AND is_active = '$is_active' ";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param('ii', $companyname, $is_active);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $response['error'] = false;
                    $response['message'] = RECORD_FOUND;
                    $response['is_active'] = $is_active;
                    $response['companyname'] = $companyname;

                } else {
                    $response['error'] = true;
                    $response['message'] = NO_RECORD_FOUND;
                }
                $stmt->close();
            } else {
                $response['error'] = true;
                $response['message'] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function storeUserMappingDetails($client_id, $client_master_id)
    {
        $is_default = 1;
        $sql = "INSERT INTO client_user_mapping (client_id, client_master_id, is_default) values(?, ?, ?)";
        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->bind_param("iii", $client_id, $client_master_id, $is_default);
            $result = $stmt->execute();
            $response["error"] = false;
        } else {
            $response["error"] = true;
            $response["message"] = QUERY_EXCEPTION;
        }
    }

}
