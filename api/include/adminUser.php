<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
require_once dirname(__FILE__) . '/DbConnect.php';
class adminUser extends utility
{

    // private $conn;
    public $db;

    public function __construct()
    {
        // opening db connection
        $this->db = new DbConnect();
        $this->conn = $this->db->connect();
    }

    public function addNewAdmin($admin_name, $admin_phone, $admin_email, $admin_role_id, $admin_password)
    {
        try {
            $userResult = $this->getFieldByID('admin_users', 'admin_email', $admin_email, 'admin_user_id');
            if ($userResult["error"] == true) {
                $this->conn->autocommit(false);
                $date = date("Y-m-d h:i:s");
                $is_active = 1;
                $response = array();
                $admin_password_hash = PassHash::hash($admin_password);
                $api_key = $this->generateApiKey();
                if ($stmt = $this->conn->prepare("INSERT INTO admin_users(admin_name, admin_phone, admin_email, admin_role_id, admin_password_hash, is_active, created_date, last_updated_on, api_key) values(?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
                    $stmt->bind_param("sssisisss", $admin_name, $admin_phone, $admin_email, $admin_role_id, $admin_password_hash, $is_active, $date, $date, $api_key);
                    $result = $stmt->execute();
                    $stmt->close();
                    if ($result) {
                        $this->conn->commit();
                        $response["error"] = false;
                        $response["message"] = ADD_ADMIN_SUCCESS;
                    } else {
                        $response["error"] = true;
                        $response["message"] = ADD_ADMIN_FAILURE;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            } else {
                // User with same email already existed in the db
                $response["error"] = true;
                $response["message"] = USER_ALREADY_EXISTED;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function updateAdmin($admin_user_id, $admin_name, $admin_phone, $admin_email, $admin_role_id)
    {
        try {
            $this->conn->autocommit(false);
            $date = date("Y-m-d h:i:s");
            $is_active = 1;
            $response = array();
            if ($stmt = $this->conn->prepare("UPDATE admin_users SET admin_name = ?, admin_phone = ?, admin_email = ?, admin_role_id = ?, last_updated_on = ? WHERE admin_user_id = ?")) {
                $stmt->bind_param("sssisi", $admin_name, $admin_phone, $admin_email, $admin_role_id, $date, $admin_user_id);
                $result = $stmt->execute();
                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    $response["message"] = UPDATE_ADMIN_SUCCESS;

                } else {
                    $response["error"] = true;
                    $response["message"] = UPDATE_ADMIN_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    /**
     * Deleting a branch
     * @param String $industry_type_id id of the task to delete
     */
    public function accessAdminUser($admin_user_id, $is_active)
    {
        try {
            $response = array();
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            //if ($stmt = $this->conn->prepare("UPDATE admin_users set is_active = ?, last_updated_on = ? WHERE admin_user_id = ?")) {
            if ($stmt = $this->conn->prepare("DELETE FROM admin_users  WHERE admin_user_id = ?")) {
                $stmt->bind_param("i", $admin_user_id);
                //$stmt->bind_param("isi", $is_active, $date, $admin_user_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    if ($is_active == 0) {
                        $response["message"] = ADMIN_DEACTIVATED_SUCCESS;
                    } else {
                        $response["message"] = ADMIN_ACTIVATED_SUCCESS;
                    }

                } else {
                    $response["error"] = true;
                    if ($is_active == 0) {
                        $response["message"] = ADMIN_DEACTIVATED_FAILURE;
                    } else {
                        $response["message"] = ADMIN_ACTIVATED_FAILURE;
                    }
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password)
    {
        try {
            $is_active = 1;
            //$result = $this->getApiKeyById($user_id);
            // fetching user by user id
            if ($stmt = $this->conn->prepare("SELECT admin_password_hash FROM admin_users WHERE admin_email = ? AND is_active = ?")) {

                $stmt->bind_param("si", $email, $is_active);

                $stmt->execute();

                $stmt->bind_result($admin_password_hash);

                $stmt->store_result();

                if ($stmt->num_rows > 0) {
                    // Found user
                    // Now verify the password
                    $stmt->fetch();

                    $stmt->close();

                    if (PassHash::check_password($admin_password_hash, $password)) {
                        $response["error"] = false;
                        $response["message"] = USER_LOGGED_IN;
                        $user = array();
                        $user = $this->getAdminUserDetail($email);
                        $response["userdetails"] = $user;

                        // User password is correct
                        return $response;
                    } else {
                        $response["error"] = true;
                        $response["message"] = INCORRECT_CREDENTIALS;
                        // user password is incorrect
                        return $response;
                    }
                } else {
                    $stmt->close();
                    $response["error"] = true;
                    $response["message"] = USER_NOT_ACTIVATED;
                    // user not existed with the user id
                    return $response;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * old password validation
     * @param String $user_id user id to check in db
     * @return boolean
     */
    public function getOldAdminPasswordFromDB($admin_user_id)
    {
        try {
            $response = array();

            $sql = "SELECT admin_password_hash FROM admin_users WHERE admin_user_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("i", $admin_user_id);
                $stmt->execute();
                $stmt->bind_result($oldpassword);
                $stmt->fetch();
                $response["error"] = false;
                $response["message"] = OLD_PASSWORD_EXIST;
                $response["password"] = $oldpassword;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function changePassword($admin_user_id, $oldpassword, $password)
    {
        try {
            $response = array();
            $this->conn->autocommit(false);
            require_once 'PassHash.php';
            // Generating password hash
            $admin_password_hash = PassHash::hash($password);
            $old_password_hash = PassHash::hash($oldpassword);
            $pwdResult = array();
            $pwdResult = $this->getOldAdminPasswordFromDB($admin_user_id);
            $old_password_hash_from_db = $pwdResult["password"];

            if (PassHash::check_password($old_password_hash_from_db, $oldpassword)) {
                if ($old_password_hash != $password_hash) {

                    $sql = "UPDATE admin_users SET admin_password_hash = ? WHERE admin_user_id = ?";

                    if ($stmt = $this->conn->prepare($sql)) {
                        $stmt->bind_param("si", $admin_password_hash, $admin_user_id);
                        $result = $stmt->execute();
                        $stmt->close();
                    } else {
                        $response["error"] = true;
                        $response["message"] = QUERY_EXCEPTION;
                    }

                    if ($result) {
                        $this->conn->commit();
                        $response["error"] = false;
                        $response["message"] = CHANGE_PASSWORD_SUCCESSFUL;
                    } else {
                        $response["error"] = true;
                        $response["message"] = CHANGE_PASSWORD_FAILURE;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = OLD_NEW_PASSWORD_SAME;
                }
            } else {
                $response["error"] = true;
                $response["message"] = OLD_PASSWORD_WRONG;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getAllAdminUsers($is_active)
    {
        try {
            $sql = "SELECT au.admin_user_id, au.admin_name, au.admin_phone, au.admin_email, au.admin_role_id, au.is_active, au.created_date, ar.role_name
					FROM admin_users au
					INNER JOIN admin_roles ar on au.admin_role_id = ar.admin_role_id ";

            if (strlen($is_active) > 0) {
                $sql .= " WHERE au.is_active = ? and ar.is_active = ?";
            }
            $sql .= " ORDER BY au.admin_name, au.is_active ";
            $response["adminUserDetails"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                if (strlen($is_active) > 0) {
                    $stmt->bind_param("ii", $is_active, $is_active);
                }
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($admin_user_id, $admin_name, $admin_phone, $admin_email, $admin_role_id, $is_active, $created_date, $role_name);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["admin_user_id"] = $admin_user_id;
                        $tmp["admin_name"] = $admin_name;
                        $tmp["admin_phone"] = $admin_phone;
                        $tmp["admin_email"] = $admin_email;
                        $tmp["admin_role_id"] = $admin_role_id;
                        $tmp["is_active"] = $is_active;
                        $tmp["created_date"] = $created_date;
                        $tmp["role_name"] = $role_name;
                        $response["adminUserDetails"][] = $tmp;
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAdminUserDetail($admin_email)
    {
        try {
            $is_active = 1;
            $sql = "SELECT au.admin_user_id, au.admin_name, au.admin_phone, au.admin_email, au.admin_role_id, au.admin_password_hash,
							au.is_active, au.created_date, au.last_updated_on, au.api_key, ar.role_name
					FROM admin_users au
					INNER JOIN admin_roles ar on au.admin_role_id = ar.admin_role_id
					WHERE au.is_active = ? and ar.is_active = ? ";

            if (strlen($admin_email) > 0) {
                $sql .= " AND admin_email = ? ";
            }
            if ($stmt = $this->conn->prepare($sql)) {
                if (strlen($admin_email) > 0) {
                    $stmt->bind_param("iis", $is_active, $is_active, $admin_email);
                } else {
                    $stmt->bind_param("ii", $is_active, $is_active);
                }
                if ($stmt->execute()) {
                    $stmt->bind_result($admin_user_id, $admin_name, $admin_phone, $admin_email, $admin_role_id, $admin_password_hash, $is_active, $created_date, $last_updated_on, $api_key, $role_name);
                    $stmt->fetch();
                    $stmt->close();
                    $response["error"] = false;
                    $response["message"] = USER_RECORD_FOUND;
                    $response["admin_user_id"] = $admin_user_id;
                    $response["admin_name"] = $admin_name;
                    $response["admin_phone"] = $admin_phone;
                    $response["admin_email"] = $admin_email;
                    $response["admin_role_id"] = $admin_role_id;
                    $response["admin_password_hash"] = $admin_password_hash;
                    $response["is_active"] = $is_active;
                    $response["created_date"] = $created_date;
                    $response["last_updated_on"] = $last_updated_on;
                    $response["api_key"] = $api_key;
                    $response["role_name"] = $role_name;
                } else {
                    $response["error"] = true;
                    $response["message"] = USER_DOES_NOT_EXIST;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAdminUserByID($admin_user_id)
    {
        try {
            $is_active = 1;
            $sql = "SELECT au.admin_user_id, au.admin_name, au.admin_phone, au.admin_email, au.admin_role_id, au.admin_password_hash,
							au.is_active, au.created_date, au.last_updated_on, au.api_key, ar.role_name
					FROM admin_users au
					INNER JOIN admin_roles ar on au.admin_role_id = ar.admin_role_id
					WHERE au.is_active = ? and ar.is_active = ? AND admin_user_id = ? ";

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("iii", $is_active, $is_active, $admin_user_id);

                if ($stmt->execute()) {
                    $stmt->bind_result($admin_user_id, $admin_name, $admin_phone, $admin_email, $admin_role_id, $admin_password_hash, $is_active, $created_date, $last_updated_on, $api_key, $role_name);
                    $stmt->fetch();
                    $stmt->close();
                    $response["error"] = false;
                    $response["message"] = USER_RECORD_FOUND;
                    $response["admin_user_id"] = $admin_user_id;
                    $response["admin_name"] = $admin_name;
                    $response["admin_phone"] = $admin_phone;
                    $response["admin_email"] = $admin_email;
                    $response["admin_role_id"] = $admin_role_id;
                    $response["admin_password_hash"] = $admin_password_hash;
                    $response["is_active"] = $is_active;
                    $response["created_date"] = $created_date;
                    $response["last_updated_on"] = $last_updated_on;
                    $response["api_key"] = $api_key;
                    $response["role_name"] = $role_name;
                } else {
                    $response["error"] = true;
                    $response["message"] = USER_DOES_NOT_EXIST;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sendResetKey($email)
    {
        try {
            $usql = "UPDATE admin_users SET reset_key = ?  WHERE admin_email = ? ";
            $this->conn->autocommit(false);
            $resetKey = $this->generateApiKey();
            $response = array();
            if ($stmt = $this->conn->prepare($usql)) {
                $stmt->bind_param("ss", $resetKey, $email);
                $result = $stmt->execute();

                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                    $subject = RESET_EMAIL_TITLE;
                    $body = RESET_EMAIL_CONTENT;
                    $resetURL = RESET_BASE_ADMIN_URL . $resetKey;
                    $userResult = $this->getFieldByID('admin_users', 'admin_email', $email, 'admin_name');
                    $mail_search = array('^user_first_name^', '^reset_url^');
                    $mail_replace = array($userResult['field_key'], $resetURL);
                    $body = str_replace($mail_search, $mail_replace, $body);
                    $subject = str_replace($mail_search, $mail_replace, $subject);
                    $res = $this->saveMail($email, $subject, $body);

                    if ($res == true) {
                        $response["error"] = false;
                        $response["message"] = RESET_PASSWORD_SUCCESSFUL;
                    } else {
                        $response["error"] = true;
                        $response["message"] = RESET_PASSWORD_FAILURE;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = RESET_PASSWORD_FAILURE;
                }

            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function resetPassword($reset_key, $new_password)
    {
        try {
            $adminResult = $this->getFieldByID('admin_users', 'reset_key', $reset_key, 'admin_user_id');
            if ($adminResult['error'] == false) {
                $admin_user_id = $adminResult['field_key'];
                $admin_password_hash = PassHash::hash($new_password);
                $reset_key = '';
                $sql = "UPDATE admin_users SET admin_password_hash = ?, reset_key = ? WHERE admin_user_id = ?";
                if ($stmt = $this->conn->prepare($sql)) {
                    $stmt->bind_param("ssi", $admin_password_hash, $reset_key, $admin_user_id);
                    $result = $stmt->execute();
                    $stmt->close();
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                    $response["message"] = CHANGE_PASSWORD_SUCCESSFUL;
                } else {
                    $response["error"] = true;
                    $response["message"] = CHANGE_PASSWORD_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = USER_NOT_ACTIVATED;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }
    public function resetAdminPassword($admin_user_id)
    {
        try {
            $new_password = $this->generateString();
            $admin_password_hash = PassHash::hash($new_password);
            $sql = "UPDATE admin_users SET admin_password_hash = ? WHERE admin_user_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("si", $admin_password_hash, $admin_user_id);
                $result = $stmt->execute();
                $stmt->close();
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            if ($result) {
                $this->conn->commit();
                $response["error"] = false;
                $response["message"] = CHANGE_PASSWORD_SUCCESSFUL;
                $response["new_password"] = $new_password;
            } else {
                $response["error"] = true;
                $response["message"] = CHANGE_PASSWORD_FAILURE;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getRoles()
    {
        try {
            $is_active = 1;
            $sql = "SELECT admin_role_id, role_name, is_active, created_date, last_updated_on FROM admin_roles WHERE is_active = ? ";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("i", $is_active);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($admin_role_id, $role_name, $is_active, $created_date, $last_updated_on);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["admin_role_id"] = $admin_role_id;
                        $tmp["role_name"] = $role_name;
                        $tmp["is_active"] = $is_active;
                        $tmp["created_date"] = $created_date;
                        $tmp["last_updated_on"] = $last_updated_on;
                        $response["roles"][] = $tmp;
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getUserRoles()
    {
        try {
            $is_active = 1;
            $sql = 'SELECT user_role_id, role_name, is_active, created_date, last_updated_on FROM user_roles WHERE is_active = ? ';
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param('i', $is_active);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($user_role_id, $role_name, $is_active, $created_date, $last_updated_on);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp['user_role_id'] = $user_role_id;
                        $tmp['role_name'] = $role_name;
                        $tmp['is_active'] = $is_active;
                        $tmp['created_date'] = $created_date;
                        $tmp['last_updated_on'] = $last_updated_on;
                        $response['roles'][] = $tmp;
                    }
                    $response['error'] = false;
                    $response['message'] = RECORD_FOUND;
                } else {
                    $response['error'] = true;
                    $response['message'] = NO_RECORD_FOUND;
                }
            } else {
                $response['error'] = true;
                $response['message'] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * close the database connection
     */
    public function __destruct()
    {
        // close the database connection
        //$this->conn->closeconnection();
        $this->db->closeconnection();
    }
}
