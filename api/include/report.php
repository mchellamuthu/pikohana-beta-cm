<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
require_once dirname(__FILE__) . '/DbConnect.php';
class report extends utility
{

    // private $conn;

    public function __construct()
    {

        // opening db connection
        $this->db = new DbConnect();
        $this->conn = $this->db->connect();
    }

    public function addNewReport($report_name, $report_auth_key, $report_logo, $is_source_single, $report_type)
    {
        try {
            $this->conn->autocommit(false);
            $date = date("Y-m-d h:i:s");
            $is_active = 1;
            $response = array();
            $reportResult = $this->getFieldByID('report_master', 'report_name', $report_name, 'report_id');
            if ($reportResult["error"] == true) {
                if (isset($report_logo['name']) && sizeof($report_logo) > 0) {
                    $upload_result = array();
                    $module = 'report';
                    $upload_result = $this->uploadImages($report_logo, $module, $report_name);
                    if ($upload_result["error"] == false) {
                        $report_logo_path = $upload_result["message"];
                    }
                }
                if ($stmt = $this->conn->prepare("INSERT INTO report_master(report_name, report_logo_path, report_auth_key, created_date, is_active, is_source_single, report_type) values(?, ?, ?, ?, ?, ?, ?)")) {
                    $stmt->bind_param("ssssiis", $report_name, $report_logo_path, $report_auth_key, $date, $is_active, $is_source_single, $report_type);
                    $result = $stmt->execute();
                    $stmt->close();
                    if ($result) {
                        $this->conn->commit();
                        $response["error"] = false;
                        $response["message"] = REPORT_CREATE_SUCCESS;

                    } else {
                        $response["error"] = true;
                        $response["message"] = REPORT_CREATE_FAILURE;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            } else {
                $response["error"] = true;
                $response["message"] = REPORT_ALREADY_EXIST;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    /**
     * Fetching all report_name
     *
     */
    public function getAllReports($searchItems)
    {
        try {
            $sql = "SELECT report_id, report_name, report_logo_path, report_auth_key, created_date, is_active, last_updated_on, 
                            is_source_single, report_type FROM report_master";

            if (sizeof($searchItems) > 0) {
                $sql .= ' where ';

                foreach ($searchItems as $key => $value) {
                    switch ($key) {
                        case 'report_id':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = "report_id = ? ";
                            break;
                        case 'is_active':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = "is_active = ? ";
                            break;
                        case 'is_source_single':
                            $a_param_type[] = 'i';
                            $a_bind_params[] = $value;
                            $query[] = "is_source_single = ? ";
                            break;

                        case 'more_reports':
                            $a_param_type[] = 's';
                            $a_bind_params[] = $value;
                            $query[] = " report_id NOT IN (?) ";
                            break;
                    }
                }

                $sql .= implode(' AND ', $query);

                $param_type = '';
                $n = count($a_param_type);
                for ($i = 0; $i < $n; $i++) {
                    $param_type .= $a_param_type[$i];
                }
                $a_params[] = &$param_type;
                for ($i = 0; $i < $n; $i++) {
                    $a_params[] = &$a_bind_params[$i];
                }

            }

            if (isset($searchItems['more_reports'])) {
                if (strlen($searchItems['more_reports']) > 0) {
                    $more_reports = $searchItems['more_reports'];
                    $sql .= " AND report_id NOT IN (" . $more_reports . ")";
                }
            }

            if (isset($searchItems["master_reports"]) && strlen(trim($searchItems["master_reports"])) > 0) {
                $search_report_ids = $searchItems["master_reports"];
                $sql .= " AND report_id IN ($search_report_ids)";
            }

            $sql .= " ORDER BY is_active desc";

            $response["reportDetails"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                if (sizeof($searchItems) > 0) {
                    call_user_func_array(array($stmt, 'bind_param'), $a_params);
                }
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $aReportSource = array();
                    $stmt->bind_result($report_id, $report_name, $report_logo_path, $report_auth_key, $created_date, $is_active, $last_updated_on, $is_source_single, $report_type);
                    while ($result = $stmt->fetch()) {

                        $tmp = array();
                        $tmp["report_id"] = $report_id;
                        $tmp["report_name"] = $report_name;
                        $tmp["report_logo_path"] = $report_logo_path;
                        $tmp["report_auth_key"] = $report_auth_key;
                        $tmp["created_date"] = $created_date;
                        $tmp["last_updated_on"] = $last_updated_on;
                        $tmp["is_active"] = $is_active;
                        $tmp["is_source_single"] = $is_source_single;
                        $tmp["report_type"] = $report_type;
                        $aReportSource = explode(',', $report_type);
                        $tmp["report_type_as_array"] = $aReportSource;
                        $response["reportDetails"][] = $tmp;
                        $response['report_ids'][] = $report_id;
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function updateReport($report_id, $report_name, $report_auth_key, $report_logo)
    {

        try {

            $response = array();
            $date = date("Y-m-d h:i:s");
            if (isset($report_logo['name']) && sizeof($report_logo) > 0) {
                $upload_result = array();
                $module = 'report';
                $upload_result = $this->uploadImages($report_logo, $module, $report_name);
                if ($upload_result["error"] == false) {
                    $report_logo_path = $upload_result["message"];
                }
            }
            $this->conn->autocommit(false);
            $is_active = 1;
            if ($stmt = $this->conn->prepare("UPDATE report_master set report_name = ?, report_logo_path = ? , report_auth_key = ? , last_updated_on = ? WHERE report_id = ? ")) {
                $stmt->bind_param("ssssi", $report_name, $report_logo_path, $report_auth_key, $date, $report_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = REPORT_UPDATE_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = REPORT_UPDATE_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    /**
     * Deleting a branch
     * @param String $report_name_id id of the task to delete
     */
    public function accessReport($report_id, $is_active)
    {
        try {
            $response = array();
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            if ($stmt = $this->conn->prepare("UPDATE report_master set is_active = ? WHERE report_id = ?")) {
                $stmt->bind_param("ii", $is_active, $report_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    if ($is_active == 0) {
                        $response["message"] = REPORT_DEACTIVATED_SUCCESS;
                    } else {
                        $response["message"] = REPORT_ACTIVATED_SUCCESS;
                    }
                } else {
                    $response["error"] = true;
                    if ($is_active == 0) {
                        $response["message"] = REPORT_DEACTIVATED_FAILURE;
                    } else {
                        $response["message"] = REPORT_ACTIVATED_FAILURE;
                    }
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    /**
     * close the database connection
     */
    public function __destruct()
    {
        // close the database connection
        $this->db->closeconnection();
    }

}
