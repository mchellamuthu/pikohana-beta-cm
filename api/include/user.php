<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
 require_once dirname(__FILE__) . '/DbConnect.php';
class user extends utility{

   // private $conn;
   public $db;

    function __construct() {
       // opening db connection
        $this->db = new DbConnect();
        $this->conn = $this->db->connect();
    }
	
	
	/**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
   public function createUser($email, $user_name, $mobile, $password) {
       try {
			require_once 'PassHash.php';
			$table_name = 'user_master';
			$response = array();
			$mobileResult = $this->getFieldByID($table_name, 'mobile', $mobile, 'user_id');
			if($mobileResult["error"] == true) {
				
				$this->conn->autocommit(false);
				$userResult = array();
				// Checking whether the email already exist or not!
				$userCodeResult = $this->generateUserCode();
				$user_code = $userCodeResult['user_code'];
							
				$sql = "INSERT INTO user_master (name, mobile, email, user_code, is_active, password_hash, created_date, last_updated_date, last_otp, last_otp_date, is_otp_verified) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				$userResult = $this->isUserExists('user_master', $email);
				
				// First check if user already existed in db
				if ($userResult["error"] == true) {
					$password_hash = PassHash::hash($password);               
					$date =  date("Y-m-d h:i:s");
					$is_active = 1;
					$is_otp_verified = 0;
					$otp = $this->generateOTP();
					if ($stmt = $this->conn->prepare($sql)) {				
						$stmt->bind_param("ssssisssssi", $user_name, $mobile, $email, $user_code, $is_active, $password_hash, $date, $date,  $otp, $date, $is_otp_verified);
						$result = $stmt->execute();
					
						if ($stmt->errno == 1062) {
							$response["error"] = true;
							$response["message"] = DUPLICATE_ENTRY;
							return $response;
						}
						$stmt->close();
					
				
						$aResult = $this->createUserKey($email);
						if ($aResult["error"] == false) {
						// Check for successful insertion
							if ($result) {
								$this->conn->commit();  //tempory
								$email_template = $this->getMailTemplate('USER_SIGNUP_MAIL');	
								if($email_template['error']==false){
									$subject = $email_template['email_title'];
									$body = $email_template['email_content'];
									$mail_search = array('^user_first_name^', '^otp_value^');
									$mail_replace = array($user_name, $otp);
									$body = str_replace($mail_search, $mail_replace, $body);
									$subject = str_replace($mail_search, $mail_replace, $subject);
									$this->saveMail($email, $subject, $body);
								}
								$sms_template = $this->getSMSTemplate('USER_SIGNUP_SMS');
								if($sms_template['error']==false){
									$message = str_replace($mail_search, $mail_replace, $sms_template['sms_content']);
									if ($this->sendsms($mobile, $message)) {
										//$this->conn->commit();
										$message = USER_CREATED_SUCCESSFULLY;
										$response["error"] = false;
										$response["message"] = $message;
									} else {
										$response["error"] = true;
										$response["message"] = SMS_SENT_FAILURE;
									}  
								}
								
							}
						} else {
						
							$response["error"] = true;
							$response["message"] = USER_CREATE_FAILED;
						}	
					} else {
						
						$response["error"] = true;
						$response["message"] = QUERY_EXCEPTION;
					}	
				} else {
					// User with same email already existed in the db
					$response["error"] = true;
					$response["message"] = USER_ALREADY_EXISTED;
				}
			} else {
					// User with same email already existed in the db
					$response["error"] = true;
					$response["message"] = USER_ALREADY_EXISTED;
				}	
				
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}	

	}
	
	 /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password) {
        try {
			$is_active = 1;
			$response = array();
			
			
			$sql = "SELECT password_hash FROM user_master WHERE email = ? AND is_active = ?";
			// fetching user by user id
			if ($stmt = $this->conn->prepare($sql)) {

				$stmt->bind_param("si", $email, $is_active);

				$stmt->execute();

				$stmt->bind_result($password_hash);

				$stmt->store_result();

				if ($stmt->num_rows > 0) {
					// Found user
					// Now verify the password
					$stmt->fetch();

					$stmt->close();

					if (PassHash::check_password($password_hash, $password)) {
						// User password is correct
						$user = array();
						$user = $this->getUserByUserID($email);
						 
						if ($user['error'] == false) {
							$response["userdetails"] = $user;
						} else {
							return $user;
						}
					} else {
						// user password is incorrect
						$response["error"] = true;
						$response["message"] = INCORRECT_CREDENTIALS;
						
					}
				} else {
					$stmt->close();
					$response["error"] = true;
					$response["message"] = INCORRECT_CREDENTIALS;

				}
			} else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
				
			}
			
			return $response;		
		} catch (Exception $e) {
			echo $e->getMessage();
		}	
    }
	
	
	 /**
     * old password validation
     * @param String $user_id user id to check in db
     * @return boolean
     */
    public function getOldPasswordFromDB($user_id) {
		try {
			$response = array();
			
			
			$sql = "SELECT password_hash FROM user_master WHERE user_id = ?";
			if ($stmt = $this->conn->prepare($sql)) {
				$stmt->bind_param("i", $user_id);
				$stmt->execute();
				$stmt->bind_result($oldpassword);
			//	$stmt->store_result();
				$stmt->fetch();
				
				$response["error"] = false;
				$response["message"] = OLD_PASSWORD_EXIST;
				$response["password"] = $oldpassword;
			} else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
			}
			return $response;
     		   } catch (Exception $e) {
			echo $e->getMessage();
		   }	
    }
    
    
    public function changePassword($user_id, $oldpassword, $password) {
		try {
			$response = array();
			$this->conn->autocommit(false);
			require_once 'PassHash.php';
			// Generating password hash
			$password_hash = PassHash::hash($password);
			$old_password_hash = PassHash::hash($oldpassword);
			$pwdResult = array();
			$pwdResult = $this->getOldPasswordFromDB($user_id);
			$old_password_hash_from_db = $pwdResult["password"];
			
			if ( PassHash::check_password($old_password_hash_from_db , $oldpassword)) {
				if ($old_password_hash != $password_hash) {
					
							
					$sql = "UPDATE user_master SET password_hash = ? WHERE user_id = ?";
			
					if ($stmt = $this->conn->prepare($sql)) {
						$stmt->bind_param("si", $password_hash, $user_id);	
						$result = $stmt->execute();
						$stmt->close();
					} else {
						$response["error"] = true;
						$response["message"] = QUERY_EXCEPTION;
					}	
						   
					if ($result) {
						$this->conn->commit();
						$email_template = $this->getMailTemplate('PASSWORD_UPDATE_MAIL');	
						if($email_template['error']==false){
							$userDetails = $this->getUserDetails($user_id, $is_seeker);
							
							$email = $eResponse['field_key'];
							$subject = $email_template['email_title'];
							$body = $email_template['email_content'];
							$mail_search = array('^user_first_name^');
							$mail_replace = array($userDetails['name']);
							$body = str_replace($mail_search, $mail_replace, $body);
							$this->saveMail($userDetails['email'], $subject, $body);
							$sms_template = $this->getSMSTemplate('PASSWORD_UPDATE_SMS');
							if($sms_template['error']==false){
								$message = str_replace($mail_search, $mail_replace, $sms_template['sms_content']);
								$this->sendsms($userDetails['mobile'], $message);
							}	
						}
						
						$response["error"] = false;
						$response["message"] = CHANGE_PASSWORD_SUCCESSFUL;
					} else {
						$response["error"] = true;
						$response["message"] = CHANGE_PASSWORD_FAILURE;
					}
				} else {
						$response["error"] = true;
						$response["message"] = OLD_NEW_PASSWORD_SAME;
				}
			} else {
				$response["error"] = true;
                $response["message"] = OLD_PASSWORD_WRONG;
			}
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}
	}	 

	
	
	
	/** Regenerate OTP code
	**/
	
	function regenerateOTP($email) {
		try {
			$this->conn->autocommit(false);
			$otp = $this->generateOTP();
			$date =  date("Y-m-d h:i:s");
			$is_otp_verified = 0;
			
			
			$sql = "UPDATE user_master SET last_otp = ?, last_otp_date = ?, is_otp_verified = ? where email = ?";
			if ($stmt = $this->conn->prepare($sql)) {
				$stmt->bind_param("ssis", $otp, $date, $is_otp_verified, $email);	
				$oresult = $stmt->execute();
				
				$stmt->close();
				if ($oresult) {
					$this->conn->commit();
					$response = array();
					
					$uResponse = $this->getFieldByID(user_master, 'email', $email, 'name');
					$pResponse = $this->getFieldByID(user_master, 'email', $email, 'mobile');
					$mail_search = array('^user_first_name^', '^otp_value^');
					$mail_replace = array($uResponse['field_key'], $otp);
				    $sms_template = $this->getSMSTemplate('REGENERATE_OTP_SMS');
					if($sms_template['error']==false){
						$message = str_replace($mail_search, $mail_replace, $sms_template['sms_content']);
						$result = $this->sendsms($pResponse['field_key'], $message);
						if ($result) {
							$email_template = $this->getMailTemplate('REGENERATE_OTP_MAIL');	
							if($email_template['error']==false){
								$subject = $email_template['email_title'];
								$body = $email_template['email_content'];
								
								$body = str_replace($mail_search, $mail_replace, $body);
								$subject = str_replace($mail_search, $mail_replace, $subject);
								$this->saveMail($email, $subject, $body);
							}
							
							$response["user_otp"] = array("new_otp" => $otp, "otp_updated_time" => $date);
							$response["error"] = false;
							$response["message"] = OTP_REGENERATED_SUCCESSFULLY;
						} else {
							$response["error"] = true;
							$response["message"] = OTP_REGENERATED_FAILURE;
						} 
					} else {
						$response["error"] = true;
						$response["message"] = OTP_REGENERATED_FAILURE;
					}
					
				}	
			} else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
			}	
				return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}
			
	}
	
	function verifyOTP($email, $otp) {
		try {
			$this->conn->autocommit(false);
			$aotp = $this->getOTPByUserId($email);
			
			$response = array();
			
			if (sizeof($aotp) > 0 && $aotp["error"] == false ) {
				$otpresult = $aotp["otpdetail"];
				if ($otpresult['is_otp_verified'] == 0) {
					$otp_in_db = $otpresult["otp"];
					$user_id = $otpresult['user_id'];
				
					
					$date =  date("Y-m-d h:i:s");
					$otp_dt_in_db = $otpresult["otp_date"];
					$timediff = round((strtotime($date) - strtotime($otp_dt_in_db)) /60);

					if ($otp == $otp_in_db ) {
						if ($timediff < OTP_EXPIRY_IN_MINUTES) {
							
										
							$is_otp_verified = 1;
							$sql = "UPDATE user_master SET is_otp_verified = ?, last_otp_date = ? where email = ?";
							if ($stmt = $this->conn->prepare($sql)){
								$stmt->bind_param("iss",  $is_otp_verified, $date, $email);	
								$verifyresult = $stmt->execute();
								$stmt->close();
								if ($verifyresult) {
									$this->conn->commit();
									$email_template = $this->getMailTemplate('USER_ACCOUNT_SUCCESS_ACTIVATION_MAIL');	
									if($email_template['error']==false){
										$uResponse = $this->getFieldByID(user_master, 'email', $email, 'name');
										$eResponse = $this->getFieldByID(user_master, 'email', $email, 'mobile');
										$email = $eResponse['field_key'];
										$subject = $email_template['email_title'];
										$body = $email_template['email_content'];
										$mail_search = array('^user_first_name^', '^OTP_VALUE^');
										$mail_replace = array($uResponse['field_key'], $otp);
										$body = str_replace($mail_search, $mail_replace, $body);
										$subject = str_replace($mail_search, $mail_replace, $subject);
										$this->saveMail($email, $subject, $body);
										$sms_template = $this->getSMSTemplate('USER_ACCOUNT_SUCCESS_ACTIVATION_SMS');
										if($sms_template['error']==false){
											$message = str_replace($mail_search, $mail_replace, $sms_template['sms_content']);
											$this->sendsms($eResponse['field_key'], $message);
										}
									}
									
									$response["error"] = false;
									$response["message"] = VERIFY_OTP_SUCCESSFUL;
									$response['user_id'] = $user_id;
									$aUserDetails = array();
								
									$aUserDetails = $this->getUserByUserID($user_id);
										
									
									$response["userdetails"] = $aUserDetails;
									
								} else {
									$response["error"] = true;
									$response["message"] = VERIFY_OTP_FAILURE;
								}
								
							} else {
								$response["error"] = true;
								$response["message"] = QUERY_EXCEPTION;
							}
						} else {
								$response["error"] = true;
								$response["otp_expiry"] = true;
								$response["message"] = OTP_EXPIRED;
						}
					
					} else {
							$response["error"] = true;
							$response["message"] = OTP_INCORRECT;
					}
				} else {
					$response['error'] = false;
					$response['message'] = OTP_VERIFIED_ALREADY;
				}	
			} else {
					$response["error"] = true;
					$response["message"] = NO_OTP_RECORDS;
			}
			return $response;
		} catch (Exception $e) {
				$this->conn->rollback();
				echo $e->getMessage();
		}
	}
	
	public function resetPassword($mobile, $email) {
		try {
			$goAhead = false;
			
			$userInfo = $this->getFieldByID('user_master', 'email', $email, 'name');
			
			if ($userInfo["error"] == false) {
				$user_name = $userResult['field_key'];
				$mobileInfo = $this->getFieldByID('user_master', 'email', $email, 'mobile');
				$mobileNumber = $mobileInfo['field_key'];
				$goAhead = true;
			} else {
				$goAhead = false;
			}		
			$usql = "UPDATE user_master SET password_hash = ? , last_updated_date = ? where email = ?";
			if ($goAhead == true) {
				$this->conn->autocommit(false);
				$randomPassword = $this->generateString();
				$random_password_hash = PassHash::hash($randomPassword);
				$date =  date("Y-m-d h:i:s");
				//$is_otp_verified = 0;
				$response= array();
				if ($stmt = $this->conn->prepare($usql)) {
					$stmt->bind_param("sss", $random_password_hash, $date, $email);	
					$presult = $stmt->execute();
					$stmt->close();
				} else {
					$response["error"] = true;
					$response["message"] = QUERY_EXCEPTION;
				}
				if ($presult) {
					$this->conn->commit();
					$email_template = $this->getMailTemplate('PASSWORD_RESET_CONFIRMATION_MAIL');	
					
					if($email_template['error']==false){
						$subject = $email_template['email_title'];
						$body = $email_template['email_content'];
						$mail_search = array('^user_first_name^', '^new_password^');
						$mail_replace = array($user_name, $randomPassword);
						$body = str_replace($mail_search, $mail_replace, $body);
						$subject = str_replace($mail_search, $mail_replace, $subject);
						$this->saveMail($email, $subject, $body);
					}	
					$sms_template = $this->getSMSTemplate('PASSWORD_RESET_CONFIRMATION_SMS');
					if($sms_template['error']==false){
						$message = str_replace($mail_search, $mail_replace, $sms_template['sms_content']);
						$result = $this->sendsms($mobileNumber, $message);
						if ($result) { 
							$response["error"] = false;
							$response["message"] = SMS_MAIL_RESET_PASSWORD_MESSAGE_SUCCESSFUL;
						} else {
							$response["error"] = true;
							$response["message"] = SMS_MAIL_RESET_PASSWORD_MESSAGE_FAILURE;
						}
					}
				}	
			} else {
				$response["error"] = true;
				$response["message"] = USER_DOES_NOT_EXIST;
			}
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}	
	}
	
    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getOTPByUserId($email) {
		try {
			$is_active = 1;
			
			
			if (is_numeric($email)) {
				$usql = "SELECT user_id, last_otp,last_otp_date, is_otp_verified FROM user_master WHERE user_id = ? AND is_active = ?";
			} else {
				$usql = "SELECT user_id, last_otp,last_otp_date, is_otp_verified FROM user_master WHERE email = ? AND is_active = ?";
			}	
	;
			if ($stmt = $this->conn->prepare($usql)) {
		
				if (is_numeric($email)) {
					
					$stmt->bind_param("si", $email, $is_active);
				} else {	
				
					$stmt->bind_param("si", $email, $is_active);
				}	
				if ($stmt->execute()) {
					$stmt->store_result();
					$stmt->bind_result($user_id, $last_otp, $last_otp_datetime, $is_otp_verified);
					$stmt->fetch();
					// TODO
					// $user_id = $stmt->get_result()->fetch_assoc();
					$stmt->close();
					$userOTP = array();
					$userOTP["otp"] = $last_otp;
					$userOTP["otp_date"] = $last_otp_datetime;
					$userOTP["is_otp_verified"] = $is_otp_verified;
					$userOTP['user_id'] = $user_id;
					$response["error"] = false;
					$response["message"] = OTP_EXIST;
					$response["otpdetail"] = $userOTP;
				} else {
					$response["error"] = true;
					$response["message"] = QUERY_EXCEPTION;
				}
			} else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
			}	
			return $response;	
		} catch (Exception $e) {
			echo $e->getMessage();
		}	
   	 }
	 
	 /**
     * Fetching user by id
     * @param String $user_id User  id 
     */
    public function getUserByUserID($user_id) {
		try {
			$is_active = 1;
			$user = array();
			$otpDetails = array();
			$objUser = new user();
			$objClaim = new claim();
			$otpDetails = $objUser->getOTPByUserId($user_id);
		
			if ($otpDetails['otpdetail']['is_otp_verified'] == 0) {
				$user["error"] = true;
				$user["message"] = OTP_NOT_VERIFIED;
				$user['is_otp_verified'] = $otpDetails['otpdetail']['is_otp_verified'];
				return $user;
			}
			if(is_numeric($user_id)) {
				$sql = "SELECT user_id, name, mobile, email, last_otp, created_date, last_updated_date, password_hash, last_otp_date, is_active,  user_code, is_otp_verified, last_login FROM user_master WHERE user_id = ? AND is_active = ?";
			} else {
				$sql = "SELECT user_id, name,  mobile, email,  last_otp, created_date, last_updated_date, password_hash, last_otp_date, is_active, user_code, is_otp_verified, last_login FROM user_master WHERE email = ? AND is_active = ?";
			}		
			if ($stmt = $this->conn->prepare($sql)) {
				if(is_numeric($user_id))
					$stmt->bind_param("ii", $user_id, $is_active);
				else
					$stmt->bind_param("si", $user_id, $is_active);
				if ($stmt->execute()) {
					$stmt->bind_result($user_id, $user_name, $mobile, $email, $last_otp, $created_date, $last_updated_date, $password_hash, $last_otp_date, $is_active, $user_code, $is_otp_verified, $last_login);
					$stmt->fetch();
					     $user= array();
					    $user["user_id"] = $user_id;
					    $user["is_otp_verified"] = $is_otp_verified;
						$user["user_code"] = $user_code;
						$user["name"] = $user_name;
						$user["mobile"] = $mobile;
						$user["email"] = $email;
						$user["last_otp"] = $last_otp;
						$user["profile_create_date"] = $created_date;
						$user["profile_lastupdate_date"] = $last_updated_date;
						$user["password_hash"] = $password_hash;
						$user["last_otp_date"] = $last_otp_date;
						$user["is_active"] = $is_active;
						$user["last_login"] = $last_login;
											
					$stmt->close(); 
				} else {
					$user["error"] = true;
					$user["message"] = QUERY_EXCEPTION . " - " . USER_QUERY;
				}
				 
				
					if ($stmt = $this->conn->prepare("SELECT api_key from manage_key where email = ? and is_active = ?")) {
						$stmt->bind_param("si", $email, $is_active);
						if ($stmt->execute()) {
							$stmt->bind_result($api_key);
							$stmt->fetch();
							$user["api_key"] = $api_key;
							$stmt->close();
								
						}
					} else {
						$user["error"] = true;
						$user["message"] = QUERY_EXCEPTION . " - " . KEY_QUERY;
					}
					
					$user['claimStatDetails'] = $objClaim->statsOnExpenseHeads($user_id);
				
			}
		
		return $user;
			
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}	
    }
	
	public function giveFeedback($user_id, $app_rating) {
       try {
			$this->conn->autocommit(false);
			
				$date =  date("Y-m-d h:i:s");
		
				$response = array();
				$feedbackResult = array();
				$feedbackResult = $this->isFeedbackAlreadyGiven($user_id);
				
				if ($feedbackResult["error"] == false) {
					if ($stmt = $this->conn->prepare("INSERT INTO user_feedback(user_id, app_rating, created_date) values(?, ?, ?)")) {
						$stmt->bind_param("sis", $user_id, $app_rating, $date);
						$result = $stmt->execute();
					     $stmt->close();
					}
				} else {	
					 if ($stmt = $this->conn->prepare("UPDATE user_feedback set app_rating = ? WHERE user_id = ?")) {
						$stmt->bind_param("is", $app_rating, $user_id);
						$result = $stmt->execute();
						
					    $stmt->close();
					}
				}
				if ($result) {
					$this->conn->commit();
					$email_template = $this->getMailTemplate('FEEDBACK_CREATE_MAIL');	
					if($email_template['error']==false){
						$userDetails = $this->getUserDetails($user_id);
						$email = $eResponse['field_key'];
						$subject = $email_template['email_title'];
						$body = $email_template['email_content'];
						$mail_search = array('^user_first_name^');
						$mail_replace = array($userDetails['name']);
						$body = str_replace($mail_search, $mail_replace, $body);
						$this->saveMail($userDetails['email'], $subject, $body);
						$sms_template = $this->getSMSTemplate('FEEDBACK_CREATE_SMS');
						if($sms_template['error']==false){
							$message = str_replace($mail_search, $mail_replace, $sms_template['sms_content']);
							$this->sendsms($userDetails['mobile'], $message);
						}	
					}
					$response["error"] = false;
					$response["message"] = FEEDBACK_CREATED_SUCCESSFULLY;
				} else {
					$response["error"] = true;
					$response["message"] = FEEDBACK_CREATE_FAILURE;
				}
				
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}			
		
 }
 
    public function isFeedbackAlreadyGiven($user_id) {
        try {
			$response = array();
		
			if ($stmt = $this->conn->prepare("SELECT user_id from user_feedback where user_id = ?")) {
				$stmt->bind_param("s", $user_id);
				$stmt->execute();
				$stmt->store_result();
				
				$num_rows = $stmt->num_rows;
				$stmt->close();
				
				if ($num_rows <= 0) {
					$response["error"] = false;
					$response["message"] = NO_FEEDBACK_FOUND;
				} else {
					$response["error"] = true;
					$response["message"] = FEEDBACK_ALREADY_GIVEN;
				}
			} else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
			}	
			return $response;
		} catch (Exception $e) {
			echo $e->getMessage();
		}	
    }
	
}

?>