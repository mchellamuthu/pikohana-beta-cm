<?php
/**
 * Database configuration
 */

$live = false;
$staging = false;

if ($live) {
    $db_user= "pikohana_dba";
    $db_pass= "HPAKT6Ve";
    $db_name= "pikohana_prd";
    $db_host= "pikohana-prd.cqolviivmhgq.ap-southeast-1.rds.amazonaws.com";
    $base_url = "http://".$_SERVER['SERVER_NAME'];
    $xero_oauth_callback = $base_url. '/api/v1/abc.php';
    define('ENV_LIVE', $live);
    define('BIZ_FILE_PATH', '/var/www/pikohana.com/media/bizfile/');
}
else if ($staging) {
    $db_user= "pikohana_dba";
    $db_pass= "HPAKT6Ve";
    $db_name= "pikohana_beta";
    $db_host= "pikohana-prd.cqolviivmhgq.ap-southeast-1.rds.amazonaws.com";
    $base_url = "http://".$_SERVER['SERVER_NAME']."/beta";
    $xero_oauth_callback = $base_url. '/api/v1/abc.php';
    define('ENV_LIVE', $staging);
    define('BIZ_FILE_PATH', '/var/www/pikohana.com/beta/media/bizfile/');
}
else {
    $db_user= "root";
    $db_pass= ""; 
    $db_name= "pikohana-beta";
    $db_host= "127.0.0.1";
    $base_url = "http://localhost/pikohana-beta-dev";
    $xero_oauth_callback = $base_url . '/api/v1/abc.php';
    define('ENV_LIVE', false);
    define('BIZ_FILE_PATH','/opt/lampp/htdocs/pikohana-beta-dev/media/bizfile/');
}

define('OAUTH_CALLBACK', $xero_oauth_callback);

define('DB_USERNAME', $db_user);
define('DB_PASSWORD', $db_pass);
define('DB_HOST', $db_host);
define('DB_NAME', $db_name);
define('DB_PORT', 3306);

define('WEBSITE_URL', $base_url);
define('CLIENT_PIC_URL', $base_url.'media/client');
define('APPLICATION_PIC_URL', $base_url.'media/application/');
define('URL', 'www.yourhousing.com');
define('CUSTOMER_CARE_NUMBER', '+6593271966');
define('QUERY_EXCEPTION', 'Please check mysql query. Fatal error');
define('WELCOME_MESSAGE', 'Welcome to On Job, your account is activated, now you can access your account.Kindly save our help center number ^Number^');
define('API_KEY_MISSING', 'Api key is missing');
define('API_ACCESS_DENIED', 'Access Denied. Invalid Api key');
define('UPDATE_PROFILE_SUCCESSFUL', 'Profile updated successfully');
define('UPDATE_PROFILE_FAILURE', 'Profile update failure');
define('IS_ACTIVE','Is active');
define('INVALID_CLIENT', 'Invalid user');
define('PIC_BASE_URL', 'media');
define('CLIENT_LOGGED_IN', 'Successfully logged in');
define('INCORRECT_CREDENTIALS', 'Invalid credentials');
define('CLIENT_CREATED_SUCCESSFULLY', 'Client created successfully');
define('CLIENT_CREATE_FAILED', 'Client cannot be created');
define('CLIENT_ALREADY_EXISTED', 'Client already exist');
define('CLIENT_UPDATED_SUCCESSFULLY', 'Client updated successfully');
define('CLIENT_USER_CREATED_SUCCESSFULLY', 'Client user created successfully');
define('CLIENT_USER_CREATE_FAILED', 'Client user cannot be created');
define('CLIENT_USER_ALREADY_EXISTED', 'Client user already exist');
define('CLIENT_USER_UPDATED_SUCCESSFULLY', 'Client user updated successfully');
define('DELETE_CLIENT_SUCCESS', 'Client deleted successfully');
define('DELETE_CLIENT_FAILURE', 'Client failed to delete');
define('CLIENT_UPDATE_FAILED', 'Client cannot be updated');
define('CLIENT_DOES_NOT_EXIST', 'Client does not exist');
define('CLIENT_RECORD_FOUND', 'Client record found');
define('CHANGE_PASSWORD_SUCCESSFUL', 'Password changed successfully');
define('CHANGE_PASSWORD_FAILURE', 'Password change failed');
define('OLD_NEW_PASSWORD_SAME', 'Old and New password is the same');
define('OLD_PASSWORD_WRONG', 'Old password is wrong. Please try again');
define('DUPLICATE_ENTRY', 'Data already exist');
define('NO_CLIENT_FOUND', 'No Client found');
define('CLIENT_QUERY', 'Please check Client table');
define('EMAIL_DUPLICATE_ENTRY', 'E-MAil already exist');
define('PASSWORD_MINIMUM_LENGTH', 'Password must be minimum 8 characters');
define('ADD_ADMIN_SUCCESS', 'Admin created successfully');
define('ADD_ADMIN_FAILURE', 'Admin cannot be created');
define('UPDATE_ADMIN_SUCCESS', 'Admin updated successfully');
define('UPDATE_ADMIN_FAILURE', 'Admin failed to update');
define('DELETE_ADMIN_SUCCESS', 'Admin deleted successfully');
define('DELETE_ADMIN_FAILURE', 'Admin failed to delete');
define('IMAGE_UPLOAD_SUCCESS', 'Image uploaded successfully');
define('IMAGE_UPLOAD_FAILURE', 'Image upload failure');
define('UPLOAD_FAIL', 'Upload failure');
define('IMAGE_OVERSIZE', 'Image size exceeds 135px*145px. Please upload another one.');
define('INVALID_FORMAT', 'Image should be jpg, png, gif, bmp, pdf format');
define('NO_FILE_SELECTED', 'File not selected');
define('RECORD_FOUND' , 'Record found');
define('NO_RECORD_FOUND', 'No record found');
define('USER_ALREADY_EXISTED', 'User already exist');
define('USER_LOGGED_IN', 'User logged in');
define('USER_RECORD_FOUND', 'User found');
define('CHOOSE_CLIENT_ADMIN', 'Are you a client or an admin?');
define('APPLICATION_CREATE_FAILURE', 'Application cannot be created');
define('APPLICATION_CREATE_SUCCESS', 'Application created successfully');
define('APPLICATION_UDPATE_FAILURE', 'Application cannot be updated');
define('APPLICATION_UPDATE_SUCCESS', 'Application updated successfully');
define('APPLICATION_DELETE_FAILURE', 'Application cannot be deleted');
define('APPLICATION_DELETE_SUCCESS', 'Application deleted successfully');
define('APPLICATION_ALREADY_EXIST', 'Application already exist.');
define('REPORT_CREATE_FAILURE', 'Report cannot be created');
define('REPORT_CREATE_SUCCESS', 'Report created successfully');
define('REPORT_UDPATE_FAILURE', 'Report cannot be updated');
define('REPORT_UPDATE_SUCCESS', 'Report updated successfully');
define('REPORT_DELETE_FAILURE', 'Report cannot be deleted');
define('REPORT_DELETE_SUCCESS', 'Report deleted successfully');
define('REPORT_ALREADY_EXIST', 'Report already exist.');
define('NOT_CLIENT', 'You have to login as client to perform this action');
define('NOT_ADMIN', 'You have to login as admin to perform this action');
define('RESET_PASSWORD_SUCCESSFUL', 'Your Password reset is successful. Please check your email. ');
define('RESET_PASSWORD_FAILURE', 'Reset Password failure. Please try after sometime');
define('NOTIFICATION_ENABLED_SUCCESS', 'Notification enabled successfully');
define('NOTIFICATION_DISABLED_SUCCESS', 'Notification disabled successfully');
define('NOTIFICATION_MARKED_AS_READ', 'Notification marked as Read');
define('ALREADY_READ_OR_WRONG_RECORD', 'Either notification is already in read state or it is not yours');
define('CLIENT_APPLICATION_MAP_CREATED_SUCCESS', 'Application mapping success');
define('CLIENT_APPLICATION_MAP_CREATED_FAILURE', 'Application mapping failure');
define('UPDATE_CLIENT_APPLICATION_MAP_SUCCESS', 'Updating of application mapping successful');
define('UPDATE_CLIENT_APPLICATION_MAP_FAILURE', 'Updating of application mapping failure');
define('DELETE_CLIENT_APPLICATION_MAP_SUCCESS', ' Deleting of application mapping successful');
define('DELETE_CLIENT_APPLICATION_MAP_FAILURE', 'Deleting of application mapping failure');
define('ALREADY_EXIST', 'Already exist. Please try another');
define('HISTORY_CREATE_FAILURE', 'History cannot be created');
define('HISTORY_CREATE_SUCCESS', 'History created successfully');
define('REPORT_CLIENT_APPLICATION_MAP_CREATED_SUCCESS', 'Report - Client - Application mapping success');
define('REPORT_CLIENT_APPLICATION_MAP_CREATED_FAILURE', 'Report - Client - Application mapping failure');
define('UPDATE_REPORT_CLIENT_APPLICATION_MAP_SUCCESS', 'Updating of Report - Client - Application mapping successful');
define('UPDATE_REPORT_CLIENT_APPLICATION_MAP_FAILURE', 'Updating of Report - Client - Application mapping failure');
define('DELETE_REPORT_CLIENT_APPLICATION_MAP_SUCCESS', ' Deleting of Report - Client - Application mapping successful');
define('DELETE_REPORT_CLIENT_APPLICATION_MAP_FAILURE', 'Deleting of Report - client - Application mapping failure');
define('SAVE_NOTIFICATION_SUCCESS', 'Notification creation success');
define('SAVE_NOTIFICATION_FAILURE', 'Notification creation failure');
define('USER_DOES_NOT_EXIST', 'User does not exist');
define('ADMIN_DEACTIVATED_SUCCESS', 'Admin User deactivated successfully');
define('ADMIN_ACTIVATED_SUCCESS', 'Admin User activated successfully');
define('ADMIN_DEACTIVATED_FAILURE', 'Admin User cannot be deactivated. Please contact Site admin.');
define('ADMIN_ACTIVATED_FAILURE', 'Admin User cannot be activated. Please contact Site admin.');
define('APPLICATION_DEACTIVATED_SUCCESS', 'Application deactivated successfully');
define('APPLICATION_ACTIVATED_SUCCESS', 'Application activated successfully');
define('APPLICATION_DEACTIVATED_FAILURE', 'Application cannot be deactivated. Please contact Site admin.');
define('APPLICATION_ACTIVATED_FAILURE', 'Application cannot be activated. Please contact Site admin.');
define('CLIENT_ACTIVATED_SUCCESS', 'Client activated successfully');
define('CLIENT_ACTIVATED_FAILURE', 'Client cannot be activated. Please contact Site admin.');
define('CLIENT_DEACTIVATED_SUCCESS', 'Client deactivated successfully');
define('CLIENT_DEACTIVATED_FAILURE', 'Client cannot be deactivated. Please contact Site admin.');
define('CLIENT_USER_ACTIVATED_SUCCESS', 'Client User(s) activated successfully');
define('CLIENT_USER_ACTIVATED_FAILURE', 'Client User(s) cannot be activated. Please contact Site admin.');
define('CLIENT_USER_DEACTIVATED_SUCCESS', 'Client User(s) deactivated successfully');
define('CLIENT_USER_DEACTIVATED_FAILURE', 'Client User(s) cannot be deactivated. Please contact Site admin.');
define('REPORT_DEACTIVATED_SUCCESS', 'Report deactivated successfully');
define('REPORT_ACTIVATED_SUCCESS', 'Report activated successfully');
define('REPORT_DEACTIVATED_FAILURE', 'Report cannot be deactivated. Please contact Site admin.');
define('REPORT_ACTIVATED_FAILURE', 'Report cannot be activated. Please contact Site admin.');
define('UPDATE_SECURITY_QUESTION_SUCCESS', 'Update security question success');
define('UPDATE_SECURITY_QUESTION_FAILURE', 'Update security question failure');
define('RESET_EMAIL_TITLE', 'Pikahona - Your request to reset password');
define('RESET_EMAIL_CONTENT', '<html><head></head><body><table><tr><td>Hi ^user_first_name^,</td></tr><tr><td>We have received your request to reset your password.</td></tr><tr><td><a href=^reset_url^> Click this link </a> </td></tr><tr></tr><tr><td>If this was not initiated by you, do reach out to us immediately at  +6593271966 OR write to us at support@pikohana.com</td></tr><tr></tr><tr><td>Thanks,</td></tr><tr><td>PikoHANA Team</td></tr><tr><td><hr></td></tr><tr><td>You are receiving this mail because you have an active account at PikoHANA. For any queries, you can reach out to us @ support@pikohana.com </td></tr></table></body></html>');
define('RESET_BASE_CLIENT_URL', $base_url.'client/#/resetpassword/');
define('RESET_BASE_ADMIN_URL', $base_url.'admin/#/resetpassword/');
define('MAIL_FROM', 'support@pikohana.com');
define('MAILER', 'smtp');
define('PIKOHANA', 'PIKOHANA');
define('MAIL_HOST', 'pikohana.com');
define('NEW_PASSWORD', 'Password');
define('RESET_KEY', 'Token');
define('USER_NOT_ACTIVATED', 'You do not have an active account with Pikohona');
define('REPORT_ARCHIVED_SUCCESSFULLY', 'Report archived successfully');
define('REPORT_ARCHIVE_FAILURE', 'Report cannot be archived');
define('KPI_ZOHO_FETCH_URL', '/private/json/CustomModule1/searchRecords');
define('WEIGHTED_PIPELINE_ZOHO_FETCH_URL', '/private/json/Potentials/getRecords');
define('SALES_ORDERS_ZOHO_FETCH_URL', '/private/json/SalesOrders/getRecords');
define('KEY_GENERATE_SUCCESS', 'Key generate success');
define('KEY_GENERATE_FAILURE', 'Key generate failure');
define('KEY_ALREADY_EXIST', 'Key already exist');
define('UPDATE_AUTHORIZE_SUCCESS', 'Authorize update success');
define('UPDATE_AUTHORIZE_FAILURE', 'Authorize update failure');
define('CONFIGURE_COMPANY', 'Please configure company in your profile to get data for this report');
define('CLIENT_URL', $base_url.'/client/#/');
define('TOKEN_EXPIRED', 'Token expired');
define('USER_NOT_AUTHORIZED', 'User not authorized to view Xero data');
define('LOGIN_COUNT_SUCCESS', 'Login Success');
define('LOGIN_COUNT_FAILURE', 'Login failure');
define('XERO_HAVING_PROBLEM','Xero experiencing difficulty in getting data');
define('BUDGET_IMPORT_SUCCESS','Data imported successfully');
define('BUDGET_IMPORT_FAILURE', 'Data import failure');
define('ADD_CATEGORY_SUCCESS', 'Add Category success');
define('ADD_CATEGORY_FAILURE', 'Add Category failure');
define('ADD_SUB_CATEGORY_SUCCESS', 'Add Sub Category success');
define('ADD_SUB_CATEGORY_FAILURE', 'Add Sub Category failure');
define('DELETE_BUDGET_SUCCESS', 'Delete budget success');
define('DELETE_BUDGET_FAILURE', 'Delete budget failure');
define('NO_AUTH_KEYS', 'No Auth keys');
define('ISSUE_ON_KEYS', 'Please verify keys');
define('KEYS_FOUND', 'Keys found');
define('SERVER_NO_RESPONSE', 'Could not connect to CRM system to get data');
define('NO_CRM_RECORD_FOUND', 'Data not available in the CRM system');
define('XERO_AUTH_PROBLEM', 'Some issue with Xero. Please check after sometime.');
define('UPDATE_CACHE_LOG_SUCCESS','Update cache log success');
define('UPDATE_CACHE_LOG_FAILURE','Update cache log failure');

/*** Login fields *****/
define('CLIENT_ID', 'Client ID');
define('CLIENT_NAME', 'Name');
define('CLIENT_EMAIL', 'Email');
define('CLIENT_PHONE', 'Mobile Number');
define('CLIENT_ROLE', 'Role');
define('CLIENT_PASSWORD', 'Password');

define('APPLICATION_ID', 'Application ID');
define('API_URL', 'Api URL');
define('AUTH_KEY', 'Auth Key');
define('START_DATE', 'Start Date');
define('END_DATE', 'End Date');
// define('IS_ACTIVE', 'Enabled or Disabled?');
define('SECURITY_QUESTION', 'Security Question');
define('SECURITY_ANSWER', 'Security Answer');
define('REPORT_NAME', 'Report Name');
define('REPORT_AUTH_KEY', 'Report level auth key');
define('AUTHENTICATE','Authenticate');
define('PAGE_NAME','Page name');
define('REPORT_TYPE', 'Report Type');

/** SIGN UP fields ***/
define('NAME', 'Full Name');
define('EMAIL', 'Email');
define('MOBILE', 'Mobile');
define('PASSWORD', 'Password');
define('CUSTOMER_KEY', 'Customer Key');
define('SECRET_KEY', 'Secret Key');
define('IS_AUTH_REQUIRED', 'Is auth key');

/******  ADMIN SIGN UP***/

define('ADMIN_NAME', 'Full Name');
define('ADMIN_EMAIL', 'Email');
define('ADMIN_PHONE', 'Mobile');
define('ADMIN_ROLE_ID', 'Role');
define('ADMIN_PASSWORD', 'Password');

/***Application fields ***/
define('APPLICATION_NAME', 'Application Name');
define('APPLICATION_TYPE', 'Application Type');
define('APPLICATION_API_URL', 'Application API URL');
define('STATUS', 'Status');
define('IS_READ', 'Is Notification Read?');
define('CLIENT_APPLICATION_MAP_ID', 'Client to Application Mapping ID');
define('REPORT_ID', 'Report ID');
define('NOTIFICATION_MESSAGE', 'Notification Message');
// define('IS_READ', 'Is it tested or not?');
define('COMPANY_NAME', 'Company Name');
define('COMPANY_OWNER', 'Company Owner');
define('COMPANY_ADDRESS', 'Company Address');
define('TIME_ZONE', 'Time Zone');
define('NOTIFICATION_STATUS', 'Notification Status');
define('HISTORY_URL', 'History URL');
define('GENERATED_DATE', 'Generated Date');
define('IS_SOURCE_SINGLE', 'Single Source?');
define('AUTHORIZATION_FAILURE', 'Authorization failure');
define('OAUTH_VERIFIER', 'oauth verifier');
//define('DEFAULT_CURRENCY', 'SGD');
define('IS_SYSTEM_TOKEN_REQUIRED', 'Application Level Token');
define('IS_USER_AUTH_KEY_REQUIRED', 'User Level Token');
define('DEFAULT_CURRENCY', 'Default Currency');

/*** Change Password fields *****/
define('OLDPASSWORD', 'Old Password');
define('NEWPASSWORD', 'New Password');

/*** Clientname numeric msg ***/
define('NUMERIC_NOT_ALLOWED', 'Only numeric is not allowed');

/*** Email message ***/
define('EMAIL_ALREADY_EXISTED', 'Email ID already exists.');
define('EMAIL_DOES_NOT_EXIST', 'Email ID does not exists.');

/*** Mobile message ***/
define('MOBILE_ALREADY_EXISTED', 'Mobile number already exists.');
define('MOBILE_DOES_NOT_EXIST', 'Mobile number does not exists.');
define('ORG_NAME', 'Organisation Name');
define('APP_SITE_URL', 'Application Site URL');
define('CUSHION_VALUE', 'Cushion');
define('FLASH', 1);
define('SALES', 2);
define('PNL', 3);
define('KPI', 4);
define('XERO', 1);
define('ZOHO', 2);
define('BRAND_ID', 1000);
define('TERRITORY_ID', 1000);
define('CLIENT_MASTER_ID', 'Client Master ID');
define('BASE_PATH', dirname(__FILE__) );
define('XRO_APP_TYPE', 'Partner');
define('XERO_ACCOUNT', 'Pikohana');



define('BANK_SUMMARY_API', 'https://api.xero.com/api.xro/2.0/Reports/BankSummary');
define('PROFIT_LOSS_API', 'https://api.xero.com/api.xro/2.0/Reports/ProfitAndLoss');
define('BALANCE_SHEET_API', 'https://api.xero.com/api.xro/2.0/Reports/BalanceSheet');
define('BUDGET_SUMMARY_API', 'https://api.xero.com/api.xro/2.0/Reports/BudgetSummary');
define('INVOICE_API', 'https://api.xero.com/api.xro/2.0/invoices');
define('CURRENCY_API', 'https://api.xero.com/api.xro/2.0/Currencies');
define('BANK_TRANSACTION_API', 'https://api.xero.com/api.xro/2.0/banktransactions');
define('INVOICE_CONTACT_BY_NAME', 'https://api.xero.com/api.xro/2.0/Invoices');
define('CHART_OF_ACCOUNT_CATEGORY_API', 'https://api.xero.com/api.xro/2.0/Accounts');
define('INCOME','Income');
define('NON_OPERATING_EXPENSE','Non-operating Expenses');
define('TOTAL_OPERATING_EXPENSES', 'Total Operating Expenses');
define('LESS_OPERATING_EXPENSES', 'Less Operating Expenses');
define('OPERATING_EXPENSES', 'Operating Expenses');
define('TOP_OPERATING_EXPENSES', 'Top Operating Expenses');
define('TOP_N_RECORDS', 10);
define('LESS_COST_OF_SALES', 'Less Cost of Sales');
define('EMPLOYEE_COMPENSATION', 'Employee Compensation & Related Expenses');
define('GROSS_PROFIT', 'Gross Profit');
define('NET_PROFIT', 'Net Profit');
define('OPERATING_PROFIT', 'Operating Profit');
define('NON_OPERATING_INCOME', 'Non-operating Income');
define('TOTAL_NON_OPERATING_INCOME', 'Total Non-operating Income');
define('TOTAL_NON_OPERATING_EXPENSE', 'Total Non-operating Expenses');
define('OTHER_INCOME_EXPENSE', 'Other Income (Expenses)');
define('TOTAL_OTHER_INCOME_EXPENSE', 'Total Other Income (Expenses)');
define('CUSHION_VALUE_UPDATED','Cushion updated successfully');
define('CUSHION_VALUE_NOT_UPDATED','Cushion cannot be updated');
define('CLIENT_TOKEN_REMOVED','Client token reset done');
define('CLIENT_TOKEN_NOT_REMOVED','Client token cannot be reset');
define('REPORT_START_DATE', 'Show from');
define('REPORT_END_DATE', 'Show till');
define('CATEGORY_SUB_IDS', 'Category Sub Ids');
define('APPLICATION_IDS','Application Ids');
define('CLIENT_CATEGORY_ID1','First Tracking Category');
define('CLIENT_CATEGORY_ID2','Second Tracking Category');
define('SUB_CATEGORY_ID1','First Tracking Sub Category');
define('SUB_CATEGORY_ID2','Second Tracking Sub Category');
define('employees', 'Employees count');
define('NO_CATEGORY_RECORD_FOUND', 'Tracking Categories not found');

/**Biz file upload**/
define('BIZ_UPLOAD_SUCCESS', 'Bizfile upload successfully');
define('BIZ_UPLOAD_FAILURE', 'Bizfile upload failure');
define('BIZ_FILE_FORMAT', 'Please upload only pdf file');
define('BIZ_FILE_VALIDATION', 'Please upload BizFile (pdf format) for compliance tab');

/**Weekly PDF Report settings**/
define('WEEKLY_REPORT_SUCCESS', 'Weekly report saved successfully');
define('WEEKLY_REPORT_EXISTS', 'Weekly report already exists');
define('WEEKLY_REPORT_UPDATE_SUCCESS', 'Weekly report updated successfully');
define('WEEKLY_REPORT_NO_RECORD', 'No record found');
define('WEEKLY_REPORT_UPDATE_FAIL', 'Update failed');
define('WEEKLY_REPORT_DELETE_FAIL', 'Deleted failed');
define('WEEKLY_REPORT_DELETE_SUCCESS', 'Weekly report deleted successfully');

/*Invoice contact by name*/
define('NO_INVOICE_LIST', 'No invoice record found');
define('INVOICE_API_SUCCESS', 'Invoice getting successfully');

/**Email Settings**/
define('SMTP_USERNAME', 'info@pikohana.com');
if ($live) {
    define('SMTP_PASSWORD', 'PikoHANA01');
}
else {
    define('SMTP_PASSWORD', 'PikoHANA01#123');
}
define('FROM_NAME', 'PIKOHANA');
define('FROM_EMAIL', 'info@pikohana.com');
define('SMTP_HOST', "smtp.office365.com");
define('SMTP_PORT', "587");

define('CC_MAIL_ID', "info@pikohana.com");
define('TESTING_EMAIL', "ramesh.g@systimanx.com");

/** Create Group settings **/
define('CREATE_GROUP_INVALID_ID', 'Invalid group');
define('CREATE_GROUP_INVALID_CLIENT_ID', 'Invalid client');
define('CREATE_GROUP_SUCCESS', 'Group saved successfully');
define('CREATE_GROUP_UPDATE_SUCCESS', 'Group updated successfully. Kindly, sync consolidation group to match the source data for members of this group.');
define('NO_GROUP_CLIENT_FOUND', 'No group client found');
define('NO_COMPANY_FOUND', 'No company found');
define('NO_GROUP_DETAILS_FOUND', 'company details not found');
define('GROUP_LINK_SUCCESS', 'Group client link saved successfully');
define('GROUP_MASTER_LINK_INV_REQUEST', 'Invalid request');
define('MASTER_LINK_SUCCESS', 'Master client link saved successfully');
define('GROUP_LINK_INVALID_CLIENT_ID', 'Invalid client id');
define('GROUP_LINK_UPDATE_SUCCESS', 'Group Link updated successfully');
define('MASTER_LINK_UPDATE_SUCCESS', 'Master Link updated successfully');
define('NO_GROUP_LINK_LIST', 'No record found');
define('GROUP_NAME_EXIST', 'Group name already exist');
define('GROUP_DATE_UPDATE', 'Group Date updated successfully. Kindly, sync consolidation group to match the source data for members of this group.');
define('GROUP_NAME_UPDATE', 'Group Name updated successfully');
define('GROUP_LOGO_UPDATE', 'Group Logo updated successfully');
define('GROUP_DELETE', 'Group deleted successfully.');
define('GROUP_DELETE_FAILED', 'Group not deleted successfully.');
define('GROUP_DELETE_FAILED_MAPPED', 'Currently group is mapped to client.So, can\'t able to delete the group');
define('NO_GROUP_FOUND', 'Group not found.');
define('SYNC_GROUP', "This consolidated group is currently in sync with the source data for members of this group.");
define('CONSOLIDATE_DIFF','Adjustment Upon Consolidation');

/* Fathom Category API */
define('ACCOUNT_CATGORY_API_SUCCESS', 'Categories inserted/updated successfully');

define('REPORT_GEN_INVALID_CLIENT_ID', 'Invalid client id');
define('REPORT_GEN_ADD_SUCCESS', 'Record added successfully');
define('REPORT_GEN_UPDATE_SUCCESS', 'Record updated successfully');
define('Eliminate_UPDATE_SUCCESS', 'Eliminate updated successfully');

/* Multi company client */
define('COMPANY_CREATED_SUCCESSFULLY','Company created successfully');
define('COMPANY_UPDATE_FAILED','Company updated successfully');
define('COMPANY_EXIST', 'Company name already exist');

/* Highend zoho auth key */
define('HIGHEND_AUTHKEY', '1e61f879334d2e44c7174ddc1c18ed84');

/* Audit Activities */
define('schedule_next_audit', '-2');           //60 days (2 months) before company’s year-end
define('begin_audit', '2');                    //60 days (2 months) after company’s year-end
define('complete_audit', '4');                //120 days (4 months) after company’s year-end
define('hold_agm', '5');                      //150 days (5 months) after company’s year-end
define('file_annual_return', '6');            //180 days (6 months) after company’s year-end
define('file_eci', '2');                       //60 days (2 months) after company’s year-end
define('file_prior_years_tax_return', '1');     //November 1st

/* XE.COM Currency Converter  */
define('CURRENCY_URL', 'https://xecdapi.xe.com/v1/convert_from.json');
define('ACCOUNT_ID', 'foleytangrouppteltd179424557');
define('ACCOUNT_API_KEY', 'ac48nn29ejdrbk8er6kv59h4nm');
define('MONTHLY_CURRENCY_URL', 'https://xecdapi.xe.com/v1/monthly_average.json');
define('HISTORIC_CURRENCY_URL', 'https://xecdapi.xe.com/v1/historic_rate.json');
define('CONSOL_HISTORIC_CURRENCY_URL', 'https://xecdapi.xe.com/v1/stats');

?>