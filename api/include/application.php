<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
    require_once dirname(__FILE__) . '/DbConnect.php';
class application extends utility {

   // private $conn;

    function __construct() {
    
        // opening db connection
        $this->db = new DbConnect();
       $this->conn = $this->db->connect();
    }

	

	public function addNewApplication($application_name, $application_logo, $application_type, $application_api_url, $is_system_token_required, $auth_key, $is_user_auth_key_required, $app_site_url) {
		try {
			$this->conn->autocommit(false);
			$date =  date("Y-m-d h:i:s");
			$is_active = 1;
			$response = array();
			$applnResult = $this->getFieldByID('application_master', 'application_name', $application_name, 'application_id');
			if ($applnResult["error"] == true) {
				if (isset($application_logo['name']) && sizeof($application_logo)> 0) {
					$upload_result = array();
					$module = 'application';
					$upload_result = $this->uploadImages($application_logo, $module, $application_name);
					if ($upload_result["error"] == false) {
						$application_logo_path = $upload_result["message"];
					}
				}
				if ($stmt = $this->conn->prepare("INSERT INTO application_master(application_name, application_logo_path, application_api_url, application_type, created_date, is_active, is_system_token_required, application_auth_key, is_user_auth_key_required, app_site_url) values(?,?, ?,?, ?, ?, ?, ?, ?, ?)")) {
					$stmt->bind_param("sssssiisis", $application_name, $application_logo_path, $application_api_url, $application_type, $date, $is_active, $is_system_token_required, $auth_key, $is_user_auth_key_required, $app_site_url);
					$result = $stmt->execute();
					$stmt->close();
					if ($result) {
						$this->conn->commit();
						$response["error"] = false;
						$response["message"] = APPLICATION_CREATE_SUCCESS;
						$response["application_logo_path"] = $application_logo_path;
						
					} else {
						$response["error"] = true;
						$response["message"] = APPLICATION_CREATE_FAILURE;
					}
				} else {
					$response["error"] = true;
					$response["message"] = QUERY_EXCEPTION;
				}
			} else {
				$response["error"] = true;
				$response["message"] = APPLICATION_ALREADY_EXIST;
			}	
		
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}	
    }

	 /**
     * Fetching all application_name
     *
     */
    public function getAllApplications($searchItems) {
		try {
			$query = array();
			//$is_active = 1;
			$sql = "SELECT application_id, application_name, application_logo_path, application_api_url, application_type, created_date, is_active, 
							last_updated_on, is_system_token_required, application_auth_key, is_user_auth_key_required, app_site_url 
					FROM application_master ";
			$a_params = [];
			if (sizeof($searchItems) > 0) {
				$sql .= ' where ' ;
			
				foreach ($searchItems as $key=>$value) {
					if ($key != "more_applications") {
						switch($key) {
							 case 'application_id':
								$a_param_type[] = 'i';
								$a_bind_params[] = $value;
								$query[] = "application_id = ? ";
							break;
							case 'is_active':
								$a_param_type[] = 'i';
								$a_bind_params[] = $value;
								$query[] = "is_active = ? ";
							break;
							case 'application_type':
								$a_param_type[] = 's';
								$a_bind_params[] = $value;
								$query[] = "application_type = ? ";
							break;
							case 'is_auth_required':
								$a_param_type[] = 'i';
								$a_bind_params[] = $value;
								$query[] = " is_auth_required = ? ";
							break;
							
						}
					}
				}
					
				$sql .= implode(' AND ', $query);
					
				if (!isset($searchItems['more_applications'])) {	
					$param_type = '';
					$n = count($a_param_type);
					for($i = 0; $i < $n; $i++) {
						$param_type .= $a_param_type[$i];
					}
					$a_params[] = & $param_type;
					for($i = 0; $i < $n; $i++) {
						$a_params[] = & $a_bind_params[$i];
					}
				}	
				else {
						$more_applications = $searchItems['more_applications'];
						if (sizeof($searchItems) == 1) {
							$sql .= "  application_id NOT IN (" . $more_applications . ")";
						} else {
							$sql .= " AND application_id NOT IN (" . $more_applications . ")";
						}
				}
			}
					
			$sql .= " ORDER BY is_active desc";

			$response["applicationDetails"] = array();
			$response['application_ids'] = array();
			if ($stmt = $this->conn->prepare($sql)) {
				if ((sizeof($searchItems) > 0) && (sizeof($a_params) > 0))  {
					call_user_func_array(array($stmt, 'bind_param'), $a_params);
				}
	
				$stmt->execute();
				$stmt->store_result();
				if( $stmt->num_rows > 0 ) {
					$keys = array();
					$stmt->bind_result($application_id, $application_name,$application_logo_path, $application_api_url, $application_type, $created_date, $is_active, $last_updated_on, $is_system_token_required, $auth_key, $is_user_auth_key_required, $app_site_url);
					while ($result = $stmt->fetch()) {
				
						$tmp = array();
						$tmp["application_id"] = $application_id;
						$tmp["application_name"] = $application_name;
						$tmp["application_logo_path"] = $application_logo_path;
						$tmp["application_api_url"] = $application_api_url;
						$tmp["app_site_url"] = $app_site_url;
						$tmp["application_type"] = $application_type;
						$tmp["created_date"] = $created_date;
						$tmp["last_updated_on"] = $last_updated_on;
						$tmp["is_active"] = $is_active;
						$tmp["is_system_token_required"] = $is_system_token_required;
						$tmp["is_user_auth_key_required"] = $is_user_auth_key_required;
						if ($is_system_token_required == 0) {
							$tmp["customer_key"] = "";
							$tmp["secret_key"] = "";	
						} else {
							$tmp["application_auth_key"] = $auth_key;
							$keys = explode(':', $auth_key);
							$tmp["customer_key"] = $keys[0];
							$tmp["secret_key"] = $keys[1];
						}
						
					    $response["applicationDetails"][] = $tmp;
						$response['application_ids'][] = $application_id;
					}
						$response["error"] = false;
						$response["message"] = RECORD_FOUND;
					} else {
						$response["error"] = true;
						$response["message"] = NO_RECORD_FOUND;
					}
			} else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
			}	
			return $response;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
    }
	
    public function updateApplication($application_id, $application_name, $application_logo, $application_type, $application_api_url, $app_site_url, $auth_key) {
		try {
			$response = array();
			$date =  date("Y-m-d h:i:s");
			if (isset($application_logo['name']) && sizeof($application_logo) > 0) {
				$upload_result = array();
				$module = 'application';
				$upload_result = $this->uploadImages($application_logo, $module, $application_name);
				if ($upload_result["error"] == false) {
					$application_logo_path = $upload_result["message"];
				}
			}
			else{
				$sql = "SELECT application_logo_path FROM application_master WHERE application_id = ?";
				if ($stmt = $this->conn->prepare($sql)) {
					$stmt->bind_param("i", $application_id);
					$stmt->execute();
					$stmt->bind_result($application_logo_path);
					$stmt->fetch();
					$stmt->close();
				} 
			}
			
			$this->conn->autocommit(false);
			$is_active = 1;
			if ($stmt = $this->conn->prepare("UPDATE application_master set application_name = ?, application_logo_path = ? , application_api_url = ? , application_type = ?, last_updated_on = ?, app_site_url = ?, application_auth_key = ? WHERE application_id = ? ")) {
				$stmt->bind_param("sssssssi", $application_name, $application_logo_path, $application_api_url, $application_type, $date, $app_site_url, $auth_key, $application_id);
				$result = $stmt->execute();
				$this->conn->commit();
				$stmt->close();
				if ($result) {
					$response["error"] = false;
					$response["message"] = APPLICATION_UPDATE_SUCCESS;
					$response["application_logo_path"] = $application_logo_path;
				} else {
					$response["error"] = true;
					$response["message"] = APPLICATION_UPDATE_FAILURE;
				}
			} else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
			}	
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}
    }
	
	   /**
     * Deleting a branch
     * @param String $application_name_id id of the task to delete
     */
    public function accessApplication($application_id, $is_active) {
		try {
			$response = array();
			$date =  date("Y-m-d h:i:s");
			$this->conn->autocommit(false);
			if ($stmt = $this->conn->prepare("UPDATE application_master set is_active = ? WHERE application_id = ?")) {
				$stmt->bind_param("ii", $is_active, $application_id);
				$result = $stmt->execute();
				$this->conn->commit();
				$stmt->close();
				if ($result) {
					$response["error"] = false;
					if ($is_active == 0) {
						$response["message"] = APPLICATION_DEACTIVATED_SUCCESS;
					} else {
						$response["message"] = APPLICATION_ACTIVATED_SUCCESS;
					}
				} else {
					$response["error"] = true;
					if ($is_active == 0) {
						$response["message"] = APPLICATION_DEACTIVATED_FAILURE;
					} else {
						$response["message"] = APPLICATION_ACTIVATED_FAILURE;
					}
				}
			} else {
				$response["error"] = true;
				$response["message"] = QUERY_EXCEPTION;
			}	
			return $response;
		} catch (Exception $e) {
			$this->conn->rollback();
			echo $e->getMessage();
		}		
    }
	/**
	 * close the database connection
	 */
	 public function __destruct() {
	 // close the database connection
		 $this->db->closeconnection();
	 }
	
}

?>