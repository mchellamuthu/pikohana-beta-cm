var app = angular.module('myApp',[]);
app.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/home',
            templateUrl: './KPI.html'
        })
        
        // nested list with custom controller
        .state('home.list', {
            url: '/list',
            templateUrl: 'partial-home-list.html',
            controller: function($scope) {
                $scope.dogs = ['Bernese', 'Husky', 'Goldendoodle'];
            }
        })
        
        // nested list with just some random string data
        .state('home.paragraph', {
            url: '/paragraph',
            template: 'I could sure use a drink right now.'
        })
        
        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('about', {
            url: '/about',
            views: {
                '': { templateUrl: 'partial-about.html' },
                'columnOne@about': { template: 'Look I am a column!' },
                'columnTwo@about': { 
                    templateUrl: 'table-data.html',
                    controller: 'scotchController'
                }
            }
            
        });
        
});

app.controller('AppCtrl',['$scope',function($scope){
	$scope.custom = true;
	$scope.custom1 = true;
	$scope.custom2 = true;
	$scope.custom3 = true;
	$scope.custom4 = true;

	$scope.navbarLeft = {active:false};
	$scope.navbarLeftToggle = function(){
		$scope.navbarLeft.active = !$scope.navbarLeft.active;
		if ($scope.navbarLeft.active==true) {
			$('.icon-left-icon1').removeClass('icon-left-icon1').addClass('icon-left-icon1-white');
			$('.icon-left-icon3').removeClass('icon-left-icon3').addClass('icon-left-icon3-white');
			$('.icon-left-icon4').removeClass('icon-left-icon4').addClass('icon-left-icon4-white');
			$('.icon-left-icon5').removeClass('icon-left-icon5').addClass('icon-left-icon5-white');
		}else{
			$('.icon-left-icon1-white').removeClass('icon-left-icon1-white').addClass('icon-left-icon1');
			$('.icon-left-icon3-white').removeClass('icon-left-icon3-white').addClass('icon-left-icon3');
			$('.icon-left-icon4-white').removeClass('icon-left-icon4-white').addClass('icon-left-icon4');
			$('.icon-left-icon5-white').removeClass('icon-left-icon5-white').addClass('icon-left-icon5');
		}
	};
}]);

// app.directive('toggleClass', function($scope){
// 	return {
// 		restrict: 'A',
// 		link: function(scope,element,attrs){
// 			element.bind('click',function(){
// 				element.toggleClass(attrs.toggleClass);
// 			});
// 		}
// 	};
// });