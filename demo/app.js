var app = angular.module('pikohanademo', ['ui.router', 'ui.bootstrap', 'ngAnimate', 'ngResource', 'ngSanitize', 'restangular', 'vcRecaptcha']);

app.config(function ($stateProvider, $urlRouterProvider) {

    //$urlRouterProvider.otherwise('/login');    
    $stateProvider
	    .state('login', {
	        url: '/login',
	        templateUrl: './login.html'
	    })

        .state('forgetpassword', {
            url: '/forgetpassword',
            templateUrl: './forgetpassword.html'
        })

		.state('dashboard', {
		    url: '/dashboard',
		    templateUrl: './dashboard.html',

		})

        .state('flashreport', {
            url: '/flashreport',
            templateUrl: 'Flashreport.html'
        })

        .state('plreport', {
            url: '/plreport',
            templateUrl: './PL.html',

        })

		.state('salesreport', {
		    url: '/salesreport',
		    templateUrl: './SalesReport.html',

		})

		.state('reporthistory', {
		    url: '/reporthistory',
		    templateUrl: './ReportHistory.html',

		})

		.state('kpi', {
		    url: '/kpi',
		    templateUrl: './KPI.html',

		})

		.state('myapps', {
		    url: '/myapps',
		    templateUrl: './MyApps.html',
		})

        .state('profilesettings', {
            url: '/profilesettings',
            templateUrl: './ProfileSetting.html',
        })

        .state('passwordsettings', {
            url: '/passwordsettings',
            templateUrl: './PasswordSettings.html',
        })

		.state('system/report', {
		    url: '/system/report',
		    templateUrl: './SystemConfiguration.html',
		})

		.state('manageusers/add', {
		    url: '/manageusers/add',
		    templateUrl: './manageusers.html',
		})

		.state('manageusers/list', {
		    url: '/manageusers/list',
		    templateUrl: './manageuserslist.html',
		})

});

app.controller('AppCtrl', ['$scope', function ($scope) {
    $scope.edits = function () { alert(1) }

    $scope.edit = false;
    $scope.custom = true;
    $scope.custom1 = true;
    $scope.custom2 = true;
    $scope.custom3 = true;
    $scope.custom4 = true;

    $scope.navbarLeft = { active: false };
    $scope.navbarLeftToggle = function () {
        $scope.navbarLeft.active = !$scope.navbarLeft.active;
        if ($scope.navbarLeft.active == true) {
            $('.icon-left-icon1').removeClass('icon-left-icon1').addClass('icon-left-icon1-white');
            $('.icon-left-icon3').removeClass('icon-left-icon3').addClass('icon-left-icon3-white');
            $('.icon-left-icon4').removeClass('icon-left-icon4').addClass('icon-left-icon4-white');
            $('.icon-left-icon5').removeClass('icon-left-icon5').addClass('icon-left-icon5-white');
        } else {
            $('.icon-left-icon1-white').removeClass('icon-left-icon1-white').addClass('icon-left-icon1');
            $('.icon-left-icon3-white').removeClass('icon-left-icon3-white').addClass('icon-left-icon3');
            $('.icon-left-icon4-white').removeClass('icon-left-icon4-white').addClass('icon-left-icon4');
            $('.icon-left-icon5-white').removeClass('icon-left-icon5-white').addClass('icon-left-icon5');
        }
    };
}]);

