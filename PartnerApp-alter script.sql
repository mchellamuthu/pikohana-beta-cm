ALTER TABLE `client_oauth_token` ADD `session_handle` VARCHAR(255) NOT NULL AFTER `is_authenticated`, ADD `expires` BIGINT NOT NULL AFTER `session_handle`, ADD `client_master_id` BIGINT NOT NULL AFTER `client_id`;

CREATE TABLE `api_hits_history` (
  `id` bigint(20) NOT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `client_master_id` bigint(20) DEFAULT NULL,
  `method_name` varchar(255) DEFAULT NULL,
  `api_method` varchar(255) NOT NULL,
  `api_endpoint` text NOT NULL,
  `response_data` longtext,
  `response_code` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `api_hits_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_hits_history`
--
ALTER TABLE `api_hits_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 21, 2018 at 09:31 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `pikohana-beta`
--

-- --------------------------------------------------------

--
-- Table structure for table `cron_jobs`
--

CREATE TABLE `cron_jobs` (
  `id` bigint(20) NOT NULL,
  `cmd` longtext NOT NULL,
  `pid` bigint(20) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `client_master_id` bigint(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cron_jobs`
--
ALTER TABLE `cron_jobs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cron_jobs`
--
ALTER TABLE `cron_jobs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;
